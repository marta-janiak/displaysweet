function check_assignment_mode()
{

    if (confirm("Are you sure you wish to move to Reassignment Mode?"))
    {
        //window.location.href="/projects/7/floors/hotspots/6/reassign/";

    }
}

function select_new_floor(floor_id)
{
    redraw_main_canvas();

    $(".floor-details").removeClass('selected');
    $("#floor-details-"+floor_id).addClass('selected');

    $("#current_floor_id").val(floor_id);

    $(".apartment-details").hide();
    $(".apartment-details-"+ floor_id).show();

    var apartment = $("#selected_apartment").val();
    var project_id = $("#current_project_id").val();

    $(".listed-image-with-tags").css({"background": "white"});

    get_image(project_id, floor_id, "level_plans", apartment);

    redraw_main_canvas();
    redraw_apartment_lists();

}

function get_image(project_id, floor_id, value_selected, apartment)
{
    $.ajax({
        url: '/get_image_selection_details/' + project_id + '/' + floor_id + '/' + value_selected + '/',
        type: 'GET',
        data: {'apartment': apartment, 'value_selected': value_selected},
        success: function (data) {
            var static_image = data.static_image;

            $("#floor-plan-image img").attr('src', static_image).css("padding-top", '0px').css("padding-bottom", '0px');
        }

    });
}

function save_new_coordinate(project_id, apartment_id)
{
    var x_coordinate = $("#new_apartment_set_x_coordinate_"+apartment_id).val();
    var y_coordinate = $("#new_apartment_set_y_coordinate_"+apartment_id).val();

    $("#cross-hatch").unbind("click").css('cursor', 'default');
    var add_new = $("#add-new-hotspot-"+apartment_id).val();

    $.ajax({
        url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
        type: 'GET',
        data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new},
        success: function (data) {

            $("#apartment_set_x_coordinate_"+apartment_id).val(x_coordinate);
            $("#apartment_set_y_coordinate_"+apartment_id).val(y_coordinate);

            $("#new_apartment_set_x_coordinate_"+apartment_id).val('');
            $("#new_apartment_set_y_coordinate_"+apartment_id).val('');

            $().toastmessage('showSuccessToast', 'New co-ordinates saved successfully.');

            add_event_listener(x_coordinate, y_coordinate);

            $("#apartment_x_"+apartment_id).val(x_coordinate);
            $("#apartment_y_"+apartment_id).val(y_coordinate);

            //refresh_floor_hotspots();

            redraw_main_canvas();
            redraw_apartment_spot(x_coordinate, y_coordinate, 'blue');

            var coord_exists = $('#refresh-coordinates-x-'+apartment_id).length;
            if (coord_exists == 0)
            {
                $(".refresh-coordinate").show();
            }

            $("#save-button-"+apartment_id).hide();
            click_to_close_hotspot(apartment_id);
            $(".save-button").show();

            $("#cross-hatch").off("mousemove", mouse_move);
            document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

    },
        error: function (jqXHR, textStatus, errorMessage) {
            //console.log(errorMessage); // Optional
        }
    });

}

function copy_new_coordinates(project_id)
{
    if (confirm("Are you sure you wish to copy these apartments to the selected floors below?"))
    {
        var selected_apartments = $('.select-hotspot:checkbox:checked');
        var selected_floors = $('.floor-checkbox:checkbox:checked');

        $().toastmessage('showToast', {
             text     : 'Copying, please wait.',
             sticky   : true,
             position : 'top-right',
             type     : 'notice',
             closeText: ''
        });

        var floors_len = $(selected_floors).length;

        console.log('floors_len', floors_len);

        var count = 0;

        var current_floor_id = 0;

        $(selected_floors).each(function(index, element) {
            var floor_id = $(this).attr("id");
            floor_id = floor_id.replace("floor-hotspot-", "");

            $(selected_apartments).each(function() {
                var apartment_id = $(this).attr("id");
                apartment_id = apartment_id.replace("select-hotspot-", "");

                jQuery.get("/copy_apartment_to_floor/"+project_id+"/"+apartment_id+"/"+floor_id+"/",
                        function (data) {

                            if (data.floor_id != current_floor_id)
                            {
                                count ++;
                                current_floor_id = data.floor_id;
                            }

                            console.log(count);

                            if (floors_len == count - 1) {
                                $().toastmessage('showToast', {
                                     text     : 'Coordinates of hotspots have been copied successfully',
                                     sticky   : true,
                                     position : 'top-right',
                                     type     : 'notice',
                                     closeText: ''
                                });
                                refresh_floor_hotspots();
                            }
                });
            });

        });



    }
}

function remove_new_coordinates(project_id)
{
    if (confirm("Are you sure you wish to remove these hotspots?"))
    {
        var checked_boxes = $('.select-hotspot:checkbox:checked');

        $(checked_boxes).each(function() {

            var apartment_id = $(this).attr("id");

            apartment_id = apartment_id.replace("select-hotspot-", "");

            var x_coordinate = '0';
            var y_coordinate = '0';
            var add_new = $("#add-new-hotspot-"+apartment_id).val();


            $.ajax({
                url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
                type: 'GET',
                data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new},
                success: function (data) {

                    $("#apartment_set_x_coordinate_"+apartment_id).val(x_coordinate);
                    $("#apartment_set_y_coordinate_"+apartment_id).val(y_coordinate);

                    $("#apartment_x_"+apartment_id).val(x_coordinate);
                    $("#apartment_y_"+apartment_id).val(y_coordinate);

                    $("#new_apartment_set_x_coordinate_"+apartment_id).val('');
                    $("#new_apartment_set_y_coordinate_"+apartment_id).val('');

                    $("#refresh-coordinates-x-"+apartment_id).hide();
                    $("#refresh-coordinates-y-"+apartment_id).hide();
                    $(".refresh-coordinate").hide();

                    redraw_main_canvas();
                    refresh_floor_hotspots();
                     }
            });

        });


        $().toastmessage('showSuccessToast', 'Co-ordinates of hotspots have been deleted successfully.');
        $("#select-all-apartments").click();



    }

}

function remove_coordinates(project_id, apartment_id)
{
    if (confirm("Are you sure you wish to clear this hotspot?"))
    {

        var x_coordinate = '0';
        var y_coordinate = '0';
        var add_new = $("#add-new-hotspot-"+apartment_id).val();

         $.ajax({
            url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
            type: 'GET',
            data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new},
            success: function (data) {

                $("#apartment_set_x_coordinate_" + apartment_id).val(x_coordinate);
                $("#apartment_set_y_coordinate_" + apartment_id).val(y_coordinate);

                $("#apartment_x_" + apartment_id).val(x_coordinate);
                $("#apartment_y_" + apartment_id).val(y_coordinate);

                $("#new_apartment_set_x_coordinate_" + apartment_id).val('');
                $("#new_apartment_set_y_coordinate_" + apartment_id).val('');

                $("#refresh-coordinates-x-" + apartment_id).hide();
                $("#refresh-coordinates-y-" + apartment_id).hide();
                $(".refresh-coordinate").hide();

                $("#highlight-" + apartment_id).hide();
                $("#show_current_hotspots_" + apartment_id).hide();
                redraw_main_canvas();
                refresh_floor_hotspots();
            }
        });

        $().toastmessage('showSuccessToast', 'Co-ordinates of hotspots have been cleared successfully.');


    }

}

function update_hotspot_figures(project_id, apartment_id)
{
    var x_coordinate = $("#apartment_x_"+apartment_id).val();
    var y_coordinate = $("#apartment_y_"+apartment_id).val();
    var add_new = $("#add-new-hotspot-"+apartment_id).val();

     $.ajax({
        url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
        type: 'GET',
        data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new},
        success: function (data) {

            $("#apartment_set_x_coordinate_" + apartment_id).val(x_coordinate);
            $("#apartment_set_y_coordinate_" + apartment_id).val(y_coordinate);

            $().toastmessage('showSuccessToast', 'Co-ordinates of hotspot have been updated successfully.');

            refresh_floor_hotspots();
        }
    });
}

function redraw_apartment_lists()
{
    ////console.log('redraw_apartment_lists');
    var apartment_list = $.parseJSON($("#apartment_list").val());
    show_current_values(apartment_list);

    $("#select_all").change(function(){
       $(".floor-checkbox").prop('checked', $(this).prop("checked"));
       update_floor_hotspots();
    });
}

$(function() {
    var selected_colour = $("#selected-colour").val();

    $("#select-all-apartments").change(function(){
        var floor_id = $("#current_floor_id").val();
       $(".apartment-checkbox-"+floor_id).prop('checked', $(this).prop("checked"));
    });

    var floor_id = $("#current_floor_id").val();
    $(".apartment-details").hide();
    $(".apartment-details-"+ floor_id).show();


    redraw_apartment_lists();

    var element = $('#apartment-scroll'),
        originalY = element.offset().top;

    // Space between element and top of screen (when scrolling)
    var topMargin = 20;

    // Should probably be set in CSS; but here just for emphasis
    element.css('position', 'relative');

    $(window).on('scroll', function(event) {
        var scrollTop = $(window).scrollTop();

        if (parseFloat(scrollTop) <= 620)
        {
            element.stop(false, false).animate({
                top: scrollTop < originalY
                        ? 0
                        : scrollTop - originalY + topMargin
            }, 0);
        }

    });


});


function highlight_reassignment_hotspot(apartment_id, x_value, y_value)
{
    $('#show_current_hotspots_'+apartment_id).toggle();

    if ($("#show_current_hotspots_"+apartment_id).is(":visible")) {
        redraw_apartment_spot(x_value, y_value, 'green');
    }
    else
    {
        refresh_floor_hotspots();
    }
}

function redraw_main_canvas()
{
    var selected_colour = $("#selected-colour").val();
    ////console.log('redraw_main_canvas');
    //redraw entire canvas stuff to remove this floor's apartments.
    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.canvas.width = ctx.canvas.width;

    $("#selected_x").val('');
    $("#selected_y").val('');

    ctx.strokeStyle = $("#selected-colour").val();

    var size = 550;

    ctx.moveTo((size/2),0);
    ctx.lineTo((size/2),size);
    ctx.stroke();

    ctx.moveTo(0,(size/2));
    ctx.lineTo(size,(size/2));
    ctx.stroke();

    ctx.font = "10px Arial";

    ctx.fillText("0",0,(size/2));
    ctx.fillText("1",(size/2),size);
    ctx.fillText("1",size-5,(size/2));
    ctx.fillText("0",(size/2)-5,8);
}

function update_floor_hotspots()
{
    redraw_main_canvas();

    var checked_boxes = $('.floor-checkbox:checkbox:checked');
    $(checked_boxes).each(function(index, object) {

        var project_id = $(this).attr('project_id');
        var checkbox_id = $(this).attr('id');
        checkbox_id = checkbox_id.replace("floor-hotspot-", "");

        if($("#floor-hotspot-"+checkbox_id).is(':checked'))
        {
            jQuery.get("/get_floor_hotspots/"+project_id+"/"+checkbox_id+"/", function (data) {
                total_showing += 1;
                $("#total_showing").val(total_showing);

                var apartment_list = $.parseJSON(data.apartment_list);

                show_current_values(apartment_list);
            });
        }

    });

    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);
}


function refresh_floor_hotspots()
{
    redraw_main_canvas();

    var floor_id = $('#current_floor_id').val();
    var project_id = $("#current_project_id").val();

    jQuery.get("/get_floor_hotspots/"+project_id+"/"+floor_id+"/", function (data) {
        var apartment_list = $.parseJSON(data.apartment_list);
        show_current_values(apartment_list);
    });
}

function mouse_move(evt) {

    var x_value = $("#x_coordinate").val();
    var y_value = $("#y_coordinate").val();

    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");

    var mousePos = getMousePos(canvas, evt);

    var x = (parseFloat(mousePos.x) / 550.0).toFixed(4);
    var y = (parseFloat(mousePos.y) / 550.0).toFixed(4);

    var message = 'x: ' + x + ' y: ' + y;

    ////console.log('adding message');
    //writeMessage(canvas, message);

    ctx.canvas.width = ctx.canvas.width;

    ctx.strokeStyle = $("#selected-colour").val();

    var size = 550;

    ctx.moveTo((size/2),0);
    ctx.lineTo((size/2),size);
    ctx.stroke();

    ctx.moveTo(0,(size/2));
    ctx.lineTo(size,(size/2));
    ctx.stroke();

    ctx.font = "10px Arial";

    ctx.fillText("0",0,(size/2));
    ctx.fillText("1",(size/2),size);
    ctx.fillText("1",size-5,(size/2));
    ctx.fillText("0",(size/2)-5,8);

    //redraw selected apartment to spot select
    redraw_apartment_spot(x_value, y_value, 'blue');


    ctx.fillStyle = 'yellow';

    var selected_x = $("#selected_x").val();
    var selected_y = $("#selected_y").val();

    redraw_apartment_spot(selected_x, selected_y, 'yellow');

}

function add_event_listener(x_value, y_value)
{
    //console.log('adding event listener');
    //redraw entire canvas stuff to remove this floor's apartments.

    $("#x_coordinate").val(x_value);
    $("#y_coordinate").val(y_value);
    var canvas = document.getElementById("cross-hatch");

    canvas.addEventListener('mousemove', mouse_move);
}

function click_to_close_hotspot(apartment_id)
{
    //location.reload();

    $("#show_current_hotspots_"+apartment_id).hide();
    $("#hotspot_deselector_"+apartment_id).hide();

    if (parseFloat($("#apartment_set_x_coordinate_"+apartment_id).val()) > 0)
    {
        $("#hotspot_selector_"+apartment_id).show().css("color", 'yellow');
    }
    else
    {
        $("#hotspot_selector_"+apartment_id).show().css("color", 'blue');
    }

    $("#cross-hatch").unbind("click").css('cursor', 'default');
    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

    $(".save-button").show();

}





var canvas_click = function(e) {

  var current_apartment = $("#current_apartment_setting").val();

  if (current_apartment)
  {
      ////console.log('cross hatch click - current apartment');
      $("#save-button-"+current_apartment).show();
      var offset = $(this).offset();
      var relativeX = (e.pageX - offset.left);
      var relativeY = (e.pageY - offset.top);


      var x = parseFloat(relativeX) / 550.0;
      var y = parseFloat(relativeY) / 550.0;

      x = (x).toFixed(4);
      y = (y).toFixed(4);

      var canvas = document.getElementById("cross-hatch");
      var ctx = canvas.getContext("2d");

      show_selected(canvas, x, y);
      show_current_values(canvas);

      $("#new_apartment_set_x_coordinate_"+current_apartment).val(x);
      $("#new_apartment_set_y_coordinate_"+current_apartment).val(y);
  }
  else
  {
      redraw_main_canvas();
      redraw_apartment_lists();
  }

};

function click_to_set_hotspot(apartment_id)
{
    redraw_main_canvas();
    $(".save-button").hide();
    $("#cross-hatch").bind("click", canvas_click).bind("mousemove", mouse_move);

    $("#current_apartment_setting").val(apartment_id);
    $(".hotspot_deselector").hide();

    $(".hotspot_selector").each(function() {
       $(this).show();
       var this_color = $(this).css("color");

       if (this_color == 'rgb(0, 0, 255)')
       {
            $(this).css("color", 'blue');
       }
        else
       {
           $(this).css("color", 'yellow');
       }

    });

    $(".show_current_hotspots").hide();

    $(".newly-selected").each(function() {
       $(this).val('');
    });

    $("#hotspot_deselector_"+apartment_id).show().css("color", 'green');
    $("#hotspot_selector_"+apartment_id).hide();
    $("#show_current_hotspots_"+apartment_id).show();

    if ($("#show_current_hotspots_"+apartment_id).is(":visible"))
    {
        $("#hotspot_selector_"+apartment_id).css("color", 'green');

        var x_value = $("#apartment_set_x_coordinate_"+apartment_id).val();
        var y_value = $("#apartment_set_y_coordinate_"+apartment_id).val();

        add_event_listener(x_value, y_value);

        $().toastmessage('showToast', {
            text: 'Apartment selected. Please click on the floor plan to select new co-ordinates',
            sticky: false,
            position : 'top-right',
            type     : 'notice',
            closeText: ''}
        );

        $('#cross-hatch').css('cursor', 'crosshair');

        //redraw selected apartment to spot select
        redraw_apartment_spot(x_value, y_value, 'blue');

    }
    else
    {
        $("#hotspot_selector_"+apartment_id).css("color", '#e3001b');
        $('#cross-hatch').css('cursor', 'default');
    }
}

$("#cross-hatch").click(function(e) {

  ////console.log('cross hatch click');

  var current_apartment = $("#current_apartment_setting").val();

  if (current_apartment)
  {
      ////console.log('cross hatch click - current apartment');
      $("#save-button-"+current_apartment).show();
      var offset = $(this).offset();
      var relativeX = (e.pageX - offset.left);
      var relativeY = (e.pageY - offset.top);


      var x = parseFloat(relativeX) / 550.0;
      var y = parseFloat(relativeY) / 550.0;

      x = (x).toFixed(4);
      y = (y).toFixed(4);

      var canvas = document.getElementById("cross-hatch");
      var ctx = canvas.getContext("2d");

      show_selected(canvas, x, y);
      show_current_values(canvas);

      $("#new_apartment_set_x_coordinate_"+current_apartment).val(x);
      $("#new_apartment_set_y_coordinate_"+current_apartment).val(y);
  }
  else
  {
      redraw_main_canvas();
      redraw_apartment_lists();
  }

});


function redraw_apartment_spot(x_value, y_value, color)
 {
     if (!(x_value == '0' && y_value == '0') && !(x_value == 0 && y_value == 0))
     {

         var canvas = document.getElementById("cross-hatch");
         var ctx = canvas.getContext('2d');

         ctx.fillStyle = color;

         var current_value_x = parseFloat(x_value * 550.0);
         var current_value_y = parseFloat(y_value * 550.0);

          var centerX = current_value_x;
          var centerY = current_value_y;
          var radius = 15;

          ctx.globalAlpha = 0.5;
          ctx.beginPath();
          ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
          ctx.fillStyle = color;
          ctx.fill();
          ctx.lineWidth = 0.1;
          ctx.strokeStyle = color;
          ctx.stroke();
     }

 }

 function writeMessage(canvas, message) {
    $("#floorplan-message").html(message);
 }

 function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
 }

 function show_selected(canvas, x, y)
 {
     ////console.log('show_selected');
     var context = canvas.getContext('2d');
     var selected_colour = $("#selected-colour").val();
     context.fillStyle = selected_colour;

     var u_x = (x * 550.0).toFixed(4);
     var u_y = (y * 550.0).toFixed(4);

     context.fillStyle = 'yellow';
     redraw_apartment_spot(u_x, u_y, 'yellow');

     $("#selected_x").val(x);
     $("#selected_y").val(y);

     redraw_apartment_spot(x, y, 'yellow');

     $("#selected_current_value_x").val(x);
     $("#selected_current_value_y").val(y);
 }

function show_current_values(apartments)
{
     var canvas = document.getElementById("cross-hatch");
     var ctx = canvas.getContext('2d');

    var floor_id = $("#current_floor_id").val();

     $(apartments).each(function(index, object) {
         if (object.floor_id == floor_id)
         {
            redraw_apartment_spot(object.x_coordinate, object.y_coordinate, 'yellow');
         }

     });
}





$(function() {

    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");

    ctx.strokeStyle = $("#selected-colour").val();

    var size = 550;

    ctx.moveTo((size/2),0);
    ctx.lineTo((size/2),size);
    ctx.stroke();

    ctx.moveTo(0,(size/2));
    ctx.lineTo(size,(size/2));
    ctx.stroke();

    ctx.font = "10px Arial";

    ctx.fillText("0",0,(size/2));
    ctx.fillText("1",(size/2),size);
    ctx.fillText("1",size-5,(size/2));
    ctx.fillText("0",(size/2)-5,8);

//    $("#cross-hatch").click(function(e) {
//      var apartment_list = $.parseJSON($("#apartment_list").val());
//      show_current_values(apartment_list);
//    });

});
