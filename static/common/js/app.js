
$('.use-datepicker').each(function(){
    $(this).pickadate({
        format: 'yyyy-mm-dd',
        selectMonths: true,
        selectYears: true
    });
});

$('.fixed-alert').each(function(){
    var offset_from_top = $(this).offset().top + $(this).height();
    var clone = $(this).clone();
    var clone_is_hidden = false;

    clone.addClass();
    clone.hide();
    clone_is_hidden = true;

    $('.fixed-alerts').append(clone);

    function handle_scroll() {
        var scroll_top = $(window).scrollTop();

        if (scroll_top > offset_from_top && clone_is_hidden) {
            clone.show();
            clone_is_hidden = false;
        } else if (scroll_top <= offset_from_top && !clone_is_hidden) {
            clone.hide();
            clone_is_hidden = true;
        }
    }

    $(window).scroll(handle_scroll);
    $(document).ready(handle_scroll);
});
