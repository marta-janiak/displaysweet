$(document).ready(function() {
    $("#show-columns-dropdown").click(function() {
       if ($(".dropdown-menu").is(":visible"))
        {
            $(".dropdown-menu").hide();
        }
        else
        {
            $(".dropdown-menu").show();
        }
    });

    $(".dropdown-menu a").click(function() {
       var selected_cb = $(this).attr('data-value');

       $(this).children("i").toggleClass('on-display');
       $("#checkbox-"+selected_cb).toggle('click');

       var filter_val = $("#id_"+selected_cb).val();
       var column_value = '';

       $('#page-data-table tr th').each(function() {
           column_value = $(this).attr('data-column');

           if (selected_cb == column_value)
           {
               $(this).toggle();
           }
       });

       $('#page-data-table tr td').each(function() {
           column_value = $(this).attr('data-column');

           if (selected_cb == column_value)
           {
               $(this).toggle();
           }
       });

    });

} );

function confirm_delete(id, model_name, row_id)
{
    if (confirm("Are you sure you wish to delete this row ["+row_id+"]?"))
    {
        window.location.href="/projects/"+id+"/update/"+model_name+"/row/delete/"+row_id+"/instant/";
    }
}


$(".sorting").click(function() {

    var direction = 'asc';
    var classes = $(this).attr('class');

    if (classes.indexOf("sorting_asc") >= 0)
    {
        direction = 'descending';
        $("#order_page_by_direction").val('desc');
        $(this).addClass('sorting_desc').removeClass('sorting');
    }
    else
    {
        $(".sorting_desc").removeClass('sorting_desc').addClass('sorting');
    }

    if (classes.indexOf("sorting_desc") >= 0)
    {
        $(this).addClass('sorting_asc').removeClass('sorting_desc');
        direction = 'asc';
        $("#order_page_by_direction").val('asc');
    }
    else
    {
        $(".sorting_asc").removeClass('sorting_asc').addClass('sorting');

    }

    $(this).addClass('sorting_asc').removeClass('sorting');

    var check_class = $(this).attr('class');
    if (check_class == 'sorting_asc')
    {
        direction = 'descending';
        $("#order_page_by_direction").val('desc');
        $(this).removeClass('sorting_asc').addClass('sorting_desc');
    }

    var table_column_field = $(this).attr('data-column');
    $("#order_page_by").val(table_column_field);

    var table_column = $(this).attr('data-column-int');
    sort_table(table_column, direction);

});


function sort_table(table_column, direction)
{
    var $table=$('#page-data-table');
    var rows = $table.find('tbody tr').get();

    $.each(rows, function(index, row) {
        var td_value = $(row).find("td[data-column-int="+table_column+"]").text().replace(/ +(?= )/g,'');
        $(this).attr('sorting-by', td_value);
    });

    rows.sort(function(a, b) {

        var keyA = $(a).attr('sorting-by');
        var keyB = $(b).attr('sorting-by');

        var keyAint = parseFloat(keyA);
        var keyBint = parseFloat(keyB);

        if ($.isNumeric(keyAint) && $.isNumeric(keyBint))
        {
            keyA = keyAint;
            keyB = keyBint;
        }

        if (direction == 'asc')
        {
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
        }
        else
        {
            if (keyA < keyB) return 1;
            if (keyA > keyB) return -1;
        }

        return 0;
    });

    $.each(rows, function(index, row) {
        $table.children('tbody').append(row);
    });
}

function set_sorting(sorting_by, selected_direction)
{
    var direction = selected_direction;
    var table_column = $("#page-data-table th[data-column='"+sorting_by+"']");

    var table_column_int = $(table_column).attr("data-column-int");

    var check_class = $(table_column).attr('class');
    $(table_column).removeClass('sorting').addClass('sorting_'+selected_direction);

    sort_table(table_column_int, selected_direction);
}

function get_page_information(direction)
{
    $('.form-filter-chunk').hide();
    $("#data_page_direction").val(direction);

    $.post( '/get_project_model_filter/{{ project.pk }}/{{ model|model_name_filter }}/',
            $( "#model-filter-form" ).serialize()
    ).success(function( data )
            {
               $("#paginated-data").html(data.rendered_html);

               var column_value = '';
               $(".column-showing-checkbox").each(function() {
                   if (!$(this).hasClass("on-display"))
                   {
                       var selected_cb = $(this).attr('id').replace("checkbox-", "");

                       $('#page-data-table tr th').each(function() {
                           column_value = $(this).attr('data-column');

                           if (selected_cb == column_value)
                           {
                               $(this).hide();
                           }
                       });

                       $('#page-data-table tr td').each(function() {
                           column_value = $(this).attr('data-column');

                           if (selected_cb == column_value)
                           {
                               $(this).hide();
                           }
                       });
                   }
               });

                $("#data_page_position").val(data.position);

                $("#data_page_number").val(data.page);
                $(".page-location-information").text("Page "+ data.page +" of "+ data.pages);

                set_sorting(data.sorting_field, data.sorting_direction);

                if (parseInt(data.page) && parseInt(data.page) > 1)
                {
                    $("#paginate-back-button").removeAttr('disabled');
                }
                else
                {
                    $("#paginate-back-button").attr('disabled', 'disabled');
                }

                if (parseInt(data.page) && parseInt(data.page) == data.pages)
                {
                    $("#paginate-next-button").attr('disabled', 'disabled');
                }
                else
                {
                    $("#paginate-next-button").removeAttr('disabled');
                }

            }).error(function( data )
            {
               alert("Something went wrong with getting the next set of data. Please contact an administrator.");
            });
}