var search_array = [];

$(function() {

    $(".all-props-property-selector").val('all');
    $(".editable-props-property-selector").val('all');
    $(".custom-selector").val('all');

    var nametable = $('table.scrollable-header');

    nametable.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.wrapper');
        }
    });

    $("#edit-all-props-by-select").click(function() {
        $("#edit-all-props-by-select").removeClass('fa-square').addClass('fa-check');
        $("#edit-all-props-by-text").removeClass('fa-check').addClass('fa-square');
    });

    $("#edit-all-props-by-text").click(function() {
        $("#edit-all-props-by-text").removeClass('fa-square').addClass('fa-check');
        $("#edit-all-props-by-select").removeClass('fa-check').addClass('fa-square');
    });

    var first_building = $(".td-with-key[data-name='building_name']").first().text().trim().replace(/\n/, '');
    $(".assigned-floor-property-select-building_name").val(first_building);

    var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
    $(".assigned-floor-property-select-floor").val(first_floor);
    var project_id = $("#current_project_id").val();

    $(".allocation-search").change(function() {
        var property_selected = $("#id_properties").val();
        var property_type_selected = $("#id_property_type").val();
        var level_selected = $("#id_level").val();
        var building_selected = $("#id_building").val();
        var user_selected = $("#id_users").val();

        $.ajax({
            url: '/get_project_allocated_search_results/'+project_id+'/',
            type: 'GET',
            data: { 'property_selected': property_selected,
                    'property_type_selected': property_type_selected,
                    'level_selected': level_selected,
                    'building_selected': building_selected,
                    'user_selected': user_selected },
            success: function (data) {
                $("#allocation-search-results").html(data.rendered);
            }
        });
    });

    show_reserved();
});


function select_all_properties_to_assign()
{
    if ($("#select-all-to-assign").hasClass('fa-square'))
    {
        $('.checkbox-property-for-listing').prop('checked', true);
        $(".tr-show-all-properties.property-selections:visible").addClass('selected');
        $("#select-all-to-assign").removeClass('fa-square').addClass('fa-check');
    }
    else
    {
        $("#select-all-to-assign").addClass('fa-square').removeClass('fa-check');
        $('.checkbox-property-for-listing').prop('checked', false);
        $(".tr-show-all-properties:visible").removeClass('selected');
    }
}

function remove_all_properties_to_assign()
{
    if ($("#select-all-to-assign").hasClass('fa-square'))
    {
        $('.checkbox-property-for-listing').prop('checked', true);
        $(".tr-show-all-properties.property-selections:visible").addClass('selected');
        $("#select-all-to-assign").removeClass('fa-square').addClass('fa-check');
    }
    else
    {
        $("#select-all-to-assign").addClass('fa-square').removeClass('fa-check');
        $('.checkbox-property-for-listing').prop('checked', false);
        $(".tr-show-all-properties:visible").removeClass('selected');
    }
}

function save_reservation_form()
{
    var project_id = $("#current_project_id").val();
    var formdata = new FormData($('#reservation-modal-form')[0]);

    formdata = $('#reservation-modal-form').serialize();
    var user_formdata = $('#reservation-agent-form').serialize();

    var total_success = true;
    var id_agent = $("#id_agent").val();

    if ($("#agent-form").html() == "")
    {

    }
    else
    {
        $.ajax({
            url: '/get_agent_for_reservation/'+project_id+'/',
            type: 'GET',
            data: {'formdata': user_formdata, 'direction':'save', 'agent_user_id': id_agent},
            success: function (data) {

                console.log('error in agent form', data.success);

                if (data.success == false)
                {
                    $().toastmessage('showToast', {text : "There was an error with your form.", position : 'top-left', stayTime: 2000, type : 'error'});
                    total_success = false;
                    $("#agent-form").html('').html(data.rendered_form);
                    $("#reservation-modal").modal('show');
                }


        }});
    }

    $.ajax({
        url: '/get_property_reservation/'+project_id+'/',
        type: 'GET',
        data: {'formdata': formdata, 'direction':'save', 'user_id': id_agent},
        success: function (data) {

            if (data.success == false)
            {
                $().toastmessage('showToast', {text : "There was an error with your agent details.", position : 'top-left', stayTime: 2000, type : 'error'});
                total_success = false;
                $("#show-reservation-modal-details").html(data.rendered_form);
                $("#reservation-modal").modal('show');
            }

        }});



    if (total_success)
    {
        $().toastmessage('showToast', {text : "Reservation saved successfully", position : 'top-left', stayTime: 2000, type : 'success'});

        var property_id = $("#id_property_id").val();
        if (property_id)
        {
            $(".property-edit-reserved-"+property_id).removeClass('fa-square').addClass('fa-check');
        }

        $("#reservation-modal").modal('hide');


    }
}

function filter_images()
{
    var selected_key_list = [];
    var selected_tags = $(".editable-props-property-selector");
    $(selected_tags).each(function() {
        var key_selected = $(this).attr('class').replace("property-selector assigned-floor-property-select-", "");
        if (this.value == "all")
        {}
        else
        {
            selected_key_list.push([key_selected, this.value]);
        }
    });

    $(".property-edit-selections").each(function() {

        var show_image = true;
        var property_row = $(this);

        $.each(selected_key_list, function (k, v) {

            var key = v[0];
            var value = v[1].trim().replace(/\n/, '');

            var check_value = property_row.find('.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');
            if (value.toString() == check_value.toString())
            {  }
            else
            {
                show_image = false;
            }
        });

        if (!show_image)
        {
            property_row.hide();
        }
        else
        {
            property_row.show();
        }
    });

    $(".property-selections").each(function() {

        var show_image = true;
        var property_row = $(this);

        $.each(selected_key_list, function (k, v) {

            var key = v[0];
            var value = v[1].trim().replace(/\n/, '');

            var check_value = property_row.find('td.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');
            if (value.toString() == check_value.toString())
            {

            }

            else
            {
                show_image = false;
            }

        });

        if (!show_image)
        {
            property_row.hide();
        }
        else
        {
            property_row.show();
        }
    });


    var all_selected_key_list = [];
    var all_selected_tags = $(".all-props-property-selector");

    $(all_selected_tags).each(function() {
        var key_selected = $(this).attr('class').replace("property-selector assigned-floor-property-select-", "");
        if (this.value == "all")
        {}
        else
        {
            all_selected_key_list.push([key_selected, this.value]);
        }
    });

    $(".tr-show-all-properties").each(function(index, object_row) {

        var show_image = true;
        var property_row = $(this);

        $.each(all_selected_key_list, function (index, v) {

            var key = v[0].toString();
            key = key.replace('all-props-', '');

            var value = v[1].trim().replace(/\n/, '');
            value = value.trim();

            var check_value = property_row.find('td.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');
            check_value = check_value.trim().replace(/\r/, '');

            if (value.toString() == check_value.toString())
            {
            }

            else
            {
                show_image = false;
            }

        });

        if (!show_image)
        {
           property_row.hide();
        }
        else
        {
            property_row.show();
        }
    });


}

function update_assigned_floor_list(selected_type, value)
{

    if (selected_type == "floor")
    {
        get_new_floor_data(value, 'properties');
    }

    if (selected_type == "building_name")
    {
        get_new_building_data(value);
    }

    filter_images();
}

function update_floor_list_custom(selected_type, value)
{

    if (value == "all")
    {
        $(".custom-selector").val('all');
        filter_images();
    }

    else
    {
        $(".tr-show-all-properties").each(function() {

            var show_image = true;
            var property_row = $(this);

            var key = 'retail_price';
            var value = parseFloat(value);

            var check_value = property_row.find('td.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');

            console.log('-----', value, check_value, 'selected_type', selected_type);

            if (selected_type == "price_min") {
                if (value >= parseFloat(check_value)) {

                }
                else {
                    show_image = false;
                }
            }

            if (selected_type == "price_max") {
                if (value <= parseFloat(check_value)) {

                }
                else {
                    show_image = false;
                }
            }

            if (!show_image)
            {
                property_row.hide();
            }
            else
            {
                property_row.show();
            }
        });

    }



}

function check_selected_property_for_listing(property_id, set_as_exclusive)
{
    if (set_as_exclusive == "False")
    {
       if ($(".property-group-selection-"+property_id).hasClass('selected'))
        {
            $(".property-group-selection-"+property_id).removeClass('selected');
            $("#checkbox-property-for-listing-"+property_id).prop("checked", false);
        }
        else
        {
            $(".property-group-selection-"+property_id).addClass('selected');
            $("#checkbox-property-for-listing-"+property_id).prop("checked", true);
        }
    }

}


function check_selected_property_remove_listing(property_id)
{

    if ($("#remove_property_for_listing_"+property_id).hasClass('selected'))
    {
        $("#remove_property_for_listing_"+property_id).removeClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", false);
    }
    else
    {
        $("#remove_property_for_listing_"+property_id).addClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", true);
    }
}

function add_property_to_allocation_group()
{
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var allocation_template_id = $("#allocation_template_id").val();
    var checked_boxes = $('.checkbox-property-for-listing:checkbox:checked');

    $(checked_boxes).each(function() {
        var added_id = $(this).attr('id');
        added_id = added_id.replace("checkbox-property-for-listing-", "");

        $.ajax({
            url: '/add_property_to_allocation_group/' + project_id + '/',
            type: 'GET',
            data: {'group_id': group_id, 'added_property_id': added_id, 'allocation_template_id': allocation_template_id },
            success: function (data) {
                $().toastmessage('showToast', {text : "Property Saved to Allocation Group Saved Successfully", position : 'top-left', stayTime: 2000, type : 'success'});
            }
        });

        checked_boxes.remove(added_id);
    });

    $(".property-selections").removeClass('selected');
    $(".checkbox_property_remove_listing").prop('checked', false);

    if (checked_boxes == "")
    {
        show_allocated_properties(group_id, '');
    }
}

function remove_property_from_allocation_group()
{
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var checked_boxes = $('.checkbox_property_remove_listing:checkbox:checked');
    $(checked_boxes).each(function() {

        var removed_property_id = $(this).val();
        $.ajax({
            url: '/remove_property_from_allocation_group/' + project_id + '/',
            type: 'GET',
            data: {'group_id': group_id, 'removed_property_id': removed_property_id },
            success: function (data) {
                $("#remove_property_for_listing_"+removed_property_id).remove();
                $("#selected_property_for_listing_"+removed_property_id).removeClass('selected');

                $().toastmessage('showToast', {text : "Allocation Group Saved Successfully", position : 'top-left', stayTime: 2000, type : 'success'});
                checked_boxes.remove(removed_property_id);
            }
        });
    });

    if (checked_boxes == "")
    {
        var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
        get_new_floor_data(first_floor, 'properties');

    }

}


function reset_search_values()
{
    $(".allocation-search").val('');
    $("#allocation-search-results").html('');
}

function show_allocated_users(group_id, allocated_group_name)
{
    var group_name = $("#group-allocation-name-field-"+group_id).val();
    
    $("#assigned-users").show();
    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_project_allocated_users/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'group_name': group_name},
        success: function (data) {
            $("#show-allocated-users").html(data.rendered);
        }
    });
}

function save_users_to_group()
{
    
    var group_id = $("#selected-allocation-group").val();
    var group_name = $("#selected-allocation-group-name").val();
    var project_id = $("#current_project_id").val();

    $(".checkbox-select-user-to-group:checked").each(function() {

        var user_id = $(this).attr('id');
        user_id = user_id.replace("checkbox-select-user-to-group-", "");

        $.ajax({
            url: '/save_user_to_allocation_group/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id, 'user_id': user_id},
            success: function (data) {
                $().toastmessage('showToast', {text : "Allocation group saved successfully", position : 'top-left',  stayTime: 5000, type : 'success'});
                $("#selected-group-user-allocation").val('');
                show_allocated_users(group_id, group_name);
            }
        });
    });

    $(".select-user-to-group").removeClass('selected');
}

function remove_users_from_group()
{
    
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var group_name = $("#selected-allocation-group-name").val();

    $(".checkbox-remove-user-to-group:checked").each(function() {

        var user_id = $(this).attr('id');
        user_id = user_id.replace("checkbox-remove-user-to-group-", "");

        $.ajax({
            url: '/remove_user_from_allocation_group/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id, 'user_id': user_id},
            success: function (data) {
                $().toastmessage('showToast', {text : "User removed successfully",position : 'top-left',  stayTime: 5000, type : 'success'});
                $("#selected-group-user-allocation").val('');
                show_allocated_users(group_id, group_name);
            }
        });
    });

    $(".remove-user-to-group").removeClass('selected');
}

function save_property_to_group()
{
    
    var group_id = $("#selected-allocation-group").val();
    var properties = $("#selected-group-property-allocation").val();
    var group_name = $("#selected-allocation-group-name").val();
    var project_id = $("#current_project_id").val();

    $(properties).each(function(index, value) {
        var property_id = value;
        $.ajax({
            url: '/save_property_to_allocation_group/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id, 'property_id': property_id},
            success: function (data) {
            }
        });
    });

    $().toastmessage('showToast', {text : "Allocation group saved successfully", position : 'top-left', stayTime: 5000, type : 'success'});
    $("#selected-group-property-allocation").val('');
    show_allocated_properties(group_id, group_name);
}


function show_allocated_properties(group_id, allocated_group_name)
{
    var group_name = $("#group-allocation-name-field-"+group_id).val();

    
    $("#assigned-properties").show();
    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);

    var allocation_template_id = $("#allocation_template_id").val();

    $(".selecting-properties").removeClass('selected');
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_project_allocated_properties/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'group_name': group_name, 'allocation_template_id': allocation_template_id},
        success: function (data) {
            $("#show-allocated-properties").html('').html(data.rendered);
            $("#show-allocated-properties").css({'vertical-align':'top'});
            $().toastmessage('showToast', {text : "Properties for this group are now displayed", position : 'top-left',  stayTime: 1000, type : 'success'});
        }
    });

}

function get_new_floor_data(floor_name, tab_selected)
{
    var allocation_template_id = $("#allocation_template_id").val();
    $(".selecting-properties").removeClass('selected');
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_project_all_properties/'+project_id+'/',
        type: 'GET',
        data: {'floor_name': floor_name, 'group_id': group_id, 'allocation_template_id': allocation_template_id},
        success: function (data) {
            $(".show-filtered-properties").html('').html(data.all_prop_rendered);
            $(".show-edit-filtered-properties").html('').html(data.all_prop_rendered_editable);
            $("#section-reservations").html('').html(data.reservation_properties_rendered);

            if (tab_selected == 'reservations')
            {
                $("#reservation-list-tab").tab('show');
            }

            $().toastmessage('showToast', {text : "Properties are ready to be edited.", position : 'top-left',  stayTime: 1000, type : 'success'});
        }
    });
}

function get_new_building_data(building_name)
{
    var allocation_template_id = $("#allocation_template_id").val();
    $(".selecting-properties").removeClass('selected');

    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_project_all_properties/'+project_id+'/',
        type: 'GET',
        data: {'building_name': building_name, 'group_id': group_id, 'allocation_template_id': allocation_template_id},
        success: function (data) {
            $(".show-filtered-properties").html('').html(data.all_prop_rendered);
            $(".show-edit-filtered-properties").html('').html(data.all_prop_rendered_editable);
            $("#section-reservations").html('').html(data.reservation_properties_rendered);
        }
    });
}


function show_all_properties(group_id, allocated_group_name)
{
    var group_name = $("#group-allocation-name-field-"+group_id).val();
    
    $("#assigned-properties").show();
    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);
    show_allocated_properties(group_id, group_name);

    var first_building = $(".td-with-key[data-name='building_name']").first().text().trim().replace(/\n/, '');
    $(".assigned-floor-property-select-building_name").val(first_building);


    var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
    $(".assigned-floor-property-select-floor").val(first_floor);

}


function set_select_user(user_id)
{
    if ($("#select-user-to-group-"+user_id).hasClass('selected'))
    {
        $("#select-user-to-group-"+user_id).removeClass('selected');
        $("#checkbox-select-user-to-group-"+user_id).prop("checked", false);
    }
    else
    {
        $("#select-user-to-group-"+user_id).addClass('selected');
        $("#checkbox-select-user-to-group-"+user_id).prop("checked", true);
    }
}

function set_remove_user(user_id)
{

    if ($("#remove-user-to-group-"+user_id).hasClass('remove-selected'))
    {
        $("#remove-user-to-group-"+user_id).removeClass('remove-selected');
        $("#checkbox-remove-user-to-group-"+user_id).prop("checked", false);
    }
    else
    {
        $("#remove-users").show();
        $("#remove-user-to-group-"+user_id).addClass('remove-selected');
        $("#checkbox-remove-user-to-group-"+user_id).prop("checked", true);
    }
}

function get_csv_file(href)
{
    $("#loading-gif").show();
    $("#get_csv_data").hide();
    window.location.href(href);
}

function show_reserved()
{
    $(".property-edit-selections").each(function() {
        var property_id = $(this).attr('id');
        property_id = property_id.replace('property-edit-selection-', '');
        if ($("#editable-"+property_id+"-status").text().trim().replace(/ /g, '') == 'Reserved')
        {
            $(".property-edit-reserved-"+property_id).prop('checked', true);
            $(".property-edit-reserved-icon-"+property_id).removeClass('fa-square').addClass('fa-check');
        }
    });

}

function set_group_properties_to_exclusive()
{
    if (confirm("Are you sure you wish to set thse to exclusive?"))
    {
        var group_id = $("#selected-allocation-group").val();
        var project_id = $("#current_project_id").val();

        $.ajax({
            url: '/update_property_to_exclusive/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id},
            success: function (data) {
                $().toastmessage('showToast', {text : "Properties have been set to Exclusive",
                    position : 'top-left', stayTime: 2000, type : 'success'});

                var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
                get_new_floor_data(first_floor, 'properties');
            }
        });

    }
}



function edit_allocation_group_details(group_id)
{
    
    var current_group_name = $("#group-allocation-name-field-"+group_id).val();
    $("#edit-allocation-group-name").val(current_group_name);
    $("#edit-allocation-group").show();
    $("#selected-allocation-group").val(group_id);
}


function save_allocation_group_name()
{
    var group_id = $("#selected-allocation-group").val();
    var current_group_name = $("#group-allocation-name-field-"+group_id).val();
    var edited_group_name = $("#edit-allocation-group-name").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/update_edited_allocation_group_name/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'edited_group_name': edited_group_name},
        success: function (data) {
            $().toastmessage('showToast', {text : "Allocation group name has been updated.",
                position : 'top-left', stayTime: 2000, type : 'success'});

            $("#group-allocation-name-field-"+group_id).val(edited_group_name);
            $(".group-allocation-name-field-"+group_id).text(edited_group_name);
        }
    });
}