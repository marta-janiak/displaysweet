from django.db import models
from core.permissions import ClientPermissionsMixin
from core.models import Project


class Client(ClientPermissionsMixin, models.Model):
    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    name = models.CharField(max_length=50, null=True, blank=True, help_text="Enter a client name.")

    projects = models.ManyToManyField(Project, blank=True, related_name='client_projects')
    display_suites = models.ManyToManyField('DisplaySuite', blank=True, related_name='client_suites')

    def __unicode__(self):
        return '%s' % self.name


class DisplaySuite(ClientPermissionsMixin, models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    name = models.CharField(max_length=50, null=True, blank=True, help_text="Enter a Display Suite name.")

    def __unicode__(self):
        return '%s' % self.name