# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='display_suites',
            field=models.ManyToManyField(related_name='client_suites', to='clients.DisplaySuite', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='projects',
            field=models.ManyToManyField(related_name='client_projects', to='core.Project', blank=True),
        ),
    ]
