# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.permissions


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_auto_20151210_1417'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('name', models.CharField(help_text=b'Enter a client name.', max_length=50, null=True, blank=True)),
            ],
            bases=(core.permissions.ClientPermissionsMixin, models.Model),
        ),
        migrations.CreateModel(
            name='DisplaySuite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('name', models.CharField(help_text=b'Enter a Display Suite name.', max_length=50, null=True, blank=True)),
            ],
            bases=(core.permissions.ClientPermissionsMixin, models.Model),
        ),
        migrations.AddField(
            model_name='client',
            name='display_suites',
            field=models.ManyToManyField(related_name='client_suites', null=True, to='clients.DisplaySuite', blank=True),
        ),
        migrations.AddField(
            model_name='client',
            name='projects',
            field=models.ManyToManyField(related_name='client_projects', null=True, to='core.Project', blank=True),
        ),
    ]
