from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^home/$', 'clients.views.clients_list', name='clients-list'),
)
