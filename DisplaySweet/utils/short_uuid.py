"""
Concise UUID generation.
From the shortuuid project at
https://github.com/stochastic-technologies/shortuuid/

shortuuid copyright information:

Copyright (c) 2011, Stochastic Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

Neither the name of Stochastic Technologies nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import uuid as _uu

# Define our alphabet.
_ALPHABET = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

def uuid(url=None):
    """
    Generate and return a UUID.

    If the url parameter is provided, set the namespace to the provided
    URL and generate a UUID.
    """
    # If no URL is given, generate a random UUID.
    if url is None:
        unique_id = _uu.uuid4().int
    else:
        unique_id = _uu.uuid3(_uu.NAMESPACE_URL, url).int

    alphabet_length = len(_ALPHABET)
    output = []
    while unique_id > 0:
        digit = unique_id % alphabet_length
        output.append(_ALPHABET[digit])
        unique_id = int(unique_id / alphabet_length)
    return "".join(output)

def get_alphabet():
    """Return the current alphabet used for new UUIDs."""
    return _ALPHABET

def set_alphabet(alphabet):
    """Set the alphabet to be used for new UUIDs."""
    global _ALPHABET

    try:
       set
    except NameError:
       from sets import Set as set

    # Turn the alphabet into a set and sort it to prevent duplicates
    # and ensure reproducibility.
    new_alphabet = "".join(sorted(set(alphabet)))
    if len(new_alphabet) > 1:
        _ALPHABET = new_alphabet
    else:
        raise ValueError("Alphabet with more than one unique symbols required.")