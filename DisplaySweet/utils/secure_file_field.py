import os, datetime

from django.utils.encoding import smart_str, force_text
from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from utils.short_uuid import uuid


class SecureFileField(FileField):
    """
    Same as FileField, but you can specify:
        * content_types - list containing allowed content_types. Example: ['application/pdf', 'image/jpeg']
        * extension_whitelist - case-insensitive list of file extensions allowed ['jpg','jpeg','exe']
        * max_upload_size - a number indicating the maximum file size allowed for upload.
            2.5MB - 2621440
            5MB - 5242880
            10MB - 10485760
            20MB - 20971520
            50MB - 52428800
            100MB 104857600
            250MB - 214958080
            500MB - 429916160
    """
    def __init__(self, *args, **kwargs):
        try:
            self.content_types = kwargs.pop("content_types")
        except KeyError:
            self.content_types = settings.VALID_MIME_TYPES

        try:
            extension_whitelist = kwargs.pop("extension_whitelist")
        except KeyError:
            extension_whitelist = settings.FILE_EXTENSION_WHITELIST
        self.extension_whitelist = [e.upper() for e in extension_whitelist]

        #extension_whitelist

        try:
            self.max_upload_size = kwargs.pop("max_upload_size")
        except KeyError:
            self.max_upload_size = settings.DEFAULT_MAX_UPLOAD_SIZE

        super(SecureFileField, self).__init__(*args, **kwargs)

    def get_directory_name(self):
        name = os.path.normpath(force_text(datetime.datetime.now().strftime(smart_str(self.upload_to)) + '/' + uuid()))
        return name

    def get_filename(self, filename):
        name_from_storage = self.storage.get_valid_name(os.path.basename(filename))

        # Truncate long file names
        if len(name_from_storage) > 40:
            fname, extension = os.path.splitext(name_from_storage)
            new_len = 40 - len(extension)
            if new_len < 1:
                return "default"
            fname = fname[:new_len]
            name_from_storage = fname + extension
        return os.path.normpath(name_from_storage)

    def clean(self, *args, **kwargs):
        data = super(SecureFileField, self).clean(*args, **kwargs)


        try:
            file = data.file

            content_type = file.content_type

            filename = data.name
            extension = os.path.splitext(filename)[1][1:]
            extension = extension.upper()

            if file._size > self.max_upload_size:
                raise forms.ValidationError(_('Please keep the file size under %s. The file you uploaded was %s') % (filesizeformat(self.max_upload_size), filesizeformat(file._size)))
# 			elif not content_type in self.content_types:
# 				raise forms.ValidationError(_("Sorry, this file type is not supported. ('%s')") % (content_type))
            elif not extension in self.extension_whitelist:
                raise forms.ValidationError(_("Sorry, this file type is not supported. ('%s')") % (extension if len(extension) > 0 else 'No extension'))

        except AttributeError:
            pass

        return data


# try:
#     from south.modelsinspector import add_introspection_rules
#     add_introspection_rules([], ["^erws\.secure_file_field\.SecureFileField"])
# except:
#     pass
