from collections import defaultdict
import re
import csv
from io import StringIO

from django.core.exceptions import ValidationError

from openpyxl import load_workbook
from django.db.models import Q

from utils.template_creator.xlxs_template_generator import BulkImportTemplate
from django.apps import apps
from utils.template_creator.xlxs_common import generate_structure_hash_from_workbook, FieldIDProcessingMixin
from utils import unicode_csv
from core.models import CSVFileImportFields, ProjectCSVTemplateImport, ProjectCSVTemplateFields, ProjectCSVTemplateFieldValues
from django.utils.encoding import smart_str


MAX_INPUT_ROWS = 100000


class InvalidImportException(Exception):
    pass


class ImportedWorkbook(object):
    xlsx_file = None
    bulk_import = None
    validation_errors = None
    template_object = None
    table_values = None
    table_headers = None
    template_name = None
    project = None
    number_of_applications = None

    csv_file = None
    csv_import_details = None
    primary_worksheet = None
    workbook = None
    worksheet = None

    def __init__(self, bulk_import, template_object, xlsx_file=None):
        print('in init!')

        self.bulk_import = bulk_import
        self.template_object = template_object

        self.project = template_object.project
        self.template_name = template_object.template_name

        self.expected_template = BulkImportTemplate(self.template_name, template_object, 1, project=self.project)

        if xlsx_file:
            self.xlsx_file = xlsx_file
        else:
            self.xlsx_file = bulk_import.uploaded_xlsx

        self.workbook = None
        self.worksheet = None

        file_extension = self.xlsx_file.name[-3:]
        print('file', file_extension, file_extension in ["csv", "txt"])

        if file_extension in ["csv", "txt"]:
            self.csv_file = self.xlsx_file

            self.validation_errors_for_display = None
            self.validation_errors = None

            if self.check_all_field_mappings():
                self.import_csv_values()

            print('at mapping end')
        else:

            self.primary_worksheet = self.get_primary_worksheet()
            self.structure_hash = self.generate_structure_hash()

            try:
                self.workbook = load_workbook(self.xlsx_file)
            except Exception:
                print ('template creator __init__ error on load workbook')
                raise InvalidImportException()

            if self.structure_hash != self.expected_template.structure_hash:
                print ('template creator Structured has error')
                raise InvalidImportException()

            self.validation_errors = self.generate_validation_errors()
            self.validation_errors_for_display = self.generate_validation_errors_for_display()

    def import_csv_values(self):
        print('import_csv_values')
        headers = self.get_csv_file_headers()

        if headers:
            headers_list_dict = []
            accepted_positions = []
            created_import = ProjectCSVTemplateImport.objects.create(template=self.template_object)

            i = 0
            for item in headers:
                item = self.clean_item_for_mapping(item)
                print('FIELD', item)

                item_field = ProjectCSVTemplateFields.objects.filter(column_field_mapping__iexact=item, template=self.template_object)

                if item_field:
                    item_field = item_field[0]
                    check_field = ProjectCSVTemplateFields.objects.filter(template=self.template_object,
                                                                          column_table_mapping=item_field.column_table_mapping,
                                                                          column_field_mapping=item_field.column_field_mapping,
                                                                          column_pretty_name=item_field.column_pretty_name)

                    if check_field:
                        headers_dict = {}
                        headers_dict['position'] = i
                        headers_dict['column'] = item
                        headers_dict['project_field'] = check_field[0]

                        headers_list_dict.append(headers_dict)
                        accepted_positions.append(i)

                else:
                    print ('NO ITEM FIELD')
                i += 1

            i = 0
            adding = 0
            csv_reader = csv.reader(self.csv_file)

            for index, row in enumerate(csv_reader):
                adding += 1
                i += 1
                if i > 1:
                    for cell_index, cell in enumerate(row, start=0):
                        for available_headers in headers_list_dict:
                            if available_headers['position'] == cell_index:
                                item_field = available_headers['project_field']
                                item_field_string = str(item_field).strip()

                                cell = str(cell)
                                cell = cell.replace("'", "").replace('"', '')

                                if str(item_field_string) in ["Floor", "floor"]:
                                    try:
                                        cell = int(float(cell))
                                    except:
                                        cell = smart_str(cell, encoding='utf-8', strings_only=False,
                                                         errors='strict')

                                try:
                                    ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                 field=item_field,
                                                                                 value=cell,
                                                                                 row_id=adding)
                                except Exception as e:
                                    # print ('import field error', e

                                    cell = re.sub('[^a-zA-Z0-9\n\.]', ' ', cell)
                                    ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                 field=item_field,
                                                                                 value=cell,
                                                                                 row_id=adding)

    def get_csv_file_headers(self):
        print ('new get_csv_file_headers')

        csv_file = open(self.csv_file.path, 'rU', encoding="utf8")

        try:
            csv_reader = csv.reader(csv_file.read())

            for index, row in enumerate(csv_reader):
                print('index row', row)
                if index == 0:
                    return row

        except Exception as e:
            print ('Cannot open csv file: ', e)
            
    def check_and_get_field_mapping(self, item):
        item = self.clean_item_for_mapping(item)

        existing = ProjectCSVTemplateFields.objects.filter(column_pretty_name__iexact=item,
                                                           template=self.template_object)

        if not existing:
            print ('not existing header', item)
            raise InvalidImportException()
        else:
            return existing[0]

    def clean_item_for_mapping(self, string_value):

        try:
            english_value = str(string_value).replace("  ", " ", 10).replace("/", "", 10)
            english_value = str(english_value).strip().replace("  ", " ", 10)
            string_value = str(english_value).replace(" ", "_", 10).lower()

            return string_value
        except:
            pass

    def check_all_field_mappings(self):
        print ('check_all_field_mappings')
        validation_errors = {}

        error_messages = []

        headers = self.get_csv_file_headers()

        total_header_count = len(headers)
        attempted_field_count = len(ProjectCSVTemplateFields.objects.filter(template=self.template_object))
        
        total_field_errors = 0

        print ('total_header_count', total_header_count)
        print('attempted_field_count', attempted_field_count)

        for item in headers:
            print('---item', item)
            try:
                if item:
                    item = self.clean_item_for_mapping(item)

                    existing = ProjectCSVTemplateFields.objects.filter(column_field_mapping__iexact=item, template=self.template_object)
                    if not existing:
                        error_message = u'%s / %s - %s' % (self.primary_worksheet.worksheet.title, 0, "The column %s does not exist in the selected template. \n" % item)
                        error_messages.append(error_message)
                        total_field_errors += 1
            except Exception as e:
                print(e)

        if total_field_errors > 0:
            self.validation_errors_for_display = self.translate_errors(error_messages)
        else:
            return True

    def get_primary_worksheet(self):
        
        return PrimaryWorksheet(
            self.workbook.worksheets[0],
            self.bulk_import,
            self.template_name,
            self.template_object,
            self.number_of_applications,
            project=self.project
        )

    def generate_structure_hash(self):
        """
        Generates a simple hash of the workbook structure so that we can
        easily compare against the expected template.
        """
        return generate_structure_hash_from_workbook(self.workbook)

    def generate_validation_errors(self):
        validation_errors = {}

        validation_errors[self.primary_worksheet.worksheet.title] = self.primary_worksheet.validation_errors

        return validation_errors

    def generate_validation_errors_for_display(self):
        error_list = []

        for worksheet_title, worksheet_errors in self.validation_errors.items():
            for cell_index, cell_errors in worksheet_errors.items():

                for error in cell_errors:
                    error_message = u'%s / %s - %s' % (worksheet_title, cell_index, error)
                    error_list.append(error_message)

        error_list = self.translate_errors(error_list)

        return error_list

    def translate_errors(self, errors):
        return [self.translate_error(error) for error in errors]

    def translate_error(self, error):
        error = error.replace("must be a float", "must be a decimal number")
        error = error.replace("Enter a valid date in YYYY-MM-DD format.", "This value must be a valid date.")

        return error

    def generate_dynamic_object(self, row_id):
        base_dynamic_object = self.primary_worksheet.object_dict.get(row_id)

        if not base_dynamic_object:
            return None

        related_dynamic_objects = []

        for fieldgroup_worksheet in self.fieldgroup_worksheets:
            dynamic_objects = fieldgroup_worksheet.object_dict_by_row_id.get(row_id)

            if not dynamic_objects:
                continue

            related_dynamic_objects = related_dynamic_objects + dynamic_objects

        base_dynamic_object.save()

        for related_dynamic_object in related_dynamic_objects:
            related_dynamic_object.parent = base_dynamic_object
            related_dynamic_object.save()

        return base_dynamic_object


class BaseWorksheet(object):
    worksheet = None
    bulk_import = None
    starting_row = 2
    max_ending_row = 104

    template_object = None  # The bulk import that we're basing the worksheet structure off
    number_of_applications = None  # The number of applications that will be included
    table_values = None  # The template table_values used to prefill values
    table_headers = None
    template_name = None
    project = None

    def __init__(self, worksheet, bulk_import, template_name, template_object, number_of_applications, project=None, table_values=None):

        self.worksheet = worksheet
        self.bulk_import = bulk_import
        self.template_name = template_name
        self.template_object = template_object
        self.number_of_applications = number_of_applications
        self.project = project
        self.table_values = table_values

    def get_data_rows_by_id(self):
        data_rows = self.worksheet.rows[(self.starting_row):self.max_ending_row]
        data_rows_by_id = defaultdict(list)

        for row in data_rows:
            if len(row) < 1:
                continue

            id_value = row[0].value

            data_rows_by_id[id_value].append(row)

        return data_rows_by_id


class DynamicWorksheetMixin(object):
    def cast_value(self, value, field):
        # if field.data_store_field.field_datatype == FieldDatatype.MULTICHOICE:
        #     return self.cast_multichoice_value(value)

        # TODO: More casting as required
        return value

    def cast_multichoice_value(self, value):
        if not value:
            return []

        f = StringIO(str(value))
        reader = unicode_csv.UnicodeReader(f)
        rows = list(reader)

        if not rows:
            return []

        return [val.strip() for val in rows[0]]


class PrimaryWorksheet(FieldIDProcessingMixin, DynamicWorksheetMixin, BaseWorksheet):
    object_dict = None
    validation_errors = None
    family_model_number_column_index = 1
    template_object = None

    def __init__(self, *args, **kwargs):
        super(PrimaryWorksheet, self).__init__(*args, **kwargs)

        self.process_field_id_cells()
        self.object_dict = self.generate_object_dict()
        self.validation_errors = self.validate_object_dict()
        self.save_bulk_import()

    def get_family_model_number_values_by_id(self):
        if not self.bulk_import.allows_family_model_number:
            return {}

        return self.get_cell_values_for_column_by_id(self.family_model_number_column_index)

    def get_cell_values_for_column_by_id(self, column_index):
        data_rows_by_id = self.get_data_rows_by_id()

        cell_values_by_id = {}

        for row_id, rows in data_rows_by_id.items():
            for row in rows:
                cell = row[column_index]

                cell_values_by_id[row_id] = cell.value

        return cell_values_by_id

    def generate_object_dict(self):
        object_dict = {}
        width = self.worksheet.get_highest_column()
        data_rows = self.worksheet.rows[self.starting_row:self.max_ending_row]

        i = 0
        for data_row in data_rows:
            i+=1
            index_cell = data_row[0]
            extra_cells = data_row[0:width+1]

            object_dict[i] = {}
            for cell_index, cell in enumerate(extra_cells, start=1):

                field = self.column_mapping.get(cell_index)
                field_name = self.model_field_dict.get(cell.column)

                if not field_name:
                    continue

                object_dict[i][field_name] = cell.value

        return object_dict

    def validate_object_dict(self):
        validation_errors = {}
        field_dict_by_column_name = self.get_field_dict_by_column_name()


        for row_id, obj in self.object_dict.items():

            try:
                for key, value in obj.items():
                    if key not in field_dict_by_column_name:
                        print ('error')

            except ValidationError as e:
                for (column_name, errors) in e.message_dict.items():

                    if column_name not in field_dict_by_column_name:
                        cell_index = 'General Error'
                    else:
                        field = field_dict_by_column_name[column_name]

                        column = self.get_column_letter_for_field(field)
                        row = row_id + self.starting_row

                        if not column:
                            continue

                        cell_index = u'%s%s' % (column, row)

                    validation_errors[cell_index] = errors

        return validation_errors

    def save_bulk_import(self):
        if not self.validation_errors:
            width = self.worksheet.get_highest_column()
            data_rows = self.worksheet.rows[self.starting_row:self.max_ending_row]

            import_instance = ProjectCSVTemplateImport.objects.create(template=self.template_object)

            i = 0
            for data_row in data_rows:
                i += 1
                index_cell = data_row[0]
                extra_cells = data_row[0:width+1]

                for cell_index, cell in enumerate(extra_cells, start=1):
                    field = self.column_mapping.get(cell_index)
                    field_name = self.model_field_dict.get(cell.column)

                    if not field_name:
                        continue

                    attr_name = field_name
                    value = self.cast_value(cell.value, field)

                    # print ("creating row %s with %s - %s" % (i, attr_name, value)

                    field = ProjectCSVTemplateFields.objects.filter(template=self.template_object,
                                                                    column_pretty_name=field_name)
                    if field:
                        field = field[0]

                        ProjectCSVTemplateFieldValues.objects.create(field=field,
                                                                     value=value,
                                                                     row_id=i,
                                                                     import_instance=import_instance)


def prepare_model_number(model_number):
    return str(model_number)[:240]

