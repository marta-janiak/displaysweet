from hashlib import md5

from openpyxl.cell import get_column_letter
# from dynamicforms.models import Field
from django.db.models import ForeignKey
from core.models import ProjectCSVTemplateFields


def generate_items_to_hash_from_workbook(workbook):
    items_to_hash = []

    for worksheet in workbook.worksheets:
        # items_to_hash.append(u'worksheet_%s' % worksheet.title)

        highest_column_index = worksheet.get_highest_column()
        highest_column_letter = get_column_letter(highest_column_index)

        range_string = 'A1:%s1' % highest_column_letter
        rows = worksheet.range(range_string)

        for row in rows:
            empty_cell_backlog_count = 0

            for cell in row:
                value = cell.value

                # This weird empty cell count thing prevents
                # empty cells at the end of a row from affecting
                # the hash value. It's important because Excel
                # has a tendency to throw in empty cells for no
                # good reason.

                if not value:
                    empty_cell_backlog_count += 1
                    continue

                for _ in range(empty_cell_backlog_count):
                    items_to_hash.append((u'empty_header').encode('utf-8'))

                empty_cell_backlog_count = 0
                items_to_hash.append((u'header_%s' % value).encode('utf-8'))

        print('items_to_hash', items_to_hash)


def generate_structure_hash_from_workbook(workbook):
    """
    Generates a simple hash of the workbook structure so that we can
    easily compare against a subsequent upload.
    """
    items_to_hash = generate_items_to_hash_from_workbook(workbook)
    h = md5(str(items_to_hash).encode("utf-8")).hexdigest()[-10:]

    # print ("creator ----------------------"
    # print (items_to_hash
    # print (len(items_to_hash)
    # print (h

    return h


class FieldIDProcessingMixin(object):
    field_id_cells = None
    field_dict = None
    column_mapping = None
    foreign_key_names = None
    header_names = None
    field_id_dict = None
    model_field_dict = None

    def process_field_id_cells(self):

        self.foreign_key_names = self.get_foreign_key_names()
        self.header_names = self.get_header_names()
        self.field_id_cells = self.get_field_id_cells()
        self.field_dict = self.get_field_dict()
        self.field_id_dict = self.get_id_field_dict()
        self.model_field_dict = self.get_model_field_dict()
        self.column_mapping = self.get_column_mapping()

    def get_foreign_key_names(self):
        foreign_key_names = []

        # model_fields = self.project_model_object._meta.get_fields()
        #
        # for field in model_fields:
        #     field_object, model, direct, m2m = self.project_model_object._meta.get_field_by_name(field.name)
        #     if not m2m and direct and isinstance(field_object, ForeignKey):
        #         foreign_key_names.append(field.name)

        return foreign_key_names

    def get_header_names(self):
        header_names = []

        table_headers = ProjectCSVTemplateFields.objects.filter(template=self.template_object)

        for field in table_headers:
            header_names.append(field.column_pretty_name)

        return header_names

    def get_field_id_cells(self):
        field_id_row = 2

        highest_column_index = self.worksheet.get_highest_column()
        highest_column_letter = get_column_letter(highest_column_index)

        range_string = 'A%s:%s%s' % (field_id_row, highest_column_letter, field_id_row)
        field_id_cells = self.worksheet.range(range_string)[0]

        return field_id_cells

    def get_model_field_dict(self):
        model_field_dict = {}

        for cell in self.field_id_cells:

            try:
                model_field_dict[cell.column] = cell.value
            except:
                pass

        return model_field_dict

    def get_field_dict(self):
        field_id_values = []
        foreign_key_values = []

        for cell in self.field_id_cells:
            try:
                field_id = cell.value
            except:
                field_id = None

            field_id_values.append(field_id)

        related_model_objects = {}

        for field in foreign_key_values:
            field_object, model, direct, m2m = self.project_model_object._meta.get_field_by_name(field)
            related_class = field_object.rel.to
            related_model_objects[field] = related_class.objects.using(self.project.project_connection_name).all()

        return related_model_objects

    def get_id_field_dict(self):
        field_id_values = []

        related_model_objects = {}
        for cell in self.field_id_cells:

            try:
                field_id = cell.value
            except:
                field_id = None

            if field_id and field_id == "id":
                field_id_values.append(field_id)
                related_model_objects = {field_id:field_id}

        return related_model_objects

    def get_field_dict_by_column_name(self):
        field_dict_by_column_name = {}

        for field_id, field in self.model_field_dict.items():
            field_dict_by_column_name[field] = field

        return field_dict_by_column_name

    def get_column_letter_for_field(self, field):
        print ('get_column_letter_for_field')

        for cell in self.field_id_cells:
            print (cell, cell.value, field, cell.column)

            if field == cell.value:
                return cell.column

        return None

    def get_column_mapping(self):
        column_mapping = {}
        for column_index, field_id_value in enumerate(self.get_header_names()):
            # Add one to the index because openpyxl columns are 1-indexed
            column_mapping[column_index+1] = self.model_field_dict.get(field_id_value, None)

        return column_mapping

    def get_foreign_key_column_mapping(self):
        column_mapping = {}

        for column_index, field_id_value in enumerate(self.get_header_names()):
            if field_id_value in self.get_foreign_key_names():
                column_mapping[column_index+1] = self.field_dict.get(field_id_value, None)

        return column_mapping
