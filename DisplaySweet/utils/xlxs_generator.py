import re
from itertools import groupby

from django.http import HttpResponse
from django.utils.html import strip_tags

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.cell import get_column_letter
from django.db.models.fields.related import ForeignKey, ManyToOneRel

# Python 2.6 error in openpyxl so we use a local modified copy of DataValidation
from xlsx_tools.datavalidation import (DataValidation, ValidationType,
    ValidationOperator)

from .tools import list_to_csv
# from dynamicforms.models import FieldGroup, FieldDatatype, FieldRender
from .xlxs_common import generate_structure_hash_from_workbook, FieldIDProcessingMixin


class BulkImportTemplate(object):
    workbook = None
    primary_worksheet = None
    model_worksheet = None
    fieldgroup_worksheets = None
    structure_hash = None
    number_of_applications = None
    project_model_object = None
    table_values = None
    table_headers = None
    model_name = None
    project = None

    def __init__(self, model_name, project_model_object, number_of_applications, project=None, table_values=None):
        self.project_model_object = project_model_object
        self.table_values = table_values
        self.number_of_applications = number_of_applications
        self.table_headers = self.clean_table_headers(project_model_object._meta.get_fields())
        self.model_name = model_name
        self.project = project
        self.workbook = Workbook()
        self.create_primary_worksheet()
        self.structure_hash = self.generate_structure_hash()

    def clean_table_headers(self, table_headers):
        base_headers = []
        for field in table_headers:
            if not type(field) == ManyToOneRel and \
                    not field.name == 'id':
                base_headers.append(field.name)

        return base_headers

    def create_primary_worksheet(self):
        """
        Create the first worksheet in the excel file. This will contain
        basic information about the table_values along with every dynamic field
        that doesn't live in a repeating fieldset
        """
        self.primary_worksheet = PrimaryWorksheet(
            self.workbook.worksheets[0],
            self.model_name,
            self.project_model_object,
            self.number_of_applications,
            self.project,
            self.table_values
        )

    def render_to_response(self, file_name="template.xlsx"):
        """
        Renders the excel file to an HTTP response.
        """
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename="%s"' % file_name

        response.write(save_virtual_workbook(self.workbook))

        return response

    def generate_structure_hash(self):
        """
        Generates a simple hash of the workbook structure so that we can
        easily compare against a subsequent upload.
        """
        return generate_structure_hash_from_workbook(self.workbook)


class BaseWorksheet(object):
    """
    A base class containing functionality common to all worksheets
    """

    title = None  # The name of the worksheet
    starting_row = 2

    # The hierarchy of styles to apply to headers. These will loop if
    # the bottom is reached.
    header_style_map = (
        ('FFFFFF00', '00000000', 13),  # Background color, font color, font size
        ('FFDDFFDD', '00000000', 11),
        ('FFDDFFDD', '00000000', 11),
        ('FFDDFFDD', '00000000', 11),
        ('FFDDFFDD', '00000000', 11),
        ('FFDDFFDD', '00000000', 11),
    )

    worksheet = None  # The openpyxl worksheet object
    project_model_object = None  # The bulk import that we're basing the worksheet structure off
    number_of_applications = None  # The number of applications that will be included
    table_values = None  # The template table_values used to prefill values
    table_headers = None
    model_name = None
    project = None

    def __init__(self, worksheet, model_name, project_model_object, number_of_applications, project=None, table_values=None):
        self.worksheet = worksheet
        self.project_model_object = project_model_object
        self.number_of_applications = number_of_applications
        self.project = project

        self.table_values = table_values
        self.table_headers = self.clean_table_headers(project_model_object._meta.get_fields())

        self.model_name = model_name

        self.worksheet.title = self.title

        self.pre_headers_hook()
        self.header_list = self.generate_header_list()
        self.header_set = self.generate_header_set()
        self.apply_header_set()
        self.set_header_styles()
        self.autosize_headers()

        self.pre_validations_hook()
        self.set_validations()

        self.fill_data()

    def pre_headers_hook(self):
        """
        A function called before headers are generated.
        """
        pass

    def generate_header_list(self):
        """
        A function to generate a hierarchical list of strings that will define
        the headers in the worksheet
        """
        return []

    def generate_header_set(self):
        """
        A function that turns the header list into a set of header objects
        """
        return HeaderSet(self.header_list)

    def apply_header_set(self):
        """
        Adds the headers to the openpyxl worksheet
        """
        header_groups = self.header_set.all_grouped_by_level

        for group_index, header_group in enumerate(header_groups):
            column_index = 0
            row_index = group_index

            for header_index, header in enumerate(header_group):
                cell_index = '%s%s' % (get_column_letter(column_index + 1), row_index + 1)

                self.worksheet.cell(cell_index).value = header.name

                if header.width > 1:
                    self.worksheet.merge_cells(
                        start_row=row_index,
                        end_row=row_index,
                        start_column=column_index,
                        end_column=column_index + header.width - 1,
                    )

                column_index += header.width

        self.set_header_styles()

    def clean_table_headers(self, table_headers):
        base_headers = []
        for field in table_headers:
            if not type(field) == ManyToOneRel and \
                    not field.name == 'id':
                base_headers.append(field.name)

        return base_headers

    def set_header_styles(self, bottom_level=None):
        """
        Styles up the worksheet headers
        """
        header_set = self.header_set

        depth = header_set.depth

        if not bottom_level:
            bottom_level = depth

        # Background color, text color, font size
        style_map = self.header_style_map

        for column, dimensions in self.worksheet.column_dimensions.items():
            for row_index in range(1, depth):
                style_index = (row_index % len(style_map)) - 1
                cell_index = column + str(row_index)

                border_style = 'thin'

                self.worksheet.cell(cell_index).style.font.bold = True
                self.worksheet.cell(cell_index).style.font.color.index = style_map[style_index][1]
                self.worksheet.cell(cell_index).style.font.size = style_map[style_index][2]
                self.worksheet.cell(cell_index).style.alignment.horizontal = 'center'
                self.worksheet.cell(cell_index).style.alignment.vertical = 'center'
                self.worksheet.cell(cell_index).style.alignment.wrap_text = True
                self.worksheet.cell(cell_index).style.fill.fill_type = 'solid'
                self.worksheet.cell(cell_index).style.fill.start_color.index = style_map[style_index][0]
                self.worksheet.cell(cell_index).style.borders.left.border_style = border_style
                self.worksheet.cell(cell_index).style.borders.right.border_style = border_style
                self.worksheet.cell(cell_index).style.borders.top.border_style = border_style
                self.worksheet.cell(cell_index).style.borders.bottom.border_style = border_style

    def autosize_headers(self):
        """
        Sets the sizing of the worksheet headers
        """
        header_set = self.header_set
        primary_row = header_set.depth

        for column, dimensions in self.worksheet.column_dimensions.items():
            cell_index = column + str(primary_row)
            header_value = u'%s' % self.worksheet.cell(cell_index).value
            width = min(max(len(header_value) + 2, 15), 50)
            self.worksheet.column_dimensions[column].width = width

    def pre_validations_hook(self):
        """
        A function to be called before validations are set on the worksheet
        """
        pass

    def set_validations(self):
        """
        Sets validations on the worksheet cells
        """
        pass

    def fill_data(self):
        """
        Fills the worksheet cells up with data
        """
        pass

    def set_id_cell_style(self, cell):
        """
        Sets the styling on ID cells
        """
        cell.style.font.bold = True
        cell.style.fill.fill_type = 'solid'
        cell.style.fill.start_color.index = 'FF999999'
        cell.style.font.color.index = 'FFFFFFFF'
        cell.style.borders.bottom.border_style = 'thin'


class DynamicWorksheetMixin(FieldIDProcessingMixin, object):
    """
    A mixing contining functionality to deal with dynamic form schemas
    """
    def __init__(self, *args, **kwargs):
        super(DynamicWorksheetMixin, self).__init__(*args, **kwargs)

    def pre_validations_hook(self):
        self.process_field_id_cells()

    def set_validations(self):
        self.apply_field_validations_to_id_column()
        for cell in self.field_id_cells:
            try:
                print(1, cell, cell.value)
                field = self.field_dict.get(cell.value)
                print(self.field_dict)

                if not field:
                    continue

                self.apply_field_validations_to_column(field, cell.column)
            except Exception as e:
                print('error', e)

    def apply_field_validations_to_id_column(self):

        row_index = 1
        table_headers = ['id', ]
        for item in self.table_headers:
            table_headers.append(item)

        for item in self.table_values:
            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(pk=item.pk)

            value = getattr(model_instance, 'id')

            dv = DataValidation(validation_type="list", formula1='"%s"' % value)
            dv.attr_map['errorStyle'] = "information"
            self.worksheet.add_data_validation(dv)
            validation_range = "A%s:A%s" % ((row_index+2), (row_index+2))
            dv.ranges.append(validation_range)
            row_index+=1

    def apply_field_validations_to_column(self, fields, column_letter):

        validation_range = "%s%s:%s1048576" % (column_letter, self.starting_row+1, column_letter)

        fields = fields[:10]
        field_value_string = ','.join([str(fv.id) for fv in fields])

        dv = DataValidation(validation_type="list", formula1='"%s"' % field_value_string)
        dv.attr_map['errorStyle'] = "information"
        self.worksheet.add_data_validation(dv)
        dv.ranges.append(validation_range)


class PrimaryWorksheet(DynamicWorksheetMixin, BaseWorksheet):
    title = "Data for Import"
    family_model_number_column_index = None

    def generate_header_list(self):
        base_headers = [
            (self.model_name, (
                "id",
            )),
        ]

        for field in self.table_headers:
            base_headers.append(('', (field,)))

        return base_headers

    def fill_data(self, *args, **kwargs):
        super(PrimaryWorksheet, self).fill_data(*args, **kwargs)

        self.fill_from_table_values()

    def fill_from_table_values(self):
        if not self.table_values or not self.table_headers:
            return

        row_index = 1
        table_headers = ['id', ]
        for item in self.table_headers:
            table_headers.append(item)

        for item in self.table_values:
            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(pk=item.pk)

            for column_index, field in enumerate(table_headers, start=1):
                column_letter = get_column_letter(column_index)
                cell_index = column_letter + str(row_index + 2)
                cell = self.worksheet.cell(cell_index)

                try:
                    value = getattr(model_instance, field)
                except:
                    try:
                        #related objects id
                        field = '%s_id' % field
                        value = getattr(model_instance, field)
                    except:
                        value = None

                cell.value = value
            row_index+=1

    def cast_value(self, value):
        if type(value) == list:
            return list_to_csv(value)

        return value


class HeaderSet(object):
    def __init__(self, headers):
        self.top_headers = [Header(h) for h in headers]
        self._normalise_headers()

    @property
    def all(self):
        headers = []

        for header in self.top_headers:
            headers.append(header)
            headers = headers + header.descendents

        return headers

    @property
    def all_grouped_by_level(self):
        headers = self.all
        headers.sort(key=lambda h: h.level)

        groups = groupby(headers, lambda h: h.level)

        return [[h for h in header_group] for (level, header_group) in groups]

    @property
    def depth(self):
        return max([h.level for h in self.all])

    def _normalise_headers(self):
        depth = self.depth
        normalised = False

        while(not normalised):
            normalised = True
            headers = self.all

            for header in headers:
                if (header.level != depth and not header.subheaders):
                    header.add_blank_subheader()
                    normalised = False


class Header(object):
    def __init__(self, header, level=1):
        try:
            name, subheaders = header
        except (TypeError, ValueError):
            name = header
            subheaders = []

        self.level = level
        self.name = name
        self.subheaders = [Header(sh, level+1) for sh in subheaders]

    @property
    def width(self):
        if not self.subheaders:
            return 1

        return sum([sh.width for sh in self.subheaders])

    @property
    def descendents(self):
        if not self.subheaders:
            return []

        descendents = []

        for subheader in self.subheaders:
            descendents.append(subheader)
            descendents = descendents + subheader.descendents

        return descendents

    def add_blank_subheader(self):
        new_subheader = Header("", self.level+1)

        self.subheaders.append(new_subheader)


def get_repr(value):
    if callable(value):
        return '%s' % value()
    return value


def get_field(instance, field):

    field_path = field.split('.')
    attr = instance
    for elem in field_path:
        try:
            attr = getattr(attr, elem)
        except AttributeError:
            return None
    return attr


def get_fk_model(model, fieldname):
    '''returns None if not foreignkey, otherswise the relevant model'''
    field_object, model, direct, m2m = model._meta.get_field_by_name(fieldname)
    if not m2m and direct and isinstance(field_object, ForeignKey):
        return field_object.rel.to
    return None