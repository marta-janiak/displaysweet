from django.db import models
import json
from django.conf import settings
from datetime import datetime, date, time

class JSONEncoder(json.JSONEncoder):
    """
    Overrides the default JSONEncoder to provide better handling of dates
    """
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        elif isinstance(obj, time):
            return obj.strftime('%H:%M:%S')
        return json.JSONEncoder.default(self, obj)


class JSONField(models.TextField):
    """
    A field that stores serialized JSON in the database.
    """
    def _dumps(self, data):
        return JSONEncoder().encode(data)

    def _loads(self, string):
        return json.loads(string, encoding=settings.DEFAULT_CHARSET)

    def db_type(self, connection):
        return 'text'

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        return self._dumps(value)

    def contribute_to_class(self, cls, name):
        self.class_name = cls
        super(JSONField, self).contribute_to_class(cls, name)
        models.signals.post_init.connect(self.post_init)

        def get_json(model_instance):
            return self._dumps(getattr(model_instance, self.attname, None))
        setattr(cls, 'get_%s_json' % self.name, get_json)

        def set_json(model_instance, json):
            return setattr(model_instance, self.attname, self._loads(json))
        setattr(cls, 'set_%s_json' % self.name, set_json)

    def post_init(self, **kwargs):
        if 'sender' in kwargs and 'instance' in kwargs:
            if kwargs['sender'] == self.class_name and hasattr(kwargs['instance'], self.attname):
                value = self.value_from_object(kwargs['instance'])
                if (value):
                    # If the object is new the value might not yet be JSON encoded, therefore allow
                    # for either possibility
                    try:
                        setattr(kwargs['instance'], self.attname, self._loads(value))
                    except TypeError:
                        setattr(kwargs['instance'], self.attname, value)
                else:
                    setattr(kwargs['instance'], self.attname, None)


# try:
#     from south.modelsinspector import add_introspection_rules
#     add_introspection_rules([], ["^erws\.json_field\.JSONField"])
# except:
#     pass


# class SampleModel(models.Model):
#    first_name = models.CharField(max_length=50)
#    last_name = models.CharField(max_length=50)
#    data = JSONField(null=True, blank=True)
#
#
# sample = SampleModel(first_name='Patrick', last_name='Altman')
# sample.data = {'pets': None, 'children': ['Benjamin', 'Clare', 'Joshua'], 'some_date': datetime(2010, 02, 01)}
# sample.save()


