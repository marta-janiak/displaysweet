import pyinotify
import os
import sys
import subprocess
import threading


class OnWriteHandler(pyinotify.ProcessEvent):
    def my_init(self, localDirectory):
        self.remoteDirectory = "/var/www/html/project_data/projects"
        self.transferringList = {}
        self.localDirectory = localDirectory.replace('\ ',' ').replace('\(', '(').replace('\)', ')').rstrip('/')
        self.checkTransfers()

    def checkTransfers(self):
        threading.Timer(15.0, self.checkTransfers).start()
        keysToErase = []
        for key in self.transferringList:
            if self.transferringList[key].poll() != None:
                keysToErase.append(key)

        for key in keysToErase:
            del self.transferringList[key]

    def uploadFile(self, event):
        path = event.path
        name = event.name
        fullPath = path + '/' + name
        #fullPath = fullPath.replace(' ', '\\ ').replace('(', '\(').replace(')', '\)')
        if fullPath in self.transferringList:
            self.transferringList[fullPath].terminate()
            del self.transferringList[fullPath]
        truncatedPath = path.replace(self.localDirectory, "")
        print (self.localDirectory, truncatedPath)
        args = ['/usr/bin/ssh', '-p', '16122', 'root@103.245.145.34', 'mkdir -p ' + self.remoteDirectory + truncatedPath]
        print (" ".join(args))
        subprocess.call(args)
        truncatedFullPath = truncatedPath + '/' + name
        args = ['/usr/bin/scp', '-P', '16122', fullPath, 'root@103.245.145.34:"' + self.remoteDirectory + truncatedFullPath + '"']
        print (" ".join(args))
        self.transferringList[fullPath] = subprocess.Popen(args)

    def process_IN_CLOSE_WRITE(self, event):
        print ("Write")
        self.uploadFile(event)

    def process_IN_MOVED_TO(self, event):
        self.uploadFile(event)

class autoUploader:
    def __init__(self, rootDir):
        self.directories = []
        self.watchManager = pyinotify.WatchManager()
        self.handler = OnWriteHandler(localDirectory=rootDir)
        self.notifier = pyinotify.Notifier(self.watchManager, default_proc_fun=self.handler)
        self.setupMonitoring(rootDir)

    def setupMonitoring(self, rootDir):
        directories = [x[0] for x in os.walk(rootDir)]
        for directory in directories:
            self.watchManager.add_watch(directory, pyinotify.ALL_EVENTS, rec=True, auto_add=True)

    def run(self):
        self.notifier.loop()

if __name__ == "__main__":
    app = autoUploader('/home/gdrive/googledrive/DS SERVER DELIVERY/SERVER SET/')
    app.run()
