import pyinotify
import os
import sys
import subprocess
import psycopg2

from queue import Queue
import threading

class WriteQueue:
    def __init__(self):
        self.queue = Queue()

    def run(self):
        for i in range(8):
            t = threading.Thread(target=self.worker)
            t.daemon = True
            t.start()

    def worker(self):
        while True:
            item = self.queue.get()
            subprocess.call(item)
            self.queue.task_done()

    def add(self, task):
        self.queue.put(task)

class OnWriteHandler(pyinotify.ProcessEvent):
    def my_init(self, rootDir):
        self.rootDir = rootDir
        self.handledExtensions = ['jpg', 'jpeg', 'png']
        self.writeQueue = WriteQueue()
        self.writeQueue.run()

    def handleDimensionsXml(self, wantedPath, path, name):
        self.writeQueue.add(["python3", "convertImages.py", self.rootDir, wantedPath, path, name])

    def parseFile(self, event):
        currentPath = os.getcwd()
        #print ("Current directory is (%s)" % (currentPath))
        path = event.path
        name = event.name
        if ".inc" in path:
            #print ("Ignoring incoming folder")
            return

        if "/._" in path:
            #print ("Ignoring hidden folder")
            return

        fileParts = name.split('.')
        if not fileParts[-1] in self.handledExtensions:
            print ("(%s) not a handled extension" % (fileParts[-1]))
            os.chdir(currentPath)
            return

        if "._" in name:
            #print ("Ignored hidden file")
            return

        pathParts = path.split('/')
        for i in reversed(range(len(pathParts))):
            wantedPath = "/".join(pathParts[0:i+1])
            if os.path.isfile(wantedPath + "/dimensions.xml"):
                self.handleDimensionsXml(wantedPath, path, name)
                break

        os.chdir(currentPath)

    def process_IN_CLOSE_WRITE(self, event):
        self.parseFile(event)

    def process_IN_MOVED_TO(self, event):
        self.parseFile(event)

class autoConverter:
    def __init__(self, rootDir):
        self.directories = []
        self.watchManager = pyinotify.WatchManager()
        self.handler = OnWriteHandler(rootDir=rootDir)
        self.notifier = pyinotify.Notifier(self.watchManager, default_proc_fun=self.handler)
        self.setupMonitoring(rootDir)

    def getListenedFolders(self):
        res = self.curs.execute("select path, project_id, folder_type_id from folders group by project_id, folder_type_id, path")
        for record in self.curs:
            if not record.project_id in self.directories:
                self.directories[record.project_id] = {}

            if not record.folder_type_id in self.directories[record.project_id]:
                self.directories[record.project_id][record.folder_type_id] = []

            self.directories[record.project_id][record.folder_type_id].append(record.path)

    def setupMonitoring(self, rootDir):
        directories = [x[0] for x in os.walk(rootDir)]
        for directory in directories:
            self.watchManager.add_watch(directory, pyinotify.IN_CLOSE_WRITE | pyinotify.IN_MOVED_TO, rec=True, auto_add=True)

    def run(self):
        self.notifier.loop()

if __name__ == "__main__":
    app = autoConverter('/home/gdrive/googledrive/DS SERVER DELIVERY/IMAGE READY/')
    app.run()
