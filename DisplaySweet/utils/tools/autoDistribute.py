import pyinotify
import os
import sys
import subprocess
import shutil

outputPaths = ['ipad', 'thumb', 'projector', 'touchtable']

class OnWriteHandler(pyinotify.ProcessEvent):
    def parseFile(self, event):
        path = event.path
        name = event.name
        if ".inc" in path:
            pathParts = path.split('/')
            for i in range(len(pathParts) - 1, 0, -1):
                if ".inc" in pathParts[i]:
                    wantedPath = "/".join(pathParts[:i]) + '/'
                    print ("Creating output dirs in ", wantedPath)
                    for part in outputPaths:
                        if not os.path.exists(wantedPath + part):
                            os.mkdir(wantedPath + part)
                        outputFileName = wantedPath + part + '/'
                        nameParts = name.split('.')
                        nameParts[-2] += "_" + part
                        outputFileName += ".".join(nameParts)
                        shutil.copyfile(path + '/' + name, outputFileName)
                        print ("Copied ", path + '/' + name, " to ", outputFileName)


    def process_IN_CLOSE_WRITE(self, event):
        print ("Parsing write")
        self.parseFile(event)

    def process_IN_MOVED_TO(self, event):
        self.parseFile(event)

class autoDistributer:
    def __init__(self, rootDir):
        self.directories = []
        self.watchManager = pyinotify.WatchManager()
        self.handler = OnWriteHandler()
        self.notifier = pyinotify.Notifier(self.watchManager, default_proc_fun=self.handler)
        self.setupMonitoring(rootDir)

    def setupMonitoring(self, rootDir):
        directories = [x[0] for x in os.walk(rootDir)]
        for directory in directories:
            self.watchManager.add_watch(directory, pyinotify.ALL_EVENTS, rec=True, auto_add=True)

    def run(self):
        self.notifier.loop()

if __name__ == "__main__":
    app = autoDistributer('/home/gdrive/googledrive/DS SERVER DELIVERY/IMAGE READY/')
    app.run()

