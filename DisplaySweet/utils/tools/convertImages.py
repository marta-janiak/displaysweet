import xml.etree.ElementTree as ET
import sys
import os
import subprocess
from time import strftime

imageMagickConverter = "convert"
filePlaceholder="%s[%sx%s]"
arguments = ["-gravity", "%s", "-background", "#000000", "%s", "%sx%s"]
operations = []
outputDirectory = "../SERVER\ SET"

gravityList = {'center': 'Center', 'top-left': 'NorthWest', 'top': 'North', 'top-right': 'NorthEast', 
    'left': 'West', 'right': 'East', 'bottom-left': 'SouthWest', 'bottom': 'South', 'bottom-right': 'SouthEast'}

operationList = {'letterbox': '-extent', 'punch-out': '-crop', 'scale-n-punch': '-extent'}

watermarkFile = ""

watermarker = 'composite'
watermarkArgs = ['-dissolve', '30', '-gravity', '%s', '-', '%s', '%s']

def tsprint(message):
    currentTime = strftime("%Y-%m-%d %H:%M:%S")
    print ("%s > %s" % (currentTime, message))

def parseFile(xmlNode, directory, operation, gravity):
    wantedFile = directory + "/" + xmlNode.attrib['name']
    if os.path.isfile(wantedFile):
        pass
        #self.parseFileDirect(wantedFile, xmlNode.attrib['width'], xmlNode.attrib['height'], operation, gravity)
    else:
        tsprint ("Explicitly specified file (%s) does not exist" % wantedFile)
    return wantedFile

def parseFileDirect(fileName, node):
    width = node.attrib['width']
    height = node.attrib['height']
    operation = node.attrib['operation']
    gravity = node.attrib['anchor-point']
    watermarked = node.get('watermarked', False)
    watermarkGravity = node.get('watermark-anchor-point', 'center')

    placeHeldFile = filePlaceholder % (fileName, width, height)
    del operations[:]

    #tsprint (operation, "operation chosen")

    operations.append(operationList[operation])

    if operation == "punch-out":
        placeHeldFile = fileName
        operations.append("%sx%s+0+0" % (width, height))
        operations.append("+repage")
    elif operation == "scale-n-punch":
        placeHeldFile = "%s[%sx%s^]" % (fileName, width, height)
        operations.append("%sx%s" % (width, height))
    else:
        operations.append("%sx%s" % (width, height))

    placeHeldOperations = [arguments[0], arguments[1] % (gravityList[gravity]), arguments[2], arguments[3]]
    placeHeldOperations.extend(operations)

    outputPath = outputDirectory + "/" + fileName[:fileName.rfind("/")]
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)

    placeHeldOperations.insert(0, placeHeldFile)
    placeHeldOperations.insert(0, imageMagickConverter)
    placeHeldOperations.append(outputDirectory + "/" + fileName)

    tsprint ("Executing: (%s)" % (" ".join(placeHeldOperations)))

    subprocess.call(placeHeldOperations)

    if watermarked == "yes":
        del placeHeldOperations[:]
        placeHeldOperations = [watermarker]
        placeHeldOperations.extend(watermarkArgs)
        placeHeldOperations[4] = watermarkGravity
        placeHeldOperations[6] = outputDirectory + "/" + fileName
        placeHeldOperations[7] = watermarkFile

        subprocess.call(placeHeldOperations)

def parseDirectoryDirect(directory, node):
    for entry in os.listdir(directory):
        listedEntry = directory + "/" + entry
        if os.path.isfile(listedEntry):
            parseFileDirect(listedEntry, node)
        else:
            parseDirectoryDirect(listedEntry, node)

def parseDirectory(xmlTree, directory):
    currentDirectory = directory
    if xmlTree.attrib['name'] != "":
        currentDirectory += "/" + xmlTree.attrib['name']
    if os.path.exists(currentDirectory):
        parsedFiles = []
        for child in xmlTree:
            if child.tag == "folder":
                parseDirectory(child, currentDirectory)
            elif child.tag == "file":
                parsedFiles.append(parseFile(child, currentDirectory, xmlTree.attrib['operation'], xmlTree.attrib['anchor-point']))
        for entry in os.listdir(currentDirectory):
            listedEntry = currentDirectory + "/" + entry
            if not listedEntry in parsedFiles:
                if os.path.isfile(listedEntry):
                    #tsprint ("parsing directly ", xmlTree.attrib['name'], listedEntry)
                    parseFileDirect(listedEntry, xmlTree)
                else:
                    parseDirectoryDirect(listedEntry, xmlTree)
    else:
        tsprint ("Explicitly specified directory (%s) does not exist" % currentDirectory)

def parseExplicitlySpecified(xmlTree, directory, specifiedFile):
    #tsprint ("parseExplicitlySpecified")
    parts = directory.split("/")
    currentTag = xmlTree
    for part in parts:
        if part == '.':
            continue
        #tsprint (part)
        found = False
        for child in currentTag:
            if child.tag == "folder" and (child.attrib['name'] == part or child.attrib['name'] in directory):
                 currentTag = child
                 found = True
                 break
        if not found:
            break

    #tsprint ("currentTag = ", currentTag.attrib['name'])

    for child in currentTag:
        if child.tag == "file":
            if child.attrib['name'] == specifiedFile:
                parseFileDirect(directory + "/" + specifiedFile, child)
                return

    parseFileDirect(directory + "/" + specifiedFile, currentTag)

def parseDimensions(dimensionsFile, directory, wantedPath, specifiedFile):
    tree = ET.parse(dimensionsFile)
    root = tree.getroot()
    watermarkFile = root.get("watermark", "")    
    if specifiedFile != None:
        parseExplicitlySpecified(root, wantedPath, specifiedFile)
    else:
        parseDirectory(root, directory)

def main():
    global outputDirectory
    #tsprint (sys.argv)
    rootdir = sys.argv[1]
    directory = sys.argv[2]
    outputDirectory = directory.replace('IMAGE READY', 'SERVER SET')
    wantedPath = None
    specifiedFile = None
    if len(sys.argv) > 3:
        wantedPath = "." + sys.argv[3].replace(directory, "")
        specifiedFile = sys.argv[4]
    if os.path.exists(directory):
        currentDirectory = os.getcwd()
        os.chdir(directory)
        if os.path.isfile("dimensions.xml"):
            parseDimensions("dimensions.xml", ".", wantedPath, specifiedFile)
        else:
            tsprint ("No dimensions.xml file in folder (%s)" % directory)
        os.chdir(currentDirectory)
    else:
        tsprint ("Folder (%s) does not exist" % directory)
    subprocess.call(['chown', '-R', 'adamt:users', outputDirectory])

if __name__ == "__main__":
    main()

