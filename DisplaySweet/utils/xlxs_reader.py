from collections import defaultdict
import re
from io import StringIO

from django.core.exceptions import ValidationError

from openpyxl import load_workbook


from utils.xlxs_generator import BulkImportTemplate
from django.apps import apps
from utils.xlxs_common import generate_structure_hash_from_workbook, FieldIDProcessingMixin
from utils import unicode_csv
from core.models import Project


MAX_INPUT_ROWS = 100000


class InvalidImportException(Exception):
    pass


class ImportedWorkbook(object):
    xlsx_file = None
    bulk_import = None
    validation_errors = None
    project_model_object = None
    table_values = None
    table_headers = None
    model_name = None
    project = None
    number_of_applications = None

    def __init__(self, bulk_import, xlsx_file=None):

        self.bulk_import = bulk_import
        self.project = bulk_import.project
        self.model_name = bulk_import.table_name

        project = Project.objects.get(pk=bulk_import.project.pk)
        project_model_object = apps.get_model(project.project_connection_name, self.model_name)
        table_values = project_model_object.objects.using(project.project_connection_name).all()

        self.table_values = table_values
        self.project_model_object = project_model_object
        self.number_of_applications = len(table_values)

        self.expected_template = BulkImportTemplate(self.model_name, project_model_object, 1, project=self.project, table_values=table_values)

        if xlsx_file:
            self.xlsx_file = xlsx_file
        else:
            self.xlsx_file = bulk_import.uploaded_xlsx

        try:
            self.workbook = load_workbook(self.xlsx_file)
        except Exception:
            print ('Utils __init__ error on load workbook')
            raise InvalidImportException()

        self.primary_worksheet = self.get_primary_worksheet()
        self.structure_hash = self.generate_structure_hash()

        print ('in checking hash')
        print (self.structure_hash)
        print (self.expected_template.structure_hash)

        if self.structure_hash != self.expected_template.structure_hash:
            print ('Utils Structured has error')
            raise InvalidImportException()

        self.validation_errors = self.generate_validation_errors()
        self.validation_errors_for_display = self.generate_validation_errors_for_display()

    def get_primary_worksheet(self):
        return PrimaryWorksheet(
            self.workbook.worksheets[0],
            self.bulk_import,
            self.model_name,
            self.project_model_object,
            self.number_of_applications,
            project=self.project,
            table_values=self.table_values
        )

    def generate_structure_hash(self):
        """
        Generates a simple hash of the workbook structure so that we can
        easily compare against the expected template.
        """
        return generate_structure_hash_from_workbook(self.workbook)

    def generate_validation_errors(self):
        validation_errors = {}

        validation_errors[self.primary_worksheet.worksheet.title] = self.primary_worksheet.validation_errors

        return validation_errors

    def generate_validation_errors_for_display(self):
        error_list = []

        for worksheet_title, worksheet_errors in self.validation_errors.items():
            for cell_index, cell_errors in worksheet_errors.items():

                for error in cell_errors:
                    error_message = u'%s / %s - %s' % (worksheet_title, cell_index, error)
                    error_list.append(error_message)

        error_list = self.translate_errors(error_list)

        return error_list

    def translate_errors(self, errors):
        return [self.translate_error(error) for error in errors]

    def translate_error(self, error):
        error = error.replace("must be a float", "must be a decimal number")
        error = error.replace("Enter a valid date in YYYY-MM-DD format.", "This value must be a valid date.")

        return error

    def generate_dynamic_object(self, row_id):
        base_dynamic_object = self.primary_worksheet.object_dict.get(row_id)

        if not base_dynamic_object:
            return None

        related_dynamic_objects = []

        for fieldgroup_worksheet in self.fieldgroup_worksheets:
            dynamic_objects = fieldgroup_worksheet.object_dict_by_row_id.get(row_id)

            if not dynamic_objects:
                continue

            related_dynamic_objects = related_dynamic_objects + dynamic_objects

        base_dynamic_object.save()

        for related_dynamic_object in related_dynamic_objects:
            related_dynamic_object.parent = base_dynamic_object
            related_dynamic_object.save()

        return base_dynamic_object


class BaseWorksheet(object):
    worksheet = None
    bulk_import = None
    starting_row = 2
    max_ending_row = 104

    project_model_object = None  # The bulk import that we're basing the worksheet structure off
    number_of_applications = None  # The number of applications that will be included
    table_values = None  # The template table_values used to prefill values
    table_headers = None
    model_name = None
    project = None

    def __init__(self, worksheet, bulk_import, model_name, project_model_object, number_of_applications, project=None, table_values=None):

        self.worksheet = worksheet
        self.bulk_import = bulk_import
        self.model_name = model_name
        self.project_model_object = project_model_object
        self.number_of_applications = number_of_applications
        self.project = project
        self.table_values = table_values

    def get_data_rows_by_id(self):
        data_rows = self.worksheet.rows[(self.starting_row):self.max_ending_row]
        data_rows_by_id = defaultdict(list)

        for row in data_rows:
            if len(row) < 1:
                continue

            id_value = row[0].value

            data_rows_by_id[id_value].append(row)

        return data_rows_by_id


class DynamicWorksheetMixin(object):
    def cast_value(self, value, field):
        # if field.data_store_field.field_datatype == FieldDatatype.MULTICHOICE:
        #     return self.cast_multichoice_value(value)

        # TODO: More casting as required
        return value

    def cast_multichoice_value(self, value):
        if not value:
            return []

        f = StringIO(str(value))
        reader = unicode_csv.UnicodeReader(f)
        rows = list(reader)

        if not rows:
            return []

        return [val.strip() for val in rows[0]]


class PrimaryWorksheet(FieldIDProcessingMixin, DynamicWorksheetMixin, BaseWorksheet):
    object_dict = None
    validation_errors = None
    family_model_number_column_index = 1
    project_model_object = None

    def __init__(self, *args, **kwargs):
        super(PrimaryWorksheet, self).__init__(*args, **kwargs)

        self.process_field_id_cells()
        self.object_dict = self.generate_object_dict()
        self.validate_row_ids()
        self.validation_errors = self.validate_object_dict()
        self.save_bulk_import()

    def validate_row_ids(self):

        data_rows_by_id = self.get_data_rows_by_id()

        for row_id, rows in data_rows_by_id.items():

            try:
                row_id = int(row_id)
            except:
                print ('validate_row_ids utils error')
                raise InvalidImportException()

            if row_id > MAX_INPUT_ROWS:
                print ('validate_row_ids max utils error')
                raise InvalidImportException

    def get_family_model_number_values_by_id(self):
        if not self.bulk_import.allows_family_model_number:
            return {}

        return self.get_cell_values_for_column_by_id(self.family_model_number_column_index)

    def get_cell_values_for_column_by_id(self, column_index):
        data_rows_by_id = self.get_data_rows_by_id()

        cell_values_by_id = {}

        for row_id, rows in data_rows_by_id.items():
            for row in rows:
                cell = row[column_index]

                cell_values_by_id[row_id] = cell.value

        return cell_values_by_id

    def generate_object_dict(self):
        object_dict = {}
        width = self.worksheet.get_highest_column()
        data_rows = self.worksheet.rows[self.starting_row:self.max_ending_row]

        for data_row in data_rows:
            index_cell = data_row[0]
            extra_cells = data_row[1:width]
            index_cell_value = index_cell.value

            if not index_cell_value or index_cell_value in object_dict:
                continue

            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(pk=index_cell_value)

            for cell_index, cell in enumerate(extra_cells, start=1):
                field = self.column_mapping.get(cell_index)
                field_name = self.model_field_dict.get(cell.column)

                if not field_name:
                    continue

                attr_name = field_name
                value = self.cast_value(cell.value, field)

                #print ("Setting %s instance %s to %s" % (model_instance, attr_name, value)
                setattr(model_instance, attr_name, value)
            object_dict[index_cell_value] = model_instance

        return object_dict

    def validate_object_dict(self):
        validation_errors = {}
        field_dict_by_column_name = self.get_field_dict_by_column_name()

        for row_id, obj in self.object_dict.items():
            try:
                obj.full_clean()
            except ValidationError as e:
                for (column_name, errors) in e.message_dict.items():

                    if column_name not in field_dict_by_column_name:
                        cell_index = 'General Error'
                    else:
                        field = field_dict_by_column_name[column_name]

                        column = self.get_column_letter_for_field(field)
                        row = row_id + self.starting_row

                        if not column:
                            continue

                        cell_index = u'%s%s' % (column, row)

                    validation_errors[cell_index] = errors

        return validation_errors

    def save_bulk_import(self):
        if not self.validation_errors:
            width = self.worksheet.get_highest_column()
            data_rows = self.worksheet.rows[self.starting_row:self.max_ending_row]

            for data_row in data_rows:
                index_cell = data_row[0]
                extra_cells = data_row[1:width]
                index_cell_value = index_cell.value
                model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(pk=index_cell_value)

                for cell_index, cell in enumerate(extra_cells, start=1):
                    field = self.column_mapping.get(cell_index)
                    field_name = self.model_field_dict.get(cell.column)

                    if not field_name:
                        continue

                    attr_name = field_name
                    value = self.cast_value(cell.value, field)

                    #print ("Setting %s instance %s to %s" % (model_instance, attr_name, value)
                    setattr(model_instance, attr_name, value)
                    model_instance.save(using=self.project.project_connection_name)

def prepare_model_number(model_number):
    return str(model_number)[:240]

