import re
import inspect
from io import StringIO
import csv

from django.db.models import fields, Q
from users.models import User
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.db.models.fields.files import FieldFile
from django.forms import ClearableFileInput
from utils import unicode_csv
from django.db.models.fields.files import FileField, FieldFile


class SecureFieldFile(FieldFile):
    def _get_url(self):
        self._require_file()

        try:
            return self.storage.url(self.name, self.field, self.instance)
        except TypeError:
            return self.storage.url(self.name)

    url = property(_get_url)


def process_val(val):
    """
    Given a value, coerces it into a CSV friendly format.
    - If the value has a 'csv_render' function, that will be used
    - If the value is an iterable, and has no 'csv_render' function, it
      will be rendered in CSV

    :param val: The value to process
    """
    if type(val) == FieldFile:
        return val.name
    if hasattr(val, 'csv_render'):
        return val.csv_render()
    if hasattr(val, '__iter__'):
        return list_to_csv(val)
    else:
        return val


def list_to_csv(items):
    """
    Given a list of items, converts it to CSV. The function is recursive, so
    if you have a list in a list, you'll get CSVs all the way down.

    :param list: The list to process
    """
    processedlist = []

    # Return a blank string if everything in the list is blank
    found_something = False
    try:
        for item in items:
            if item:
                found_something = True
    except OSError as e:
        print(e)

    if not found_something:
        return ""

    # Convert any iterables within the iterable to csv also
    for item in items:
        item = process_val(item)

        processedlist.append(item)

    # Create a fake file in memory
    output = StringIO()

    # Generate the CSV writer
    writer = unicode_csv.UnicodeWriter(output)

    # Write the row
    writer.writerow(items)

    # Grab the string from the fake file
    # Strip off any trailing whitespace/newlines
    contents = output.getvalue().rstrip()

    # Close the fake file
    output.close()

    # Return the string
    return contents


class DynamicClearableFileInput(ClearableFileInput):

    template_with_initial = u"""
<div class='file-controls'>
    <div class='file-initial'>
        <span class='file-initial-title'></span>
        <span class='file-initial-link'>%(initial)s</span>
    </div>
    %(clear_template)s
    <div class='file-change'>
        <span class='file-change-title'>Change: </span>
        <span class='file-change-input'>%(input)s</span>
    </div>
</div>

<script typt="text/javascript">
    $(".file-clear-input input").trigger('change');

    // Annoying hack because django makes changing this text
    // ridiculously hard.
    $(".file-initial-link a").text('Click To Download Current File').attr('target','_blank');
</script>
"""

    template_with_clear = u"""
<div class='file-clear'>
    <label class='file-clear-label' for="%(clear_checkbox_id)s">Delete This File: </label>
    <span class='file-clear-input'>%(clear)s</span>
</div>

<script typt="text/javascript">
    $("#%(clear_checkbox_id)s").bind('change', function(){
        var clear_val = $(this).attr('checked');
        var controls = $(this).parents('.file-controls');
        var file_link = controls.find('.file-initial-link a');
        var file_change = controls.find('.file-change');

        if(clear_val){
            file_link.addClass('deleted-file-link');
            controls.addClass('deleted-file-controls');
            file_change.hide('fast');
        }
        else {
            file_link.removeClass('deleted-file-link');
            controls.removeClass('deleted-file-controls');
            file_change.show('fast');
        }
    });
</script>
"""


class DynamicFileInput(ClearableFileInput):

    template_with_initial = u"""
<div class='file-controls'>
    <div class='file-initial'>
        <span class='file-initial-title'>Currently: </span>
        <span class='file-initial-link'>%(initial)s</span>
    </div>
    <div class='file-change'>
        <span class='file-change-title'>Change: </span>
        <span class='file-change-input'>%(input)s</span>
    </div>
</div>
"""