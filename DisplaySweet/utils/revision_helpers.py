
import datetime


def add_new_revision(project, request, object_class, comment):

    # Save a new model revision.

    from core.models import ProjectChangeLog

    if project and comment:

        new_log = ProjectChangeLog(project=project, version=project.version, log=comment, title="Change made by user",
                                   created_by=request.user, modified=datetime.datetime.now())

        new_log.save()

        project.last_modified_by = request.user
        project.modified = datetime.datetime.now()
        project.save()

