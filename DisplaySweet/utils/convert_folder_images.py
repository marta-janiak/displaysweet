import xml.etree.ElementTree as ET
import sys
import os
import subprocess
from django.conf import settings
from time import strftime
import shutil
from django.contrib import messages

class InvalidImportException(Exception):
    pass


class ImportedImageDistributor(object):
    
    image_magick_converter = "convert"
    file_placeholder="%s -resize %sx%s"
    arguments = ["-gravity", "%s", "-background", "#000000", "%s", "%sx%s"]
    operations = []
    output_directory = "../SERVER_SET"
    
    gravity_list = {'center': 'Center', 'top-left': 'NorthWest', 'top': 'North', 'top-right': 'NorthEast', 
        'left': 'West', 'right': 'East', 'bottom-left': 'SouthWest', 'bottom': 'South', 'bottom-right': 'SouthEast'}
    
    operation_list = {'letterbox': '-extent', 'punch-out': '-crop', 'scale-n-punch': '-extent'}
    
    watermark_file = ""
    watermarker = 'composite'
    watermark_args = ['-dissolve', '30', '-gravity', '%s', '-', '%s', '%s']
    
    directory = None
    root_dir = None
    wanted_path = None
    specified_file = None
    dimensions_file = None
    saving_location = None
    current_directory = None
    output_path = None
    image_path = None
    errors = 0
    request = None

    def __init__(self, project, root_dir, directory, wanted_path, saving_location, specified_file=None, request=None):

        self.directory = directory
        self.root_dir = root_dir
        # self.dimensions_file = project.image_dimensions
        self.saving_location = saving_location

        if request:
            self.request = request

        # print (directory
        # print (wanted_path
        # print (root_dir
        # print (specified_file

        output_path = self.get_projector_location(project).replace("IMAGE_READY", "SERVER_SET")
        self.output_path = output_path

        self.image_path = self.get_projector_location(project)

        if specified_file:
            wanted_path = directory + specified_file
        else:
            wanted_path = directory

        wanted_path = wanted_path.replace("//", "/").replace("IMAGE_READY/", "")
        self.wanted_path = wanted_path

        if specified_file:
            total = len(specified_file)
            if specified_file[total-1] == "/":
                specified_file = specified_file[:-1]

            if specified_file[0] == "/":
                specified_file = specified_file[1:]

        if os.path.exists(directory):
            if os.path.isfile(self.dimensions_file.path):
                self.parse_dimensions(self.dimensions_file.path, ".", directory, wanted_path, None)

                # self.parse_dimensions(self.dimensions_file.path, ".", wanted_path, "Images/exterior/ipad")
        else:
            self.time_message_print("Folder (%s) does not exist" % directory)

    def get_projector_location(self, project):
        image_path = '%s/projects/%s/IMAGE_READY/' % (settings.BASE_IMAGE_LOCATION, project.project_connection_name)
        image_path = image_path.replace("//", "/")

        if not os.path.isdir(image_path):
            os.makedirs(image_path)

        return image_path

    def time_message_print(self, message):
        current_time = strftime("%Y-%m-%d %H:%M:%S")
        #print (("%s > %s" % (current_time, message))
    
    def parse_file(self, xmlNode, directory, operation, gravity):
        print ('parse_file - ', directory)
        wanted_file = directory + "/" + xmlNode.attrib['name']
        if os.path.isfile(wanted_file):
            self.parse_file_direct(wanted_file, xmlNode.attrib['width'], xmlNode.attrib['height'], operation, gravity)
        else:
            self.time_message_print("Explicitly specified file (%s) does not exist" % wanted_file)
        return wanted_file

    def parse_file_direct(self, file_name, node):
        test_file_name = file_name.split(".")

        try:
            ext = test_file_name[1]
        except:
            ext = None

        if not ext in ["png", "jpg", "gif", "tif"]:
            return False

        width = node.attrib['width']
        height = node.attrib['height']
        operation = node.attrib['operation']
        gravity = node.attrib['anchor-point']
        watermarked = node.get('watermarked', False)
        watermarkGravity = node.get('watermark-anchor-point', 'center')


        place_held_file = self.file_placeholder % (self.image_path + file_name, width, height)
        del self.operations[:]
    
        self.time_message_print(operation)
    
        self.operations.append(self.operation_list[operation])

        if operation == "punch-out":
            place_held_file = file_name
            self.operations.append("%sx%s+0+0" % (width, height))
            self.operations.append("+repage")
        elif operation == "scale-n-punch":
            place_held_file = "%s -resize %sx%s" % (file_name, width, height)
            self.operations.append("%sx%s" % (width, height))
        else:
            self.operations.append("%sx%s" % (width, height))
    
        self.operations = [self.arguments[0], self.arguments[1] % (self.gravity_list[gravity]), self.arguments[2], self.arguments[3]]
        self.operations.extend(self.operations)

        outputPath = self.output_directory + "/" + file_name[:file_name.rfind("/")]
        #print ('outputPath'

        if not os.path.exists(self.output_directory):
            os.makedirs(self.output_directory)

        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

        self.operations.insert(0, place_held_file)
        self.operations.insert(0, self.image_magick_converter)

        doctored_file_name = file_name.split('.')
        doctored_file_name = str(doctored_file_name[0]) + '.jpg'

        self.operations.append(self.output_path + doctored_file_name)

        # print ('----------', self.output_directory + "/" + doctored_file_name

        operation_string = " ".join(self.operations)
        operation_string = operation_string.replace("-gravity Center -background #000000 -gravity Center -background #000000", "")
        #-gravity Center -background #000000 -gravity Center -background #000000

        print (operation_string)

        popen = subprocess.Popen(operation_string, bufsize=4096,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                 shell=True)

        popen.wait()

        # if popen.returncode == 0:
        #     self.errors+=1
        #     messages.error(self.request, "One or more files could not be converted")
        #
        # print ('popen.returncode', popen.returncode

        if watermarked == "yes":
            self.time_message_print("watermarked - yes")
            del self.operations[:]
            self.operations = [self.watermarker]
            self.operations.extend(self.watermark_args)
            self.operations[4] = watermarkGravity
            self.operations[6] = self.output_directory + "/" + file_name
            self.operations[7] = self.watermark_file

            #print ('convert'

            popen = subprocess.Popen(" ".join(self.operations), bufsize=4096,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         shell=True)

            popen.wait()

    def parse_directory_direct(self, xmlTree, directory, wanted_path):
        #for entry in os.listdir(os.path.join(self.root_dir, self.directory)):

        node = xmlTree
        for child in xmlTree:
            if child.tag == "folder" and child.attrib['name'] == self.specified_file:
                node = child

        if not os.path.isdir(os.path.join(self.root_dir, self.wanted_path)):
            os.makedirs(os.path.join(self.root_dir, self.wanted_path))

        for entry in os.listdir(os.path.join(self.root_dir, self.directory)):
            item = os.path.join(self.directory, entry)
            if not os.path.isdir(item) and os.path.isfile(item):
                self.parse_file_direct(entry, node)

        for entry in os.listdir(self.output_path):
            if os.path.isfile(os.path.join(self.output_path, entry)):
                test_file_name = entry.split(".")
                try:
                    ext = test_file_name[1]
                except:
                    ext = None

                if not ext in ["png", "jpg", "gif"]:
                    continue

                wanted = os.path.join(self.root_dir, self.saving_location)
                shutil.copy2(os.path.join(self.output_path, entry), os.path.join(wanted, entry))

        print ('finished moving files')
        for entry in os.listdir(self.image_path):
            print (entry)
            try:
                print (os.path.join(self.output_path, entry))
                print (os.path.join(self.image_path, entry))
                os.remove(os.path.join(self.image_path, entry))
            except:
                pass

            try:
                os.remove(os.path.join(self.output_path, entry))
            except:
                pass

    def parse_directory(self, xmlTree, directory, wanted_path):
        current_directory = directory
        if xmlTree.attrib['name'] != "":
            current_directory += "/" + xmlTree.attrib['name']

        self.current_directory = current_directory

        current_directory = current_directory.replace("IMAGE_READY/", "")

        if not os.path.exists(current_directory):
            os.makedirs(current_directory)

        if os.path.exists(current_directory):
            for entry in os.listdir(current_directory):
                listedEntry = current_directory + "/" + entry

                if os.path.isfile(listedEntry):
                    #time_message_print (("parsing directly ", xmlTree.attrib['name'], listedEntry)

                    self.parse_file_direct(listedEntry, xmlTree)
                else:
                    self.parse_directory_direct(listedEntry, xmlTree, wanted_path)
        else:
                self.time_message_print("Explicitly specified directory (%s) does not exist" % current_directory)

    def parse_explicitly_specified(self, xmlTree, directory, wanted_path, specified_file):

        parts = wanted_path.split("/")
        currentTag = xmlTree

        for part in parts:

            if part == '.':
                continue
            found = False

            for child in currentTag:
                if child.tag == "folder" and (child.attrib['name'] == specified_file):
                     currentTag = child
                     found = True
                     break
            if not found:
                break

        #time_message_print (("currentTag = ", currentTag.attrib['name'])

        print (currentTag)
        for child in currentTag:
            print (child.tag, child.attrib, child.attrib['name'])
            # if child.attrib['name'] == specified_file:
            #     for item in os.listdir(directory):
            #         self.parse_file_direct(directory + "/" + specified_file, item)
            #     return

        # self.parse_file_direct(directory + "/" + specified_file, currentTag)

    def parse_dimensions(self, dimensionsFile, directory, sdirectory, wanted_path, specified_file):
        print ('parse_dimensions - ', directory)
        tree = ET.parse(dimensionsFile)
        root = tree.getroot()
        self.watermark_file = root.get("watermark", "")

        if specified_file != None:
            self.parse_explicitly_specified(root, sdirectory, wanted_path, specified_file)
        else:
            self.parse_directory_direct(root, directory, wanted_path)