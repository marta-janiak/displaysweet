# import reversion
# from reversion.helpers import generate_patch_html

from django import template



register = template.Library()


@register.filter(name='html_diff')
def html_diff(version):
    """
    Returns the html diff for a django-reversion revision
    as compared to it most recent predecessor
    """
    previous_version = get_previous_version(version)

    fields = version.field_dict.keys()

    if not previous_version:
        return None

    change_list = []

    # for field in fields:
        # diff = generate_patch_html(previous_version, version, field, cleanup="semantic")
        # change_list.append((field, diff))

    return change_list


def get_previous_version(version):
    obj = version.object

    if not obj:
        return None

    # available_versions = reversion.get_for_object(obj)
    #
    # try:
    #     version_idx = list(available_versions).index(version)
    # except IndexError:
    #     return None
    #
    # if version_idx == 0:
    #     return None
    #
    # return available_versions[version_idx-1]
