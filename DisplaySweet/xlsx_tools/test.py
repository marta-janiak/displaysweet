import os
from pprint import pprint

from . import xlsx2dict_relational


def main():
    this_directory = os.path.dirname(__file__)
    test_file = os.path.join(this_directory, 'test.xlsx')
    result = xlsx2dict_relational(test_file)
    pprint(result)


if __name__ == "__main__":
    main()
