from copy import deepcopy
from openpyxl import load_workbook
from openpyxl.cell import column_index_from_string

from openpyxl import Workbook
from openpyxl.cell import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook
from django.http import HttpResponse

class XLSXTooLargeException(Exception):
    pass


def xlsx2dict_relational(filename, max_width=702, max_height=1000):
    if not validate_all_worksheets_smaller_than(filename, max_width+1, max_height+1):
        raise XLSXTooLargeException("The XLSX file provided must have a maximum of %s columns and %s rows" % (max_width, max_height))

    workbook = load_workbook(filename)

    first_sheet = get_first_sheet(workbook)
    extra_sheets = get_extra_sheets(workbook)

    data = process_first_sheet(first_sheet)

    for extra_sheet in extra_sheets:
        data = add_extra_sheet_to_dict(extra_sheet, data)

    return data


def get_first_sheet(workbook):
    sheet_names = workbook.get_sheet_names()

    if not sheet_names:
        return None

    return workbook.get_sheet_by_name(sheet_names[0])


def get_extra_sheets(workbook):
    sheet_names = workbook.get_sheet_names()

    if not sheet_names:
        return []

    return [workbook.get_sheet_by_name(sheet_name) for sheet_name in sheet_names[1:]]


def get_column_headers(worksheet):
    column_headers = []

    height = worksheet.get_highest_row()

    if height < 1:
        return []

    rows = worksheet.rows

    first_row = rows[0]

    for cell in first_row:
        value = cell.value

        if value and value not in column_headers:
            column_headers.append(value)

    return column_headers


def process_first_sheet(worksheet):
    column_headers = get_column_headers(worksheet)
    extra_column_headers = column_headers[1:]

    data = {}

    height = worksheet.get_highest_row()
    width = worksheet.get_highest_column()

    if height < 2 or width < 1:
        return {}

    data_rows = worksheet.rows[1:]

    for data_row in data_rows:
        index_cell = data_row[0]
        extra_cells = data_row[1:len(column_headers)]

        index_cell_value = index_cell.value

        if not index_cell_value or index_cell_value in data:
            continue

        data[index_cell_value] = {}

        for cell_index, extra_cell in enumerate(extra_cells):
            header = extra_column_headers[cell_index]

            if header and header not in data[index_cell_value]:
                data[index_cell_value][header] = extra_cell.value

    return data


def add_extra_sheet_to_dict(worksheet, base_data):
    base_data = deepcopy(base_data)

    column_headers = get_column_headers(worksheet)
    extra_column_headers = column_headers[1:]

    for key, value in base_data.items():
        base_data[key][worksheet.title] = []

    height = worksheet.get_highest_row()
    width = worksheet.get_highest_column()

    if height < 2 or width < 1:
        return base_data

    data_rows = worksheet.rows[1:]

    for data_row in data_rows:
        index_cell = data_row[0]
        extra_cells = data_row[1:len(column_headers)]

        index_cell_value = index_cell.value

        if not index_cell_value or index_cell_value not in base_data:
            continue

        data = {}

        for cell_index, extra_cell in enumerate(extra_cells):
            header = extra_column_headers[cell_index]

            if header and header not in data:
                data[header] = extra_cell.value

        base_data[index_cell_value][worksheet.title].append(data)

    return base_data


def validate_all_worksheets_smaller_than(filename, width, height):
    workbook = load_workbook(filename=filename, use_iterators=True)

    sheet_names = workbook.get_sheet_names()

    if not sheet_names:
        return True

    for sheet_name in sheet_names:
        worksheet = workbook.get_sheet_by_name(sheet_name)
        highest_column = column_index_from_string(worksheet.get_highest_column())
        highest_row = worksheet.get_highest_row()

        if highest_column >= width or highest_row >= height:
            return False

    return True



def simple_single_sheet_workbook_from_data(title='export', headers=[], rows=[], **kwargs):
    wb = Workbook()
    ws = wb.get_active_sheet()
    ws.title = title

    column_count = len(headers)
    row_count = len(rows)
    row_start = 0

    if headers:
        row_start = 1
        for c in range(0, column_count):
            ws.cell(row=0, column=c).value = headers[c]

    for r in range(0,row_count):
        for c in range(0, column_count):
            ws.cell(row=row_start+r, column=c).value = rows[r][c]


    column_widths = []
    for i, header in enumerate(headers):
        column_widths += [len(str(header))]

    for row in rows:
        for i, cell in enumerate(row):
            if len(str(cell)) > column_widths[i]:
                column_widths[i] = len(str(cell))

    for i, column_width in enumerate(column_widths):
        ws.column_dimensions[get_column_letter(i+1)].width = column_width

    return wb

def simple_workbook_download(workbook, file_name="output.xlsx"):
    """
    Renders the excel file to an HTTP response.
    """
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="%s"' % file_name

    response.write(save_virtual_workbook(workbook))

    return response
