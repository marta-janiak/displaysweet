from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib


def send_smpt_email(text, html, subject, receiver):
    host = settings.SMTP_HOST
    port = settings.SMTP_PORT
    username = settings.SMTP_USERNAME
    password = settings.SMTP_PASSWORD

    sender = settings.EMAIL_FROM_ADDRESS

    # receiver = 'martajaniak@gmail.com'

    smtpObj = smtplib.SMTP(host=host, port=port)

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = receiver

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    msg.attach(part1)
    msg.attach(part2)

    try:
        smtpObj.login(username, password)
    except Exception as e:
        print ('could not login', e)

    try:
        smtpObj.sendmail(sender, receiver, msg.as_string())
        print ("Successfully sent email to: ", receiver)
    except Exception as e:
        print ("Error: unable to send email: ", e)