from django.http import HttpResponse
import json

from projects.models.model_mixin import PrimaryModelMixin
from django.db.models import Q
from django.template.loader import render_to_string
from core.models import Project
from users.models import User, UserModifiedLog


def get_project_search_list(request):

    if request.is_ajax():

        project_list = request.GET.getlist('project_list[]')

        status_list = request.GET.getlist('status_list[]')
        if status_list:
            status_list = [int(x) for x in status_list]

        project_status = 'Yes'
        user_type_list = request.GET.getlist('user_type_list[]')
        search_text = request.GET['search_text']

        users = request.user.viewable_users

        try:
            if project_list:
                project_list = [int(c) for c in project_list]
                user_list = []
                for u in users:
                    user_project = u.project_users_list.all().values_list('pk', flat=True)
                    check_project_user = list(set(project_list).intersection(user_project))

                    if check_project_user:
                        user_list.append(u.pk)

                users = users.filter(pk__in=user_list)

        except Exception as e:
            print(e)

        if user_type_list:
            users = users.filter(user_type__in=user_type_list)

        if status_list:
            users = users.filter(is_active__in=status_list)

        if search_text:
            users = users.filter(Q(first_name__icontains=search_text) |
                                 Q(last_name__icontains=search_text) |
                                 Q(email__icontains=search_text) |
                                 Q(username__icontains=search_text))

        if request.user.is_client_admin:
            if not user_type_list:
                user_type_list = User.CLIENT_ADMIN_CHOICES + [User.USER_TYPE_CLIENT_ADMIN]

            if not project_list:
                project_list = Project.objects.filter(users=request.user).values_list('pk', flat=True)

        if request.user.is_master_agent:
            if not user_type_list:
                user_type_list = User.MASTER_AGENT_CHOICES

            if not project_list:
                project_list = Project.objects.filter(users=request.user).values_list('pk', flat=True)

        if 'project_status' in request.GET and request.GET['project_status']:
            project_status = request.GET['project_status']

        rendered = None

        projects = Project.objects.filter(active=True).order_by("name")
        if project_list:
            projects = Project.objects.filter(pk__in=project_list).order_by("name")

        project_owners = []
        for project in projects:
            project_owners_list = User.objects.filter(project_users_list=project,
                                                      user_type__in=[User.USER_TYPE_MASTER_AGENT,
                                                                     User.USER_TYPE_CLIENT_ADMIN])

            for owner in project_owners_list:
                project_owners.append(owner)

        project_owners = list(set(project_owners))

        if request.user.is_ds_admin or request.user.is_client_admin:
            if project_list and project_status:
                updated_users = []
                if project_status == "Yes":
                    for project in projects:
                        project_users = project.users.all()
                        updated_users += list(set(users).intersection(project_users))

                if project_status == "No":
                    found_users = []
                    for project in projects:
                        project_users = project.users.all()
                        found_users += list(set(users).intersection(project_users))

                    updated_users = list(set(users) - set(found_users))

                users = list(set(updated_users))

        context = {
            'users': users,
            'projects': projects,
            'request': request,
            'project_owners': project_owners
        }

        html_template = "users/includes/user_list_details.html"
        try:
            rendered = render_to_string(html_template, context)
        except Exception as e:
            print(e)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_search_details(request):

    if request.is_ajax():

        project_list = request.GET.getlist('project_list[]')
        status_list = request.GET.getlist('status_list[]')
        if status_list:
            status_list = [int(x) for x in status_list]

        user_type_list = request.GET.getlist('user_type_list[]')
        search_text = request.GET['search_text']
        project_status = request.GET['project_status']

        app_access = None
        if "app_access" in request.GET and request.GET['app_access']:
            app_access = request.GET['app_access']

        rendered = None

        projects = Project.objects.filter(active=True).order_by("name")
        if project_list:
            projects = Project.objects.filter(pk__in=project_list).order_by("name")

        users = request.user.viewable_users

        if user_type_list:
            users = users.filter(user_type__in=user_type_list)

        if status_list:
            users = users.filter(is_active__in=status_list)

        users = users.distinct()

        project_owners = []
        for project in projects:
            project_owners_list = User.objects.filter(project_users_list=project, user_type__in=[User.USER_TYPE_MASTER_AGENT,
                                                                                                 User.USER_TYPE_CLIENT_ADMIN])

            for owner in project_owners_list:
                project_owners.append(owner)

        project_owners = list(set(project_owners))

        if search_text:
            users = users.filter(Q(first_name__icontains=search_text) |
                                 Q(last_name__icontains=search_text) |
                                 Q(email__icontains=search_text) |
                                 Q(username__icontains=search_text))

        if project_list and project_status:
            updated_users = []
            if project_status == "Yes":
                for project in projects:
                    project_users = project.users.all()
                    updated_users += list(set(users).intersection(project_users))

            if project_status == "No":
                found_users = []
                for project in projects:
                    project_users = project.users.all()
                    found_users += list(set(users).intersection(project_users))

                updated_users = list(set(users) - set(found_users))

            users = updated_users

        if project_list and app_access:
            updated_users = []

            try:
                if app_access == "Yes":
                    for project in projects:
                        project_users = project.users.all()
                        for user in project_users:
                            if user.has_app_access(project):
                                updated_users.append(user)

                updated_users = list(set(users).intersection(updated_users))
            except Exception as e:
                print(e)

            try:
                if app_access == "No":
                    found_users = []
                    for project in projects:
                        project_users = project.users.all()
                        for puser in project_users:
                            if puser.has_app_access(project) == False:
                                found_users += [puser]

                    updated_users = list(set(users) - set(found_users))

            except Exception as e:
                print(e)

            users = updated_users


            pass

        context = {
            'users': users,
            'projects': projects,
            'request': request,
            'project_owners': project_owners
        }

        html_template = "users/includes/user_project_list.html"
        try:
            rendered = render_to_string(html_template, context)
        except Exception as e:
            print(e)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def change_user_owner(request):

    if request.is_ajax():

        user_id = request.GET['user_id']
        new_owner_id = request.GET['new_owner_id']

        user = User.objects.get(pk=user_id)

        UserModifiedLog.objects.create_log(user, request.user, "User owner changed from %s to %s" % (user.created_by_id, new_owner_id))

        user.created_by_id=new_owner_id
        user.last_modified_by = request.user
        user.save()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)