from django.contrib import admin
from django.contrib import messages


from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from users.models import User, Purchaser, UserEmailTemplate, \
                         UserAccountEmails, UserModifiedLog, UserImports
from users.forms import UserEmailForm, TemplateEmailForm


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(label= ("Password"),
        help_text= ("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = User
        fields = ()

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class MyUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id', 'first_name', 'last_name', 'username', 'email', 'user_type',
                    'status',  'can_access_django_backend', 'is_active')

    list_filter = ('can_access_django_backend', 'status', 'user_type', 'project_users_list')

    fieldsets = (
        ("Personal Details", {'fields': ('first_name', 'last_name', 'phone_number', 'email',
                                         'company_name', 'website', 'temporary_setup_password')}),
        ("Created Details", {'fields': ('created_by', 'owned_by', 'last_modified_by')}),
        ("Login Information", {'fields': ('username','password')}),
        ('Permissions', {'fields': ('can_access_django_backend', 'status', 'user_type', 'is_active')}),
        ('Important dates', {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'date_of_birth', 'password1', 'password2')}
        ),
    )

    search_fields = ('email', 'first_name', 'last_name',)
    ordering = ('email',)
    filter_horizontal = ()

    actions = ('send_account_email', 'send_password_email', 'update_user_owner')

    def update_user_owner(modeladmin, request, queryset):
        for item in queryset:
            item.owned_by = item.created_by
            item.save()
        messages.success(request, "Updated successfully.")

    update_user_owner.short_description = 'Update users owner'

    def send_account_email(modeladmin, request, queryset):
        for item in queryset:
            item.send_setup_email()
        messages.success(request, "Email sent successfully.")

    send_account_email.short_description = 'Send account setup email'

    def send_password_email(modeladmin, request, queryset):
        for item in queryset:
            item.send_password_email()
        messages.success(request, "Email sent successfully.")

    send_password_email.short_description = 'Send reset password email'


class PurchaserAdmin(UserAdmin):
    # The forms to add and change user instances
    # form = UserChangeForm
    # add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id', 'first_name', 'last_name', 'email', 'status', 'property_id', 'title',
                    'company_name', 'agent')
    list_filter = ('status', 'projects', 'property_id')

    fieldsets = (
        ("Personal Details", {'fields': ('first_name', 'last_name', 'phone_number', 'email')}),
        ("Purchaser Details", {'fields': ('company_name','buyer_type','website','occupy_status',
                                         'purchaser_location','firb','nras','enquiry_source', 'property_id', 'projects')}),

        ("Agent Details", {'fields': ('agent',)}),
        ('Permissions', {'fields': ('status', 'user_type')}),
        #('Important dates', {'fields': ('last_login',)}),
    )


    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'date_of_birth', 'password1', 'password2')}
        ),
    )

    search_fields = ('email', 'first_name', 'last_name',)
    ordering = ('email',)
    filter_horizontal = ()


class NiceEditorMixin(object):

    class Media:
        js = [
            '//cdn.ckeditor.com/4.5.4/standard/ckeditor.js',
        ]
        # css = {
        #      'all': ('css/admin/my_own_admin.css',)
        # }


class EmailTemplateAdmin(admin.ModelAdmin):

    form = TemplateEmailForm
    model = UserEmailTemplate
    list_display = ('id', 'created', 'template_type', 'template_subject', 'view_preview_of_email' )
    list_filter = ('created', 'template_type')


class SubscriptionEmailAdmin(admin.ModelAdmin):

    form = UserEmailForm
    model = UserAccountEmails
    list_display = ('id', 'created', 'user', 'login_key', 'template_used', 'email_subject', 'email_image', 'view_preview_of_email')
    list_filter = ('created',)
    search_fields = ('name', 'email_address')

    #actions = ('second_blank_action', 'send_test_email', 'blank_action', 'send_live_email')

    def send_test_email(modeladmin, request, queryset):
        for item in queryset:
            item.send_admin_newsletter()
        messages.success(request, "TEST email to Admin accounts sent successfully.")

    send_test_email.short_description = 'Send Test email to Admin accounts'

    def send_live_email(modeladmin, request, queryset):
        for item in queryset:
            item.send_live_email()
        messages.success(request, "Client email to was sent successfully. Hooray! :)")

    send_live_email.short_description = 'Send email to all active potential clients'

    def blank_action(modeladmin, request, queryset):
        pass

    blank_action.short_description = ''

    def second_blank_action(modeladmin, request, queryset):
        pass

    second_blank_action.short_description = ''


class UserModifiedLogAdmin(admin.ModelAdmin):

    model = UserModifiedLog
    list_display = ('id', 'user_modified', 'user_modified_by', 'log', 'created',)
    list_filter = ('created', 'user_modified__project',)
    search_fields = ('user_modified__email',)


admin.site.register(UserEmailTemplate, EmailTemplateAdmin)
admin.site.register(User, MyUserAdmin)
admin.site.register(UserModifiedLog, UserModifiedLogAdmin)
admin.site.register(Purchaser, PurchaserAdmin)
admin.site.register(UserAccountEmails, SubscriptionEmailAdmin)
admin.site.register(UserImports)

