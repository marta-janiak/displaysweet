from django.conf import settings

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from users.models import User, Project, UserEmailTemplate, UserAccountEmails
from core.generic_views import ListCallbackView, VersionView, DeleteView, ActionView
from .forms import UserCreateForm, UserEditForm, UserActionForm, PasswordChangeForm, UserCreateProjectForm, \
                   UserCreateFromAllocationForm

from projects.cms_sync import ProjectCMSSync
from django.template import Context
from projects.models.model_mixin import PrimaryModelMixin
from users.forms import UserImportsForm
from users.models import UserModifiedLog


class UserListCallbackView(ListCallbackView):
    model = User
    max_display_length = 100

    # Label, selector, ordering, hidden by default
    column_details = (
        ('Username', 'username', 'username', False),
        ('First Name', 'first_name', 'first_name', False),
        ('Last Name', 'last_name', 'last_name', False),
        ('Email', 'email', 'email', False),
        ('Active', 'is_active', 'is_active', False),
        ('Actions', '_actions', '', False),
    )


@login_required
def user_list_view(request):
    template_name = "users/list.html"

    projects = Project.objects.filter(active=True)

    user_add_form = UserCreateProjectForm(request)

    if request.session.get('project', True):
        project_id = request.session['project_id']
        project = Project.objects.get(pk=project_id)
        user_add_form = UserCreateFromAllocationForm(request, project)

    if not request.user.is_ds_admin:
        projects = projects.filter(active=True, users=request.user)

        if request.user.is_client_admin:
            projects = projects.filter(status__in=[Project.LIVE, Project.STAGING])
        else:
            projects = projects.filter(status=Project.LIVE)

    users = []
    actions = []

    if request.user.can_add_user():
        actions.append(('Add a User', reverse('users-create')))

    view_filter_form = True
    user_types = User.USER_TYPE_CHOICES
    if request.user.is_client_admin:
        user_types = User.CLIENT_ADMIN_USER_TYPE_CHOICES

    if request.user.is_master_agent:
        user_types = User.MASTER_AGENT_USER_TYPE_CHOICES

    if request.user.is_agent or request.user.is_view_only_user:
        user_types = []
        view_filter_form = False

    context = {
        'users': users,
        'actions': actions,
        'user_add_form': user_add_form,
        'project_count': len(projects),
        'projects': projects,
        'total_projects': len(projects),
        'user_types': user_types,
        'view_filter_form': view_filter_form,
        'show_project': True
    }

    return render(request, template_name, context)


@login_required
def user_update_view(request):
    template_name = "users/update_owner_details.html"

    projects = Project.objects.filter(active=True)
    user_add_form = UserCreateProjectForm(request)

    if request.session.get('project', True):
        project_id = request.session['project_id']
        project = Project.objects.get(pk=project_id)
        user_add_form = UserCreateFromAllocationForm(request, project)

    if not request.user.is_ds_admin:
        projects = projects.filter(active=True, users=request.user, status=Project.LIVE)

    users = []
    project_list = []
    for project in projects:
        for user in project.users.filter(is_active=True).distinct().order_by('first_name'):
            if user not in users:
                if request.user.is_project_user:

                    if not user.is_ds_admin:
                        if request.user.is_master_agent:
                            if user.created_by == request.user:
                                users.append(user)

                        if request.user.is_client_admin:
                            if user.is_client_admin:
                                users.append(user)

                            if user.is_master_agent:
                                users.append(user)

                            if (user.is_agent or user.is_view_only_user) and user.created_by == request.user:
                                users.append(user)

                else:
                    users.append(user)

        if not project.name.lower() == "master":
            project_list.append(
                {'project': project, 'users': users})

    actions = []
    if request.user.can_add_user():
        actions.append(('Add a User', reverse('users-create')))

    if request.method == "POST":
        if request.session.get('project', True):
            project_id = request.session['project_id']
            project = Project.objects.get(pk=project_id)

            user_add_form = UserCreateFromAllocationForm(request, project, request.POST)
        else:
            user_add_form = UserCreateProjectForm(request, request.POST)

        if user_add_form.is_valid():
            user = user_add_form.save()
            user.created_by = request.user
            user.owned_by = request.user
            user.last_modified_by = request.user

            messages.success(request, "User added successfully.")
            return redirect('users-list')

    users = sorted(users, key=lambda k: str(k.first_name).lower())

    context = {
        'users': users,
        'actions': actions,
        'user_add_form': user_add_form,
        'project_list': project_list,
        'project_count': len(projects)

    }

    return render(request, template_name, context)


@login_required
def view_user(request, pk):
    template_name = "users/detail.html"

    user = get_object_or_404(User, pk=pk)

    project_group_list = []
    for project in user.project_users_list.filter(active=True):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)
        allocation_groups = model_mixin.model_object("AllocationGroups").all().values_list('pk', 'name__english')

        for group_id, group_name in allocation_groups:
            user_access = False
            check_user_access = model_mixin.model_object("AllocatedUsers").filter(user_id=user.pk,
                                                                                  allocation_group_id=group_id)

            if check_user_access:
                user_access = True

            project_group_list.append({
                'project': project,
                'allocation_group_name': group_name,
                'allocation_group_access': user_access
            })

    context = {
        'user': user,
        'actions': user_actions(request, user),
        'project_group_list': project_group_list
        
    }

    return render(request, template_name, context)


def user_actions(request, user):

    actions = []

    if user.can_be_edited_by(requested_user=request.user):

        user_projects = request.user.project_users_list.filter(active=True).count()
        actions.append(('Edit', reverse('users-update', args=[user.pk])))
        actions.append(('Edit Password', reverse('users-update_password', args=[user.pk])))
        actions.append(('Delete from Project', reverse('users-remove-from-project', args=[user.pk])))

        if user_projects > 1 or request.user.is_ds_admin:
            actions.append(('Assign to Projects', reverse('users-assign-to-project', args=[user.pk])))

    if not request.user == user and request.user.is_administrator:
        actions.append(('Delete', reverse('users-delete', args=[user.pk])))

    return actions


class UserVersionView( VersionView):
    template_name = "users/version.html"
    model = User
    context_object_name = "user"


@login_required
def assign_existing_users(request, pk):
    template = "users/assign_user_projects.html"
    user = get_object_or_404(User, pk=pk)


    if request.user.is_ds_admin:
        user_projects = Project.objects.filter(active=True)
    else:
        user_projects = request.user.project_users_list.filter(active=True,
                                                           status__in=[Project.STAGING, Project.LIVE]).order_by('name')

    if request.method == "POST":
        user_form = UserEditForm(request, request.POST, instance=user)

        if user_form.is_valid():
            user = user_form.save()
            user.last_modified_by = request.user
            user.save()

            projects = user.project_users_list.all()
            print('projects', projects)

            if projects:
                project = projects[0]
                new_sync = ProjectCMSSync()
                new_sync.update_user_email_address(project.pk, user.email, user.username)

            if not user.is_ds_admin:
                try:
                    user.username = user.email
                    user.save()
                except Exception as e:
                    print(e)

            messages.success(request, 'User edited successfully')
            return redirect('users-list')
    else:
        user_form = UserEditForm(request, instance=user)

    context = {
        'user_form': user_form,
        'user_projects': user_projects,
        'user': user,

    }
    return render(request, template, context)


@login_required
def remove_user_from_project(request, pk):
    template = "users/update_user_projects.html"
    user = get_object_or_404(User, pk=pk)

    if request.method == "POST":
        user_form = UserEditForm(request, request.POST, instance=user)

        if user_form.is_valid():
            UserModifiedLog.objects.create_log(user, request.user,
                                               "User removed from project %s" % user_form.project)
            user = user_form.save()
            user.last_modified_by = request.user
            user.save()

            projects = user.project_users_list.all()
            if projects:
                project = projects[0]
                new_sync = ProjectCMSSync()
                new_sync.update_user_email_address(project.pk, user.email, user.username)

            if not user.is_ds_admin:
                try:
                    user.username = user.email
                    user.save()
                except Exception as e:
                    print(e)

            return redirect('users-list')
    else:
        user_form = UserEditForm(request, instance=user)

    context = {
        'user_form': user_form,
        'user': user,

    }
    return render(request, template, context)


@login_required
def update_user(request, pk):
    template = "users/update.html"
    user = get_object_or_404(User, pk=pk)

    if request.method == "POST":
        user_form = UserEditForm(request, request.POST, instance=user)

        if user_form.is_valid():
            UserModifiedLog.objects.create_log(user, request.user, "User updated")

            user = user_form.save()
            user.last_modified_by = request.user
            user.save()

            projects = user.project_users_list.all()
            if projects:
                project = projects[0]
                new_sync = ProjectCMSSync()
                new_sync.update_user_email_address(project.pk, user.email, user.username)

            if not user.is_ds_admin:
                try:
                    user.username = user.email
                    user.save()
                except Exception as e:
                    print(e)

            messages.success(request, 'User edited successfully')
            return redirect('users-list')
    else:
        user_form = UserEditForm(request, instance=user)

    context = {
        'user_form': user_form,
        'user': user,
        
    }
    return render(request, template, context)


@login_required
def update_user_password(request, pk):
    template = "users/update_password.html"

    user = get_object_or_404(User, pk=pk)

    if request.method == "POST":
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(user)
            messages.success(request, "User details saved successfully.")
            return redirect('users-list')
    else:
        form = PasswordChangeForm()

    context = {
        'user': user,
        'form': form,
        
    }

    return render(request, template, context)


@login_required
def create_user(request):
    template = "users/create.html"

    if request.method == "POST":
        user_form = UserCreateForm(request, request.POST)

        if user_form.is_valid():
            user = user_form.save()
            UserModifiedLog.objects.create_log(user, request.user, "User created")
            messages.success(request, 'User created successfully')
            return redirect('users-list')
    else:
        user_form = UserCreateForm(request)

    context = {
        'user_form': user_form,
        
    }

    return render(request, template, context)


@login_required
def user_types(request):
    template = "users/user_types.html"

    context = {
        'user_types': User.USER_TYPE_CHOICES,
        
    }

    return render(request, template, context)


class UserDeleteView(DeleteView):
    template_name = "users/delete.html"
    model = User
    context_object_name = "user"
    success_url = reverse_lazy('users-list')


class BaseUsersOrderActionView( ActionView):
    template_name = None
    model = User
    form_class = UserActionForm
    context_object_name = "user"
    success_message = "Action completed successfully"

    def perform_action(self, form):
        raise NotImplementedError


def user_account_setup(request):

    template = "account_setup.html"
    form = None
    user = None
    error = False

    if "activation_key" in request.GET:
        activation_key = request.GET['activation_key']

        email = UserAccountEmails.objects.filter(login_key=activation_key)
        if not email:
            messages.error(request, "The activation key has either been used "
                                    "or is invalid.")
            error = True
        else:
            email = email[0]
            user = get_object_or_404(User, pk=email.user.pk)

            if request.method == "POST":
                form = PasswordChangeForm(request.POST)
                if form.is_valid():
                    form.save(user)

                    messages.success(request, "Login details updated successfully.")

                    email.login_key = None
                    email.save()

                    return redirect('projects-list')
            else:
                form = PasswordChangeForm()

    context = {
        'form': form,
        'user': user,
        'error': error
    }
    return render(request, template, context)


def email_preview(request, pk):

    template = "emails/email_newsletter.html"
    email = UserEmailTemplate.objects.get(pk=pk)

    email_text = str(email.template_text)

    context = Context({'STATIC_URL': settings.STATIC_URL,
                        'email_content': email_text,
                        'base_url': ''})

    return render(request, template, context)


def user_email_preview(request, pk):

    template = "emails/email_newsletter.html"
    email = UserAccountEmails.objects.get(pk=pk)

    email_text = str(email.email_text)
    context = Context({'STATIC_URL': settings.STATIC_URL,
                        'email_content': email_text,
                        'base_url': ''})

    return render(request, template, context)


@login_required
def project_permissions(request):
    template = "users/project_permissions.html"

    projects = Project.objects.filter(active=True).order_by("name")
    user_types = User.USER_TYPE_CHOICES

    file_form = UserImportsForm()

    context = {
        'projects': projects,
        'total_projects': projects,
        'user_types': user_types,
        'file_form': file_form,
        'show_project': True
    }

    return render(request, template, context)