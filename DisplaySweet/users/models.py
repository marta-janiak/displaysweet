from django.db import models
from django.core import validators
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from rest_framework.authtoken.models import Token as AuthToken
import datetime
from django.db.models import Q
from django.conf import settings
from django.core.urlresolvers import reverse
from core.models import Project
from core.permissions import ClientPermissionsMixin
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from users.permissions import AllocationDetailsPermissionMixin
import smtplib
import string
import urllib
import random
from .smtp_email import send_smpt_email
from projects.models.model_mixin import PrimaryModelMixin
from projects.cms_sync import ProjectCMSSync


class UserManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, email, user_type=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not username:
            raise ValueError('Users must have a username')

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )

        if user_type:
            user.user_type = user_type

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, first_name, last_name, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username, first_name, last_name, email, user_type=User.USER_TYPE_SUPER_ADMIN, password=password)

        user.can_access_django_backend = True
        user.status = User.APPROVED
        user.save(using=self._db)

        return user


class User(AllocationDetailsPermissionMixin, AbstractBaseUser):

    MR = 'Mr'
    MS = 'Ms'
    MRS = 'Mrs'
    DR = 'Dr'
    MISS = 'Miss'

    TITLE_OPTIONS = (
        (MS, 'Ms'),
        (MR, 'Mr'),
        (MRS, 'Mrs'),
        (MISS, 'Miss'),
        (DR, 'Dr'),
    )

    PRE_APPROVED = 'pending_registration'
    SUSPENDED = 'suspended'
    APPROVED = 'approved'

    STATUS_CHOICES = (
        (PRE_APPROVED, 'Pending Approval'),
        (SUSPENDED, 'Suspended'),
        (APPROVED, 'Approved'),
    )

    USER_TYPE_SUPER_ADMIN = 'super_admin_user'
    USER_TYPE_ADMIN = 'admin_user'

    USER_TYPE_CLIENT_ADMIN = 'client_admin_user'
    USER_TYPE_MASTER_AGENT = 'master_agent'
    USER_TYPE_AGENT = 'agent'
    USER_TYPE_VIEW_ONLY_USER = 'normal_user'

    USER_TYPE_CHOICES = (
        (USER_TYPE_SUPER_ADMIN, 'Display Sweet Super Admin'),
        (USER_TYPE_ADMIN, 'Display Sweet Admin'),
        (USER_TYPE_CLIENT_ADMIN, 'Client Admin'),
        (USER_TYPE_MASTER_AGENT, 'Master Agent'),
        (USER_TYPE_AGENT, 'Agent'),
        (USER_TYPE_VIEW_ONLY_USER, 'View Only User'),
    )

    CLIENT_ADMIN_USER_TYPE_CHOICES = (
        (USER_TYPE_MASTER_AGENT, 'Master Agent'),
        (USER_TYPE_AGENT, 'Agent'),
        (USER_TYPE_VIEW_ONLY_USER, 'View Only User'),
    )

    CLIENT_ADMIN_CHOICES = [USER_TYPE_MASTER_AGENT, USER_TYPE_AGENT, USER_TYPE_VIEW_ONLY_USER]

    MASTER_AGENT_USER_TYPE_CHOICES = (
        (USER_TYPE_AGENT, 'Agent'),
        (USER_TYPE_VIEW_ONLY_USER, 'View Only User'),
    )

    MASTER_AGENT_CHOICES = [USER_TYPE_AGENT, USER_TYPE_VIEW_ONLY_USER]

    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)

    username = models.CharField('username', max_length=255, unique=True,
        help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.',
        validators=[validators.RegexValidator(r'^[\w.@+-]+$', 'Enter a valid username.', 'invalid')])

    #this is terrible but we have to sync details between apps inc password then send password info via an email :|
    temporary_setup_password = models.CharField(max_length=50, null=True, blank=True)

    title = models.CharField('title', choices=TITLE_OPTIONS, max_length=30, null=True, blank=True, help_text="Enter your title.")
    first_name = models.CharField('first name', max_length=30, null=True, blank=True, help_text="Enter your first name.")
    last_name = models.CharField('last name', max_length=30, null=True, blank=True, help_text="Enter your last name.")
    email = models.EmailField('email address', max_length=255, unique=True, help_text="Enter your email address.")
    phone_number = models.CharField(max_length=255, null=True, blank=True, help_text="Please enter a valid 10 digit phone number. "
                                                                                     "Please include area code i.e 03.")
    date_of_birth = models.DateField(null=True, blank=True, help_text="Date of birth")
    
    company_name = models.CharField(max_length=50, null=True, blank=True, help_text="Enter your company name.")
    website = models.CharField(max_length=50, null=True, blank=True, help_text="Enter your website.")
    
    user_type = models.CharField(max_length=50, choices=USER_TYPE_CHOICES, default=USER_TYPE_VIEW_ONLY_USER)
    is_active = models.BooleanField(default=True)
    can_access_django_backend = models.BooleanField(default=False)

    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=APPROVED)
    projects = models.ManyToManyField(Project, blank=True, related_name="user_projects")

    created_by = models.ForeignKey("User", null=True, blank=True, related_name="user_created_by")
    owned_by = models.ForeignKey("User", null=True, blank=True, related_name="user_owned_by")
    last_modified_by = models.ForeignKey("User", null=True, blank=True, related_name="user_modified_by")

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    def __unicode__(self):
        return self.get_full_name()

    def delete(self, *args, **kwargs):
        new_sync = ProjectCMSSync()

        if self.is_master_agent:
            created_users = User.objects.filter(created_by=self, user_type=User.USER_TYPE_AGENT)

            for created_user in created_users:
                projects = created_user.project_users_list.all()
                for project in projects:
                    new_sync.remove_user_access_from_sync(project.pk, project.name, created_user.email)

                    UserModifiedLog.objects.create_log(created_user, None,
                                                       "Removed user access to project %s" % project.name)

                created_user.delete()

            projects = self.viewable_projects
            for project in projects:
                new_sync.remove_user_access_from_sync(project.pk, project.name, self.email)

                UserModifiedLog.objects.create_log(self, None,
                                                   "Removed user access to project %s" % project.name)

        super(User, self).delete(*args, **kwargs)

    def has_perm(self, perm, obj=None):
        "Django admin function. Does the user have a specific permission?"
        return self.can_access_django_backend

    def has_module_perms(self, app_label):
        "Django admin function. Does the user have permissions to view the app `app_label`?"
        return self.can_access_django_backend

    @property
    def is_staff(self):
        return self.can_access_django_backend

    @property
    def property_list(self):
        return self.project_users_list.filter(active=True)

    @property
    def is_site_administrator(self):
        return self.user_type == self.USER_TYPE_SUPER_ADMIN

    @property
    def is_normal_user(self):
        return self.user_type == self.USER_TYPE_VIEW_ONLY_USER

    @property
    def is_admin_user(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN, self.USER_TYPE_ADMIN]

    @property
    def is_administrator(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN, self.USER_TYPE_ADMIN]

    @property
    def is_super_admin_user(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN, self.USER_TYPE_ADMIN]

    @property
    def is_master_agent(self):
        return self.user_type == self.USER_TYPE_MASTER_AGENT

    @property
    def is_super_admin(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN, self.USER_TYPE_ADMIN]

    @property
    def is_ds_admin(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_ADMIN]

    @property
    def is_project_user(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT,
                                  self.USER_TYPE_AGENT,
                                  self.USER_TYPE_VIEW_ONLY_USER]

    @property
    def is_low_access_project_user(self):
        return self.user_type in [self.USER_TYPE_MASTER_AGENT,
                                  self.USER_TYPE_AGENT,
                                  self.USER_TYPE_VIEW_ONLY_USER]

    @property
    def is_allocations_only_user(self):
        return self.user_type in self.CLIENT_ADMIN_CHOICES

    @property
    def is_client_admin(self):
        return self.user_type == self.USER_TYPE_CLIENT_ADMIN

    @property
    def is_agent(self):
        return self.user_type == self.USER_TYPE_AGENT

    @property
    def is_view_only_user(self):
        return self.user_type == self.USER_TYPE_VIEW_ONLY_USER

    @property
    def active_nice_display(self):
        if self.is_active:
            return '<i class="fa fa-check text-success"></i>'
        else:
            return '<i class="fa fa-close text-danger"></i>'

    @property
    def viewable_users(self):

        if self.is_ds_admin:
            return User.objects.all()

        if self.is_client_admin:
            return User.objects.filter(project_users_list=self.viewable_projects)\
                               .filter(Q(created_by=self) | Q(user_type__in=[User.USER_TYPE_CLIENT_ADMIN,
                                                                             User.USER_TYPE_MASTER_AGENT])).distinct()

        if self.is_master_agent:
            return User.objects.filter(project_users_list=self.viewable_projects) \
                               .filter(created_by=self).filter(user_type__in=[User.USER_TYPE_AGENT,
                                                                              User.USER_TYPE_VIEW_ONLY_USER]).distinct()

    def viewable_groups(self, request, project):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)

        if request.user.is_client_admin or request.user.is_ds_admin:
            model_mixin.model_object("AllocationGroups").all()

        allocated_groups = model_mixin.model_object("AllocatedUsers").filter(user_id=self.pk).values_list(
            'allocation_group_id', flat=True)

        return model_mixin.model_object("AllocationGroups").filter(pk__in=allocated_groups)

    def viewable_properties(self, request, project):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)

        if request.user.is_client_admin or request.user.is_ds_admin:
            return model_mixin.model_object("Properties").all()

        allocated_group_ids = self.viewable_groups(request, project).values_list('pk', flat=True)

        allocated_properties = model_mixin.model_object("PropertyAllocations").filter(allocation_group_id__in=allocated_group_ids)\
            .values_list('property_id', flat=True)

        return model_mixin.model_object("Properties").filter(pk__in=allocated_properties)

    def viewable_properties_in_group(self, request, project, group_id):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)

        if group_id in self.viewable_groups(request, project):
            allocated_properties = model_mixin.model_object("PropertyAllocations").filter(
                allocation_group_id=group_id) \
                .values_list('property_id', flat=True)

            return model_mixin.model_object("Properties").filter(pk__in=allocated_properties)

    def viewable_floors(self, request, project, building_id=None):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)

        if building_id:
            return model_mixin.model_object("FloorsBuildings").filter(building_id=building_id).order_by('floor__orderidx').values_list('floor', flat=True)

        if not building_id:
            return self.viewable_properties(request, project).order_by('floor__orderidx').values_list('floor', flat=True)

    def viewable_buildings(self, request, project):
        model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)
        floors = self.viewable_floors(request, project).values_list('pk', flat=True)
        return model_mixin.model_object("FloorsBuildings").filter(pk__in=floors).order_by('floor__orderidx')

    @property
    def viewable_projects(self):

        if self.is_ds_admin:
            return self.project_users_list.filter(active=True)

        if self.is_client_admin:
            return self.project_users_list.filter(active=True, status__in=[Project.STAGING, Project.LIVE])

        return self.project_users_list.filter(active=True, status=Project.LIVE)

    @property
    def get_created(self):
        if self.created:
            return self.created
        else:
            if self.user_modified_log.all():
                return self.user_modified_log.all().order_by('created')[0].created
        return "n/a"

    def has_app_access(self, project):
        if settings.SYNC_CMS or settings.DEVELOPMENT_SUITE:
            user = self
            try:
                new_sync = ProjectCMSSync()
                try:
                    user_exists = new_sync.check_user_project_access_exists(project.name, user.email)
                    if user_exists:
                        return True

                    return False
                except Exception as e:
                    return False
            except:
                return False

        return False

    def get_accredited_by_person(self):
        if self.is_super_admin_user and not self.added_by:
            return self
        elif self.is_super_admin_user and self.added_by:
            return self.added_by
        else:
            if self.company_details and self.company_details.added_by:
                return self.company_details.added_by

    @property
    def current_auth_token(self):
        try:
            token = AuthToken.objects.get(user=self)
        except AuthToken.DoesNotExist:
            return None

        auth_token_lifespan_seconds = getattr(settings, 'AUTH_TOKEN_LIFESPAN_SECONDS')

        cutoff_time = datetime.datetime.now() - datetime.timedelta(seconds=auth_token_lifespan_seconds)

        if token.created < cutoff_time:
            token.delete()
            return None
        else:
            return token

    @property
    def name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def get_absolute_url(self):
        return reverse('users-view', args=[self.pk])

    def create_auth_token(self):
        try:
            token = self.auth_token
            token.delete()
        except AuthToken.DoesNotExist:
            pass

        token = AuthToken.objects.create(user=self)

        return token

    def clear_auth_token(self):
        try:
            token = self.auth_token
            token.delete()
        except AuthToken.DoesNotExist:
            pass

        return

    def get_full_name(self):
        if self.first_name and self.last_name:
            return u'%s %s' % (self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        else:
            return self.username

    def get_short_name(self):
        if self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        else:
            return self.username

    def update_password(self, password):
        self.set_password(password)
        self.save()

    @property
    def is_view_only_user(self):
        return self.user_type == self.USER_TYPE_VIEW_ONLY_USER

    def can_edit_reservation(self):
        return self.user_type in [self.USER_TYPE_SUPER_ADMIN, self.USER_TYPE_CLIENT_ADMIN, self.USER_TYPE_MASTER_AGENT]

    def send_setup_email(self, project=None):
        template = get_template('emails/email_text.html')
        txt_template = get_template('emails/email_text.txt')

        account_setup_template = UserEmailTemplate.objects.filter(template_type=UserEmailTemplate.ACCOUNT_SETUP_TEMPLATE)

        if account_setup_template:
            account_setup_template = account_setup_template[0]
            email_text = account_setup_template.template_text

            login_key = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16))
            created_email = UserAccountEmails(user=self, login_key=login_key, template_used=account_setup_template)
            created_email.save()

            # replace:
            # *|PROJECT_NAME|*
            # *|USERNAME|*
            # *|PASSWORD|*

            email_text = str(email_text).replace("*|NAME|*", '%s' % self.get_full_name(), 10)
            if project:
                email_text = str(email_text).replace("*|PROJECT_NAME|*", '%s' % project.name, 10)
            else:
                email_text = str(email_text).replace("*|PROJECT_NAME|*", "", 10)

            try:
                email_text = str(email_text).replace("*|PASSWORD|*", str(self.temporary_setup_password))
            except Exception as e:
                print(e)

            email_text = str(email_text).replace("*|USERNAME|*", '%s' % self.username, 10)

            login_string = '%s/account/setup/?activation_key=%s' % (settings.FILE_URL, login_key)
            login_string = login_string.replace("//", "/")

            email_text = str(email_text).replace("*|LOGIN_LINK|*", login_string)

            download_string = '<a target="_blank" href="http://www.displaysweet.com/#!download/fvvb3">click here</a>'
            email_text = str(email_text).replace("click here", download_string)

            context = Context({'STATIC_URL': settings.STATIC_URL,
                               'email_content': email_text,
                               'project': project,
                               'base_url': settings.FILE_URL})

            html_content = template.render(context)
            txt_content = txt_template.render(context)

            subject = str(account_setup_template.template_subject).replace("*|NAME|*", '%s' % self.first_name)

            if project:
                subject = subject.replace("*|PROJECT_NAME|*", '%s' % project.name, 10)
            else:
                subject = subject.replace("*|PROJECT_NAME|*", "", 10)

            created_email.email_subject = subject
            created_email.email_text = email_text
            created_email.save()

            values = {'agent_email': self.email,
                      'agent_first_name': self.first_name,
                      'agent_last_name': self.last_name,
                      'agent_phone': self.phone_number,
                      'email_send_to': self.email,
                      'send_to_agent': 1,
                      'send_to_client': 1,
                      'send_to_developer': 1,
                      'user_id': 1}

            if project:
                values.update({'cms_project_id': project.pk})

            try:
                send_smpt_email(txt_content, html_content, subject, self.email)
            except Exception as e:
                print ('Error: ', e)

    def send_added_to_project_email(self, project):

        print ('send_added_to_project_email')

        template = get_template('emails/email_text.html')
        txt_template = get_template('emails/email_text.txt')

        account_setup_template = UserEmailTemplate.objects.filter(
            template_type=UserEmailTemplate.PROJECT_ADD_TEMPLATE)

        if account_setup_template:
            account_setup_template = account_setup_template[0]
            email_text = account_setup_template.template_text

            login_key = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16))
            created_email = UserAccountEmails(user=self, login_key=login_key, template_used=account_setup_template)
            created_email.save()

            # replace:
            # *|PROJECT_NAME|*
            # *|USERNAME|*
            # *|PASSWORD|*

            email_text = str(email_text).replace("*|NAME|*", '%s' % self.get_full_name(), 10)
            if project:
                email_text = str(email_text).replace("*|PROJECT_NAME|*", '%s' % project.name, 10)
            else:
                email_text = str(email_text).replace("*|PROJECT_NAME|*", "", 10)

            try:
                email_text = str(email_text).replace("*|PASSWORD|*", str(self.temporary_setup_password))
            except Exception as e:
                print(e)

            email_text = str(email_text).replace("*|USERNAME|*", '%s' % self.username, 10)

            login_string = '%s/account/setup/?activation_key=%s' % (settings.FILE_URL, login_key)
            login_string = login_string.replace("//", "/")

            email_text = str(email_text).replace("*|LOGIN_LINK|*", login_string)

            download_string = '<a target="_blank" href="http://www.displaysweet.com/#!download/fvvb3">click here</a>'
            email_text = str(email_text).replace("click here", download_string)

            email_text = email_text.replace("http://localhost:8000/localhost:8000/", "http://localhost:8000/")
            email_text = email_text.replace("http://cmsstaging.displaysweet.com/cmsstaging.displaysweet.com/",
                                            "http://cmsstaging.displaysweet.com/")
            email_text = email_text.replace("http:/cmsstaging.displaysweet.com/", "http://cmsstaging.displaysweet.com/")

            email_text = email_text.replace("http://admin.displaysweet.com/admin.displaysweet.com/",
                                            "http://admin.displaysweet.com/")
            email_text = email_text.replace("http:/admin.displaysweet.com/", "http://admin.displaysweet.com/")

            context = Context({'STATIC_URL': settings.STATIC_URL,
                               'email_content': email_text,
                               'project': project,
                               'base_url': settings.FILE_URL})

            html_content = template.render(context)
            txt_content = txt_template.render(context)

            subject = str(account_setup_template.template_subject).replace("*|NAME|*", '%s' % self.first_name)

            if project:
                subject = subject.replace("*|PROJECT_NAME|*", '%s' % project.name, 10)
            else:
                subject = subject.replace("*|PROJECT_NAME|*", "", 10)

            created_email.email_subject = subject
            created_email.email_text = email_text
            created_email.save()

            values = {'agent_email': self.email,
                      'agent_first_name': self.first_name,
                      'agent_last_name': self.last_name,
                      'agent_phone': self.phone_number,
                      'email_send_to': self.email,
                      'send_to_agent': 1,
                      'send_to_client': 1,
                      'send_to_developer': 1,
                      'user_id': 1}

            if project:
                values.update({'cms_project_id': project.pk})

            print ('At account smpt attempt: ')

            try:
                send_smpt_email(txt_content, html_content, subject, self.email)
            except Exception as e:
                print ('Error: ', e)

    def send_password_email(self, project=None):

        template = get_template('emails/email_text.html')
        txt_template = get_template('emails/email_text.txt')

        account_setup_template = UserEmailTemplate.objects.filter(
            template_type=UserEmailTemplate.PASSWORD_TEMPLATE)

        if account_setup_template:
            try:
                account_setup_template = account_setup_template[0]
                email_text = account_setup_template.template_text

                login_key = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16))
                created_email = UserAccountEmails(user=self, login_key=login_key, template_used=account_setup_template)
                created_email.save()

                email_text = str(email_text).replace("*|NAME|*", '%s' % self.first_name).replace("*|NAME|*", '%s' %
                                                                                                 self.first_name).replace(
                    "*|NAME|*", '%s' % self.first_name)

                login_string = '%s/account/setup/?activation_key=%s' % (settings.FILE_URL, login_key)
                login_string = login_string.replace("//", "/")

                email_text = str(email_text).replace("*|LOGIN_LINK|*", login_string)

                email_text = str(email_text).replace("*|PASSWORD_LINK|*", login_string)

                email_text = email_text.replace("http://localhost:8000/localhost:8000/", "http://localhost:8000/")
                email_text = email_text.replace("http://cmsstaging.displaysweet.com/cmsstaging.displaysweet.com/", "http://cmsstaging.displaysweet.com/")
                email_text = email_text.replace("http:/cmsstaging.displaysweet.com/", "http://cmsstaging.displaysweet.com/")

                email_text = email_text.replace("https://admin.displaysweet.com/admin.displaysweet.com/", "https://admin.displaysweet.com/")
                email_text = email_text.replace("https:/admin.displaysweet.com/", "https://admin.displaysweet.com/")

                context = Context({'STATIC_URL': settings.STATIC_URL,
                                   'email_content': email_text,
                                   'project': project,
                                   'base_url': settings.FILE_URL})

                html_content = template.render(context)

                txt_content = txt_template.render(context)

                subject = str(account_setup_template.template_subject).replace("*|NAME|*", '%s' % self.first_name)

                created_email.email_subject = subject
                created_email.email_text = email_text
                created_email.save()
            except Exception as e:
                print ('Error: ', e)

            # kwargs = dict(
            #     to=[self.email, ],
            #     from_email=settings.EMAIL_FROM_ADDRESS,
            #     subject=subject,
            #     body=txt_content,
            #     alternatives=((html_content, 'text/html'),)
            # )

            print ('At smpt attempt: ')
            try:
                send_smpt_email(txt_content, html_content, subject, self.email)
            except Exception as e:
                print ('Error: ', e)

            #other attempt:
            # message = EmailMultiAlternatives(**kwargs)
            # message.send(fail_silently=True)


class Purchaser(models.Model):

    MR = 'Mr'
    MS = 'Ms'
    MRS = 'Mrs'
    DR = 'Dr'
    MISS = 'Miss'

    TITLE_OPTIONS = (
        (MS, 'Ms'),
        (MR, 'Mr'),
        (MRS, 'Mrs'),
        (MISS, 'Miss'),
        (DR, 'Dr'),
    )

    PRE_APPROVED = 'pending_registration'
    SUSPENDED = 'suspended'
    APPROVED = 'approved'

    STATUS_CHOICES = (
        (PRE_APPROVED, 'Pending Approval'),
        (SUSPENDED, 'Suspended'),
        (APPROVED, 'Approved'),
    )
    
    BUYER_TYPE_CHOICES = (
        ('resident', 'Resident'),        
    )
    
    OCCUPY_CHOICES = (
        ('owner_occupier', 'Owner Occupier'),        
    )
    
    LOCATION_CHOICES = (
        ('local', 'Local'),
        ('overseas', 'Overseas'),
    )
    
    YES_NO_CHOICES = (
        ('yes', 'Yes'),
        ('no', 'No'),
    )

    SALES_TYPE_CHOICES = (
        ('weekly', 'Weekly Amount'),
        ('percentage', 'Percentage'),
    )

    USER_TYPE_PURCHASE_USER = 'purchase_user'

    USER_TYPE_CHOICES = (        
        (USER_TYPE_PURCHASE_USER, 'Purchase User'),
    )

    title = models.CharField('title', choices=TITLE_OPTIONS, max_length=30, null=True, blank=True)
    first_name = models.CharField('first name', max_length=30, null=True, blank=True)
    last_name = models.CharField('last name', max_length=30, null=True, blank=True)
    email = models.EmailField('email address', max_length=255)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)

    user_type = models.CharField(max_length=50, choices=USER_TYPE_CHOICES, default=USER_TYPE_PURCHASE_USER)
    is_active = models.BooleanField(default=True)
    can_access_django_backend = models.BooleanField(default=False)

    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=APPROVED)
    projects = models.ManyToManyField(Project, blank=True, related_name="purchaser_projecs")

    property_id = models.IntegerField()
    agent = models.ForeignKey(User, null=True, blank=True, related_name='property_agent')

    buyer_type = models.CharField(max_length=50, choices=BUYER_TYPE_CHOICES,  default="resident")
    company_name = models.CharField(max_length=50, null=True, blank=True)
    website = models.CharField(max_length=50, null=True, blank=True)
    occupy_status = models.CharField(max_length=50, choices=OCCUPY_CHOICES, default="owner_occupier")
    purchaser_location = models.CharField(max_length=50, choices=LOCATION_CHOICES, default="local" )
    firb = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")
    nras = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")
    enquiry_source = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    #Summary Fields
    agent_bonus = models.CharField(max_length=50, null=True, blank=True)
    rebate = models.CharField(max_length=50, null=True, blank=True)
    rental_guarantee = models.CharField(max_length=50, null=True, blank=True)

    #Rental Fields
    sales_price = models.CharField(max_length=50, null=True, blank=True)
    sales_type = models.CharField(max_length=50, default="weekly", choices=SALES_TYPE_CHOICES)
    weekly_amount = models.CharField(max_length=50, null=True, blank=True)
    percentage_amount = models.CharField(max_length=50, null=True, blank=True)
    number_of_weeks = models.CharField(max_length=50, null=True, blank=True)
    max_cost = models.CharField(max_length=50, null=True, blank=True)

    #Leasing Details
    weeks_leased = models.CharField(max_length=50, null=True, blank=True)
    leased_amount = models.CharField(max_length=50, null=True, blank=True)
    recouped = models.CharField(max_length=50, null=True, blank=True)
    current_position = models.CharField(max_length=50, null=True, blank=True)

    #Contract Info
    reservation_fee_date = models.DateTimeField(null=True, blank=True)
    reservation_fee_refund = models.BooleanField(default=False)
    contract_exchange_date = models.DateTimeField(null=True, blank=True)
    cancelled = models.BooleanField(default=False)
    deposit_date = models.DateTimeField(null=True, blank=True)
    deposit_refund = models.BooleanField(default=False)

    def __unicode__(self):
        return self.get_full_name()

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def is_staff(self):
        return False

    @property
    def is_site_administrator(self):
        return False

    @property
    def is_admin_user(self):
        return False

    @property
    def is_administrator(self):
        return False

    @property
    def is_super_admin_user(self):
        return False

    @property
    def is_super_admin(self):
        return False

    def send_property_reservation_email(self, user_id, project_id, property_name=None, property_type=None):
        print('in send_property_reservation_email')
        try:
            values = {'agent_email': self.agent.email,
                      'agent_first_name': self.agent.first_name,
                      'agent_last_name': self.agent.last_name,
                      'agent_phone': self.agent.phone_number,
                      'agent_company': self.agent.company_name,
                      'email_send_to': self.agent.email,
                      'buyer_email': self.email,
                      'buyer_first_name': self.first_name,
                      'buyer_last_name': self.last_name,
                      'buyer_address': '',
                      'buyer_city': '',
                      'buyer_phone': self.phone_number,
                      'buyer_country': '',
                      'buyer_enquiry_source': self.get_enquiry_source_display(),
                      'buyer_postcode': '',
                      'buyer_state': '',
                      'firb_status': self.get_firb_display(),
                      'property_name': property_name,
                      'property_type': property_type,
                      'send_to_agent': 1,
                      'send_to_client': 1,
                      'send_to_developer': 1,
                      'user_id': user_id,
                      'cms_project_id': project_id,
                      'apartment_id': self.property_id}

            data = urllib.parse.urlencode(values)
            full_url = 'http://live.displaysweet.com/email2/sendEmail.php?' + data

            #circular import made me put this here
            from core.models import ProjectSyncBackLog
            ProjectSyncBackLog.objects.create(direction=0, project_id=project_id, log=full_url)

            print ('reservation full_url: ')
            print (full_url)
            print ('----------')

            try:
                urllib.request.urlopen(full_url)
            except Exception as e:
                print('Error submitting registration email to live: ', e)

        except Exception as e:
            print('err', e)


class PropertyPurchaseFiles(models.Model):

    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    attached_file = models.FileField(null=True, blank=True)
    attached_by = models.ForeignKey("User")

    purchase = models.ForeignKey(Purchaser)

    def __unicode__(self):
        if self.attached_file:
            return self.attached_file.name


class PropertyPurchaseComments(models.Model):

    COMMENT_STATUS_CHOICES = (
        ('no_action', 'No action required'),
        ('action_required', 'Action required'),
        ('pending_approval', 'Action pending approval'),
        ('action_approved', 'Action approved'),
        ('action_not_approved', 'Action rejected'),
        ('action_completed', 'Action completed'),
    )

    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    comment = models.TextField(null=True, blank=True)
    comment_by = models.ForeignKey("User", related_name="property_comment_by")

    purchase = models.ForeignKey(Purchaser)

    comment_status = models.CharField(max_length=100, default="no_action")

    action = models.TextField(null=True, blank=True)
    action_required_user = models.ForeignKey("User", related_name="action_comment_by")
    action_required_by = models.DateTimeField(null=True, blank=True)
    action_user_notified = models.BooleanField(default=False)

    action_is_approved = models.BooleanField(default=True)
    approved_by = models.ForeignKey("User", null=True, blank=True, related_name="action_approved_by_user")
    date_approved = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return self.pk


class UserToDoList(models.Model):

    STATUS_CHOICES = (
        ('action_required', 'Action required'),
        ('pending_approval', 'Action pending approval'),
        ('action_approved', 'Action Approved'),
        ('action_completed', 'Action completed'),
        ('follow_up', 'Follow up'),
    )

    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    user = models.ForeignKey("User", related_name="to_do_user")

    comment = models.TextField(null=True, blank=True)
    purchase = models.ForeignKey(Purchaser, null=True, blank=True)

    todo_status = models.CharField(max_length=100, default="action_required")

    to_do_action = models.TextField(null=True, blank=True)
    assign_to_do_action = models.ForeignKey("User", related_name="to_do_comment_by")

    action_required_by = models.DateTimeField(null=True, blank=True)
    action_user_notified = models.BooleanField(default=False)

    todo_is_approved = models.BooleanField(default=True)
    approved_by = models.ForeignKey("User", null=True, blank=True, related_name="to_do_approved_by_user")
    date_approved = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return self.pk


class UserAccountEmails(models.Model):

    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True, auto_now_add=True)

    user = models.ForeignKey(User, null=True, blank=True)
    login_key = models.CharField(max_length=50, blank=True, null=True)
    active = models.BooleanField(default=True)
    template_used = models.ForeignKey("UserEmailTemplate", null=True, blank=True)

    email_subject = models.CharField(max_length=150, blank=True, null=True)
    email_text = models.TextField(blank=True, null=True)
    email_image = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return '%s' % self.email_subject

    class Meta:
        verbose_name_plural = 'User account emails'

    def view_preview_of_email(self):
        return '<a href="/email/account/%s/" target="_blank">View Preview of Email</a>' % self.pk
    view_preview_of_email.allow_tags = True


class UserEmailTemplate(models.Model):

    ACCOUNT_SETUP_TEMPLATE = "account_setup"
    PASSWORD_TEMPLATE = "account_password"
    PROJECT_ADD_TEMPLATE = "added_to_project"

    TEMPLATE_TYPE_CHOICES = (
        (ACCOUNT_SETUP_TEMPLATE, "Account Setup Email"),
        (PASSWORD_TEMPLATE, "Account Password Email"),
        (PROJECT_ADD_TEMPLATE, "Added to Project Email")
    )

    created = models.DateTimeField(null=True, blank=True, auto_created=True)
    modified = models.DateTimeField(null=True, blank=True, auto_created=True)

    template_subject = models.CharField(max_length=100, null=True, blank=True)
    template_text = models.TextField()

    active = models.BooleanField(default=True)

    template_type = models.CharField(max_length=50, choices=TEMPLATE_TYPE_CHOICES, default="account_setup")

    def __unicode__(self):
        return '%s' % self.template_type

    def view_preview_of_email(self):
        return '<a href="/email/%s/" target="_blank">View Preview of Email</a>' % self.pk
    view_preview_of_email.allow_tags = True

    def send_admin_newsletter(self):
        template = get_template('newsletters/email_newsletter.html')
        txt_template = get_template('newsletters/email_newsletter.txt')

        #To Admin Accounts
        admin_users = User.objects.filter(status=User.APPROVED)
        for user in admin_users:
            email_text = str(self.email_text).replace("*|NAME|*", '%s' % user.first_name).replace("*|NAME|*", '%s' % user.first_name).replace("*|NAME|*", '%s' % user.first_name)

            context = Context({'STATIC_URL': settings.STATIC_URL,
                                'email_content': str(email_text),
                                'base_url': settings.FILE_URL})

            html_content = template.render(context)
            txt_content = txt_template.render(context)

            subject = str(self.email_subject).replace("*|NAME|*", '%s' % user.first_name)

            kwargs = dict(
                to=[user.email,],
                from_email=settings.EMAIL_FROM_ADDRESS,
                subject=subject,
                body=txt_content,
                alternatives=((html_content, 'text/html'),)
            )

            message = EmailMultiAlternatives(**kwargs)
            message.send(fail_silently=True)


class UserImports(models.Model):

    created = models.DateTimeField(null=True, blank=True, auto_created=True)
    modified = models.DateTimeField(null=True, blank=True, auto_created=True)

    # created_by = models.ForeignKey(User, null=True, blank=True)
    user_file = models.FileField(upload_to="user_imports", null=True, blank=True)

    imported = models.BooleanField(default=False)
    date_imported = models.DateTimeField(null=True, blank=True, auto_created=True)

    def __unicode__(self):
        return '%s' % self.pk


class UserModifiedLogManager(models.Manager):

    def create_log(self, modified_user, modified_by, log_script):
        log = self.create(user_modified=modified_user, user_modified_by=modified_by, log=log_script)
        return log


class UserModifiedLog(models.Model):

    created = models.DateTimeField(null=True, blank=True, auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True, auto_created=True, auto_now_add=True)

    user_modified = models.ForeignKey("User", null=True, blank=True, related_name="user_modified_log")
    user_modified_by = models.ForeignKey("User", null=True, blank=True, related_name="user_modified_by_log")

    log = models.TextField(null=True, blank=True)

    objects = UserModifiedLogManager()

    def __unicode__(self):
        return '%s' % self.pk