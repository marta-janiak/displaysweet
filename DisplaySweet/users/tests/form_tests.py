from django.test import TestCase
from users.forms import UserImportsForm
from core.models import Project
from os import listdir
from django.core.files.uploadedfile import SimpleUploadedFile



class UserFormTestCase(TestCase):

    fixtures = ['project_testdata.json']

    def setUp(self):
        self.project_1 = Project.objects.get(pk=2)
        self.csv_files = [f for f in listdir("users/tests/user_import_files/")]

    def test_user_import_file_form(self):

        projects = Project.objects.filter(active=True)

        self.assertIsNotNone(projects)

        # if projects:
        #     project = projects[0]
        #
        #     for file in self.csv_files:
        #         upload_file = open(file, 'rb')
        #
        #         post_dict = {'project': project}
        #         file_dict = {'user_file': SimpleUploadedFile(upload_file.name, upload_file.read())}
        #
        #         form = UserImportsForm(post_dict, file_dict)
        #         # self.assertEqual(form.is_valid(), True)
        #
        #         self.assertEqual(form.errors, 'err')