from users.models import User
from django.core.urlresolvers import reverse
from django.test.client import Client
from django.test import TestCase


class UserTestCase(TestCase):
    fixtures = ['user_testdata.json']

    def setUp(self):
        super(UserTestCase, self).setUp()
        self.client = Client()

    def test_create_with_valid_attributes(self):
        self.assertTrue(User.objects.create_user('johnlennon', 'John', 'Lennon', 'lennon@thebeatles.com',
                                                 password='johnpassword'))

    def test_create_with_empty_email(self):
        try:
            User.objects.create_user('johnlennon', 'john', 'lennon', '')
        except ValueError as e:
            self.assertEqual(e.args, ('Users must have an email address',))

    def test_create_superuser(self):
        super_user = User.objects.create_superuser('ringostarr', 'Ringo', 'Starr',
                                                   'ringo@thebeatles.com', 'ringostarr!')
        self.assertTrue(super_user)
        self.assertTrue(super_user.is_staff)
        self.assertTrue(super_user.is_site_administrator)
        self.assertTrue(super_user.is_ds_admin)
        self.assertFalse(super_user.is_project_user)




