from django import forms
from users.models import User, UserAccountEmails, UserEmailTemplate, UserImports
from core.models import Project
from tinymce.widgets import TinyMCE
from projects.cms_sync import ProjectCMSSync
import uuid
from users.models import UserModifiedLog


class UserImportsForm(forms.ModelForm):

    project = forms.ModelChoiceField(queryset=Project.objects.filter(active=True).order_by('name'),
                                     widget=forms.Select(attrs={'style': 'width: 250px;'}))

    class Meta:
        model = UserImports
        fields = ('user_file', )

    def __init__(self, *args, **kwargs):
        super(UserImportsForm, self).__init__(*args, **kwargs)

        self.fields['user_file'].label = 'Upload new file of users'


class UserCreateForm(forms.ModelForm):

    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={"placeholder": "Enter a username"}), help_text="Enter a username")
    first_name = forms.CharField(label="First Name", widget=forms.TextInput(attrs={"placeholder": "Enter a First Name"}), help_text="Enter your first name")
    last_name = forms.CharField(label="Last Name", widget=forms.TextInput(attrs={"placeholder": "Enter a Last Name"}), help_text="Enter your last name")
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={"placeholder": "Email Address"}), help_text="Enter an email address")

    password1 = forms.CharField(label="Password", widget=forms.HiddenInput)
    password2 = forms.CharField(label="Password confirmation", widget=forms.HiddenInput)
    temporary_setup_password = forms.CharField(widget=forms.HiddenInput, required=False)

    send_setup_email = forms.BooleanField(label="Send user account setup email?", required=False)

    class Meta:
        model = User
        fields = ("username",
                  "first_name",
                  "last_name",
                  "email",
                  "user_type",
                  "temporary_setup_password"
                  )

    def __init__(self, request, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)

        self.fields["first_name"].required = True
        self.fields["last_name"].required = True

        uid = uuid.uuid4()
        initial_random_password = uid.hex[:8]
        self.fields["password1"].initial = initial_random_password
        self.fields["password2"].initial = initial_random_password
        self.fields["temporary_setup_password"].initial = initial_random_password

        if request.user.is_client_admin:
            self.fields["user_type"].choices = User.CLIENT_ADMIN_USER_TYPE_CHOICES

        if request.user.is_master_agent:
            self.fields["user_type"].choices = User.MASTER_AGENT_USER_TYPE_CHOICES

    def clean_password(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        
        if not password2 == password1:
            self._errors["password1"] = self.error_class(
                ["The passwords you have entered do not match"])
            raise forms.ValidationError("The passwords you have entered do not match")

        min_length = 5

        # At least MIN_LENGTH long
        if min_length and len(password1) < min_length:
            self._errors["new_password1"] = self.error_class(["The new password must be at least %d characters long." % min_length])
            raise forms.ValidationError("The new password must be at least %d characters long." % min_length)

        return password1

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)

        project = Project.objects.get(name='Master')

        new_sync = ProjectCMSSync()
        new_sync.update_password_sync(project.pk, self.cleaned_data["password1"], user.first_name, user.last_name, user.email)
        UserModifiedLog.objects.create_log(user, None,
                                           "Password updated %s" % self.cleaned_data["password1"])

        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserCreateFromAllocationForm(forms.ModelForm):

    username = forms.CharField(label="Username", widget=forms.HiddenInput, required=False)
    first_name = forms.CharField(label="First Name", widget=forms.TextInput(attrs={"placeholder": "Enter a First Name"}))
    last_name = forms.CharField(label="Last Name", widget=forms.TextInput(attrs={"placeholder": "Enter a Last Name"}))
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={"placeholder": "Email Address"}))

    password1 = forms.CharField(label="Password", required=False)
    password2 = forms.CharField(label="Password confirmation", required=False)
    temporary_setup_password = forms.CharField(widget=forms.HiddenInput, required=False)

    user_type = forms.ChoiceField(label="User Type", choices=User.USER_TYPE_CHOICES)
    project = forms.ModelChoiceField(queryset=Project.objects.all(), required=True)

    send_setup_email = forms.BooleanField(label="Send user account setup email?", required=False)

    class Meta:
        model = User
        fields = ("username",
                  "first_name",
                  "last_name",
                  "email",
                  'phone_number',
                  'website',
                  'company_name',
                  "user_type",
                  "temporary_setup_password"
                  )

    def __init__(self, request, project, *args, **kwargs):

        self.request = request


        super(UserCreateFromAllocationForm, self).__init__(*args, **kwargs)

        self.fields["first_name"].required = True
        self.fields["last_name"].required = True

        project_list = request.user.viewable_projects.order_by('name')
        self.fields['project'].initial = project

        if request.user.is_client_admin:
            if project_list.count() == 1:
                self.fields['project'].widget = forms.HiddenInput()
            else:
                self.fields['project'].queryset = project_list

            self.fields["user_type"].choices = User.CLIENT_ADMIN_USER_TYPE_CHOICES

        if request.user.is_master_agent:
            if request.user.project_users_list.all().count() == 1:
                self.fields['project'].widget = forms.HiddenInput()
            else:
                self.fields['project'].queryset = project_list

            self.fields["user_type"].choices = User.MASTER_AGENT_USER_TYPE_CHOICES
            self.fields['password1'].widget = forms.HiddenInput()
            self.fields['password2'].widget = forms.HiddenInput()

        uid = uuid.uuid4()
        initial_random_password = uid.hex[:8]
        self.fields["password1"].initial = initial_random_password
        self.fields["password2"].initial = initial_random_password
        self.fields["temporary_setup_password"].initial = initial_random_password

    def clean(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if not password2 == password1:
            self._errors["password1"] = self.error_class(
                ["The passwords you have entered do not match"])
            raise forms.ValidationError("The passwords you have entered do not match")
        
        cleaned_data = self.cleaned_data

        email = cleaned_data.get('email')
        if not email:
            self._errors["email"] = self.error_class(["This field is required as it will be your login username."])

        self.cleaned_data['username'] = email

        return cleaned_data

    def save(self, commit=True):

        email = self.cleaned_data.get('email')
        project = self.cleaned_data.get('project')

        send_setup_email = self.cleaned_data['send_setup_email']

        check_user = User.objects.filter(email=email)
        if check_user:
            user = check_user[0]

            if send_setup_email:
                user.send_added_to_project_email(project)

            project.users.add(user)
            project.save()

        else:
            user = super(UserCreateFromAllocationForm, self).save(commit=False)
            user.set_password(self.cleaned_data["password1"])
            user.temporary_setup_password = self.cleaned_data["password1"]
            user.save()

            project.users.add(user)
            project.save()

        try:
            new_sync = ProjectCMSSync()
            new_sync.update_password_sync(project.pk, self.cleaned_data["password1"], user.first_name, user.last_name, user.email)
        except Exception as e:
            pass

        UserModifiedLog.objects.create_log(user, None,
                                           "Password updated %s" % self.cleaned_data["password1"])

        if commit:
            user.save()
            if send_setup_email:
                user.send_setup_email(project=project)

        return user


class UserCreateProjectForm(forms.ModelForm):

    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={"placeholder": "Enter a username"}))
    first_name = forms.CharField(label="First Name",
                                 widget=forms.TextInput(attrs={"placeholder": "Enter a First Name"}))
    last_name = forms.CharField(label="Last Name",
                                widget=forms.TextInput(attrs={"placeholder": "Enter a Last Name"}))
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={"placeholder": "Email Address"}))

    password1 = forms.CharField(label="Password", widget=forms.HiddenInput, required=False)
    password2 = forms.CharField(label="Password confirmation", widget=forms.HiddenInput, required=False)
    temporary_setup_password = forms.CharField(widget=forms.HiddenInput, required=False)

    project = forms.ModelChoiceField(queryset=Project.objects.all(), label="Select a project", required=False)

    send_setup_email = forms.BooleanField(label="Send user account setup email?", required=False)

    class Meta:
        model = User
        fields = ("username",
                  "first_name",
                  "last_name",
                  "email",
                  "user_type",
                  "temporary_setup_password"
                  )

    def __init__(self, request, *args, **kwargs):
        super(UserCreateProjectForm, self).__init__(*args, **kwargs)

        self.fields["first_name"].required = True
        self.fields["last_name"].required = True

        uid = uuid.uuid4()
        initial_random_password = uid.hex[:8]
        self.fields["password1"].initial = initial_random_password
        self.fields["password2"].initial = initial_random_password
        self.fields["temporary_setup_password"].initial = initial_random_password

        if request.user.is_client_admin:
            self.fields["user_type"].choices = User.CLIENT_ADMIN_USER_TYPE_CHOICES
            self.fields["project"].queryset = Project.objects.filter(active=True, status=Project.LIVE,
                                                                     users=request.user)
        if request.user.is_master_agent:
            self.fields["user_type"].choices = User.MASTER_AGENT_USER_TYPE_CHOICES
            self.fields["project"].queryset = Project.objects.filter(active=True, status=Project.LIVE, users=request.user)

    def clean(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if not password2 == password1:
            self._errors["password1"] = self.error_class(
                ["The passwords you have entered do not match"])
            raise forms.ValidationError("The passwords you have entered do not match")

        cleaned_data = self.cleaned_data
        
        return cleaned_data


    def save(self, commit=True):
        user = super(UserCreateProjectForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])

        if commit:
            user.temporary_setup_password = self.cleaned_data["password1"]
            user.save()

        project = self.cleaned_data["project"]
        if project:
            project.users.add(user)
            project.save()

        new_sync = ProjectCMSSync()
        try:
            new_sync.add_user_into_sync(project.pk, user.first_name, user.last_name, user.email, user.is_super_admin)
        except Exception as e:
            print(e)

        try:
            new_sync.add_user_access_into_sync(project.pk, project.name, user.email, user.can_edit_reservation(), user.can_edit_reservation())
        except Exception as e:
            print(e)

        return user


class UserEditForm(forms.ModelForm):

    username = forms.CharField(label="Username", required=True, widget=forms.TextInput(attrs={"placeholder": "Enter a username"}), help_text="Enter a username")
    first_name = forms.CharField(label="First Name", required=True, widget=forms.TextInput(attrs={"placeholder": "Enter a First Name"}), help_text="Enter your first name")
    last_name = forms.CharField(label="Last Name", required=True, widget=forms.TextInput(attrs={"placeholder": "Enter a Last Name"}), help_text="Enter your last name")
    email = forms.CharField(label="Email", required=True, widget=forms.TextInput(attrs={"placeholder": "Email Address"}), help_text="Enter an email address")

    class Meta:
        model = User
        fields = ("username",
                  "first_name",
                  "last_name",
                  "email",
                  "user_type"
                  )

    def __init__(self, request, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)

        if request.user.is_client_admin:
            self.fields["user_type"].choices = User.CLIENT_ADMIN_USER_TYPE_CHOICES

        if request.user.is_master_agent:
            self.fields["user_type"].choices = User.MASTER_AGENT_USER_TYPE_CHOICES


class UserAllocationEditForm(forms.ModelForm):


    first_name = forms.CharField(label="First Name", required=True, widget=forms.TextInput(attrs={"placeholder": "Enter a First Name"}), help_text="Enter your first name")
    last_name = forms.CharField(label="Last Name", required=True, widget=forms.TextInput(attrs={"placeholder": "Enter a Last Name"}), help_text="Enter your last name")

    user_type = forms.ChoiceField(label="User Type", choices=User.CLIENT_ADMIN_USER_TYPE_CHOICES)

    class Meta:
        model = User
        fields = ("first_name",
                  "last_name",
                  "user_type"
                  )


class PasswordChangeForm(forms.Form):

    new_password1 = forms.CharField(label="New Password", required=True, widget=forms.PasswordInput(attrs={"placeholder": "Enter a new password"}))
    new_password2 = forms.CharField(label="Confirm New Password", required=True, widget=forms.PasswordInput(attrs={"placeholder": "Confirm the new password"}))

    def clean(self):
        password1 = self.cleaned_data.get("new_password1")
        password2 = self.cleaned_data.get("new_password2")

        if not password2 == password1:
            self._errors["new_password1"] = self.error_class(
                ["The passwords you have entered do not match"])
            raise forms.ValidationError("The passwords you have entered do not match")
        
        cleaned_data = self.cleaned_data
        password1 = cleaned_data.get('new_password1')

        min_length = 5

        # At least MIN_LENGTH long
        if min_length and len(password1) < min_length:
            self._errors["new_password1"] = self.error_class(["The new password must be at least %d characters long." % min_length])
            raise forms.ValidationError("The new password must be at least %d characters long." % min_length)

        return cleaned_data

    def save(self, user):
        password1 = self.cleaned_data.get('new_password1')
        password1 = str(password1).strip()

        user.set_password(password1)
        user.temporary_setup_password = password1
        user.update_password(password1)
        user.save()

        project = Project.objects.get(name='Master')
        new_sync = ProjectCMSSync()
        new_sync.update_password_sync(project.pk, self.cleaned_data["new_password1"], user.first_name, user.last_name, user.email)
        UserModifiedLog.objects.create_log(user, None,
                                           "Password updated %s" % self.cleaned_data["new_password1"])

        return True


class UserActionForm(forms.Form):
    comment = forms.CharField(required=False)


class TemplateEmailForm(forms.ModelForm):
    template_text = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30,  'class': "full-width wysiwyg",}))

    class Meta:
        model = UserEmailTemplate
        fields = ('template_type', 'template_subject', 'template_text', )


class UserEmailForm(forms.ModelForm):
    email_text = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30,  'class': "full-width wysiwyg",}))

    class Meta:
        model = UserAccountEmails
        fields = ('email_subject', 'email_text', 'email_image', 'user', 'login_key', 'template_used', )