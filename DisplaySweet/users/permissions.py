

class UserPermissionsMixin(object):
    """
    All methods relating to permissions go on this mixin!
    """

    def can_be_viewed_in_api_by(self):
        if self.is_administrator:
            return True

    def check_plant_sales_organisation_and_country(self):
        return self in self.plants.filter(sales_organisation=self.sales_organisation, country=self.country)

    def can_be_edited_by(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_be_created_by(self):
        return self.user_type in [self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_be_deleted_by(self):
        return self.can_be_created_by()

    def can_create_allocation_group(self):
        return self.can_create_allocation_group()


class AllocationDetailsPermissionMixin(object):

    def can_be_edited_by(self, requested_user=None):
        if requested_user:
            if requested_user.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                                  self.USER_TYPE_ADMIN,
                                                  self.USER_TYPE_SUPER_ADMIN] \
            and not self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]:
                return True

            if requested_user.user_type in [self.USER_TYPE_ADMIN,
                                            self.USER_TYPE_SUPER_ADMIN]:
                return True

        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_be_created_by(self):
        return self.user_type in [self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_be_deleted_by(self):
        return self.can_be_created_by()

    def can_delete_project(self):
        return self.user_type == self.USER_TYPE_SUPER_ADMIN

    def can_view_user_link(self):
        return self.user_type in [self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_group(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_view_staging(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_property_data(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_status(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_create_project(self):
        return self.user_type in [self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_add_user(self):
        return self.user_type in [self.USER_TYPE_MASTER_AGENT,
                                  self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_user_projects(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_properties(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_view_project_properties_screen(self):
        return True

    def can_view_project_details(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_VIEW_ONLY_USER,
                                  self.USER_TYPE_AGENT]

    def can_view_allocation_admin(self):
        return self.user_type in [self.USER_TYPE_VIEW_ONLY_USER,
                                  self.USER_TYPE_AGENT]

    def can_edit_allocation_admin(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_allocate_properties(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_view_properties(self):
        return True

    def can_create_user(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_assign_user(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_mark_as_exclusive(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_edit_property_status(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_edit_all_fields(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_edit_reservation_modal(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN, 
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_set_to_reserved(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN]

    def can_edit_reservation_status(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN, 
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]

    def can_set_as_excusive(self):
        return self.user_type in [self.USER_TYPE_CLIENT_ADMIN,
                                  self.USER_TYPE_ADMIN,
                                  self.USER_TYPE_SUPER_ADMIN,
                                  self.USER_TYPE_MASTER_AGENT]