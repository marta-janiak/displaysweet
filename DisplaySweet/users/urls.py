from django.conf.urls import patterns, url
from .views import (UserListCallbackView,
                    UserVersionView,
                    UserDeleteView)


urlpatterns = patterns('',
    url(r'^$', 'users.views.user_list_view', name='users-list'),
    url(r'^/user/owner/$', 'users.views.user_update_view', name='users-owner-list'),

    url(r'^list_callback/$', UserListCallbackView.as_view(), name='users-list_callback'),
    url(r'^user/types/$',  'users.views.user_types', name='users-user_types'),
    url(r'^create/$', 'users.views.create_user', name='users-create'),
    url(r'^(?P<pk>\d+)/$', 'users.views.view_user', name='users-view'),
    url(r'^(?P<pk>\d+)/history/(?P<version_idx>\d+)/$', UserVersionView.as_view(), name='users-version'),
    url(r'^(?P<pk>\d+)/update/$', 'users.views.update_user', name='users-update'),
    url(r'^(?P<pk>\d+)/update/password/$', 'users.views.update_user_password', name='users-update_password'),
    url(r'^(?P<pk>\d+)/delete/$', UserDeleteView.as_view(), name='users-delete'),

    url(r'^(?P<pk>\d+)/update/user/account/$', 'users.views.remove_user_from_project', name='users-remove-from-project'),
    url(r'^(?P<pk>\d+)/update/user/projects/$', 'users.views.assign_existing_users', name='users-assign-to-project'),

    url(r'^project/permissions/$',  'users.views.project_permissions', name='users-project_permissions'),

)