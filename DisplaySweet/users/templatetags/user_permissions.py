from django import template
from projects.permissions import ProjectModelMixin
from users.permissions import AllocationDetailsPermissionMixin

import os
from django.conf import settings
from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
register = template.Library()


@register.filter(name='user_project')
def user_project(user, project):
    return user, project


