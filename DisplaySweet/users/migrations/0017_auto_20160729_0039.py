# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0016_auto_20160728_2116'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserModifiedLog',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('created', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('log', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='last_modified_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, blank=True, related_name='user_modified_by'),
        ),
        migrations.AddField(
            model_name='user',
            name='owned_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, blank=True, related_name='user_owned_by'),
        ),

        migrations.AddField(
            model_name='usermodifiedlog',
            name='user_modified',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, blank=True, related_name='user_modified_log'),
        ),
        migrations.AddField(
            model_name='usermodifiedlog',
            name='user_modified_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, blank=True, related_name='user_modified_by_log'),
        ),
    ]
