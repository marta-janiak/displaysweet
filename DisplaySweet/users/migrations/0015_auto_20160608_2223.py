# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0014_user_temporary_setup_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useraccountemails',
            name='email_subject',
            field=models.CharField(max_length=150, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='useremailtemplate',
            name='template_type',
            field=models.CharField(default=b'account_setup', max_length=50, choices=[(b'account_setup', b'Account Setup Email'), (b'account_password', b'Account Password Email'), (b'added_to_project', b'Added to Project Email')]),
        ),
    ]
