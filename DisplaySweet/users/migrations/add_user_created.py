# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0017_auto_20160729_0039'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='created',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='propertypurchasefiles',
            name='created',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='propertypurchasecomments',
            name='created',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='usertodolist',
            name='created',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='useraccountemails',
            name='created',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True),
        ),
    ]