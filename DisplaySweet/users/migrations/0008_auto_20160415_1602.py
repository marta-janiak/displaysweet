# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20160415_1530'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchaser',
            name='last_login',
        ),
        migrations.RemoveField(
            model_name='purchaser',
            name='password',
        ),
        migrations.RemoveField(
            model_name='purchaser',
            name='username',
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='email',
            field=models.EmailField(max_length=255, verbose_name=b'email address'),
        ),
    ]
