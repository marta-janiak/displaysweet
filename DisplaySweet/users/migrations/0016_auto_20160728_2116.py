# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0015_auto_20160608_2223'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserImports',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('date_imported', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('created', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('user_file', models.FileField(blank=True, null=True, upload_to='user_imports')),
                ('imported', models.BooleanField(default=False)),
            ],
        ),

    ]
