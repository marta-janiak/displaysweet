# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_purchaser_property_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='created_by',
            field=models.ForeignKey(related_name='user_created_by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='company_name',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='date_of_birth',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='email',
            field=models.EmailField(unique=True, max_length=255, verbose_name=b'email address'),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='first_name',
            field=models.CharField(max_length=30, null=True, verbose_name=b'first name', blank=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='last_name',
            field=models.CharField(max_length=30, null=True, verbose_name=b'last name', blank=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='phone_number',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='title',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name=b'title', choices=[(b'Ms', b'Ms'), (b'Mr', b'Mr'), (b'Mrs', b'Mrs'), (b'Miss', b'Miss'), (b'Dr', b'Dr')]),
        ),
        migrations.AlterField(
            model_name='purchaser',
            name='website',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
