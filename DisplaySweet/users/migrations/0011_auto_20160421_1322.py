# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_auto_20160421_1318'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraccountemails',
            name='email_image',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='useraccountemails',
            name='email_subject',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='useraccountemails',
            name='email_text',
            field=models.TextField(null=True, blank=True),
        ),
    ]
