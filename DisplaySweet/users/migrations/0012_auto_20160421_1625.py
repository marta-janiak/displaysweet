# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20160421_1322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='useraccountemails',
            options={'verbose_name_plural': 'User account emails'},
        ),
        migrations.AddField(
            model_name='useremailtemplate',
            name='template_subject',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='useraccountemails',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='useraccountemails',
            name='modified',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='useremailtemplate',
            name='template_type',
            field=models.CharField(default=b'account_setup', max_length=50, choices=[(b'account_setup', b'Account Setup Email')]),
        ),
    ]
