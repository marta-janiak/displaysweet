# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_auto_20160421_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useremailtemplate',
            name='template_type',
            field=models.CharField(default=b'account_setup', max_length=50, choices=[(b'account_setup', b'Account Setup Email'), (b'account_password', b'Account Password Email')]),
        ),
    ]
