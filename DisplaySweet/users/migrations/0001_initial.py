# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.CharField(help_text=b'Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=255, verbose_name=b'username', validators=[django.core.validators.RegexValidator(b'^[\\w.@+-]+$', b'Enter a valid username.', b'invalid')])),
                ('title', models.CharField(choices=[(b'Ms', b'Ms'), (b'Mr', b'Mr'), (b'Mrs', b'Mrs'), (b'Miss', b'Miss'), (b'Dr', b'Dr')], max_length=30, blank=True, help_text=b'Enter your title.', null=True, verbose_name=b'title')),
                ('first_name', models.CharField(help_text=b'Enter your first name.', max_length=30, null=True, verbose_name=b'first name', blank=True)),
                ('last_name', models.CharField(help_text=b'Enter your last name.', max_length=30, null=True, verbose_name=b'last name', blank=True)),
                ('email', models.EmailField(help_text=b'Enter your email address.', unique=True, max_length=255, verbose_name=b'email address')),
                ('phone_number', models.CharField(help_text=b'Please enter a valid 10 digit phone number. Please include area code i.e 03.', max_length=255, null=True, blank=True)),
                ('date_of_birth', models.DateField(help_text=b'Date of birth', null=True, blank=True)),
                ('user_type', models.CharField(default=b'normal_user', max_length=50, choices=[(b'admin_user', b'Admin'), (b'super_admin_user', b'Super Admin'), (b'normal_user', b'User')])),
                ('is_active', models.BooleanField(default=True)),
                ('can_access_django_backend', models.BooleanField(default=False)),
                ('status', models.CharField(default=b'approved', max_length=50, choices=[(b'pending_registration', b'Pending Approval'), (b'suspended', b'Suspended'), (b'approved', b'Approved')])),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
