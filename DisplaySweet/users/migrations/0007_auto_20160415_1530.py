# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_manual_20160415_agent_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='agent',
        ),
        migrations.AddField(
            model_name='purchaser',
            name='agent',
            field=models.ForeignKey(related_name='property_agent', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
