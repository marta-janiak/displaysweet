# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150910_1008'),
        ('users', '0001_initial'),
    ]

    # operations = [
    #     migrations.AddField(
    #         model_name='user',
    #         name='projects',
    #         field=models.ManyToManyField(related_name='props', null=True, to='core.Project', blank=True),
    #     ),
    # ]
