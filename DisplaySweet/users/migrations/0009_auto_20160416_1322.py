# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20160415_1602'),
    ]

    operations = [
        migrations.CreateModel(
            name='PropertyPurchaseComments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(null=True, blank=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('comment_status', models.CharField(default=b'no_action', max_length=100)),
                ('action', models.TextField(null=True, blank=True)),
                ('action_required_by', models.DateTimeField(null=True, blank=True)),
                ('action_user_notified', models.BooleanField(default=False)),
                ('action_is_approved', models.BooleanField(default=True)),
                ('date_approved', models.DateTimeField(null=True, blank=True)),
                ('action_required_user', models.ForeignKey(related_name='action_comment_by', to=settings.AUTH_USER_MODEL)),
                ('approved_by', models.ForeignKey(related_name='action_approved_by_user', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('comment_by', models.ForeignKey(related_name='property_comment_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PropertyPurchaseFiles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(null=True, blank=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('attached_file', models.FileField(null=True, upload_to=b'', blank=True)),
                ('attached_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserToDoList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(null=True, blank=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('todo_status', models.CharField(default=b'action_required', max_length=100)),
                ('to_do_action', models.TextField(null=True, blank=True)),
                ('action_required_by', models.DateTimeField(null=True, blank=True)),
                ('action_user_notified', models.BooleanField(default=False)),
                ('todo_is_approved', models.BooleanField(default=True)),
                ('date_approved', models.DateTimeField(null=True, blank=True)),
                ('approved_by', models.ForeignKey(related_name='to_do_approved_by_user', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('assign_to_do_action', models.ForeignKey(related_name='to_do_comment_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='purchaser',
            name='agent_bonus',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='cancelled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='contract_exchange_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='current_position',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='deposit_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='deposit_refund',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='leased_amount',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='max_cost',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='number_of_weeks',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='percentage_amount',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='rebate',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='recouped',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='rental_guarantee',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='reservation_fee_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='reservation_fee_refund',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='sales_price',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='sales_type',
            field=models.CharField(default=b'weekly', max_length=50, choices=[(b'weekly', b'Weekly Amount'), (b'percentage', b'Percentage')]),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='weekly_amount',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='purchaser',
            name='weeks_leased',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='usertodolist',
            name='purchase',
            field=models.ForeignKey(blank=True, to='users.Purchaser', null=True),
        ),
        migrations.AddField(
            model_name='usertodolist',
            name='user',
            field=models.ForeignKey(related_name='to_do_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='propertypurchasefiles',
            name='purchase',
            field=models.ForeignKey(to='users.Purchaser'),
        ),
        migrations.AddField(
            model_name='propertypurchasecomments',
            name='purchase',
            field=models.ForeignKey(to='users.Purchaser'),
        ),
    ]
