# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20160416_1322'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAccountEmails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modified', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('login_key', models.CharField(max_length=50, null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserEmailTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modified', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('template_text', models.TextField()),
                ('active', models.BooleanField(default=True)),
                ('template_type', models.CharField(default=b'account_setup', max_length=50)),
            ],
        ),
        migrations.AlterField(
            model_name='user',
            name='user_type',
            field=models.CharField(default=b'normal_user', max_length=50, choices=[(b'super_admin_user', b'Display Sweet Super Admin'), (b'admin_user', b'Display Sweet Admin'), (b'client_admin_user', b'Client Admin'), (b'master_agent', b'Master Agent'), (b'agent', b'Agent'), (b'normal_user', b'View Only User')]),
        ),
        migrations.AddField(
            model_name='useraccountemails',
            name='template_used',
            field=models.ForeignKey(blank=True, to='users.UserEmailTemplate', null=True),
        ),
        migrations.AddField(
            model_name='useraccountemails',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
