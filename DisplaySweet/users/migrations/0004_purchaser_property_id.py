# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160414_1707'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaser',
            name='property_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
