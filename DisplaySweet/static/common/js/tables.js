/*global _ */

(function() {
    // Add object serialization of forms to jQuery
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function fill_form_with_object(data){
        // reset form values from an object
        $.each(data, function(name, val){
            var $el = $('[name="'+name+'"]');
            var type = $el.attr('type');

            switch(type){
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="'+val+'"]').attr('checked', 'checked');
                    break;
                default:
                    $el.val(val);
            }
        });
    }

    $(document).ready(function() {
        // Set up toggling of the filters panel
        $(".table-filters-header").click(function(){
            $(this).toggleClass('active');
            $(this).next(".table-filters").toggleClass('active');
        });

        $(".listview-datatable").each(function(){
            var column_options = [];
            
            // Look at the data fields applied to each column header
            // to figure out whether it should be sortable or hidden
            // by default
            $(this).find('thead tr th').each(function(){
                var sortable = false;
                var visible = true;

                if($(this).data('sortable') !== undefined) {
                    sortable = true;
                }

                if($(this).data('hidden') !== undefined) {
                    visible = false;
                }

                column_options.push({ "bSortable": sortable, "bVisible": visible });
            });

            // The filter form should be specified in a data field on the table
            var filter_form_selector = $(this).data('filter-form');
            var filter_form = $(filter_form_selector);
            var filter_form_reset_button = filter_form.find('.table-filters-reset');
            var autoreload_interval = $(this).data('autoreload');

            // The callback url should be specified in a data field on the table
            var callback = $(this).data('callback');
            var default_ordering_col = $(this).data('ordering-col');
            var default_ordering_direction = $(this).data('ordering-direction');

            if (!default_ordering_col){
                default_ordering_col = 0;
            }

            if (!default_ordering_direction){
                default_ordering_direction = 'asc';
            }

            var table = $(this).dataTable({
                "dom": '<"table-controls"Cl><"table-content"frt><"table-footer"<"row"<"col-lg-4 col-md-4 col-sm-4"i><"col-lg-8 col-sm-8 col-xs-8"p>>>',
                "processing": true,
                "serverSide": true,
                "searching": false,
                "lengthMenu": [ 10, 25, 50, 75, 100, 200 ],
                "aoColumns": column_options,
                "order": [[default_ordering_col, default_ordering_direction]],
                "scrollX": true,
                "stateSave": true,
                "oLanguage": {
                    "oPaginate": {
                        "sNext": '<i class="icon-arrow-right"></i>',
                        "sPrevious": '<i class="icon-arrow-left"></i>'
                    }
                },
                "stateSaveParams": function (settings, data) {
                    var filter_form_data = filter_form.serializeObject();
                    data.filter_form_data = filter_form_data;
                },
                "stateLoaded": function (settings, data) {
                    var filter_form_data = data.filter_form_data;

                    fill_form_with_object(filter_form_data);

                    if (_.some(filter_form_data)){
                        $(".table-filters-header").addClass('active');
                        $(".table-filters-header").next(".table-filters").addClass('active');
                    }
                },
                "ajax": {
                    "url": callback,
                    "type": "GET",
                    "data": function (d) {
                        // Serialize the filter form and add its data to the callback payload
                        var filter_form_data = filter_form.serializeObject();
                        _.extend(d, filter_form_data);
                        return d;
                    }
                }
            });

            $('<button class="refresh"><i class="icon-refresh"></i></button>').appendTo('div.table-controls');

            $('div.table-controls .refresh').click(function(){
                table.api().ajax.reload(null, false);
            });

            if (autoreload_interval) {
                setInterval(function () {
                    table.api().ajax.reload(null, false); // user paging is not reset on reload
                }, autoreload_interval * 1000 );
            }

            // Reload the table whenever the fiter form inputs change
            filter_form.find(':input').change(function(){
                table.api().ajax.reload();
            });

            filter_form_reset_button.click(function(){
                filter_form.trigger('reset');
                table.api().ajax.reload();
            });
        });

        $(".simple-datatable").each(function(){
            var column_options = [];
            
            // Look at the data fields applied to each column header
            // to figure out whether it should be sortable or hidden
            // by default
            $(this).find('thead tr th').each(function(){
                var sortable = false;
                var visible = true;

                if($(this).data('sortable') === true) {
                    sortable = true;
                }

                if($(this).data('hidden') === true) {
                    visible = false;
                }

                column_options.push({ "bSortable": sortable, "bVisible": visible });
            });

            $(this).dataTable({
                "dom": '<"table-controls"Cl><"table-content"frt>',
                "searching": false,
                "paginate": false,
                "lengthMenu": [ 10, 25, 50, 75, 100, 200 ],
                "aoColumns": column_options,
                "scrollX": true,
                "stateSave": true
            });
        });
    });
})();
