function update_selected_hotpots(x_id, y_id, form_id)
{
    var selected_current_value_x = $("#selected_current_value_x").val();
    var selected_current_value_y = $("#selected_current_value_y").val();

    if (selected_current_value_x && x_id)
    {
        $("#"+ x_id).val(selected_current_value_x);
    }

    if (selected_current_value_y && y_id)
    {
        $("#"+ y_id).val(selected_current_value_y);
    }

    $("#"+form_id).submit();
}

function show_current_values(canvas)
 {
     var ctx = canvas.getContext('2d');
     ctx.fillStyle = 'blue';

     var current_value_x = parseFloat($("#current_value_x").text() * 500.0);
     var current_value_y = parseFloat($("#current_value_y").text() * 500.0);

//            console.log(current_value_x, current_value_y);
//     ctx.fillText("X",current_value_x,current_value_y);

      var centerX = current_value_x;
      var centerY = current_value_y;
      var radius = 14;

      ctx.globalAlpha = 0.5;
      ctx.beginPath();
      ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
      ctx.fillStyle = 'red';
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = '#003300';
      ctx.stroke();

      ctx.fillStyle = 'black';
      ctx.globalAlpha = 1;
      ctx.fillText("X",(current_value_x - 4),(current_value_y+2));

 }

 function writeMessage(canvas, message) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.font = '11pt Calibri';
    context.fillStyle = 'black';
    context.fillText(message, 10, 25);
 }

 function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
 }

 function show_selected(canvas, x, y)
 {
     var context = canvas.getContext('2d');
     context.fillStyle = 'green';

     var u_x = (x * 500.0).toFixed(4);
     var u_y = (y * 500.0).toFixed(4);

     context.fillText("X",u_x,u_y);

     $("#selected_x").val(x);
     $("#selected_y").val(y);

     $("#selected_current_value_x").val(x);
     $("#selected_current_value_y").val(y);
 }

$(function() {

    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");

    ctx.moveTo(250,0);
    ctx.lineTo(250,500);
    ctx.stroke();

    ctx.moveTo(0,250);
    ctx.lineTo(500,250);
    ctx.stroke();

    ctx.font = "10px Arial";
    ctx.fillText("0",0,250);
    ctx.fillText("1",250,500);
    ctx.fillText("1",495,250);
    ctx.fillText("0",245,8);

    show_current_values(canvas);

    $("#cross-hatch").click(function(e) {

      var offset = $(this).offset();

      var relativeX = (e.pageX - offset.left);
      var relativeY = (e.pageY - offset.top);


      var x = parseFloat(relativeX) / 500.0;
      var y = parseFloat(relativeY) / 500.0;

      x = (x).toFixed(4);
      y = (y).toFixed(4);


      var canvas = document.getElementById("cross-hatch");
      var ctx = canvas.getContext("2d");
      show_selected(canvas, x, y);

      show_current_values(canvas);


    });

    //x = 0.567
    // y = 0.79

    canvas.addEventListener('mousemove', function(evt) {
        var mousePos = getMousePos(canvas, evt);

        var x = (parseFloat(mousePos.x) / 500.0).toFixed(4);
        var y = (parseFloat(mousePos.y) / 500.0).toFixed(4);

        var message = 'x: ' + x + ' y: ' + y;

        writeMessage(canvas, message);

        ctx.strokeStyle = 'red';

        ctx.moveTo(250,0);
        ctx.lineTo(250,500);

        ctx.stroke();

        ctx.moveTo(0,250);
        ctx.lineTo(500,250);
        ctx.stroke();

        ctx.font = "10px Arial";
        ctx.fillText("0",0,250);
        ctx.fillText("1",250,500);
        ctx.fillText("1",495,250);
        ctx.fillText("0",245,8);

        show_current_values(canvas);

        ctx.fillStyle = 'green';

        var selected_x = $("#selected_x").val();
        var selected_y = $("#selected_y").val();

        ctx.fillText("X",(selected_x * 500.0),(selected_y * 500.0));

    }, false);

});
