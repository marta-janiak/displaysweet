
        // function filter_images_by_tag()
        // {
        //     var tag_name = $('#tag-selected').find('option:selected').text();
        //     var tag_id = $('#tag-selected').find('option:selected').val();
        //
        //     if (! tag_id == "")
        //     {
        //         var tag_list = $("#list-of-selected-tags");
        //
        //         tag_list.append('<button id="added-button-tag-'+tag_id+'" onclick="remove_button_tag('+tag_id+', \''+tag_name+'\');" class="btn btn-xs btn-black added-tag-button">'+tag_name+'<i class="fa fa-minus red small" style="margin-left: 10px;"></i></button> &nbsp;');
        //         update_image_list(tag_name);
        //         filter_images();
        //
        //         $('#tag-selected').val('');
        //
        //         $("#tag-selected option[value='"+tag_id+"']").hide();
        //     }
        //
        // }

        function update_image_list(tag_name)
        {
            $(".image-list-tags").each(function() {
                var tag_list = $(this).find('span');
                var tag_details_list = [];

                $(tag_list).each(function() {
                    tag_details_list.push($(this).text());
                });

                if (!$.inArray( tag_name, tag_details_list ))
                {
                    $(this).parent().hide();
                }
                else
                {
                    $(this).parent().show();
                }
            });
        }

        function remove_button_tag(tag_id, tag_name)
        {
            //remove filter
            $("#added-button-tag-"+tag_id).remove();

            $("#tag-selected").append("<option value='"+tag_id+"'>"+tag_name+"</option>");

            filter_images();
        }

        function filter_images()
            {
                var selected_tag_list = [];
                var selected_tags = $(".added-tag-button");
                $(selected_tags).each(function() {
                    selected_tag_list.push($(this).text().replace(/ /g, ""));
                });

                $(".image-list-tags").each(function() {

                    var show_image = true;
                    var tag_list = $(this).find('span');
                    var tag_details_list = [];

                    $(tag_list).each(function () {
                        tag_details_list.push($(this).text());
                    });

                    if (selected_tag_list == "") {
                        $("#no-tag-filter").addClass("fa-check").removeClass('fa-square');
                        $("#list-of-selected-tags").text('').html('');

                        if (tag_details_list == "")
                        {
                            show_image = true;
                        }
                        else
                        {
                            show_image = false;
                        }
                    }
                    else
                    {

                        var image_list = [];
                        $.each(tag_details_list, function (k, v) {
                            var value = v.replace(" ", "");
                            var in_array = $.inArray(value, selected_tag_list) == -1;
                            if (!in_array) {
                                image_list.push(value);
                            }
                        });

                        if (image_list == "")
                        {
                            show_image = false;
                        }
                    }

                    if (!show_image)
                    {
                        $(this).parent().hide();
                    }
                    else
                    {
                        $(this).parent().show();
                    }
                });
            }

        function set_apartment_selected(apartment)
        {
            var x_value = $("#property-hotspot-x-"+apartment).val();
            var y_value = $("#property-hotspot-y-"+apartment).val();

            var color = "green";
            var size = 550;

            redraw_main_canvas(550);
            redraw_apartment_spot(apartment, x_value, y_value, color);

            $("#selected_apartment").val(apartment);
            var project_id = $("#project_id").val();
            var floor_id = $("#floor_id").val();

            $(".listed-image-with-tags").css({"background": "white"});
            $("#selected_apartment").val(apartment);
            $("#selected_apartment_x_coordinate").val(x_value);
            $("#selected_apartment_y_coordinate").val(y_value);

            get_image(project_id, floor_id, "level_plans", apartment);
            get_image(project_id, floor_id, "floor_plans", apartment);
            get_image(project_id, floor_id, "key_plans", apartment);
        }

        function image_selector_change(value_selected)
        {
            var project_id = $("#project_id").val();
            var floor_id = $("#floor_id").val();
            var apartment = $("#selected_apartment").val();
            var x_coordinate = $("#selected_apartment_x_coordinate").val();
            var y_coordinate = $("#selected_apartment_y_coordinate").val();
            var canvas_width = $("#canvas_width").val();

            redraw_apartment_spot(apartment, x_coordinate, y_coordinate, "green");
            get_image(project_id, floor_id, value_selected, apartment);

        }

        function update_plans(image_type) {
            var apartment = $("#selected_apartment").val();
            var floor_id = $("#floor_id").val();
            var project_id = $("#project_id").val();

            $("#image-type-selected-for-floor").val(image_type);
            $("#image_type_displayed").val(image_type);

            //var floor_image_id = $("#"+image_type+"-selected-image").val();
            //
            //if (floor_image_id)
            //{
            //    $("#image-list-" + floor_image_id).css({"background": "pink"});
            //}

        }


        function get_image(project_id, floor_id, value_selected, apartment)
        {
            value_selected = value_selected.replace(" ", "_").toLowerCase();

            $.ajax({
                url: '/get_image_selection_details/' + project_id + '/' + floor_id + '/' + value_selected + '/',
                type: 'GET',
                data: {'apartment': apartment, 'value_selected': value_selected},
                success: function (data) {
                    var static_image = data.static_image;
                    var image = data.image_path;
                    var image_name = data.image_name;

                    if (value_selected == "level_plans")
                    {
                        $("#level_plans-image_name_text").text(image_name);
                        $("#level_plan_on_key").attr('src', static_image).css("padding-top", '0px').css("padding-bottom", '0px');
                        $("#level_plan_name").text(image_name);
                    }

                    $("#" + value_selected + "-image_name_text").html(image_name);
                    $("#" + value_selected + "-show-image-name-text-" + apartment).html(image_name);
                    $("#" + value_selected + "_name").text(image_name);
                    $(".canvas-image-" + value_selected + "").attr('src', static_image);

                    if (data.resource_id)
                    {
                        $("#image-list-" + data.resource_id).css({"background": "pink"});
                    }


                    if (data.rendered)
                    {
                        $("#property-combined-listings").html(data.rendered);

                    }
                    else
                    {
                        $("#property-combined-listings").html('');
                    }

                    $("#canvas-image-"+value_selected).attr('src', data.static_image)
                    $("#"+value_selected+"-message").html(image);

                }

            });
        }

    function save_selected_images_ns(project_id) {
        var path_selected = $("#import_image_path").val();
        var floor_images = $("#floor_images").val();
        var error = false;

        if (!floor_images) {
            $().toastmessage('showToast', {
                        text: "You have not selected any images to load.",
                        sticky: true,
                        position: 'top-left',
                        type: 'error',
                        closeText: ''}
            );
            error = true;
        }

        if (!path_selected) {
            $().toastmessage('showToast', {
                        text: "You have not selected an image path.",
                        sticky: true,
                        position: 'top-left',
                        type: 'error',
                        closeText: ''}
            );
            error = true;
        }

        if (!error)
        {
            var formdata = new FormData($('#import-folder-images')[0]);
            var csrf_token = $('#import-folder-images input[name="csrfmiddlewaretoken"]').val();

            $.ajax({
                url: '/import_images_ns/' + project_id + '/',
                type: 'POST',
                csrfmiddlewaretoken: csrf_token,
                data: formdata,
                enctype: "multipart/form-data",
                processData: false,
                contentType: false,
                success: function (data) {

                    $("#close-image-saver").click();

                    if (data.success)
                    {
                        $().toastmessage('showToast', {
                            text: "Images imported successfully",
                            sticky: false,
                            position: 'top-left',
                            type: 'notice',
                            closeText: ''}
                        );

                        $().toastmessage('showToast', {
                                    text: "Images folder path saved successfully",
                                    sticky: false,
                                    position: 'top-left',
                                    type: 'notice',
                                    closeText: ''}
                        );

                        $("#path-details").effect("highlight", {color: "#F7D0D4"}, 3000).delay(800);

                    }

                    else

                    {
                        $().toastmessage('showToast', {
                                text: "Project dimensions file has not been loaded.",
                                sticky: false,
                                position: 'top-left',
                                type: 'error',
                                closeText: ''}
                        );
                    }

                      },
               error: function(jqXHR, textStatus, errorMessage) {
                   //console.log(errorMessage); // Optional


               }
            });

        }
    }

    function save_page_image_details(project_id)
    {
        var floor_id = $("#floor_id").val();
        var image_type = $("#image-type-selected-for-floor").val();

        var apartment = $("#selected_apartment").val();
        var selected_image_id = $("#selected_image_id").val();
        var image_path = $("#floorplan-message").text();

        var image_selected = $("#image-selected-for-floor").val();

        $.ajax({
            url: '/save_image_to_floor/' + project_id + '/' + floor_id + '/' + image_type +'/',
            type: 'GET',
            data: {'image_selected': image_selected, 'apartment': apartment, 'image_path': image_path, 'selected_image_id':selected_image_id},
            success: function (data) {
                $().toastmessage('showToast', {
                            text: "Details saved successfully",
                            sticky: false,
                            position: 'top-left',
                            type: 'success',
                            closeText: ''}
                );

                var image_name = data.image_name;

                $("#"+image_type+"-image_name_text").text(data.image_name);
                if (image_type == "level_plans")
                {

                    $("#level_plans-show-image-name-text-"+floor_id).text(image_name);
                    $("#"+image_type+"-image_name_text-"+floor_id).val(image_name);
                }
                else
                {
                    $("#apartment-"+image_type+"-show-image-name-text-"+apartment).text(image_name);
                    $("#apartment-"+image_type+"-image_name_text-"+apartment).val(image_name);
                }

                $("#"+image_type+"-selected-image").val(data.resource_id);

                $("#canvas-image-"+image_type).attr('src', data.static_image).css("padding-top", '0px').css("padding-bottom", '0px');
                $(".canvas-image-"+image_type).attr('src', data.static_image).css("padding-top", '0px').css("padding-bottom", '0px');
                $("#" + image_type + "_name").text(image_name);

                if (data.update_image_names)
                {
                    $(".selector-image-apartment").append(data.rendered);
                    $("#selector-image-apartment-"+apartment).val(data.resource_id+'-');
                }
                else
                {
                    $("#selector-image-apartment-"+apartment).html('');
                    $("#selector-image-apartment-"+apartment).html(data.rendered);
                    $("#selector-image-apartment-"+apartment).val(data.resource_id+'-'+data.image_name);
                }

                $("#apartment-show-image-name-"+apartment).text(data.image_name);


            },
            error: function (jqXHR, textStatus, errorMessage) {
                //console.log(errorMessage); // Optional
            }
        });

    }

    function change_selected_image(resource_id, image_path, value_selected)
    {
        $(".image-selection-box").hide();
        $("#clear-selection").show();
        $("#selected_image_id").val(resource_id);

        var display_image_path = image_path;

        var image_path_split = image_path.split("/");
        var image_name_short = image_path_split[image_path_split.length-1];

        $("#canvas-image").attr('src', display_image_path).css("padding-top", '0px').css("padding-bottom", '0px');

        var img = document.getElementById('canvas-image');
        var width = img.clientWidth;
        var height = img.clientHeight;



        image_name_short = image_name_short.replace(".jpg", "").replace(".png", "");
        $("#"+value_selected+"-image-selection-"+image_name_short).show();
        $("#image-selected-for-floor").val(image_name_short);

        $("#image-type-selected-for-floor").val(value_selected);
        $("#floorplan-message").text(image_path_split.join("/"));

    }

function change_display()
{
    $("#show-preview").toggleClass('display');

    if (!$("#show-preview").hasClass('display'))
    {
        $('a.hovering').off('mouseover');
    }
    else
    {
         $('a.hovering').on({
            mouseover: function(e){
                var xOffset = 310;
                var yOffset = 30;

                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? "<br/>" + this.t : "";
                $("body").append("<p id='preview'><img src='" + this.href + "' alt='Image preview' /></p>");
                $("#preview")
                    .css("top", (e.pageY - xOffset) + "px")
                    .css("left", (e.pageX + yOffset) + "px")
                    .fadeIn("fast");
            },
             mouseleave: function(){
                this.title = this.t;
                $("#preview").remove();
            },
            click: function(e){
                e.preventDefault();
            }
        });
    }

}

function toggle_name_editor(element_id)
{
    $('#name-input-'+element_id).toggle();
    $('#pencil-'+element_id).toggleClass('fa-save').toggleClass('btn-danger');
}


function redraw_apartment_spot(apartment, x_value, y_value, color)
 {

     if (x_value == '')
     {
         x_value = 0;
     }
     else
     {
         x_value = parseFloat(x_value);
     }

     if (y_value == '')
     {
         y_value = 0;
     }
     else
     {
         y_value = parseFloat(y_value);
     }

     $("#selected_apartment").val(apartment);
     $(".show-apartment").css('background', 'white').css('color', 'black');

     $("#apartment-link-"+apartment).css('background', 'red').css('color', 'white');
     $(".apartment-link-"+apartment).css('background', 'red').css('color', 'white');

     $(".row-name").css('background', 'white').css('color', 'black');
     $(".row-name-"+apartment).css('background', 'red').css('color', 'white');


     size = 550;

     if (!(x_value == '0' && y_value == '0') && !(x_value == 0 && y_value == 0))
     {
         
         $("#nav-tab-link-li").click();

         var canvas = document.getElementById("cross-hatch-level_plans");
         if (canvas)
         {
             var ctx = canvas.getContext('2d');

             ctx.fillStyle = color;

             var current_value_x = parseFloat(x_value * size);
             var current_value_y = parseFloat(y_value * size);

             //console.log('current_value_x', current_value_x);

              var centerX = current_value_x;
              var centerY = current_value_y;
              var radius = 16;

              ctx.globalAlpha = 0.5;
              ctx.beginPath();
              ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
              ctx.fillStyle = color;
              ctx.fill();
              ctx.lineWidth = 0.1;
              ctx.strokeStyle = color;
              ctx.stroke();
         }

     }


 }


function draw_canvas_update(canvas)
{

}



function redraw_main_canvas(size)
{
    //console.log('redraw_main_canvas');
    var canvas = document.getElementById("cross-hatch-level_plans");
    if (canvas)
    {
        var ctx = canvas.getContext("2d");

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.canvas.width = ctx.canvas.width;

        $("#selected_x").val('');
        $("#selected_y").val('');

        ctx.strokeStyle = 'red';
        ctx.moveTo((size/2),0);
        ctx.lineTo((size/2),size);
        ctx.stroke();

        ctx.moveTo(0,(size/2));
        ctx.lineTo(size,(size/2));
        ctx.stroke();

        ctx.font = "10px Arial";

        ctx.fillText("0",0,(size/2));
        ctx.fillText("1",(size/2),size);
        ctx.fillText("1",size-5,(size/2));
        ctx.fillText("0",(size/2)-5,8);
    }



}


function save_image_folder_path(project_id, image_type)
{

    $('#edit-'+image_type+'path').toggle();
    $('#show-'+image_type+'path').toggle();

    $.post( '/save_image_path/'+project_id+'/'+image_type+'/',
            $("#form-save-"+image_type+"-folder-path").serialize()
          ).success(function( data )
            {
                $().toastmessage('showToast', {
                    text: "Images folder path saved successfully",
                    sticky: false,
                    position : 'top-left',
                    type     : 'notice',
                    closeText: ''}
                );

                $("#path-details").effect( "highlight", {color:"#F7D0D4"}, 3000).delay(800);
            }
    );
}

function save_image_name(project_id, floor_id, value_selected)
{

    $('#'+value_selected+'-show-image-name-'+floor_id).toggle();
    $('#'+value_selected+'-edit-image-name-'+floor_id).toggle();

    var csrf_token = $('#'+value_selected+'form-save-image-name-'+ floor_id + 'input[name="csrfmiddlewaretoken"]').val();
    var formdata = $("#"+value_selected+"-form-save-image-name-"+ floor_id).serialize();
    var apartment = $("#selected_apartment").val();
    var actual_floor_id = $("#floor_id").val();

    $.ajax({
        url: '/save_image_name/'+project_id+'/'+floor_id+'/'+value_selected+'/',
        type: 'GET',
        csrfmiddlewaretoken: csrf_token,
        data: formdata,
            success: function (data) {
                   $().toastmessage('showToast', {
                    text: "Image name saved successfully",
                    sticky: false,
                    position : 'top-left',
                    type     : 'notice',
                    closeText: ''}
                );

                $("#"+value_selected+"-show-image-name-text-"+ floor_id).text($("#"+value_selected+"-image_name_text-"+floor_id).val()).effect( "highlight", {color:"#F7D0D4"}, 3000).delay(800);
                get_image(project_id, actual_floor_id, value_selected, apartment);

              }
    });

}

function hide_selected_preview_image()
{
    $(".image-selection-box").hide();
    $("#clear-selection").hide();

    var current_image = $("#current-floor-image-path").val();
        current_image = "/static/"+ current_image;
    $("#canvas-image").attr('src', current_image);
}

function change_selected_preview_image(image_name, image_path, value_selected)
{
    $(".image-selection-box").hide();
    $("#clear-selection").show();

    var image_name_short = image_name.replace('.jpg', '');
    var image_path_split = image_path.split("/");
    var image_path_split_count = image_path_split.length - 1;
    image_path_split[image_path_split_count] = image_name;
    var display_image_path = '/static/'+image_path_split.join("/");

    $("#canvas-image").attr('src', display_image_path).css("padding-top", '0px').css("padding-bottom", '0px');

    var img = document.getElementById('canvas-image');
    var width = img.clientWidth;
    var height = img.clientHeight;

    if ($.inArray( value_selected, ["key", "key_plans"]) == 1)
    {
        $("#canvas-image").css("padding-top", '125px').css("padding-bottom", '125px');
    }

    $("#"+value_selected+"-image-selection-"+image_name_short).show();
    $("#image-selected-for-floor").val(image_name_short);

    $("#image-type-selected-for-floor").val(value_selected);
    $("#image-mode-selected-for-floor").val($("#change-side-image-type-mode").val());
    $("#image-output-selected-for-floor").val($("#change-side-image-type-output").val());

    $("#floorplan-message").text(image_path_split.join("/"));

}


function save_new_coordinate(project_id, apartment_id)
{

    var x_coordinate = $("#new_apartment_set_x_coordinate_"+apartment_id).val();
    var y_coordinate = $("#new_apartment_set_y_coordinate_"+apartment_id).val();

    $.ajax({
        url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
        type: 'GET',
        data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': false},
        success: function (data) {
            $("#apartment_set_x_coordinate_"+apartment_id).val(x_coordinate);
            $("#apartment_set_y_coordinate_"+apartment_id).val(y_coordinate);

            $("#property-hotspot-x-"+apartment_id).val(x_coordinate);
                $("#property-hotspot-y-"+apartment_id).val(y_coordinate);

            $("#new_apartment_set_x_coordinate_"+apartment_id).val('');
            $("#new_apartment_set_y_coordinate_"+apartment_id).val('');

            $().toastmessage('showSuccessToast', 'New co-ordinates saved successfully.');

            $("#apartment_x_"+apartment_id).val(x_coordinate);
            $("#apartment_y_"+apartment_id).val(y_coordinate);

            var coord_exists = $('#refresh-coordinates-x-'+apartment_id).length;
            if (coord_exists == 0)
            {
                $(".refresh-coordinate").show();
            }

            refresh_floor_hotspots();
            $( "#cross-hatch-level_plans" ).unbind( "click" );
            $('#cross-hatch-level_plans').css('cursor', 'default');


        },
        error: function (jqXHR, textStatus, errorMessage) {
            //console.log(errorMessage); // Optional
        }
    });

        click_to_close_hotspot(apartment_id);
}




function save_page_details(project_id, floor_id)
{
    if (confirm("The image selected will be renamed to the name saved for the current floor. Continue?")) {
        var image_selected = $("#image-selected-for-floor").val();

        var image_type = $("#image-type-selected-for-floor").val();
        var image_mode = $("#image-mode-selected-for-floor").val();
        var image_output = $("#image-output-selected-for-floor").val();

        var apartment = $("#selected_apartment").val();
        var image_path = $("#floorplan-message").text();

        $.ajax({
            url: '/save_image_to_floor/' + project_id + '/' + floor_id + '/' + image_type +'/',
            type: 'GET',
            data: {'image_selected': image_selected, 'apartment': apartment, 'image_mode': image_mode, 'image_output': image_output},
            success: function (data) {
                $().toastmessage('showToast', {
                            text: "Image saved to floor successfully",
                            sticky: false,
                            position: 'top-left',
                            type: 'success',
                            closeText: ''}
                );

                get_image(project_id, floor_id, image_path, apartment);

            },
            error: function (jqXHR, textStatus, errorMessage) {
                //console.log(errorMessage); // Optional
            }
        });
    }
}

function check_save_selected_images(project_id) {

    var path_selected = $("input:radio[name=path_to_save_to]:checked").val();
    var path = $("#" + path_selected + "_path").val();
    var floor_images = $("#floor_images").val();
    var error = false;

    if (!floor_images) {
        $().toastmessage('showToast', {
                    text: "You have not selected any images to load.",
                    sticky: true,
                    position: 'top-left',
                    type: 'error',
                    closeText: ''}
        );

        error = true;
    }

    if (path == '') {
        $().toastmessage('showToast', {
                    text: "You have selected the " + path_selected.replace('_', ' ') + " path but not entered a path value.",
                    sticky: true,
                    position: 'top-left',
                    type: 'error',
                    closeText: ''}
        );

        error = true;
    }

    if (!error)
    {
        $().toastmessage('showToast', {
                    text: "Images imported successfully",
                    sticky: false,
                    position: 'top-left',
                    type: 'notice',
                    closeText: ''}
        );

        $("#close-image-saver").click();

        var formdata = new FormData($('#import-folder-images')[0]);
        var csrf_token = $('#import-folder-images input[name="csrfmiddlewaretoken"]').val();

        $.ajax({
            url: '/import_images/' + project_id + '/',
            type: 'POST',
            csrfmiddlewaretoken: csrf_token,
            data: formdata,
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            success: function (data) {
                    $().toastmessage('showToast', {
                                text: "Images folder path saved successfully",
                                sticky: false,
                                position: 'top-left',
                                type: 'notice',
                                closeText: ''}
                    );

                $("#path-details").effect("highlight", {color: "#F7D0D4"}, 3000).delay(800);

                  },
           error: function(jqXHR, textStatus, errorMessage) {
               //console.log(errorMessage); // Optional
           }
        });

    }
}

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function remove_image_from_location(project_id, value_selected)
{
    var image = $("#image-selected-for-floor").val();
    var path = $("#image-plan-path").val();
    if (value_selected)
    {
        image = $("#"+value_selected+"-image-selected-for-floor").val();
        path = $("#"+value_selected+"-image-plan-path").val();
    }


    $.ajax({
        url: '/remove_image_from_path/' + project_id + '/',
        type: 'GET',
        data: {'image':image, 'path': path},
        success: function() {
            window.location.reload();
        },
        fail: function() {
            alert('image fail');
        }
    });
}