$(function() {
    var floor_id = '';
    var building_id = $("#current_building_id").val();
    var project_id = $("#current_project_id").val();

    refresh_with_new_building(building_id);
    redraw_main_canvas();

    $("#select_all").change(function(){
        $(".floor-checkbox").prop('checked', $(this).prop("checked"));
        redraw_main_canvas();
    });

    $(".nav-tab-link").click(function() {
        $("#image-selector-section").css({'margin-top':'-10px'});
    });

    $(init);

    $(".zoom-in").click(function() {
        var zoom = $("#zoom_factor_level_plans").val();
        $("#zoom_factor_level_plans").val(parseInt(zoom) + 1);

        $(".hotspot_deselector").hide();
        $(".hotspot_selector").show();
        $(".show_current_hotspots").hide();

    });

    $(".zoom-out").click(function() {
        var zoom = $("#zoom_factor_level_plans").val();
        $("#zoom_factor_level_plans").val(parseInt(zoom) - 1);

        $(".hotspot_deselector").hide();
        $(".hotspot_selector").show();
        $(".show_current_hotspots").hide();

    });

    $(".zoom-reset").click(function() {
        $("#zoom_factor_level_plans").val(0);
        $(".hotspot_deselector").hide();
        $(".hotspot_selector").show();
        $(".show_current_hotspots").hide();

    });

    $(".selector-image-apartment").change(function() {
        var  property_id = $(this).attr('id');
        property_id = property_id.replace("selector-image-apartment-", "");

        var typical_string = $.trim($("#selector-image-apartment-"+ property_id+" option:selected").text());
        var project_id = $("#current_project_id").val();

       var canvas_id = $("#canvas_id").val();
        var sub_wireframe_id = $("#sub_wireframe_id").val();
        var template_id = $("#template_id").val();

        $.ajax({
            url: '/add_typical_string_to_property/' + project_id + '/',
            type: 'GET',
            data: {'typical_string': typical_string, 'property_id': property_id, 'canvas_id': canvas_id,
            'sub_wireframe_id': sub_wireframe_id, 'template_id': template_id},
            success: function (data) {
                $().toastmessage('showToast', {
                            text: "Image saved to type successfully",
                            sticky: false,
                            position: 'top-right',
                            type: 'success',
                            closeText: ''}
                );

                if (data.image_name)
                {
                    $("#apartment-show-image-name-"+property_id).html(data.image_name);
                }

                set_apartment_selected(property_id);
            }
        });

        typical_string = typical_string.replace(/ /g, "");
        $("#apartment-show-image-name-"+property_id).removeClass("property-typical-None").addClass('property-typical-'+typical_string);
        $("#trash-property-image-"+property_id).html('<a id="trash-property-image-'+property_id+'" class="link fa-lg" onclick="remove_property_assigned_image('+property_id+', \''+property_id+'\');"><i class="fa fa-trash-o"></i> </a>');
        $("#trash-property-type-"+property_id).html('<a id="trash-property-type-'+property_id+'" class="link fa-lg" onclick="clear_image_type_from_property('+property_id+');"> <i class="fa fa-trash-o"></i> </a>');
    });
});

function save_image_selector_files()
{
    var project_id = $("#current_project_id").val();
    var individual_file_id = $("#individual_file").val();
    var multi_file_id = $("#multi_file").val();
    var canvas_id = $("#canvas_id").val();
    var sub_wireframe_id = $("#sub_wireframe_id").val();
    var template_id = $("#template_id").val();
    var multi_image_container_id = $("#multi_image_container_id").val();
    var individual_image_container_id = $("#individual_image_container_id").val();
    
    var removed_assets = $("#removed-asset-ids").html().trim();

    $.ajax({
        url: '/save_image_selector_files/' + project_id + '/',
        type: 'GET',
        data: {'multi_file_id': multi_file_id, 'individual_file_id': individual_file_id, 'canvas_id': canvas_id,
               'sub_wireframe_id': sub_wireframe_id, 'template_id': template_id, 
               'individual_image_container_id': individual_image_container_id,
                'multi_image_container_id': multi_image_container_id, 'removed_assets': removed_assets},
        success: function (data) {
                $().toastmessage('showToast', {
                    text: "Images saved successfully",
                    sticky: false,
                    position: 'top-right',
                    type: 'success',
                    closeText: ''}
                );

            $("#removed-asset-ids").html('');


        }
    });
}

function naturalCompare(a, b) {
    if (a && b)
    {
        var ax = [], bx = [];

        a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
        b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

        while(ax.length && bx.length) {
            var an = ax.shift();
            var bn = bx.shift();
            var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
            if(nn) return nn;
        }

        return ax.length - bx.length;
    }

}


function filter_property_type(property_type_id)
{
    console.log('filter_property_type', property_type_id);

    $("#property_type_filter").val(property_type_id);

    $(".property-type-selection").val(property_type_id);
    $(".apartment-details").hide();

    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

    if (property_type_id)
    {
        $(".property-type-"+property_type_id+"-"+floor_id+"-"+building_id).show();
    }
    else {
        $(".apartment-details-"+floor_id+"-"+ building_id).show();
    }

    console.log('property_type_id', property_type_id);
}

function filter_by_building(building_id)
{
    $(".building-floor-select").hide();
    $(".building-floor-select-"+building_id).show();
}

function refresh_with_new_building(building_id)
{
    $("#apartment-content-card").html('<div style="text-align: center; margin: auto;"><h3>Loading...</h3>' +
                                      '<img src="/static/common/img/loading-blue.gif" style="width: 100px;"></div>');

    $("#current_building_id").val(building_id);
    $("#building-selected").val(building_id);

    var project_id = $("#current_project_id").val();

    $("#floor-tab-content").css('background','rgb(250,250,245)');
    $("#apartment-content-card").css('background','rgb(250,250,245)');
    $("#building-selected").hide();


    $("#all-floors-list").html('<div style="text-align: center; margin: auto;"><h3>Loading...</h3>' +
        '<img src="/static/common/img/loading-blue.gif" style="width: 100px;"></div>');

    $("#select-all-floors-table").hide();

    $.ajax({
        url: '/get_level_plans_screen/' + project_id + '/' + building_id + '/',
        type: 'GET',
        success: function (data) {

            $("#floor-tab-content").css('background','#fff');
            $("#building-selected").show();

            if (data.first_apartment_id)
            {
                $("#current_apartment_setting").val(data.first_apartment_id);
            }

            $("#level-plan-apartments").html(data.rendered);

            $("#select-all-floors-table").show();
            $("#all-floors-list").html(data.select_floors_rendered);

            var floor_id = data.floor_id;
            $("#current_floor_id").val(floor_id);


            $(".linked-floor-select").removeClass('selected');
            $(".floor-details").removeClass('selected');
            $("#floor-details-"+floor_id).removeClass('selected');
            $("#floor-selected-"+floor_id).addClass('selected');
            $("#floor-hotspot-" + floor_id).prop('checked', true);

            var static_image = data.static_image;
            $("#canvas-image-level_plans").attr('src', static_image);
            $(".canvas-image-level_plans").attr('src', static_image);
            $("#level_plans-image_name_text").html(data.image_name);
            $("#level_plans_name").text(data.image_name);

            if (data.first_apartment_id) {
                set_apartment_selected($("#current_apartment_setting").val(), 'level_plans');
            }

            if (data.apartment_list)
            {
                
                show_current_values(data.apartment_list);
            }


        }
    });

}



function init() {

    $('.draggable-image').draggable( {
      containment: 'document',
      helper: myHelper,
      cursor: 'move',
      appendTo: 'body',
      scroll: true,
      refreshPositions: true

  } );

    $('#cross-hatch').droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropEvent
    } );

    $(".highlight-floor-select").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleFloorDropEvent
    });

    $(".drop-floor-plan").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropTableFloorEvent
    });

    $("#key-plan-canvas-floor").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropTableFloorEvent
    });

    $(".apartment-show-key-name").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropTableKeyEvent
    });

    $(".drop-key-plan").droppable({
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropTableKeyEvent
    });

    $("#cross-hatch-level_plans").droppable({
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropEvent
    } );

    $("#cross-hatch-key_plans").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropKeyEvent
    } );

    $("#cross-hatch-floor_plans").droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropFloorEvent
    } );

    $("#small-canvas-image-floor_plans").droppable( {
        hoverClass: "ui-state-active",
        drop: handleDropFloorEvent
    } );

    $('.snap-image-name').droppable( {
        hoverClass: "ui-state-active",
        activeClass: "ui-state-highlight",
        drop: handleDropTypeImageEvent
    } );

    $("#floor-content-sortable").sortable({
            revert: false,
            activeClass: "ui-state-highlight",
            hoverClass: "ui-state-highlight",
            placeholder: "ui-state-error",
            stop: function () {
                handleDropFloorOrderEvent()
            }
    });

    $('#apartment-content-sortable', "ul, li").disableSelection();

}


function handleDropTypeImageEvent( event, ui ) {
    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');
    resource_id = resource_id.replace("image-", "");
    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");
    image_name = image_name[image_name.length-1];

    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var canvas_id = $("#canvas_id").val();
    var sub_wireframe_id = $("#sub_wireframe_id").val();
    var template_id = $("#template_id").val();

    var typical_string_id = $(this).attr('id').replace("type-image-selector-", "");
    var typical_string = $("#type-image-"+ typical_string_id).text();
    typical_string = typical_string.trim();

    $.ajax({
        url: '/update_type_image/' + project_id + '/',
        type: 'GET',
        data: {'typical_string': typical_string, 'resource_id': resource_id, 'canvas_id': canvas_id,
               'sub_wireframe_id': sub_wireframe_id, 'template_id': template_id},
        success: function (data) {

            // console.log('update_type_image', data.success);
            if (data.success == true)
            {
                $().toastmessage('showToast', {
                        text: "Image saved to type successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
                );
            }
            //else
            //{
            //    $().toastmessage('showToast', {
            //            text: "Please assign this floor plan typical value to a property first before assigning an image.",
            //            sticky: false,
            //            position: 'top-right',
            //            type: 'error',
            //            closeText: ''}
            //    );
            //}

            typical_string = typical_string.replace(/ /g, "").replace(/\n/g, "");

            try
            {
                $(".property-typical-"+typical_string).text(image_name);
            }

            catch(err) {

            }

            $("#type-image-selector-"+typical_string_id).text(image_name);

            $("#canvas-image-floor_plans").attr("src", image_src);
            $("#floor_plans-image_name_text").text(image_name);

            $(".canvas-image-floor_plans").attr('src', image_src);
            $("#floor_plans_name").text(image_name);
            $("#trash-property-image-"+property_id).html('<a id="trash-property-image-'+property_id+'" class="link fa-lg" onclick="remove_property_assigned_image('+property_id+', \''+property_id+'\');"><i class="fa fa-trash-o"></i> </a>');
            set_apartment_selected(property_id, 'floor_plans');

            $(".selector-image-apartment").each(function() {
                var this_typical_string = $(this).val();
                if (this_typical_string)
                {
                    if (typical_string == this_typical_string)
                    {
                        var this_property_id = $(this).attr('id');
                        this_property_id = this_property_id.replace("selector-image-apartment-", "");
                        $("#apartment-show-image-name-"+this_property_id).text(image_name);
                    }
                }

            });
        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });

}

function handleDropTableFloorEvent( event, ui ) {

    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');

    resource_id = resource_id.replace("image-", "");
    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");

    var project_id = $("#current_project_id").val();
    var canvas_id = $("#canvas_id").val();
    var property_id = $(this).attr('id').replace("drop-floor-plan-", "").replace("apartment-show-image-name-", "");

    if (!parseInt(property_id))
    {
        property_id = $("#current_apartment_setting").val();
    }

    image_name = image_name[image_name.length-1];
    var sub_wireframe_id = $("#sub_wireframe_id").val();


    $.ajax({
        url: '/update_floor_plan_image/' + project_id + '/',
        type: 'GET',
        data: {'property_id': property_id, 'resource_id': resource_id, 'canvas_id': canvas_id, 'sub_wireframe_id': sub_wireframe_id},
        success: function (data) {
            $().toastmessage('showToast', {
                        text: "Image saved to floor plan successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
            );

            $("#canvas-image-floor_plans").attr("src", image_src);
            $("#floor_plans-image_name_text").text(image_name);
            $("#apartment-show-image-name-"+property_id).text(image_name);
            $(".canvas-image-floor_plans").attr('src', image_src);
            $("#floor_plans_name").text(image_name);
            $("#trash-property-image-"+property_id).html('<a id="trash-property-image-'+property_id+'" class="link fa-lg" onclick="remove_property_assigned_image('+property_id+', \''+property_id+'\');"><i class="fa fa-trash-o"></i> </a>');

            $(init);

        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });

}

function handleDropFloorEvent( event, ui ) {
    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');
    resource_id = resource_id.replace("image-", "");
    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");

    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var canvas_id = $("#canvas_id").val();

    image_name = image_name[image_name.length-1];

    $.ajax({
        url: '/update_floor_plan_image/' + project_id + '/',
        type: 'GET',
        data: {'property_id': property_id, 'resource_id': resource_id, 'canvas_id': canvas_id},
        success: function (data) {
            $().toastmessage('showToast', {
                        text: "Image saved to floor successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
            );

            $("#canvas-image-floor_plans").attr("src", image_src);
            $("#floor_plans-image_name_text").text(image_name);
            $("#apartment-show-image-name-"+property_id).text(image_name);
            $(".canvas-image-floor_plans").attr('src', image_src);
            $("#floor_plans_name").text(image_name);
            $("#trash-property-image-"+property_id).html('<a id="trash-property-image-'+property_id+'" class="link fa-lg" onclick="remove_property_assigned_image('+property_id+', \''+property_id+'\');"><i class="fa fa-trash-o"></i> </a>');

        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });

}

function handleDropTableKeyEvent( event, ui ) {

    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');

    resource_id = resource_id.replace("image-", "");

    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");

    var project_id = $("#current_project_id").val();
    var property_id = $(this).attr('id').replace("drop-key-plan-", "").replace("apartment-show-key-name-", "").replace("drop-key-apartment-", "");
    image_name = image_name[image_name.length-1];

    update_key_images(project_id, property_id, image_name, resource_id, image_src);
    set_apartment_selected(property_id, 'key_plans');

}

function handleDropKeyEvent( event, ui ) {

    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');
    resource_id = resource_id.replace("image-", "");
    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");

    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();

    image_name = image_name[image_name.length-1];

    update_key_images(project_id, property_id, image_name, resource_id, image_src);
    set_apartment_selected(property_id, 'key_plans');

}

function update_key_images(project_id, property_id, image_name, resource_id, image_src)
{
    var canvas_id = $("#canvas_id").val();
    var sub_wireframe_id = $("#sub_wireframe_id").val();
    
    $.ajax({
        url: '/update_key_plan_image/' + project_id + '/',
        type: 'GET',
        data: {'property_id': property_id, 'resource_id': resource_id, 'canvas_id': canvas_id, 'sub_wireframe_id': sub_wireframe_id},
        success: function (data) {
            $().toastmessage('showToast', {
                        text: "Image saved to canvas successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
            );

            $("#canvas-image-key_plans").attr("src",  image_src);
            $(".canvas-image-key_plans").attr("src",  image_src);
            $("#key_plans-image_name_text").text(image_name);
            $("#key_plans_name").text(image_name);
            $("#apartment-show-key-name-"+property_id).text(image_name);
            $("#trash-property-key-"+property_id).html('<a id="trash-property-image-key-'+property_id+'" ' +
                    'onclick="remove_property_assigned_key('+property_id+');" class="link fa-lg"> <i class="fa fa-trash-o"></i> </a>');

            set_apartment_selected(property_id, 'key_plans');


        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });
}

function handleDropEvent( event, ui ) {
    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');
    resource_id = resource_id.replace("image-", "");
    var normal_src = draggable.attr('normal_src');
    var image_src = draggable.attr('src');
    var image_name = image_src.split("/");

    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var canvas_id = $("#canvas_id").val();

    image_name = image_name[image_name.length-1];
    var sub_wireframe_id = $("#sub_wireframe_id").val();

    $.ajax({
        url: '/update_level_plan_image/' + project_id + '/',
        type: 'GET',
        data: {'floor_id': floor_id, 'resource_id': resource_id, 'canvas_id': canvas_id, 'sub_wireframe_id': sub_wireframe_id},
        success: function (data) {
            $().toastmessage('showToast', {
                        text: "Image saved to level successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
            );

            $("#canvas-image-level_plans").attr("src", image_src.replace("thumbnails/", ""));
            $("#level_plans-image_name_text").text(image_name);
            $(".canvas-image-level_plans").attr('src', image_src);
            $("#level_plans_name").text(image_name);
            $("#trash-level-image-"+floor_id).show();
            $(".floor-image-name-"+floor_id).text(image_name);

            set_apartment_selected($("#current_apartment_setting").val(), 'level_plans');

        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });
}

function handleFloorDropEvent( event, ui ) {
    var draggable = ui.draggable;
    var resource_id = draggable.attr('id');
    resource_id = resource_id.replace("image-", "").replace("floor-content-list-", "");
    var normal_src = draggable.attr('normal_src');

    var image_src = draggable.attr('src');

    var image_name = '';
    if (image_src)
    {
        image_name = image_src.split("/");
    }

    var project_id = $("#current_project_id").val();
    var floor_id = $(this).attr('id').replace("floor-selected-", "").replace("floor-image-name-", "");
    var canvas_id = $("#canvas_id").val();

    image_name = image_name[image_name.length-1];
    var sub_wireframe_id = $("#sub_wireframe_id").val();

    if (resource_id)
    {
       $.ajax({
            url: '/update_level_plan_image/' + project_id + '/',
            type: 'GET',
            data: {'floor_id': floor_id, 'resource_id': resource_id, 'canvas_id': canvas_id, 'sub_wireframe_id': sub_wireframe_id},
            success: function (data) {
                $().toastmessage('showToast', {
                            text: "Image saved to level successfully",
                            sticky: false,
                            position: 'top-right',
                            type: 'success',
                            closeText: ''}
                );

                $("#canvas-image-level_plans").attr("src", image_src.replace("thumbnails/", ""));
                $("#level_plans-image_name_text").text(image_name);
                $(".canvas-image-level_plans").attr('src', image_src);
                $("#level_plans_name").text(image_name);
                $("#trash-level-image-"+floor_id).show();
                $(".floor-image-name-"+floor_id).text(image_name);

            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
            }
        });
    }


}

function remove_property_assigned_key(property_id)
{
    var project_id = $("#current_project_id").val();
    var canvas_id = $("#canvas_id").val();

    $.ajax({
        url: '/remove_key_plan_to_property/' + project_id + '/',
        type: 'GET',
        data: {
            'property_id': property_id, 'canvas_id': canvas_id
        },
        success: function (data) {

            $("#canvas-image-key_plans").attr("src",  '/static/common/img/placeholder.jpg');
            $(".canvas-image-key_plans").attr("src",  '/static/common/img/placeholder.jpg');
            $("#key_plans-image_name_text").text('n/a');
            $("#key_plans_name").text('n/a');
            $("#trash-property-key-"+property_id).text('');
            $("#apartment-show-key-name-"+property_id).text('');
        }
    });

}


function remove_property_assigned_image(property_id, property_name)
    {
        var project_id = $("#current_project_id").val();
        var canvas_id = $("#canvas_id").val();

        $.ajax({
            url: '/remove_floor_plan_to_property/' + project_id + '/',
            type: 'GET',
            data: {
                'property_id': property_id, 'canvas_id': canvas_id
            },
            success: function (data) {

                $("#canvas-image-floor_plans").attr("src",  '/static/common/img/placeholder.jpg');
                $(".canvas-image-floor_plans").attr("src",  '/static/common/img/placeholder.jpg');
                $("#floor_plans-image_name_text").text('n/a');
                $("#floor_plans_name").text('n/a');
                $("#apartment-show-image-name-"+property_id).text('');
                $("#trash-property-image-"+property_id).hide();

            }
        });

    }

function remove_level_assigned_image(floor_id)
{
    var project_id = $("#current_project_id").val();
    var canvas_id = $("#canvas_id").val();

    $.ajax({
        url: '/remove_level_plan_to_floor/' + project_id + '/',
        type: 'GET',
        data: {
            'floor_id': floor_id, 'canvas_id': canvas_id
        },
        success: function (data) {

            $("#canvas-image-level_plans").attr("src",  '/static/common/img/placeholder.jpg');
            $(".canvas-image-level_plans").attr("src",  '/static/common/img/placeholder.jpg');
            $("#level_plans-image_name_text").text('n/a');
            $("#level_plans_name").text('n/a');
            $("#trash-level-image-"+floor_id).hide();
            $("#floor-image-name-"+floor_id).text('n/a');

        }
    });

}

function redraw_apartment_floor_spot(x_value, y_value, canvas_id)
{
 if (!(x_value == '0' && y_value == '0') && !(x_value == 0 && y_value == 0))
 {

     var canvas = document.getElementById(canvas_id);
     var ctx = canvas.getContext('2d');

     ctx.fillStyle = 'blue';

     var size = parseFloat($("#cross-hatch").width());
     var current_value_x = parseFloat(x_value * size);
     var current_value_y = parseFloat(y_value * size);

      var centerX = current_value_x;
      var centerY = current_value_y;
      var radius = 15;

      ctx.globalAlpha = 0.5;
      ctx.beginPath();
      ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
      ctx.fillStyle = 'blue';
      ctx.fill();
      ctx.lineWidth = 0.1;
      ctx.strokeStyle = 'blue';
      ctx.stroke();
 }

}
function redraw_canvas(canvas_name)
{
    var canvas = document.getElementById(canvas_name);
    var ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.canvas.width = ctx.canvas.width;

    $("#selected_x").val('');
    $("#selected_y").val('');

    ctx.strokeStyle = $("#selected-colour").val();

    var size = parseFloat($("#cross-hatch").width());

    ctx.font = "10px Arial";


    ctx.fillText("0",7,(size/2));
    ctx.fillText("1",(size/2),size-7);
    ctx.fillText("1",size-15,(size/2));
    ctx.fillText("0",(size/2)-5,15);
}


function set_apartment_selected(apartment_id, value_selected)
{
    var building_id = $("#current_building_id").val();

    if (parseInt(apartment_id))
    {
        var x_value = $("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val();
        var y_value = $("#apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val();

        var color = "blue";
        redraw_canvas("cross-hatch-floor_plans");
        redraw_apartment_floor_spot(x_value, y_value, "cross-hatch-floor_plans");

        var project_id = $("#current_project_id").val();
        var floor_id = $("#current_floor_id").val();

        $(".listed-image-with-tags").css({"background": "white"});

        $("#current_apartment_setting").val(apartment_id);
        $("#current_apartment_setting_x_coordinate").val(x_value);
        $("#current_apartment_setting_y_coordinate").val(y_value);

        get_image(project_id, floor_id, value_selected, apartment_id);

        if (value_selected == "key_plans")
        {
            get_image(project_id, floor_id, 'floor_plans', apartment_id);

            var canvas = document.getElementById('cross-hatch-level_plans');
            update_key_plan_canvases(canvas, x_value, y_value, color, 400.0);

            canvas = document.getElementById('cross-hatch-key_plans');
            update_key_plan_canvases(canvas, x_value, y_value, color, 180.0);

            canvas = document.getElementById('key-plan-canvas-floor');
            update_key_plan_canvases(canvas, x_value, y_value, color, 180.0);



        }

        $("#selected-template-details").val('');
        $("#property-details-listings").html('');

        $(".apartment-link").removeClass('btn-success').addClass('btn-default');
        $(".had-danger").removeClass('had-danger').removeClass('btn-default').addClass('btn-danger');

        if (parseFloat($("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val()) > 0)
        {
            $("#hotspot_selector_"+apartment_id).removeClass('btn-danger').addClass('had-danger');
            $("#apartment-link-"+apartment_id).removeClass('btn-danger').addClass('had-danger');
            $("#key-apartment-link-"+apartment_id).removeClass('btn-danger').addClass('had-danger');
            $(".apartment-link-"+apartment_id+"-"+building_id).removeClass('btn-danger').addClass('had-danger');
        }

        $(".apartment-link-"+apartment_id+"-"+building_id).removeClass('btn-danger').removeClass('btn-default').addClass('btn-success');

        $(init);
    }
}

function update_key_plan_canvases(canvas, x_value, y_value, color, size)
{
    var ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, size, size);
    ctx.canvas.width = ctx.canvas.width;
    ctx.strokeStyle = $("#selected-colour").val();

    ctx.font = "10px Arial";

    ctx.fillText("0",7,(size/2));
    ctx.fillText("1",(size/2),size-7);
    ctx.fillText("1",size-15,(size/2));
    ctx.fillText("0",(size/2)-5,15);

     ctx.fillStyle = color;

     var current_value_x = parseFloat(x_value * size);
     var current_value_y = parseFloat(y_value * size);

      var centerX = current_value_x;
      var centerY = current_value_y;
      var radius = 10;

      ctx.globalAlpha = 0.5;
      ctx.beginPath();
      ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
      ctx.fillStyle = color;
      ctx.fill();
      ctx.lineWidth = 0.1;
      ctx.strokeStyle = color;
      ctx.stroke();
}


function switch_plans_tab(plan)
{
    // console.log('switch_plans_tab', plan);

    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var building_id = $("#current_building_id").val();
    var first_template_id = $("#first_template_id").val();
    var property_type_id = $("#property_type_filter").val();

    var floor_id = $("#current_floor_id").val();

    var original_select = plan;
    plan = plan.replace(/ /g, "").toString();

    var tag_name = '';
    var tag_id = "";
    var apartment_list = null;

    $(".added-tag-button").click();

    $('#tag-selected option').each(function() {
        option_value = $(this).text().replace(/ /g, "").toString();
        id_option = $(this).val().replace(/ /g, "");

        if (option_value && tag_id == "" && id_option)
        {
            if ($.inArray( plan, [option_value]) >=0)
            {
                tag_name = $(this).text();
                tag_id = id_option;
            }
        }
    });

    if (!tag_id == "")
    {
        var tag_list = $("#list-of-selected-tags");

        tag_list.append('<button id="added-button-tag-'+tag_id+'" onclick="remove_button_tag('+tag_id+', \''+tag_name+'\');" class="btn btn-xs btn-black added-tag-button">'+tag_name+'<i class="fa fa fa-trash-o red small" style="margin-left: 10px;"></i></button> &nbsp;');
        update_image_list(tag_name);
        filter_images();

        $('#tag-selected').val('');
        $("#tag-selected option[value='"+tag_id+"']").hide();
    }


    if (original_select == "Floor Plans" && floor_id)
    {
        //$("#preview-apartment-editor").removeClass('col-xs-2').addClass('col-xs-3');
        //$("#preview-details-editor").removeClass('col-xs-6').addClass('col-xs-5');

        $('a.sub-menu').removeClass('red').addClass('black');
        $('a.sub-menu-Floor').removeClass('black').addClass('red');
        $("#level-plan-apartments").hide();

        $("#floor-plan-apartments").show();
        $("#floor-plans-content-card").html('<div  style="margin: auto; width: 100%;"><img src="/static/common/img/loading-trans.gif" id="loading-gif"></div>');

        $("#key-plan-apartments").hide();
        $(".hotspot-notification").hide();
        $("#basic-level-apartments").hide();

        $.ajax({
            url: '/get_floor_plans_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id, 'selected_property': property_id},
            type: 'GET',
            success: function (data) {
                $("#floor-plan-apartments").html(data.floor_rendered);
                set_apartment_selected(property_id, 'floor_plans');

                if (property_type_id)
                {
                    $(".property-type-selection").each(function() {
                        $(this).val(property_type_id);
                    });

                    filter_property_type(property_type_id);
                }
                else {
                    $(".apartment-details-"+floor_id+"-"+ building_id).show();
                }
            }});


    }

    if (original_select == "Level Plans" && floor_id)
    {
        $(window).queue("namedQueue", function() {
            $('a.sub-menu').removeClass('red').addClass('black');
            $('a.sub-menu-Level').removeClass('black').addClass('red');
            $("#floor-plan-apartments").hide();
            $("#level-plan-apartments").show();
            $("#key-plan-apartments").hide();
            $("#basic-level-apartments").hide();
            $(".hotspot-notification").show();

            setTimeout(function() {
                $(self).dequeue("namedQueue");
             }, 100);

        });

        $(window).queue("namedQueue", function() {

            select_new_floor(floor_id);

            setTimeout(function() {
                $(self).dequeue("namedQueue");
             }, 2000);

        });

        $(window).dequeue("namedQueue");

    }

    if (original_select == "Key Plans" && floor_id)
    {
        $("#key-plan-apartments").show();
        $("#level-plan-apartments").hide();
        $("#floor-plan-apartments").hide();
        $("#multi-level-apartments").hide();
        $(".hotspot-notification").hide();
        $("#basic-level-apartments").hide();

        redraw_main_canvas();

        $.ajax({
            url: '/get_key_plans_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#key-plan-apartments").html(data.key_rendered);

                var apartment_id = $(".apartment-link:visible").first().attr('id');
                apartment_id = apartment_id.replace("key-apartment-link-", "");
                $("#key-apartment-link-"+apartment_id).trigger('click');

                if (property_type_id)
                {
                    $(".property-type-selection").each(function() {
                        $(this).val(property_type_id);
                    });

                    filter_property_type(property_type_id);
                }
                else {
                    $(".apartment-details-"+floor_id+"-"+ building_id).show();
                }

        }});

        var x_value = $("#property-hotspot-x-"+property_id+"-"+building_id).val();
        var y_value = $("#property-hotspot-y-"+property_id+"-"+building_id).val();

        var canvas = document.getElementById('cross-hatch-level_plans');
        var ctx = canvas.getContext("2d");

        ctx.clearRect(0, 0, 400.0, 400.0);
        ctx.canvas.width = ctx.canvas.width;

        ctx.strokeStyle = $("#selected-colour").val();
        var size = 400.0;

        ctx.font = "10px Arial";

        ctx.fillText("0",7,(size/2));
        ctx.fillText("1",(size/2),size-7);
        ctx.fillText("1",size-15,(size/2));
        ctx.fillText("0",(size/2)-5,15);

        if (x_value && y_value)
        {
            redraw_apartment_level_plan_spot(x_value, y_value, 'yellow');
        }

    }

    if (original_select == "Multi Levels" && floor_id)
    {
        $('a.sub-menu').removeClass('red').addClass('black');
        $('a.sub-menu-Multi').removeClass('black').addClass('red');

        $("#key-plan-apartments").hide();
        $("#level-plan-apartments").hide();
        $("#floor-plan-apartments").hide();
        $(".hotspot-notification").hide();
        $("#multi-level-apartments").hide();
        $("#basic-level-apartments").show();

        if ($("#basic-level-apartments").html() == "")
        {
            $.ajax({
            url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#basic-level-apartments").html(data.basic_rendered);
                var apartment_id = $(".apartment-link:visible").first().attr('id');
                $("#"+apartment_id).trigger('click');
            }});
        }

        if ($("li.option-details-tabs.active").hasClass('combination'))
        {
           show_combination_details();
        }

        if ($("li.option-details-tabs.active").hasClass('upgrade'))
        {
            show_upgrade_details();
        }

    }

    if (original_select == "Details" && floor_id)
    {

        $('a.sub-menu').removeClass('red').addClass('black');
        $('a.sub-menu-Floor').removeClass('black').addClass('red');
        $("#key-plan-apartments").hide();
        $("#level-plan-apartments").hide();
        $("#floor-plan-apartments").hide();
        $(".hotspot-notification").hide();
        $("#multi-level-apartments").hide();
        $("#basic-level-apartments").show();

        $.ajax({
        url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
        data: {'building_id': building_id},
        type: 'GET',
        success: function (data) {
            $("#basic-level-apartments").html(data.basic_rendered);
            $("#section-building").html(data.buildings_rendered);
            $("#listing-floor-selection").html(data.listing_floor_rendered);

            $("#selected-template-details").val(first_template_id);
            get_selected_template_for_apartment(first_template_id, '#property-details-listings', '');
            show_template_for_apartment(property_id);

        }});
    }

    if (original_select == "Building" && floor_id)
    {
        $('a.sub-menu').removeClass('red').addClass('black');
        $('a.sub-menu-Floor').removeClass('black').addClass('red');
        $("#key-plan-apartments").hide();
        $("#level-plan-apartments").hide();
        $("#floor-plan-apartments").hide();
        $(".hotspot-notification").hide();
        $("#multi-level-apartments").hide();
        $("#basic-level-apartments").show();

        $.ajax({
        url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
        data: {'building_id': building_id},
        type: 'GET',
        success: function (data) {
            $("#basic-level-apartments").html(data.basic_rendered);
            $("#section-building").html(data.buildings_rendered);
            $("#listing-floor-selection").html(data.listing_floor_rendered);

            var apartment_id = $(".apartment-link:visible").first().attr('id');
            $("#"+apartment_id).trigger('click');

            highlight_property(property_id, building_id, "basic-apartment-link-");
        }});

        show_template_for_apartment(property_id);
    }

    if (original_select == "Listing" && floor_id)
    {
        $('a.sub-menu').removeClass('red').addClass('black');
        $('a.sub-menu-Floor').removeClass('black').addClass('red');

        $("#key-plan-apartments").hide();
        $("#level-plan-apartments").hide();
        $("#floor-plan-apartments").hide();
        $(".hotspot-notification").hide();
        $("#multi-level-apartments").hide();
        $("#basic-level-apartments").show();


        $.ajax({
        url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
        data: {'building_id': building_id},
        type: 'GET',
        success: function (data) {
            $("#basic-level-apartments").html(data.basic_rendered);
            $("#section-building").html(data.buildings_rendered);
            $("#listing-floor-selection").html(data.listing_floor_rendered);

            var apartment_id = $(".apartment-link:visible").first().attr('id');
            $("#"+apartment_id).trigger('click');

            highlight_property(apartment_id, building_id, "basic-apartment-link-");
        }});

    }

    if (property_type_id)
    {
        $(".property-type-selection").each(function() {
            $(this).val(property_type_id);
        });

        filter_property_type(property_type_id);
    }
    else {
        $(".apartment-details-"+floor_id+"-"+ building_id).show();
    }

    $("#image-selector-section").animate({ scrollTop: $('#cross-hatch').offset().top }, 1000);
    $(init);

}


function show_combination_details()
{
    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

    $.ajax({
            url: '/get_combination_filters/' + project_id + '/',
            type: 'GET',
              data: {'property_id': property_id, 'floor_id': floor_id, 'building_id': building_id},
            success: function (data) {
                $("#section-combination").html(data.combined_html);

                highlight_property(property_id, building_id, "basic-apartment-link-");

            }
    });
}

function highlight_property(property_id, building_id, property_field_id)
{
    $(".apartment-link").removeClass('btn-success');
    $(".had-danger").removeClass('had-danger').removeClass('btn-default').addClass('btn-danger');
    var apartment_button = $("#"+property_field_id +property_id+"-"+building_id);

    if (apartment_button.hasClass('btn-danger'))
    {
        apartment_button.removeClass('btn-default').removeClass('btn-default').removeClass('btn-danger').removeClass('btn-danger').addClass('had-danger');
    }

    apartment_button.removeClass('btn-danger').addClass('btn-success');
}

function show_upgrade_details()
{
    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

     $.ajax({
                url: '/get_upgrade_filters/' + project_id + '/',
                type: 'GET',
                  data: {'property_id': property_id, 'floor_id': floor_id, 'building_id': building_id},
                success: function (data) {
                    $("#section-upgrade").html(data.combined_html);

                    highlight_property(property_id, building_id, "basic-apartment-link-");
                }
       });
}

function clear_image_type_from_property(property_id)
{

    var typical_string = '';
    var project_id = $("#current_project_id").val();

    var canvas_id = $("#canvas_id").val();
    var sub_wireframe_id = $("#sub_wireframe_id").val();
    var template_id = $("#template_id").val();

    $.ajax({
        url: '/add_typical_string_to_property/' + project_id + '/',
        type: 'GET',
        data: {'typical_string': typical_string, 'property_id': property_id, 'canvas_id': canvas_id,
               'sub_wireframe_id': sub_wireframe_id, 'template_id': template_id},
        success: function (data) {
            $().toastmessage('showToast', {
                        text: "Image type removed successfully",
                        sticky: false,
                        position: 'top-right',
                        type: 'success',
                        closeText: ''}
            );

            if (data.static_image)
            {
                $("#canvas-image-floor_plans").attr('src', data.static_image);
            }


            set_apartment_selected(property_id, 'floor_plans');
            $("#selector-image-apartment-" + property_id).val('');
            $("#trash-property-type-"+property_id).html('');

        }
    });
}

function myHelper( event ) {

    var list_image = event.currentTarget;

    var id = $(list_image).attr("id");
    var static_image = $(list_image).attr("src");
    var image_name = static_image.split("/");

    image_name = image_name[image_name.length-1];

    return '<div id="draggableHelper" style="background: #fff; border: black 2px solid; z-index: 99999999999; opacity: 0.7 !important; display: inline; width: 125px; padding: 10px;"><div style="position: relative;">' +
           '<img style="max-width: 100px; opacity: 0.8 !important;" src='+static_image+'>' +
           '<span style="position: absolute; top: 1px; left: -1px; z-index: 9999999; float: left; margin-top: -10px; ' +
           'font-weight: 800">'+image_name+'</span>' +
           '</div></div>';
}

// function filter_images_by_tag()
// {
//     var tag_name = $('#tag-selected').find('option:selected').text();
//     var tag_id = $('#tag-selected').find('option:selected').val();
//
//     if (! tag_id == "")
//     {
//         var tag_list = $("#list-of-selected-tags");
//
//         tag_list.append('<button id="added-button-tag-'+tag_id+'" onclick="remove_button_tag('+tag_id+', \''+tag_name+'\');" class="btn btn-xs btn-black added-tag-button">'+tag_name+'<i class="fa fa fa-trash-o red small" style="margin-left: 10px;"></i></button> &nbsp;');
//         update_image_list(tag_name);
//         filter_images();
//
//         $('#tag-selected').val('');
//         $("#tag-selected option[value='"+tag_id+"']").hide();
//     }
//
// }

function update_image_list(tag_name)
{
    $(".image-list-tags").each(function() {
        var tag_list = $(this).find('span');
        var tag_details_list = [];

        $(tag_list).each(function() {
            tag_details_list.push($(this).text());
        });

        if (!$.inArray( tag_name, tag_details_list ))
        {
            $(this).parent().hide();
        }
        else
        {
            $(this).parent().show();
        }
    });
}

function filter_images()
{
    var selected_tag_list = [];
    var selected_tags = $(".added-tag-button");

    $(selected_tags).each(function() {
        selected_tag_list.push($(this).text().replace(/ /g, ""));
    });

    $(".image-list-tags").each(function() {

        var show_image = true;
        var tag_list = $(this).find('span');
        var tag_details_list = [];

        $(tag_list).each(function () {
            tag_details_list.push($(this).text());
        });

        if (selected_tag_list == "") {

            if (tag_details_list == "")
            {
                show_image = true;
            }
            else
            {
                show_image = false;
            }
        }
        else
        {

            var image_list = [];

            $.each(tag_details_list, function (k, v) {
                var value = v.replace(/ /g, "");

                var in_array = $.inArray(value, selected_tag_list) >= 0;

                if (in_array) {
                    image_list.push(value);
                }
            });

            var difference = [];

            jQuery.grep(selected_tag_list, function(el) {
                    if (jQuery.inArray(el, image_list) == -1) difference.push(el);
            });


            if (difference == "")
            {
                show_image = true;
            }
            else
            {
                show_image = false;
            }
        }


        if (!show_image)
        {
            $(this).parent().hide();
        }
        else
        {
            $(this).parent().show();
        }
    });
}

function remove_button_tag(tag_id, tag_name)
{
    //remove filter
    $("#added-button-tag-"+tag_id).remove();

    $("#tag-selected").append("<option value='"+tag_id+"'>"+tag_name+"</option>");

    filter_images();
}


function refresh_floor_hotspots()
{
    redraw_main_canvas();

    var floor_id = $('#current_floor_id').val();
    var project_id = $("#current_project_id").val();
    var building_id = $("#current_building_id").val();
    var property_id = $("#current_apartment_setting").val();

    $.ajax({
        url: '/get_floor_hotspots/' + project_id + '/' + floor_id + '/',
        data: {'building_id': building_id},
        type: 'GET',
        success: function (data) {
            show_current_values(data.apartment_list);
        }
    });

}

function remove_new_coordinates(project_id)
{
    if (confirm("Are you sure you wish to remove these hotspots?"))
    {
        var checked_boxes = $('.select-hotspot:checkbox:checked');
        var current_apartment_setting = $("#current_apartment_setting").val();
        var building_id = $("#current_building_id").val();

        $(checked_boxes).each(function() {

            var apartment_id = $(this).attr("id");
            apartment_id = apartment_id.replace("select-hotspot-", "");

            var x_coordinate = '0';
            var y_coordinate = '0';

            var add_new = $("#add-new-hotspot-"+apartment_id).val();
            var floor_id = $('#hotspot-level-selector-'+apartment_id).val();

            $.ajax({
                url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
                type: 'GET',
                data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new,
                    'floor_id': floor_id, 'current_apartment_setting': current_apartment_setting},
                success: function (data) {

                    $("#property-hotspot-x-"+apartment_id+"-"+building_id).val(x_coordinate);
                    $("#property-hotspot-y-"+apartment_id+"-"+building_id).val(y_coordinate);

                    $("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val(x_coordinate);
                    $("#apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val(y_coordinate);

                    if ($("#hotspot_selector_"+apartment_id+"-"+building_id).hasClass('btn-default'))
                    {
                        $("#hotspot_selector_"+apartment_id+"-"+building_id).removeClass('btn-default').addClass('btn-danger');
                    }


                    $("#apartment_x_"+apartment_id).val(x_coordinate);
                    $("#apartment_y_"+apartment_id).val(y_coordinate);

                    $("#new_apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val('');
                    $("#new_apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val('');

                    $(".refresh-coordinates-x-"+apartment_id).hide();
                    $(".refresh-coordinates-y-"+apartment_id).hide();
                    $(".refresh-coordinate").hide();

                    update_floor_hotspots();
                }
            });

        });


        $().toastmessage('showSuccessToast', 'Co-ordinates of hotspots have been deleted successfully.');
        $("#select-all-apartments").click();

    }

}

function assign_typical_to_floor(type_string, string_id)
{
    var project_id = $("#current_project_id").val();
    var floor_id = $('#current_floor_id').val();
    var building_id = $("#current_building_id").val();

    if (confirm("Are you sure you wish to assign this typical to all selected floors?"))
    {

        var floor_checkboxes = $("input.floor-checkbox:checked");

        $().toastmessage('showSuccessToast', 'Updating floor typicals, please wait.');

        $(floor_checkboxes).each(function() {
            var floor_id = $(this).attr('id');
            floor_id = floor_id.replace("floor-hotspot-", "");
            $(".selector-image-floor-"+floor_id).val(string_id).trigger('change');

            $.ajax({
                url: '/assign_typical_to_floor/' + project_id + '/',
                type: 'GET',
                data: {'type_string': type_string, 'string_id':string_id,
                    'floor_id': floor_id, 'building_id': building_id},
            
                success: function (data) {
                    // var image_text = $("#type-image-selector-"+string_id).text();
                    // $('.apartment-show-image-name-'+floor_id).text(image_text);

                }
            
              });
        });

    }
}

function assign_typical_to_building(type_string, string_id)
{
    var project_id = $("#current_project_id").val();
    var floor_id = $('#current_floor_id').val();
    var building_id = $("#current_building_id").val();

    if (confirm("Are you sure you wish to assign this typical to this building? This can take a couple of minutes. "))
    {
          $().toastmessage('showSuccessToast', 'Updating floor typicals to entire building, please wait.');

          $(".selector-image-floor-"+floor_id).val(string_id).trigger('change');

          $.ajax({
            url: '/assign_typical_to_building/' + project_id + '/',
            type: 'GET',
            data: {'type_string': type_string, 'string_id':string_id,
                   'floor_id': floor_id, 'building_id': building_id},

            success: function (data) {
                select_new_floor(floor_id);
                $().toastmessage('showSuccessToast', 'Property typicals have been saved successfully.');
            }

          });
    }
}


function delete_typical_type(type_string, string_id)
{
    var project_id = $("#current_project_id").val();

    if (confirm("Are you sure you wish to delete this floor plan typical?"))
    {
      $.ajax({
        url: '/delete_image_string_typical/' + project_id + '/',
        type: 'GET',
        data: {'type_string': type_string},

        success: function (data) {

            $('.selector-image-apartment').find("option:contains("+type_string+")").remove();
            $("#string-to-assign-"+string_id).remove();

        }

      });
    }
}

function select_new_floor(floor_id)
{
    redraw_main_canvas();
    $(".hotspot_deselector").hide();
    $(".hotspot_selector").show();
    $(".show_current_hotspots").hide();

    $("#new_level_name").val('');
    $("#number_of_apartments").val('0');

    var apartment_id = $("#current_apartment_setting").val();

    var project_id = $("#current_project_id").val();
    var building_id = $("#current_building_id").val();

    var original_select = $("li.main-tabs.active").text().replace(/ /g, "").replace(/\n/g, "");

    if (original_select == "LevelPlan")
    {
        $("#apartment-content-card").html('<div  style="margin: auto; width: 100%;"><img src="/static/common/img/loading-trans.gif" id="loading-gif"></div>');

        $.ajax({
            url: '/get_level_plans_screen/' + project_id + '/' + building_id + '/' + floor_id +'/',
            type: 'GET',
            success: function (data) {

                $("#floor-tab-content").css('background','#fff');
                $("#building-selected").show();

                if (data.first_apartment_id)
                {
                    $("#current_apartment_setting").val(data.first_apartment_id);
                }

                $("#level-plan-apartments").html(data.rendered);

                if (data.first_apartment_id) {
                    set_apartment_selected($("#current_apartment_setting").val(), 'level_plans');
                }

                var static_image = data.static_image;
                $("#canvas-image-level_plans").attr('src', static_image);
                $(".canvas-image-level_plans").attr('src', static_image);
                $("#level_plans-image_name_text").html(data.image_name);
                $("#level_plans_name").text(data.image_name);

                //show_current_values(data.apartment_list);

            }
        });
    }

    if (original_select == "FloorPlan")
    {

        redraw_main_canvas();

        $("#floor-plan-apartments").show();
        $("#floor-plans-content-card").html('<div  style="margin: auto; width: 100%;"><img src="/static/common/img/loading-trans.gif" id="loading-gif"></div>');

        $.ajax({
            url: '/get_floor_plans_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id, 'selected_property': apartment_id},
            type: 'GET',
            success: function (data) {
                $("#floor-plan-apartments").html(data.floor_rendered);
                $(".apartment-link-"+apartment_id+"-"+building_id).addClass('btn-success');

                apartment_id = $(".apartment-link:visible").first().attr('id');
                if (parseInt(apartment_id))
                {
                    apartment_id = apartment_id.replace("apartment-link-", "");
                    $("#apartment-link-"+apartment_id).trigger('click');
                    $("#current_apartment_setting").val(apartment_id);
                }

            }});
    }

    if (original_select == "KeyPlan")
    {

        $.ajax({
            url: '/get_key_plans_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#key-plan-apartments").html(data.key_rendered);

                apartment_id = $(".apartment-link:visible").first().attr('id');

                if (parseInt(apartment_id))
                {
                    apartment_id = apartment_id.replace("key-apartment-link-", "");
                    $("#apartment-link-"+apartment_id).trigger('click');
                    $("#current_apartment_setting").val(apartment_id);
                }
            }});

    }

    if (original_select == "Options")
    {

        $.ajax({
        url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
        data: {'building_id': building_id},
        type: 'GET',
        success: function (data) {
            $("#basic-level-apartments").html(data.basic_rendered);
            var apartment_id = $(".apartment-link:visible").first().attr('id');
            $("#"+apartment_id).trigger('click');
            show_template_for_apartment(apartment_id);
        }});


        if ($("li.option-details-tabs.active").hasClass('combination'))
        {
           show_combination_details();
        }

        if ($("li.option-details-tabs.active").hasClass('upgrade'))
        {
            show_upgrade_details();
        }
    }

    if (original_select == "Details")
    {

        $.ajax({
            url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#basic-level-apartments").html(data.basic_rendered);
                $("#section-building").html(data.buildings_rendered);
                $("#listing-floor-selection").html(data.listing_floor_rendered);

                apartment_id = $(".apartment-link:visible").first().attr('id');
                if (parseInt(apartment_id)) {
                    apartment_id = apartment_id.replace('apartment-link-');
                    $("#current_apartment_setting").val(apartment_id);
                }
            }});

        var first_template_id = $("#first_template_id").val();
        $("#selected-template-details").val(first_template_id);
        get_selected_template_for_apartment(first_template_id, '#property-details-listings', '')
    }

    if (original_select == "Building")
    {
         $.ajax({
            url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#basic-level-apartments").html(data.basic_rendered);
                $("#section-building").html(data.buildings_rendered);
                $("#listing-floor-selection").html(data.listing_floor_rendered);

                apartment_id = $(".apartment-link:visible").first().attr('id');
                if (parseInt(apartment_id)) {
                    apartment_id = apartment_id.replace('apartment-link-');
                    $("#current_apartment_setting").val(apartment_id);
                }
            }});
    }

    if (original_select == "Listing")
    {
        $.ajax({
            url: '/get_details_screen/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {
                $("#basic-level-apartments").html(data.basic_rendered);
                $("#section-building").html(data.buildings_rendered);
                $("#listing-floor-selection").html('').html(data.listing_floor_rendered);

                apartment_id = $(".apartment-link:visible").first().attr('id');
                if (parseInt(apartment_id)) {
                    apartment_id = apartment_id.replace('apartment-link-');
                    $("#current_apartment_setting").val(apartment_id);
                }

                show_listing_for_property(apartment_id);
            }});
    }

    $("#current_floor_id").val(floor_id);
    $(".floor-checkbox").attr("checked", false);

    $(".apartment-details").hide();
    var apartment_class_string = ".apartment-details-"+floor_id+"-"+ building_id;

    $(apartment_class_string).show();

    $(".building-floor-select").hide();
    $(".building-floor-select-"+building_id).show();

    $(".linked-floor-select").removeClass('selected');
    $(".floor-details").removeClass('selected');
    $("#floor-details-"+floor_id).removeClass('selected');

    $("#floor-selected-"+floor_id).addClass('selected');
    $("#floor-hotspot-" + floor_id).prop('checked', true);

    $(".listed-image-with-tags").css({"background": "white"});

    $(init);
    update_floor_hotspots();

}

function get_image(project_id, floor_id, value_selected, apartment)
{
    if (parseInt(apartment))
    {
        $.ajax({
            url: '/get_image_selection_details/' + project_id + '/' + floor_id + '/' + value_selected + '/',
            type: 'GET',
            data: {'apartment': apartment, 'value_selected': value_selected},
            success: function (data) {
                var static_image = data.static_image;

                if (value_selected == "level_plans")
                {
                    $("#floor-plan-image img").attr('src', static_image);
                }

                $("#"+value_selected+"-image_name_text").text(data.image_name);
                $("#canvas-image-"+value_selected).attr('src', static_image);
                $(".canvas-image-"+value_selected).attr('src', static_image);
                $("#"+value_selected+"_name").text(data.image_name);
            }

        });
    }
}

function get_all_images(project_id, floor_id, apartment)
{

    $.ajax({
        url: '/get_image_selection_details/' + project_id + '/' + floor_id + '/' + value_selected + '/',
        type: 'GET',
        data: {'apartment': apartment, 'value_selected': 'all_images'},
        success: function (data) {
            var static_image = data.static_image;

            if (value_selected == "level_plans")
            {
                $("#floor-plan-image img").attr('src', static_image);
            }

            $("#"+value_selected+"-image_name_text").text(data.image_name);
            $("#canvas-image-"+value_selected).attr('src', static_image);
            $(".canvas-image-"+value_selected).attr('src', static_image);
            $("#"+value_selected+"_name").text(data.image_name);
        }

    });
}

function delete_apartment_spot(hotspot_id, apartment_id, counter)
{
    var project_id = $("#current_project_id").val();
     $.ajax({
         url: '/delete_hotspot/' + project_id + '/' + hotspot_id + '/',
         type: 'GET',
         data: {'type': 'hotspot'},
         success: function (data) {

             var apartment = $("#current_apartment_setting").val();
             var project_id = $("#current_project_id").val();
             var floor_id = $("#current_floor_id").val();
             get_image(project_id, floor_id, "level_plans", apartment);

              $().toastmessage('showToast', {
                 text     : 'Coordinates have been deleted successfully',
                 sticky   : false,
                 position : 'top-right',
                 type     : 'notice',
                 closeText: ''
            });

             $("#spot-list-"+apartment_id+"-"+counter).remove();
         }
     });
}

function add_another_hotspot(project_id, apartment_id)
{

    $("#add-new-hotspot-"+apartment_id).val(true);
    var add_new = $("#add-new-hotspot-"+apartment_id).val();
    var selected_floor = $("#hotspot-level-selector-"+apartment_id).val();
    var current_apartment_setting = $("#apartment-value").val();
    var building_id = $("#current_building_id").val();

    $(".floor-apartment-details-"+ selected_floor).show();
    $(".hotspot-level-selector").val(selected_floor);

    $("#save-plus-"+apartment_id).attr('disabled', true);

    var x_coordinate = $("#new_apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val();
    var y_coordinate = $("#new_apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val();

    if (x_coordinate == '')
    {
        x_coordinate = $("#property-hotspot-x-"+apartment_id+"-"+building_id).val();
        y_coordinate = $("#property-hotspot-y-"+apartment_id+"-"+building_id).val();
    }

    $("#current_apartment_setting").val(apartment_id);
    $("#cross-hatch").unbind("click").css('cursor', 'default');

    $.ajax({
        url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
        type: 'GET',
        data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new,
            'floor_id': selected_floor, 'current_apartment_setting': current_apartment_setting},
        success: function (data) {

            if (data.add_row)
            {
               $("#current-list-hotspots-"+apartment_id).append('<div class="row" id="spot-list-'+current_apartment_setting+'-'+current_apartment_setting+'">' +
                   '<div class="col-sm-3"><b>Floor:</b> '+data.floor_name +'</div>' +
                    '<div class="col-sm-5"><b>Apartment:</b> '+data.property_name+'</div><div class="col-sm-2">' +
                    '<a data-toggle="tooltip" data-position="top" title="View this hotspot" ' +
                    'onclick="show_selected_hotspot('+apartment_id+', '+x_coordinate+','+y_coordinate+', \'red\');">' +
                    '<i class="fa fa-eye"></i> </a></div><div class="col-sm-2"><a data-toggle="tooltip" data-position="top" title="Delete this hotspot" ' +
                    'onclick="delete_apartment_spot('+data.hotspot_id+', '+apartment_id+', '+current_apartment_setting+');"><i class="fa fa-trash"></i> </a></div></div>');
            }


            $("#property-hotspot-x-"+apartment_id+"-"+building_id).val(x_coordinate);
            $("#property-hotspot-y-"+apartment_id+"-"+building_id).val(y_coordinate);

            $("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val(x_coordinate);
            $("#apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val(y_coordinate);

            $("#apartment_x_"+apartment_id).val(x_coordinate);
            $("#apartment_y_"+apartment_id).val(y_coordinate);

            $("#add-new-hotspot-"+apartment_id).val("False");

            var floor_id = $("#current_floor_id").val();

            get_image(project_id, floor_id, "level_plans", apartment_id);

            redraw_main_canvas();
            redraw_apartment_lists();
            $(init);

            $().toastmessage('showToast', {
                text: 'Hotspot saved. Please add another set of coordinates',
                sticky: false,
                position : 'top-right',
                type     : 'notice',
                closeText: ''}
            );

    },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });

}

function save_new_coordinate(project_id, apartment_id)
{
    var property_id = $("#current_apartment_setting").val();
    var building_id = $("#current_building_id").val();

    $("li.apartment-content-list-open").removeClass('apartment-content-list-open').addClass('apartment-content-list');
    $("li.apartment-content-list").removeClass('apartment-content-list-open').addClass('apartment-content-list');
    $(".apartment-content-list-"+apartment_id).addClass('apartment-content-list').removeClass('apartment-content-list-open');

    $(".hotspot_deselector").hide();
    $(".hotspot_selector").show();

    var selected_floor = $("#current_floor_id").val();
    var current_apartment_setting = $("#apartment-value").val();

    $("#add-new-hotspot-"+property_id).attr('value', 'true');
    $(".floor-apartment-details").hide();

    $(".floor-apartment-details-"+ selected_floor).show();

    var x_coordinate = $("#new_apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val();
    var y_coordinate = $("#new_apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val();
    var hotspot_order = $("#apartment_set_order_"+apartment_id+"-"+building_id).val();

    if (x_coordinate == '')
    {
        x_coordinate = $("#property-hotspot-x-"+apartment_id+"-"+building_id).val();
        y_coordinate = $("#property-hotspot-y-"+apartment_id+"-"+building_id).val();
    }

    var add_new = $("#add-new-hotspot-"+apartment_id).val();

    $("#current_apartment_setting").val(apartment_id);
    $("#cross-hatch").unbind("click").css('cursor', 'default');

    var linked_floor = $("#hotspot-select-floor-"+apartment_id).val();
    var linked_property = $("#hotspot-select-aprt-"+apartment_id).val();

    $.ajax({
        url: '/save_new_floor_hotspots/' + project_id + '/' + apartment_id + '/',
        type: 'GET',
        data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate, 'add_new': add_new, 'floor_id': selected_floor,
                'selected_apartment': current_apartment_setting, 'linked_floor': linked_floor, 'linked_property': linked_property,
                'hotspot_order': hotspot_order},
        success: function (data) {

            update_floor_hotspots();

            $("#property-hotspot-x-"+apartment_id+"-"+building_id).val(x_coordinate);
            $("#property-hotspot-y-"+apartment_id+"-"+building_id).val(y_coordinate);

            $("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val(x_coordinate);
            $("#apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val(y_coordinate);

            $("#new_apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val('');
            $("#new_apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val('');

            $("#apartment_x_"+apartment_id).val(x_coordinate);
            $("#apartment_y_"+apartment_id).val(y_coordinate);

            redraw_main_canvas();
            redraw_apartment_spot(x_coordinate, y_coordinate, 'blue');
            add_event_listener(x_coordinate, y_coordinate);

            var coord_exists = $('#refresh-coordinates-x-'+apartment_id).length;
            if (coord_exists == 0)
            {
                $(".refresh-coordinate").show();
            }

            $(".save-button-"+apartment_id).attr('disabled', 'disabled');
            $("#show_current_hotspots_"+apartment_id+"-"+building_id).hide();
            $("#hotspot_deselector_"+apartment_id+"-"+building_id).hide();

            $(".apartment-link").removeClass('btn-success').addClass('btn-default');
            $(".apartment-link-"+property_id+"-"+building_id).removeClass('btn-default').addClass('btn-success');

            $(".save-button").show();
            $("#cross-hatch").off("mousemove", mouse_move);

            document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);


            $("#add-new-hotspot-"+apartment_id).val("False");
            $().toastmessage('showSuccessToast', 'New co-ordinates for Property saved successfully.');

    },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage); // Optional
        }
    });


}

function copy_new_coordinates(project_id)
{
    if (confirm("Are you sure you wish to copy these hotspots to the selected floors below?"))
    {

        $(window).queue("namedQueue", function() {
            waitingDialog.show('Copying, please wait...', {dialogSize: 'sm', progressType: 'info'});


            var current_apartment_settings = $('.select-hotspot:checkbox:checked');
            var selected_floors = $('.floor-checkbox:checkbox:checked');


            var floors_len = $(selected_floors).length;
            var count = 0;
            var current_floor_id = 0;
            var building_id = $("#current_building_id").val();

            $(selected_floors).each(function(index, element) {
                var floor_id = $(this).attr("id");
                floor_id = floor_id.replace("floor-hotspot-", "");

                $(current_apartment_settings).each(function() {
                    var apartment_id = $(this).attr("id");
                    split_value = apartment_id.replace("select-hotspot-", "").split('-');

                    apartment_id = split_value[0];
                    building_id = split_value[1];

                    jQuery.get("/copy_apartment_to_floor/"+project_id+"/"+apartment_id+"/"+floor_id+"/",
                        function (data) {

                            if (data.floor_id != current_floor_id)
                            {
                                count ++;
                                current_floor_id = data.floor_id;
                            }

                            if (data.saved)
                            {
                                $(".apartment-details-"+apartment_id+"-"+building_id).removeClass('btn-default').addClass('btn-danger');
                            }
                    });
                });

                selected_floors.remove(floor_id);

            });

             setTimeout(function() {
                $(self).dequeue("namedQueue");
             }, 3000);

        });


        $(window).queue("namedQueue", function() {
            waitingDialog.hide();

            $().toastmessage('showToast', {
                 text     : 'Coordinates of hotspots have been copied successfully',
                 sticky   : false,
                 stayTime: 3000,
                 position : 'top-right',
                 type     : 'notice',
                 closeText: ''
            });
            refresh_floor_hotspots();

            var self = this;
              setTimeout(function() {
                $(self).dequeue("namedQueue");
              }, 1000);
        });

        $(window).dequeue("namedQueue");

    }
}

function redraw_apartment_lists()
{
    var apartment_lists = $("#apartment_list").val();
    if (apartment_lists)
    {
        var apartment_list = $.parseJSON(apartment_lists.toString());
        show_current_values(apartment_list);
    }

}


function select_all_properties()
{
    var floor_id = $("#current_floor_id").val();
    $(".apartment-checkbox-"+floor_id).prop('checked', $("#select-all-apartments").prop("checked"));
    $(".hotspot-checkbox-"+floor_id).prop('checked', $("#select-all-apartments").prop("checked"));
}



function highlight_reassignment_hotspot(apartment_id, x_value, y_value)
{
    var building_id = $("#current_building_id").val();

    $("#show_current_hotspots_"+apartment_id+"-"+building_id).toggle();

    if ($("#show_current_hotspots_"+apartment_id+"-"+building_id).is(":visible")) {
        redraw_apartment_spot(x_value, y_value, 'green');
    }
    else
    {
        refresh_floor_hotspots();
    }
}

function redraw_main_canvas()
{
    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.canvas.width = ctx.canvas.width;

    $("#selected_x").val('');
    $("#selected_y").val('');

    ctx.strokeStyle = $("#selected-colour").val();

    var size = parseFloat($("#cross-hatch").width());

    ctx.font = "12px Arial";

    ctx.fillText("0",7,(size/2));
    ctx.fillText("1",(size/2),size-7);
    ctx.fillText("1",size-15,(size/2));
    ctx.fillText("0",(size/2)-5,15);
}

function update_floor_hotspots()
{
    redraw_main_canvas();

    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();
    var checked_boxes = $('.floor-checkbox:checkbox:checked');
    var total_showing = 0;

    $(checked_boxes).each(function(index, object) {

        var project_id = $(this).attr('project_id');
        var checkbox_id = $(this).attr('id');
        checkbox_id = checkbox_id.replace("floor-hotspot-", "");

            $.ajax({
                url: '/get_floor_hotspots/' + project_id + '/' + checkbox_id + '/',
                data: {'building_id': building_id},
                type: 'GET',
                success: function (data) {

                    total_showing += 1;
                    $("#total_showing").val(total_showing);

                    //console.log('update_floor_hotspots success');

                    if (data.apartment_list)
                    {
                        
                        show_current_values(data.apartment_list);
                    }

                }
            });

    });

    $("#cross-hatch").off("mousemove", mouse_move);
    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);
}



function skin_floor_hotspots()
{
    //console.log('skin_floor_hotspots');
    var project_id = $("#current_project_id").val();
    redraw_main_canvas();

    var checked_boxes = $('.floor-checkbox:checkbox:checked');
    $(checked_boxes).each(function(index, object) {

        var project_id = $(this).attr('project_id');
        var checkbox_id = $(this).attr('id');
        var floor_id = checkbox_id.replace("floor-hotspot-", "");
        var building_id = $("#current_building_id").val();

        var total_showing = 0;
        $.ajax({
            url: '/get_floor_hotspots/' + project_id + '/' + floor_id + '/',
            data: {'building_id': building_id},
            type: 'GET',
            success: function (data) {

                var apartments = $.parseJSON(data.apartment_list);
                $(apartments).each(function (index, object) {
                    redraw_apartment_spot(object.x_coordinate, object.y_coordinate, 'yellow');
                });

                //$("#apartment-linked-details tr.apartment-details-"+floor_id+"-"+building_id).remove();
                //var hot_spot_html = data.linked_hotspots;
                //hot_spot_html = hot_spot_html.replace('<table class="crunched-table" style="padding: 0; border-top: none;" id="linked-hotspots-table">', '').replace("</table>", "");
                //$("#apartment-linked-details").append(hot_spot_html);

            }
        });
    });

    $("#cross-hatch").off("mousemove", mouse_move);
    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

}

function mouse_move(evt) {

    var x_value = $("#x_coordinate").val();
    var y_value = $("#y_coordinate").val();

    var canvas = document.getElementById("cross-hatch");
    var ctx = canvas.getContext("2d");
    var mousePos = getMousePos(canvas, evt);
    var size = parseFloat($("#cross-hatch").width());

    var x = (parseFloat(mousePos.x) / size).toFixed(4);
    var y = (parseFloat(mousePos.y) / size).toFixed(4);

    var message = 'x: ' + x + ' y: ' + y;
    ctx.canvas.width = ctx.canvas.width;
    ctx.strokeStyle = $("#selected-colour").val();

    ctx.font = "10px Arial";

    ctx.fillText("0",7,(size/2));
    ctx.fillText("1",(size/2),size-7);
    ctx.fillText("1",size-15,(size/2));
    ctx.fillText("0",(size/2)-5,15);

    //redraw selected apartment to spot select
    redraw_apartment_spot(x_value, y_value, 'blue');


    ctx.fillStyle = 'yellow';

    var selected_x = $("#selected_x").val();
    var selected_y = $("#selected_y").val();

    redraw_apartment_spot(selected_x, selected_y, 'yellow');


    mousePos = getMousePos(canvas, evt);
    message = 'Mouse position: ' + parseFloat(mousePos.x).toFixed(4) + ',' + parseFloat(mousePos.y).toFixed(4);
    // writeMessage(canvas, message);


}

function add_event_listener(x_value, y_value)
{
    //console.log('adding event listener');
    //redraw entire canvas stuff to remove this floor's apartments.

    $("#x_coordinate").val(x_value);
    $("#y_coordinate").val(y_value);
    var canvas = document.getElementById("cross-hatch");

    canvas.addEventListener('mousemove', mouse_move);


}

function update_property_show_links()

{}


function click_to_close_hotspot(apartment_id)
{
    //location.reload();

    var building_id = $("#current_building_id").val();

    $("li.apartment-content-list-open").removeClass('apartment-content-list-open').addClass('apartment-content-list');
    $(".apartment-content-list-"+apartment_id).addClass('apartment-content-list').removeClass('apartment-content-list-open');

    $("#show_current_hotspots_"+apartment_id+"-"+building_id).hide();
    $("#hotspot_deselector_"+apartment_id+"-"+building_id).hide();

    if (parseFloat($("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val()) > 0)
    {
        //has value success
        //console.log('click_to_close_hotspot has data');
        $("#hotspot_selector_"+apartment_id+"-"+building_id).show().addClass('btn-success'); //.css("background", 'yellow');
    }
    else
    {
        //does not have value
        //console.log('click_to_close_hotspot does not has data');
        $("#hotspot_selector_"+apartment_id+"-"+building_id).show().removeClass('btn-danger').addClass('btn-default'); //.css("background", 'blue');
    }

    $("#cross-hatch").unbind("click").css('cursor', 'default');
    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

    $(".save-button").show();
    update_floor_hotspots();

}


function relMouseCoords(event){

    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent);

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    //console.log('relMouseCoords', canvasY, canvasX);

    return {'x':canvasX, 'y':canvasY}
}

var canvas_click = function(e) {

  var current_apartment = $("#current_apartment_setting").val();
  var building_id = $("#current_building_id").val();

  if (current_apartment)
  {

      var canvas = document.getElementById("cross-hatch");

      //console.log('cross hatch click - current apartment');
      $(".save-button-"+current_apartment).removeAttr('disabled');
      
      var offset = $(this).offset();

      var totalOffsetX = $(this).offsetLeft - $(this).scrollLeft;
      var totalOffsetY = $(this).offsetTop - $(this).scrollTop;

      //console.log('test', totalOffsetX, totalOffsetY);

      var relativeX = (e.pageX - offset.left);
      var relativeY = (e.pageY - offset.top);

      //console.log('offset', offset, relativeX , relativeY);

      var zoom_factor_level_plans = $("#zoom_factor_level_plans").val();
      var size = parseFloat($("#cross-hatch").width());
      
      //console.log('zoom_factor_level_plans', zoom_factor_level_plans, 'size', size);

      if (parseInt(zoom_factor_level_plans) > 0)
      {
          var increment = 0.3;
          increment = increment * zoom_factor_level_plans;

          //console.log('zooms', zoom_factor_level_plans);
          //console.log('increment', increment);

          size = parseFloat((size * (increment)) + size);

          //console.log('size', size);

      }

      var x = parseFloat(relativeX) / parseFloat(size);
      var y = parseFloat(relativeY) / parseFloat(size);

      x = (x).toFixed(4);
      y = (y).toFixed(4);

      show_selected(canvas, x, y);
      show_current_values(canvas);

      $("#new_apartment_set_x_coordinate_"+current_apartment+"-"+building_id).val(x);
      $("#new_apartment_set_y_coordinate_"+current_apartment+"-"+building_id).val(y);

  }
  else
  {
      var floor_id = $("#current_floor_id").val();
      select_new_floor(floor_id);
  }

};

function click_to_set_hotspot(apartment_id)
{
    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

    $("li.apartment-content-list-open").removeClass('apartment-content-list-open').addClass('apartment-content-list');
    $(".apartment-content-list-"+apartment_id).removeClass('apartment-content-list').addClass('apartment-content-list-open');

    redraw_main_canvas();

    $(".save-button").hide();
    $("#cross-hatch").bind("click", canvas_click).bind("mousemove", mouse_move);
    $("#current_apartment_setting").val(apartment_id);

    $(".hotspot_deselector").hide();
    $(".hotspot_selector").show();

    $("#hotspot_deselector_"+apartment_id+"-"+building_id).show().removeClass('btn-default').removeClass('btn-danger').addClass('btn-success');
    $("#hotspot_selector_"+apartment_id+"-"+building_id).hide();

    $(".show_current_hotspots").hide();
    $(".newly-selected").val('');
    $(".floor-apartment-details").hide();
    $(".floor-apartment-details-"+ floor_id).show();
    $(".hotspot-level-selector").val(floor_id);

    $("#show_current_hotspots_"+apartment_id+"-"+building_id).show();

    $(".apartment-checkbox").attr('checked', false);
    $("#select-hotspot-"+ apartment_id).attr('checked', true);

    if ($("#show_current_hotspots_"+apartment_id+"-"+building_id).is(":visible"))
    {
        $("#hotspot_selector_"+apartment_id+"-"+building_id).addClass('btn-danger'); //.css("background", 'green');

        var x_value = $("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val();
        var y_value = $("#apartment_set_y_coordinate_"+apartment_id+"-"+building_id).val();

        add_event_listener(x_value, y_value);

        $('#cross-hatch').css('cursor', 'crosshair');

        //redraw selected apartment to spot select
        redraw_apartment_spot(x_value, y_value, 'blue');

        $().toastmessage('showToast', {
            text: 'Apartment selected. Please click on the floor plan to select new co-ordinates',
            sticky: false,
            position : 'top-right',
            type     : 'notice',
            closeText: ''}
        );

    }
    else
    {
        $("#hotspot_selector_"+apartment_id+"-"+building_id).removeClass('btn-danger');
        $('#cross-hatch').css('cursor', 'default');
    }
}


var canvas_click_new = function(e) {


  //console.log('cross hatch click - current apartment');
  $(".save-button-new").removeAttr('disabled');
  $(".save-button").removeAttr('disabled');

  var offset = $(this).offset();

  var relativeX = (e.pageX - offset.left);
  var relativeY = (e.pageY - offset.top);

  var size = parseFloat($("#cross-hatch").width());

    //console.log('size', size);

  var x = parseFloat(relativeX) / size;
  var y = parseFloat(relativeY) / size;

    //console.log('size', x, y);

  x = (x).toFixed(4);
  y = (y).toFixed(4);

  var canvas = document.getElementById("cross-hatch");
  var ctx = canvas.getContext("2d");

  show_selected(canvas, x, y);

  show_current_values(canvas);

  $(".new_linked_property_x_coordinate").val(x);
  $(".new_linked_property_y_coordinate").val(y);

};

function click_to_set_new_hotspot()
{

    $(".new_linked_property_x_coordinate").val('');
    $(".new_linked_property_y_coordinate").val('');


    $("#hotspot_selector_new").hide();
    $("#hotspot_deselector_new").show().addClass('btn-warning');
    $("#show_new_hotspot").show();

    var floor_id = $("#current_floor_id").val();
    $(".floor-apartment-details").hide();
    $(".floor-apartment-details-"+ floor_id).show();
    $(".hotspot-level-selector").val(floor_id);

    redraw_main_canvas();

    $("#cross-hatch").bind("click", canvas_click_new).bind("mousemove", mouse_move);

    if ($("#show_new_hotspot").is(":visible"))
    {
        add_event_listener(0, 0);

        $().toastmessage('showToast', {
            text: 'Please click on the floor plan to select new co-ordinates',
            sticky: false,
            position : 'top-right',
            type     : 'notice',
            closeText: ''}
        );

        $('#cross-hatch').css('cursor', 'crosshair');
    }
    else
    {
        $("#show_new_hotspot").removeClass('btn-danger');
        $('#cross-hatch').css('cursor', 'default');
    }
}




function click_to_close_new_hotspot()
{
    $("#hotspot_selector_new").show();
    $("#hotspot_deselector_new").hide().removeClass('btn-info');
    $("#show_new_hotspot").hide();

    $("#cross-hatch").unbind("click").css('cursor', 'default');
    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

    $(".save-button").show();
    update_floor_hotspots();
}

function save_new_coordinate_linked()
{
    var project_id = $("#current_project_id").val();
    var new_linked_property = $("#new-hotspot-select-aprt").val();

    var test_link = $("#adding-new-hotspot-select-aprt").val();

    //console.log('new_linked_property', new_linked_property, 'test_link', test_link);
    
    var new_floor_id = $("#new-hotspot-select-floor").val();
    var floor_id = $("#current_floor_id").val();

    var x_coordinate = $("#new_linked_property_x_coordinate").val();
    var y_coordinate = $("#new_linked_property_y_coordinate").val();

    var new_hotspot_name = $("#new_hotspot_name").val();
    
    if (new_hotspot_name == "" && new_linked_property == null)
    {
        $().toastmessage('showToast', {
            text: 'Please enter a hotspot name or linked property',
            sticky: false,
            position : 'top-right',
            type     : 'error',
            closeText: ''}
        );
    }
    else {

        $.ajax({
            url: '/save_new_linked_floor_hotspots/' + project_id + '/',
            type: 'GET',
            data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate,
                    'floor_id': floor_id, 'property_id': new_linked_property,
                    'new_hotspot_name': new_hotspot_name, 'new_floor_id': new_floor_id},
            success: function (data) {

                $("#linked-apartment_set_x_coordinate_"+new_linked_property).val(x_coordinate);
                $("#linked-apartment_set_y_coordinate_"+new_linked_property).val(y_coordinate);

                var floor_id = $("#current_floor_id").val();

                $().toastmessage('showToast', {
                    text: 'Linked property and hotspot saved successfully.',
                    sticky: false,
                    position : 'top-right',
                    type     : 'notice',
                    closeText: ''}
                );

                $("#new-hotspot-select-floor").val('');
                $("#new_linked_property_x_coordinate").val('');
                $("#new_linked_property_y_coordinate").val('');
                $("#new_hotspot_name").val('');

                select_new_floor(floor_id);

        },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
            }
        });
    }
}


function save_hotspot_coordinate(project_id, hotspot_id)
{
    var floor_id = $("#current_floor_id").val();

    var x_coordinate = $("#linked-new_apartment_set_x_coordinate_"+hotspot_id).val();
    var y_coordinate = $("#linked-new_apartment_set_y_coordinate_"+hotspot_id).val();

    if (x_coordinate == "" || y_coordinate == "")
    {
        x_coordinate = $("#new_linked_property_x_coordinate").val();
        y_coordinate = $("#new_linked_property_y_coordinate").val();
    }

    var new_hotspot_name = $("#hotspot-name-"+hotspot_id).val();
    var linked_property = $("#new-hotspot-select-aprt").val();

    if (new_hotspot_name == "" && linked_property == "")
    {
        $().toastmessage('showToast', {
            text: 'Please enter a hotspot name',
            sticky: false,
            position : 'top-right',
            type     : 'error',
            closeText: ''}
        );
    }
    else {

        $.ajax({
            url: '/save_new_linked_floor_hotspots/' + project_id + '/',
            type: 'GET',
            data: {'x_coordinate': x_coordinate, 'y_coordinate': y_coordinate,
                    'floor_id': floor_id, 'property_id': linked_property,
                    'new_hotspot_name': new_hotspot_name, 'hotspot_id': hotspot_id},
            success: function (data) {

                //console.log('hotspot_id', hotspot_id);

                $("#linked-apartment_set_x_coordinate_"+hotspot_id).val(x_coordinate);
                $("#linked-apartment_set_y_coordinate_"+hotspot_id).val(y_coordinate);

                $("#linked-new_apartment_set_x_coordinate_"+hotspot_id).val('');
                $("#linked-new_apartment_set_y_coordinate_"+hotspot_id).val('');


                $("#linked_hotspot_selector_"+hotspot_id).text(data.property_name);
                $("#linked_hotspot_deselector_"+hotspot_id).text(data.property_name);

                var floor_id = $("#current_floor_id").val();

                $().toastmessage('showToast', {
                    text: 'Linked property and hotspot saved successfully.',
                    sticky: false,
                    position : 'top-right',
                    type     : 'notice',
                    closeText: ''}
                );
        },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
            }
        });
    }

    select_new_floor(floor_id);
}

function click_to_close_linked_hotspot(hotspot_id)
{
    $(".new_linked_property_x_coordinate").val('');
    $(".new_linked_property_y_coordinate").val('');
    $("#cross-hatch").unbind("click", canvas_click_new);
    $("#cross-hatch").unbind("click").css('cursor', 'default');

    $("li.apartment-content-list-open").removeClass('apartment-content-list-open');

    $("#linked-show_current_hotspots_"+hotspot_id).hide();
    $("#linked_hotspot_deselector_"+hotspot_id).hide().addClass('btn-black');
    $("#linked_hotspot_selector_"+hotspot_id).show().addClass('btn-black');

    document.getElementById("cross-hatch").removeEventListener('mousemove', mouse_move);

    $(".save-button").show();
    update_floor_hotspots();
}


function click_to_set_linked_hotspot(hotspot_id)
{

    $(".new_linked_property_x_coordinate").val('');
    $(".new_linked_property_y_coordinate").val('');

    // var floor_linked_spot = $("#linked-hotspot-select-floor-"+hotspot_id).val();
    // if (floor_linked_spot)
    // {
    //     show_linked_floor_apartments(floor_linked_spot, hotspot_id);
    // }
    //

    $("li.apartment-content-hotspot.apartment-content-list-open").removeClass('apartment-content-list-open');
    $(".apartment-content-hotspot-"+hotspot_id).addClass('apartment-content-list-open');

    var floor_id = $("#current_floor_id").val();

    redraw_main_canvas();

    $("#cross-hatch").bind("click", canvas_click_new).bind("mousemove", mouse_move);

    $("#linked-show_current_hotspots_"+hotspot_id).show();
    $("#linked_hotspot_deselector_"+hotspot_id).show().removeClass('btn-black').addClass('btn-warning');
    $("#linked_hotspot_selector_"+hotspot_id).hide();

    $(".apartment-checkbox").attr('checked', false);
    $("#linked-select-hotspot-"+ hotspot_id).attr('checked', true);

    if ($("#linked-show_current_hotspots_"+hotspot_id).is(":visible"))
    {
        var x_value = $("#linked-apartment_set_x_coordinate_"+hotspot_id).val();
        var y_value = $("#linked-apartment_set_y_coordinate_"+hotspot_id).val();

        add_event_listener(x_value, y_value);
        redraw_apartment_spot(x_value, y_value, 'blue');

        $().toastmessage('showToast', {
            text: 'Apartment selected. Please click on the floor plan to select new co-ordinates',
            sticky: false,
            position : 'top-right',
            type     : 'notice',
            closeText: ''}
        );

        $('#cross-hatch').css('cursor', 'crosshair');

    }
    else
    {
        $("#linked_hotspot_deselector_"+hotspot_id).removeClass('btn-warning').addClass('btn-black'); //.css("background", 'green');
        $('#cross-hatch').css('cursor', 'default');
    }
}

function update_selected_apartment(apartment_id)
{
    //console.log('in update_selected_apartment');
    
    // $("#new-hotspot-select-aprt").val(apartment_id);
    $("#adding-new-hotspot-select-aprt").val(apartment_id);
}

function show_linked_floor_apartments(floor_id, hotspot_id)
{
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_floor_apartments_html/' + project_id + '/',
        type: 'GET',
          data: {'floor_id': floor_id},
        success: function (data) {
            $("#show-linked-floor-apartments-"+hotspot_id).html(data.rendered);
        }
     });
}

function show_changed_floor_apartments(floor_id, property_id)
{

    var project_id = $("#current_project_id").val();
    var select_id = "hotspot-select-aprt-" + property_id;

    //console.log(select_id, 'floor_id', floor_id);

    $.ajax({
        url: '/get_floor_apartments_html/' + project_id + '/',
        type: 'GET',
          data: {'floor_id': floor_id, 'selected_id': select_id},

        success: function (data) {
            //console.log('here');
            //console.log(data.rendered);

            $("#show-floor-apartments-"+property_id).html('');
            $("#show-floor-apartments-"+property_id).html(data.rendered);
        }
     });
}

function show_floor_apartments(floor_id)
{
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_floor_apartments_html/' + project_id + '/',
        type: 'GET',
          data: {'floor_id': floor_id, 'selected_id': 'new-hotspot-select-aprt'},
        success: function (data) {
            $("#show-new-floor-apartments").html(data.rendered);
        }
     });
}

function show_selected_hotspot(floor_id, x_value, y_value, color)
{
    var property_id = $("#current_apartment_setting").val();
    var project_id = $("#current_project_id").val();
    var current_apartment_setting = $("#hotspot-apt-selector-"+property_id).val();

    get_image(project_id, floor_id, "level_plans", property_id);

    redraw_main_canvas();

    $(init);

    redraw_apartment_spot(x_value, y_value, color);
}


function redraw_apartment_spot(x_value, y_value, color)
 {
     if (!(x_value == '0' && y_value == '0') && !(x_value == 0 && y_value == 0))
     {

         var canvas = document.getElementById("cross-hatch");
         var ctx = canvas.getContext('2d');

         ctx.fillStyle = color;
         var size = parseFloat($("#cross-hatch").width());
         
         var current_value_x = parseFloat(x_value * size);
         var current_value_y = parseFloat(y_value * size);

          var centerX = current_value_x;
          var centerY = current_value_y;

          var radius = $("#canvas_circle_size").val();

          ctx.globalAlpha = 0.5;
          ctx.beginPath();

          ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
          ctx.fillStyle = color;
          ctx.fill();
          ctx.lineWidth = 0.1;
          ctx.strokeStyle = color;
          ctx.stroke();
     }

 }

function redraw_apartment_level_plan_spot(x_value, y_value, color)
 {

     //console.log(x_value, y_value);
     if (!(x_value == '0' && y_value == '0') && !(x_value == 0 && y_value == 0))
     {

         var canvas = document.getElementById("cross-hatch-level_plans");
         var ctx = canvas.getContext('2d');

         ctx.fillStyle = color;

         var current_value_x = parseFloat(x_value * $("#cross-hatch").val());
         var current_value_y = parseFloat(y_value * $("#cross-hatch").val());

          var centerX = current_value_x;
          var centerY = current_value_y;
          var radius = 15;

          ctx.globalAlpha = 0.5;
          ctx.beginPath();
          ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
          ctx.fillStyle = color;
          ctx.fill();
          ctx.lineWidth = 0.1;
          ctx.strokeStyle = color;
          ctx.stroke();
     }
 }

 function writeMessage(canvas, message) {
    $("#floorplan-message").html(message);
 }

 function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
 }

 function show_selected(canvas, x, y)
 {
     //console.log('show_selected', x, y);
     var context = canvas.getContext('2d');
     context.fillStyle = $("#selected-colour").val();

     var size = parseFloat($("#cross-hatch").width());

     var u_x = (x * size).toFixed(4);
     var u_y = (y * size).toFixed(4);

     context.fillStyle = 'yellow';
     redraw_apartment_spot(u_x, u_y, 'yellow');

     $("#selected_x").val(x);
     $("#selected_y").val(y);

     redraw_apartment_spot(x, y, 'yellow');

     $("#selected_current_value_x").val(x);
     $("#selected_current_value_y").val(y);
 }

function show_current_values(apartments)
{
    try {
        apartments = $.parseJSON(apartments);

         $(apartments).each(function(index, object) {
             if (object.x_coordinate)
             {
                redraw_apartment_spot(object.x_coordinate, object.y_coordinate, 'yellow');
             }
         });
    }
    catch(err) {
        $(init);
        // console.log(err);

         // $(apartments).each(function(index, object) {
         //     if (object.x_coordinate)
         //     {
         //        redraw_apartment_spot(object.x_coordinate, object.y_coordinate, 'yellow');
         //     }
         // });
    }


}

function show_template_for_apartment(apartment_id)
{
    $("#current_apartment_setting").val(apartment_id);

    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var selected_tab = $("li.main-tabs.active");
    var building_id = $("#current_building_id").val();


    if (selected_tab.hasClass("listing-plans"))
    {
         $.ajax({
            url: '/get_listing_details/' + project_id + '/',
            type: 'GET',
              data: {'property_id': apartment_id, 'floor_id': floor_id, 'building_id': building_id},
            success: function (data) {
                $("#section-listing").html(data.combined_html);

                highlight_property(apartment_id, building_id, "basic-apartment-link-");
            }
         });
    }

    if (selected_tab.hasClass("options-plans"))
    {
        if ($("li.option-details-tabs.active").hasClass('combination'))
        {
            //console.log('in here combo');
           show_combination_details();
        }

        if ($("li.option-details-tabs.active").hasClass('upgrade'))
        {
            //console.log('in here upgrade');
            show_upgrade_details();
        }
    }

    $(".listed-image-with-tags").css({"background": "white"});

    $("#selected-template-details").val('');
    $('#property-details-listings').html('');
    $("#save-current-property-details").hide();

    var first_template_id = $("#first_template_id").val();
    $("#selected-template-details").val(first_template_id);
    get_selected_template_for_apartment(first_template_id, '#property-details-listings', '');

    $(".apartment-link").removeClass('btn-success').addClass('btn-default');
    $(".had-danger").removeClass('had-danger').removeClass('btn-default').addClass('btn-danger');

    // if (parseFloat($("#apartment_set_x_coordinate_"+apartment_id+"-"+building_id).val()) > 0)
    // {
    //     $("#hotspot_selector_"+apartment_id+"-"+building_id).removeClass('btn-danger').addClass('had-danger');
    //     $("#apartment-link-"+apartment_id+"-"+building_id).removeClass('btn-danger').addClass('had-danger');
    //     $("#key-apartment-link-"+apartment_id+"-"+building_id).removeClass('btn-danger').addClass('had-danger');
    //     $("#basic-apartment-link-"+apartment_id+"-"+building_id).removeClass('btn-danger').addClass('had-danger');
    // }
    //
    // $(".apartment-link-"+apartment_id+"-"+building_id).addClass('btn-success');

    highlight_property(apartment_id, building_id, "basic-apartment-link-");

}




function get_selected_template_for_apartment(template_id, div_id, apartment_id)
  {

      if (template_id)
      {
          $("#edit-current-property-form input#selected_template_id").val(template_id);

          var project_id = $("#current_project_id").val();
          var property_id = '';
          if (apartment_id == '')
          {
            property_id = $("#current_apartment_setting").val();
          }


          $("#edit-current-property-form input#selected_property_id").val(property_id);
          $("#floor_id_property").val($("#current_floor_id").val());
          
          var building_id = $("#current_building_id").val();

          $.ajax({
            url: '/get_editable_property_template/' + project_id + '/',
            type: 'GET',
              data: {'property_id': property_id, 'building_id': building_id, 
                     'save_current': 'True',
                     'template_id': template_id},
            success: function (data) {

                $(div_id).html(data.editable_property_html);

                $("#save-current-property-details").show();
            }
          });
      }


      //$(".hotspot_selector").removeClass('btn-warning');
      //$(".hotspot_selector_"+ apartment_id).addClass('btn-warning');


  }


function remove_selected_hotspots()
{
    //console.log('remove_selected_hotspots');

    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var canvas_id = $("#canvas_id").val();

    if (confirm("Are you sure you wish to delete the checked hotspots?")) {
        //console.log('and delete');

        var checked_apartments = $("input.apartment-checkbox:checked");

        //console.log('checked_apartments', checked_apartments.length);

        if (checked_apartments.length >= 1) {

            $(checked_apartments).each(function () {
                var property_id = $(this).attr('id');
                property_id = property_id.replace("select-hotspot-", "");
                property_id = property_id.split('-')[0];

                $.ajax({
                    url: '/delete_hotspot/' + project_id + '/' + property_id + '/',
                    type: 'GET',
                    data: {'type': 'property'},
                    success: function (data) {

                        var apartment = $("#current_apartment_setting").val();
                        var project_id = $("#current_project_id").val();
                        var floor_id = $("#current_floor_id").val();
                        get_image(project_id, floor_id, "level_plans", apartment);

                    }
                });
            });
        }


        var checked_hotspots = $("input.hotspot-checkbox:checked");

        //console.log('checked_hotspots', checked_hotspots.length);

        if (checked_hotspots.length >= 1) {

            $(checked_hotspots).each(function () {
                var hotspot_id = $(this).attr('id');
                hotspot_id = hotspot_id.replace("linked-select-hotspot-", "");

                //console.log('hotspot_id', hotspot_id);

                $.ajax({
                    url: '/delete_hotspot/' + project_id + '/' + hotspot_id + '/',
                    data: {'type': 'hotspot'},
                    type: 'GET',
                    success: function (data) {

                        var apartment = $("#current_apartment_setting").val();
                        var project_id = $("#current_project_id").val();
                        var floor_id = $("#current_floor_id").val();
                        get_image(project_id, floor_id, "level_plans", apartment);

                        $("#linked-apartment-name-"+hotspot_id).remove();
                    }
                });
            });

        }

    }


    $().toastmessage('showToast', {
        text: 'Coordinates have been deleted successfully',
        sticky: false,
        stayTime: 1000,
        position: 'top-right',
        type: 'notice',
        closeText: ''
    });

    select_new_floor(floor_id);
}

function remove_selected_property()
{
    var checked_apartments = $("input.apartment-checkbox:checked");
    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var canvas_id = $("#canvas_id").val();

    $(window).queue("namedQueue", function() {
        if (checked_apartments.length >= 1)
        {
            if (confirm("Are you sure you wish to delete the "+ checked_apartments.length +" checked properties?"))
            {
                $(checked_apartments).each(function() {
                    var property_id = $(this).attr('id');

                    property_id = property_id.split('-');
                    property_id = property_id[2];

                     $.ajax({
                        url: '/delete_property/' + project_id + '/',
                        type: 'GET',
                        data: {'property_id': property_id},
                        success: function (data) {
                            $().toastmessage('showToast', {text : "Property has been deleted successfully", stayTime: 2000, type : 'success'});
                        }
                     });
                });
            }


        }

        setTimeout(function() {
            $(self).dequeue("namedQueue");
        }, 1000);

    });

    $(window).queue("namedQueue", function() {
        var total_properties = $("li.apartment-content-list-floor-"+ floor_id).length;
        total_properties = total_properties - checked_apartments.length;
        $("#floor-count-total-"+ floor_id).text(total_properties);

        select_new_floor(floor_id);

        setTimeout(function() {
            $(self).dequeue("namedQueue");
        }, 3000);

    });

    $(window).queue("namedQueue", function() {


        $().toastmessage('showToast', {text : "Floors have been updated successfully", stayTime: 2000, type : 'success'});

        setTimeout(function() {
            $(self).dequeue("namedQueue");
        }, 1000);
    });

    $(window).dequeue("namedQueue");



}

function remove_selected_level()
{
    if (confirm("Are you sure you wish to remove this level?"))
    {
        var project_id = $("#current_project_id").val();
        var property_id = $("#current_apartment_setting").val();
        var canvas_id = $("#canvas_id").val();

        var selected_floors = $(".floor-checkbox:checkbox:checked");

        if (selected_floors.length)
        {
            $(selected_floors).each(function () {

                var floor_id = $(this).attr('id');
                floor_id = floor_id.replace('floor-hotspot-', '');

                $.ajax({
                    url: '/delete_level/' + project_id + '/',
                    type: 'GET',
                    data: {'property_id': property_id, 'floor_id': floor_id, 'canvas_id': canvas_id},
                    success: function (data) {
                        $("#floor-content-list-"+ floor_id).remove();
                        select_new_floor(data.floor_id);
                    }
                });
            }).promise().done(function () {
                $().toastmessage('showToast', {text : "Level(s) Removed Successfully", stayTime: 2000, type : 'success'});
            });
        }


    }

}

function save_current_property_details()
{
    var project_id = $("#current_project_id").val();
    var property_id = $("#current_apartment_setting").val();
    var floor_id = $("#current_floor_id").val();
    var canvas_id = $("#canvas_id").val();

      $.post('/save_new_property_editable_template/' + project_id + '/',
        $( "#edit-current-property-form" ).serialize()).success(function( data ) {
          $().toastmessage('showToast', {text : "New Property Saved Successfully", stayTime: 2000, type : 'success'});
          $("#selected-template").val('');

          if (data.selected_property_id == "")
          {
              var total = parseInt($("#floor-count-total-"+floor_id).text());
              if (total)
              {
                  total = total + 1;
                  $("#floor-count-total-"+floor_id).text(total);
              }
          }

          if (floor_id)
          {
             console.log('save floor_id', floor_id);
             select_new_floor(floor_id);    
          }
          
      });
}


function show_listing_for_property(apartment_id)
{
    //$("#section-listing").html('<div style="height: 500px;"></div>');

    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

    $.ajax({
        url: '/get_listing_details/' + project_id + '/',
        type: 'GET',
          data: {'property_id': apartment_id, 'floor_id': floor_id, 'building_id': building_id},
        success: function (data) {
            $("#section-listing").html(data.combined_html);
        }
   });

    $(".listed-image-with-tags").css({"background": "white"});

    $("#current_apartment_setting").val(apartment_id);

    //console.log('show_listing_for_property');
    $(".apartment-link").removeClass('btn-success').addClass('btn-default');
    $(".apartment-link-"+apartment_id+"-"+building_id).addClass('btn-success');
}

function filter_listing_building(building_id)
{
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_listing_floor_select/' + project_id + '/' + building_id +'/',
        type: 'GET',
        success: function (data) {
            $("#listing-floor-selection").html(data.rendered);

            $(".listing-floor-selection").hide();
            $(".listing-building-selection-"+ building_id).show();
        }
   });
}

function filter_listing()
{
    //console.log('filter_listing');

    var selected_type = $("#listing-select-type").val();
    var selected_floor = $("#listing-select-floor").val();
    var selected_building = $("#listing-select-building").val();

    if (selected_type && selected_floor)
    {
        $(".listing-floor-selection").hide();
        $(".listing-"+selected_type+"-"+selected_floor).show();

    }
    else
    {
        if (selected_floor)
        {
            $(".listing-floor-selection").hide();
            $(".listing-floor-selection-"+selected_floor).show();
        }

        if (selected_type)
        {
            $(".listing-floor-selection").hide();
            $(".listing-type-selection-"+selected_type).show();
        }
    }
}


function check_selected_property_for_listing(property_id)
{
    if ($("#selected_property_for_listing_"+property_id).hasClass('selected'))
    {
        $("#selected_property_for_listing_"+property_id).removeClass('selected');
        $("#checkbox_property_for_listing_"+property_id).prop("checked", false);
    }
    else
    {
        $("#selected_property_for_listing_"+property_id).addClass('selected');
        $("#checkbox_property_for_listing_"+property_id).prop("checked", true);
    }
}

function check_selected_property_remove_listing(property_id)
{
    if ($("#remove_property_for_listing_"+property_id).hasClass('selected'))
    {
        $("#remove_property_for_listing_"+property_id).removeClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", false);
    }
    else
    {
        $("#remove_property_for_listing_"+property_id).addClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", true);
    }
}


function add_property_to_listing()
{
    var property_id = $("#current_apartment_setting").val();
    var project_id = $("#current_project_id").val();
    var floor_id = $("#current_floor_id").val();
    var building_id = $("#current_building_id").val();

    var checked_boxes = $('.checkbox_property_for_listing:checkbox:checked');

    $(checked_boxes).each(function() {
        var added_id = $(this).val();
        $.ajax({
            url: '/add_property_to_listing/' + project_id + '/',
            type: 'GET',
            data: {'property_id': property_id, 'added_property_id': added_id },
            success: function (data) {

               $("#properties-in-listing").append('<tr class="link" id="remove_property_for_listing_'+added_id+'" onclick="check_selected_property_remove_listing('+added_id+');"> '+
                '<td> '+data.child_name+' </td> <td> '+data.child_property_type+' ' +
                '<div style="display: none"> <input id="listing-property-toggle-'+added_id+'" value="'+added_id+'" class="checkbox_property_remove_listing pull-right" type="checkbox">' +
                ' </div> </td> </tr> ');


                $("#checkbox_property_for_listing_"+added_id).attr('checked', false);

            }
        });
    }).promise().done(function() {
        $().toastmessage('showToast', {text : "Listing Saved Successfully", stayTime: 2000, type : 'success'});
    });

}

function remove_property_from_listing()
{
    var property_id = $("#current_apartment_setting").val();
    var project_id = $("#current_project_id").val();

    var checked_boxes = $('input.checkbox_property_remove_listing:checked');

    //console.log('checked_boxes', checked_boxes);
    $(checked_boxes).each(function() {

        var removed_property_id = $(this).val();

        $.ajax({
            url: '/remove_property_from_listing/' + project_id + '/',
            type: 'GET',
            data: {'property_id': property_id, 'removed_property_id': removed_property_id },
            success: function (data) {

                //console.log('here', removed_property_id);
                //console.log('here', $("#remove_property_for_listing_"+removed_property_id));

                $("#remove_property_for_listing_"+removed_property_id).remove();
                $("#selected_property_for_listing_"+removed_property_id).removeClass('selected');
            }
        });
    }).promise().done(function() {
        $().toastmessage('showToast', {text : "Listing Saved Successfully", stayTime: 2000, type : 'success'});
    });



}

// function get_new_hotspot_image(floor_id)
// {
//     var property_id = $("#current_apartment_setting").val();
//     $("#add-new-hotspot-"+property_id).attr('value', 'true');
//     $(".floor-apartment-details").hide();
//     $(".floor-apartment-details-"+ floor_id).show();
//
// }

