
$(function() {
    $( init );

    $("#page-details-edited").val('False');

    window.onload = function() {
        window.addEventListener("beforeunload", function (e) {
            var formSubmitting = $("#page-details-edited").val();

            if (formSubmitting.toString() == "False") {
                return undefined;
            }

            var confirmationMessage = 'It looks like you have been editing something. '
                                    + 'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    };

    $(".instant-search").prop('checked', true);
    $(".all-props-property-selector").val('all');
    $(".editable-props-property-selector").val('all');
    $(".custom-selector").val('all');

    var nametable = $('table.scrollable-header');

    nametable.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.wrapper');
        }
    });

    $("#edit-all-props-by-select").click(function() {
        check_current_fields_to_save();
        $("#edit-all-props-by-select").removeClass('fa-square').addClass('fa-check');
        $("#edit-all-props-by-text").removeClass('fa-check').addClass('fa-square');
    });

    $("#edit-all-props-by-text").click(function() {
        check_current_fields_to_save();
        $("#edit-all-props-by-text").removeClass('fa-square').addClass('fa-check');
        $("#edit-all-props-by-select").removeClass('fa-check').addClass('fa-square');
    });

    $('#editable-props-property-selector-building_name option:eq(1)').attr('selected', 'selected');
    $('#all-props-property-selector-building_name option:eq(1)').attr('selected', 'selected');

    $('#editable-props-property-selector-floor option:eq(1)').attr('selected', 'selected');
    $('#all-props-property-selector-floor option:eq(1)').attr('selected', 'selected');

    // var first_floor = $('#editable-props-property-selector-floor').val();
    // get_new_floor_data(first_floor, 'properties');

    // $(".assigned-floor-property-select-floor").val(first_floor);
    var project_id = $("#current_project_id").val();

    $(".allocation-search").change(function() {
        var property_selected = $("#id_properties").val();
        var property_type_selected = $("#id_property_type").val();
        var level_selected = $("#id_level").val();
        var building_selected = $("#id_building").val();
        var user_selected = $("#id_users").val();

        $.ajax({
            url: '/get_project_allocated_search_results/'+project_id+'/',
            type: 'GET',
            data: { 'property_selected': property_selected,
                    'property_type_selected': property_type_selected,
                    'level_selected': level_selected,
                    'building_selected': building_selected,
                    'user_selected': user_selected },
            success: function (data) {
                $("#allocation-search-results").html(data.rendered);
            }
        });
    });

    show_reserved();

    var building = $("#editable-props-property-selector-building_name").val();
    if (building)
    {
        $("#selected-building-name").val(building);
    }

});


function init() {

    var $table = $('.table-fixed-header');
    $table.floatThead({
        //debounceResizeMs: 300,
        scrollContainer: function ($table) {
            return $table.closest('.wrapper');
        }
    });

    var $allocated_table = $('.table-fixed-header-allocated');
    $allocated_table.floatThead({
        //debounceResizeMs: 300,
        scrollContainer: function ($allocated_table) {
            return $allocated_table.closest('.wrapper');
        }
    });

    var $table_propertyedit = $(".show-all-properties-for-edit");
    $table_propertyedit.floatThead({
        //debounceResizeMs: 300,
        scrollContainer: function ($table_propertyedit) {
            return $table_propertyedit.closest('.edit-wrapper');
        }
    });
}



(function($) {
    $.fn.fixedHeader = function () {

    return this.each( function() {
      var o = $(this)
        , nhead = o.closest('.fixed-table');

      var $head = $('thead.header', o);

      $head.clone().removeClass('header').addClass('header-copy header-fixed table').appendTo(nhead);
      var ww = [];

      o.find('thead.header > tr:first > th').each(function (i, h){
        ww.push($(h).width());
      });

      $.each(ww, function (i, w){
          nhead.find('thead.header > tr > th:eq('+i+'), thead.header-copy > tr > th:eq('+i+')').css({width: parseFloat(w) + 3 });
      });

      nhead.find('thead.header-copy').css({ width: o.width()});

      // makes the header scroll horziontally with the table if
      // the table is too wide to fit into the surrounding div
      o.closest('.table-content').scroll(function(){
        var scroll = $(this).scrollLeft();
        nhead.find('thead.header-copy').css('margin-left', -scroll);
      });

    });
}})(jQuery);

(function($) {
    $.fn.fixedHeaderAllProp = function () {
        $('#all-prop-table-content').on('scroll', function () {
            $('#show-all-properties').scrollLeft($(this).scrollLeft());
        });
    };
})(jQuery);



function select_all_properties_to_remove()
{
    ////console.log('select_all_properties_to_remove');
    if ($("#select-remove-all").hasClass('fa-square'))
    {
        $('.checkbox_property_remove_listing').prop('checked', true);
        $(".remove_property_for_listing:visible").addClass('selected');
        $("#select-remove-all").removeClass('fa-square').addClass('fa-check');
    }
    else
    {
        $('.checkbox_property_remove_listing').prop('checked', false);
        $(".remove_property_for_listing:visible").removeClass('selected');
        $("#select-remove-all").addClass('fa-square').removeClass('fa-check');
    }
}

function sort_exclusive()
{

}

function search_for_users()
{
    $("#search-users-results").html("Loading....");

    var user_type_list = $("#selected-user-types").val();
    var status_list = $("#selected-status-values").val();
    var project_status = $("#selected-project-access").val();
    var search_text = $("#search-text").val();
    var project_id = $("#current_project_id").val();
    var group_id = $("#selected-allocation-group").val();
    $("#show-selected-user-groups").html('');

    $.ajax({
        url: '/get_allocation_search_list/' + project_id + '/',
        data: {'user_type_list': user_type_list, 'group_id': group_id,
               'status_list': status_list, 'search_text': search_text, 'project_status': project_status},
        type: 'GET',
        success: function (data) {

            $("#search-users-results").html(data.rendered);

        }
    });
}

function download_all_data(group_id, href)
{

    $("#download-loading-"+group_id).show();
    window.location.href=href;

    setTimeout(function() {
        $("#download-loading-"+group_id).hide();
    }, 6000);

}

function delete_allocation_group(group_id)
{
    if (confirm("Are you sure you wish to delete this allocation group? "))
    {
        var project_id = $("#current_project_id").val();

        $.ajax({
            url: '/delete_allocation_group/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id },
            success: function (data) {

                $().toastmessage('showToast', {text : "Allocation group has been deleted", position : 'top-left', stayTime: 2000, type : 'success'});
                $("#allocation-group-row-"+group_id).remove();

                $( init );
        }});
    }
}

function add_exclusive_to_selected()
{
    if (confirm("Are you sure you wish to set all selected properties to Exclusive? ")) {
        $("#select-all-to-excusive").addClass('fa-check').removeClass('fa-square');

        $('.remove_property_for_listing.selected').each(function () {
            var id = $(this).attr('id');
            id = id.replace('exclusive-property-toggle-', '').replace("remove_property_for_listing_", "");
            set_group_properties_to_exclusive(id, false);

            $("#exclusive-property-toggle-"+id).prop('checked', true);


            $("#remove_property_for_listing_"+id).removeClass("selected");
        });

        $().toastmessage('showToast', {
            text: "Properties have been updated",
            position: 'top-left', stayTime: 2000, type: 'success'
        });

        var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
        get_new_floor_data(first_floor, 'properties');

        $(init);
    }
}

function remove_exclusive_to_selected()
{
    if (confirm("Are you sure you wish to remove Exclusive from selected properties? ")) {

        $("#select-all-to-excusive").addClass('fa-square').removeClass('fa-check');

        $('.remove_property_for_listing.selected').each(function () {
            var id = $(this).attr('id');
            id = id.replace('exclusive-property-toggle-', '').replace("remove_property_for_listing_", "");

            remove_group_properties_to_exclusive(id);
            $("#exclusive-property-toggle-"+id).prop('checked', false);
            $("#remove_property_for_listing_"+id).removeClass("selected");

        });

        $().toastmessage('showToast', {
            text: "Properties have been updated",
            position: 'top-left', stayTime: 2000, type: 'success'
        });

        var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
        get_new_floor_data(first_floor, 'properties');

        $(init);
    }
}

function select_all_properties_to_exclusive()
{
    ////console.log('select_all_properties_to_exclusive');
    if (confirm("Are you sure you wish to update the exclusive status? "))
    {
        if ($("#select-all-to-excusive").hasClass('fa-square'))
        {
            $("#select-all-to-excusive").addClass('fa-check').removeClass('fa-square');
            $('.selected-property-exclusive').prop('checked', true);

            $('.selected-property-exclusive').each(function() {
                var id = $(this).attr('id');
                id = id.replace('exclusive-property-toggle-', '');
                set_group_properties_to_exclusive(id, false);
            });

            $().toastmessage('showToast', {text : "Properties have been updated",
                    position : 'top-left', stayTime: 2000, type : 'success'});

            var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
            get_new_floor_data(first_floor, 'properties');

            $( init );
        }
        else
        {
            $("#select-all-to-excusive").addClass('fa-square').removeClass('fa-check');
            $('.selected-property-exclusive').prop('checked', false);
            $('.selected-property-exclusive').each(function() {
                var id = $(this).attr('id');
                id = id.replace('exclusive-property-toggle-', '');
                remove_group_properties_to_exclusive(id);

            });
            $().toastmessage('showToast', {text : "Properties have been updated",
                    position : 'top-left', stayTime: 2000, type : 'success'});

            var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');
            get_new_floor_data(first_floor, 'properties');

            $( init );

        }
    }

}

function select_all_properties_to_assign()
{
    ////console.log('select_all_properties_to_assign');
    if ($("#select-all-to-assign").hasClass('fa-square'))
    {
        $('.checkbox-property-for-listing').prop('checked', true);
        $(".tr-show-all-properties.property-selections:visible").addClass('selected');
        $("#select-all-to-assign").removeClass('fa-square').addClass('fa-check');
    }
    else
    {
        $("#select-all-to-assign").addClass('fa-square').removeClass('fa-check');
        $('.checkbox-property-for-listing').prop('checked', false);
        $(".tr-show-all-properties:visible").removeClass('selected');
    }
}

function save_new_user_form()
{
    ////console.log('save_new_user_form');
    var project_id = $("#current_project_id").val();
    var user_formdata = $('#add-new-user-details-form').serialize();

    $.ajax({
        url: '/save_new_user_form/'+project_id+'/' ,
        type: 'GET',
        data: {'formdata': user_formdata, 'direction':'save'},

        success: function (data) {

            if (data.success == false)
            {
                $().toastmessage('showToast', {text : "There was an error with your form.", position : 'top-left', stayTime: 2000, type : 'error'});
                total_success = false;

                $("#add-new-user-details").html('').html(data.rendered_form);
                $("#add_new_user_modal").modal('show');
            }
            else
            {
                $().toastmessage('showToast', {text : "User saved successfully", position : 'top-left', stayTime: 2000, type : 'success'});
                $("#add_new_user_modal").modal('hide');

                var group_id = $("#selected-allocation-group").val();

                $("#id_username").val('');
                $("#id_first_name").val('');
                $("#id_last_name").val('');
                $("#id_email").val('');

                show_allocated_users(group_id);
            }

        }, error  : function (data) {
            ////console.log('error');
        }
    });
}

function get_user_form(user_id)
{
    $("#update_user_modal").modal('show');
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_or_update_user/'+project_id+'/'+ user_id +'/',
        type: 'GET',
        data: {'direction':'get'},
        success: function (data) {

            $("#update-user-form").html(data.rendered);

        }, error  : function (data) {
            ////console.log('error');
        }
    });
}

function save_updated_user_form(user_id)
{

    var project_id = $("#current_project_id").val();
    var user_formdata = $('#update-user-form').serialize();

    $.ajax({
        url: '/get_or_update_user/'+project_id+'/'+ user_id +'/',
        type: 'GET',
        data: {'user_formdata': user_formdata, 'direction':'save'},

        success: function (data) {

            $().toastmessage('showToast', {text : "User has been updated successfully",
                position : 'top-right', stayTime: 2000, type : 'success'});

            $("#update_user_modal").modal('hide');

            var group_id = $("#selected-allocation-group").val();
            show_allocated_users(group_id, '');

        }, error  : function (data) {
            ////console.log('error');
        }
    });
}

function go_to_allocations_tab()
{
    var check_details_changed = $("#page-details-changed").val();

    if (check_details_changed == "True")
    {
        if (confirm("Are you sure you wish to leave without saving changed details?"))
        {
            console.log('confirmed');
            $(".property-search-team").hide();
            $(".allocation-search-team").show();
            $(".search-filter-item-reserved").parent().show();
            $(".search-filter-item-exclusive").parent().css({'margin-left': ''});
        }
        else
        {

            $("#allocation-section-tab").removeClass('active');
            $('.nav-tabs a[href="#section-properties"]').tab('show');
            $("li#properties-tab").trigger('click').addClass('active');
            $("#section-properties").addClass('active');
            $("#properties-tab-link").trigger('click');
            go_to_properties_tab();
        }

    }
    else
    {
        $(".property-search-team").hide();
        $(".allocation-search-team").show();
        $(".search-filter-item-reserved").parent().show();
        $(".search-filter-item-exclusive").parent().css({'margin-left': ''});
    }

}

function go_to_properties_tab()
{
    ////console.log('go_to_properties_tab');
    $('.allocation-assigned-details').hide();
    $(".property-search-team").show();
    $(".allocation-search-team").hide();

    $(".search-filter-item-reserved").parent().hide();
    $(".search-filter-item-exclusive").parent().css({'margin-left': '-10px'});

}

function save_reservation_form()
{
    console.log('save_reservation_form called');
    var project_id = $("#current_project_id").val();
    var property_id = $("#showing-property-id").val();
    var formdata = new FormData($('#reservation-modal-form')[0]);

    formdata = $('#reservation-modal-form').serialize();
    var user_formdata = $('#reservation-agent-form').serialize();

    var total_success = true;
    var id_agent = $("#id_agent").val();

    if ($("#agent-form").html() == "")
    {

    }
    else
    {
        $.ajax({
            url: '/get_agent_for_reservation/'+project_id+'/',
            type: 'GET',
            data: {'formdata': user_formdata, 'direction':'save', 'agent_user_id': id_agent},
            success: function (data) {

                console.log('error in agent form', data.success);

                if (data.success == false)
                {
                    $().toastmessage('showToast', {text : "There was an error with your form.", position : 'top-left', stayTime: 2000, type : 'error'});
                    total_success = false;
                    $("#agent-form").html('').html(data.rendered_form);
                    $("#reservation-modal").modal('show');
                }


        }});
    }

    $.ajax({
        url: '/get_property_reservation/'+project_id+'/',
        type: 'GET',
        data: {'formdata': formdata, 'direction':'save', 'user_id': id_agent, 'property_id': property_id},
        success: function (data) {

            if (data.success == false)
            {
                $().toastmessage('showToast', {text : "There was an error with your agent details.", position : 'top-left', stayTime: 2000, type : 'error'});
                total_success = false;
                $("#show-reservation-modal-details").html(data.rendered_form);
                $("#reservation-modal").modal('show');
            }

        }});

    if (total_success)
    {
        $().toastmessage('showToast', {text : "Reservation request saved successfully", position : 'top-left', stayTime: 2000, type : 'success'});


        // if (property_id)
        // {
        //     $(".property-edit-reserved-"+property_id).removeClass('fa-square').addClass('fa-check');
        //     $(".property-edit-reserved-"+property_id).prop('checked', true);
        //     $("#editable-"+property_id+"-status").text('Reserved');
        // }

        $("#reservation-modal").modal('hide');

    }
}


function check_filter(key_name, class_name, user_type)
{
    var check_search = true;

    if (class_name == "all-props")
    {
        check_search = $("#all-props-instant-search").is(':checked');
    }
    else if (class_name == "editable-props")
    {
        check_search = $("#editable-props-instant-search").is(':checked');
    }

    console.log('check_search', class_name, check_search);

    if (class_name)
    {
        if (class_name == "all-props")
        {
            if (key_name.toString() == "floor")
            {
                $(window).queue("namedQueue", function() {
                    if (check_search) {
                        waitingDialog.show('Loading', {dialogSize: 'sm', progressType: 'info'});
                        var first_floor = $("#all-props-property-selector-floor").val();
                        get_new_floor_data(first_floor, 'properties');

                        var self = this;
                        if (first_floor.toString() == "all")
                        {
                          setTimeout(function() {
                            $(self).dequeue("namedQueue");
                          }, 20000);
                        }
                        else {

                          setTimeout(function() {
                            $(self).dequeue("namedQueue");
                          }, 2000);
                        }
                    }

                });

                $(window).queue("namedQueue", function() {
                    if (check_search) {
                        filter_images(class_name);
                    }

                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("namedQueue");
                      }, 1000);
                });

                $(window).queue("namedQueue", function() {
                    waitingDialog.hide();

                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("namedQueue");
                      }, 1000);
                });

                $(window).dequeue("namedQueue");

            }

            else if (key_name == "building_name")
            {

                $(window).queue("namedQueue", function() {
                    if (check_search) {
                        waitingDialog.show('Loading', {dialogSize: 'sm', progressType: 'info'});
                        var first_building = $("#all-props-property-selector-building_name").val();
                        get_new_building_data(first_building, class_name);

                        if (get_new_building_data.toString() == "all")
                        {
                          setTimeout(function() {
                            $(self).dequeue("namedQueue");
                          }, 25000);
                        }
                        else {

                          setTimeout(function() {
                            $(self).dequeue("namedQueue");
                          }, 3000);
                        }
                    }


                });

                $(window).queue("namedQueue", function() {
                    console.log('at filter images');

                    if (check_search) {
                        filter_images(class_name);
                    }


                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("namedQueue");
                      }, 1000);
                });

                $(window).queue("namedQueue", function() {
                    waitingDialog.hide();

                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("namedQueue");
                      }, 1000);
                });

                $(window).dequeue("namedQueue");

            }

            else {
                console.log('at filter images');
                if (check_search) {
                    filter_images(class_name);
                }
            }

        }
        else
        {
            var check_details_changed = $("#page-details-changed").val();

            if (check_details_changed == "True" && (key_name == "floor" || key_name == "building_name"))
            {
                if (confirm("Are you sure you wish to filter for new properties without saving changed details?"))
                {
                    update_edit_props_filter(key_name, class_name, user_type);
                }
                else
                {
                    $("#"+class_name+"-property-selector-"+key_name).val('all');
                }
            }
            else
            {
                if (check_search) {
                    update_edit_props_filter(key_name, class_name, user_type);
                }

            }
        }
    }

}

function show_floors_filter(first_floor)
{
    $(window).queue("namedQueue", function() {
        waitingDialog.show('Loading', {dialogSize: 'sm', progressType: 'info'});

        get_new_floor_data(first_floor, 'properties');
        var self = this;

        if (first_floor.toString() == "all")
        {
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 25000);
        }
        else {

          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 2000);
        }

    });

    $(window).queue("namedQueue", function() {
        console.log('-- 1 filter_images_properties');
        filter_images_properties();


        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).queue("namedQueue", function() {
        waitingDialog.hide();

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).dequeue("namedQueue");
}

function show_building_filter(first_building)
{
    $(window).queue("namedQueue", function() {
        waitingDialog.show('Loading, please wait', {dialogSize: 'sm', progressType: 'info'});
        get_new_building_data(first_building, 'all-props');

        if (get_new_building_data.toString() == "all")
        {
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 10000);
        }
        else {

          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
        }
    });

    $(window).queue("namedQueue", function() {
        console.log('-- 2 filter_images_properties');
        filter_images_properties();

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).queue("namedQueue", function() {
        waitingDialog.hide();

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).dequeue("namedQueue");
}

function update_edit_props_filter(key_name, class_name, user_type)
{
    if (class_name)
    {
        var first_floor = $("#"+class_name+"-property-selector-floor").val();
        var building = $("#"+class_name+"-property-selector-building_name").val();

        $("#selected-building-name").val(building);

        console.log('update_edit_props_filter', class_name, 'called key_name', key_name, 'first_floor', first_floor, 'building', building);

        if (key_name.toString() == "floor")
            {

                if (first_floor.toString() == "all" || building.toString() == "all")
                {

                    show_floors_filter(first_floor);

                }

                else
                {
                    show_floors_filter(first_floor);
                }

            }

            else if (key_name == "building_name")
            {
                if (first_floor.toString() == "all" || building.toString() == "all")
                {

                    show_building_filter(building);

                }

                else
                {
                    show_building_filter(building);
                }

            }

            else {
                console.log('-- 3 filter_images_properties');
                filter_images_properties();

            }
    }

}

function filter_images(class_name)
{
    console.log('filter_images called', class_name);

    $(window).queue("namedQueue", function() {

        var all_selected_key_list = [];
        var all_selected_tags = $("."+class_name+"-property-selector");

        $(all_selected_tags).each(function() {
            var key_selected = $(this).attr('class').replace("property-selector assigned-floor-property-select-", "").replace("allocation-section-main", "").replace("allocations-selector", "").replace("all-props-", "");
            key_selected = key_selected.trim().replace(' ', '');

            if (this.value == "all")
            {}
            else
            {
                all_selected_key_list.push([key_selected, this.value]);
            }
        });

        // console.log('all_selected_key_list', all_selected_key_list);

        $(".tr-show-all-properties").each(function( i, value ) {

            var show_image = true;
            var property_row = $(this);

            $.each(all_selected_key_list, function (index, v) {

                var key = v[0].toString();
                key = key.replace('all-props-', '').replace("property-selector assigned-floor-property-select-", "").replace("allocation-section-main", "").replace("allocations-selector", "").replace("all-props-", "");;
                key = key.replace('editable-props-', '').replace('allocations-selector', '').replace('allocation-section-main', '');
                key = key.trim().replace(' ', '');

                var value = v[1].trim().replace(/\n/, '');
                value = value.trim();

                var check_value = property_row.find('td.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');
                check_value = check_value.trim().replace(/\r/, '');

                var value_matches = value.toString().toLowerCase() == check_value.toString().toLowerCase();
                console.log('--------------- ');
                console.log('+++', value.toString().toLowerCase(),check_value.toString().toLowerCase(), value_matches);
                if (value_matches)
                {

                }

                else
                {
                    show_image = false;
                    console.log('rollng', key, value, check_value, 'showing image');
                }

            });

            if (show_image)
            {
               property_row.show();

            }
            else
            {
                property_row.hide();
                var row_id = $(value).attr('id');
                ////console.log(row_id);
                $("#"+row_id).hide();
                $(this).hide();
            }
        });

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 3000);
    });

    $(window).dequeue("namedQueue");

}


function filter_images_properties()
{
    console.log('filter_images_properties called');

    var selected_key_list = [];
    var selected_tags = $(".editable-props-property-selector");
    $(selected_tags).each(function() {
        var key_selected = $(this).attr('class').replace("property-selector assigned-floor-property-select-", "").replace("allocation-section-main", "").replace("allocations-selector", "").replace("all-props-", "");
        key_selected = key_selected.replace(' ', '');

        if (this.value == "all")
        {}
        else
        {
            selected_key_list.push([key_selected, this.value]);
        }
    });

    $(".property-edit-selections").each(function(index, value) {

        var show_image = true;
        var property_row = $(this);

        $.each(selected_key_list, function (k, v) {

            var key = v[0];

            key = key.replace('editable-props-', '').replace('allocations-selector', '').replace('allocation-section-main', '').replace("allocations-selector", "").replace("all-props-", "");
            key = key.trim().replace(' ', '');

            var value = v[1].trim().replace(/\n/, '');
            var check_value = property_row.find('.td-with-key[data-name='+key+']').text().trim().replace(/\n/, '');

            if (value.toString().toLowerCase() == check_value.toString().toLowerCase())
            {

            }
            else
            {
                show_image = false;
            }
        });

        var row_id = property_row.attr('id');
        ////console.log(property_row.attr('class'), 'show_image', show_image);

        if (show_image)
        {
            property_row.show();
            $("#"+row_id).addClass('show-row');
        }
        else
        {
            property_row.hide();

            $("#"+row_id).addClass('hide-row');
            $("#"+row_id).hide();
            $("table#show-reserved-table #"+row_id).hide();
            $("."+row_id).hide();
        }
    });

}


function change_search(id_value)
{
    var instant_on = $("#" + id_value).prop('checked');

    if (instant_on)
    {
        $().toastmessage('showToast', {text : "Search loads now instantly",
                position : 'top-left', stayTime: 2000, type : 'success'});
    }
    else
    {
        $().toastmessage('showToast', {text : "Search loads now via the Search Button",
                position : 'top-left', stayTime: 2000, type : 'success'});
    }

}


function update_assigned_floor_list_all(class_name)
{
    $(window).queue("namedQueue", function() {
        waitingDialog.show('Loading, please wait', {dialogSize: 'sm', progressType: 'info'});

        // var first_floor = $("#"+class_name+"-property-selector-floor").val();
        // get_new_floor_data(first_floor, 'properties');

        var first_building = $("#"+class_name+"-property-selector-building_name").val();
        get_new_building_data(first_building, class_name);

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 3000);
    });

    $(window).queue("namedQueue", function() {
        filter_images(class_name);
        waitingDialog.hide();
        update_with_all_custom_values(class_name);

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });
    $(window).dequeue("namedQueue");
}

function update_with_all_custom_values(class_name)
{
    console.log('---------- in update_with_all_custom_values');

    if (class_name)
    {
        console.log(1, class_name);
        check_filter('', class_name, '');

        var exclusive_value = $("#"+class_name+"-assigned-floor-property-select-exclusive").val();
        var reserved_value = $("#"+class_name+"-assigned-floor-property-select-reserved").val();
        var price_min_value = $("#"+class_name+"-assigned-floor-property-select-price_min").val();
        var price_max_value = $("#"+class_name+"-assigned-floor-property-select-price_max").val();
        var key = 'retail_price';

        var table_rows = $(".tr-show-all-properties:visible");

        if (class_name == "editable-props")
        {
            table_rows = $("tr.property-edit-selections:visible");
        }

        $(table_rows).each(function( i, value ) {

            var show_image = true;
            var property_row = $(this);

            var property_id = $(this).attr('id');
            property_id = property_id.replace('property-group-selection-', '');
            property_id = property_id.replace('property-edit-selection-', '');

            if (reserved_value.toString() == "all")
            {

            }
            else {

                var property_reserved_value = $("#property-edit-reserved-"+property_id).is(":checked");
                if (property_reserved_value == true)
                {
                    property_reserved_value = "True";
                }
                else
                {
                    property_reserved_value = "False";
                }

                if (reserved_value.toString() == property_reserved_value.toString())
                {

                }
                else
                {
                    show_image = false;

                }
            }

            if (exclusive_value.toString() == "all")
            {

            }
            else
            {

                var property_exclusive_value = $("#exclusive-status-"+ property_id).val();

                if (!property_exclusive_value)
                {
                    property_exclusive_value = "False";
                }

                if (exclusive_value.toString() == property_exclusive_value)
                {

                }
                else
                {
                    show_image = false;
                }

            }

                console.log('price_min_value', price_min_value);

                if (price_min_value.toString() == "all")
                {

                }
                else {

                    key = $("#property-price-field").val();
                    price_min_value = parseFloat(price_min_value);

                    check_value = property_row.find('td.td-with-key[data-name=' + key + ']').text().trim();
                    
                    check_value = check_value.split(".");

                    if(check_value)
                    {
                        check_value = check_value[0].toString();
                    }
                    check_value = check_value.replace(/\n/, '').replace("$", "").replace(/,/, "").replace(".", "");
                    check_value = check_value.replace(/,/, "");


                    if (parseFloat(price_min_value) >= parseFloat(check_value)) {
                        show_image = false;
                    }

                }

            if (price_max_value.toString() == "all")
            {

            }
            else {

                key = $("#property-price-field").val();
                price_max_value = parseFloat(price_max_value);

                var check_value = property_row.find('td.td-with-key[data-name=' + key + ']').text().trim();

                check_value = check_value.split(".");

                if(check_value)
                {
                    check_value = check_value[0].toString();
                }
                check_value = check_value.replace(/\n/, '').replace("$", "").replace(/,/, "").replace(".", "");
                check_value = check_value.replace(/,/, "");

                if (parseFloat(price_max_value) <= parseFloat(check_value)) {
                    show_image = false;
                }
            }

            if (show_image)
            {

                property_row.show();
            }
            else
            {
                property_row.hide();
                var row_id = $(value).attr('id');
                ////console.log(row_id);
                $("#"+row_id).hide();
                $(this).hide();
            }

        //end property row loop
        });
    }



}


function update_editable_floor_list_all(classname)
{
    console.log('update_editable_floor_list_all');

    $(window).queue("namedQueue", function() {
        waitingDialog.show('Loading, please wait', {dialogSize: 'sm', progressType: 'info'});

        var first_floor = $("#"+classname+"-property-selector-floor").val();
        get_new_floor_data(first_floor, 'properties');

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 2000);
    });


    $(window).queue("namedQueue", function() {
        console.log('528 filter_images_properties');
        filter_images_properties();

        waitingDialog.hide();
        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).dequeue("namedQueue");

}

function update_assigned_floor_list(selected_type, value, class_name)
{

    console.log('update_assigned_floor_list', selected_type);

    var check_search = true;
    if (class_name == "all-props")
    {
        check_search = $("#all-props-instant-search").is(':checked');
    }
    else if (class_name == "editable-props")
    {
        check_search = $("#editable-props-instant-search").is(':checked');
    }

    console.log("check_search", class_name, check_search, selected_type, value);


    if (check_search) {
        console.log('go search');

        var floor_queue = null;
        if (selected_type == "floor") {
            $(window).queue("namedQueue", function () {
                waitingDialog.show('Loading, please wait', {dialogSize: 'sm', progressType: 'info'});
                get_new_floor_data(value, 'properties');

                floor_queue = this;

                if (value == "all") {
                    setTimeout(function () {
                        $(floor_queue).dequeue("namedQueue");
                    }, 10000);
                }

                else {
                    setTimeout(function () {
                        $(self).dequeue("namedQueue");
                    }, 1000);
                }

            });

            $(window).queue("namedQueue", function () {
                if (class_name == "all-props") {
                    filter_images(class_name);
                    ////console.log('594 filter_images');
                }
                else if (class_name == "editable-props") {
                    filter_images_properties();
                    console.log('597 filter_images_properties');
                }
                var self = this;
                setTimeout(function () {
                    $(self).dequeue("namedQueue");
                }, 1000);
            });

            $(window).queue("namedQueue", function () {
                waitingDialog.hide();
                update_with_all_custom_values(class_name);
                $(floor_queue).dequeue("namedQueue");

                var self = this;
                setTimeout(function () {
                    $(self).dequeue("namedQueue");
                }, 1000);
            });


            $(window).dequeue("namedQueue");
        }

        else if (selected_type == "building_name") {
            $(window).queue("namedQueue", function () {
                waitingDialog.show('Loading', {dialogSize: 'sm', progressType: 'info'});
                get_new_building_data(value, class_name);
                var self = this;
                setTimeout(function () {
                    $(self).dequeue("namedQueue");
                }, 1000);
            });

            $(window).queue("namedQueue", function () {
                if (class_name == "all-props") {
                    filter_images(class_name);
                    ////console.log('635 filter_images');
                }
                else if (class_name == "editable-props") {
                    filter_images_properties();
                    console.log('640 filter_images_properties');
                }
                var self = this;
                setTimeout(function () {
                    $(self).dequeue("namedQueue");
                }, 1000);
            });

            $(window).queue("namedQueue", function () {
                waitingDialog.hide();
                update_with_all_custom_values(class_name);
                var self = this;
                setTimeout(function () {
                    $(self).dequeue("namedQueue");
                }, 1000);
            });

            $(window).dequeue("namedQueue");

        }

        else {
            console.log('in else', class_name);

            if (class_name == "all-props") {
                filter_images(class_name);
                update_with_all_custom_values(class_name);
                //console.log('666 filter_images');
            }
            else if (class_name == "editable-props") {
                filter_images_properties();
                console.log('671 filter_images_properties');
            }

        }
    }

}

function update_floor_list_custom(selected_type, value, class_name)
{
    var check_search = true;
    if (class_name == "all-props")
    {
        check_search = $("#all-props-instant-search").is(':checked');
    }
    else if (class_name == "editable-props")
    {
        check_search = $("#editable-props-instant-search").is(':checked');
    }

    console.log('in update custom with: ', selected_type, '. check_search is: ', check_search);

    if (check_search) {
        filter_images(class_name);

        if (selected_type == "reserved") {
            if (value.toString() == "True") {
                $('tr:visible .reserved-status-False').parent().parent().hide();
            }

            else if (value.toString() == "False") {
                $('tr:visible .reserved-status-True').parent().parent().hide();
            }

            if (value == "all") {
                filter_images(class_name);
                update_with_all_custom_values(class_name);
            }
        }

        else if (selected_type == "exclusive") {
            if (value.toString() == "True") {
                $('tr:visible .exclusive-status-False').parent().parent().hide();
            }

            else if (value.toString() == "False") {
                $('tr:visible .exclusive-status-True').parent().parent().hide();
            }

            if (value == "all") {
                filter_images(class_name);
                update_with_all_custom_values(class_name);
            }
        }
        else {

            if (value == "all") {
                filter_images(class_name);
            }

            else {
                $(".tr-show-all-properties").each(function () {

                    var show_image = true;
                    var property_row = $(this);

                    var key = 'retail_price';
                    var value = parseFloat(value);

                    var check_value = property_row.find('td.td-with-key[data-name=' + key + ']').text().trim().replace(/\n/, '');

                    if (selected_type == "price_min") {
                        if (value >= parseFloat(check_value)) {

                        }
                        else {
                            show_image = false;
                        }
                    }

                    if (selected_type == "price_max") {
                        if (value <= parseFloat(check_value)) {

                        }
                        else {
                            show_image = false;
                        }
                    }

                    if (!show_image) {
                        property_row.hide();
                        ////console.log('763 ------', $("#"+row_id));
                    }
                    else {
                        property_row.show();
                    }
                });

            }
        }
    }

}

function check_selected_property_for_listing(property_id, set_as_exclusive)
{
    if (set_as_exclusive == "False")
    {
       if ($(".property-group-selection-"+property_id).hasClass('selected'))
        {
            $(".property-group-selection-"+property_id).removeClass('selected');
            $("#checkbox-property-for-listing-"+property_id).prop("checked", false);

        }
        else
        {
            $(".property-group-selection-"+property_id).addClass('selected');
            $("#checkbox-property-for-listing-"+property_id).prop("checked", true);
        }
    }

}


function check_selected_property_remove_listing(property_id)
{

    if ($("#remove_property_for_listing_"+property_id).hasClass('selected'))
    {
        $("#remove_property_for_listing_"+property_id).removeClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", false);
    }
    else
    {
        $("#remove_property_for_listing_"+property_id).addClass('selected');
        $("#listing-property-toggle-"+property_id).prop("checked", true);
    }
}


function add_property_to_allocation_group()
{
    var selected_properties = [];
    $(window).queue("namedQueue", function() {
        waitingDialog.show('Saving', {dialogSize: 'sm', progressType: 'info'});

        selected_properties = update_property_to_allocation_group();

      var self = this;
      setTimeout(function() {
        $(self).dequeue("namedQueue");
      }, 1000);
    });

    $(window).queue("namedQueue", function() {
        saved_allocation_group_callback();
        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).queue("namedQueue", function() {
        waitingDialog.hide();

        var last_assigned = $('#last-assigned-properties').val();
        if(last_assigned)
        {
            last_assigned = last_assigned.split(",");
            console.log()
            $(last_assigned).each(function (index, objectid) {
                objectid = parseInt(objectid.trim());
                check_selected_property_remove_listing(objectid);

                $("#assign-undo").show();
            });
        }

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).queue("namedQueue", function() {

        var group_id = $("#selected-allocation-group").val();
        var allproperties = $('tr.remove_property_for_listing');
        if (allproperties)
        {
            var allocated_properties = allproperties.length;
            $("#group-allocation-count-field-"+group_id).text(allocated_properties);
        }


        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 500);
    });

    $(window).dequeue("namedQueue");


}

function update_property_to_allocation_group()
{
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var allocation_template_id = $("#allocation_template_id").val();
    var checked_boxes = $('.checkbox-property-for-listing:checkbox:checked');

    var check_list = [];
    var check_list_count = 0;

    $(checked_boxes).each(function() {
        var added_id = $(this).attr('id');
        added_id = added_id.replace("checkbox-property-for-listing-", "");
        check_list.push(added_id);
    });

    if (check_list)
    {

        $("#last-assigned-properties").val(check_list);

        check_list_count = check_list.length;

        $.each(check_list, function(index, item) {

            var added_id = item;

            $.ajax({
                url: '/add_property_to_allocation_group/' + project_id + '/',
                type: 'GET',
                data: {'group_id': group_id, 'added_property_id': added_id, 'allocation_template_id': allocation_template_id },
                success: function (data) {
                    $("#property-group-selection-"+added_id).removeClass('selected');
                }
            });

            check_list_count -=1;

        })
    }

}


function remove_property_from_allocation_group()
{
    console.log('remove_property_from_allocation_group called');

    $(window).queue("namedQueue", function() {
     waitingDialog.show('Saving', {dialogSize: 'sm', progressType: 'info'});
     update_remove_allocation_groups();
      var self = this;
      setTimeout(function() {
        $(self).dequeue("namedQueue");
      }, 1000);
    });

    $(window).queue("namedQueue", function() {
        saved_allocation_group_callback();
        waitingDialog.hide();
        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 1000);
    });

    $(window).queue("namedQueue", function() {
        var group_id = $("#selected-allocation-group").val();
        var allproperties = $('tr.remove_property_for_listing');

        if (allproperties)
        {
            console.log('1615');
            var allocated_properties = allproperties.length;
            $("#group-allocation-count-field-"+group_id).text(allocated_properties);
        }

        $('#last-assigned-properties').val('');

        var self = this;
          setTimeout(function() {
            $(self).dequeue("namedQueue");
          }, 500);
    });

    $(window).dequeue("namedQueue");

}

function update_remove_allocation_groups()
{
    console.log('update_remove_allocation_groups called');
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var checked_boxes = $('.checkbox_property_remove_listing:checkbox:checked');

    $(checked_boxes).each(function() {
        var removed_property_id = $(this).val();
        $.ajax({
            url: '/remove_property_from_allocation_group/' + project_id + '/',
            type: 'GET',
            data: {'group_id': group_id, 'removed_property_id': removed_property_id },
            success: function (data) {
                $("#remove_property_for_listing_"+removed_property_id).remove();
                $( init );
            }
        });
    });

}

function saved_allocation_group_callback()
{
    console.log('saved_allocation_group_callback called');
    $().toastmessage('showToast', {text : "Allocation Group Saved Successfully", position : 'top-left', stayTime: 2000, type : 'success'});
    var first_floor = $(".td-with-key[data-name='floor']").first().text().trim().replace(/\n/, '');

    get_new_floor_data(first_floor, 'properties');

    $("#select-all-to-assign").addClass('fa-square').removeClass('fa-check');
    $("#select-all-to-excusive").addClass('fa-square').removeClass('fa-check');
    $("#select-remove-all").addClass('fa-square').removeClass('fa-check');

    var group_id = $("#selected-allocation-group").val();
}


function reset_search_values()
{
    console.log('reset_search_values called');
    $(".allocation-search").val('');
    $("#allocation-search-results").html('');
}

function show_allocated_users(group_id, allocated_group_name)
{
    $("#select-group-section").hide('slow');
    $("#show-selected-user-groups").html('');
    $("#show-allocated-users").html('Loading...');

    $(".allocation-group-row").removeClass('selected-low-highlight');
    $("#allocation-group-row-"+group_id).addClass('selected-low-highlight');

    var group_name = $("#group-allocation-name-field-"+group_id).val();

    $("#assigned-users").show();
    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/get_project_allocated_users/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'group_name': group_name},
        success: function (data) {

            console.log('at success');

            $("#show-allocated-users").html(data.rendered);

            $( init );
        }
    });
}

function save_users_to_group()
{
    console.log('save_users_to_group called');
    var group_id = $("#selected-allocation-group").val();
    var group_name = $("#selected-allocation-group-name").val();
    var project_id = $("#current_project_id").val();

    $(".checkbox-select-user-to-group:checked").each(function() {

        var user_id = $(this).attr('id');
        user_id = user_id.replace("checkbox-select-user-to-group-", "");

        $.ajax({
            url: '/save_user_to_allocation_group/'+project_id+'/',
            type: 'GET',
            data: {'group_id': group_id, 'user_id': user_id},
            success: function (data) {
                $().toastmessage('showToast', {text : "Allocation group saved successfully", position : 'top-left',  stayTime: 5000, type : 'success'});
                $("#selected-group-user-allocation").val('');
                show_allocated_users(group_id, group_name);
                $( init );
            }
        });
    });

    $(".select-user-to-group").removeClass('selected');
}


function show_allocated_properties(group_id)
{
    var group_name = $("#group-allocation-name-field-"+group_id).val();
    $(".allocation-group-row").removeClass('selected-low-highlight');
    $("#allocation-group-row-"+group_id).addClass('selected-low-highlight');

    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);

    var allocation_template_id = $("#allocation_template_id").val();

    $(".selecting-properties").removeClass('selected');
    var project_id = $("#current_project_id").val();

    var user_type = $("#user-type").val();
    var selected_floor = $("#all-props-property-selector-floor").val();
    var selected_building = $("#all-props-property-selector-building_name").val();

    $("#show-allocated-properties").html('Loading...');

    $.ajax({
        url: '/get_project_allocated_properties/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'group_name': group_name,
               'floor_name': selected_floor, 'selected_building': selected_building,
               'allocation_template_id': allocation_template_id},
        success: function (data) {

            waitingDialog.hide();

            $(window).queue("properties_queue", function() {
                $("#show-allocated-properties").html('').html(data.rendered);
                $("#show-allocated-properties").css({'vertical-align':'top'});

                var self = this;
                  setTimeout(function() {
                    $(self).dequeue("properties_queue");
                  }, 500);
            });
            $(window).queue("properties_queue", function() {
                // document.getElementById('assigned-properties').scrollIntoView();
                $( init );
                var self = this;
                  setTimeout(function() {
                    $(self).dequeue("properties_queue");
                  }, 500);
            });
            $(window).dequeue("properties_queue");
        }
    });

}



function get_new_floor_data(floor_name, tab_selected)
{
    console.log('get_new_floor_data', floor_name, tab_selected);

    $("#section-reservations").html('Loading...');
    var allocation_template_id = $("#allocation_template_id").val();
    $(".selecting-properties").removeClass('selected');
    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    var building_name = $("#selected-building-name").val();

    if(typeof building_name == 'undefined')
    {
        building_name = $("#editable-props-property-selector-building_name").val();
    }

    if (building_name == "all" || building_name == "")
    {
        building_name = $("#editable-props-property-selector-building_name").val();
    }

    if (building_name == "all" || building_name == "" || typeof building_name == 'undefined')
    {
        building_name = $("#all-props-property-selector-building_name").val();
    }

    $("#show-allocated-properties").html('Loading...');
    $(".show-filtered-properties").html('Loading...');

    $.ajax({
        url: '/get_project_all_properties/'+project_id+'/',
        type: 'GET',
        data: {'floor_name': floor_name, 'building_name':building_name,
               'group_id': group_id, 'allocation_template_id': allocation_template_id},
        success: function (data) {

            $(".show-filtered-properties").html('').html(data.all_prop_rendered).hide();
            $(".show-edit-filtered-properties").html('').html(data.all_prop_rendered_editable).hide();
            $("#section-reservations").html('').html(data.reservation_properties_rendered).hide();

            if (tab_selected == 'reservations')
            {
                $("#reservation-list-tab").tab('show');
            }
            else
            {
                $("#price-list-tab").tab('show');
            }

            $(".show-filtered-properties").show();
            $(".show-edit-filtered-properties").show();
            $("#section-reservations").show();

            $("html").css('cursor', 'default');
            $(".allocations-selector").css('cursor', 'default');
            $(".assigned-floor-property-select-floor").val(floor_name);

            console.log('line 1868');
            show_allocated_properties(group_id);
        }
    });
}

function get_new_building_data(building_name, class_name)
{
    console.log('get_new_building_data called');
    var allocation_template_id = $("#allocation_template_id").val();
    $(".selecting-properties").removeClass('selected');

    $("#selected-building-name").val(building_name);
    $("#all-props-property-selector-building_name").val(building_name);

    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();
    var selected_floor = $("#"+class_name+"-property-selector-floor").val();

    $.ajax({
        url: '/get_project_all_properties/'+project_id+'/',
        type: 'GET',
        data: {'building_name': building_name,
               'floor_name': selected_floor, 'group_id': group_id,
               'allocation_template_id': allocation_template_id},
        success: function (data) {
            $(".show-filtered-properties").html('').html(data.all_prop_rendered);
            $(".show-edit-filtered-properties").html('').html(data.all_prop_rendered_editable);
            $("#section-reservations").html('').html(data.reservation_properties_rendered);

            console.log('line 1898');
            show_allocated_properties(group_id);
        }
    });
}


function show_all_properties(group_id)
{
    $("#select-group-section").hide('slow');

    var group_name = $("#group-allocation-name-field-"+group_id).val();
    var group_parent = $("#group-allocation-parent-field-"+group_id).val();

    $(".allocation-group-row").removeClass('selected-low-highlight');
    $("#allocation-group-row-"+group_id).addClass('selected-low-highlight');

    $("#select-all-to-assign").addClass('fa-square').removeClass('fa-check');
    $("#select-all-to-excusive").addClass('fa-square').removeClass('fa-check');
    $("#select-remove-all").addClass('fa-square').removeClass('fa-check');
    $(".tr-show-all-properties").removeClass('selected');
    var selected_floor = $("#all-props-property-selector-floor").val();

    $("#assigned-properties").show();
    $("#selected-allocation-group").val(group_id);
    $("#selected-allocation-group-name").val(group_name);
    $(".selected-allocation-group-name").html(group_name);

    $("#show-all-properties").html('Loading...');

    // if (group_parent)
    // {
    //     console.log('group_parent', 1, group_parent);
    //
    //     var project_id = $("#current_project_id").val();
    //     var allocation_template_id = $("#allocation_template_id").val();
    //
    //     $.ajax({
    //         url: '/get_project_allocated_properties/'+project_id+'/',
    //         type: 'GET',
    //         data: {'group_id': group_parent, 'group_name': group_name, 'selected_floor':selected_floor,
    //                'allocation_template_id': allocation_template_id},
    //         success: function (data) {
    //             $("#show-all-properties").html('').html(data.all_rendered);
    //             $("#show-all-properties").css({'vertical-align':'top'});
    //             $( init );
    //
    //             $("#assign-text-property").text('Assign Property to Group (from parent group)');
    //             show_allocated_properties(group_id);
    //         }
    //     });
    // }
    // else
    // {
        console.log(2);
        get_new_floor_data(selected_floor, 'properties');
        $("#assign-text-property").text('Assign Property to Group');
    // }


    // var first_building = $(".td-with-key[data-name='building_name']").first().text().trim().replace(/\n/, '');
    // $(".assigned-floor-property-select-building_name").val(first_building);
    // $(".assigned-floor-property-select-floor").val(selected_floor);

}

function save_selected_user_and_group(group_id, user_id)
{
    var project_id = $("#current_project_id").val();
    $.ajax({
        url: '/save_user_to_allocation_group/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'user_id': user_id},
        success: function (data) {
            $().toastmessage('showToast', {text : "Allocation group saved successfully", position : 'top-left',  stayTime: 5000, type : 'success'});
            $("#selected-group-user-allocation").val('');

            var check_allocated = $("#check-selected-group-user-"+user_id).hasClass('fa-square');

            $("#select-user-to-group-"+user_id).trigger('click');

            if (check_allocated)
            {
                $("#checkbox-select-user-to-group-"+user_id).prop('checked', true);
                $("#check-selected-group-user-"+user_id).addClass('fa-check').removeClass('fa-square');

            }
            else
            {
                $("#checkbox-select-user-to-group-"+user_id).prop('checked', false);
                $("#check-selected-group-user-"+user_id).removeClass('fa-check').addClass('fa-square');

            }

        }
    });
}

function set_select_user(user_id)
{
    $("#select-user-to-group-"+user_id).addClass('selected');
    var group_id = $("#selected-allocation-group").val();
    save_selected_user_and_group(group_id, user_id);
}


function save_user_group(user_id, group_id)
{
    var project_id = $("#current_project_id").val();
    $.ajax({
        url: '/save_user_to_allocation_group/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'user_id': user_id},
        success: function (data) {
            $("#select-user-to-group-"+user_id).trigger('click');
            $().toastmessage('showToast', {text : "User allocation saved successfully", position : 'top-right',  stayTime: 2000, type : 'success'});
        }
    });
}


function get_csv_file(href)
{
    $("#loading-gif").show();
    window.location.href = href;

    setTimeout(function() {
        $("#loading-gif").hide();
    }, 3000);
}

function show_reserved()
{
    $(".property-edit-selections").each(function() {
        var property_id = $(this).attr('id');
        property_id = property_id.replace('property-edit-selection-', '');
        if ($("#editable-"+property_id+"-status").text().trim().replace(/ /g, '') == 'Reserved')
        {
            $(".property-edit-reserved-"+property_id).prop('checked', true);
            $(".property-edit-reserved-icon-"+property_id).removeClass('fa-square').addClass('fa-check');
        }
    });

}

function select_property_for_exclusive(property_id)
{
    var property_exclusive = $("#exclusive-property-toggle-"+property_id).is(":checked");

    if (property_exclusive)
    {
        set_group_properties_to_exclusive(property_id, true);
    }
    else
    {
        remove_group_properties_to_exclusive(property_id, true);
    }
}

function remove_group_properties_to_exclusive(property_id, show_update)
{
    console.log('---- remove_group_properties_to_exclusive', property_id);

    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/remove_property_from_exclusive/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'property_id': property_id},
        success: function (data) {

            $("#span-exclusive-property-toggle-"+property_id).text('False');
            $("#property-group-selection-"+property_id).removeClass('property-is-exclusive');
            $("#exclusive-status-"+ property_id).val('False');
            $("#show-property-exclusive-"+property_id).html('<span style="color: transparent">False</span>');

            if (show_update)
            {
                $().toastmessage('showToast', {text : "Property has been updated",
                position : 'top-left', stayTime: 2000, type : 'success'});


               $(window).queue("exclusive_queue", function() {
                    console.log('filter_images in remove exclusive');
                    filter_images(class_name);
                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("exclusive_queue");
                      }, 1000);
                });

                $(window).queue("exclusive_queue", function() {
                    console.log('remove update_with_all_custom_values');

                    update_with_all_custom_values('all-props');
                    $(".remove_property_for_listing").removeClass('selected');

                    var self = this;
                      setTimeout(function() {
                        $(self).dequeue("exclusive_queue");
                      }, 1000);
                });


                $(window).dequeue("exclusive_queue");
            }
        }
    });

}

function set_group_properties_to_exclusive(property_id, show_update)
{

    var group_id = $("#selected-allocation-group").val();
    var project_id = $("#current_project_id").val();

    $.ajax({
        url: '/update_property_to_exclusive/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'property_id': property_id},
        success: function (data) {

            $("#span-exclusive-property-toggle-"+property_id).text('True');
            $("#property-group-selection-"+property_id).addClass('property-is-exclusive');
            $("#exclusive-status-"+ property_id).val('True');
            $("#show-property-exclusive-"+property_id).html('<i class="fa fa-check"></i> <span style="color: transparent">True</span>');

            if (show_update)
            {
                    $().toastmessage('showToast', {text : "Property has been set to Exclusive",
                    position : 'top-left', stayTime: 2000, type : 'success'});

                    $(window).queue("exclusive_queue", function() {
                        console.log('filter_images in set exclusive');
                        filter_images(class_name);
                        var self = this;
                          setTimeout(function() {
                            $(self).dequeue("exclusive_queue");
                          }, 1000);
                    });

                    $(window).queue("exclusive_queue", function() {
                        console.log('update_with_all_custom_values');

                        update_with_all_custom_values('all-props');
                        $(".remove_property_for_listing").removeClass('selected');

                        var self = this;
                          setTimeout(function() {
                            $(self).dequeue("exclusive_queue");
                          }, 1000);
                    });


                    $(window).dequeue("exclusive_queue");
                }


        }
    });
}


function edit_allocation_group_details(group_id)
{
    ////console.log('edit_allocation_group_details');
    $("#select-group-section").hide('slow');

    $(".allocation-group-row").removeClass('selected-low-highlight');
    $("#allocation-group-row-"+group_id).addClass('selected-low-highlight');

    var current_group_name = $("#group-allocation-name-field-"+group_id).val();
    var current_group_hidden = $("#group-allocation-hidden-field-"+group_id).val();

    
    $("#edit-allocation-group-name").val(current_group_name);

    $("#selected-allocation-group").val(group_id);
    $("#edit-allocation-group-hidden").val(current_group_hidden);

    var parent = $("#group-allocation-parent-field-"+group_id).val();

    if (parent)
    {
        $("#edit-allocation-group-parent").val(parent);
    }
    else
    {
        $("#edit-allocation-group-parent").val('');
    }

    $('html, body').animate({
        scrollTop: $("#edit-allocation-group").show().offset().top
    }, 2000);

}


function save_allocation_group_name()
{
    var group_id = $("#selected-allocation-group").val();
    var current_group_name = $("#group-allocation-name-field-"+group_id).val();
    var edited_group_name = $("#edit-allocation-group-name").val();
    var edited_group_hidden = $("#edit-allocation-group-hidden").val();
    var project_id = $("#current_project_id").val();

    var selected_parent_id = $("#edit-allocation-group-parent").val();

    $.ajax({
        url: '/update_edited_allocation_group_name/'+project_id+'/',
        type: 'GET',
        data: {'group_id': group_id, 'edited_group_name': edited_group_name,
                'selected_parent_id': selected_parent_id, 'is_hidden': edited_group_hidden},
        success: function (data) {
            $().toastmessage('showToast', {text : "Allocation group name has been updated.",
                position : 'top-left', stayTime: 2000, type : 'success'});

            $("#group-allocation-name-field-"+group_id).val(edited_group_name);
            $(".group-allocation-name-field-"+group_id).text(edited_group_name);
            $("#group-allocation-hidden-field-"+group_id).val(edited_group_hidden);

            if (selected_parent_id == "no_parent")
            {
                $("#group-allocation-parent-field-"+group_id).val('');
            }
            else
            {
                $("#group-allocation-parent-field-"+group_id).val(selected_parent_id);
            }

        }
    });
}

