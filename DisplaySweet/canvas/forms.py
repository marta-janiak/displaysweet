from datetime import timedelta
import os
import shutil
from django import forms
from django.conf import settings
from itertools import chain
from core.models import Project, CSVFileImport
from core.forms import FilterForm
from django.contrib.contenttypes.models import ContentType
from users.models import User
from django.apps import apps
from utils import happyforms
from utils.tools import DynamicFileInput
from django.forms.models import model_to_dict
from utils.xlxs_reader import ImportedWorkbook, InvalidImportException
from utils.template_creator.xlxs_reader import ImportedWorkbook as TemplateImportedWorkBook

from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.db.models.query import ValuesListQuerySet
from django.apps import apps


def get_and_set_devices_form(project, devices_model, devices_fields, names_model):

    class _ObjectFilterForm(forms.ModelForm):

        name = forms.CharField(label="", widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
        type_of_device = forms.CharField(label="", required=False, widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))

        class Meta:
            model = devices_model
            excludes = ('device_type',)
            fields = ()

        def __init__(self, *args, **kwargs):
            super(_ObjectFilterForm, self).__init__(*args, **kwargs)

            if self.instance:
                name = names_model.objects.using(project.project_connection_name).filter(pk=self.instance.device_name_id)
                if name:
                    self.fields['name'].initial = name[0].english

        def clean(self):
            cleaned_data = self.cleaned_data
            return cleaned_data

        def save(self, commit=True):
            device = super(_ObjectFilterForm, self).save(commit=False)

            cleaned_data = self.cleaned_data
            name = cleaned_data.get('name', None)

            if name:
                if self.instance and self.instance.pk:
                    names = names_model.objects.using(project.project_connection_name).filter(pk=self.instance.device_name_id)
                    if names:
                        names[0].english = name
                        names[0].save(using=project.project_connection_name)

                else:
                    strings_type_object = apps.get_model(project.project_connection_name, 'Strings')
                    device_type_string = strings_type_object.objects.using(project.project_connection_name).filter(english='device')
                    if not device_type_string:
                        strings_type_object.objects.using(project.project_connection_name).create(english='device')
                        device_type_string = strings_type_object.objects.using(project.project_connection_name).filter(english='device')

                    device_type_string = device_type_string[0]
                    device_type_object = apps.get_model(project.project_connection_name, 'Devicetypes')
                    device_type = device_type_object.objects.using(project.project_connection_name).filter(type_string=device_type_string)
                    if not device_type:
                        device_type_object.objects.using(project.project_connection_name).create(type_string=device_type_string)
                        device_type = device_type_object.objects.using(project.project_connection_name).filter(type_string=device_type_string)


                    category_model_object = apps.get_model(project.project_connection_name, 'Texttablecategories')
                    queryset = category_model_object.objects.using(project.project_connection_name).filter(category="names")

                    if queryset and device_type:
                        device_type = device_type[0]

                        category = queryset[0]
                        names_model_object = apps.get_model(project.project_connection_name, 'Names')
                        names_model_object.objects.using(project.project_connection_name).create(english=name, text_table_category=category)
                        names = names_model_object.objects.using(project.project_connection_name).filter(english=name,
                                                                                                         text_table_category=category).order_by('-id')
                        if names:
                            device.device_name = names[0]
                            device.device_type_id = device_type.pk
                            device.save(using=project.project_connection_name)

            return device

    return _ObjectFilterForm


def get_and_set_object_string_fields(project, model_instance, model_fields):

    class _ObjectForm(forms.Form):

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

            for model_field in model_fields:
                self.fields[model_field] = forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))

                if model_instance:
                    value = get_string_field(project, model_instance, model_field)
                    self.fields[model_field].initial = value

        def save(self, commit=True):
            cleaned_data = self.cleaned_data
            save_string_field(project, model_instance, cleaned_data)

    return _ObjectForm


def get_object_form(model_instance, model_fields, model_object, exclude_fields):

    class _ObjectForm(forms.ModelForm):
        class Meta:
            model = model_object
            excludes = exclude_fields
            fields = model_fields

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

            if model_instance:
                for field in self.fields:
                    value = get_repr(get_field(model_instance, field))
                    self.fields[field].initial = value

    return _ObjectForm



def get_repr(value):
    if callable(value):
        return '%s' % value()
    return value


def get_field(instance, field):
    field_path = field.split('.')

    attr = instance
    for elem in field_path:
        try:
            attr = getattr(attr, elem)
        except AttributeError:
            return None
    return attr


def get_string_field(project, instance, field):
    category_model_object = apps.get_model(project.project_connection_name, 'Texttablecategories')
    strings_queryset = category_model_object.objects.using(project.project_connection_name).filter(category="canvas_strings")
    if not strings_queryset:
        category_model_object.objects.using(project.project_connection_name).create(category="canvas_strings")
        strings_queryset = category_model_object.objects.using(project.project_connection_name).filter(category="canvas_strings")

    canvas_string = strings_queryset[0]

    strings_model_object = apps.get_model(project.project_connection_name, 'Strings')
    strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(text_table_category=canvas_string,
                                                                                                  english=field)
    if not strings_queryset:
        strings_model_object.objects.using(project.project_connection_name).create(text_table_category=canvas_string, english=field)
        strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(text_table_category=canvas_string, english=field)

    key_string = strings_queryset[0]

    cstrings_type_object = apps.get_model(project.project_connection_name, 'CanvasStringFields')
    key_string_value = cstrings_type_object.objects.using(project.project_connection_name).filter(key_string=key_string, canvas=instance)
    if not key_string_value:
        cstrings_type_object.objects.using(project.project_connection_name).create(key_string=key_string, canvas=instance)
        key_string_value = cstrings_type_object.objects.using(project.project_connection_name).filter(key_string=key_string, canvas=instance)

    if key_string_value:
        key_string_value = key_string_value[0]
        return key_string_value.value


def save_string_field(project, instance, cleaned_data):

    for field, value in cleaned_data.items():
        category_model_object = apps.get_model(project.project_connection_name, 'Texttablecategories')
        strings_queryset = category_model_object.objects.using(project.project_connection_name).filter(category="canvas_strings")
        if not strings_queryset:
            category_model_object.objects.using(project.project_connection_name).create(category="canvas_strings")
            strings_queryset = category_model_object.objects.using(project.project_connection_name).filter(category="canvas_strings")

        canvas_string = strings_queryset[0]

        strings_model_object = apps.get_model(project.project_connection_name, 'Strings')
        strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(text_table_category=canvas_string,
                                                                                                      english=field)
        if not strings_queryset:
            strings_model_object.objects.using(project.project_connection_name).create(text_table_category=canvas_string, english=field)
            strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(text_table_category=canvas_string, english=field)

        key_string = strings_queryset[0]

        cstrings_type_object = apps.get_model(project.project_connection_name, 'CanvasStringFields')
        key_string_value = cstrings_type_object.objects.using(project.project_connection_name).filter(key_string=key_string, canvas=instance)
        if not key_string_value:
            cstrings_type_object.objects.using(project.project_connection_name).create(key_string=key_string, canvas=instance)
            key_string_value = cstrings_type_object.objects.using(project.project_connection_name).filter(key_string=key_string, canvas=instance)

        if key_string_value:
            key_string_value = key_string_value[0]
            key_string_value.value = value
            key_string_value.save(using=project.project_connection_name)