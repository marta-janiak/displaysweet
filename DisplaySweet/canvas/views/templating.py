
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.forms.models import model_to_dict


from projects.models.model_mixin import PrimaryModelMixin
from ..forms import get_and_set_object_string_fields


@login_required
def templating(request, project_id, pk=None, canvas_id=None, wire_frame_id=None):
    template = "canvas/templating.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    selected_template_name = None
    selected_template = None
    templates = model_mixin.model_object("Templates").all()

    if pk:
        filtered_templates = model_mixin.model_object("Templates").get(pk=pk)
        selected_template_name = filtered_templates.name
        selected_template = filtered_templates

    else:
        if templates:
            selected_template_name = templates[0].name
            selected_template = templates[0]

    controllers = None
    if selected_template:
        controllers = model_mixin.model_object("WireframeControllers").filter(template_id=selected_template.pk)

    template_types = model_mixin.model_object("TemplateTypes").all()
    canvases =  model_mixin.model_object("Canvases").all()

    selected_canvas = None
    if canvases:
        selected_canvas = canvases[0]

    if canvas_id:
        selected_canvas = model_mixin.model_object("Canvases").get(pk=canvas_id)

    wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(template=selected_template,
                                                                                     canvas=selected_canvas)

    wireframes_from_wireframe_controllers = wireframe_controllers.filter(template__template_type_id=
                                                                         selected_template.template_type_id).values_list('wireframe_id',
                                                                                                                         flat=True)

    main_containers = model_mixin.model_object("Containers").filter(container_parent__isnull=True)
    container_templates = model_mixin.model_object("WireframeControllerContainers").all()

    wire_frame = None
    parent_wireframe = model_mixin.model_object("Wireframes").filter(pk__in=wireframes_from_wireframe_controllers,
                                                                      wireframe_parent_id=None)

    wireframes = model_mixin.model_object("Wireframes").filter(pk__in=wireframes_from_wireframe_controllers,
                                                                template__template_type_id=selected_template.template_type_id)

    wireframes = list(parent_wireframe) + list(wireframes)
    all_wireframe_ids = []
    for frame in wireframes:
        all_wireframe_ids.append(frame.pk)

    if wire_frame_id:
        wire_frame = model_mixin.model_object("Wireframes").get(pk=wire_frame_id)
    else:
        if wireframes:
            wire_frame = wireframes[0]
            wire_frame_id = wire_frame.pk

    if not wire_frame_id and not wireframe_controllers:
        all_parent_wireframes = model_mixin.model_object("Wireframes").filter(wireframe_parent_id=None)
        for p_wireframe in all_parent_wireframes:
            print ('1 create')
            model_mixin.model_object('WireframeControllers').create(template=selected_template,
                                                                     canvas=selected_canvas,
                                                                     wireframe=p_wireframe)

    if wire_frame_id:
        wireframe_controllers = wireframe_controllers.filter(wireframe_id=wire_frame_id)

        if not wireframe_controllers:
            print ('2 create')
            model_mixin.model_object('WireframeControllers').create(template=selected_template,
                                                                    canvas=selected_canvas,
                                                                    wireframe_id=wire_frame_id)

        wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(template=selected_template,
                                                                                         canvas=selected_canvas,
                                                                                         wireframe_id=wire_frame_id)

    selected_container_templates = model_mixin.model_object('WireframeControllerContainers').filter(wireframe_controller=wireframe_controllers)

    if controllers and not selected_container_templates:
        containers = model_mixin.model_object('TemplateTypeContainers').filter(template_type_id=selected_template.template_type_id)
        for item in wireframe_controllers:
            for container in containers:
                print ('3 create')
                model_mixin.model_object('WireframeControllerContainers').create(wireframe_controller=item,
                                                                                 container_id=container.container_id)

    selected_container_templates = model_mixin.model_object('WireframeControllerContainers').filter(wireframe_controller=wireframe_controllers)

    main_container_list = []
    selected_container_id_list = []
    for sc in selected_container_templates:
        if sc.container_id not in selected_container_id_list:
            main_container_list.append(sc)
            selected_container_id_list.append(sc.container_id)

    template_containerlist, container_list = model_mixin.klass("Containers").get_template_container_object_list(main_container_list,
                                                                                                                selected_template,
                                                                                                                selected_canvas,
                                                                                                                wire_frame)

    canvas_project_settings = model_mixin.model_object("ContainerSettings").filter(main_container_id__in=container_list).order_by('orderidx')

    canvas_fields =  ["device", "no", "width", "height"]
    form_class = get_and_set_object_string_fields(model_mixin.project, selected_canvas, canvas_fields)
    string_form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        save_to_all = True

        if "selected_wireframe" in request.POST:
            selected_wireframe = request.POST['selected_wireframe']

            if not selected_wireframe in [0, "0"]:
                save_to_all = False
                all_wireframe_ids = [selected_wireframe, ]

        if string_form.is_valid():
            string_form.save()

        if "template_type" in request.POST and request.POST['template_type']:
            template_type = request.POST['template_type']

            selected_template.template_type_id = template_type
            selected_template.save(using=model_mixin.project.project_connection_name)

            containers = []
            template_types = model_mixin.model_object("TemplateTypeContainers").filter(template_type_id=template_type)
            for ttype in template_types:
                containers.append(ttype.container_id)

            children_containers = model_mixin.model_object("Containers").filter(container_parent_id__in=containers)
            for c in children_containers:
                containers.append(c.pk)

            canvases = model_mixin.model_object("Canvases").all()

            for canvas in canvases:
                for wire_frame_id in all_wireframe_ids:
                    wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(template=selected_template,
                                                                                                     canvas=canvas,
                                                                                                     wireframe_id=wire_frame_id)

                    if not wireframe_controllers:
                        print ('4 create')
                        model_mixin.model_object('WireframeControllers').create(template=selected_template,
                                                                                 canvas=canvas,
                                                                                 wireframe_id=wire_frame_id)

                        wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(template=selected_template,
                                                                                                        canvas=canvas,
                                                                                                        wireframe_id=wire_frame_id)

                    for wc in wireframe_controllers:
                        if wc.wireframe_id:
                            this_wire_frame = model_mixin.model_object("Wireframes").filter(pk=wc.wireframe_id)
                            if this_wire_frame:
                                model_mixin.klass("Containers").save_container_to_template(containers,
                                                                                           selected_template,
                                                                                           canvas,
                                                                                           this_wire_frame[0])

                                if save_to_all:
                                    # print ('selected_canvas', selected_canvas

                                    model_mixin.klass("Containers").get_template_container_object_list(main_container_list,
                                                                                                       selected_template,
                                                                                                       selected_canvas,
                                                                                                       this_wire_frame)

                                    model_mixin.klass("Containers").save_container_string_fields(request.POST,
                                                                                                 selected_canvas_id=selected_canvas.pk,
                                                                                                 wireframe_controller=wc,
                                                                                                 save_to_all=True)

                    if not save_to_all:
                        model_mixin.klass("Containers").save_container_string_fields(request.POST, selected_canvas_id=selected_canvas.pk)

            if selected_template.pk:
                messages.success(request, "Template saved successfully")
                if wire_frame:
                    return redirect('canvas-edit-templating-canvas', model_mixin.project.pk, selected_template.pk, selected_canvas.pk, wire_frame.pk)
                else:
                    return redirect('canvas-edit-templating',  model_mixin.project.pk, selected_template.pk, selected_canvas.pk)

    context = {
        'sub_nav': True,
        'templates': templates,
        'selected_template_name': selected_template_name,
        'this_template': selected_template,
        'main_containers': main_containers,
        'container_templates': container_templates,
        'canvases': canvases,
        'selected_container_templates': selected_container_templates,
        'canvas_project_settings': canvas_project_settings,
        'template_containerlist': template_containerlist,
        'controllers': controllers,
        'template_types': template_types,
        'selected_canvas': selected_canvas,
        'string_form': string_form,
        'wire_frame': wire_frame,
        'wireframes': wireframes
    }

    return render(request, template, context)


@login_required
def template_types(request, project_id, pk, canvas_id, template_id=None):
    template = "canvas/template_types.html"

    non_editable_template_types = ['Thumbnail Gallery', 'Standard Image Gallery',  'Gallery with menu option']
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    container_templates = model_mixin.model_object("WireframeControllerContainers").all()
    main_containers = model_mixin.model_object("Containers").filter(container_parent__isnull=True)
    template_types = model_mixin.model_object("TemplateTypes").all()

    selected_type = model_mixin.model_object("TemplateTypes").get(pk=pk)
    container_list = model_mixin.model_object("TemplateTypeContainers").filter(template_type=selected_type)
    type_containers = model_mixin.queryset_to_list(container_list)

    if request.method == "POST":
        if "new_template_type" in request.POST and request.POST['new_template_type']:
            new_template_type = request.POST['new_template_type']
            model_mixin.model_object("TemplateTypes").create(name=new_template_type)
            template_type = model_mixin.model_object("TemplateTypes").filter(name=new_template_type)

            if template_type and template_id:
                messages.success(request, "Template type saved successfully")
                return redirect('canvas-template-types', project.pk, template_type[0].pk, canvas_id, template_id)

        if "template_type_name" in request.POST and request.POST['template_type_name']:
            template_type_name = request.POST['template_type_name']

            if not str(template_type_name) == str(selected_type.name):
                if not selected_type.name in non_editable_template_types:
                    selected_type.name = template_type_name
                    selected_type.save(using=project.project_connection_name)
                else:
                    messages.info(request, "You cannot change this template type name, it is a mandatory template type.")

        if "containers" in request.POST:
            containers = request.POST.getlist('containers')

            containers_to_remove = []
            existing_containers = []

            for item in container_list:
                existing_containers.append(int(item.container_id))

            for e_container in existing_containers:
                if int(e_container) not in containers:
                    containers_to_remove.append(int(e_container))

            if containers_to_remove:
                for item in containers_to_remove:
                    model_mixin.model_object("TemplateTypeContainers").filter(template_type=selected_type,
                                                                               container_id=item).delete()

            for container in containers:
                model_mixin.model_object("TemplateTypeContainers").get_or_create(template_type=selected_type,
                                                                                  container_id=container)

            all_templates_with_type = []
            template_types = model_mixin.model_object("TemplateTypeContainers").filter(template_type=selected_type)

            for ttype in template_types:
                model_dict = model_to_dict(ttype)
                template = model_mixin.model_object("Templates").filter(template_type_id=model_dict['template_type'])

                if template and not template in all_templates_with_type:
                    all_templates_with_type.append(template[0])

            containers = request.POST.getlist('containers')

            for selected_template in all_templates_with_type:
                children_containers = model_mixin.model_object("Containers").filter(container_parent_id__in=containers)
                for c in children_containers:
                    containers.append(c.pk)

                wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(template=selected_template,
                                                                                                 canvas_id=canvas_id)

                for controller in wireframe_controllers:
                    canvas = model_mixin.model_object("Canvases").get(pk=controller.canvas_id)
                    wireframe = model_mixin.model_object("Wireframes").filter(pk=controller.wireframe_id)
                    if canvas and wireframe:
                        model_mixin.klass("Containers").save_container_to_template(containers, selected_template, canvas, wireframe[0])

                model_mixin.klass("Containers").save_container_string_fields(request.POST, selected_template)

        messages.success(request, "Template type containers saved successfully")
        if template_id:
            return redirect('canvas-template-types', project_id, pk, canvas_id, template_id )

        return redirect('canvas-template-types', project_id, pk, canvas_id)

    context = {
        'sub_nav': True,
        'container_templates': container_templates,
        'main_containers': main_containers,
        'selected_type': selected_type,
        'template_types': template_types,
        'type_containers': type_containers,
        'canvas_id': canvas_id,
        'template_id': template_id
    }

    return render(request, template, context)
