from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages


from core.models import *
from django.forms.models import modelformset_factory
from projects.models.model_mixin import PrimaryModelMixin
from projects.forms import get_property_name_template_form


@login_required
def project_select(request, pk):
    project = Project.objects.get(pk=pk)
    return redirect('canvas-devices', project.pk)


@login_required
def home(request):
    template = "canvas/home.html"

    context = {
        'sub_nav': True,
    }

    return render(request, template, context)


def save_new_floor_level_names(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    new_floor = model_mixin.model_object("Floors").filter(pk=floor_id)[0]

    new_level_name = model_mixin.model_object("Names").filter(pk=new_floor.name_id)[0]
    new_level_name = new_level_name.english

    properties = model_mixin.model_object("Properties").filter(floor_id=floor_id).values_list('name_id', flat=True)
    number_of_apartments = len(properties)

    names_model_object = apps.get_model(model_mixin.project.project_connection_name, 'Names')
    property_names = model_mixin.model_object("Names").filter(pk__in=properties)

    form = get_property_name_template_form(model_mixin.project, model_mixin, names_model_object)
    PropertyNameFormSet = modelformset_factory(names_model_object, form=form, can_delete=True, extra=0)

    names_formset = PropertyNameFormSet(queryset=property_names)

    if request.method == "POST":
        devices_formset = PropertyNameFormSet(request.POST, queryset=property_names)
        for form in devices_formset:
            if form.is_valid():
                form.save()

        messages.success(request, "Devices saved successfully")
        return redirect('canvas-devices', model_mixin.project.pk)

    context = {
        'project': model_mixin.project,
        'formset_facory_form': form,
        'names_formset': names_formset,
        'floor_id': floor_id,
        'new_level_name': new_level_name,
        'number_of_apartments': number_of_apartments
    }
    template = "canvas/property_names.html"

    return render(request, template, context)