import json
import csv
import os, sys
import string
from PIL import Image


from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages


from ..forms import get_and_set_devices_form
from django.forms.models import modelformset_factory
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def devices(request, project_id):
    template = "canvas/devices.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)
    devices = model_mixin.model_object("Devices").all()

    form = get_and_set_devices_form(model_mixin.project, model_mixin.get_class_object('Devices'), model_mixin,
                                    model_mixin.get_class_object('Names'))
    DevicesFormSet = modelformset_factory(model_mixin.get_class_object('Devices'), form=form, extra=1, can_delete=True)
    devices_formset = DevicesFormSet(queryset=devices)

    if request.method == "POST":
        devices_formset = DevicesFormSet(request.POST, queryset=devices)
        for form in devices_formset:
            if form.is_valid():
                form.save()

        messages.success(request, "Devices saved successfully")
        return redirect('canvas-devices', model_mixin.project.pk)

    context = {
        'sub_nav': True,
        'devices_formset': devices_formset,
        'devices': devices,
    }

    return render(request, template, context)