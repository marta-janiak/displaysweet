from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from core.models import *
from projects.permissions import ProjectModelMixin
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def completed_canvas_publish(request, pk, canvas_id):
    template = "canvas/canvas_published_preview.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    request.session['canvas_id'] = canvas_id

    menus = None
    publishing_path = model_mixin.project.get_publish_path_example

    if not model_mixin.project.project_publishing_path:
        model_mixin.project.project_publishing_path = publishing_path
        model_mixin.project.save()

    main_wireframe_canvas = model_mixin.model_object("CanvasWireframes").filter(canvas_id=canvas_id)
    if main_wireframe_canvas:
        main_wireframe_canvas = main_wireframe_canvas[0]
        wireframe = main_wireframe_canvas.wireframe_id
        menus = model_mixin.model_object("Wireframes").filter(wireframe_parent=wireframe).order_by('orderidx')

    selected_canvas = model_mixin.model_object("Canvases").filter(pk=canvas_id)[0]

    context = {'canvas': selected_canvas,
               'project_image_folder': os.path.join(settings.BASE_IMAGE_LOCATION,
                                                    model_mixin.project.project_connection_name),
               'publishing_location_for_project': model_mixin.project.project_publishing_path,
               'publishing_path': publishing_path,
               'menus': menus}

    return render(request, template, context)


@login_required
def publish_canvas(request, pk, canvas_id):
    template = "canvas/canvas_to_publish.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    request.session['canvas_id'] = canvas_id

    menus = None

    publishing_path = model_mixin.project.get_publish_path_example

    if not model_mixin.project.project_publishing_path:
        model_mixin.project.project_publishing_path = publishing_path
        model_mixin.project.save()

    main_wireframe_canvas = model_mixin.model_object("CanvasWireframes").filter(canvas_id=canvas_id)
    if main_wireframe_canvas:
        main_wireframe_canvas = main_wireframe_canvas[0]
        wireframe = main_wireframe_canvas.wireframe_id
        menus = model_mixin.model_object("Wireframes").filter(wireframe_parent=wireframe).order_by('orderidx')

    selected_canvas = model_mixin.model_object("Canvases").filter(pk=canvas_id)[0]

    context = {'canvas': selected_canvas,
               'project_image_folder': os.path.join(settings.BASE_IMAGE_LOCATION, model_mixin.project.project_connection_name),
               'publishing_location_for_project': model_mixin.project.project_publishing_path,
               'publishing_path': publishing_path,
               'menus': menus}

    return render(request, template, context)


@login_required
def delete_canvas_model_instant(request, pk, model_name, row_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    controllers = model_mixin.model_object("WireframeControllers").filter(canvas_id=row_id).values_list('pk', flat=True)
    model_mixin.model_object("WireframeControllerContainers").filter(wireframe_controller_id=controllers).delete()

    model_instance.delete(using=project.project_connection_name)
    messages.success(request, "Canvas deleted successfully")
    return redirect('canvas-wireframes', project.pk)


@login_required
def canvasing(request, project_id, pk=None):
    template = "canvas/canvas.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    devices = model_mixin.model_object("Devices").all()
    canvases =  model_mixin.model_object("Canvases").all()

    device_list = []
    if pk:
        selected_canvas =  model_mixin.model_object("Canvases").filter(pk=pk)
        if selected_canvas:
            selected_canvas = selected_canvas[0]

            for device in devices:
                canvas_device = model_mixin.model_object("CanvasDevices").filter(device=device, canvas=selected_canvas)
                if canvas_device:
                    device_list.append({'device': device, 'selected': True})
                else:
                    device_list.append({'device': device, 'selected': False})

    else:
        selected_canvas = None

    if request.method == "POST":
        if "canvas_name" in request.POST:
            canvas_name = request.POST['canvas_name']
            selected_canvas = model_mixin.klass("Canvases").save_canvas_name(canvas_name, selected_canvas)

        if 'selected_device' in request.POST:
            selected_devices = request.POST.getlist('selected_device')
            for device in devices:
                if str(device.pk) in selected_devices:
                    model_mixin.model_object("CanvasDevices").get_or_create(device=device, canvas=selected_canvas)
                else:
                    model_mixin.model_object("CanvasDevices").filter(device=device, canvas=selected_canvas).delete()

        messages.success(request, 'Canvas details saved successfully')
        return redirect('canvas-edit-canvasing', project_id, selected_canvas.pk)

    context = {
        'sub_nav': True,
        'devices': devices,
        'canvases': canvases,
        'selected_canvas': selected_canvas,
        'device_list': device_list,
        'screen_list': [{'screen_name': 'Screen One', 'pk': 1}]

    }

    return render(request, template, context)


@login_required
def canvasing_presenter(request, project_id, pk):
    template = "canvas/canvas_presenter.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    devices = model_mixin.model_object("Devices").all()
    canvases =  model_mixin.model_object("Canvases").all()

    device_list = []
    if pk:
        selected_canvas = model_mixin.model_object("Canvases").filter(pk=pk)
        if selected_canvas:
            selected_canvas = selected_canvas[0]

            for device in devices:
                canvas_device = model_mixin.model_object("CanvasDevices").filter(device=device, canvas=selected_canvas)
                if canvas_device:
                    device_list.append({'device': device, 'selected': True})
                else:
                    device_list.append({'device': device, 'selected': False})

    else:
        selected_canvas = None

    context = {
        'sub_nav': True,
        'devices': devices,
        'canvases': canvases,
        'selected_canvas': selected_canvas,
        'device_list': device_list
    }

    return render(request, template, context)


def handle_uploaded_file(f, file_name_and_path):
    with open(file_name_and_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
