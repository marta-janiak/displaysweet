from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from projects.models.model_mixin import PrimaryModelMixin


@login_required
def containers(request, project_id):
    template = "canvas/containers.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)
    main_containers = model_mixin.model_object("Containers").filter(container_parent__isnull=True, cms_display=True)

    if request.method == "POST":
        model_mixin.klass("Containers").save_new_container_data(request.POST)
        messages.info(request, "Containers saved successfully.")
        return redirect('canvas-containers', project_id)

    context = {
        'sub_nav': True,
        'template_columns_form': [1,2,3],
        'containers': containers,
        'main_containers': main_containers
    }

    return render(request, template, context)


@login_required
def containers_edit(request, project_id, pk):
    template = "canvas/containers_edit.html"
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    main_containers = model_mixin.model_object('Containers').filter(container_parent__isnull=True, cms_display=True).order_by('orderidx')

    for main in main_containers:
        container_settings = model_mixin.model_object("ContainerSettings").filter(main_container_id=main.pk)
        menus = model_mixin.model_object('Containers').filter(container_parent=main).order_by('orderidx')
        for menu in menus:
            if not container_settings:
                model_mixin.model_object("ContainerSettings").create(main_container_id=main.pk,
                                                                      container_id=menu.pk,
                                                                      title=model_mixin.model_object('Names').filter(pk=menu.container_name_id)[0].english)



                container_settings = model_mixin.model_object("ContainerSettings").filter(main_container_id=main.pk)
                container_settings = container_settings[0]

                template_containers = model_mixin.model_object('WireframeControllerContainers').filter(container=menu)

                if template_containers:
                    template_container = template_containers[0]
                    container_strings = model_mixin.model_object('ContainerStringFields').filter(template_container=template_container)
                    i = 1
                    for string_value in container_strings:

                        model_mixin.model_object("ContainerFields").create(container_settings=container_settings,
                                                                            string_field=model_mixin.model_object('Strings').filter(pk=string_value.key_string_id)[0].english,
                                                                            field_type='text_box',
                                                                            orderidx=i)
                        i+=1

    this_template = model_mixin.model_object("Containers").filter(pk=pk)
    if this_template:
        this_template = this_template[0]

    menus = model_mixin.model_object("Containers").filter(container_parent=this_template).order_by('orderidx')

    if request.method == "POST":
        model_mixin.klass("Containers").edit_new_container_data(request.POST, this_template)
        messages.success(request, "Containers have been saved successsfully.")
        return redirect('canvas-edit-containers', project_id, pk)

    context = {
        'sub_nav': True,
        'containers': containers,
        'main_containers': main_containers,
        'menus': menus,
        'this_template': this_template
    }

    return render(request, template, context)

