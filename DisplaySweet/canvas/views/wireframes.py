import json
import csv
import os, sys
import string
from PIL import Image


from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.forms.models import model_to_dict


from core.models import *
from projects.permissions import ProjectModelMixin
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def wireframes(request, project_id):
    template = "canvas/wireframes.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    templates = model_mixin.model_object("Templates").all()
    main_wireframes = model_mixin.model_object("Wireframes").filter(wireframe_parent__isnull=True).order_by('id')
    canvases = model_mixin.model_object("Canvases").all()

    if request.method == "POST":
        canvas_to_clone = request.POST['selected_canvas']
        canvas_to_clone = model_mixin.model_object("Canvases").get(pk=canvas_to_clone)

        if canvas_to_clone:
            new_canvas_name = request.POST['new_canvas_name']
            if new_canvas_name == "":
                new_canvas_name = "New Canvas"

            new_wireframe_name = request.POST['new_wireframe_name']
            canvas_to_clone = model_mixin.model_object("Canvases").filter(pk=canvas_to_clone.pk)
            model_mixin.klass("Wireframes").clone_entire_wireframe(canvas_to_clone, new_canvas_name, new_wireframe_name)

    context = {
        'sub_nav': True,
        'template_columns_form': [1,2,3],
        'templates': templates,
        'main_wireframes': main_wireframes,
        'canvases': canvases
    }

    return render(request, template, context)


@login_required
def wireframes_canvas_edit(request, project_id, pk):
    template = "canvas/wireframes_edit.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    menus = []

    templates = model_mixin.model_object("Templates").all()
    main_wireframes = model_mixin.model_object("Wireframes").filter(wireframe_parent__isnull=True)
    canvases = model_mixin.model_object("Canvases").all()
    selected_canvase = model_mixin.model_object("Canvases").filter(pk=pk)

    selected_canvase_id = pk

    #this is if they change the project in the header & the id doesn't correlate between projects
    if selected_canvase:
        selected_canvase = canvases[0]
        selected_canvase_id = pk

    if not selected_canvase:
        canvases = model_mixin.model_object("Canvases").all().order_by('-pk')
        selected_canvase = canvases[0]
        selected_canvase_id = selected_canvase.pk

    wire_parent_id = None
    canvas_wireframes = model_mixin.model_object("CanvasWireframes").filter(canvas_id=selected_canvase_id).order_by(
        'pk')

    if not canvas_wireframes:
        model_mixin.model_object("Names").create(english="New Wireframe")
        new_name = model_mixin.model_object("Names").filter(english="New Wireframe")

        model_mixin.model_object("Wireframes").create(display_name=new_name[0], orderidx=1)
        new_wireframe = model_mixin.model_object("Wireframes").filter(display_name=new_name[0],
                                                                      orderidx=1)

        if new_wireframe:
            new_wireframe = new_wireframe[0]
            wire_parent_id = new_wireframe.pk

        model_mixin.model_object("CanvasWireframes").create(canvas_id=selected_canvase_id, wireframe=new_wireframe)
        canvas_wireframes = model_mixin.model_object("CanvasWireframes").filter(canvas_id=selected_canvase_id)

    if canvas_wireframes:
        canvas_wireframes = canvas_wireframes[0]
        model_dict = model_to_dict(canvas_wireframes)
        wire_parent_id = model_dict['wireframe']
        menus = model_mixin.model_object("Wireframes").filter(wireframe_parent_id=wire_parent_id).order_by('orderidx')

    if request.method == "POST" and wire_parent_id:
        # Checking if the menus have been deleted
        current_menu_ids = menus.values_list('pk', flat=True)
        data = dict(request.POST.items())
        model_mixin.klass("Wireframes").check_and_delete_wireframes_assets(current_menu_ids, data)

        model_mixin.klass("Wireframes").edit_new_wireframe_data(request.POST, selected_canvase, wire_parent_id)
        messages.success(request, "Wireframes details have been saved successfully")
        return redirect('canvas-edit-wireframe', project_id, pk)

    context = {
        'sub_nav': True,
        'templates': templates,
        'main_wireframes': main_wireframes,
        'menus': menus,
        'canvas': selected_canvase,
        'total_sections': len(menus),
        'canvases': canvases,
        'wire_parent_id': wire_parent_id
    }

    return render(request, template, context)


@login_required
def wireframes_select_preview(request, project_id, canvas_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    selected_canvase = model_mixin.model_object("Canvases").filter(pk=canvas_id)

    wire_parent_id = None
    if selected_canvase:
        selected_canvase = selected_canvase[0]

        canvas_wireframes = model_mixin.model_object("CanvasWireframes").filter(canvas=selected_canvase)

        if not canvas_wireframes:
            model_mixin.model_object("Names").create(english="New Wireframe")
            new_name = model_mixin.model_object("Names").filter(english="New Wireframe")

            model_mixin.model_object("Wireframes").create(display_name=new_name[0], orderidx=1)
            new_wireframe = model_mixin.model_object("Wireframes").filter(display_name=new_name[0],
                                                                           orderidx=1)

            if new_wireframe:
                new_wireframe = new_wireframe[0]
                wire_parent_id = new_wireframe.pk

            model_mixin.model_object("CanvasWireframes").create(canvas=selected_canvase, wireframe=new_wireframe)
            canvas_wireframes = model_mixin.model_object("CanvasWireframes").filter(canvas=selected_canvase)

        if canvas_wireframes:
            for canvas_wireframe in canvas_wireframes:
                wireframe = model_mixin.model_object("Wireframes").filter(pk=canvas_wireframe.wireframe_id)

                if wireframe:
                    wireframe = wireframe[0]
                    sub_wireframes = model_mixin.model_object("Wireframes").filter(wireframe_parent_id=wireframe.pk)
                    for sub_wireframe in sub_wireframes:
                        if "Plans" in str(sub_wireframe.name):
                            return redirect('canvas-wireframe-preview', model_mixin.project.pk, canvas_id, sub_wireframe.pk)

            canvas_wireframes = canvas_wireframes[0]
            model_dict = model_to_dict(canvas_wireframes)
            wire_parent_id = model_dict['wireframe']

        return redirect('canvas-wireframe-preview', model_mixin.project.pk, canvas_id, wire_parent_id)


@login_required
def wireframes_preview(request, project_id, canvas_id, wireframe_id, selected_parent_wireframe=None, selected_sub_wireframe=None):
    
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    if len(model_mixin.klass("Floors").get_all_floors()) == 0:
        messages.error(request, "You have not imported any data to your project yet. Start here.")
        return redirect("projects-select-template", model_mixin.project.pk)

    menus, parent_menus, sub_menus, selected_wireframe, \
    selected_parent_wireframe, selected_sub_wireframe = model_mixin.klass("Wireframes").get_wireframe_preview_menus(canvas_id,
                                                                                                                    wireframe_id,
                                                                                                                    selected_parent_wireframe,
                                                                                                                    selected_sub_wireframe)



    selected_template = model_mixin.model_object('Templates').filter(pk=selected_sub_wireframe['template'])

    asset_list = []
    asset_list_ids = []

    if selected_template:
        selected_template = selected_template[0]

        page_type = model_mixin.model_object("TemplateTypes").filter(pk=selected_template.template_type_id)
        template_type = None
        if page_type:
            template_type = page_type[0].name

        canvases = model_mixin.model_object('Canvases').all()
        selected_canvas = model_mixin.model_object('Canvases').filter(pk=canvas_id)
        if not selected_canvas:
            messages.info(request, "The same canvas did not exist whilst switching projects so select the first available.")
            selected_canvas = model_mixin.model_object('Canvases').all().order_by('pk')

        selected_canvas = selected_canvas[0]

        wireframe_settings = model_mixin.klass("Wireframes").get_wireframe_template_settings(selected_canvas,
                                                                          selected_template,
                                                                          selected_sub_wireframe,
                                                                          str(selected_template))

        wireframe_controller = model_mixin.klass("Wireframes").get_wireframe_controller(selected_canvas,
                                                                                        selected_template,
                                                                                        selected_sub_wireframe['id'])
        template_containers = model_mixin.model_object("WireframeControllerContainers").filter(wireframe_controller=wireframe_controller)

        containers = []
        for tc in template_containers:
            containers.append(tc.container_id)

        template_containers = model_mixin.model_object('TemplateTypeContainers').filter(template_type_id=selected_template.template_type_id)

        for tcontainer in template_containers:
            containers.append(tcontainer.container_id)


        sub_wireframe_id = selected_sub_wireframe['id']

        appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                          template_type,
                                                                                          'main')

        if not template_type in ["Level Plans", "Floor Plans", "Key Plans", "Multi Levels"]:
            main_container_assigned_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                        template_id=selected_template.pk,
                                                                                                        container_id=appropriate_container,
                                                                                                        wireframe_id=selected_sub_wireframe['id'])

            for container_asset in main_container_assigned_assets:
                asset = model_mixin.model_object("Assets").filter(pk=container_asset.asset_id)

                if asset:
                    asset = asset[0]
                    images = model_mixin.model_object("Resources").filter(pk=asset.source_id)

                    if images:
                        image = images[0]
                        tags = model_mixin.klass("Assets").get_image_asset_tags(image)
                        container_settings = model_mixin.klass("ContainerAssignedAssetSettings").get_image_settings(container_asset,
                                                                                                                    canvas_id=selected_canvas.pk,
                                                                                                                    template_id=selected_template.pk,
                                                                                                                    wireframe_id=selected_sub_wireframe['id'])

                        image_name = image.path.split("/")[-1]
                        extension = image_name[-3:]

                        image_static_path = '/static/%s/thumbnails/%s' % (model_mixin.project.project_connection_name, image_name)
                        static_file_path = '/static/%s/thumbnails/%s' % (model_mixin.project.project_connection_name, image_name)
                        file_type = 'image'

                        if extension in model_mixin.sound_files():
                            image_static_path = '/static/common/img/musical-notes.png'
                            static_file_path = '/static/%s/media/%s' % (model_mixin.project.project_connection_name, image_name)
                            file_type = 'sound'

                        if extension in model_mixin.video_files():
                            image_static_path = '/static/common/img/movie.png'
                            static_file_path = '/static/%s/media/%s' % (model_mixin.project.project_connection_name, image_name)
                            file_type = 'video'

                        if extension in model_mixin.pdf_files():
                            image_static_path = '/static/common/img/pdf.jpg'
                            static_file_path = '/static/%s/media/%s' % (model_mixin.project.project_connection_name, image_name)
                            file_type = 'pdf'

                        if extension in model_mixin.document_files():
                            image_static_path = '/static/common/img/document.jpg'
                            static_file_path = '/static/%s/media/%s' % (model_mixin.project.project_connection_name, image_name)
                            file_type = 'document'

                        full_image_name = image_name
                        if len(image_name) > 20:
                            try:
                                image_name = str(image_name)[:20] + '...'
                            except:
                                pass

                        asset_list.append({'asset': asset,
                                           'image': image,
                                           'image_name': image_name,
                                           'full_image_name': full_image_name,
                                           'tags': tags,
                                           'settings': container_settings,
                                           'image_static_path':image_static_path,
                                           'static_file_path': static_file_path,
                                           'image_type': 'main',
                                           'file_type': file_type})
                        asset_list_ids.append(image.pk)

        if asset_list:
            try:
                asset_list = sorted(asset_list, key=lambda k: int(k['settings']['order']))
            except:
                pass

    if request.method == "POST":
        if "selected-asset" in request.POST:
            images = list(set(request.POST.getlist('selected-asset')))

            order = 1
            asset_check_list = []
            for image_id in images:
                asset = model_mixin.model_object("Assets").filter(source_id=image_id)

                for as_image in asset:
                    asset_check_list.append(as_image.pk)

                if not asset:
                    asset = model_mixin.klass("Resources").save_resource_to_asset(image_id)

                if asset:
                    asset = asset[0]
                    asset_id = asset.pk

                    if containers:
                        asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                    template_id=selected_template.pk,
                                                                                                    container_id=appropriate_container,
                                                                                                    wireframe_id=selected_sub_wireframe['id'],
                                                                                                    asset_id=int(asset_id))
                        if not asset_created:
                            appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                                              template_type,
                                                                                                              'main')

                            model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=selected_canvas.pk,
                                                                                        template_id=selected_template.pk,
                                                                                        container_id=appropriate_container,
                                                                                        wireframe_id=selected_sub_wireframe['id'],
                                                                                        asset_id=int(asset_id),
                                                                                        orderidx=order)
                        order += 1
                        asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                    template_id=selected_template.pk,
                                                                                                    container_id=appropriate_container,
                                                                                                    wireframe_id=selected_sub_wireframe['id'],
                                                                                                    asset_id=int(asset_id))
                        if asset_created:
                            model_mixin.klass("Assets").save_asset_settings(request.POST, int(asset_id), asset_created, resource_id=image_id)

        if "selected-thumbnail-asset" in request.POST:
            images = request.POST.getlist('selected-thumbnail-asset')
            thumbnail_appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                                        str(selected_template),
                                                                                                        'unique_thumbnail_files')

            container_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                          template_id=selected_template.pk,
                                                                                          container_id=thumbnail_appropriate_container,
                                                                                          wireframe_id=selected_sub_wireframe['id'])

            model_mixin.model_object("ContainerAssignedAssetSettings").filter(container_assigned_asset__in=container_assets).delete()
            container_assets.delete()


            order = 1
            for image_id in images:
                asset = model_mixin.model_object("Assets").filter(source_id=image_id)

                if not asset:
                    asset = model_mixin.klass("Resources").save_resource_to_asset(image_id)

                if asset:
                    asset = asset[0]
                    asset_id = asset.pk

                    if containers:
                        asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                   template_id=selected_template.pk,
                                                                                                   container_id=thumbnail_appropriate_container,
                                                                                                   wireframe_id=selected_sub_wireframe['id'],
                                                                                                   asset_id=int(asset_id))

                        if not asset_created:
                            model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=selected_canvas.pk,
                                                                                       template_id=selected_template.pk,
                                                                                       container_id=thumbnail_appropriate_container,
                                                                                       wireframe_id=selected_sub_wireframe['id'],
                                                                                       asset_id=int(asset_id),
                                                                                       orderidx=order)
                        order += 1
                        asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                   template_id=selected_template.pk,
                                                                                                   container_id=thumbnail_appropriate_container,
                                                                                                   wireframe_id=selected_sub_wireframe['id'],
                                                                                                   asset_id=int(asset_id))

                        if asset_created:
                            model_mixin.klass("Assets").save_asset_settings(request.POST, int(asset_id), asset_created, resource_id=image_id)

        if "watermark_file" in request.POST:
            image = request.POST['watermark_file']

            asset = model_mixin.model_object("Assets").filter(source_id=image)

            if not asset:
                asset = model_mixin.klass("Resources").save_resource_to_asset(image)

            if asset:
                asset = asset[0]
                asset_id = asset.pk

                appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers, str(selected_template), 'watermark')

                if containers:
                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                               template_id=selected_template.pk,
                                                                                               container_id=appropriate_container,
                                                                                               wireframe_id=selected_sub_wireframe['id'],
                                                                                               asset_id=int(asset_id))

                    if not asset_created:
                        model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=selected_canvas.pk,
                                                                                   template_id=selected_template.pk,
                                                                                   container_id=appropriate_container,
                                                                                   wireframe_id=selected_sub_wireframe['id'],
                                                                                   asset_id=int(asset_id),
                                                                                   orderidx=1)

                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                               template_id=selected_template.pk,
                                                                                               container_id=appropriate_container,
                                                                                               wireframe_id=selected_sub_wireframe['id'],
                                                                                               asset_id=int(asset_id))
                    if asset_created:
                        model_mixin.klass("Assets").save_asset_settings(request.POST, int(asset_id), asset_created)

        if "individual_file" in request.POST:
            image = request.POST['individual_file']
            asset = model_mixin.model_object("Assets").filter(source_id=image)

            if not asset:
                asset = model_mixin.klass("Resources").save_resource_to_asset(image)

            if asset:
                asset = asset[0]
                asset_id = asset.pk

                appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                                  str(selected_template),
                                                                                                  'individual_image')
                if containers:
                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                               template_id=selected_template.pk,
                                                                                               container_id=appropriate_container,
                                                                                               wireframe_id=selected_sub_wireframe['id'],
                                                                                               asset_id=int(asset_id))

                    if not asset_created:
                        model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=selected_canvas.pk,
                                                                                    template_id=selected_template.pk,
                                                                                    container_id=appropriate_container,
                                                                                    wireframe_id=selected_sub_wireframe['id'],
                                                                                    asset_id=int(asset_id),
                                                                                    orderidx=1)

                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                template_id=selected_template.pk,
                                                                                                container_id=appropriate_container,
                                                                                                wireframe_id=selected_sub_wireframe['id'],
                                                                                                asset_id=int(asset_id))
                    if asset_created:
                        model_mixin.klass("Assets").save_asset_settings(request.POST, int(asset_id), asset_created)

        if "multi_file" in request.POST:
            image = request.POST['multi_file']

            asset = model_mixin.model_object("Assets").filter(source_id=image)

            if not asset:
                asset = model_mixin.klass("Resources").save_resource_to_asset(image)

            if asset:
                asset = asset[0]
                asset_id = asset.pk

                appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                                  str(selected_template),
                                                                                                  'multi_image')

                if containers:
                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                template_id=selected_template.pk,
                                                                                                container=appropriate_container,
                                                                                                wireframe_id=selected_sub_wireframe['id'],
                                                                                                asset_id=int(asset_id))

                    if not asset_created:
                        model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=selected_canvas.pk,
                                                                                    template_id=selected_template.pk,
                                                                                    container=appropriate_container,
                                                                                    wireframe_id=selected_sub_wireframe['id'],
                                                                                    asset_id=int(asset_id),
                                                                                    orderidx=1)

                    asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                               template_id=selected_template.pk,
                                                                                               container=appropriate_container,
                                                                                               wireframe_id=selected_sub_wireframe['id'],
                                                                                               asset_id=int(asset_id))
                    if asset_created:
                        model_mixin.klass("Assets").save_asset_settings(request.POST, int(asset_id), asset_created)

        if "removed_assets" in request.POST:
            removed_images = request.POST.getlist('removed_assets')

            for image_id in removed_images:
                assets = model_mixin.model_object("Assets").filter(source_id=image_id)

                for asset in assets:
                    asset_id = asset.pk

                    remove_assigned = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                 template_id=selected_template.pk,
                                                                                                 wireframe_id=selected_sub_wireframe['id'],
                                                                                                 asset_id=int(asset_id))

                    model_mixin.model_object("ContainerAssignedAssetSettings").filter(container_assigned_asset__in=remove_assigned).delete()
                    remove_assigned.delete()

        if "publish_content" in request.POST:
            print ("publishing all content")

        messages.success(request, "Wireframe preview details saved successfully")
        return redirect('canvas-wireframe-preview', model_mixin.project.pk, canvas_id, wireframe_id, selected_sub_wireframe['id'])

    project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, model_mixin.project.project_connection_name)
    if project_image_folder[-1] == "/":
        project_image_folder = project_image_folder[:-1]

    #Get only the assets / resources with no tags.
    image_list = model_mixin.klass("AssetTags").get_assets_without_tags()

    try:
        image_list = list(sorted(image_list, key=lambda k: str(k['full_image_name']).lower()))
    except Exception as e:
        print(e)

    context = {
        'sub_nav': True,
        'menus': menus,
        'parent_menus': parent_menus,
        'sub_menus': sub_menus,
        'canvas': selected_canvas,
        'total_sections': len(menus),
        'canvases': canvases,
        'selected_wireframe': selected_wireframe,
        'selected_parent_wireframe': selected_parent_wireframe,
        'selected_sub_wireframe': selected_sub_wireframe,
        'page_type': template_type,
        'asset_list': asset_list,
        'project': model_mixin.project,
        'image_list': image_list,
        'tag_names': model_mixin.model_object("AssetTags").all().order_by('tag').exclude(tag=''),
        'project_image_folder': project_image_folder,
        'static_image_folder': '/static/%s/thumbnails' % model_mixin.project.project_connection_name,
        'canvas_id': canvas_id,
        'wireframe_id': wireframe_id,
        'sub_wireframe_id': sub_wireframe_id,
        'template_id': selected_template.pk
    }

    extra_context = get_page_settings(request, template_type, model_mixin, wireframe_settings,
                                      asset_list_ids, containers, selected_canvas, selected_template,
                                      selected_sub_wireframe, menus, model_mixin.project)
    context.update(extra_context)

    template = "canvas/wireframes_preview.html"

    if template_type in ["Level Plans", "Floor Plans", "Key Plans", "Multi Levels"]:
        template = "canvas/templates/level_plans.html"

    return render(request, template, context)


def get_page_settings(request, page_type, model_mixin, wireframe_settings, asset_list_ids,
                      containers, selected_canvas, selected_template, selected_sub_wireframe, menus, project):

    individual_file = None
    individual_file_id = None
    individual_file_full = None
    multi_file = None
    multi_file_id = None
    multi_file_full = None
    individual_image_container = None
    multi_image_container = None

    context = {}
    level_image_selector = None
    editable_templates_fields = None
    selected_render_field = None

    if page_type == "Renders":
        editable_templates_fields = ProjectCSVTemplateFields.objects.filter(template__project=model_mixin.project)\
                                    .exclude(column_table_mapping="Properties")\
                                    .values_list('column_pretty_name', flat=True).distinct()\
                                    .order_by('column_pretty_name')

        selected_render_field = model_mixin.klass("Strings").get_string_from_category('property_render_field')

    if page_type in ["Thumbnail Gallery", 'Standard Image Gallery', "Gallery with menu option", "Fitting Gallery", "Multi Levels"]:

        default_source_image_settings = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('default_source_image_settings',
                                                                                                            wireframe_settings)

        thumbnail_details = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('thumbnail_details',
                                                                                                wireframe_settings)

        watermark = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('watermark', wireframe_settings)

        watermark_file = None
        watermark_file_id = None
        watermark_file_full = None

        appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                          page_type,
                                                                                          'watermark')

        watermark_files = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                     template_id=selected_template.pk,
                                                                                     container_id=appropriate_container,
                                                                                     wireframe_id=selected_sub_wireframe['id'])

        if watermark_files:
            watermarkfile = watermark_files[0]
            asset = model_mixin.model_object("Assets").filter(pk=watermarkfile.asset_id)
            if asset:
                image = model_mixin.model_object("Resources").filter(pk=asset[0].source_id)
                if image:
                    watermark_file = str(image[0].path).replace(settings.BASE_IMAGE_LOCATION, "/static/").replace("//", "/")
                    watermark_file_id = image[0].pk
                    watermark_file_full = image[0].path
            else:
                watermarkfile.delete(using=project.project_connection_name)

        level_image_selector = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('level_image_selector',
                                                                                wireframe_settings)

        individual_image_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                               page_type,
                                                                                               'individual_image')

        level_image_individual_files = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                  template_id=selected_template.pk,
                                                                                                  container_id=individual_image_container,
                                                                                                  wireframe_id=selected_sub_wireframe['id'])

        if level_image_individual_files:
            individual_file = level_image_individual_files[0]
            asset = model_mixin.model_object("Assets").filter(pk=individual_file.asset_id)
            if asset:
                image = model_mixin.model_object("Resources").filter(pk=asset[0].source_id)
                if image:
                    individual_file = str(image[0].path).replace(settings.BASE_IMAGE_LOCATION, "/static/").replace("//", "/")

                    individual_file_id = image[0].pk
                    individual_file_full = image[0].path
            else:
                individual_file.delete(using=project.project_connection_name)

        multi_image_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                          page_type,
                                                                                          'multi_image')

        level_image_multi_files = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                              template_id=selected_template.pk,
                                                                                              container_id=multi_image_container,
                                                                                              wireframe_id=selected_sub_wireframe['id'])
        if level_image_multi_files:
            multi_file = level_image_multi_files[0]
            asset = model_mixin.model_object("Assets").filter(pk=multi_file.asset_id)
            if asset:
                image = model_mixin.model_object("Resources").filter(pk=asset[0].source_id)
                if image:
                    image = image[0]
                    multi_file = str(image.path).replace(settings.BASE_IMAGE_LOCATION, "/static/").replace("//", "/")
                    print ('multi_file', multi_file)

                    multi_file_id = image.pk
                    multi_file_full = image.path.split("/")[-1]
            else:
                multi_file.delete(using=project.project_connection_name)

        unique_thumbnail_files = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('unique_thumbnail_files', wireframe_settings)

        unique_thumbnail_asset_list = []
        unique_thumbnail_asset_list_ids = []

        if unique_thumbnail_files:

            thumbnail_appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers,
                                                                                                        str(selected_template),
                                                                                                        'unique_thumbnail_files')

            unique_thumbnail_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                                 template_id=selected_template.pk,
                                                                                                 container_id=thumbnail_appropriate_container,
                                                                                                 wireframe_id=selected_sub_wireframe['id'])

            for thumbnail_asset in unique_thumbnail_assets:
                asset = model_mixin.model_object("Assets").filter(pk=thumbnail_asset.asset_id)

                if asset:
                    thumbnail_image = model_mixin.model_object("Resources").filter(pk=asset[0].source_id)

                    if thumbnail_image:
                        image = thumbnail_image[0]

                        tags = model_mixin.klass("Assets").get_image_asset_tags(image)
                        container_settings = model_mixin.klass("ContainerAssignedAssetSettings").get_image_settings(thumbnail_asset)

                        image_name = image.path.split("/")[-1]
                        image_static_path = '/static/%s/thumbnails/%s' % (project.project_connection_name, image_name)
                        static_file_path = '/static/%s/thumbnails/%s' % (project.project_connection_name, image_name)
                        file_type = 'image'

                        full_image_name = image_name
                        if len(image_name) > 30:
                            try:
                                image_name = str(image_name)[:30] + '...'
                            except:
                                pass

                        unique_thumbnail_asset_list.append({ 'image': image,
                                                             'image_name': image_name,
                                                             'full_image_name': full_image_name,
                                                             'tags': tags,
                                                             'settings': container_settings,
                                                             'image_static_path': image_static_path,
                                                             'static_file_path': static_file_path,
                                                             'file_type': file_type})

                        unique_thumbnail_asset_list_ids.append(image.pk)

        thumbnail_settings = model_mixin.klass("Wireframes").get_wireframe_settings_for_variable('thumbnail_settings', wireframe_settings)

        image_settings = 12
        image_size = 75
        if thumbnail_settings:
            if "thumbnail_settings" in thumbnail_settings:
                thumbnail_settings = thumbnail_settings['thumbnail_settings']

                image_settings, image_size = get_image_width_settings(request, thumbnail_settings)
                if not image_settings:
                    image_settings = 12
                    image_size = 75

        context = {
            'default_source_image_settings': default_source_image_settings,
            'thumbnail_details': thumbnail_details,
            'watermark': watermark,
            'static_image_folder': '/static/%s/thumbnails' % model_mixin.project.project_connection_name,
            'image_settings': image_settings,
            'watermark_file': watermark_file,
            'watermark_file_id': watermark_file_id,
            'watermark_file_full': watermark_file_full,

            'multi_file': multi_file,
            'multi_file_id': multi_file_id,
            'multi_file_full': multi_file_full,

            'individual_file': individual_file,
            'individual_file_id': individual_file_id,
            'individual_file_full': individual_file_full,

            'level_image_selector': level_image_selector,
            'image_size': image_size,
            'unique_thumbnail_files': unique_thumbnail_files,
            'unique_thumbnail_asset_list': unique_thumbnail_asset_list
        }

    if page_type in ["Level Plans"]:
        tag_selected = "level"
        context = model_mixin.klass("LevelPlans").get_level_plan_details(request=request)
        context.update({'tag_selected': tag_selected.replace("_", " "),
                        'multi_file': multi_file,
                        'multi_file_id': multi_file_id,
                        'multi_file_full': multi_file_full,
                        'individual_file': individual_file,
                        'individual_file_id': individual_file_id,
                        'individual_file_full': individual_file_full,
                        'individual_image_container': individual_image_container,
                        'multi_image_container': multi_image_container})

        return context

    if page_type in ["Floor Plans"]:
        tag_selected = "floor_plans"
        context = model_mixin.klass("LevelPlans").get_level_plan_details(request=request)
        context.update({'tag_selected': tag_selected.replace("_", " "),
                        'multi_file': multi_file,
                        'multi_file_id': multi_file_id,
                        'multi_file_full': multi_file_full,
                        'individual_file': individual_file,
                        'individual_file_id': individual_file_id,
                        'individual_file_full': individual_file_full,
                        'individual_image_container': individual_image_container,
                        'multi_image_container': multi_image_container})

        return context

    if page_type in ["Key Plans"]:
        tag_selected = "key_plans"
        context = model_mixin.klass("LevelPlans").get_level_plan_details(request=request)
        context.update({'tag_selected': tag_selected.replace("_", " "),
                        'multi_file': multi_file,
                        'multi_file_id': multi_file_id,
                        'multi_file_full': multi_file_full,
                        'individual_file': individual_file,
                        'individual_file_id': individual_file_id,
                        'individual_file_full': individual_file_full,
                        'individual_image_container': individual_image_container,
                        'multi_image_container': multi_image_container})

        return context

    tag_selected = page_type
    second_context = model_mixin.klass("LevelPlans").get_level_plan_details(request=request)
    context.update(second_context)

    if level_image_selector:
        context.update({'level_image_selector': level_image_selector})

    if editable_templates_fields:
        editable_templates_fields = [str(i) for i in editable_templates_fields]
        editable_templates_fields = json.dumps(editable_templates_fields)

    if selected_render_field:
        selected_render_field = selected_render_field.english

    context.update({'tag_selected': tag_selected.replace("_", " "),
                    'multi_file': multi_file,
                    'multi_file_id': multi_file_id,
                    'multi_file_full': multi_file_full,
                    'individual_file': individual_file,
                    'individual_file_id': individual_file_id,
                    'individual_file_full': individual_file_full,
                    'individual_image_container': individual_image_container,
                    'multi_image_container': multi_image_container,
                    'editable_templates_fields': editable_templates_fields,
                    'selected_render_field': selected_render_field })

    return context


def get_image_width_settings(request, thumbnail_settings):

    if thumbnail_settings == "1 Column":
        return '12', '60'

    if thumbnail_settings == "2 Columns":
        return '6', '25'

    if thumbnail_settings == "3 Columns":
        return '4', '70'

    if thumbnail_settings == "Full Screen":
        return '12', '75'



