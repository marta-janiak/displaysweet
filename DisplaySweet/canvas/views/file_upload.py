import json
import csv
import os, sys
import string
from PIL import Image

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from core.models import *
from projects.models.model_mixin import PrimaryModelMixin
from .canvas_details import handle_uploaded_file

@login_required
def folder_paths(request, project_id):
    template = "canvas/folder_paths.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, model_mixin.project.project_connection_name)
    image_list = model_mixin.klass("Assets").get_image_list_for_tags()

    if request.method == "POST":
        if not os.path.isdir(project_image_folder):
            os.makedirs(project_image_folder)

        thumbnail_path = os.path.join(project_image_folder, "thumbnails")
        if not os.path.isdir(thumbnail_path):
            os.makedirs(thumbnail_path)

        media_path = os.path.join(project_image_folder, "media")
        if not os.path.isdir(media_path):
            os.makedirs(media_path)

        basewidth = 500

        for afile in request.FILES.getlist('image_files'):

            file_name = afile.name
            file_name = file_name.replace(" ", "_", 10)

            extension = file_name[-3:]
            handle_uploaded_file(afile, os.path.join(project_image_folder, file_name))

            if extension in model_mixin.image_files():
                img = Image.open(os.path.join(project_image_folder, file_name))

                wpercent = (basewidth/float(img.size[0]))
                hsize = int((float(img.size[1])*float(wpercent)))

                img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                img.save(os.path.join(thumbnail_path, file_name))

                model_mixin.klass("Assets").add_new_image_asset(afile, project_image_folder)
            else:
                handle_uploaded_file(afile, os.path.join(media_path, file_name))
                model_mixin.klass("Assets").add_new_image_asset(afile, media_path)

        messages.success(request, "Images uploaded successfully")
        return redirect('canvas-folder_paths', project_id)

    context = {
        'sub_nav': True,
        'images': model_mixin.model_object("Resources").all().order_by('path'),
        'image_list': image_list,
        'project_image_folder': project_image_folder,
        'static_image_folder': '/static/%s/thumbnails' % model_mixin.project.project_connection_name,
        'tag_names': model_mixin.model_object("AssetTags").all().order_by('tag').exclude(tag=''),
    }

    return render(request, template, context)