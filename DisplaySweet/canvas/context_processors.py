from core.models import *


def return_canvas_attributes_project(request):

    if request.user and request.user.is_authenticated():
        check_access = True
        if request.session.get('project_id', True):
            path_info = request.META.get('PATH_INFO')
            path_info = path_info.split("/")

            found_initial_project = None
            users_field = False

            if not request.user.is_ds_admin:
                for item in path_info:
                    check_not_user_field = str(item).strip()
                    if check_not_user_field == "users":
                        users_field = True

                    if not users_field:
                        try:
                            test_item = int(item)
                            if not found_initial_project:
                                found_initial_project = test_item
                        except:
                            pass

            try:
                project_id = request.session['project_id']
                project = Project.objects.get(pk=project_id)

                if project and not project in request.user.project_users_list.filter(active=True):
                    if request.user.is_ds_admin:
                        projects = request.user.project_users_list.filter(active=True).exclude(name='Master')
                    elif request.user.is_client_admin:
                        projects = request.user.project_users_list.filter(active=True, status__in=[Project.STAGING,
                                                                                                   Project.LIVE]).exclude(name='Master')
                    else:
                        projects = request.user.project_users_list.filter(active=True, status=Project.LIVE).exclude(
                            name='Master')

                    if projects:
                        project = projects[0]
                        project_id = project.pk
                        request.session['project_id'] = project.pk
            except Exception as e:
                projects = request.user.project_users_list.filter(active=True)

                if projects:
                    if request.user.is_client_admin:
                        projects = projects.filter(status__in=[Project.STAGING,
                                                               Project.LIVE]).exclude(name='Master')
                    elif request.user.is_low_access_project_user:
                        projects = projects.filter(status=Project.LIVE)

                    if projects:
                        project = projects[0]
                        project_id = project.pk
                        request.session['project_id'] = project.pk

            if found_initial_project and not found_initial_project == project.pk:
                check_access = False
        else:
            print('dont have project error')
            projects = Project.objects.filter(users=request.user, active=True).order_by('-id').exclude(name='Master')
            if projects:
                project = projects[0]
                project_id = project.pk
                request.session['project_id'] = project.pk

        try:
            if not request.user.is_ds_admin:
                if check_access and project_id:
                    user_projects = request.user.project_users_list.filter(active=True, pk=project_id)
                    if not user_projects:
                        check_access = "False"
                        projects = request.user.project_users_list.filter(active=True)
                        project = projects[0]
                        project_id = project.pk
                        request.session['project_id'] = project.pk

            return {
                'project_id': project_id,
                'project': project,
                'context_project': project,
                'check_access': check_access
                }

        except Exception as e:
            pass

    return {}