from django import template
from projects.project_mixin import ProjectModelMixin
import os
from django.conf import settings
from django.apps import apps
from PIL import Image

# from wand.image import Image as Wimage
# from wand.display import display as Wdisplay

from django.db.models.fields.related import ForeignKey, ManyToOneRel
register = template.Library()
from django.forms.models import model_to_dict
from projects.models.model_mixin import PrimaryModelMixin


def user_projects_family(context):
    request = context['request']
    user = request.user

    from core.models import Project, ProjectFamily

    all_user_projects = Project.objects.filter(users=user, active=True).exclude(name='Master').order_by('name',
                                                                                                        '-version')
    if user.is_project_user:
        all_user_projects = all_user_projects.filter(status=Project.LIVE)

    project_versions = []
    project_family_names = []

    families = ProjectFamily.objects.filter(project_list=all_user_projects)

    for row in families:
        if row.family_name not in project_family_names:
            project_versions.append(row)
            project_family_names.append(row.family_name)

    if project_versions:
        return project_versions

register.assignment_tag(user_projects_family, takes_context=True)


@register.filter(name='user_allocated')
def user_allocated(request_project, user):
    from users.models import User

    request, project = request_project
    user = User.objects.filter(project_users_list=project, pk=user.pk)

    if user:
        return True
    return False


@register.filter(name='user_app_access')
def user_app_access(request_project, user):
    from users.models import User
    request, project = request_project
    user = User.objects.filter(project_users_list=project, pk=user.pk).distinct()

    if user and (settings.SYNC_CMS or settings.DEVELOPMENT_SUITE):
        user = user[0]
        return user.has_app_access(project)
    return False


@register.filter(name='show_last_slash')
def show_last_slash(image_path_name):
    split = image_path_name.split("/")
    return split[-1]


@register.filter(name='device_name')
def device_name(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    names_model = apps.get_model(project.project_connection_name, 'Names')
    name = names_model.objects.using(project.project_connection_name).filter(pk=obj.device_name_id)
    if name:
        return name[0].english


@register.filter(name='image_name')
def image_name(image_path):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """
    if image_path:
        image_name = image_path.split("/")
        return image_name[-1]


@register.filter(name='image_src')
def image_src(image_path):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
    static_image = static_image.replace("//", "/")
    return static_image


@register.filter(name='canvas_name')
def canvas_name(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    if obj:
        names_model = apps.get_model(project.project_connection_name, 'Names')
        name = names_model.objects.using(project.project_connection_name).filter(pk=obj.canvas_name_id)
        if name:
            return name[0].english

    return 'Enter new canvas name'


@register.filter(name='wireframe_name_from_canvas')
def wireframe_name_from_canvas(project, wireframe_id):

    wireframe_model = apps.get_model(project.project_connection_name, 'Wireframes')
    wireframe = wireframe_model.objects.using(project.project_connection_name).filter(pk=wireframe_id)

    if wireframe:
        wireframe = wireframe[0]

        names_model = apps.get_model(project.project_connection_name, 'Names')
        name = names_model.objects.using(project.project_connection_name).filter(pk=wireframe.display_name_id)
        if name:
            return name[0].english


@register.filter(name='wireframe_name_from_wireframe')
def wireframe_name_from_wireframe(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    names_model = apps.get_model(project.project_connection_name, 'Wireframes')
    wireframe = names_model.objects.using(project.project_connection_name).filter(pk=obj.pk)

    if wireframe:
        wireframe = wireframe[0]

        names_model = apps.get_model(project.project_connection_name, 'Names')
        name = names_model.objects.using(project.project_connection_name).filter(pk=wireframe.display_name_id)
        if name:
            return name[0].english


@register.filter(name='container_name')
def container_name(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    names_model = apps.get_model(project.project_connection_name, 'Names')
    name = names_model.objects.using(project.project_connection_name).filter(pk=obj.container_name_id)
    if name:
        return name[0].english


def get_apartments(context, building_id):
    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    property_list = []

    floor_buildings = model_mixin.model_object("FloorsBuildings").filter(building_id=building_id).values_list('floor_id', flat=True)

    floors = model_mixin.model_object("Floors").filter(pk__in=floor_buildings).values_list('pk', flat=True)
    properties = model_mixin.model_object("Properties").filter(floor_id__in=floors)

    for property in properties:
        name = model_mixin.model_object("Names").filter(pk=property.name_id)
        if name:
            name = name[0]
            property_list.append({'name': name.english, 'property': property})

    return property_list
register.assignment_tag(get_apartments, takes_context=True)


def get_container_list_menu(context, obj):

    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    list_items = model_mixin.model_object("ContainerSettings").filter(container_id=obj.pk)

    if list_items:
        item = list_items[0]
        return item.title

register.assignment_tag(get_container_list_menu, takes_context=True)


def get_container_list_item(context, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    list_items = model_mixin.model_object("ContainerSettings").filter(container_id=obj.pk)
    if list_items:
        settings_object = list_items[0]
        container_fields = model_mixin.model_object("ContainerFields").filter(container_settings_id=settings_object.pk)
        return container_fields

register.assignment_tag(get_container_list_item, takes_context=True)


def get_menu_width(context, obj):

    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    list_items = model_mixin.model_object("ContainerSettings").filter(container_id=obj.pk)

    if list_items:
        item = list_items[0]
        return item.width

register.assignment_tag(get_menu_width, takes_context=True)


def get_building_floors(context, building_id):

    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    floor_buildings = model_mixin.model_object("FloorsBuildings").filter(building_id=building_id).values_list('floor_id', flat=True)

    floorlist = []
    floors = model_mixin.model_object("Floors").filter(pk__in=floor_buildings).order_by("name__english")

    for floor in floors:
        properties = model_mixin.model_object("Properties").filter(floor_id=floor.pk)
        name = model_mixin.model_object("Names").filter(pk=floor.name_id)
        if name and properties:
            name = name[0]

            floorlist.append({'name': name.english, 'floor': floor})

    return floorlist

register.assignment_tag(get_building_floors, takes_context=True)


def get_all_floors(context):
    request = context['request']
    project_id = context['project'].pk
    model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

    floorlist = []
    floors = model_mixin.model_object("Floors").order_by("name__english")

    for floor in floors:
        name = model_mixin.model_object("Names").filter(pk=floor.name_id)
        if name:
            name = name[0]
            floorlist.append({'name': name.english, 'floor': floor})

    return floorlist

register.assignment_tag(get_all_floors, takes_context=True)


@register.filter(name='name_by_name_id')
def name_by_name_id(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    if obj and project:
        names_model = apps.get_model(project.project_connection_name, 'Names')
        name = names_model.objects.using(project.project_connection_name).filter(pk=obj.name_id)
        if name:
            return name[0].english


@register.filter(name='show_user_by_id')
def show_user_by_id(user_id):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """
    from users.models import User

    try:
        user = User.objects.get(pk=user_id)
        if user:
            return user.get_full_name()
    except Exception as e:
        return "n/a"


@register.filter(name='get_property_resource_image')
def get_property_resource_image(resource_object, resource_id):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    if resource_object:
        resource = resource_object.filter(pk=resource_id)
        if resource:
            resource = resource[0]
            if os.path.isfile(resource.path):
                static_image = '/static/' + str(resource.path.replace(settings.BASE_IMAGE_LOCATION, ''))
                static_image = static_image.replace("//", "/")
                return static_image


@register.filter(name='get_property_resource_name')
def get_property_resource_name(model_mixin, resource_id):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    resource = model_mixin.model_object("Resources").filter(pk=resource_id)
    if resource:
        resource = resource[0]
        if os.path.isfile(resource.path):
            name = image_name(resource.path)
            if len(name) >= 18:
                return str(name[:18]) +'...'
            return name


@register.filter(name='menu_list_item')
def menu_list_item(project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    wire_model = apps.get_model(project.project_connection_name, 'Wireframes')
    list_items = wire_model.objects.using(project.project_connection_name).filter(wireframe_parent=obj).order_by('orderidx')

    item_list = []
    for item in list_items:
        if not item:
            item = 1

        dict_item = model_to_dict(item)
        item_list.append(dict_item)

    return item_list


@register.filter(name='canvas_publish_list_item')
def canvas_publish_list_item(request_project, obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    request, project = request_project
    model_mixin = PrimaryModelMixin(request=request, project=project)

    list_items = model_mixin.model_object("Wireframes").filter(wireframe_parent=obj).order_by('orderidx')

    if "canvas_id" in request.session:
        canvas_id = request.session['canvas_id']

        item_list = []
        for item in list_items:
            if not item:
                item = 1

            dict_item = model_to_dict(item)

            template_id = dict_item['template']

            template_name = model_mixin.model_object("Templates").filter(pk=template_id).values_list('template_type__name', flat=True)[0]
            dict_item.update({'template_name': template_name})

            total_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                      wireframe_id=item,
                                                                                      template_id=template_id).count()

            dict_item.update({'asset_count': total_assets,
                              'wireframe_id': item.pk,
                              'template_id': template_id })

            item_list.append(dict_item)

        return item_list


@register.filter(name='one_more')
def one_more(object, item):
    return object, item


@register.filter(name='check_container_template')
def check_container_template(one_more, container):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    project, template_type = one_more

    if container and template_type:
        template_model = apps.get_model(project.project_connection_name, 'TemplateTypeContainers')
        list_items = template_model.objects.using(project.project_connection_name).filter(container_id=container.pk,
                                                                                          template_type_id=template_type.pk)
        if list_items:
            return True


@register.filter(name="file_exists")
def file_exists(image):
    if os.path.isfile(image):
        return True

@register.filter(name="clean_tags")
def clean_tags(tag_string):

    tag_string = str(tag_string).replace("/", "", 10).replace(" ", "", 10).replace(",", "", 10).replace("$", "", 10)\
        .replace("-", "", 10).replace("_", "", 10).replace(".", "_", 10)
    return tag_string
