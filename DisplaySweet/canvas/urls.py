from django.conf.urls import patterns, url

urlpatterns = patterns('',

    url(r'^$', 'canvas.views.home', name='canvas-home'),
    url(r'^(?P<pk>\d+)/$', 'canvas.views.project_select', name='canvas-project_select'),

    url(r'^devices/(?P<project_id>\d+)/$', 'canvas.views.devices', name='canvas-devices'),
    url(r'^edit/(?P<project_id>\d+)/$', 'canvas.views.canvasing', name='canvas-canvasing'),
    url(r'^edit/(?P<project_id>\d+)/canvas/(?P<pk>\d+)/$', 'canvas.views.canvasing', name='canvas-edit-canvasing'),
    url(r'^edit/(?P<project_id>\d+)/canvas/(?P<pk>\d+)/presenter/$', 'canvas.views.canvasing_presenter', name='canvas-edit-canvasing-presenter'),

    url(r'^folder/paths/(?P<project_id>\d+)/$', 'canvas.views.folder_paths', name='canvas-folder_paths'),
    url(r'^templates/(?P<project_id>\d+)/$', 'canvas.views.templating', name='canvas-templating'),

    url(r'^(?P<pk>\d+)/remove/(?P<model_name>\w+)/row/delete/(?P<row_id>\d+)/instant/$', 'canvas.views.delete_canvas_model_instant',
        name='canvas-model-delete-instant'),

    url(r'^edit/(?P<project_id>\d+)/templates/(?P<pk>\d+)/$', 'canvas.views.templating', name='canvas-edit-templating'),
    url(r'^edit/(?P<project_id>\d+)/templates/(?P<pk>\d+)/(?P<canvas_id>\d+)/$', 'canvas.views.templating', name='canvas-edit-templating'),
    url(r'^edit/(?P<project_id>\d+)/templates/(?P<pk>\d+)/(?P<canvas_id>\d+)/(?P<wire_frame_id>\d+)/$', 'canvas.views.templating',
        name='canvas-edit-templating-canvas'),

    url(r'^template/types/(?P<project_id>\d+)/(?P<pk>\d+)/(?P<canvas_id>\d+)/(?P<template_id>\d+)/$', 'canvas.views.template_types', name='canvas-template-types'),
    url(r'^template/types/(?P<project_id>\d+)/(?P<pk>\d+)/(?P<canvas_id>\d+)/$', 'canvas.views.template_types', name='canvas-template-types'),

    url(r'^(?P<project_id>\d+)/wireframes/$', 'canvas.views.wireframes', name='canvas-wireframes'),
    url(r'^(?P<project_id>\d+)/wireframes/edit/(?P<pk>\d+)/$', 'canvas.views.wireframes_canvas_edit', name='canvas-edit-wireframe'),

    url(r'^containers/(?P<project_id>\d+)/$', 'canvas.views.containers', name='canvas-containers'),
    url(r'^containers/(?P<project_id>\d+)/(?P<pk>\d+)/$', 'canvas.views.containers_edit', name='canvas-edit-containers'),

    url(r'^(?P<pk>\d+)/publish/canvas/(?P<canvas_id>\d+)/$', 'canvas.views.publish_canvas', name='canvas-publish'),
   url(r'^(?P<pk>\d+)/published/canvas/preview/(?P<canvas_id>\d+)/$', 'canvas.views.completed_canvas_publish', name='canvas-published-preview'),


    url(r'^property/names/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.views.save_new_floor_level_names', name='canvas-edit-property-names'),

)