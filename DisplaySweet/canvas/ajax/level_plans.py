from django.http import HttpResponse
import json
import os
from django.apps import apps

from django.conf import settings

from projects.models.model_mixin import PrimaryModelMixin
from django.forms.models import modelformset_factory

from core.models import ProjectCSVTemplate
from projects.forms import get_property_name_template_form

from django.template.loader import render_to_string
from projects.project_mixin import ProjectModelMixin


def sortkeypicker(keynames):
    negate = set()
    for i, k in enumerate(keynames):
        if k[:1] == '-':
            keynames[i] = k[1:]
            negate.add(k[1:])
    def getit(adict):
       composite = [adict[k] for k in keynames]
       for i, (k, v) in enumerate(zip(keynames, composite)):
           if k in negate:
               composite[i] = -v
       return composite
    return getit


def get_level_plans_screen(request, pk, building_id, floor_id=None):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        apartment_list = []
        apartments_dict = []
        linked_hotspots = []
        first_apartment_id = None
        floor_hotspots = []

        if not floor_id:
            floor_buildings = model_mixin.model_object("FloorsBuildings").filter(building_id=building_id).order_by(
                'floor__orderidx')

            if not floor_buildings:
                floor = model_mixin.model_object("Floors").all()[0]
                floor_id = floor.pk
            else:
                floor_id = floor_buildings[0].floor_id

        apartments = model_mixin.model_object("Properties").filter(floor_id=floor_id).order_by('orderidx')
        floor = model_mixin.model_object("Floors").get(pk=floor_id)

        if not floor_id:
            floor_id = floor.pk


        for property_item in apartments:
            hotspot_list = model_mixin.klass("Properties").get_hotspot_values(property_item.pk, floor.pk)
            property_type = model_mixin.klass("Properties").get_property_type(property_item.property_type_id)

            for hotspot in hotspot_list:
                hotspot_order = 1
                if hotspot['floor_hotspot_id'] and not hotspot['floor_hotspot_id'] in floor_hotspots:
                    floor_hotspots.append(hotspot['floor_hotspot_id'])

                if "hotspot_order" in hotspot:
                    hotspot_order = hotspot['hotspot_order']

                property_name = model_mixin.klass("Names").get_names_instance(property_item.name_id)
                apartment_kwargs = { 'id': property_item.pk,
                                     'floor_hotspot_id': hotspot['floor_hotspot_id'],
                                     'x_coordinate': hotspot['picking_coord_x'],
                                     'y_coordinate': hotspot['picking_coord_y'],
                                     'hotspot_order': hotspot_order,
                                     'hotspot_name': hotspot['name'],
                                     'name': property_name,
                                     'floor_id': int(floor.pk),
                                     'building_id': building_id,
                                     'property_type': property_type,
                                     'property_type_id': property_item.property_type_id,
                                     'orderidx': property_item.orderidx,
                                     'compare_name': property_name.lower(),
                                     'floor_plan_typical': model_mixin.klass("Properties").get_property_typicals(property_item.pk),
                                    }

                apartment_list.append(apartment_kwargs)
                apartments_dict.append(apartment_kwargs)

        hotspots_on_floor = model_mixin.model_object("FloorsHotspots").filter(floor_id=floor_id).order_by('name').exclude(pk__in=floor_hotspots)
        for hotspot in hotspots_on_floor:
            property_id = None
            property_floor_id = None
            property_name = None
            check_hotspot = model_mixin.model_object("PropertyHotspots").filter(floors_hotspot_id=hotspot.pk)
            if check_hotspot:
                property_id = check_hotspot[0].property_id
                property_floor_id = check_hotspot.values_list('property__floor_id', flat=True)
                property_floor_id = property_floor_id[0]
                property_name = check_hotspot.values_list('property__name__english', flat=True)
                property_name = property_name[0]

            apartment_kwargs = {'id': None,
                                'property_id': property_id,
                                'hotspot_id': hotspot.pk,
                                'x_coordinate': hotspot.x,
                                'y_coordinate': hotspot.y,
                                'hotspot_name': hotspot.name,
                                'floor_id': int(floor_id),
                                'property_floor_id': property_floor_id,
                                'property_name': property_name,
                                'building_id': building_id,
                                'compare_name': str(hotspot.name).strip().lower(),
                                'orderidx': 99
                                }

            apartment_list.append(apartment_kwargs)
            apartments_dict.append(apartment_kwargs)

        try:
            apartments_dict = sorted(apartments_dict, key=sortkeypicker(['orderidx', 'floor_id', 'compare_name']))
            apartment_list = json.dumps(apartments_dict)
        except Exception as e:
            print('level screen err', e)

        context = model_mixin.klass("Floors").get_select_floors_html(building_id, request=request)
        template = "canvas/templates/includes/floor_select_list.html"
        select_floors_rendered = render_to_string(template, context)

        template = 'canvas/templates/includes/apartments/apartment_content.html'

        context = {'apartments_dict': apartments_dict,
                   'project': model_mixin.project,
                   'building_id': building_id,
                   'linked_hotspots': linked_hotspots,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'request': request
                   }


        try:
            rendered = render_to_string(template, context)
        except Exception as e:
            print ('error rendering apartments', e)

        proprty_images = model_mixin.model_object("FloorsPlans").filter(key="level_plans",
                                                                        floor_id=floor_id).values_list("resource_id", flat=True)

        images = model_mixin.model_object("Resources").filter(pk=proprty_images)
        image_name = 'n/a'
        static_image = '/static/common/img/placeholder.jpg'

        if images:
            selected_image = images[0]
            image_path = selected_image.path
            image_name = selected_image.path.split("/")[-1]

            if image_path:
                if os.path.isfile(image_path):
                    image_name = image_path.split("/")[-1]
                    static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                    static_image = static_image.replace("//", "/")
                else:
                    static_image = '/static/common/img/placeholder.jpg'
                    image_name = ''

        apartment_list = json.dumps(apartment_list)

        data = json.dumps({'apartment_list': apartment_list,
                          'apartments_dict': apartments_dict,
                           'success': True,
                           'rendered': rendered,
                           'select_floors_rendered': select_floors_rendered,
                           'floor_id': floor_id,
                           'first_apartment_id': first_apartment_id,
                           'static_image': static_image,
                           'image_name': image_name})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_property_order(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']
        order = request.GET['order']

        model_mixin.model_object("Properties").filter(pk=property_id).update(orderidx=order)
        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_plan_html(request, pk):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        floor_id = request.GET['floor_id']
        plans = request.GET['plans']
        rendered = ''

        if plans == "floor_plans":
            context = model_mixin.klass("Floors").get_floor_plan_details_path(pk, floor_id)
            html_template = "canvas/templates/includes/apartments/floor_plan_apartments.html"
            rendered = render_to_string(html_template, context)

        if plans == "level_plans":
            context = model_mixin.klass("LevelPlans").get_level_plan_details(floor_id)
            html_template = "canvas/templates/includes/level_plan_tabbed.html"
            rendered = render_to_string(html_template, context)

        data = json.dumps({
            'success': True,
            'rendered': rendered
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_level_plan_image(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        floor_id = request.GET['floor_id']
        resource_id = request.GET['resource_id']
        sub_wireframe_id = request.GET['sub_wireframe_id']

        selected_image = model_mixin.model_object("Resources").filter(pk=resource_id)[0]
        proprty_images = model_mixin.model_object("FloorsPlans").filter(key="level_plans",
                                                                        floor_id=floor_id).values_list("resource_id", flat=True)

        if proprty_images:
            images = model_mixin.model_object("Resources").filter(pk__in=proprty_images)

            if images:
                proprty_images = model_mixin.model_object("FloorsPlans").filter(key="level_plans",
                                                                                          floor_id=floor_id)
                proprty_images = proprty_images[0]
                proprty_images.resource_id = selected_image.pk
                proprty_images.save(using=model_mixin.project.project_connection_name)
        else:
            model_mixin.model_object("FloorsPlans").create(key="level_plans",
                                                                     floor_id=floor_id,
                                                                     resource_id=resource_id)

        canvas_id = request.GET['canvas_id']
        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Level Plans",
                                                                           resource_id,
                                                                           canvas_id,
                                                                           None)
        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_new_property_names(request, pk, floor_id, building_id):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        new_floor = model_mixin.model_object("Floors").filter(pk=floor_id)[0]
        properties = model_mixin.model_object("Properties").filter(floor_id=new_floor.pk).values_list('name_id', flat=True)
        number_of_apartments = len(properties)

        new_level_name = model_mixin.model_object("Names").get(pk=new_floor.name_id)

        if request.method == "POST":            
            properties = model_mixin.model_object("Properties").filter(floor_id=new_floor.pk).values_list('name_id', flat=True)
            names_model_object = apps.get_model(model_mixin.project.project_connection_name, 'Names')
            property_names = model_mixin.model_object("Names").filter(pk__in=properties)

            form = get_property_name_template_form(model_mixin.project, model_mixin, names_model_object)
            PropertyNameFormSet = modelformset_factory(names_model_object, form=form, can_delete=True, extra=0)

            devices_formset = PropertyNameFormSet(request.POST, queryset=property_names)
            for form in devices_formset:
                if form.is_valid():
                    form.save()

        context = model_mixin.klass("Floors").get_select_floors_html(building_id, new_floor.pk, request=request)
        template = "canvas/templates/includes/floor_select_list.html"
        select_floor_rendered = render_to_string(template, context)


        data = json.dumps({
            'success': True,
            'select_floor_rendered': select_floor_rendered
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_property(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']

        try:
            resources = model_mixin.model_object("PropertyResources").filter(property_id=property_id)
            resources.update(resource_id=None)
            resources.delete()

            model_mixin.model_object("PropertyHotspots").filter(property_id=property_id).delete()
            model_mixin.model_object("PropertyAllocations").filter(property_id=property_id).delete()
            model_mixin.model_object("Properties").filter(pk=property_id).delete()
        except Exception as e:
            print('delete error ', e)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_level(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        floor_id = request.GET['floor_id']
        model_mixin.model_object("Properties").filter(floor_id=floor_id).delete()
        model_mixin.model_object("Floors").filter(pk=floor_id).delete()

        floors = model_mixin.model_object("Floors").all()

        data = json.dumps(
            {
                'success': True,
                'floor_id': floors[0].pk
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    
    
def get_details_screen(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        combined_parent_string = model_mixin.model_object("Strings").filter(english=settings.COMBINED_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(english=settings.COMBINED_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]

        upgrade_view_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
        if not upgrade_view_string:
            model_mixin.model_object("Strings").create(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
            upgrade_view_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_PARENT_PROPERTY_KEY)

        upgrade_view_string = upgrade_view_string[0]

        apartment_list = []
        apartments_dict = []

        floor = model_mixin.model_object("Floors").get(pk=floor_id)
        floor_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id)
        building_floors = model_mixin.model_object("FloorsBuildings").filter(building_id=floor_buildings[0].building_id).values_list('floor_id',
                                                                                                                                     flat=True).distinct()

        all_floors = model_mixin.model_object("Floors").filter(pk__in=building_floors).order_by('orderidx')
        floor_buildings = floor_buildings.values_list('building_id', flat=True).distinct()

        for building_id in floor_buildings:
            apartments = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')
            floor = model_mixin.model_object("Floors").get(pk=floor.pk)
            if not floor_id:
                floor_id = floor.pk

            for apartment in apartments:
                hotspot_list = model_mixin.klass("Properties").get_hotspot_values(apartment.pk, floor.pk)
                property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)
                property_name = model_mixin.klass("Names").get_names_instance(apartment.name_id)

                has_combination = False
                if model_mixin.model_object("PropertiesIntFields").filter(type_string=combined_parent_string, value=apartment.pk):
                    has_combination = True

                has_upgrade = False
                if model_mixin.model_object("PropertiesIntFields").filter(value=apartment.pk, type_string=upgrade_view_string):
                    has_upgrade = True

                for hotspot in hotspot_list:
                    apartment_kwargs = { 'id': apartment.pk,
                                         'floor_hotspot_id': hotspot['floor_hotspot_id'],
                                         'x_coordinate': hotspot['picking_coord_x'],
                                         'y_coordinate': hotspot['picking_coord_y'],
                                         'name': property_name,
                                         'hotspot_name': hotspot['name'],
                                         'floor_id': int(floor.pk),
                                         'building_id': building_id,
                                         'property_type': property_type,
                                         'property_type_id': apartment.property_type_id,
                                         'combination': has_combination,
                                         'upgrade': has_upgrade
                                        }

                    apartment_list.append(apartment_kwargs)
                    apartments_dict.append(apartment_kwargs)

        template = 'canvas/templates/includes/apartments/basic_apartments.html'
        context = {'apartments_dict': apartments_dict,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'project': model_mixin.project}

        editable_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project,
                                                               is_editable_template=True).order_by('-is_editable_template_default', 'id')

        if not editable_templates:
            editable_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project).order_by(
                '-is_editable_template_default', 'id')

        basic_rendered = render_to_string(template, context)

        floor_list = []
        floors_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id).values_list('building_id', flat=True)
        floors_buildings = model_mixin.model_object("Buildings").filter(pk__in=floors_buildings)

        floor_list.append(
            {'floor': floor,
             'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
             'level_plan_image': model_mixin.klass("Floors").get_floor_level_image(floor.pk),
             'assigned_buildings': floors_buildings,
            }
        )

        builting_floor_list = []
        for floor in all_floors:
            builting_floor_list.append({'floor': floor,
                                        'floor_id': floor.pk,
                                        'name': model_mixin.klass("Names").get_names_instance(floor.name_id)
                                        })

        template = 'canvas/templates/includes/assigned_buildings.html'
        context = {'apartments_dict': apartments_dict,
                   'floor_list': floor_list,
                   'project': model_mixin.project,
                   'all_buildings': model_mixin.klass("Buildings").get_all_buildings(),
                   'request': request}
        buildings_rendered = render_to_string(template, context)

        listing_floor_template = 'canvas/templates/includes/listing_floor_selection.html'
        context = {'builting_floor_list': builting_floor_list, 'request': request}
        listing_floor_rendered = render_to_string(listing_floor_template, context)

        apartment_list = json.dumps(apartment_list)

        data = json.dumps({'apartment_list': apartment_list,
                           'apartments_dict': apartments_dict,
                           'success': True,
                           'basic_rendered': basic_rendered,
                           'buildings_rendered': buildings_rendered,
                           'listing_floor_rendered': listing_floor_rendered})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)




def get_level_plan_details(request, pk, floor_id):
    if request.is_ajax():
        static_image = '/static/common/img/placeholder.jpg'
        image_path = '<em>n/a</em>'
        image_exists = False
        image_name = ''
        resource_id = None
        property_details = {}

        rendered = None

        model_mixin = ProjectModelMixin()
        model_mixin.get_project(pk)

        if "apartment" in request.GET and request.GET['apartment']:
            image_type = request.GET['image_type']
            property_item = model_mixin.model_object("Properties").filter(pk=request.GET['apartment'])[0]

            proprty_images = model_mixin.model_object("PropertyResources").filter(key=image_type,
                                                                                  property_id=property_item.pk).values_list(
                "resource_id", flat=True)
            images = model_mixin.model_object("Resources").filter(pk=proprty_images)

            if images:
                plan = images[0]
                image_path = plan.path
                resource_id = plan.pk
                image_exists = True
                image_name = plan.path.split("/")[-1]

                if image_path:
                    if not os.path.isfile(image_path):
                        static_image = '/static/common/img/placeholder.jpg'
                        image_path = '<em>This floor has no image selected (placeholder displayed).</em>'
                        image_name = ''
                    else:
                        image_name = image_path.split("/")[-1]
                        static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                        static_image = static_image.replace("//", "/")
                        image_path = str(image_path)

            context = {
                'success': True,
                'static_image': str(static_image),
                'image_path': str(image_path),
                'image_exists': image_exists,
                'image_name': image_name,
                'property_details': property_details,
                'rendered': rendered,
                'resource_id': resource_id
            }

            data = json.dumps(context)
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)