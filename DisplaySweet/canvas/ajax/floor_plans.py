from django.http import HttpResponse
import json
import os
from django.conf import settings
from django.forms.models import model_to_dict

from django.apps import apps

from projects.models.model_mixin import PrimaryModelMixin
from django.forms.models import  modelformset_factory
from projects.forms import get_property_name_template_form

from django.template.loader import render_to_string


def get_floor_plans_screen(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        apartment_list = []
        apartments_dict = []

        selected_property = request.GET['selected_property']
        model_mixin.klass("Properties").check_and_update_typicals()

        building_id = None
        floor = model_mixin.model_object("Floors").filter(pk=floor_id)[0]
        floor_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id).values_list('building_id',
                                                                                                            flat=True).distinct()
        for building_id in floor_buildings:
            apartments = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')
            floor = model_mixin.model_object("Floors").get(pk=floor.pk)

            if not floor_id:
                floor_id = floor.pk

            for apartment in apartments:
                hotspot_list = model_mixin.klass("Properties").get_hotspot_values(apartment.pk, floor.pk)
                property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)
                property_name = model_mixin.klass("Names").get_names_instance(apartment.name_id)
                floor_plan_image_name = model_mixin.klass("Properties").get_image_type_canvas('floor_plans', apartment.pk)
                floor_plan_typical = model_mixin.klass("Properties").get_property_typicals(apartment.pk)

                for hotspot in hotspot_list:
                    apartment_kwargs = {'id': apartment.pk,
                                        'floor_hotspot_id': hotspot['floor_hotspot_id'],
                                        'x_coordinate': hotspot['picking_coord_x'],
                                        'y_coordinate': hotspot['picking_coord_y'],
                                        'name': property_name,
                                        'floor_id': int(floor.pk),
                                        'building_id': building_id,
                                        'property_type': property_type,
                                        'property_type_id': apartment.property_type_id,
                                        'selected_property': selected_property,
                                        'floor_plan_image_name': floor_plan_image_name,
                                        'floor_plan_typical': floor_plan_typical }

                    apartment_list.append(apartment_kwargs)
                    apartments_dict.append(apartment_kwargs)

        template = 'canvas/templates/includes/apartments/floor_plan_apartments.html'

        try:
            typicals_list = model_mixin.klass("Properties").get_all_property_typicals()
        except Exception as e:
            print(e)
            typicals_list = []

        context = {'apartments_dict': apartments_dict,
                   'project': model_mixin.project,
                   'property_typical_image_list': typicals_list,
                   'building_id': building_id,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'request': request}

        try:
            floor_rendered = render_to_string(template, context)
        except Exception as e:
            print('render err:', e)
            floor_rendered = ''

        data = json.dumps({'success': True,
                           'floor_rendered': floor_rendered})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_floor_apartments(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        properties = model_mixin.model_object("Properties").filter(floor_id=floor_id).order_by('orderidx').order_by(
            'name__english')
        apartments_dict = []
        first_aspect = 1

        first_apartment = None
        for apartment in properties:
            if not first_apartment:
                first_apartment = apartment.pk

            floor_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id)
            for building in floor_buildings:
                building_id = building.building_id
                picking_coord_x, picking_coord_y = model_mixin.klass("Properties").get_apartment_hotspot_values(apartment.pk,
                                                                                                                floor_id=floor_id)

                apartment_kwargs = {'id': apartment.pk,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                    'floor_id': int(floor_id),
                                    'aspects': model_mixin.klass("Properties").get_property_aspects(apartment.pk),
                                    'building_id': building_id,
                                    'key_plan': model_mixin.klass("Properties").get_property_static_image(apartment.pk, 'key_plans'),
                                    'property_type': model_mixin.klass("Properties").get_property_type(apartment.property_type_id)
                                    }

                apartments_dict.append(apartment_kwargs)

        try:
            template = 'canvas/templates/includes/apartments/views_apartment_content.html'
            context = {'apartments_dict': apartments_dict,
                       'project': model_mixin.project,
                       'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                       'request': request}

            rendered = render_to_string(template, context)
        except Exception as e:
            print (e)



        try:
            template = 'canvas/templates/includes/apartments/basic_apartments.html'
            context = {'apartments_dict': apartments_dict,
                       'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                       'project': model_mixin.project,
                       'request': request}

            basic_rendered = render_to_string(template, context)
        except Exception as e:
            print(e)

        aspect_dict = []

        file_aspects = model_mixin.model_object("FloorsPlans").filter(floor_id=floor_id)
        template = 'canvas/templates/includes/aspects_floor_table.html'

        context = {'file_aspects': file_aspects,
                   'aspect_dict': aspect_dict,
                   'project': model_mixin.project,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all()}
        file_rendered = render_to_string(template, context)

        template = 'canvas/templates/includes/views_image_display.html'

        context = {'file_aspects': file_aspects,
                   'aspect_dict': aspect_dict,
                   'project': model_mixin.project,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all()}

        image_file_rendered = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'rendered': rendered,
                           'aspects_table': file_rendered,
                           'image_file_rendered': image_file_rendered,
                           'first_apartment': first_apartment,
                           'first_aspect': first_aspect,
                           'basic_rendered': basic_rendered
                           })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)

def get_floor_apartments_html(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        floor_id = request.GET['floor_id']
        selected_id = request.GET['selected_id']

        floor_apartments = []

        properties = model_mixin.model_object("Properties").filter(floor_id=floor_id).order_by('orderidx')
        for apartment in properties:
            name = model_mixin.model_object("Names").filter(pk=apartment.name_id)
            if name:
                name = name[0]
                floor_apartments.append({'name': name.english, 'property': apartment})

        template = "canvas/templates/includes/select_apartment.html"
        context = {'floor_apartments': floor_apartments, 'selected_id': selected_id}
        rendered_html = render_to_string(template, context)

        data = json.dumps({'rendered': rendered_html,
                           'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def copy_key_plan_to_floor(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        floor_id = request.GET['floor_id']
        key_plan = request.GET['key_plan']

        properties = model_mixin.model_object("Properties").filter(floor_id=floor_id).values_list('pk', flat=True)

        model_mixin.model_object("PropertyResources").filter(key="key_plans",
                                                             property_id__in=properties).update(resource_id=key_plan)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_floor_order(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        floor_id = request.GET['floor_id']
        order = request.GET['order']

        property = model_mixin.model_object("Floors").filter(pk=floor_id)[0]
        property.orderidx = order
        property.save(using=model_mixin.project.project_connection_name)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_image_selection_details(request, pk, floor_id, image_type=None):

    if request.is_ajax():

        if not image_type:
            if "image_type" in request.GET:
                image_type = request.GET['image_type']

        image_type = image_type.replace(" ", "_").replace(" ", "_").lower().lstrip().rstrip()

        static_image = '/static/common/img/placeholder.jpg'
        image_path = '<em>n/a</em>'
        image_exists = False
        image_name = 'n/a'
        property_details = {}
        resource_id = None
        rendered = None

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        property_id = request.GET['apartment']

        if property_id:
            property_item = model_mixin.model_object("Properties").get(pk=property_id)
            property_fields = []
            property_string_fields = model_mixin.model_object("PropertiesStringFields").filter(property=property_item)

            for field in property_string_fields:
                string_field = model_mixin.model_object("Strings").filter(pk=field.type_string_id)
                if string_field:
                    english = str(string_field[0].english).replace("_", " ").capitalize()
                    value = field.value

                    property_fields.append({english:value})

            property_int_fields = model_mixin.model_object("PropertiesIntFields").filter(property=property_item)

            for field in property_int_fields:
                string_field = model_mixin.model_object("Strings").filter(pk=field.type_string_id)
                if string_field:
                    english = str(string_field[0].english).replace("_", " ").capitalize()
                    value = field.value

                    property_fields.append({english:value})

            property_name = model_mixin.model_object("Names").filter(pk=property_item.name_id)[0]
            context = {'name': property_name.english,
                       'property_type': model_mixin.klass("Properties").get_property_type(property_item.property_type_id),
                       'string_fields': property_fields,
                       'request': request}

            html_template = "canvas/templates/includes/details_listing.html"
            rendered = render_to_string(html_template, context)

        if image_type:
            images = None

            if image_type in ["key_plans", "floor_plans"]:
                proprty_images = model_mixin.model_object("PropertyResources").filter(key=image_type,
                                                                                      property_id=property_id)

                if proprty_images:
                    image_dict = model_to_dict(proprty_images[0])
                    images = model_mixin.model_object("Resources").filter(pk=image_dict['resource'])

            else:
                proprty_images = model_mixin.model_object("FloorsPlans").filter(key=image_type,
                                                                                floor_id=floor_id).values_list("resource_id", flat=True)

                images = model_mixin.model_object("Resources").filter(pk=proprty_images)

            if images:
                selected_image = images[0]
                image_path = selected_image.path
                image_exists = True
                image_name = selected_image.path.split("/")[-1]
                resource_id = selected_image.pk

                if image_path:
                    if not os.path.isfile(image_path):
                        static_image = '/static/common/img/placeholder.jpg'
                        image_path = '<em>This floor has no image selected (placeholder displayed).</em>'
                        image_name = ''
                    else:
                        image_name = image_path.split("/")[-1]
                        static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                        static_image = static_image.replace("//", "/")
                        image_path = str(image_path)

            context = {
                'success': True,
                'static_image': str(static_image),
                'image_path': str(image_path),
                'image_exists': image_exists,
                'image_name': image_name,
                'property_details': property_details,
                'rendered': rendered,
                'resource_id': resource_id
            }

        else:

            context = {
                'success': True,
                'static_image': str(static_image),
                'image_name': 'n/a',
                'image_path': str(image_path),
                'image_exists': False,
                'property_details': property_details,
                'rendered': rendered,
                'resource_id': resource_id
            }

        data = json.dumps(context)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    
    
def add_new_property_typical_string(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        set_string = request.GET['typical_string']
        new_typical = model_mixin.klass("Properties").add_new_property_typical_string(set_string)

        data = json.dumps({
            'success': True,
            'new_typical_id': new_typical.pk,
            'set_string': set_string
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def assign_typical_to_floor(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        type_string = request.GET['type_string']
        floor_id = request.GET['floor_id']

        properties = model_mixin.model_object("Properties").filter(floor_id=floor_id)

        for apartment in properties:
            model_mixin.klass("Properties").add_typical_string_to_property(apartment.pk, type_string)

        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def assign_typical_to_building(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        type_string = request.GET['type_string']
        building_id = request.GET['building_id']

        floor_ids = model_mixin.model_object("FloorsBuildings").filter(building_id=building_id).values_list(
                                                                                                'floor_id', flat=True)

        properties = model_mixin.model_object("Properties").filter(floor_id__in=floor_ids)

        for apartment in properties:
            model_mixin.klass("Properties").add_typical_string_to_property(apartment.pk, type_string)

        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_image_string_typical(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        type_string = request.GET['type_string']
        model_mixin.klass("Properties").remove_new_property_typical_string(type_string)

        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    

def add_typical_string_to_property(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']
        set_string = request.GET['typical_string']

        model_mixin.klass("Properties").add_typical_string_to_property(property_id, set_string)
        property_typical_image_list = model_mixin.klass("Properties").get_all_property_typicals()

        resource_id = None
        image_name = ''
        static_image = '/static/common/img/placeholder.jpg'

        for image in property_typical_image_list:
            if image['type'] == str(set_string):
                resource_id = image['image_id']
                image_name = image['image']
                static_image = image['static_image']

        if resource_id:
            proprty_images = model_mixin.model_object("PropertyResources").filter(key="floor_plans",
                                                                                  property_id=property_id)
            if proprty_images:
                proprty_images = proprty_images[0]
                proprty_images.resource_id = resource_id
                proprty_images.save(using=model_mixin.project.project_connection_name)
            else:
                model_mixin.model_object("PropertyResources").create(key="floor_plans",
                                                                     property_id=property_id,
                                                                     resource_id=resource_id)


            # model_mixin.klass("Assets").save_resource_to_asset_plans_container("Floor Plans",
            #                                                                   resource_id,
            #                                                                   request.GET['canvas_id'],
            #                                                                   None)
        else:
            model_mixin.model_object("PropertyResources").filter(key="floor_plans",
                                                                 property_id=property_id).delete()

        data = json.dumps({
            'success': True,
            'image_name': image_name,
            'static_image': static_image
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_new_floor_level(request, pk, building_id):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        print(request.GET)

        number_of_apartments = request.GET['number_of_apartments']
        new_level_name = request.GET['new_level_name']

        model_mixin.model_object("Names").create(english=new_level_name)
        new_level = model_mixin.model_object("Names").filter(english=new_level_name)
        if not new_level:
            model_mixin.model_object("Names").create(english=new_level_name)

        new_item_name = model_mixin.model_object("Names").filter(english=new_level_name)[0]
        model_mixin.model_object("Floors").create(name_id=new_item_name.pk)
        new_floor = model_mixin.model_object("Floors").filter(name_id=new_item_name.pk)[0]

        model_mixin.model_object("FloorsBuildings").create(floor_id=new_floor.pk, building_id=building_id)
        new_property_and_names = []
        new_id = 1
        if number_of_apartments and int(number_of_apartments) > 0:
            for i in range(int(number_of_apartments)):
                if new_id < 10:
                    property_name = '%s%s' % (new_level_name, '0%s' % new_id)
                else:
                    property_name = '%s%s' % (new_level_name, new_id)

                model_mixin.model_object("Names").create(english=property_name)
                new_property_name = model_mixin.model_object("Names").filter(english=property_name).order_by('-pk')[0]
                model_mixin.model_object("Properties").create(name_id=new_property_name.pk,
                                                              property_type_id=1,
                                                              floor_id=new_floor.pk)

                new_property = model_mixin.model_object("Properties").filter(name_id=new_property_name.pk,
                                                                               floor_id=new_floor.pk,
                                                                               property_type_id=1).order_by('-pk')[0]

                new_property_and_names.append({new_property.pk: new_property_name.pk})

                new_id += 1


        new_floor = model_mixin.model_object("Floors").filter(pk=new_floor.pk)[0]
        new_level_name = model_mixin.model_object("Names").filter(pk=new_floor.name_id)[0]
        new_level_name = new_level_name.english

        properties = model_mixin.model_object("Properties").filter(floor_id=new_floor.pk).values_list('name_id',
                                                                                                        flat=True)
        number_of_apartments = len(properties)

        if number_of_apartments:
            names_model_object = apps.get_model(model_mixin.project.project_connection_name, 'Names')
            property_names = model_mixin.model_object("Names").filter(pk__in=properties)

            form = get_property_name_template_form(model_mixin.project, model_mixin, names_model_object)
            PropertyNameFormSet = modelformset_factory(names_model_object, form=form, can_delete=True, extra=0)
            names_formset = PropertyNameFormSet(queryset=property_names)

            context = {
                'project': model_mixin.project,
                'formset_facory_form': form,
                'names_formset': names_formset,
                'floor_id': new_floor.pk,
                'new_level_name': new_level_name,
                'number_of_apartments': number_of_apartments
            }

            html_template = "canvas/templates/includes/editable_property_name.html"
            rendered = render_to_string(html_template, context)
            select_floor_rendered = None

        else:
            rendered = None

            context = model_mixin.klass("Floors").get_select_floors_html(building_id, new_floor.pk, request=request)
            template = "canvas/templates/includes/floor_select_list.html"
            select_floor_rendered = render_to_string(template, context)

        data = json.dumps({
            'success': True,
            'name_form_rendered': rendered,
            'select_floor_rendered': select_floor_rendered,
            'new_floor_id': new_floor.pk
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_type_image(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        resource_id = request.GET['resource_id']
        typical_string = request.GET['typical_string']

        typical_string = typical_string.lstrip().rstrip().replace("\n", "", 10)
        property_asset = model_mixin.klass("Properties").update_with_property_typicals(typical_string, resource_id)

        print('property_asset', property_asset)

        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Floor Plans",
                                                                           resource_id,
                                                                           request.GET['canvas_id'],
                                                                           None)
        data = json.dumps({
            'success': property_asset
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_floor_plan_image(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']
        resource_id = request.GET['resource_id']
        canvas_id = request.GET['canvas_id']

        model_mixin.model_object("PropertyResources").update_or_create(key="floor_plans",
                                                                       property_id=property_id,
                                                                       defaults={'resource_id': resource_id})

        key_string = model_mixin.model_object("Strings").filter(english='floor_plans_image_id')
        if not key_string:
            model_mixin.model_object("Strings").create(english='floor_plans_image_id')
            key_string = model_mixin.model_object("Strings").filter(english='floor_plans_image_id')

        key_string = key_string[0]

        asset = model_mixin.model_object("Assets").filter(source_id=resource_id)
        if asset:
            model_mixin.model_object("PropertiesIntFields").update_or_create(property_id=property_id,
                                                                             type_string=key_string,
                                                                             defaults={'value': asset[0].pk})

        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Floor Plans",
                                                                           resource_id,
                                                                           canvas_id,
                                                                           None)

        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_floor_plan_to_property(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if 'property_id' in request.GET:
            property_id = request.GET['property_id']
            model_mixin.model_object("PropertyResources").filter(property_id=property_id, key="floor_plans").delete()

            data = json.dumps({'success': True})
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def remove_level_plan_to_floor(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if 'floor_id' in request.GET:
            floor_id = request.GET['floor_id']
            model_mixin.model_object("FloorsPlans").filter(floor_id=floor_id, key="level_plans").delete()

            data = json.dumps({'success': True})
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def save_image_to_floor(request, pk, floor_id, image_type):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if 'selected_image_id' in request.GET:

            resource_id = request.GET['selected_image_id']
            image = model_mixin.model_object("Resources").get(pk=resource_id)
            image_name = image.path.split("/")[-1]

            update_image_names = model_mixin.klass("Properties").check_if_image_name_in_property_list(image_name, resource_id)
            if update_image_names:
                context = {'resource_id': resource_id, 'name': image_name}
                html_template = "canvas/templates/includes/image_list_options.html"
                rendered = render_to_string(html_template, context)
            else:
                property_typical_image_list = model_mixin.klass("Properties").get_all_property_typicals()
                context = {'property_typical_image_list': property_typical_image_list}
                html_template = "canvas/templates/includes/image_list_updated_options.html"
                rendered = render_to_string(html_template, context)

            if image_type == "level_plans":
                proprty_images = model_mixin.model_object("FloorsPlans").filter(floor_id=floor_id,
                                                                                key=image_type)

                if not proprty_images:
                    model_mixin.model_object("FloorsPlans").create(resource_id=resource_id,
                                                                   floor_id=floor_id,
                                                                   key=image_type)
                else:
                    proprty_images = proprty_images[0]
                    proprty_images.key = image_type
                    proprty_images.resource_id = resource_id
                    proprty_images.save(using=model_mixin.project.project_connection_name)
            else:
                proprty_images = model_mixin.model_object("PropertyResources").filter(key=image_type,
                                                                                      property_id=request.GET['apartment'])

                if not proprty_images:
                    model_mixin.model_object("PropertyResources").create(resource_id=resource_id,
                                                                         property_id=request.GET['apartment'],
                                                                         key=image_type)
                else:
                    proprty_images = proprty_images[0]
                    proprty_images.key = image_type
                    proprty_images.resource_id = resource_id
                    proprty_images.save(using=model_mixin.project.project_connection_name)

            image = model_mixin.model_object("Resources").filter(pk=resource_id)[0]
            image_path = image.path

            image_name = image_path.split("/")[-1]
            image_key = '%s_image_name' % image_type
            image_key = image_key.replace("_plans", "")

            static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
            static_image = static_image.replace("//", "/")

            context = {
                'success': True,
                'static_image': str(static_image),
                'image_path': str(image_path),
                'image_exists': True,
                'image_name': image_name,
                'image_key': image_key,
                'resource_id': image.pk,
                'update_image_names': update_image_names,
                'rendered': rendered
            }

            data = json.dumps(context)
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)
        
        
def get_floor_resources(request, pk):
    
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        aspects = []
        aspect_dict = []

        ignore_keys = ['level_plans', 'key_plans', 'floor_plans']

        first_aspect = ''
        if 'floor_id' in request.GET:
            floor_id = request.GET['floor_id']

            file_aspects = model_mixin.model_object("FloorsPlans").filter(floor_id=floor_id)
            print('file_aspects', file_aspects)
            for aspect in file_aspects:
                if aspect.resource_id and aspect.key not in ignore_keys:
                    if not first_aspect:
                        first_aspect = aspect.pk

                    aspect_file = None
                    key_file = model_mixin.model_object("Resources").filter(pk=aspect.resource_id)
                    if key_file:
                        aspect_file = key_file[0]

                    aspect_dict.append({'key': aspect.key,
                                        'file': aspect_file,
                                        'pk': aspect.pk,
                                        'floor_id': floor_id
                                        })

            template = 'canvas/templates/includes/aspects_floor_table.html'
            context = {'aspect_dict': aspect_dict,
                       'project': model_mixin.project,
                       'request': request}

            file_rendered = render_to_string(template, context)

            template = 'canvas/templates/includes/aspects_table.html'
            context = {'aspects': aspects,
                       'aspect_dict': aspect_dict,
                       'project': model_mixin.project,
                       'request': request}

            rendered = render_to_string(template, context)

            template = 'canvas/templates/includes/views_image_display.html'
            context = {'aspect_dict': aspect_dict,
                       'project': model_mixin.project,
                       'request': request}
            image_file_rendered = render_to_string(template, context)

            data = json.dumps({'success': True,
                               'rendered': rendered,
                               'file_rendered': file_rendered,
                               'image_file_rendered': image_file_rendered,
                               'first_aspect': first_aspect})

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)