from django.http import HttpResponse
import json


from django.template.loader import render_to_string

from projects.models.model_mixin import PrimaryModelMixin
from core.models import Project, ProjectCSVTemplateFields
from projects.project_mixin import ProjectModelMixin
from django.db.models.functions import Lower


def select_all_typicals_for_render(request, pk):
    print('select_all_typicals_for_render')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        field_value = request.GET['field_value']

        floor_list = []
        if "floor_list[]" in request.GET and request.GET.getlist('floor_list[]'):
            floor_list = request.GET.getlist('floor_list[]')

        selected_render_field = model_mixin.klass("Strings").get_string_from_category('property_render_field')
        model_mixin.klass("Strings").check_and_updated_renderd_field(selected_render_field, field_value)

        property_render_values = model_mixin.klass("PropertyRenders").get_property_render_values(field_value, floor_list)

        file_list = model_mixin.klass("PropertyRenders").get_property_render_resources(field_value, floor_list=floor_list)

        template = 'canvas/templates/includes/renders/property_rendered_fields.html'

        context = {'property_render_values': property_render_values,
                   'field_value': field_value,
                   'file_list': file_list}

        try:
            view_rendered = render_to_string(template, context)
        except Exception as e:
            print('property_rendered_fields error', e)
            view_rendered = ''

        try:
            template = 'canvas/templates/includes/renders/renders_image_display.html'
            context = {'aspect_dict': file_list,
                       'file_list': file_list,
                       'project': model_mixin.project,
                       'request': request}

            rendered = render_to_string(template, context)

        except Exception as e:
            print('renders_image_display error', e)
            rendered = ''

        data = json.dumps({
            'success': True,
            'view_rendered': view_rendered,
            'rendered': rendered,
            'selected_field_value':field_value
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_render_type(request, pk):
    print('update_property_render_type')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        field_type_value = request.GET['field_type_value']
        property_id = request.GET['property_id']

        found_renders = model_mixin.model_object("PropertyRenders").filter(property_id=property_id)
        found_renders.delete()

        if field_type_value == "Individual":
            model_mixin.model_object("PropertyRenders").create(property_id=property_id,
                                                               key="Individual",
                                                               resource_id=0,
                                                               orderidx=1)

        found_renders = model_mixin.model_object("PropertyRenders").filter(property_id=property_id)

        data = json.dumps({
            'success': True
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_rendered_typical_image(request, pk):
    print('update_property_rendered_typical_image')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        resource_id = 0
        if "resource_id" in request.GET and request.GET['resource_id']:
            resource_id = request.GET['resource_id']

        file_order = 1
        if "file_order" in request.GET and request.GET['file_order']:
            file_order = request.GET['file_order']

        floor_list = []
        if "floor_list[]" in request.GET and request.GET.getlist('floor_list[]'):
            floor_list = request.GET.getlist('floor_list[]')

        key_value = request.GET['key_value']
        field_value = request.GET['field_value']
        cleaned_key_value = model_mixin.klass("Strings").clean_for_mapping(key_value)

        try:
            properties_with_key_and_value = model_mixin.klass("Properties").get_all_properties_with_render_key_and_value(key_value, cleaned_key_value, field_value, floor_list)
        except Exception as e:
            print('error', e)
            properties_with_key_and_value = []

        for apartment_id in properties_with_key_and_value:
            check_key_resource = model_mixin.model_object("PropertyRenders").filter(key=field_value,
                                                                                    resource_id=resource_id,
                                                                                    property_id=apartment_id)
            if not check_key_resource:
                model_mixin.model_object("PropertyRenders").create(key=field_value,
                                                                   resource_id=resource_id,
                                                                   property_id=apartment_id,
                                                                   orderidx=file_order)

        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Renders",
                                                                           resource_id,
                                                                           request.GET['canvas_id'],
                                                                           None)

        if len(properties_with_key_and_value) == 0:
            data = json.dumps({'success': False})
        else:
            data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_rendered_image(request, pk):
    print('update_property_rendered_image')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        property = model_mixin.model_object("Properties").filter(pk=request.GET['property_id'])[0]

        resource_id = 0
        if "resource_id" in request.GET and request.GET['resource_id']:
            resource_id = request.GET['resource_id']

        file_order = 1
        if "file_order" in request.GET and request.GET['file_order']:
            file_order = request.GET['file_order']

        field_value = request.GET['field_value']

        field_type = None
        if "field_type" in request.GET and request.GET['field_type']:
            field_type = request.GET['field_type']

        check_field = model_mixin.model_object("PropertyRenders").filter(property=property,
                                                                         resource_id=resource_id,
                                                                         key=field_value)
        if not check_field:
            try:
                model_mixin.model_object("PropertyRenders").create(property=property,
                                                                   resource_id=resource_id,
                                                                   key=field_value)
            except Exception as e:
                print(e)

        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Renders",
                                                                           resource_id,
                                                                           request.GET['canvas_id'],
                                                                           None)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_property_render(request, pk):
    print('delete_property_render')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        key_selected = None
        property_id = None
        resource_id = None
        render_id = None

        if "resource_id" in request.GET and request.GET['resource_id']:
            resource_id = request.GET['resource_id']

        if "property_id" in request.GET and request.GET['property_id']:
            property_id = request.GET['property_id']

        if "key_selected" in request.GET and request.GET['key_selected']:
            key_selected = request.GET['key_selected']

        if "render_id" in request.GET and request.GET['render_id']:
            render_id = request.GET['render_id']

        property_renders = None

        if render_id:
            property_renders = model_mixin.model_object("PropertyRenders").filter(pk=render_id)
        if resource_id:
            property_renders = model_mixin.model_object("PropertyRenders").filter(resource_id=resource_id)

        if key_selected:
            property_renders = property_renders.filter(key=key_selected)

        if property_id:
            property_renders = property_renders.filter(property_id=property_id)

        if property_renders:
            property_renders.delete()

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_property_render_type(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        if "render_type" in request.GET and request.GET['render_type']:
            render_type = request.GET['render_type']

            property_id = None
            if "property_id" in request.GET and request.GET['property_id']:
                property_id = request.GET['property_id']

            resources = model_mixin.model_object("PropertyRenders").filter(key=render_type)
            if property_id:
                resources = resources.filter(property_id=property_id)

            resources.delete()

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_render_building_details(request, pk, building_id):
    print('get_render_building_details')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        floors = model_mixin.klass("Floors").get_all_floors(building_id=building_id)
        floor_list = []

        for floor in floors:
            floors_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor.pk).values_list('building_id',
                                                                                                                 flat=True)
            floors_buildings = model_mixin.model_object("Buildings").filter(pk__in=floors_buildings)

            floor_list.append(
                {'floor': floor,
                 'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                 'assigned_buildings': floors_buildings,
                 }
            )

        try:
            template = 'canvas/templates/includes/renders/property_rendered_fields.html'
            context = {'floor_list': floor_list}

            rendered = render_to_string(template, context)
        except Exception as e:
            print(e)
            rendered = ''

        data = json.dumps({'success': True, 'rendered': rendered})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_render_caption(request, pk):
    print('update_property_render_caption')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        render_id = 0
        if "render_id" in request.GET and request.GET['render_id']:
            render_id = request.GET['render_id']

        orderidx = 1
        if "orderidx" in request.GET and request.GET['orderidx']:
            orderidx = request.GET['orderidx']

        caption = ''
        if "caption" in request.GET and request.GET['caption']:
            caption = request.GET['caption']

        if render_id:
            check_field = model_mixin.model_object("PropertyRenders").filter(pk=render_id)
            if check_field:
                check_field = check_field[0]
                check_field.caption = caption
                check_field.orderidx = orderidx
                check_field.save(using=model_mixin.project.project_connection_name)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_render(request, pk):
    print('update_property_render')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        render_id = 0
        if "render_id" in request.GET and request.GET['render_id']:
            render_id = request.GET['render_id']

        check_field = model_mixin.model_object("PropertyRenders").filter(pk=render_id)
        if check_field:
            check_field = check_field[0]
            check_field.key = request.GET['field_value']
            check_field.save(using=model_mixin.project.project_connection_name)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_rendered_individual_image(request, pk):
    print('update_property_rendered_individual_image')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        resource_id = request.GET['resource_id']

        check_render = model_mixin.model_object("PropertyRenders").filter(property_id=property_id, resource_id=resource_id, key="Individual" )
        if not check_render:
            model_mixin.model_object("PropertyRenders").create(property_id=property_id,
                                                               resource_id=resource_id, key="Individual")

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_property_rendered_fields(request, pk):
    print('get_property_rendered_fields')
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        model_mixin.model_object("PropertyRenders").filter(key="", resource_id=0).delete()
        selected_render_field = model_mixin.klass("Strings").get_string_from_category('property_render_field')

        field_value = None
        if selected_render_field:
            field_value = selected_render_field.english

        property_id = request.GET['property_id']
        property_name = request.GET['property_name']

        check_renders = model_mixin.model_object("PropertyRenders").filter(property_id=property_id).values_list('key', flat=True)

        property_renders_for_key = []
        resources = []
        individual_renders_for_property = []
        total_individual_renders = 0

        set_individual = True
        if not "Individual" in check_renders:
            set_individual = False

            cleaned_field_value = model_mixin.klass("Strings").clean_for_mapping(field_value)
            table_name = model_mixin.klass("PropertyRenders").get_property_render_table(field_value)
            key_values_for_property = model_mixin.klass("Properties").get_property_distinct_editable_value_for_key(property_id,
                                                                                                                   cleaned_field_value,
                                                                                                                   table_name)
            if key_values_for_property:
                for key_value in key_values_for_property:
                    renders = model_mixin.model_object("PropertyRenders").filter(key=key_value,
                                                                                 property_id=property_id)
                    for render in renders:
                        resources.append({'path': model_mixin.model_object("Resources").filter(pk=render.resource_id)[0].path,
                                          'render_id': render.resource_id})

                    property_renders_for_key.append({'key': key_value, 'resources': resources})
            else:
                property_renders_for_key.append({'key': 'n/a', 'resources': []})

        else:
            resources = []
            key_id_list = []

            individual_renders = model_mixin.model_object("PropertyRenders").filter(property_id=property_id)

            for render in individual_renders:
                resource_path = None
                resource = model_mixin.model_object("Resources").filter(pk=render.resource_id)
                if resource:
                    resource_path = resource[0].path

                if resource_path:
                    total_individual_renders += 1

                    if render.pk not in key_id_list:
                        key_id_list.append(render.pk)
                        resources.append(
                            {'path': resource_path,
                             'render_id': render.pk,
                             'caption': render.caption,
                             'orderidx': render.orderidx})

            individual_renders_for_property.append({'key': "Individual", 'resources': resources, 'property_id': property_id})

        try:
            template = 'canvas/templates/includes/renders/property_individual_renders.html'
            context = {'project': model_mixin.project,
                       'individual_renders_for_property': individual_renders_for_property,
                       'property_name': str(property_name).strip(),
                       'set_individual': set_individual,
                       'total_individual_renders': total_individual_renders,
                       'request': request}

            individual_rendered = render_to_string(template, context)
        except Exception as e:
            print('error rendering individual', e)
            individual_rendered = ''

        try:
            template = 'canvas/templates/includes/renders/property_selected_renders.html'
            context = {'project': model_mixin.project,
                       'property_renders_for_key': property_renders_for_key,
                       'property_name': property_name,
                       'set_individual': set_individual,
                       'request': request}

            rendered = render_to_string(template, context)
        except Exception as e:
            print('error getting renders', e)
            rendered = ''

        data = json.dumps({'success': True,
                           'rendered': rendered,
                           'individual_rendered': individual_rendered })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_floor_apartment_renders(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        properties = model_mixin.model_object("Properties").filter(floor_id=floor_id).order_by('orderidx').order_by(
            'name__english')

        apartments_dict = []
        field_value = "Field"
        selected_render_field = model_mixin.klass("Strings").get_string_from_category('property_render_field')

        if selected_render_field:
            field_value = selected_render_field.english

        first_apartment = None
        for apartment in properties:
            if not first_apartment:
                first_apartment = apartment.pk

            render_key = "Individual"
            render = model_mixin.model_object("PropertyRenders").filter(property=apartment, key=render_key)
            if not render:
                render_key = "Field"

            floor_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id)
            for building in floor_buildings:
                building_id = building.building_id

                apartment_kwargs = {'id': apartment.pk,
                                    'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                    'floor_id': int(floor_id),
                                    'building_id': building_id,
                                    'render_key': render_key
                                    }

                apartments_dict.append(apartment_kwargs)

        try:
            template = 'canvas/templates/includes/renders/renders.html'
            context = {'apartments_dict': apartments_dict,
                       'project': model_mixin.project,
                       'selected_render_field': field_value,
                       'request': request}

            rendered = render_to_string(template, context)
        except Exception as e:
            print (e)
            rendered = ''

        data = json.dumps({'success': True,
                           'rendered': rendered,
                           'first_apartment': first_apartment,
                           })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
