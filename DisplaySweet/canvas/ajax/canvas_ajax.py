from django.http import HttpResponse
import json
from django.template.loader import render_to_string
from projects.models.model_mixin import PrimaryModelMixin
from projects.project_mixin import ProjectModelMixin


def get_canvas_wireframe_template_assets(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        canvas_id = request.GET['canvas_id']
        template_id = request.GET['template_id']
        wireframe_id = request.GET['wireframe_id']

        thumbnail_resolution_dict = model_mixin.klass("Canvases").return_container_objects_thumbnail_resolution(template_id, canvas_id)
        default_resolution_dict = model_mixin.klass("Canvases").return_container_objects_default_resolution(template_id, canvas_id)

        all_template_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                         template_id=template_id,
                                                                                             wireframe_id=wireframe_id)


        image_assets = []

        for asset in all_template_assets:
            asset_from_container = model_mixin.model_object("Assets").filter(pk=asset.asset_id)
            if asset_from_container:
                asset_from_container = asset_from_container[0]
                image = model_mixin.model_object("Resources").filter(pk=asset_from_container.source_id)
                if image:
                    image_assets.append(image[0])

        template = "canvas/includes/canvas_assets.html"
        context = {'project': model_mixin.project,
                   'request': request,
                   'thumbnail_image_display_dict': thumbnail_resolution_dict,
                   'default_image_display_dict': default_resolution_dict,
                   'wireframe_id': wireframe_id,
                   'template_id': template_id,
                   'assets': image_assets}

        file_rendered = render_to_string(template, context)


        data = json.dumps(
            {
                'success': True,
                'file_rendered': file_rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_canvas_wireframe_template_published_assets(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        canvas_id = request.GET['canvas_id']
        template_id = request.GET['template_id']
        wireframe_id = request.GET['wireframe_id']

        canvas_name = model_mixin.model_object("Canvases").filter(pk=canvas_id).values_list('canvas_name__english', flat=True)[0]
        wireframe_name = model_mixin.model_object("Wireframes").filter(pk=wireframe_id).values_list('name', flat=True)[0]

        thumbnail_resolution_dict = model_mixin.klass("Canvases").return_container_objects_thumbnail_resolution(template_id, canvas_id)
        default_resolution_dict = model_mixin.klass("Canvases").return_container_objects_default_resolution(template_id, canvas_id)

        all_template_assets = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                         template_id=template_id,
                                                                                         wireframe_id=wireframe_id)

        container_resources = []

        for asset in all_template_assets:
            asset_from_container = model_mixin.model_object("Assets").filter(pk=asset.asset_id)
            if asset_from_container:
                asset_from_container = asset_from_container[0]
                resource = model_mixin.model_object("Resources").filter(pk=asset_from_container.source_id)[0]

                show_image_path = model_mixin.klass("Canvases").return_canvas_save_path(model_mixin.project.project_connection_name,
                                                                                        canvas_name, wireframe_name)[:-1]

                resource_name = resource.path.split("/")[-1]
                publish_link = show_image_path + '/thumbnails/' + resource_name

                resource_dimensions = model_mixin.klass("Canvases").get_published_resource_dimensions(resource, publish_link)

                static_link = 'http://services.displaysweet.com/get_resource.php?preview_path=%s' % publish_link
                static_link = static_link.replace("//", "/")

                container_resources.append({'path': resource.path,
                                            'publish_link': publish_link,
                                            'resource_dimensions': resource_dimensions,
                                            'show_static': static_link})

        template = "canvas/includes/canvas_published_assets.html"
        context = {'project': model_mixin.project,
                   'request': request,
                   'thumbnail_image_display_dict': thumbnail_resolution_dict,
                   'default_image_display_dict': default_resolution_dict,
                   'wireframe_id': wireframe_id,
                   'template_id': template_id,
                   'assets': container_resources}

        file_rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'file_rendered': file_rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_project_publish_path(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if "publish_path" in request.GET and request.GET['publish_path']:
            publish_path = request.GET['publish_path']
            model_mixin.project.project_publishing_path = publish_path
            model_mixin.project.save()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def torch_canvas_content_table(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        model_mixin.model_object("Content").filter(canvas_id=request.GET['canvas_id']).delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def publish_canvas_template(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        print('publish_canvas_template', model_mixin.project, model_mixin.project.project_connection_name)

        template_id = request.GET['template_id']
        canvas_id = request.GET['selected_canvas_id']
        wireframe_id = request.GET['wireframe_id']
        skip_movies = request.GET['skip_movies']

        if skip_movies == "true":
            skip_movies = True
        if skip_movies == "false":
            skip_movies = False

        publishing_message = ''

        try:
            hashed_file_details, publishing_message = model_mixin.klass("Canvases").pubish_canvas_template(wireframe_id,
                                                                                                           canvas_id,
                                                                                                           template_id,
                                                                                                           user_id=request.user.pk,
                                                                                                           skip_movies=skip_movies)
        except Exception as e:
            hashed_file_details = None
            publishing_message += 'Error publishing canvas template: %s <br>' % e
            print(e)

        if hashed_file_details:
            try:
                for key, value in hashed_file_details.items():
                    content_row = model_mixin.model_object("Content").filter(pk=key)
                    if content_row:
                        content_row = content_row[0]
                        content_row.has = value
                        content_row.save(using=model_mixin.project.project_connection_name)
            except Exception as e:
                publishing_message += "<br> >>>>--- Error updating content table %s <br>", e

        data = json.dumps(
            {
                'success': True,
                'publishing_message': publishing_message
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_image(request, pk):
    if request.is_ajax():
        if "id" in request.GET:
            model_mixin = PrimaryModelMixin(request=request, project_id=pk)

            success = True
            id = request.GET['id']
            image = model_mixin.model_object("Resources").filter(pk=id)
            if image:
                assets = model_mixin.model_object("Assets").filter(source_id=id)
                container_assigned_assets = model_mixin.model_object("ContainerAssignedAssets").filter(asset__in=assets)

                container_assigned_assets.delete()
                assets.delete()

                image[0].delete()

            data = json.dumps(
                {
                    'success': success
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def remove_wireframes(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if "removed_wireframes" in request.GET:
            removed_wireframes = request.GET['removed_wireframes'].replace(' ', '', 10)
            removed_wireframes_list = removed_wireframes.split(',')

            for item in removed_wireframes_list:
                item = str(item).strip()

                if item and int(item):
                    wire_frame = model_mixin.model_object('Wireframes').filter(pk=item)
                    if wire_frame:
                        children = model_mixin.model_object("Wireframes").filter(wireframe_parent=wire_frame[0])
                        if children:
                            children_ids = children.values_list('pk', flat=True)
                            model_mixin.model_object('CanvasWireframes').filter(wireframe_id__in=children_ids).delete()
                            wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(
                                wireframe_id__in=children_ids)
                            template_containers = model_mixin.model_object('WireframeControllerContainers').filter(
                                wireframe_controller=wireframe_controllers)
                            model_mixin.model_object('ContainerStringFields').filter(
                                template_container=template_containers).delete()

                            template_containers.delete()
                            wireframe_controllers.delete()
                            children.delete()

                        model_mixin.model_object('CanvasWireframes').filter(wireframe_id=item).delete()
                        wireframe_controllers = model_mixin.model_object('WireframeControllers').filter(
                            wireframe_id=item)
                        template_containers = model_mixin.model_object('WireframeControllerContainers').filter(
                            wireframe_controller=wireframe_controllers)
                        model_mixin.model_object('ContainerStringFields').filter(
                            template_container=template_containers).delete()

                        template_containers.delete()
                        wireframe_controllers.delete()

                        model_mixin.model_object('Wireframes').filter(pk=item).delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_containers(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if "removed_containers" in request.GET:
            removed_wireframes = request.GET['removed_containers']
            removed_wireframes_list = removed_wireframes.split(',')

            for item in removed_wireframes_list:
                item = str(item).lstrip().rstrip()

                if item and int(item):
                    wire_frame = model_mixin.model_object("Containers").filter(pk=item)
                    if wire_frame:
                        wire_frame = wire_frame[0]
                        children = model_mixin.model_object('Containers').filter(container_parent=wire_frame)
                        if children:
                            children.delete()

                        model_mixin.model_object('WireframeControllerContainers').filter(container_id=item).delete()
                        model_mixin.model_object('Containers').filter(pk=item).delete()

        if "removed_sub_containers" in request.GET:
            removed_wireframes = request.GET['removed_sub_containers']
            removed_wireframes_list = removed_wireframes.split(',')

            for item in removed_wireframes_list:
                item = str(item).lstrip().rstrip()

                if item and int(item):
                    model_mixin.model_object("ContainerFields").filter(pk=item).delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_add_template_names(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if "this_template" in request.GET:
            this_template = request.GET['this_template']
            template = model_mixin.model_object("Templates").filter(pk=this_template)
            if "current_template_name" in request.GET:
                template_name = request.GET['current_template_name']
                if template and template_name:
                    template = template[0]
                    template.name = template_name
                    template.save(using=model_mixin.project.project_connection_name)

        if "new_template_name" in request.GET:
            new_template_name = request.GET['new_template_name']
            model_mixin.model_object("Templates").create(name=new_template_name)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_canvas_name(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        canvas_instance_id = None
        if "canvas_name" in request.GET:
            selected_canvas = request.GET['selected_canvas']
            canvas_name = request.GET['canvas_name']

            if selected_canvas == "None":
                canvas_name = model_mixin.klass("Names").get_or_create_display_name_instance(canvas_name)
                model_mixin.model_object("Canvases").create(canvas_name=canvas_name)
                canvases = model_mixin.model_object("Canvases").filter(canvas_name=canvas_name)
                canvas_instance = canvases[0]
                canvas_instance_id = canvas_instance.pk
            else:
                canvas_instance = model_mixin.model_object("Canvases").filter(
                    pk=selected_canvas)
                if canvas_instance:
                    canvas_instance = canvas_instance[0]
                    canvas_instance_id = canvas_instance.pk
                    name_instance = model_mixin.model_object("Names").filter(
                        pk=canvas_instance.canvas_name_id)
                    if name_instance:
                        name_instance = name_instance[0]
                        name_instance.english = canvas_name
                        name_instance.save(using=model_mixin.project.project_connection_name)

        data = json.dumps(
            {
                'success': True,
                'canvas_id': canvas_instance_id
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_wireframe_for_canvas(request, pk, canvas_id):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        canvas = model_mixin.model_object("Canvases").filter(pk=canvas_id)[0]
        model_mixin.model_object("Wireframes").filter(pk=canvas.wireframe_id).delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_canvas(request, pk, canvas_id):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        canvas = model_mixin.model_object("Canvases").filter(pk=canvas_id)[0]
        controllers = model_mixin.model_object("WireframeControllers").filter(canvas_id=canvas_id)
        model_mixin.model_object("CanvasStringFields").filter(canvas_id=canvas_id).delete()
        model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id).delete()

        for controller in controllers:
            model_mixin.model_object("CanvasWireframes").filter(canvas_id=canvas.pk,
                                                                 wireframe_id=controller.wireframe_id).delete()

            model_mixin.model_object("ContainerAssignedAssets").filter(template_id=controller.template_id).delete()
            model_mixin.model_object("WireframeControllerContainers").filter(wireframe_controller_id=controller.pk).delete()

        model_mixin.model_object("CanvasWireframes").filter(canvas_id=canvas.pk).delete()

        try:
            model_mixin.model_object("CanvasDevices").filter(canvas_id=canvas_id).delete()
        except:
            pass

        controllers.delete()

        try:
            delete_model_row_instant(request, pk, "Canvases", canvas_id)
        except:
            pass

        canvas.delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_model_row_instant(request, pk, model_name, row_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_instance_from_id(row_id)
    model_instance.delete(using=project.project_connection_name)