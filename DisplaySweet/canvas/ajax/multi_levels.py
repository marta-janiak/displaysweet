from django.http import HttpResponse
import json
from projects.models.model_mixin import PrimaryModelMixin


def save_image_selector_files(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        canvas_id = request.GET['canvas_id']

        individual_file_id = None
        if "individual_file_id" in request.GET and request.GET['individual_file_id']:
            individual_file_id = request.GET['individual_file_id']

        multi_file_id = None
        if "multi_file_id" in request.GET and request.GET['multi_file_id']:
            multi_file_id = request.GET['multi_file_id']

        individual_image_container_id = None
        if "individual_image_container_id" in request.GET and request.GET['individual_image_container_id']:
            individual_image_container_id = request.GET['individual_image_container_id']

        multi_image_container_id = None
        if "multi_image_container_id" in request.GET and request.GET['multi_image_container_id']:
            multi_image_container_id = request.GET['multi_image_container_id']

        sub_wireframe_id = request.GET['sub_wireframe_id']
        wireframe = model_mixin.model_object("Wireframes").filter(pk=sub_wireframe_id).values_list(
            'template__template_type__name', flat=True)

        try:
            removed_images = request.GET.getlist('removed_assets')
            if removed_images:
                removed_images = removed_images[0].split(',')
                for image_id in removed_images:
                    if image_id:
                        assets = model_mixin.model_object("Assets").filter(source_id=image_id)

                        for asset in assets:
                            asset_id = asset.pk

                            model_mixin.model_object("ContainerAssignedAssets").filter(
                                canvas_id=canvas_id,
                                wireframe_id=sub_wireframe_id,
                                asset_id=int(asset_id)).delete()

        except Exception as e:
            print ('error removing files', e)

        if wireframe:
            if individual_file_id:
                if individual_image_container_id:
                    model_mixin.model_object("ContainerAssignedAssets").filter(wireframe_id=sub_wireframe_id,
                                                                               canvas_id=canvas_id,
                                                                               container_id=individual_image_container_id).delete()

                    image_list = model_mixin.klass("Assets").save_resource_to_asset_plans_container(wireframe[0],
                                                                                                    individual_file_id,
                                                                                                    canvas_id,
                                                                                                    sub_wireframe_id,
                                                                                                    container_id=individual_image_container_id)
                else:
                    image_list = model_mixin.klass("Assets").save_resource_to_asset_plans_container(wireframe[0],
                                                                                                    individual_file_id,
                                                                                                    canvas_id,
                                                                                                    sub_wireframe_id)

                print ('individual image_list', image_list)

            if multi_file_id:
                if multi_image_container_id:
                    model_mixin.model_object("ContainerAssignedAssets").filter(wireframe_id=sub_wireframe_id,
                                                                               canvas_id=canvas_id,
                                                                               container_id=multi_image_container_id).delete()

                    image_list = model_mixin.klass("Assets").save_resource_to_asset_plans_container(wireframe[0],
                                                                                                 multi_file_id,
                                                                                                 canvas_id,
                                                                                                 sub_wireframe_id,
                                                                                                 container_id=multi_image_container_id)
                else:
                    image_list = model_mixin.klass("Assets").save_resource_to_asset_plans_container(wireframe[0],
                                                                                                    multi_file_id,
                                                                                                    canvas_id,
                                                                                                    sub_wireframe_id)

                print ('multi image_list', image_list)

        data = json.dumps({
            'success': True
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)

