from django.http import HttpResponse
import json
import string
from difflib import SequenceMatcher
from projects.models.model_mixin import PrimaryModelMixin


def save_new_floor_hotspots(request, pk, apartment_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        x_coordinate = request.GET['x_coordinate']
        y_coordinate = request.GET['y_coordinate']
        floor_id = request.GET['floor_id']
        hotspot_order = request.GET['hotspot_order']
        add_row = True

        if "linked_property" in request.GET and "linked_floor" in request.GET:
            linked_floor = request.GET['linked_floor']
            linked_property = request.GET['linked_property']
            linked_property = model_mixin.model_object("Properties").get(pk=linked_property)

            model_mixin.klass("Properties").save_apartment_hotspot_values(linked_property,
                                                                          x_coordinate,
                                                                          y_coordinate,
                                                                          floor_id=linked_floor,
                                                                          hotspot_order=hotspot_order)

        property_item = model_mixin.model_object("Properties").get(pk=apartment_id)

        hotspot = model_mixin.klass("Properties").save_apartment_hotspot_values(property_item,
                                                                                x_coordinate,
                                                                                y_coordinate,
                                                                                floor_id=floor_id,
                                                                                hotspot_order=hotspot_order)

        floor = model_mixin.model_object("Floors").get(pk=floor_id)

        data = json.dumps({
            'success': True,
            'hotspot_id': hotspot.pk,
            'floor_name': model_mixin.klass("Names").get_names_instance(floor.name_id),
            'property_name': model_mixin.klass("Names").get_names_instance(property_item.name_id),
            'add_row': add_row
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_hotspot(request, pk, hotspot_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        type = request.GET['type']

        if type == "hotspot":
            model_mixin.model_object("FloorsHotspots").filter(pk=hotspot_id).delete()
        else:
            property_hotspots = model_mixin.model_object("PropertyHotspots").filter(property_id=hotspot_id)
            floors_hotspots = property_hotspots.values_list('floors_hotspot_id', flat=True)
            model_mixin.model_object("FloorsHotspots").filter(pk__in=floors_hotspots).delete()
            property_hotspots.delete()

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_new_linked_floor_hotspots(request, pk):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        x_coordinate = request.GET['x_coordinate']
        y_coordinate = request.GET['y_coordinate']
        floor_id = request.GET['floor_id']

        new_hotspot_name = None
        if "new_hotspot_name" in request.GET and request.GET['new_hotspot_name']:
            new_hotspot_name = request.GET['new_hotspot_name']

        hotspot_id = None
        if "hotspot_id" in request.GET and request.GET['hotspot_id']:
            hotspot_id = request.GET['hotspot_id']

        new_floor_id = None
        if "new_floor_id" in request.GET and request.GET['new_floor_id']:
            new_floor_id = request.GET['new_floor_id']

        property_name = ''
        if hotspot_id:
            apartment_id = None
            if "property_id" in request.GET and request.GET['property_id']:
                apartment_id = request.GET['property_id']

            hotspot = model_mixin.model_object("FloorsHotspots").filter(pk=hotspot_id)

            if hotspot:
                hotspot = hotspot[0]
                hotspot.x = x_coordinate
                hotspot.y = y_coordinate

                if new_hotspot_name:
                    hotspot.name = new_hotspot_name
                    property_name = new_hotspot_name
                else:
                    linked_property = model_mixin.model_object("PropertyHotspots").filter(floors_hotspot=hotspot)

                    if linked_property:
                        linked_property = linked_property[0]
                        property_name = model_mixin.model_object("Properties").filter(pk=linked_property.property_id).values_list('name__english', flat=True)
                        if property_name:
                            property_name = property_name[0]

                hotspot.save(using=model_mixin.project.project_connection_name)

                if apartment_id and not apartment_id == "None":
                    model_mixin.model_object("PropertyHotspots").get_or_create(floors_hotspot=hotspot,
                                                                           property_id=apartment_id)
        else:
            apartment_id = None
            if "property_id" in request.GET and request.GET['property_id']:
                apartment_id = request.GET['property_id']

            if apartment_id and not apartment_id == "None":
                property_item = model_mixin.model_object("Properties").get(pk=apartment_id)
                property_name = model_mixin.klass("Names").get_names_instance(property_item.name_id)

            else:
                property_item = None
                property_name = new_hotspot_name

            #Two floor hotspots to one property unless it's the same floor and property
            model_mixin.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                              x=x_coordinate,
                                                              y=y_coordinate,
                                                              name=new_hotspot_name)

            hotspot = model_mixin.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                        x=x_coordinate,
                                                                        y=y_coordinate,
                                                                        name=new_hotspot_name).order_by('-pk')[0]

            hotspot_id = hotspot.pk
            
            if property_item:
                model_mixin.model_object("PropertyHotspots").create(property_id=apartment_id,
                                                                    floors_hotspot_id=hotspot.pk,
                                                                    orderidx=2)

            if new_floor_id and not new_floor_id == floor_id:
                model_mixin.model_object("FloorsHotspots").create(floor_id=new_floor_id,
                                                                  x=x_coordinate,
                                                                  y=y_coordinate,
                                                                  name=new_hotspot_name)

                hotspot = model_mixin.model_object("FloorsHotspots").filter(floor_id=new_floor_id,
                                                                            x=x_coordinate,
                                                                            y=y_coordinate,
                                                                            name=new_hotspot_name).order_by('-pk')[0]

                if property_item:
                    model_mixin.model_object("PropertyHotspots").create(property_id=apartment_id,
                                                                        floors_hotspot_id=hotspot.pk,
                                                                        orderidx=2)
                hotspot_id = hotspot.pk

        floor = model_mixin.model_object("Floors").get(pk=floor_id)

        data = json.dumps({
            'success': True,
            'hotspot_id': hotspot_id,
            'floor_name': model_mixin.klass("Names").get_names_instance(floor.name_id),
            'property_name': property_name,
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_floor_hotspots(request, pk, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        apartments_dict = []
        floor_hotspots = []

        building_id = None
        if "building_id" in request.GET:
            building_id = request.GET['building_id']

        floor = model_mixin.model_object("Floors").filter(pk=floor_id)[0]
        apartments = model_mixin.model_object("Properties").filter(floor_id=floor_id).order_by('orderidx')

        for property_item in apartments:
            hotspot_list = model_mixin.klass("Properties").get_hotspot_values(property_item.pk, floor_id)
            for hotspot in hotspot_list:
                if hotspot['floor_hotspot_id'] and not hotspot['floor_hotspot_id'] in floor_hotspots:
                    floor_hotspots.append(hotspot['floor_hotspot_id'])

                apartment_kwargs = { 'id': property_item.pk,
                                     'floor_hotspot_id': hotspot['floor_hotspot_id'],
                                     'x_coordinate': hotspot['picking_coord_x'],
                                     'y_coordinate': hotspot['picking_coord_y'],
                                     'floor_id': int(floor.pk),
                                     'property_floor_id': 'ere',
                                     'building_id': building_id,
                                     'property_type_id': property_item.property_type_id,
                                     'compare_name': str(model_mixin.klass("Names").get_names_instance(property_item.name_id)).strip().lower(),
                                     'orderidx': property_item.orderidx
                                    }

                apartments_dict.append(apartment_kwargs)

        hotspots_on_floor = model_mixin.model_object("FloorsHotspots").filter(floor_id=floor_id).order_by('name').exclude(pk__in=floor_hotspots)

        for hotspot in hotspots_on_floor:
            property_id = None
            property_floor_id = None
            check_hotspot = model_mixin.model_object("PropertyHotspots").filter(floors_hotspot_id=hotspot.pk)
            if check_hotspot:
                property_id = check_hotspot[0].property_id
                property_floor_id = check_hotspot.values_list('property__floor_id', flat=True)
                property_floor_id = property_floor_id[0]

            apartment_kwargs = { 'id': None,
                                 'property_id': property_id,
                                 'hotspot_id': hotspot.pk,
                                 'x_coordinate': hotspot.x,
                                 'y_coordinate': hotspot.y,
                                 'hotspot_name': hotspot.name,
                                 'floor_id': int(floor_id),
                                 'property_floor_id': property_floor_id,
                                 'building_id': building_id,
                                 'compare_name': str(hotspot.name).strip().lower(),
                                 'orderidx': 99
                                }

            apartments_dict.append(apartment_kwargs)

        apartment_list = json.dumps(apartments_dict)

        data = json.dumps({'apartment_list': apartment_list,
                           'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def copy_apartment_to_floor(request, pk, property_id, floor_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        main_instance = model_mixin.model_object("Properties"). \
            filter(pk=property_id).values_list('pk', 'name__english')

        success = False

        name = main_instance[0][1]
        id = main_instance[0][0]

        selected_floor = model_mixin.model_object("Properties").filter(pk=property_id)[0].floor_id
        spot_list = model_mixin.klass("Properties").get_hotspot_values(id, selected_floor)

        for spot in spot_list:

            try:
                picking_coord_x = spot['picking_coord_x']
                picking_coord_y = spot['picking_coord_y']
            except:
                picking_coord_x = 0
                picking_coord_y = 0

            apartment_instances = model_mixin.model_object("Properties"). \
                filter(floor_id=floor_id).values('pk', 'name__english')

            most_likely_apartment_id = None
            found_floor_id = None
            x = None
            y = None
            score = 0

            for row in apartment_instances:

                apartment_id = row['pk']
                property_name = row['name__english']

                if "." in str(name) and "." in str(property_name):
                    selected_name = str(name).split('.')[1]
                    property_name = str(property_name).split('.')[1]

                else:
                    selected_name = ''.join(filter(lambda x: x.isdigit(), str(name)))
                    property_name = ''.join(filter(lambda x: x.isdigit(), str(property_name)))

                ratio_score = get_likely_score_ration(selected_name, property_name)

                if float(ratio_score) >= float(score):
                    most_likely_apartment_id = apartment_id
                    check_property = model_mixin.model_object("Properties").filter(pk=apartment_id)[0]
                    found_floor_id = check_property.floor_id

                    score = ratio_score
                    x = picking_coord_x
                    y = picking_coord_y

                if str(property_name[1:]) == str(name[1:]) and picking_coord_x and picking_coord_y:
                    x = picking_coord_x
                    y = picking_coord_y

                    found_property = model_mixin.model_object("Properties").filter(pk=apartment_id)[0]
                    model_mixin.klass("Properties").save_apartment_hotspot_values(apartment_id, x, y,
                                                                                  floor_id=found_property.floor_id,
                                                                                  hotspot_order=1)

                    break

            if most_likely_apartment_id and x and y:
                model_mixin.klass("Properties").save_apartment_hotspot_values(most_likely_apartment_id, x,
                                                                              y, floor_id=found_floor_id,
                                                                              hotspot_order=1)

                success = True

        data = json.dumps({'success': success, 'floor_id': floor_id, 'saved': success})
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_likely_score_ration(field_one, field_two):
    return SequenceMatcher(None, field_one.lower(), field_two.lower()).ratio()


def reassign_apartment_hotspots(request):
    if request.is_ajax():
        data = []

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)