from django.http import HttpResponse
import json
from django.template.loader import render_to_string
from projects.models.model_mixin import PrimaryModelMixin


def update_aspect_image_floor(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        resource_id = request.GET['resource_id']
        floor_id = request.GET['floor_id']

        aspect = model_mixin.klass("Properties").add_floor_aspect(floor_id, resource_id)
        if aspect:
            aspect_id = aspect.pk
        else:
            aspect_id = 1

        asset = model_mixin.model_object("Assets").add_new_image_asset_resource(resource_id)

        selected_canvas = model_mixin.model_object('Canvases').filter(pk=request.GET['canvas_id'])[0]
        selected_template = model_mixin.model_object('Templates').filter(pk=request.GET['template_id'])[0]

        wireframe_controller = model_mixin.klass("Wireframes").get_wireframe_controller(selected_canvas, selected_template,
                                                                                        request.GET['sub_wireframe_id'])

        template_containers = model_mixin.model_object("WireframeControllerContainers").filter(wireframe_controller=wireframe_controller)

        containers = []
        for tc in template_containers:
            containers.append(tc.container_id)

        page_type = model_mixin.model_object("TemplateTypes").filter(pk=selected_template.template_type_id)
        template_type = None
        if page_type:
            template_type = page_type[0].name

        appropriate_container = model_mixin.klass("Containers").get_appropriate_container(containers, template_type, 'main')

        container_assigned_asset = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=request.GET['canvas_id'],
                                                                                              template_id=request.GET[
                                                                                                  'template_id'],
                                                                                              wireframe_id=request.GET[
                                                                                                  'sub_wireframe_id'],
                                                                                              asset_id=int(asset.pk),
                                                                                              container_id=appropriate_container)

        if not container_assigned_asset:
            model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=int(request.GET['canvas_id']),
                                                                       template_id=int(request.GET['template_id']),
                                                                       wireframe_id=int(request.GET['sub_wireframe_id']),
                                                                       asset_id=int(asset.pk),
                                                                       container_id=appropriate_container,
                                                                       orderidx=1)

        container_assigned_asset = model_mixin.model_object("ContainerAssignedAssets").filter(
                                                                            canvas_id=request.GET['canvas_id'],
                                                                            template_id=request.GET['template_id'],
                                                                            wireframe_id=request.GET['sub_wireframe_id'],
                                                                            asset_id=int(asset.pk),
                                                                            container_id=appropriate_container)

        all_floor_images = model_mixin.model_object("FloorsPlans").all()
        all_floor_images = all_floor_images.exclude(key="level_plans")

        for floor_plan_image in all_floor_images:
            asset = model_mixin.klass("Assets").add_new_image_asset_resource(floor_plan_image.resource_id)
            check_asset_created = model_mixin.model_object("ContainerAssignedAssets").filter(canvas_id=request.GET['canvas_id'],
                                                                                             template_id=request.GET['template_id'],
                                                                                             wireframe_id=request.GET['sub_wireframe_id'],
                                                                                             asset_id=int(asset.pk),
                                                                                             container_id=appropriate_container)

            if not check_asset_created:
                model_mixin.model_object("ContainerAssignedAssets").create(canvas_id=int(request.GET['canvas_id']),
                                                                           template_id=int(request.GET['template_id']),
                                                                           wireframe_id=int(request.GET['sub_wireframe_id']),
                                                                           asset_id=int(asset.pk),
                                                                           container_id=appropriate_container,
                                                                           orderidx=1)

                model_mixin.klass("Assets").save_resource_to_asset_plans_container("Floor Views",
                                                                                   resource_id,
                                                                                   request.GET['canvas_id'],
                                                                                   request.GET['sub_wireframe_id'])
        data = json.dumps({'success': True,
                           'aspect_id': aspect_id})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_aspect(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        type = request.GET['type']
        aspect_id = request.GET['aspect_id']

        if type == "apartment":
            model_mixin.klass("Properties").remove_property_resource_by_id(aspect_id)
        elif type == "floor":
            model_mixin.klass("Properties").remove_floor_resource_by_id(aspect_id)
        elif type == "aspect":
            model_mixin.klass("Properties").remove_property_aspect_by_id(aspect_id)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def add_property_aspect(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        property_id = request.GET['property_id']
        aspect = request.GET['aspect']

        file_order = 1
        if 'file_order' in request.GET:
            file_order = request.GET['file_order']

        if aspect:
            model_mixin.klass("PropertyAspects").create_new_aspect_key(property_id,
                                                                       aspect,
                                                                       file_order)

        data = json.dumps({'success': True})
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_aspect_image(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        print('--', request.GET)
        resource_id = request.GET['resource_id']
        property_id = request.GET['property_id']
        selected_aspect = request.GET['selected_aspect']

        file_order = 1
        if 'file_order' in request.GET:
            file_order = request.GET['file_order']

        aspect_id = 0
        aspect = model_mixin.klass("Properties").add_property_aspect(property_id,
                                                                     file_order,
                                                                     selected_aspect,
                                                                     resource_id=resource_id)

        if aspect:
            aspect_id = aspect.pk

        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Views",
                                                                           resource_id,
                                                                           request.GET['canvas_id'],
                                                                           request.GET['sub_wireframe_id'])

        data = json.dumps({'success': True,
                           'aspect_id': aspect_id})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_aspect_caption(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        file_id = request.GET['file_id']
        new_caption = request.GET['new_caption']
        aspect = model_mixin.model_object("PropertyResources").filter(pk=file_id)
        aspect = aspect[0]
        aspect.key = new_caption
        aspect.save(using=model_mixin.project.project_connection_name)
        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_property_aspects(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        aspect_dict = []
        ignore_keys = ['level_plans', 'key_plans', 'floor_plans']

        property_id = request.GET['property_id']

        file_aspects = model_mixin.model_object("PropertyAspects").filter(property_id=property_id)
        print('file_aspects', file_aspects, property_id)

        key_list = []
        for aspect in file_aspects:
            key = str(aspect.key).strip()

            if key not in ignore_keys:
                if not key in key_list:
                    key_list.append(key)

                aspect_file = None
                key_file = model_mixin.model_object("Resources").filter(pk=aspect.resource_id)
                if key_file:
                    aspect_file = key_file[0]

                if aspect_file:
                    aspect_dict.append({'key': key,
                                        'file': aspect_file,
                                        'pk': aspect.pk,
                                        'property_id': aspect.property_id,
                                        })

        print('key_list', key_list)
        template = 'canvas/templates/includes/aspects_file_table.html'
        context = {'aspect_dict': aspect_dict,
                   'key_list': key_list,
                   'project': model_mixin.project,
                   'model_mixin': model_mixin,
                   'request': request}

        try:
            file_rendered = render_to_string(template, context)
        except Exception as e:
            print(e)

        template = 'canvas/templates/includes/aspects_table.html'
        context = {'aspect_dict': aspect_dict,
                   'key_list': key_list,
                   'project': model_mixin.project,
                   'model_mixin': model_mixin,
                   'request': request}

        try:
            file_aspect_rendered = render_to_string(template, context)
        except Exception as e:
            print(e)

        template = 'canvas/templates/includes/views_image_display.html'

        context = {'aspect_dict': aspect_dict,
                   'key_list': key_list,
                   'project': model_mixin.project,
                   'model_mixin': model_mixin,
                   'request': request}

        try:
            image_file_rendered = render_to_string(template, context)
        except Exception as e:
            print(e)

        data = json.dumps({'success': True,
                           'file_rendered': file_rendered,
                           'file_aspect_rendered': file_aspect_rendered,
                           'image_file_rendered': image_file_rendered,
                           })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


