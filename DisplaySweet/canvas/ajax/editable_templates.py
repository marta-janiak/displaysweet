from django.http import HttpResponse
import json
from django.conf import settings
from django.apps import apps
from django.forms.models import inlineformset_factory

from django.db.models.fields.related import ForeignKey, ManyToOneRel
from core.models import ProjectCSVTemplate, ProjectCSVTemplateFields
from projects.forms import get_new_property_template_formset, get_apartment_template_formset, \
                            CreateImportTemplateForm

from django.template.loader import render_to_string
from projects.project_mixin import ProjectModelMixin
from projects.models.model_mixin import PrimaryModelMixin


def get_editable_property_template(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)


        property_id = request.GET['property_id']
        template_id = request.GET['template_id']
        building_id = request.GET['building_id']

        save_current = False
        if "save_current" in request.GET:
            save_current = True

        apartment = model_mixin.model_object("Properties").filter(pk=property_id)[0]

        all_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project).order_by('id')
        editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
        total_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template)

        if save_current:
            get_form_class = get_apartment_template_formset(model_mixin.project,
                                                            model_mixin,
                                                            apartment,
                                                            building_id=building_id)
        else:
            get_form_class = get_new_property_template_formset(model_mixin.project,
                                                               model_mixin,
                                                               apartment,
                                                               building_id=building_id)

        IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                                  form=get_form_class,
                                                  extra=0)

        form = CreateImportTemplateForm(instance=editable_template)
        ingredient_formset = IngredientFormSet(instance=editable_template,
                                               queryset=editable_template.project_template_fields.order_by("order"))

        context = {
            'project': model_mixin.project,
            'template_form': form,
            'template_columns_form': ingredient_formset,
            'total_fields': len(total_fields),
            'apartment': apartment,
            'all_templates': all_templates,
            'template_id': int(template_id),
            'request': request
        }
        html_template = "canvas/templates/includes/editable_property.html"

        try:
            rendered = render_to_string(html_template, context)
        except Exception as e:
            print('get_editable_property_template', e)

        data = json.dumps(
            {
                'success': True,
                'editable_property_html': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_new_property_editable_template(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        floor_id = request.POST['floor_id']
        selected_property_id = request.POST['selected_property_id']

        id_fields = []
        for key, value in request.POST.items():
            if "-id" in key:
                new_id = key.replace("-id", "").replace("project_template_fields-", "")
                if new_id not in id_fields:
                    id_fields.append(new_id)

        apartment_kwargs = {'floor_id': floor_id, 'property_type_id': 1}
        all_apartment_kwargs = {}
        string_kwargs = {}
        int_fields_kwargs = {}
        boolean_fields_kwargs = {}
        float_fields_kwargs = {}
        hotspot_kwargs = {}
        apartment_update_list = []

        new_item_id = ''
        new_item_floor_id = ''

        property_model_object = model_mixin.get_class_object('Properties')
        apartment_model_fields = property_model_object._meta.get_fields()

        apartment_fields = []
        for field in apartment_model_fields:
            if field.name not in ['id', 'floor', 'building']:
                apartment_fields.append(field)

        for field in id_fields:
            column_field_mapping = 'project_template_fields-%s-column_field_mapping' % field
            column_table_mapping = 'project_template_fields-%s-column_table_mapping' % field
            value = 'project_template_fields-%s-apartment_value' % field

            if value in request.POST and request.POST[value] and column_table_mapping in request.POST:
                column_field_mapping = request.POST[column_field_mapping]
                column_table_mapping = request.POST[column_table_mapping]
                value = request.POST[value]

                # print ('column_field_mapping', column_field_mapping)
                # print ('column_table_mapping', column_table_mapping)
                # print ('value', value)

                if column_field_mapping.strip().lower() in ["floor_plan_typical", "floor plan typical"]:
                    category_queryset = model_mixin.model_object("Texttablecategories").filter(
                        category=settings.FLOOR_PLAN_TYPICAL)

                    category = category_queryset[0]
                    strings_queryset = model_mixin.model_object('Strings').filter(text_table_category=category,
                                                                                    english="floor_plan_typical")

                    if strings_queryset:
                        type_string = strings_queryset[0]
                        kwargs = {
                            '{0}'.format(model_mixin.project.apartment_model_id_field): selected_property_id,
                            '{0}'.format('type_string'): type_string,
                        }

                        queryset = model_mixin.model_object("PropertiesStringFields").filter(**kwargs)
                        if queryset:
                            queryset[0].value = value
                            queryset[0].save(using=model_mixin.project.project_connection_name)

                if "Propertiesstringfields" in column_table_mapping:
                    # ##print'--- adding strings key value pair'
                    string_kwargs.update({column_field_mapping: value})

                elif "Propertiesintfields" in column_table_mapping:
                    # ##print'--- adding int field key value pair'
                    int_fields_kwargs.update({column_field_mapping: value})

                elif "Propertiesbooleanfields" in column_table_mapping:
                    # ##print'--- adding boolean field key value pair'
                    boolean_fields_kwargs.update({column_field_mapping: value})

                elif "Propertiesfloatfields" in column_table_mapping:
                    # ##print'--- adding float field key value pair'
                    float_fields_kwargs.update({column_field_mapping: value})

                else:
                    apartment_key = column_field_mapping

                    if apartment_key == "property_type":
                        apartment_kwargs.update(
                            {'{0}'.format("property_type_id"): value}
                        )
                    elif apartment_key == "floor":
                        apartment_kwargs.update(
                            {'{0}'.format("floor_id"): value}
                        )
                    else:
                        for a_field in apartment_fields:
                            if type(a_field) in [ForeignKey, ManyToOneRel] and a_field.name == apartment_key:
                                new_item_name = model_mixin.model_object("Names").filter(english=value)
                                if not new_item_name:
                                    kwargs = {
                                        '{0}'.format('english'): value,
                                    }

                                    model_mixin.model_object("Names").create(**kwargs)
                                    new_item_name = model_mixin.model_object("Names").filter(english=value)
                                    new_item_name = new_item_name[0]
                                else:
                                    new_item_name = new_item_name[0]

                                related_object = a_field.rel.to
                                try:
                                    new_item_kwargs = {
                                        '{0}'.format('name'): new_item_name,
                                    }
                                    new_items = related_object.objects.using(model_mixin.project.project_connection_name).filter(
                                        **new_item_kwargs)

                                except:
                                    new_item_kwargs = {
                                        '{0}'.format('english'): new_item_name,
                                    }
                                    new_items = related_object.objects.using(model_mixin.project.project_connection_name).filter(
                                        **new_item_kwargs)

                                ##print'new_items', new_items
                                if not new_items:
                                    related_object.objects.using(model_mixin.project.project_connection_name).create(
                                        **new_item_kwargs)
                                    new_items = related_object.objects.using(model_mixin.project.project_connection_name).filter(
                                        **new_item_kwargs)
                                    if new_items:
                                        new_item = new_items[0]
                                    else:
                                        new_item = None

                                else:
                                    new_item = new_items[0]

                                if new_item:
                                    apartment_kwargs.update(
                                        {'{0}'.format(a_field.name): new_item}
                                    )

        if "name" in apartment_kwargs:
            if selected_property_id:
                new_apartment = model_mixin.model_object("Properties").filter(pk=selected_property_id)
            else:
                new_apartment = model_mixin.model_object("Properties").filter(**apartment_kwargs)

                if "floor_id" in apartment_kwargs:
                    new_apartment = new_apartment.filter(floor_id=apartment_kwargs['floor_id'])

            if not new_apartment:
                try:
                    model_mixin.model_object("Properties").create(**apartment_kwargs)
                    new_apartment = model_mixin.model_object("Properties").filter(**apartment_kwargs)
                    if new_apartment:
                        new_apartment = new_apartment[0]
                except Exception as e:
                    print('new apt err', e)
                    pass

            else:
                if new_apartment:
                    new_apartment = new_apartment[0]

            if new_apartment:
                PropertyObject = apps.get_model(model_mixin.project.project_connection_name, "Properties")
                prop_obj = PropertyObject(pk=new_apartment.pk, **apartment_kwargs)
                try:
                    prop_obj.save(using=model_mixin.project.project_connection_name)
                except:
                    for k, v in apartment_kwargs.items():
                        setattr(new_apartment, k, v)
                    new_apartment.save(using=model_mixin.project.project_connection_name)

                all_apartment_kwargs.update({'property_id': new_apartment.pk})
                all_apartment_kwargs.update({'Propertiesstringfields': string_kwargs})
                all_apartment_kwargs.update({'Propertiesintfields': int_fields_kwargs})
                all_apartment_kwargs.update({'Propertiesbooleanfields': boolean_fields_kwargs})
                all_apartment_kwargs.update({'Propertiesfloatfields': float_fields_kwargs})
                all_apartment_kwargs.update({'Propertyhotspots': hotspot_kwargs})

                apartment_update_list.append(all_apartment_kwargs)

                new_item_id = new_apartment.pk
                new_item_floor_id = new_apartment.floor_id

        # ##print'apartment_update_list', apartment_update_list

        for new_apartment in apartment_update_list:
            # ##print'new apartment ', new_apartment
            property_id = new_apartment['property_id']
            ##print'property_idproperty_idproperty_id', property_id
            for key, value in new_apartment.items():
                if key in ['Propertiesstringfields', 'Propertiesintfields', 'Propertiesbooleanfields',
                           'Propertiesfloatfields']:
                    # ##printkey
                    # ##printvalue
                    # ##print'----------------------------'
                    for ikey, ivalue in value.items():
                        strings_item = model_mixin.klass("TextTableCategories").check_category_string(key, ikey)

                        ##print'strings_item', strings_item

                        if strings_item:
                            string_kwargs = {
                                'property_id': property_id,
                                'type_string': strings_item,
                            }

                            key_model_object = apps.get_model(model_mixin.project.project_connection_name, key)

                            try:
                                existing_fields = key_model_object.objects.using(
                                    model_mixin.project.project_connection_name).filter(**string_kwargs)
                                if not existing_fields:
                                    string_kwargs.update({'value': ivalue})
                                    key_model_object.objects.using(model_mixin.project.project_connection_name).create(
                                        **string_kwargs)
                                else:
                                    existing_fields = existing_fields[0]
                                    existing_fields.value = ivalue
                                    existing_fields.save(using=model_mixin.project.project_connection_name)
                            except:
                                print ('canvas ajax: error with string kwargs')

        data = json.dumps(
            {
                'success': True,
                'property_id': new_item_id,
                'floor_id': new_item_floor_id,
                'selected_property_id': selected_property_id
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_template_default_status(request, pk):
    if request.is_ajax():
        if "template" in request.GET:

            if request.GET['checked'] == "true":
                checked = True
                ProjectCSVTemplate.objects.all().update(is_editable_template_default=False)
            else:
                checked = False

            template_id = str(request.GET['template']).replace('edit-template-default-', '')
            template = ProjectCSVTemplate.objects.get(id=template_id)
            template.is_editable_template_default = checked
            template.save()

            data = json.dumps(
                {
                    'success': True
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def update_template_allocation_status(request, pk):
    if request.is_ajax():
        if "template" in request.GET:

            if request.GET['checked'] == "true":
                checked = True
                ProjectCSVTemplate.objects.all().update(is_allocation_template=False)
            else:
                checked = False

            template_id = str(request.GET['template']).replace('edit-template-allocation-', '')
            template = ProjectCSVTemplate.objects.get(id=template_id)
            template.is_allocation_template = checked
            template.save()

            data = json.dumps(
                {
                    'success': True
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def update_template_status(request, pk):
    if request.is_ajax():
        if "template" in request.GET:

            if request.GET['checked'] == "true":
                checked = True
            else:
                checked = False

            template_id = str(request.GET['template']).replace('edit-template-', '')
            template = ProjectCSVTemplate.objects.get(id=template_id)
            template.is_editable_template = checked
            template.save()

            data = json.dumps(
                {
                    'success': True
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def clone_template(request, template_id):
    if request.is_ajax():
        template = ProjectCSVTemplate.objects.get(id=template_id)
        fields = ProjectCSVTemplateFields.objects.filter(template=template)

        if fields:

            new_template = template
            new_template.id = None
            new_template.template_name = '%s - copy' % template.template_name
            new_template.save()

            for field in fields:
                new_field = field
                new_field.id = None
                new_field.template = new_template
                new_field.save()

            data = json.dumps(
                {
                    'success': True
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)

