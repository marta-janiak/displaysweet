from django.http import HttpResponse
import json
from django.conf import settings
from projects.models.model_mixin import PrimaryModelMixin
from django.template.loader import render_to_string


def sortkeypicker(keynames):
    negate = set()
    for i, k in enumerate(keynames):
        if k[:1] == '-':
            keynames[i] = k[1:]
            negate.add(k[1:])
    def getit(adict):
       composite = [adict[k] for k in keynames]
       for i, (k, v) in enumerate(zip(keynames, composite)):
           if k in negate:
               composite[i] = -v
       return composite
    return getit


def get_listing_floor_select(request, pk, building_id):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        builting_floor_list = []
        for floor in model_mixin.klass("Floors").get_all_floors(building_id=building_id):
            builting_floor_list.append({'floor': floor,
                                        'floor_id': floor.pk,
                                        'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                                        })

        template = "canvas/templates/includes/listing_select_floor.html"
        context = {'builting_floor_list': builting_floor_list, 'building_id': building_id}
        rendered = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'rendered': rendered})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_listing_details(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        property_id = request.GET['property_id']
        building_id = request.GET['building_id']

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)
        children_listing_dict = []
        selected_properties = []

        if main_property:
            main_property = main_property[0]
            model_mixin_listing = model_mixin.model_object("ListingMembers").filter(property_id=property_id)

            if model_mixin_listing:
                new_listing = model_mixin_listing[0]
                children_properties = model_mixin.model_object("ListingMembers").filter(
                    listing_id=new_listing.listing_id)
                for property_item in children_properties:
                    selected_properties.append(property_item.property_id)
                    selected_property = model_mixin.model_object("Properties").filter(pk=property_item.property_id)[0]
                    children_listing_dict.append({
                        'pk': selected_property.pk,
                        'name': model_mixin.klass("Names").get_names_instance(selected_property.name_id),
                        'property_type': model_mixin.klass("Properties").get_property_type(
                            selected_property.property_type_id),
                    })

        builting_floor_list = []
        for floor in model_mixin.klass("Floors").get_all_floors(building_id=building_id):
            builting_floor_list.append({'floor': floor,
                                        'floor_id': floor.pk,
                                        'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                                        })

        floor_list = []
        for floor_building in model_mixin.model_object("FloorsBuildings").all().order_by('building_id'):
            floor = model_mixin.model_object("Floors").filter(pk=floor_building.floor_id)
            if floor:
                floor = floor[0]

                apartments_dict = []
                property_list = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')

                for apartment in property_list:
                    property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)

                    selected_class = False
                    if apartment.pk in selected_properties:
                        selected_class = True

                    apartment_kwargs = {'id': apartment.pk,
                                        'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                        'floor_id': floor_building.floor_id,
                                        'building_id': floor_building.building_id,
                                        'property_type': property_type,
                                        'property_type_id': apartment.property_type_id,
                                        'selected_class': selected_class}

                    apartments_dict.append(apartment_kwargs)

                floor_list.append(
                    {'floor': floor,
                     'floor_id': floor.pk,
                     'building_id': floor_building.building_id,
                     'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                     'apartments_dict': apartments_dict
                     })

        template = "canvas/templates/includes/property_listing_details.html"
        context = {'floor_list': floor_list,
                   'builting_floor_list': builting_floor_list,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'project': model_mixin.project,
                   'buildings': model_mixin.klass("Buildings").get_all_buildings(),
                   'children_listing_dict': children_listing_dict,
                   'property_id': main_property.pk,
                   'property_type': model_mixin.klass("Properties").get_property_type(main_property.property_type_id),
                   'property_name': model_mixin.klass("Names").get_names_instance(main_property.name_id),
                   'request': request}

        combined_html = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'combined_html': combined_html})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_combination_filters(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    
    if request.is_ajax():
        
        property_id = request.GET['property_id']
        building_id = request.GET['building_id']

        main_property = model_mixin.model_object("Properties").get(pk=property_id)
        children_properties_dict = []
        selected_properties = []
        selected_upgrades = []

        combined_view_string = model_mixin.model_object("Strings").filter(
            english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)
        if not combined_view_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)
            combined_view_string = model_mixin.model_object("Strings").filter(
                english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)

        combined_view_string = combined_view_string[0]

        combined_parent_string = model_mixin.model_object("Strings").filter(
            english=settings.COMBINED_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(
                english=settings.COMBINED_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]
        children_properties = model_mixin.model_object("PropertiesIntFields").filter(type_string=combined_parent_string,
                                                                                     value=main_property.pk)

        for property_item in children_properties:
            selected_properties.append(property_item.property_id)

            has_combined_view = False
            has_view = model_mixin.model_object("PropertiesStringFields").filter(type_string=combined_view_string,
                                                                                 property_id=property_item.property_id,
                                                                                 value=True)
            if has_view:
                has_combined_view = True

            selected_property = model_mixin.model_object("Properties").filter(pk=property_item.property_id)[0]
            children_properties_dict.append({
                'pk': selected_property.pk,
                'name': model_mixin.klass("Names").get_names_instance(selected_property.name_id),
                'property_type': model_mixin.klass("Properties").get_property_type(selected_property.property_type_id),
                'has_combined_view': has_combined_view,
                'panrent_combined_pk': main_property.pk
            })

        floor_list = []
        for floor in model_mixin.klass("Floors").get_all_floors(building_id=building_id):
            apartments_dict = []

            property_list = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')
            for apartment in property_list:
                property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)

                selected_class = False
                if apartment.pk in selected_properties:
                    selected_class = True

                upgrade_selected_class = False
                if apartment.pk in selected_upgrades:
                    upgrade_selected_class = True

                picking_coord_x, picking_coord_y = model_mixin.klass("Properties").get_apartment_hotspot_values(apartment.pk,
                                                                                                                floor_id=floor.pk)

                apartment_kwargs = {'id': apartment.pk,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                    'floor_id': floor.pk,
                                    'property_type': property_type,
                                    'property_type_id': apartment.property_type_id,
                                    'selected_class': selected_class,
                                    'upgrade_selected_class': upgrade_selected_class}

                apartments_dict.append(apartment_kwargs)

            floor_list.append({'floor': floor,
                               'floor_id': floor.pk,
                               'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                               'apartments_dict': apartments_dict})

        template = "canvas/templates/includes/property_combined_details.html"
        context = {'floor_list': floor_list,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'project': model_mixin.project,
                   'children_properties_dict': children_properties_dict,
                   'property_name': model_mixin.klass("Names").get_names_instance(main_property.name_id),
                   'property_id': main_property.pk,
                   'request': request}

        combined_html = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'combined_html': combined_html})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_upgrade_filters(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        property_id = request.GET['property_id']
        building_id = request.GET['building_id']

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)
        selected_properties = []
        selected_upgrades = []
        children_upgrades_dict = []

        if main_property:
            main_property = main_property[0]

            upgrade_view_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)
            if not upgrade_view_string:
                model_mixin.model_object("Strings").create(english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)
                upgrade_view_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)

            upgrade_view_string = upgrade_view_string[0]

            upgrade_parent_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
            if not upgrade_parent_string:
                model_mixin.model_object("Strings").create(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
                upgrade_parent_string = model_mixin.model_object("Strings").filter(english=settings.UPGRADE_PARENT_PROPERTY_KEY)

            upgrade_parent_string = upgrade_parent_string[0]
            upgrade_children_properties = model_mixin.model_object("PropertiesIntFields").filter(type_string=upgrade_parent_string,
                                                                                                 value=main_property.pk)

            for property_item in upgrade_children_properties:
                selected_upgrades.append(property_item.property_id)
                selected_property = model_mixin.model_object("Properties").filter(pk=property_item.property_id)[0]

                has_upgrade_view = False
                check_upgrade_view = model_mixin.model_object("PropertiesStringFields").filter(type_string=upgrade_view_string,
                                                                                               property_id=selected_property.pk,
                                                                                               value=True)
                if check_upgrade_view:
                    has_upgrade_view = True

                children_upgrades_dict.append({
                    'pk': selected_property.pk,
                    'name': model_mixin.klass("Names").get_names_instance(selected_property.name_id),
                    'property_type': model_mixin.klass("Properties").get_property_type(selected_property.property_type_id),
                    'has_upgrade_view': has_upgrade_view
                })

        floor_list = []
        for floor in model_mixin.klass("Floors").get_all_floors(building_id=building_id):
            apartments_dict = []

            property_list = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')

            for apartment in property_list:
                property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)

                selected_class = False
                if apartment.pk in selected_properties:
                    selected_class = True

                upgrade_selected_class = False
                if apartment.pk in selected_upgrades:
                    upgrade_selected_class = True

                picking_coord_x, picking_coord_y = model_mixin.klass("Properties").get_apartment_hotspot_values(apartment.pk,
                                                                                                                floor_id=floor.pk)

                apartment_kwargs = {'id': apartment.pk,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                    'floor_id': floor.pk,
                                    'property_type': property_type,
                                    'property_type_id': apartment.property_type_id,
                                    'selected_class': selected_class,
                                    'upgrade_selected_class': upgrade_selected_class}

                apartments_dict.append(apartment_kwargs)

            floor_list.append(
                {'floor': floor,
                 'floor_id': floor.pk,
                 'name': model_mixin.klass("Names").get_names_instance(floor.name_id),
                 'apartments_dict': apartments_dict}
            )

        template = "canvas/templates/includes/property_upgrade_details.html"
        context = {'floor_list': floor_list,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'project': model_mixin.project,
                   'children_upgrades_dict': children_upgrades_dict,
                   'property_id': main_property.pk,
                   'request': request,
                   'property_name': model_mixin.klass("Names").get_names_instance(main_property.name_id)}

        combined_html = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'combined_html': combined_html})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_to_combination(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        removed_property_id = request.GET['removed_property_id']

        combined_parent_string = model_mixin.model_object("Strings").filter(
            english=settings.COMBINED_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(
                english=settings.COMBINED_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        check_existing_combination = model_mixin.model_object("PropertiesIntFields").filter(
            type_string=combined_parent_string,
            property_id=removed_property_id,
            value=main_property.pk)
        if check_existing_combination:
            check_existing_combination.delete()

        has_combination = False
        if model_mixin.model_object("PropertiesIntFields").filter(type_string=combined_parent_string,
                                                                    value=property_id):
            has_combination = True

        data = json.dumps({'success': True, 'has_combination': has_combination})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def add_property_to_combination(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        added_property_id = request.GET['added_property_id']

        combined_parent_string = model_mixin.model_object("Strings").filter(
            english=settings.COMBINED_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(
                english=settings.COMBINED_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        check_existing_combination = model_mixin.model_object("PropertiesIntFields").filter(
            type_string=combined_parent_string,
            property_id=added_property_id,
            value=main_property.pk)
        if not check_existing_combination:
            model_mixin.model_object("PropertiesIntFields").create(type_string=combined_parent_string,
                                                                     property_id=added_property_id,
                                                                     value=main_property.pk)

        added_property = model_mixin.model_object("Properties").filter(pk=added_property_id)[0]

        data = json.dumps({'success': True,
                           'child_id': added_property_id,
                           'child_name': model_mixin.klass("Names").get_names_instance(added_property.name_id),
                           'child_property_type': model_mixin.klass("Properties").get_property_type(added_property.property_type_id)})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def toggle_combination_view(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        property_id = request.GET['property_id']
        toggle_value = str(request.GET['toggle_value']).capitalize()

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        combined_view_string = model_mixin.model_object("Strings").filter(
            english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)
        if not combined_view_string:
            model_mixin.model_object("Strings").create(english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)
            combined_view_string = model_mixin.model_object("Strings").filter(
                english=settings.COMBINED_HAS_VIEW_PROPERTY_KEY)

        combined_view_string = combined_view_string[0]
        has_view = model_mixin.model_object("PropertiesStringFields").filter(type_string=combined_view_string,
                                                                               property_id=main_property.pk)

        if not has_view:
            model_mixin.model_object("PropertiesStringFields").create(type_string=combined_view_string,
                                                                        property_id=main_property.pk,
                                                                        value=toggle_value)

        if has_view:
            has_view = has_view[0]
            has_view.value = toggle_value
            has_view.save(using=model_mixin.project.project_connection_name)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def toggle_upgrade_view(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        ####print request.GET

        property_id = request.GET['property_id']
        toggle_value = str(request.GET['toggle_value']).capitalize()

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        upgrade_view_string = model_mixin.model_object("Strings").filter(
            english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)
        if not upgrade_view_string:
            model_mixin.model_object("Strings").create(english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)

            upgrade_view_string = model_mixin.model_object("Strings").filter(
                english=settings.UPGRADE_HAS_VIEW_PROPERTY_KEY)

        upgrade_view_string = upgrade_view_string[0]
        has_view = model_mixin.model_object("PropertiesStringFields").filter(type_string=upgrade_view_string,
                                                                             property_id=main_property.pk)

        if not has_view:
            model_mixin.model_object("PropertiesStringFields").create(type_string=upgrade_view_string,
                                                                      property_id=main_property.pk,
                                                                      value=toggle_value)

        if has_view:
            has_view = has_view[0]
            has_view.value = toggle_value
            has_view.save(using=model_mixin.project.project_connection_name)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_to_upgrade(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        removed_property_id = request.GET['removed_property_id']

        combined_parent_string = model_mixin.model_object("Strings").filter(
            english=settings.UPGRADE_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(
                english=settings.UPGRADE_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        check_existing_upgrade = model_mixin.model_object("PropertiesIntFields").filter(
            type_string=combined_parent_string,
            property_id=removed_property_id,
            value=main_property.pk)
        if check_existing_upgrade:
            check_existing_upgrade.delete()

        has_upgrade = False
        if model_mixin.model_object("PropertiesIntFields").filter(value=property_id,
                                                                  type_string=combined_parent_string):
            has_upgrade = True

        data = json.dumps({'success': True, 'has_upgrade': has_upgrade})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def add_property_to_upgrade(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        added_property_id = request.GET['added_property_id']

        combined_parent_string = model_mixin.model_object("Strings").filter(
            english=settings.UPGRADE_PARENT_PROPERTY_KEY)
        if not combined_parent_string:
            model_mixin.model_object("Strings").create(english=settings.UPGRADE_PARENT_PROPERTY_KEY)
            combined_parent_string = model_mixin.model_object("Strings").filter(
                english=settings.UPGRADE_PARENT_PROPERTY_KEY)

        combined_parent_string = combined_parent_string[0]

        main_property = model_mixin.model_object("Properties").filter(pk=property_id)[0]
        check_existing_upgrade = model_mixin.model_object("PropertiesIntFields").filter(
            type_string=combined_parent_string,
            property_id=added_property_id,
            value=main_property.pk)
        if not check_existing_upgrade:
            model_mixin.model_object("PropertiesIntFields").create(type_string=combined_parent_string,
                                                                     property_id=added_property_id,
                                                                     value=main_property.pk)

        added_property = model_mixin.model_object("Properties").filter(pk=added_property_id)[0]

        data = json.dumps({'success': True,
                           'child_id': added_property_id,
                           'child_name': model_mixin.klass("Names").get_names_instance(added_property.name_id),
                           'child_property_type': model_mixin.klass("Properties").get_property_type(added_property.property_type_id)})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_from_listing(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        removed_property_id = request.GET['removed_property_id']
        check_member_listing = model_mixin.model_object("ListingMembers").filter(property_id=int(removed_property_id))

        try:
            if check_member_listing:
                check_member_listing.delete()
        except:
            for listing in check_member_listing:
                listing.property_item = None
                listing.save(using=model_mixin.project.project_connection_name)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)

def add_property_to_listing(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        added_property_id = request.GET['added_property_id']

        listing_status = model_mixin.model_object("ListingStatus").filter(status="Available")

        if not listing_status:
            model_mixin.model_object("ListingStatus").create(status="Available")
            listing_status = model_mixin.model_object("ListingStatus").filter(status="Available")

        listing_status = listing_status[0]

        model_mixin_listing = model_mixin.model_object("ListingMembers").filter(property_id=property_id)

        if not model_mixin_listing:
            model_mixin.model_object("Listings").create(listing_status=listing_status)
            new_listing = model_mixin.model_object("Listings").filter(listing_status=listing_status).order_by('-pk')[0]
            model_mixin.model_object("ListingMembers").create(listing=new_listing, property_id=property_id)
            model_mixin.model_object("ListingMembers").create(listing=new_listing, property_id=added_property_id)
        else:
            new_listing = model_mixin_listing[0]
            model_mixin.model_object("ListingMembers").filter(listing_id=new_listing.listing_id,
                                                              property_id=added_property_id).delete()
            model_mixin.model_object("ListingMembers").create(listing_id=new_listing.listing_id,
                                                              property_id=added_property_id)

        added_property = model_mixin.model_object("Properties").filter(pk=added_property_id)[0]

        data = json.dumps({
            'success': True,
            'child_id': added_property_id,
            'child_name': model_mixin.klass("Names").get_names_instance(added_property.name_id),
            'child_property_type': model_mixin.klass("Properties").get_property_type(added_property.property_type_id)
        })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)