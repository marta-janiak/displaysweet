from django.http import HttpResponse
import json
from projects.models.model_mixin import PrimaryModelMixin
from django.template.loader import render_to_string


def get_all_images_without_tags(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        image_list = model_mixin.klass("AssetTags").get_assets_without_tags()

        tag_names = model_mixin.model_object("AssetTags").all()
        template = "canvas/includes/tag_filter_images.html"
        context = {'image_list': image_list, 'tag_names': tag_names,
                   'project': model_mixin.project}
        rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_all_images_with_tags(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        image_list = model_mixin.klass("AssetTags").get_all_assets_with_tags()

        tag_names = model_mixin.model_object("AssetTags").all().order_by('tag').exclude(tag='')

        tag_name_list = []
        for tag in tag_names:
            check_assets = model_mixin.model_object("AssetsTagged").filter(asset_tag=tag)
            if check_assets:
                tag_name_list.append({'tag_id': tag.pk, 'tag_name': tag.tag})

        template = "canvas/includes/tag_filter_images.html"
        context = {'image_list': image_list, 'tag_names': tag_names,
                   'project': model_mixin.project,
                   'tag_name_list': tag_name_list}
        rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_images_with_tags(request, pk):

    if request.is_ajax():
        if "tag_name" in request.GET:

            tag_name = request.GET['tag_name']
            model_mixin = PrimaryModelMixin(request=request, project_id=pk)
            image_list = model_mixin.klass("AssetTags").get_assets_with_tag(tag_name)

            print('image_list', image_list)

            tag_names = model_mixin.model_object("AssetTags").all().order_by('tag').exclude(tag='')

            template = "canvas/includes/tag_filter_images.html"
            context = {'image_list': image_list,
                       'tag_names': tag_names,
                       'project': model_mixin.project}

            rendered = render_to_string(template, context)

            data = json.dumps(
                {
                    'success': True,
                    'rendered': rendered
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def save_new_tag_name(request, pk):

    if request.is_ajax():
        if "tag_name" in request.GET:

            model_mixin = PrimaryModelMixin(request=request, project_id=pk)

            success = True

            tag_name = str(request.GET['tag_name']).lstrip().rstrip()
            text_category, created = model_mixin.model_object("AssetTags").get_or_create(tag=tag_name)

            if not created:
                success = False

            data = json.dumps(
                {
                    'success': success
                }
            )

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def save_new_tag_name_to_image(request, pk):

    if request.is_ajax():
        if "tag_name" in request.GET:

            model_mixin = PrimaryModelMixin(request=request, project_id=pk)

            success = True
            if "direction" in request.GET and "tag_name" in request.GET:
                direction = request.GET['direction']

                tag_name = None
                try:
                    tag_id = int(request.GET['tag_name'])
                    tags = model_mixin.model_object("AssetTags").filter(pk=tag_id)
                    if tags:
                        tag_name = tags[0].tag
                except:
                    tag_name = request.GET['tag_name']

                if tag_name:
                    resources = request.GET.getlist('resource_list[]')
                    for resource in resources:
                        model_mixin.klass("Assets").save_image_asset_tags(resource_id=resource,
                                                                          tag_name=tag_name,
                                                                          direction=direction)

                data = json.dumps(
                    {
                        'success': success
                    }
                )

                mimetype = 'application/json'
                return HttpResponse(data, mimetype)


def trash_tag(request, pk, tag_id):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        model_mixin.klass("AssetTags").remove_tag(tag_id=tag_id)
        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)

