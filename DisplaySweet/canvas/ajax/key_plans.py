from django.http import HttpResponse
import json

from projects.models.model_mixin import PrimaryModelMixin
from django.template.loader import render_to_string


def get_key_plans_screen(request, pk, floor_id):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        apartment_list = []
        apartments_dict = []

        floor = model_mixin.model_object("Floors").get(pk=floor_id)
        floor_buildings = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id).values_list('building_id',
                                                                                                            flat=True).distinct()

        for building_id in floor_buildings:
            apartments = model_mixin.model_object("Properties").filter(floor_id=floor.pk).order_by('orderidx')
            floor = model_mixin.model_object("Floors").get(pk=floor.pk)

            if not floor_id:
                floor_id = floor.pk

            for apartment in apartments:
                hotspot_list = model_mixin.klass("Properties").get_hotspot_values(apartment.pk, floor.pk)
                property_type = model_mixin.klass("Properties").get_property_type(apartment.property_type_id)
                property_name = model_mixin.klass("Names").get_names_instance(apartment.name_id)
                key_plan_image_name = model_mixin.klass("Properties").get_image_type_canvas('key_plans', apartment.pk)

                for hotspot in hotspot_list:
                    apartment_kwargs = {'id': apartment.pk,
                                        'floor_hotspot_id': hotspot['floor_hotspot_id'],
                                        'x_coordinate': hotspot['picking_coord_x'],
                                        'y_coordinate': hotspot['picking_coord_y'],
                                        'name': property_name,
                                        'floor_id': int(floor.pk),
                                        'building_id': building_id,
                                        'property_type': property_type,
                                        'property_type_id': apartment.property_type_id,
                                        'key_plan_image_name': key_plan_image_name}

                    apartment_list.append(apartment_kwargs)
                    apartments_dict.append(apartment_kwargs)

        template = 'canvas/templates/includes/apartments/key_plan_apartments.html'

        context = {'apartments_dict': apartments_dict,
                   'project': model_mixin.project,
                   'property_type_list': model_mixin.model_object("PropertyTypes").all(),
                   'request': request}

        key_rendered = render_to_string(template, context)

        data = json.dumps({'apartment_list': apartment_list,
                           'apartments_dict': apartments_dict,
                           'success': True,
                           'key_rendered': key_rendered
                           })

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_key_plan_image(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']
        resource_id = request.GET['resource_id']

        selected_image = model_mixin.model_object("Resources").filter(pk=resource_id)[0]
        proprty_images = model_mixin.model_object("PropertyResources").filter(key="key_plans",
                                                                              property_id=property_id).values_list("resource_id", flat=True)

        if proprty_images:
            images = model_mixin.model_object("Resources").filter(pk__in=proprty_images)

            if images:
                proprty_images = model_mixin.model_object("PropertyResources").filter(key="key_plans",
                                                                                      property_id=property_id)
                proprty_images = proprty_images[0]
                proprty_images.resource_id = selected_image.pk
                proprty_images.save(using=model_mixin.project.project_connection_name)
        else:
            model_mixin.model_object("PropertyResources").create(key="key_plans",
                                                                 property_id=property_id,
                                                                 resource_id=selected_image.pk)

        key_string, created = model_mixin.model_object("Strings").get_or_create(english='key_plans_image_id')

        asset = model_mixin.model_object("Assets").filter(source_id=resource_id)
        if asset:
            model_mixin.model_object("PropertiesIntFields").update_or_create(property_id=property_id,
                                                                             type_string=key_string,
                                                                             defaults={'value': asset[0].pk})

        canvas_id = request.GET['canvas_id']
        model_mixin.klass("Assets").save_resource_to_asset_plans_container("Key Plans",
                                                                           resource_id,
                                                                           canvas_id,
                                                                           None)

        data = json.dumps({
            'success': True
        })
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_key_plan_to_property(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if 'property_id' in request.GET:
            property_id = request.GET['property_id']
            model_mixin.model_object("PropertyResources").filter(property_id=property_id, key="key_plans").delete()

            data = json.dumps({'success': True})
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)