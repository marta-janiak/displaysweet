import sqlite3
import os
import sys
import getopt
import argparse
import hashlib


# Maintenance script that needs to be run every time an asset in a project is changed.
#
# This script recalculates MD5 checksums for all assets in a given project and writes them
#
# into that project's individual database, and calculates the total asset size for the
#
# project to store in the projects.db database.
#
#
# When run as-is, it will perform all maintenance tasks on all projects. Arguments can be
#
# given to specify the type of task required and/or the individual project to be targeted.


def processArguments():
    parser = argparse.ArgumentParser(description='Display Sweet database maintenance')
    parser.add_argument("-n", "--nocommit", help="no database commit, changes will not be visible", action='store_true')
    parser.add_argument("-v", "--verbose", help="verbose output", action='store_true')
    parser.add_argument("-s", "--sizeonly", help="recalculate project sizes only", action='store_true')
    parser.add_argument("-m", "--md5only", help="recalculate MD5 checksums only", action='store_true')
    parser.add_argument("-p", "--projectname", help="target specific project for maintenance", nargs=1)
    parser.add_argument("-l", "--listprojects", help="list all project names but perform no maintenance", action='store_true')
    args = parser.parse_args()
    return args


# Returns the number of bytes of a given project in a given database

def calculateProjectSize(database, projectId, verbose):
    con = sqlite3.connect(database)

    cur = con.cursor()
    # Get the table names

    tableNames = []
    imageIds = []
    cur.execute('SELECT * FROM sqlite_master WHERE type=\'table\';')

    results = cur.fetchall()

    for table in results:
        nameString = str(table[1])
        if nameString != 'images':
            tableNames.append(nameString)

    for tableName in tableNames:
        # Get all the column names

        cur.execute('SELECT * FROM "' + tableName + '"')
        contents = cur.fetchall()
        columnNames = []
        columnIds = []

        for index, columnDesc in enumerate(cur.description):
            columnNames.append(str(columnDesc[0]))
            columnIds.append(index)

        # For each image id column, add all the values to our set
        for index, columnName in enumerate(columnNames):
            if columnName.endswith("image_id"):
                # Have to check for a project_id column and see what number it has
                hasProjectId = False
                if "project_id" in columnNames:
                    hasProjectId = True

                #print(columnName + " from " + tableName
                for row in contents:
                    thisProjectId = projectId
                    if hasProjectId:
                        thisProjectId = row[columnNames.index("project_id")]
                    if str(thisProjectId) == str(projectId):
                        imageIds.append(row[columnIds[index]])

    # Now we have all the image rows that are in use, we get the full set of resource strings
    resources = []
    uniqueIds = set(imageIds)
    cur.execute('SELECT * FROM images')
    imageRows = cur.fetchall()
    for row in imageRows:
        if row[0] in uniqueIds:
            resources.append(str(row[1]))
            resources.append(str(row[3]))
            #resources.append(str(row[4]))
            #resources.append(str(row[8]))

    uniqueResources = sorted(set(resources))

    # Sum up the size of all the resources
    totalSize = 0
    if verbose:
        print("\n\nDatabase " + database + ", project ID " + str(projectId))
    print(database)
    for resource in uniqueResources:
        if verbose:
            print(resource + (("\t\t" + str(os.path.getsize(resource))) if os.path.isfile(resource) else "\t\tFILENOTFOUND"))
        if os.path.isfile(resource):
            #print("size of " + resource + " is " + str(os.path.getsize(resource)/1024)

            totalSize += os.path.getsize(resource)

    con.close()

    if verbose:
        print("TOTAL SIZE: " + str(totalSize))
    return totalSize

# Return the number of bytes of all projects in a given database
def calculateDatabaseSize(database, verbose):
    con = sqlite3.connect(database)
    cur = con.cursor()
    cur.execute("SELECT id FROM projects")
    results = cur.fetchall()
    con.close()

    totalSize = 0
    for result in results:
        for projectId in result:
            totalSize += calculateProjectSize(database, projectId, verbose)
    return totalSize


def hashfile(file, hasher, blocksize=65536):
    if (file == None): return None
    if not os.path.isfile(file):
        if not file == '':
            print(("(%s) does not exist" % (file)))
            return None

    #print('processing ' + file
    afile = open(file, 'rb')
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()

def calculateMd5(database, nocommit):
    con = sqlite3.connect(database)
    cur = con.cursor()
    cur.execute('SELECT * FROM images')
    results = cur.fetchall()

    for image in results:
        thumbnail = image[1]
        hires= image[3]
        fullres = image[4]
        thumb_md5 = hashfile(thumbnail, hashlib.md5())
        hires_md5 = hashfile(hires, hashlib.md5())
        fullres_md5 = hashfile(fullres, hashlib.md5())

        # """if thumb_md5 == image[6]:
        #
        # 	print(("(%s) thumb md5 has not changed" % (thumbnail))
        #
        # if hires_md5 == image[7]:
        #
        # 	print(("(%s) hires md5 has not changed" % (hires))
        #
        # if fullres_md5 == image[8]:
        #
        # 	print(("(%s) fullres md5 has not changed" % (fullres))
        #
         #    #"""

        #print("MD5s Generated: (%s)(%s) - (%s)(%s) - (%s)(%s)" % (image[1], thumb_md5, image[3], hires_md5, image[4], fullres_md5)

        inquery = "UPDATE images SET image_md5 = ?, thumb_md5 = ?, fullres_md5 = ? WHERE id = ?"
        to_db = [hires_md5, thumb_md5, fullres_md5, image[0]]
        cur.execute(inquery, to_db)

    if not nocommit:
        con.commit()
    con.close()


def calculateSizes(database, nocommit):
    conn = sqlite3.connect(database)
    cur = conn.cursor()

    try:
        cur.execute('DROP TABLE image_sizes')
    except Exception as e:

        pass

    cur.execute('CREATE TABLE image_sizes (image_id integer primary key, thumb_size integer, hires_size integer, fullres_size integer)')
    cur.execute('SELECT id, thumbnail_path, fullres_path, highres_path FROM images')
    results = cur.fetchall()

    for image in results:
        thumbSize = 0
        hiresSize = 0
        fullresSize = 0
        if image[1] != None and os.path.exists(image[1]):
            thumbSize = os.path.getsize(image[1])

        if image[2] != None and os.path.exists(image[2]):
            hiresSize = os.path.getsize(image[2])

        if image[3] != None and os.path.exists(image[3]):
            hiresSize = os.path.getsize(image[3])

        cur.execute("INSERT INTO image_sizes (image_id, thumb_size, hires_size, fullres_size) VALUES (?, ?, ?, ?)", [image[0], thumbSize, hiresSize, fullresSize])

    if not nocommit:
        conn.commit()
    conn.close()


if __name__ == '__main__':

    args = processArguments()
    con = sqlite3.connect("projects.db")
    cur = con.cursor()

    if not args.projectname:
        cur.execute("SELECT projects.id, projects.name, projects.ipad_download_size_bytes, images.image_path FROM projects INNER JOIN images ON projects.project_database=images.id")
    else:
        cur.execute("SELECT projects.id, projects.name, projects.ipad_download_size_bytes, images.image_path FROM projects INNER JOIN images ON projects.project_database=images.id WHERE projects.name=\'" + args.projectname[0] + "\'")
    rows = cur.fetchall()

    if args.listprojects:
        print("Listing all project names:")
        for row in rows:
            print(row[1])
        print("Exiting without performing maintenance")
        sys.exit(0)

    if not args.md5only:
        # Calculate the size of each project
        print("Calculating project sizes...")
        for row in rows:
            assetSize = calculateDatabaseSize(row[3], args.verbose)
            inquery = "UPDATE projects SET ipad_download_size_bytes = ? WHERE id = ?"
            to_db = [assetSize, row[0]]
            cur.execute(inquery, to_db)
            print("Project " + row[1] + " now has size " + str(assetSize))

        if not args.nocommit:
            con.commit()
        con.close()
        print("DONE")

    if not args.sizeonly:
        # Calculate MD5s
        databases = []
        for row in rows:
            databases.append(row[3])
        uniqueDatabases = set(databases)
        for database in uniqueDatabases:
            print("Calculating MD5s for " + database)
            calculateMd5(database, args.nocommit)
            print("Calculating image sizes for " + database)
            calculateSizes(database, args.nocommit)
            print("DONE")

