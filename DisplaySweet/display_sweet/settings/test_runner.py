from django.test.runner import DiscoverRunner
from django.conf import settings


class ExcludeAppsTestSuiteRunner(DiscoverRunner):
    """Override the default django 'test' command, exclude from testing
    all apps apart from master as the other."""

    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        if not test_labels:
            # No appnames specified on the command line, so we run all
            # tests, but remove those which we know are troublesome.


            return super(ExcludeAppsTestSuiteRunner, self).\
                run_tests(test_labels, extra_tests, **kwargs)