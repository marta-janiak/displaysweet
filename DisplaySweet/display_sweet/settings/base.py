import os

from unipath import Path


def get_env_variable(var_name):
    """ Get the environment variable or return exception """
    try:
        return os.environ[var_name]
    except KeyError:
        return None

SECRET_KEY = '$6^#k!h7@6cx+_sn!9r+hc9-9de8wt^igc_13@p4y@13kk(dgd'

ENVIRONMENT_SETTINGS = {
    'database_user': get_env_variable('DATABASE_USER'),
    'database_password': get_env_variable('DATABASE_PASSWORD'),
    'secret_key': get_env_variable('SECRET_KEY'),
}


PROJECT_ROOT = Path(__file__).ancestor(3)
REPOSITORY_ROOT = Path(__file__).ancestor(4)
PROJECT_CONTAINER = Path(__file__).ancestor(5)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# ALLOWED_HOSTS = ['*', ]

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                       # Or path to database file if using sqlite3.
        'USER': '',                       # Not used with sqlite3.
        'PASSWORD': '',                   # Not used with sqlite3.
        'HOST': '',                       # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                       # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
# TIME_ZONE = 'Australia/Melbourne'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/str/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

AUTH_USER_MODEL = 'users.User'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

DATE_INPUT_FORMATS = ("%Y-%m-%d", "%Y / %m / %d")

STATIC_ROOT = PROJECT_CONTAINER.child('static')
STATIC_URL = '/static/'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = PROJECT_CONTAINER.child('media')

MEDIA_URL = '/media/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_ROOT.child('static'),
)


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'axes.middleware.FailedLoginMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

TEMPLATE_CONTEXT_PROCESSORS += ("canvas.context_processors.return_canvas_attributes_project", )

ROOT_URLCONF = 'display_sweet.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'display_sweet.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_ROOT.child('templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.webdesign',
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.admin',
    'widget_tweaks',
    'django_extensions',
    'ajax_select',
    'core',
    'users',
    'utils',
    'my_account',
    'projects',
    'clients',
    'canvas',
    'tinymce'
)

########## TEST SETTINGS
TEST_RUNNER = "django.test.runner.DiscoverRunner"


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'filters': {
#         'require_debug_false': {
#             '()': 'django.utils.log.RequireDebugFalse'
#         }
#     },
#     'handlers': {
#         'mail_admins': {
#             'level': 'ERROR',
#             'filters': ['require_debug_false'],
#             'class': 'django.utils.log.AdminEmailHandler'
#         },
#         'print_to_stderr': {
#             'level': 'ERROR',
#             'class': 'logging.StreamHandler'
#         }
#     },
#     'loggers': {
#         'django.request': {
#             'handlers': ['mail_admins'],
#             'level': 'ERROR',
#             'propagate': True,
#         },
#     }
# }

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

# REST_FRAMEWORK = {
#     'DEFAULT_AUTHENTICATION_CLASSES': (
#         'apiv1.authentication.TokenAuthentication',
#         'rest_framework.authentication.SessionAuthentication',
#     )
# }

AUTH_TOKEN_LIFESPAN_SECONDS = 60 * 60 * 24 * 7 * 8  # 8 weeks

AJAX_LOOKUP_CHANNELS = {

}

########## Lockout settings
AXES_LOGIN_FAILURE_LIMIT = 10
AXES_LOCK_OUT_AT_FAILURE = True
AXES_USE_USER_AGENT = False
AXES_COOLOFF_TIME = 24



DDF_DEFAULT_DATA_FIXTURE = 'random'
DATABASE_ROUTERS = []

# SECURITY WARNING: keep the secret key used in production secret!


GRAPPELLI_INDEX_DASHBOARD = 'display_sweet.dashboard.CustomIndexDashboard'
# GRAPPELLI_INDEX_DASHBOARD = {  # alternative method
#     'display_sweet.admin.admin_site': 'display_sweet.dashboard.CustomIndexDashboard',
# }

IMAGE_MIME_TYPES = [
    'image/bmp',
    'image/x-windows-bmp',
    'image/gif',
    'image/jpeg',
    'image/tiff',
    'image/x-tiff',
    'image/png',
]

DOCUMENT_MIME_TYPES = [
    'application/pdf',
    'application/x-pdf',
    'application/acrobat',
    'applications/vnd.pdf',
    'text/pdf',
    'text/x-pdf',
    'application/zip',
    'application/x-zip-compressed',
    'application/mspowerpoint',
    'application/powerpoint',
    'application/x-mspowerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.ms-excel',
    'application/x-excel',
    'application/excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text',
    'application/vnd.oasis.opendocument.spreadsheet',
    'application/vnd.oasis.opendocument.presentation',
    'text/plain',
]

VALID_MIME_TYPES = IMAGE_MIME_TYPES + DOCUMENT_MIME_TYPES

FILE_EXTENSION_WHITELIST = [
    'AI', 'BMP', 'CMP', 'CSV', 'DOC', 'DOCM', 'DOCX', 'EML',
    'EPS', 'GIF', 'HPS', 'HTM', 'JPEG', 'JPG', 'LOG', 'MDI',
    'MHT', 'MSG', 'OFT', 'PDF', 'PNG', 'PPT', 'PPTX', 'RAR',
    'RTF', 'TEXT', 'TIF', 'TIFF', 'TXT', 'WIT', 'XLS', 'XLSX',
    'ZIP', 'ZIPX',
]

DEFAULT_MAX_UPLOAD_SIZE = 10485760
BASE_IMAGE_LOCATION = ''

GRAPPELLI_ADMIN_TITLE = "Admin for The Greatest CMS In The World"

REMOTE_FLOOR_PLAN_URL = 'http://103.245.145.34/'

FLOOR_PLAN_TYPICAL = 'floor_plan_typical'

COMBINED_PARENT_PROPERTY_KEY = 'combined_parent_property_id'
COMBINED_HAS_VIEW_PROPERTY_KEY = 'combined_apartment_has_view'

UPGRADE_PARENT_PROPERTY_KEY = 'upgrade_parent_property_id'
UPGRADE_HAS_VIEW_PROPERTY_KEY = 'upgrade_apartment_has_view'

LISTING_PARENT_PROPERTY_KEY = 'listing_parent_property_id'
LISTING_HAS_VIEW_PROPERTY_KEY = 'listing_apartment_has_view'


aws_access_key_id = "AKIAJXKAXQUFZALERVTA"
aws_secret_access_key = "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"

EMAIL_FROM_ADDRESS = "admin@displaysweet.com"

TRANSLATE_DB_PATH = '/mnt/coredesktop/project_data/databases/'
TRANSLATE_DB_PROJECTDB = '/mnt/liveds/projects_test.db'
TRANSLATE_DB_PATH_REPLACE = '/mnt/coredesktop/'
TRANSLATE_SCRIPT_PATH = '/mnt/coredesktop/project_data/scripts/'

TRANSLATE_LOADING_IMAGES_PATH = '/mnt/coredesktop/project_data/projects/PROJECT_NAME/IMAGES/SETUP/'




EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

SMTP_HOST = 'smtp.sendgrid.net'
SMTP_PORT = '587'
SMTP_USERNAME = 'Vairn'
SMTP_PASSWORD = 'catharsis45'

BUCKETNAME = 'com.displaysweet.content'

SYNC_CMS = True