import os
import socket

from .mapping import DEFAULT_SETTINGS, SETTINGS_MAPPING


def get_settings_from_hostname():
    hostname = socket.gethostname()

    return SETTINGS_MAPPING.get(hostname, DEFAULT_SETTINGS)


def add_settings_to_environ():
    if os.environ.get("DJANGO_SETTINGS_OVERRIDE"):
        os.environ["DJANGO_SETTINGS_MODULE"] = os.environ.get("DJANGO_SETTINGS_OVERRIDE")
    else:
        os.environ["DJANGO_SETTINGS_MODULE"] = get_settings_from_hostname()

