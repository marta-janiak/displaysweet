from .base import *

TEMPLATE_DEBUG = DEBUG
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '/Users/martajaniak/Sites/DisplaySweet/DisplaySweet/display_sweet/databases/sweet_display.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    },
    # 'default': {
    #     'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'display_sweet',                      # Or path to database file if using sqlite3.
    #     'USER': 'postgres',                      # Not used with sqlite3.
    #     'PASSWORD': 'PostgresSQL123',                  # Not used with sqlite3.
    #     'HOST': 'localhost',  # Set to empty string for localhost. Not used with sqlite3.
    #     'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    # },
}


try:
    from .databases import DATABASES as DATABASE_CONNECTIONS
    DATABASES = DATABASES.copy()
    DATABASES.update(DATABASE_CONNECTIONS)
except:
    pass

try:
    from .installed_apps import INSTALLED_APPS as PROJECT_INSTALLED_APPS
    INSTALLED_APPS += PROJECT_INSTALLED_APPS
except Exception as e:
    print ('IN INSTALLED APPS ERROR', e)
    pass




# Additional locations of static files
STATICFILES_DIRS += (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/Users/martajaniak/DS/images/project_data/',
    '/Users/martajaniak/DS/images/',
)

BASE_IMAGE_LOCATION = '/Users/martajaniak/DS/images/'

BASE_S3_IMAGE_LOCATION = '/Users/martajaniak/DS/Images/project_data/'

BASE_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
INTERNAL_IPS = ('127.0.0.1',)
MIDDLEWARE_CLASSES += ()
FILE_URL = 'http://localhost:8000/'


BUCKETNAME = 'com.displaysweet.content'

SYNC_CMS = False