"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'aca_online.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name
import os



class CustomIndexDashboard(Dashboard):

    def init_with_context(self, context):

        self.children.append(modules.ModelList(
            title='User Administration',
            column=1,
            models=('users.models.*',)
        ))

        self.children.append(modules.ModelList(
            title='Project Administration',
            column=1,
            models=('core.models.*', ),
            exclude=('core.models.DisplaySweetConfiguration', 'core.models.ProjectCSVTemplate',
                     'core.models.ProjectCSVTemplateFields',
                    'core.models.ProjectCSVTemplateFieldValues', 'core.models.ProjectCSVTemplateImport',
                    'core.models.BulkImportCompletedTemplate', 'core.models.BulkImport', 'core.models.CSVFileImport',
                    'core.models.CSVFileImportFields')
        ))

        self.children.append(modules.ModelList(
            title='Project Import Administration',
            column=1,
            models=('core.models.ProjectCSVTemplate', 'core.models.ProjectCSVTemplateFields',
                    'core.models.ProjectCSVTemplateFieldValues', 'core.models.ProjectCSVTemplateImport',
                    'core.models.BulkImportCompletedTemplate', 'core.models.BulkImport', 'core.models.CSVFileImport',
                    'core.models.CSVFileImportFields'),
        ))


        self.children.append(modules.ModelList(
            title='Reversion',
            column=1,
            models=('reversion.models.*'),
            exclude=('core.models.*', 'users.models.*', 'django.db.models.*')
        ))

        # for folder in os.listdir('projects'):
        #     self.children.append(modules.ModelList(
        #         title='%s' % folder,
        #         column=1,
        #         models=('projects.%s.models.*' % folder, )
        #     ))