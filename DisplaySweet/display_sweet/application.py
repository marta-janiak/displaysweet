"""
WSGI config for yourmove project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
import site


VIRTUALENV_SITE_PACKAGES_PATH = '/opt/virtualenv/DisplaySweet/lib/python2.7/site-packages'

if VIRTUALENV_SITE_PACKAGES_PATH:
	site.addsitedir(VIRTUALENV_SITE_PACKAGES_PATH)

# Print statements should always go to stderr
sys.stdout = sys.stderr

os.environ["DJANGO_SETTINGS_MODULE"] = "display_sweet.settings.injected"

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(root_dir)

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
