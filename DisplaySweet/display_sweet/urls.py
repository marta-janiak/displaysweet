from django.conf.urls import include, url
from django.contrib import admin
from ajax_select import urls as ajax_select_urls
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [

    url(r'^', include('core.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="logout"),
    url(r'^users/', include('users.urls')),
    url(r'^my_account/', include('my_account.urls')),
    url(r'^clients/', include('clients.urls')),
    url(r'^projects/', include('projects.urls')),
    url(r'^canvas/', include('canvas.urls')),

    url(r'^clone_template/(?P<template_id>\d+)/$', 'canvas.ajax.clone_template', name='clone_template'),
    url(r'^clone_project/$', 'projects.ajax.clone_project', name='clone_project'),

    #Ajax

    url(r'^send_user_email/(?P<project_id>\d+)/(?P<pk>\d+)/$', 'projects.ajax.send_user_email', name='send_user_email'),

    url(r'^create_new_project/$', 'projects.ajax.create_new_project', name='create_new_project'),
    url(r'^delete_selected_project/(?P<pk>\d+)/$', 'projects.ajax.delete_selected_project',
        name='delete_selected_project'),

    url(r'^update_project_details/(?P<pk>\d+)/$', 'projects.ajax.update_project_details',
        name='update_project_details'),

    url(r'^get_floor_hotspots/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_floor_hotspots',
        name='get_floor_hotspots'),

    url(r'^get_level_plans_screen/(?P<pk>\d+)/(?P<building_id>\d+)/$', 'canvas.ajax.get_level_plans_screen',
        name='get_level_plans_screen'),
    url(r'^get_level_plans_screen/(?P<pk>\d+)/(?P<building_id>\d+)/(?P<floor_id>\d+)/$',
        'canvas.ajax.get_level_plans_screen', name='get_level_plans_screen'),

    url(r'^get_floor_plans_screen/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_floor_plans_screen',
        name='get_floor_plans_screen'),
    url(r'^get_key_plans_screen/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_key_plans_screen',
        name='get_key_plans_screen'),
    url(r'^get_details_screen/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_details_screen',
        name='get_details_screen'),

    url(r'^get_floor_apartment_renders/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_floor_apartment_renders',
        name='get_floor_apartment_renders'),

    url(r'^update_property_rendered_image/(?P<pk>\d+)/$', 'canvas.ajax.update_property_rendered_image',
        name='update_property_rendered_image'),

    url(r'^update_property_rendered_typical_image/(?P<pk>\d+)/$', 'canvas.ajax.update_property_rendered_typical_image',
        name='update_property_rendered_typical_image'),

    url(r'^get_property_rendered_fields/(?P<pk>\d+)/$', 'canvas.ajax.get_property_rendered_fields',
        name='get_property_rendered_fields'),

    url(r'^update_property_render_type/(?P<pk>\d+)/$', 'canvas.ajax.update_property_render_type',
        name='update_property_render_type'),

    url(r'^update_property_rendered_individual_image/(?P<pk>\d+)/$', 'canvas.ajax.update_property_rendered_individual_image',
        name='update_property_rendered_individual_image'),

    url(r'^update_property_render/(?P<pk>\d+)/$', 'canvas.ajax.update_property_render',
        name='update_property_render'),

    url(r'^update_property_render_caption/(?P<pk>\d+)/$', 'canvas.ajax.update_property_render_caption',
        name='update_property_render_caption'),

    url(r'^save_new_floor_hotspots/(?P<pk>\d+)/(?P<apartment_id>\d+)/$', 'canvas.ajax.save_new_floor_hotspots',
        name='save_new_floor_hotspots'),
    url(r'^save_new_linked_floor_hotspots/(?P<pk>\d+)/$', 'canvas.ajax.save_new_linked_floor_hotspots',
        name='save_new_linked_floor_hotspots'),

    url(r'^get_images_with_tags/(?P<pk>\d+)/$', 'canvas.ajax.get_images_with_tags',
        name='get_images_with_tags'),

    url(r'^get_all_images_with_tags/(?P<pk>\d+)/$', 'canvas.ajax.get_all_images_with_tags',
        name='get_all_images_with_tags'),

    url(r'^get_all_images_without_tags/(?P<pk>\d+)/$', 'canvas.ajax.get_all_images_without_tags',
        name='get_all_images_without_tags'),

    url(r'^delete_property_render/(?P<pk>\d+)/$', 'canvas.ajax.delete_property_render',
        name='delete_property_render'),

    url(r'^delete_property_render_type/(?P<pk>\d+)/$', 'canvas.ajax.delete_property_render_type',
        name='delete_property_render_type'),

    url(r'^select_all_typicals_for_render/(?P<pk>\d+)/$', 'canvas.ajax.select_all_typicals_for_render',
        name='select_all_typicals_for_render'),

    url(r'^get_floor_apartments/(?P<pk>\d+)/(?P<floor_id>\d+)/$', 'canvas.ajax.get_floor_apartments',
        name='get_floor_apartments'),
    url(r'^get_property_aspects/(?P<pk>\d+)/$', 'canvas.ajax.get_property_aspects', name='get_property_aspects'),
    url(r'^remove_property_aspect/(?P<pk>\d+)/$', 'canvas.ajax.remove_property_aspect',
        name='remove_property_aspect'),
    url(r'^add_property_aspect/(?P<pk>\d+)/$', 'canvas.ajax.add_property_aspect', name='add_property_aspect'),
    url(r'^update_aspect_image/(?P<pk>\d+)/$', 'canvas.ajax.update_aspect_image', name='update_aspect_image'),
    url(r'^update_aspect_image_floor/(?P<pk>\d+)/$', 'canvas.ajax.update_aspect_image_floor',
        name='update_aspect_image_floor'),

    url(r'^copy_resource_image_to_floors/(?P<pk>\d+)/$', 'projects.ajax.copy_resource_image_to_floors',
        name='copy_resource_image_to_floors'),
    url(r'^copy_resource_image_to_properties/(?P<pk>\d+)/$', 'projects.ajax.copy_resource_image_to_properties',
        name='copy_resource_image_to_properties'),

    url(r'^update_aspect_caption/(?P<pk>\d+)/$', 'canvas.ajax.update_aspect_caption', name='update_aspect_caption'),

    url(r'^get_project_model_filter/(?P<project_id>\d+)/(?P<model_name>\w+)/$',
        'projects.ajax.get_project_model_filter', name='get_project_model_filter'),

    url(r'^copy_apartment_to_floor/(?P<pk>\d+)/(?P<property_id>\d+)/(?P<floor_id>\d+)/$',
        'canvas.ajax.copy_apartment_to_floor', name='copy_apartment_to_floor'),

    url(r'^save_image_path/(?P<pk>\d+)/(?P<path_type>\w+)/$', 'projects.ajax.save_image_path', name='save_image_path'),
    url(r'^save_image_name/(?P<pk>\d+)/(?P<floor_id>\w+)/(?P<image_type>\w+)/$', 'projects.ajax.save_image_name',
        name='save_image_name'),
    url(r'^import_images/(?P<pk>\d+)/$', 'projects.ajax.import_images', name='import_images'),
    url(r'^import_images_ns/(?P<pk>\d+)/$', 'projects.ajax.import_images_ns', name='import_images_ns'),
    url(r'^remove_image_from_path/(?P<pk>\d+)/$', 'projects.ajax.remove_image_from_path',
        name='remove_image_from_path'),

    url(r'^get_image_selection_details/(?P<pk>\d+)/(?P<floor_id>\w+)/$', 'canvas.ajax.get_image_selection_details',
        name='get_image_selection_details'),
    url(r'^get_image_selection_details/(?P<pk>\d+)/(?P<floor_id>\w+)/(?P<image_type>\w+)/$',
        'canvas.ajax.get_image_selection_details', name='get_image_selection_details'),

    url(r'^remove_floor_plan_to_property/(?P<pk>\d+)/$', 'canvas.ajax.remove_floor_plan_to_property',
        name='remove_floor_plan_to_property'),
    url(r'^remove_level_plan_to_floor/(?P<pk>\d+)/$', 'canvas.ajax.remove_level_plan_to_floor',
        name='remove_level_plan_to_floor'),

    url(r'^remove_key_plan_to_property/(?P<pk>\d+)/$', 'canvas.ajax.remove_key_plan_to_property',
        name='remove_key_plan_to_property'),

    url(r'^get_level_plan_details/(?P<pk>\d+)/(?P<floor_id>\w+)/$', 'canvas.ajax.get_level_plan_details',
        name='get_level_plan_details'),

    url(r'^get_table_mapping_options/(?P<pk>\d+)/$', 'projects.ajax.get_table_mapping_options',
        name='get_table_mapping_options'),

    url(r'^update_template_status/(?P<pk>\d+)/$', 'canvas.ajax.update_template_status', name='update_template_status'),
    url(r'^update_template_default_status/(?P<pk>\d+)/$', 'canvas.ajax.update_template_default_status',
        name='update_template_default_status'),
    url(r'^update_template_allocation_status/(?P<pk>\d+)/$', 'canvas.ajax.update_template_allocation_status',
        name='update_template_allocation_status'),

    url(r'^clone_template/(?P<template_id>\d+)/$', 'canvas.ajax.clone_template', name='clone_template'),
    url(r'^save_new_tag_name/(?P<pk>\d+)/$', 'canvas.ajax.save_new_tag_name', name='save_new_tag_name'),
    url(r'^save_new_tag_name_to_image/(?P<pk>\d+)/$', 'canvas.ajax.save_new_tag_name_to_image',
        name='save_new_tag_name_to_image'),
    url(r'^trash_tag/(?P<pk>\d+)/(?P<tag_id>\d+)/$', 'canvas.ajax.trash_tag', name='trash_tag'),
    url(r'^remove_wireframes/(?P<pk>\d+)/$', 'canvas.ajax.remove_wireframes', name='remove_wireframes'),
    url(r'^remove_containers/(?P<pk>\d+)/$', 'canvas.ajax.remove_containers', name='remove_containers'),
    url(r'^update_add_template_names/(?P<pk>\d+)/$', 'canvas.ajax.update_add_template_names',
        name='update_add_template_names'),
    url(r'^save_canvas_name/(?P<pk>\d+)/$', 'canvas.ajax.save_canvas_name', name='save_canvas_name'),
    url(r'^delete_image/(?P<pk>\d+)/$', 'canvas.ajax.delete_image', name='delete_image'),

    url(r'^delete_canvas/(?P<pk>\d+)/(?P<canvas_id>\d+)/$', 'canvas.ajax.delete_canvas', name='delete_canvas'),
    url(r'^delete_wireframe_for_canvas/(?P<pk>\d+)/(?P<canvas_id>\d+)/$', 'canvas.ajax.delete_wireframe_for_canvas',
        name='delete_wireframe_for_canvas'),

    url(r'^get_editable_property_template/(?P<pk>\d+)/$', 'canvas.ajax.get_editable_property_template',
        name='get_editable_property_template'),
    url(r'^save_new_property_editable_template/(?P<pk>\d+)/$', 'canvas.ajax.save_new_property_editable_template',
        name='save_new_property_editable_template'),

    url(r'^save_new_property_names/(?P<pk>\d+)/(?P<floor_id>\d+)/(?P<building_id>\d+)/$', 'canvas.ajax.save_new_property_names',
        name='save_new_property_names'),

    url(r'^delete_property/(?P<pk>\d+)/$', 'canvas.ajax.delete_property', name='delete_property'),
    url(r'^save_new_floor_level/(?P<pk>\d+)/(?P<building_id>\d+)/$', 'canvas.ajax.save_new_floor_level',
        name='save_new_floor_level'),
    url(r'^delete_level/(?P<pk>\d+)/$', 'canvas.ajax.delete_level', name='delete_level'),

    url(r'^add_new_property_typical_string/(?P<pk>\d+)/$', 'canvas.ajax.add_new_property_typical_string',
        name='add_new_property_typical_string'),
    url(r'^delete_image_string_typical/(?P<pk>\d+)/$', 'canvas.ajax.delete_image_string_typical',
        name='delete_image_string_typical'),
    url(r'^assign_typical_to_floor/(?P<pk>\d+)/$', 'canvas.ajax.assign_typical_to_floor',
        name='assign_typical_to_floor'),
    url(r'^assign_typical_to_building/(?P<pk>\d+)/$', 'canvas.ajax.assign_typical_to_building',
        name='assign_typical_to_building'),
    url(r'^save_image_selector_files/(?P<pk>\d+)/$', 'canvas.ajax.save_image_selector_files',
        name='save_image_selector_files'),

    url(r'^upload_image_files/(?P<pk>\d+)/$', 'projects.ajax.upload_image_files',
        name='upload_image_files'),

    url(r'^get_new_image_list/(?P<pk>\d+)/$', 'projects.ajax.get_new_image_list',
        name='get_new_image_list'),

    url(r'^copy_key_plan_to_floor/(?P<pk>\d+)/$', 'canvas.ajax.copy_key_plan_to_floor',
        name='copy_key_plan_to_floor'),

    url(r'^save_user_to_project/$', 'projects.ajax.save_user_to_project',
        name='save_user_to_project'),

    url(r'^save_user_to_project_without_sync/$', 'projects.ajax.save_user_to_project_without_sync',
        name='save_user_to_project_without_sync'),

    url(r'^remove_user_from_project/$', 'projects.ajax.remove_user_from_project',
        name='remove_user_from_project'),

    url(r'^get_plan_html/(?P<pk>\d+)/$', 'canvas.ajax.get_plan_html', name='get_plan_html'),
    url(r'^update_level_plan_image/(?P<pk>\d+)/$', 'canvas.ajax.update_level_plan_image',
        name='update_level_plan_image'),
    url(r'^update_floor_plan_image/(?P<pk>\d+)/$', 'canvas.ajax.update_floor_plan_image',
        name='update_floor_plan_image'),
    url(r'^update_key_plan_image/(?P<pk>\d+)/$', 'canvas.ajax.update_key_plan_image', name='update_key_plan_image'),
    url(r'^update_type_image/(?P<pk>\d+)/$', 'canvas.ajax.update_type_image', name='update_type_image'),
    url(r'^add_typical_string_to_property/(?P<pk>\d+)/$', 'canvas.ajax.add_typical_string_to_property',
        name='add_typical_string_to_property'),

    url(r'^display/suite/(?P<project_id>\d+)/(?P<canvas_id>\d+)/select/$', 'canvas.views.wireframes_select_preview',
        name='canvas-wireframe-select-preview'),
    url(r'^display/suite/(?P<project_id>\d+)/(?P<canvas_id>\d+)/(?P<wireframe_id>\d+)/$',
        'canvas.views.wireframes_preview', name='canvas-wireframe-preview'),
    url(
        r'^display/suite/(?P<project_id>\d+)/(?P<canvas_id>\d+)/(?P<wireframe_id>\d+)/(?P<selected_sub_wireframe>\d+)/$',
        'canvas.views.wireframes_preview', name='canvas-wireframe-preview'),

    # url(r'^reset_password/$', 'django.contrib.auth.views.password_reset', name="password_reset"),
    url(r'^reset_password/$', 'core.views.reset_password', name="password_reset"),

    url(r'^reset_password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    url(r'^reset_password/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset_password/complete/$', 'django.contrib.auth.views.password_reset_complete',
        name='password_reset_complete'),

    # combination settings
    url(r'^add_property_to_combination/(?P<pk>\d+)/$', 'canvas.ajax.add_property_to_combination',
        name='add_property_to_combination'),
    url(r'^remove_property_to_combination/(?P<pk>\d+)/$', 'canvas.ajax.remove_property_to_combination',
        name='remove_property_to_combination'),

    url(r'^get_combination_filters/(?P<pk>\d+)/$', 'canvas.ajax.get_combination_filters',
        name='get_combination_filters'),
    url(r'^get_upgrade_filters/(?P<pk>\d+)/$', 'canvas.ajax.get_upgrade_filters', name='get_upgrade_filters'),

    url(r'^add_property_to_upgrade/(?P<pk>\d+)/$', 'canvas.ajax.add_property_to_upgrade',
        name='add_property_to_upgrade'),
    url(r'^remove_property_to_upgrade/(?P<pk>\d+)/$', 'canvas.ajax.remove_property_to_upgrade',
        name='remove_property_to_upgrade'),

    url(r'^publish_canvas_template/(?P<pk>\d+)/$', 'canvas.ajax.publish_canvas_template',
        name='publish_canvas_template'),

    url(r'^get_canvas_wireframe_template_assets/(?P<pk>\d+)/$', 'canvas.ajax.get_canvas_wireframe_template_assets',
        name='get_canvas_wireframe_template_assets'),

    url(r'^get_canvas_wireframe_template_published_assets/(?P<pk>\d+)/$', 'canvas.ajax.get_canvas_wireframe_template_published_assets',
        name='get_canvas_wireframe_template_published_assets'),

    url(r'^torch_canvas_content_table/(?P<pk>\d+)/$', 'canvas.ajax.torch_canvas_content_table',
        name='torch_canvas_content_table'),

    url(r'^save_project_publish_path/(?P<pk>\d+)/$', 'canvas.ajax.save_project_publish_path',
        name='save_project_publish_path'),

    url(r'^add_property_to_listing/(?P<pk>\d+)/$', 'canvas.ajax.add_property_to_listing',
        name='add_property_to_listing'),
    url(r'^remove_property_from_listing/(?P<pk>\d+)/$', 'canvas.ajax.remove_property_from_listing',
        name='remove_property_from_listing'),
    url(r'^delete_building/(?P<pk>\d+)/$', 'projects.ajax.delete_building', name='delete_building'),

    url(r'^get_listing_details/(?P<pk>\d+)/$', 'canvas.ajax.get_listing_details', name='get_listing_details'),
    url(r'^save_floor_buildings/(?P<pk>\d+)/$', 'projects.ajax.save_floor_buildings', name='save_floor_buildings'),
    url(r'^get_listing_floor_select/(?P<pk>\d+)/(?P<building_id>\d+)/$', 'canvas.ajax.get_listing_floor_select',
        name='get_listing_floor_select'),

    url(r'^save_building_name/(?P<pk>\d+)/$', 'projects.ajax.save_building_name', name='save_building_name'),
    url(r'^toggle_combination_view/(?P<pk>\d+)/$', 'canvas.ajax.toggle_combination_view',
        name='toggle_combination_view'),
    url(r'^toggle_upgrade_view/(?P<pk>\d+)/$', 'canvas.ajax.toggle_upgrade_view', name='toggle_upgrade_view'),
    url(r'^delete_hotspot/(?P<pk>\d+)/(?P<hotspot_id>\d+)/$', 'canvas.ajax.delete_hotspot', name='delete_hotspot'),
    url(r'^get_floor_apartments_html/(?P<pk>\d+)/$', 'canvas.ajax.get_floor_apartments_html',
        name='get_floor_apartments_html'),

    # Allocations
    url(r'^get_project_allocated_search_results/(?P<pk>\d+)/$', 'projects.ajax.get_project_allocated_search_results',
        name='get_project_allocated_search_results'),

    url(r'^get_project_allocated_users/(?P<pk>\d+)/$', 'projects.ajax.get_project_allocated_users',
        name='get_project_allocated_users'),
    url(r'^save_user_to_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.save_user_to_allocation_group',
        name='save_user_to_allocation_group'),

    url(r'^remove_user_from_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.remove_user_from_allocation_group',
        name='remove_user_from_allocation_group'),

    url(r'^remove_project_script/(?P<pk>\d+)/$', 'projects.ajax.remove_project_script', name='remove_project_script'),

    url(r'^change_project_selected_script/(?P<pk>\d+)/$', 'projects.ajax.change_project_selected_script',
        name='change_project_selected_script'),

    url(r'^change_project_version_active/(?P<pk>\d+)/$', 'projects.ajax.change_project_version_active',
        name='change_project_version_active'),

    url(r'^get_property_reservation/(?P<pk>\d+)/$', 'projects.ajax.get_property_reservation',
        name='get_property_reservation'),
    url(r'^get_agent_for_reservation/(?P<pk>\d+)/$', 'projects.ajax.get_agent_for_reservation',
        name='get_agent_for_reservation'),
    url(r'^remove_property_reservation_details/(?P<pk>\d+)/$', 'projects.ajax.remove_property_reservation_details',
        name='remove_property_reservation_details'),

    url(r'^get_project_allocated_properties/(?P<pk>\d+)/$', 'projects.ajax.get_project_allocated_properties',
        name='get_project_allocated_properties'),
    url(r'^get_project_all_properties/(?P<pk>\d+)/$', 'projects.ajax.get_project_all_properties',
        name='get_project_all_properties'),

    url(r'^save_property_to_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.save_property_to_allocation_group',
        name='save_property_to_allocation_group'),

    url(r'^get_allocations_property_template/(?P<pk>\d+)/$', 'projects.ajax.get_allocations_property_template',
        name='get_allocations_property_template'),

    url(r'^update_csv_import_details/(?P<pk>\d+)/$', 'projects.ajax.update_csv_import_details',
        name='update_csv_import_details'),

    url(r'^update_project_data_allocation/(?P<pk>\d+)/$', 'projects.ajax.update_project_data_allocation',
        name='update_project_data_allocation'),

    url(r'^get_data_comparison_csv_import/(?P<pk>\d+)/$', 'projects.ajax.get_data_comparison_csv_import',
        name='get_data_comparison_csv_import'),

    url(r'^remove_property_from_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.remove_property_from_allocation_group',
        name='remove_property_from_allocation_group'),

    url(r'^add_property_to_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.add_property_to_allocation_group',
        name='add_property_to_allocation_group'),

    url(r'^update_property_to_reserved/(?P<pk>\d+)/$', 'projects.ajax.update_property_to_reserved',
        name='update_property_to_reserved'),

    url(r'^update_property_to_exclusive/(?P<pk>\d+)/$', 'projects.ajax.update_property_to_exclusive',
        name='update_property_to_exclusive'),

    url(r'^remove_property_from_exclusive/(?P<pk>\d+)/$', 'projects.ajax.remove_property_from_exclusive',
        name='remove_property_from_exclusive'),

    url(r'^save_new_user_form/(?P<pk>\d+)/$', 'projects.ajax.save_new_user_form',
        name='save_new_user_form'),

    url(r'^delete_allocation_group/(?P<pk>\d+)/$', 'projects.ajax.delete_allocation_group',
        name='delete_allocation_group'),

    url(r'^update_edited_allocation_group_name/(?P<pk>\d+)/$', 'projects.ajax.update_edited_allocation_group_name',
        name='update_edited_allocation_group_name'),

    url(r'^save_property_order/(?P<pk>\d+)/$', 'canvas.ajax.save_property_order',
        name='save_property_order'),

    url(r'^save_floor_order/(?P<pk>\d+)/$', 'canvas.ajax.save_floor_order',
        name='save_floor_order'),

    url(r'^change_project_version_status/(?P<pk>\d+)/$', 'projects.ajax.change_project_version_status',
        name='change_project_version_status'),

    url(r'^change_user_owner/$', 'users.ajax.change_user_owner',
        name='change_user_owner'),

    url(r'^get_new_project_version/(?P<pk>\d+)/$', 'projects.ajax.get_new_project_version',
        name='get_new_project_version'),

    url(r'^save_version_script/(?P<pk>\d+)/$', 'projects.ajax.save_version_script',
        name='save_version_script'),

    url(r'^save_version_comment/(?P<pk>\d+)/$', 'projects.ajax.save_version_comment',
        name='save_version_comment'),

    url(r'^start_version_translation/(?P<pk>\d+)/(?P<family_id>\d+)/$', 'projects.ajax.start_version_translation',
        name='start_version_translation'),

    url(r'^save_project_translation/(?P<pk>\d+)/(?P<family_id>\d+)/$', 'projects.ajax.save_project_translation',
        name='save_project_translation'),

    url(r'^get_translation_form_for_project/(?P<pk>\d+)/(?P<family_id>\d+)/$', 'projects.ajax.get_translation_form_for_project',
        name='get_translation_form_for_project'),

    url(r'^upload_user_details/$', 'projects.ajax.upload_user_details',
        name='upload_user_details'),

    url(r'^get_project_search_details/$', 'users.ajax.get_project_search_details',
        name='get_project_search_details'),

    url(r'^get_project_search_list/$', 'users.ajax.get_project_search_list',
        name='get_project_search_list'),

    url(r'^import_user_details/(?P<pk>\d+)/$', 'projects.ajax.import_user_details',
        name='import_user_details'),

    url(r'^get_user_allocation_list/(?P<pk>\d+)/$', 'projects.ajax.get_user_allocation_list',
        name='get_user_allocation_list'),

    url(r'^get_allocation_search_list/(?P<pk>\d+)/$', 'projects.ajax.get_allocation_search_list',
        name='get_allocation_search_list'),

    url(r'^save_projects_to_family/$', 'projects.ajax.save_projects_to_family',
        name='save_projects_to_family'),

    url(r'^remove_projects_from_family/$', 'projects.ajax.remove_projects_from_family',
        name='remove_projects_from_family'),

    url(r'^remove_project_family/$', 'projects.ajax.remove_project_family',
        name='remove_project_family'),

    url(r'^email/(?P<pk>\w+)/$', 'users.views.email_preview', name='email-preview'),
    url(r'^email/account/(?P<pk>\w+)/$', 'users.views.user_email_preview', name='user-email-preview'),
    url(r'^account/setup/$', 'users.views.user_account_setup', name='user-account-setup'),
    url(r'^account/password/update/$', 'users.views.user_account_setup', name='user-account-setup'),

    url(r'^tinymce/', include('tinymce.urls')),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)