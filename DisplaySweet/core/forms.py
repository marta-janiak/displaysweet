from django import forms
from users.models import User


class FilterForm(forms.Form):
    default_prefix = 'filter'
    placeholders = None

    def __init__(self, *args, **kwargs):
        if 'prefix' not in kwargs:
            kwargs['prefix'] = self.default_prefix

        result = super(FilterForm, self).__init__(*args, **kwargs)

        if self.placeholders:
            for field_name, placeholder in self.placeholders.items():
                self.fields[field_name].widget.attrs['placeholder'] = placeholder

        return result


class PasswordResetForm(forms.Form):
    email_address = forms.EmailField(label="Email address")

    def clean(self):

        cleaned_data = self.cleaned_data
        email_address = cleaned_data.get('email_address')
        if email_address:
            check_user = User.objects.filter(email=email_address)
            #print 'check_user', check_user
            if not check_user:
                self._errors['email_address'] = self.error_class(['This email address is not associated with an account in the CMS.'])

        return cleaned_data

    def send_email(self):

        cleaned_data = self.cleaned_data
        email_address = cleaned_data.get('email_address')
        if email_address:
            check_user = User.objects.get(email=email_address)
            check_user.send_password_email()
