from django.contrib import admin

from core.models import *
import shutil
import os
from projects.cms_sync import ProjectCMSSync


class ProjectAdmin(admin.ModelAdmin):

    model = Project
    list_display = ('id', 'name', 'version', 'project_connection_name', 'project_copied_from',
                    'number_of_buildings', 'created', 'active', 'version_family', 'status')
    readonly_fields = ('installed',)

    list_filter = ('project_copied_from', 'number_of_buildings', 'created', 'active', 'version_family', 'status')
    search_fields = ('name', 'project_connection_name', 'id')

    actions = ('copy_master_to_project', 'create_project_mapping',
               'refresh_all_connections', 'remove_project_folders', 'clean_models_file',
               'update_inprogress_to_staging',
               'refresh_installed_apps')

    def update_inprogress_to_staging(modeladmin, request, queryset):
        for item in queryset:
            if item.status == "in_progress":
                item.status = "staging"
                item.save()

    update_inprogress_to_staging.short_description = 'In progress to Staging'

    def copy_master_to_project(modeladmin, request, queryset):
        for item in queryset:
            shutil.copy2('projects/project_folders/master/models.py',
                         'projects/project_folders/%s/models.py' % item.project_connection_name)

            shutil.copy2('projects/project_folders/master/models_file.py',
                         'projects/project_folders/%s/models_file.py' % item.project_connection_name)

            for connection in item.project_connections.all():
                connection.map_project_to_installed_apps()
    copy_master_to_project.short_description = 'Fix models file from master'

    def create_project_mapping(modeladmin, request, queryset):

        for item in queryset:
            for connection in item.project_connections.all():
                try:
                    connection.run_project_mapping()
                except Exception as e:
                    print(e)

    create_project_mapping.short_description = 'Create project mappings'

    def refresh_installed_apps(modeladmin, request, queryset):
        #rewrite all connection files and remove folder
        project_connections = ProjectConnection.objects.all()
        if project_connections:
            for connection in project_connections:
                connection.map_project_to_installed_apps()

    refresh_installed_apps.short_description = 'Add Project to Installed Apps'

    def clean_models_file(modeladmin, request, queryset):

        for item in queryset:
            for connection in item.project_connections.all():
                print('connection', connection)
                connection.map_database_models(skip_mapping=True)

    clean_models_file.short_description = 'Clean model file'

    def refresh_all_connections(modeladmin, request, queryset):
        #rewrite all connection files and remove folder
        project_connections = ProjectConnection.objects.all()
        if project_connections:
            for connection in project_connections:
                connection.map_project_to_installed_apps()
                connection.map_database_connection()

    refresh_all_connections.short_description = 'After deleting a project, refresh all connections.'

    def remove_project_folders(modeladmin, request, queryset):
        projects = Project.objects.all()
        project_list =['templatetags', 'templates']

        for project in projects:
            project_list.append(project.project_connection_name)

        for folder in os.listdir('projects'):
            if os.path.isdir(os.path.join('projects', folder)):
                folder_item = os.path.join('projects', folder)
                for item in os.listdir(folder_item):
                    if os.path.isdir(os.path.join(folder_item, item)) and item not in project_list:
                        try:
                            shutil.rmtree('projects/projects_folders/%s/' % item)
                        except:
                            pass
    remove_project_folders.short_description = 'Remove redundant project folders.'


class ProjectConnectionAdmin(admin.ModelAdmin):

    model = ProjectConnection
    list_display = ('id', 'project', 'connection_type', 'path_to_database', 'username', )
    list_filter = ('project', 'connection_type')


class ImageLocationConfigurationAdmin(admin.StackedInline):
    model = ImageLocationConfiguration
    readonly_fields = ('modified',)
    extra = 0


class DisplaySweetConfigurationAdmin(admin.ModelAdmin):

    model = DisplaySweetConfiguration
    list_display = ('id', )
    readonly_fields = ('modified',)
    exclude = ('image_locations',)

    inlines = [ImageLocationConfigurationAdmin]


class ProjectCSVTemplateAdmin(admin.ModelAdmin):
    model = ProjectCSVTemplate
    # inlines = [ProjectCSVTemplateFieldsAdmin,]
    list_display = ('id', 'project', 'template_name')
    list_filter = ('project',)


class ProjectCSVTemplateFieldValuesAdmin(admin.ModelAdmin):
    model = ProjectCSVTemplateFieldValues
    list_display = ('id', 'row_id', 'field', 'value')
    search_fields = ('id', 'row_id', 'value')


class ProjectCSVTemplateImportsAdmin(admin.ModelAdmin):
    model = ProjectCSVTemplateImport
    list_display = ('id', 'template', 'created')
    list_filter = ('template__project',)


class CSVFileImportFieldsAdmin(admin.StackedInline):
    model = CSVFileImportFields
    extra = 0


class CSVFileImportAdmin(admin.ModelAdmin):
    model = CSVFileImport
    inlines = [CSVFileImportFieldsAdmin,]
    list_display = ('id', 'project', 'created', 'csv_file')
    list_filter = ('project',)


class BulkImportAdmin(admin.ModelAdmin):
    model = BulkImport
    list_display = ('id', 'project', 'table_name', 'uploaded_xlsx')


class BulkImportCompletedTemplateAdmin(admin.ModelAdmin):
    model = BulkImportCompletedTemplate
    list_display = ('id', 'project', 'template', 'uploaded_xlsx')


class ProjectCSVTemplateFieldsAdmin(admin.ModelAdmin):
    model = ProjectCSVTemplateFields
    list_display = ('id', 'template', 'column_table_mapping', 'column_field_mapping')
    list_filter = ('template', )


class ProjectSyncBackLogAdmin(admin.ModelAdmin):
    model = ProjectSyncBackLog
    list_display = ('id', 'project', 'status_log', 'log',  'created')
    list_filter = ('project', 'logged')
    search_fields = ('status_log', 'log', )

    def has_delete_permission(self, request, obj=None):
        return False

    actions = ('sync_logs', )

    def sync_logs(modeladmin, request, queryset):
        new_sync = ProjectCMSSync()
        for item in queryset:
            if item.log:
                new_log = str(item.log).replace("'' ", "''")
                new_sync.update_syncdb(item.project.pk, new_log)

    sync_logs.short_description = 'Re-Sync these logs'


class ProjectTranslationsAdmin(admin.ModelAdmin):
    model = ProjectTranslations
    list_display = ('id', 'project', 'created', 'database_id', 'version_published', 'published_to', 'post_details_log')
    list_filter = ('project', 'published_to', 'created_by', 'version_published')
    readonly_fields = ('project', 'created_by', 'created', 'modified', 'post_details_log')

    def has_delete_permission(self, request, obj=None):
        return False


class ProjectVersionAdmin(admin.ModelAdmin):
    model = ProjectVersion
    list_display = ('id', 'project', 'active_version')
    list_filter = ('project', 'active_version', 'project__active')


class ProjectFamilyAdmin(admin.ModelAdmin):
    model = ProjectFamily
    list_display = ('id', 'created', 'created_by', 'family_name')
    search_fields = ('family_name', )


admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectConnection, ProjectConnectionAdmin)
admin.site.register(CSVFileImport, CSVFileImportAdmin)
admin.site.register(ProjectFamily, ProjectFamilyAdmin)

admin.site.register(ProjectChangeLog)
admin.site.register(ProjectNotes)
admin.site.register(ProjectVersion, ProjectVersionAdmin)
admin.site.register(ProjectScripts)
admin.site.register(ProjectTranslations, ProjectTranslationsAdmin)

admin.site.register(ProjectCSVTemplate, ProjectCSVTemplateAdmin)
admin.site.register(ProjectCSVTemplateImport, ProjectCSVTemplateImportsAdmin)

admin.site.register(BulkImport, BulkImportAdmin)
admin.site.register(BulkImportCompletedTemplate, BulkImportCompletedTemplateAdmin)
admin.site.register(ProjectSyncBackLog, ProjectSyncBackLogAdmin)
admin.site.register(ProjectCSVTemplateFields, ProjectCSVTemplateFieldsAdmin)

admin.site.register(ProjectCSVTemplateFieldValues, ProjectCSVTemplateFieldValuesAdmin)
# admin.site.register(DisplaySweetConfiguration, DisplaySweetConfigurationAdmin)
