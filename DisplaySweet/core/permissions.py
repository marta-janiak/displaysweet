from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
import os
from django.conf import settings
import imghdr
from django.forms.models import model_to_dict


class ClientPermissionsMixin(object):
    """
    All methods relating to permissions go on this mixin!
    """
    def can_be_viewed_in_api_by(self, user):
        if user.is_administrator:
            return True

        return False