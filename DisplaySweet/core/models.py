from django.db import models
import os
import sqlite3
import subprocess
import json
import sys
import re
import time
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.conf import settings

import fileinput
import tempfile
import shutil
from django.apps import apps
from django.db import connection
from django.utils.encoding import smart_str

from projects.permissions import ProjectsPermissionsMixin, ProjectModelMixin
from utils.secure_file_field import SecureFileField
from utils.json_field import JSONField
import xlrd
import csv
from django.core.files import File


class Project(ProjectsPermissionsMixin, ProjectModelMixin, models.Model):

    LIVE = 'live'
    STAGING = 'staging'
    NOT_ACTIVE = 'not_active'
    TEST = 'testing'
    IN_PROGRESS = 'in_progress'

    STATUS_OPTIONS = (
        (NOT_ACTIVE, "Inactive"),
        (STAGING, "Staging"),
        (LIVE, "Live"),
    )

    created_by = models.ForeignKey("users.User", null=True, blank=True)
    last_modified_by = models.ForeignKey("users.User", null=True, blank=True, related_name="modified_by")

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True, auto_now_add=True)

    name = models.CharField(max_length=50, null=True, blank=True, help_text="Enter a project name.")
    project_connection_name = models.CharField(max_length=50, unique=True, null=True, blank=True, help_text="Enter a project connection name. This must be unique with no spaces.")
    description = models.TextField(null=True, blank=True, help_text="Enter a project description.")

    installed = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    users = models.ManyToManyField('users.User', blank=True, related_name='project_users_list')

    number_of_buildings = models.IntegerField(default=1)

    street_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street number", help_text="Enter the street number.")
    street_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street name", help_text="Enter the street name.")
    street_type = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street type", help_text="Enter the street type.")
    street_suffix = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street suffix", help_text="Enter the street suffix.")
    suburb = models.CharField(max_length=255, null=True, blank=True, verbose_name="Suburb", help_text="Enter the suburb.")
    postcode = models.CharField(max_length=255, null=True, blank=True, verbose_name="Postcode", help_text="Enter the postcode.")
    state = models.CharField(max_length=255, default="VIC", verbose_name="State", help_text="Enter the state.")

    postal_address_same_as_physical = models.BooleanField(default=True)

    postal_street_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street number", help_text="Enter the postal address street number.")
    postal_street_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street name", help_text="Enter the postal address street name.")
    postal_street_type = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street type", help_text="Enter the postal address street type.")
    postal_street_suffix = models.CharField(max_length=255, null=True, blank=True, verbose_name="Street suffix", help_text="Enter the postal address street suffix.")
    postal_suburb = models.CharField(max_length=255, null=True, blank=True, verbose_name="Suburb", help_text="Enter the postal address suburb.")
    postal_postcode = models.CharField(max_length=255, null=True, blank=True, verbose_name="Postcode", help_text="Enter the postal address postcode.")
    postal_state = models.CharField(max_length=255, null=True, blank=True, verbose_name="State", help_text="Enter the postal address state.")

    company_logo = models.FileField(upload_to="project_logo", null=True, blank=True)
    project_copied_from = models.ForeignKey("Project", null=True, blank=True, on_delete=models.SET_NULL)

    project_publishing_path = models.CharField(max_length=400, null=True, blank=True)
    version = models.FloatField(null=True, blank=True, default=1, help_text="The version number of this instance")

    version_family = models.ForeignKey("ProjectVersion", null=True, blank=True, related_name="version_list")

    status = models.CharField(max_length=50, choices=STATUS_OPTIONS, default=STAGING)

    selected_script = models.ForeignKey("ProjectScripts", null=True, blank=True, related_name="selected_project_script")

    apartment_translation_file = models.FileField(upload_to="apartment_translations", null=True, blank=True)
    sold_status_translation_file = models.FileField(upload_to="sold_status_translations", null=True, blank=True)

    project_select_image = models.FileField(upload_to="project_select_images", null=True, blank=True)
    project_select_chinese_image = models.FileField(upload_to="project_select_chinese_images", null=True, blank=True)
    project_loading_image = models.FileField(upload_to="project_loading_images", null=True, blank=True)
    project_cms_image = models.FileField(upload_to="project_cms_images", null=True, blank=True)
    project_cms_chinese_image = models.FileField(upload_to="project_cms_chinese_images", null=True, blank=True)

    def __str__(self):
        return '(%s) %s - %s [v%s]' % (self.pk, self.name, self.get_status_display(), self.version)

    def __unicode__(self):
        return '%s - %s' % (self.pk, self.name)

    @property
    def project_version_list(self):
        versions = Project.objects.filter(version_family=self.version_family)
        return versions

    @property
    def project_versions(self):
        versions = Project.objects.filter(version_family=self.version_family).values_list('version', flat=True).distinct()
        return versions

    @property
    def dashboard_versions(self):
        versions = Project.objects.filter(version_family=self.version_family,
                                          status__in=[Project.STAGING, Project.LIVE])

        return versions

    @property
    def parent_version(self):
        if self.project_copied_from:
            return self.project_copied_from.version

    @property
    def object_version(self):
        return None

    @property
    def project_last_sync(self):
        return None

    @property
    def version_idx(self):
        versions = self.versions
        version_idx = list(versions).index(-1)
        return version_idx

    @property
    def all_versions(self):
        # versions = self.versions
        # version_list = list(versions)
        # return version_list
        return 1.0

    @property
    def get_publish_path(self):
        if not self.project_publishing_path:
            publishing_path = 'project_images/%s/%s/' % (self.name, self.version)
            publishing_path = publishing_path.replace("//", "/")
            self.project_publishing_path = publishing_path
            self.save()

            return publishing_path
        else:
            return self.project_publishing_path

    @property
    def family(self):
        families = ProjectFamily.objects.filter(project_list=self)
        if families:
            return families[0]

    @property
    def has_family(self):
        families = ProjectFamily.objects.filter(project_list=self)
        if families:
            return True
        return False

    @property
    def is_live(self):
        return self.status == Project.LIVE

    @property
    def is_staging(self):
        return self.status == Project.STAGING

    @property
    def get_publish_path_example(self):
        publishing_path = 'project_images/%s/%s/' % (self.name, self.version)
        publishing_path = publishing_path.replace("//", "/")
        return publishing_path

    @property
    def active_version(self):
        return self.version

    @property
    def last_sync(self):
        all_syncs = ProjectSyncBackLog.objects.filter(project=self).order_by('-pk')
        if all_syncs:
            all_syncs = all_syncs[0]
            if all_syncs.modified:
                return all_syncs.modified
            return all_syncs.created
        return 'n/a'

    @property
    def all_logged_scripts(self):
        return self.logged_scripts.all()

    @property
    def all_logged_comments(self):
        return self.notes_list.all()

    @property
    def all_logged_changes(self):
        logs = []
        projects_in_family = Project.objects.filter(version_family=self.version_family).order_by('version')

        for project in projects_in_family:
            for log in project.change_log.all():
                logs.append(log)

        return logs

    @property
    def connection(self):
        connections = self.project_connections.all()
        return connections[0]

    @property
    def html_friendly_version(self):
        return str(self.version).replace(".", "-")

    @property
    def has_successful_translation(self):
        if self.translations.all():
            check_translations = self.translations.filter(database_id__isnull=False, post_details_log__contains='Success.')
            if check_translations:
                return True

        return False

    @property
    def active_nice_display(self):
        if self.active:
            return '<i class="fa fa-check"></i>'
        else:
            return '<i class="fa fa-close"></i>'

    def get_default_connection_name(self):
        return '%s_%s' % (self.name.replace(" ", "_", 10).replace("'", "", 10).replace('"', "", 10).replace(".", "", 10), self.pk)

    def check_connection_name(self):
        if not self.project_connection_name:
            self.project_connection_name = '%s_%s' % (self.name.replace(" ", "_", 10).replace("'", "", 10).replace('"', "", 10).replace(".", "", 10), self.pk)
            self.save()

            self.run_project_mapping()

    def check_connection_mapping(self):

        if os.path.isdir(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/" % self.project_connection_name)) \
                and os.path.isfile(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/database/%s.db" %
                        (self.project_connection_name, self.project_connection_name))):
            return True
        else:
            self.connection.run_project_mapping()

    def get_absolute_url(self):

        return reverse('projects-view', args=[self.pk])

    def property_count(self, model_mixin):
        try:
            available_properties = model_mixin.model_object("Properties").all().count()
            return available_properties
        except:
            return 0

    def reserved_count(self, model_mixin):
        try:
            reserved_properties = model_mixin.klass("Properties").get_count_properties_with_string_status_value(status="reserved")
            return reserved_properties
        except:
            return 0

    def available_count(self, model_mixin):
        try:
            available_properties = model_mixin.klass("Properties").get_count_properties_with_string_status_value(status="available")
            return available_properties
        except:
            return self.property_count(model_mixin)

    def sold_count(self, model_mixin):
        try:
            sold_properties = model_mixin.klass("Properties").get_count_properties_with_string_status_value(status="sold")
            return sold_properties
        except:
            return 0

    @property
    def project_status(self):
        if self.active:
            return 'Active'
        return 'Inactive'

    @property
    def database_connection(self):
        self.check_connection_name()

        if ProjectConnection.objects.filter(project=self):
            return ProjectConnection.objects.filter(project=self)[0].connection_type
        return None

    @property
    def buildings_model(self):
        return self.has_building_model()

    def has_building_model(self):
        try:
            return apps.get_model(self.project_connection_name, 'buildings')
        except:
            return None

    def has_floors_model(self):
        try:
            return apps.get_model(self.project_connection_name, 'Floors')
        except:
            return None

    def has_hotspot_model(self):
        try:
            return apps.get_model(self.project_connection_name, 'apartmenthotspots')
        except:
            return None

    def has_views_model(self):
        try:
            return apps.get_model(self.project_connection_name, 'apartmentviews')
        except:
            return None

    def has_apartment_types_model(self):
        try:
            return apps.get_model(self.project_connection_name, 'Apartmenttypes')
        except:
            return None

    def has_images_schema(self):
        #new database type
        try:
            return apps.get_model(self.project_connection_name, 'floorviewimages')
        except:
            return None

    def has_apartment_string_fields(self):
        #new database type
        try:
            return apps.get_model(self.project_connection_name, 'Propertiesstringfields')
        except:
            return None

    @property
    def get_database_file(self):
        for connection in self.project_connections.all():
            database_url = connection.save_new_database_file()
            if database_url:
                database_file = os.path.join(settings.MEDIA_ROOT, database_url)
                return database_file
    @property
    def apartment_model(self):
        if self.has_apartment_string_fields():
            return 'Properties'
        return 'Apartments'

    @property
    def images_model(self):
        if self.has_apartment_string_fields():
            return 'Resources'
        return 'Images'

    @property
    def apartment_model_id_field(self):
        if self.has_apartment_string_fields():
            return 'property_id'
        return 'apartment_id'

    @property
    def hotspot_model(self):
        if self.has_apartment_string_fields():
            return 'Propertyhotspots'
        return 'Apartmenthotspots'

    @property
    def import_table_list(self):
        tables = [
            'Select a table mapping',
            'Buildings',
            'Properties',
            'Propertiesstringfields',
            'Propertiesintfields',
            'Propertyhotspots',
            'Propertiesbooleanfields',
            'Propertiesfloatfields',
        ]
        return tables

    @property
    def project_user_viewable(self):
        return self.active and self.status == Project.LIVE and not self.name == "Master"

    def product_model_filter_url(self, model_name, queries):
        url_string = "/projects/%s/filter/values/%s/?%s&amp;export_format=csv" % (self.pk, model_name, queries.urlencode())
        return url_string

    def has_floor_plan_image(self, floor_plate_image_id):
        file_location = None
        configuration = DisplaySweetConfiguration.objects.all()
        if configuration:
            configuration = configuration[0]

        base_file_location = settings.BASE_IMAGE_LOCATION

        for image_locations in configuration.image_configuration.all():
            file_location = '%s/%s%s/' % (base_file_location, image_locations.image_file_location, self.project_connection_name)

            if not os.path.isdir(file_location):
                os.makedirs(file_location)

        floor_location_and_image = '%sfloor_%s.jpg' % (file_location, floor_plate_image_id)
        if os.path.isdir(file_location) and os.path.isfile(os.path.join(file_location, floor_location_and_image)):
            return True

    def over_write_new_database_file(self, database_file):

        file_location = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database/%s.db') \
                        % (self.project_connection_name, self.project_connection_name)


        database_file = database_file['database_file']
        if ".db" in database_file.name:

            with open(file_location, 'wb+') as destination:
                for chunk in database_file.chunks():
                    destination.write(chunk)

            return False


class ProjectConnection(ProjectsPermissionsMixin, models.Model):

    SQLITE_CONNECTION = 'sqlite'
    POSTGRES_CONNECTION = 'postgres'

    CONNECTION_OPTIONS = (
        (SQLITE_CONNECTION, 'SQLite'),
        (POSTGRES_CONNECTION, 'PostgresSQL')
    )

    project = models.ForeignKey('Project', related_name="project_connections")
    connection_type = models.CharField(max_length=50, choices=CONNECTION_OPTIONS)

    database_file = models.FileField(upload_to="database_files", null=True, blank=True, help_text="Select the SQLite database to load. Only required for SQLite.")
    path_to_database = models.CharField(max_length=400, null=True, blank=True)

    username = models.CharField(max_length=30, null=True, blank=True, help_text="Only required for PostgreSQL connection")
    password = models.CharField(max_length=50, null=True, blank=True, help_text="Only required for PostgreSQL connection")

    def __str__(self):
        return '%s' % self.project.project_connection_name

    def __unicode__(self):
        return '%s' % self.project.project_connection_name

    def save_new_database_file(self):
        if self.database_file:
            self.database_file = None
            self.save()

        database_file = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database/%s.db' % (self.project.project_connection_name, self.project.project_connection_name))
        if os.path.isfile(database_file):
            file_name = '%s.db' % self.project.project_connection_name
            f = open(database_file, "rb")

            try:
                self.database_file.save(file_name, File(f))
            except Exception as e:
                print('db save error', e)
                return None

            return self.database_file

        return False

    def run_project_mapping(self, create_from_master=False, test=False):

        if not self.project.project_connection_name:
            self.project.project_connection_name = '%s_%s' % (self.project.name.replace(" ", "_", 10).replace("'", "", 10).replace('"', "", 10).replace(".", "", 10), self.project.pk)
            self.project.save()

        self.check_and_create_initial_files(create_from_master=create_from_master, test=test)
        self.map_database_connection(test=test)
        self.map_database_models(test=test)

        return True

    def check_and_create_initial_files(self, create_from_master=True, test=False):
        print('check_and_create_initial_files')

        if not test:
            if not os.path.isdir(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s' % self.project.project_connection_name)):
                os.makedirs(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s' % self.project.project_connection_name))

            if not os.path.isdir(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/migrations' % self.project.project_connection_name)):
                os.makedirs(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/migrations' % self.project.project_connection_name))
                file = open(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/__init__.py' % self.project.project_connection_name),'w+')
                file.close()

                file = open(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/migrations/__init__.py' % self.project.project_connection_name),'w+')
                file.close()

            if not os.path.isdir(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database' % self.project.project_connection_name)):
                os.makedirs(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database' % self.project.project_connection_name))
                file = open(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database/__init__.py' % self.project.project_connection_name),'w+')
                file.close()

            database_path = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database/%s.db' %
                                         (self.project.project_connection_name, self.project.project_connection_name))

            print('********* create from master: ', create_from_master)

            if create_from_master in [1, "1"]:
                print('cloning without data')
                print(self.project)

                master_database_path = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/master/database/master.db')
                shutil.copy2(master_database_path, database_path)
            else:
                try:
                    print('cloning with data')
                    print('self.project', self.project)

                    print('project copied from: ', self.project.project_copied_from)

                    if self.project.project_copied_from and self.project.project_copied_from.project_connection_name:
                        print('copying db over')
                        project_copied = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/database/%s.db' %
                                                      (self.project.project_copied_from.project_connection_name,
                                                       self.project.project_copied_from.project_connection_name))

                        print('project_copied')
                        print(project_copied)
                        print('')

                        print('database_path')
                        print(database_path)
                        print('')

                        shutil.copy2(project_copied, database_path)
                    else:
                        print('copying master db over')

                        master_database_path = os.path.join(settings.PROJECT_ROOT,
                                                            'projects/project_folders/master/database/master.db')

                        print('master_database_path')
                        print(master_database_path)
                        print('')

                        print('database_path')
                        print(database_path)
                        print('')

                        shutil.copy2(master_database_path, database_path)

                except Exception as e:

                    print('error cloning with data', e)
                    print('self.project', self.project)
                    print('self.project.project_copied_from.project_connection_name', self.project.project_copied_from.project_connection_name)

                    master_database_path = os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/master/database/master.db')
                    shutil.copy2(master_database_path, database_path)

            print('end of check_and_create_initial_files')
        return True

    def map_database_connection(self, test=False):
        print('mapping database connection')
        print('self.connection_type', self.connection_type)

        if not test:
            if self.connection_type in [self.SQLITE_CONNECTION, None, ""]:

                all_databases = {}
                all_routers = ''

                projects = Project.objects.all()
                for project in projects:
                    project_connection_name = project.project_connection_name
                    if project_connection_name:
                        if os.path.isdir(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/" % project_connection_name)) \
                            and os.path.isfile(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/database/%s.db" %
                                (project_connection_name, project_connection_name))):

                            new_connection = {
                                '%s' % str(project_connection_name): {
                                'ENGINE': 'django.db.backends.sqlite3',
                                'NAME': 'projects/project_folders/%s/database/%s.db' % (project_connection_name, project_connection_name),
                                'USER': '',
                                'PASSWORD': '',
                                'HOST': '',
                                'PORT': '',
                                },
                            }

                            all_databases.update(new_connection)

                            router_name = ''
                            router_names = project_connection_name.split("_")
                            for name in router_names:
                                router_name+= '%s' % str(name).capitalize()

                            if not router_name:
                                router_name = str(project_connection_name).capitalize()

                            router_name+= 'Router'
                            new_router = "'projects.project_folders.%s.router.%s'," % (project_connection_name, router_name)

                            all_routers+= new_router

                            try:
                                shutil.copy2(os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py"),
                                             os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/models.py" % project_connection_name))
                            except:
                                pass

                database_text = 'DATABASES = %s' % str(json.dumps(all_databases, indent=4, sort_keys=True))

                file_path = os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/databases_temp_file.py")
                copy_file_path = os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/databases.py")

                try:
                    print('')
                    print('copying databases over')
                    print(file_path)
                    print('')

                    print('copy_file_path')
                    print(copy_file_path)
                    print('')

                    text_file = open(file_path, "w")
                    text_file.write(str(database_text))
                    text_file.close()
                    shutil.copy2(file_path, copy_file_path)

                    print('finished copying databases file')

                except Exception as e:
                    print('failed to update database files: ', e)

                    #
                    #
                    # print('trying to update: ', file_path
                    # print('will copy to: ', copy_file_path

                    copy_db = True
                    try:
                        print('starting db update')
                        text_file = open(file_path, "w", encoding='utf-8')
                        text_file.write(str(database_text))
                        text_file.close()
                        print('completed db update')

                    except Exception as e:
                        copy_db = False
                        print('failed project root write database apps file: ', e)
                        print('file_path root', file_path)

                    if copy_db:
                        try:
                            print('starting to copy database files over ')
                            shutil.copy2(file_path, copy_file_path)
                            print('finished copying databases file')
                        except Exception as e:
                            print('failed to copy root db files', e)
                            print(copy_file_path)

                        try:
                            print('trying to run project restart: ')
                            restart_file = os.path.join(settings.PROJECT_ROOT, "restartApache.sh")
                            command_args = './%s' % restart_file
                            popen = subprocess.Popen(command_args, bufsize=4096,
                                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                                     shell=True)

                            popen.wait()
                            print('completed project restart: ')
                        except Exception as e:
                            print('failed to run restart sub process: ', e)

    def map_project_to_installed_apps(self, test=False):

        if not test:
            self.project.check_connection_mapping()

            print('map_project_to_installed_apps')

            all_installed_apps = ''
            router_text = ''

            projects = Project.objects.all()
            current_installed_apps_file = os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/installed_apps.py")
            for project in projects:
                project_connection_name = project.project_connection_name
                if project_connection_name and os.path.isdir(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/" % project_connection_name)) \
                    and os.path.isfile(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/database/%s.db" %
                                (project_connection_name, project_connection_name))):

                    all_installed_apps += "'projects.project_folders.%s'," % project_connection_name

                router_text = 'INSTALLED_APPS = (%s)' % all_installed_apps

            text_file = open(os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/installed_apps_temp_file.py"), "w")
            text_file.write(str(router_text))
            text_file.close()

            try:
                shutil.copy2(os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/installed_apps_temp_file.py"),
                             current_installed_apps_file)
            except Exception as e:
                print('error updating installed apps:', e)

            print('****** setting installed apps in memory')
            all_installed_apps = []
            for app in settings.INSTALLED_APPS:
                if app and app not in all_installed_apps:
                    all_installed_apps.append(app)

            try:
                settings.INSTALLED_APPS = tuple(all_installed_apps)
            except Exception as e:
                print('installed apps error', e)

    def create_router_files(self):

        print('creating router files')

        if not os.path.isfile(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/router.py' % self.project.project_connection_name)):
            shutil.copy2(os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/master/router.py'), os.path.join(settings.PROJECT_ROOT, 'projects/project_folders/%s/router.py' % self.project.project_connection_name))

        file_to_search = os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/router.py" % self.project.project_connection_name)

        router_name = str(self.project.project_connection_name).capitalize()
        if "_" in self.project.project_connection_name:
            router_names = self.project.project_connection_name.split("_")
            for name in router_names:
                router_name+= '%s' % str(name).capitalize()

        for line in fileinput.input(file_to_search, inplace=1):
            if 'MasterRouter' in line:
                line = line.replace('Master', '%s' % router_name)

            if 'master' in line:
                line = line.replace('master', '%s' % str(self.project.project_connection_name).lower())

            line = line.replace(', ,', '')
            sys.stdout.write(line)

        all_routers = ''
        projects = Project.objects.all()
        for project in projects:
            project_connection_name = project.project_connection_name
            if project_connection_name and \
                    os.path.isdir(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/" % project_connection_name))\
                    and os.path.isfile(os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/database/%s.db" %
                            (project_connection_name, project_connection_name))):

                router_name = ''
                router_names = project_connection_name.split("_")
                for name in router_names:
                    router_name += '%s' % str(name).capitalize()

                if not router_name:
                    router_name = str(project_connection_name).capitalize()

                router_name += 'Router'
                new_router = "'projects.project_folders.%s.router.%s'," % (project_connection_name, router_name)

                all_routers += new_router

        # router_text = 'DATABASE_ROUTERS = [%s]' % all_routers
        # text_file = open(os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/router_temp_file.py"), "w")
        # text_file.write(str(router_text))
        # text_file.close()
        #
        # shutil.copy2(os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/router_temp_file.py"), os.path.join(settings.PROJECT_ROOT, "display_sweet/settings/routers.py"))

        shutil.copy2(os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py"),
                     os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/models.py" % self.project.project_connection_name))

    def map_database_models(self, over_write=False, skip_mapping=False, test=False):
        print('mapping / cleaning database models', 'skip_mapping', skip_mapping)

        if not skip_mapping:
            self.map_project_to_installed_apps()

        try:
            copyfrom = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
            copyto = os.path.join(settings.PROJECT_ROOT,
                                      "projects/project_folders/%s/models.py" % self.project.project_connection_name)

            print (copyfrom)
            print(copyto)
            shutil.copy2(copyfrom, copyto)

            print('Successfully copied project models')

        except Exception as e:
            print('error', e)

    def map_database_models_to_admin_register(self):
        pass


class CSVFileImport(ProjectsPermissionsMixin, models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    csv_file = models.FileField(upload_to="csv_import_files")
    project = models.ForeignKey(Project)

    def __str__(self):
        return '%s - %s' % (self.project.name, self.pk)

    def __unicode__(self):
        return '%s - %s' % (self.project.name, self.pk)

    def has_csv_file(self):
        if self.csv_file:
            return True

    def has_mapped_all_fields(self):
        errors = 0
        headers = self.get_file_headers()
        for item in headers:
            existing = CSVFileImportFields.objects.filter(csv_column_name__iexact=item, csv_import=self)
            if not existing:
                errors += 1

        return errors

    def has_mapped_all_field_values(self, template=None):
        errors = 0
        if not self.has_mapped_all_fields():
            for item in self.csv_template_fields.all():
                if item.column_pretty_name and template:
                    existing = ProjectCSVTemplateFields.objects.filter(column_pretty_name=item.column_pretty_name,
                                                                       column_table_mapping=item.column_table_mapping,
                                                                       column_field_mapping=item.column_field_mapping,
                                                                       order=item.order,
                                                                       is_aspects_field=item.is_aspects_field,
                                                                       template=template,
                                                                       column_csv_table_mapping=item.csv_column_name)

                    if not existing:
                        errors += 1
        return errors

    def get_file_headers(self):
        # self.check_and_change_xlsx_to_csv()

        if ".xlsx" in self.csv_file.name:
            wb = xlrd.open_workbook(self.csv_file.path)
            sh = wb.sheet_by_index(0)

            i = 1
            for rownum in range(sh.nrows):
                headers = sh.row_values(rownum)

                i+=1
                if i == 2:
                    return headers
        else:
            csv_file = open(self.csv_file.path, 'rU', encoding='utf-8')
            csv_reader = csv.reader(csv_file)

            try:
                for index, row in enumerate(csv_reader):
                    if index == 0:
                        return row

            except Exception as e:

                try:
                    i = 1
                    for row in csv_reader:
                        print('row', row)
                        if i == 1:
                            return row
                        i += 1
                except Exception as d:
                    print('second error', d)

                print('error in get file headers', e)

    def create_field_mappings(self, model_mixin):
        headers = self.get_file_headers()
        for item in headers:
            if item:
                #item = model_mixin.klass("Strings").clean_for_mapping(item)
                item = item.replace('\n', ' ', 10).replace('\r', '', 10).replace('(', '', 10).replace(')', '', 10).replace('%', '', 10).replace('&', '', 10)

                existing = CSVFileImportFields.objects.filter(csv_column_name__iexact=item, csv_import=self)
                if not existing:
                    CSVFileImportFields.objects.create(csv_import=self, csv_column_name=item)

    def copy_to_template_fields(self, template):
        for item in self.csv_template_fields.all():
            if item.column_pretty_name:
                existing = ProjectCSVTemplateFields.objects.filter(column_pretty_name=item.column_pretty_name,
                                                                   column_table_mapping=item.column_table_mapping,
                                                                   column_field_mapping=item.column_field_mapping,
                                                                   order=item.order,
                                                                   is_aspects_field=item.is_aspects_field,
                                                                   template=template,
                                                                   column_csv_table_mapping=item.csv_column_name)

                if not existing:
                    ProjectCSVTemplateFields.objects.create(column_pretty_name=item.column_pretty_name,
                                                            column_table_mapping=item.column_table_mapping,
                                                            column_field_mapping=item.column_field_mapping,
                                                            order=item.order,
                                                            is_aspects_field=item.is_aspects_field,
                                                            column_csv_table_mapping=item.csv_column_name,
                                                            template=template)

    def copy_to_import_template_fields(self, template):

        if ".xlsx" in self.csv_file.name:
            wb = xlrd.open_workbook(self.csv_file.path)
            sh = wb.sheet_by_index(0)

            i = 1
            headers = None
            for rownum in range(sh.nrows):
                headers = sh.row_values(rownum)

                i+=1
                if i == 2:
                    break

            if headers:
                headers_list_dict = []
                accepted_positions = []
                created_import = ProjectCSVTemplateImport.objects.create(template=template)

                i = 0
                for item in headers:
                    item = item.replace('\n', ' ', 10).replace('\r', '', 10).replace('(', '', 10).replace(')', '', 10).replace('%', '', 10).replace('&', '', 10)
                    item_field = ProjectCSVTemplateFields.objects.filter(column_csv_table_mapping__iexact=item,
                                                                         template=template)

                    if item_field:
                        item_field = item_field[0]
                        check_field = ProjectCSVTemplateFields.objects.filter(template=template,
                                                                              column_table_mapping=item_field.column_table_mapping,
                                                                              column_field_mapping=item_field.column_field_mapping,
                                                                              column_pretty_name=item_field.column_pretty_name,
                                                                              order=item_field.order,
                                                                              is_aspects_field=item_field.is_aspects_field)

                        if check_field:
                            check_field = check_field[0]
                            headers_dict = {}
                            headers_dict['position'] = i
                            headers_dict['column'] = item
                            headers_dict['project_field'] = check_field

                            headers_list_dict.append(headers_dict)
                            accepted_positions.append(i)
                    i +=1

                i = 0
                adding = 0
                for rownum in range(sh.nrows):
                    values = sh.row_values(rownum)

                    adding += 1
                    i += 1
                    if i > 1: # So we skip header row
                        for cell_index, cell in enumerate(values, start=0):

                            for available_headers in headers_list_dict:
                                if available_headers['position'] == cell_index:
                                    cell = str(cell).strip()
                                    cell = cell.replace("'", "").replace('"', '')

                                    item_field = available_headers['project_field']
                                    item_field_string = str(item_field).strip()

                                    if str(item_field_string) in ["Floor", "floor"]:
                                        try:
                                            cell = int(float(cell))
                                        except:
                                            cell = smart_str(cell, encoding='utf-8', strings_only=False, errors='strict')

                                    try:

                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)

                                    except Exception as e:
                                        #print('import field error', e
                                        cell = re.sub('[^a-zA-Z0-9\n\.]', ' ', cell)
                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)

        else:
            csv_file = open(self.csv_file.path, 'rU', encoding='utf-8')
            csv_reader = csv.reader(csv_file)

            headers = None
            for index, row in enumerate(csv_reader):
                if index == 0:
                    headers = row

            if headers:
                headers_list_dict = []
                accepted_positions = []
                created_import = ProjectCSVTemplateImport.objects.create(template=template)

                i = 0
                for item in headers:
                    item = item.replace('\n', ' ', 10).replace('\r', '', 10).replace('(', '', 10).replace(')', '', 10).replace('%', '', 10).replace('&', '', 10)
                    item_field = CSVFileImportFields.objects.filter(csv_column_name__iexact=item, csv_import=self)

                    if item_field:
                        item_field = item_field[0]
                        check_field = ProjectCSVTemplateFields.objects.filter(template=template,
                                                                              column_table_mapping=item_field.column_table_mapping,
                                                                              column_field_mapping=item_field.column_field_mapping,
                                                                              column_pretty_name=item_field.column_pretty_name)

                        if check_field:
                            headers_dict = {}
                            headers_dict['position'] = i
                            headers_dict['column'] = item
                            headers_dict['project_field'] = check_field[0]

                            headers_list_dict.append(headers_dict)
                            accepted_positions.append(i)
                    i +=1

                i = 0
                adding = 0
                csv_file = open(self.csv_file.path, 'rU', encoding='utf-8')
                csv_reader = csv.reader(csv_file)

                for index, row in enumerate(csv_reader):
                    adding += 1
                    i += 1
                    if i > 1:
                        for cell_index, cell in enumerate(row, start=0):
                            for available_headers in headers_list_dict:
                                if available_headers['position'] == cell_index:
                                    item_field = available_headers['project_field']
                                    item_field_string = str(item_field).strip()

                                    cell = str(cell).strip()
                                    cell = cell.replace("'", "").replace('"', '')

                                    if str(item_field_string) in ["Floor", "floor"]:
                                        try:
                                            cell = int(float(cell))
                                        except:
                                            cell = smart_str(cell, encoding='utf-8', strings_only=False, errors='strict')

                                    try:
                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)
                                    except Exception as e:
                                        print('import field error', e)
                                        cell = re.sub('[^a-zA-Z0-9\n\.]', ' ', cell)

                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                 field=item_field,
                                                                                 value=cell,
                                                                                 row_id=adding)
        return True

    def check_and_change_xlsx_to_csv(self):
        if ".xlsx" in self.csv_file.name:
            wb = xlrd.open_workbook(self.csv_file.path)
            sh = wb.sheet_by_index(0)
            your_csv_file = open('replace_csv_file.csv', 'wb')

            wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
            for rownum in range(sh.nrows):
                value = sh.row_values(rownum)
                try:
                    wr.writerow(value)
                except:
                    modified_row = []
                    for item in value:
                        if type(item) not in [int, float, str]:
                            new_item = item
                            modified_row.append(str(new_item))
                        else:
                            modified_row.append(item)
                    wr.writerow(modified_row)

            your_csv_file.close()

            self.csv_file = your_csv_file
            self.save()

    def attempted_column_match(self, column):
        pass


class CSVFileImportFields(ProjectsPermissionsMixin, models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    csv_import = models.ForeignKey(CSVFileImport, related_name="csv_template_fields")
    csv_column_name = models.CharField(max_length=255, null=True)
    column_table_mapping = models.CharField(max_length=255, null=True, blank=True)
    column_field_mapping = models.CharField(max_length=255, null=True, blank=True)
    column_pretty_name = models.CharField(max_length=255, null=True, blank=True)
    column_field_mapping_type = models.CharField(max_length=255, null=True, blank=True)

    is_aspects_field = models.BooleanField(default=False)
    order = models.IntegerField(null=True, blank=True, default=1)

    def __str__(self):
        return '%s - %s %s' % (self.csv_import.project.name, self.csv_column_name, self.column_pretty_name)

    def __unicode__(self):
        return '%s - %s %s' % (self.csv_import.project.name, self.csv_column_name, self.column_pretty_name)


class BulkImport(models.Model):
    """
    A bulk import of registration objects from an XLSX file
    """
    STATUS_DRAFT = 'draft'
    STATUS_SUBMITTED = 'submitted'
    STATUS_PROCESSING_UNDERWAY = 'processing_underway'
    STATUS_PROCESSING_FAILED = 'processing_failed'
    STATUS_PROCESSED = 'processed'

    STATUS_CHOICES = (
        (STATUS_DRAFT, u'Draft'),
        (STATUS_SUBMITTED, u'Submitted'),
        (STATUS_PROCESSING_UNDERWAY, u'Processing Underway'),
        (STATUS_PROCESSING_FAILED, u'Processing Failed'),
        (STATUS_PROCESSED, u'Processed'),
    )

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    status = models.CharField(max_length=50, default=STATUS_DRAFT, choices=STATUS_CHOICES)

    project = models.ForeignKey("Project", help_text="The project this file was loaded for.")
    table_name = models.CharField(max_length=255, null=True, blank=True, help_text="The table this file was loaded for.")

    uploaded_xlsx = SecureFileField(upload_to="bulkimport_xlsx_uploads", null=True, blank=True, help_text="The XLSX file containing the registration data")
    uploaded_data = JSONField(null=True, blank=True, help_text="The data contained in the registration data upload")
    notes = models.TextField(blank=True, null=True, help_text="Notes from the user regarding file.")

    def status_is_processed(self):
        return self.status == self.STATUS_PROCESSED

    def status_is_submitted(self):
        return self.status == self.STATUS_SUBMITTED

    def status_is_draft(self):
        return self.status == self.STATUS_DRAFT

    def status_is_processing_underway(self):
        return self.status == self.STATUS_PROCESSING_UNDERWAY

    def status_is_processing_failed(self):
        return self.status == self.STATUS_PROCESSING_FAILED

    def submit(self):
        if self.status != self.STATUS_DRAFT:
            return False

        if self.type_page_status != self.PAGE_STATUS_LOCKED:
            return False

        self.status = self.STATUS_SUBMITTED
        self.save()

        return True

    def cancel_submit(self):
        if self.status != self.STATUS_SUBMITTED:
            return False

        self.status = self.STATUS_DRAFT
        self.save()

    def reset_failed_processing(self):
        if self.status != self.STATUS_PROCESSING_FAILED:
            return False

        self.status = self.STATUS_DRAFT
        self.save()


class DisplaySweetConfiguration(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    image_locations = models.ManyToManyField("ImageLocationConfiguration")

    def __str__(self):
        return '%s' % self.pk

    def __unicode__(self):
        return '%s' % self.pk


class ImageLocationConfiguration(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    IMAGE_LOCATION_IPAD = 'ipad_image_config'
    IMAGE_LOCATION_PROJECTOR = 'projector_image_config'
    MAIN_FLOOR_PLAN_LOCATION = 'floor_plan_location'

    LOCATION_TYPE_OPTIONS = (
        (MAIN_FLOOR_PLAN_LOCATION, 'Floor Plan Location'),
        (IMAGE_LOCATION_IPAD, 'Ipad Floorplan location'),
        (IMAGE_LOCATION_PROJECTOR, 'Projector Floorplan location')
    )

    configuration = models.ForeignKey(DisplaySweetConfiguration, null=True, blank=True, related_name='image_configuration')
    image_file_location = models.CharField(max_length=400, null=True, blank=True,
                                           help_text="Please include the full path to the project image directory as follows: "
                                                     "/var/www/html/project_data/projects/")

    location_type = models.CharField(max_length=255, choices=LOCATION_TYPE_OPTIONS, default=MAIN_FLOOR_PLAN_LOCATION)

    def __str__(self):
        return '%s' % self.image_file_location

    def __unicode__(self):
        return '%s' % self.image_file_location


class ProjectCSVTemplate(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_created=True, auto_now_add=True, null=True, blank=True)

    project = models.ForeignKey(Project, null=True, blank=True)
    template_name = models.CharField(max_length=255, null=True, blank=True)

    is_editable_template = models.BooleanField(default=False)
    is_editable_template_default = models.BooleanField(default=False)

    is_allocation_template = models.BooleanField(default=False)

    csv_file_import = models.ForeignKey(CSVFileImport, null=True, blank=True)

    def __str__(self):
        return '%s - %s' % (self.project, self.template_name)

    def __unicode__(self):
        return '%s - %s' % (self.project, self.template_name)


class ProjectCSVTemplateFields(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)

    template = models.ForeignKey(ProjectCSVTemplate, related_name="project_template_fields")
    column_table_mapping = models.CharField(max_length=255, null=True, blank=True)
    column_field_mapping = models.CharField(max_length=255, null=True, blank=True)
    column_pretty_name = models.CharField(max_length=255, null=True, blank=True)
    column_field_mapping_type = models.CharField(max_length=255, null=True, blank=True)

    order = models.IntegerField(null=True, blank=True, default=1)
    is_aspects_field = models.BooleanField(default=False)
    column_csv_table_mapping = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return '%s' % self.column_pretty_name

    def __unicode__(self):
        return '%s' % self.column_pretty_name

    class Meta:
        verbose_name_plural = 'Project CSV Template Fields'


class ProjectCSVTemplateImport(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    template = models.ForeignKey(ProjectCSVTemplate)

    def __str__(self):
        return '%s' % self.template

    def __unicode__(self):
        return '%s' % self.template


class ProjectCSVTemplateFieldValues(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)

    field = models.ForeignKey(ProjectCSVTemplateFields)
    value = models.CharField(max_length=255, null=True, blank=True)

    row_id = models.IntegerField(default=1)

    import_instance = models.ForeignKey(ProjectCSVTemplateImport, null=True, blank=True)

    def __str__(self):
        return '%s - %s' % (self.row_id, self.field)

    def __unicode__(self):
        return '%s - %s' % (self.row_id, self.field)


class BulkImportCompletedTemplate(models.Model):
    """
    A bulk import of registration objects from an XLSX file
    """
    STATUS_DRAFT = 'draft'
    STATUS_SUBMITTED = 'submitted'
    STATUS_PROCESSING_UNDERWAY = 'processing_underway'
    STATUS_PROCESSING_FAILED = 'processing_failed'
    STATUS_PROCESSED = 'processed'

    STATUS_CHOICES = (
        (STATUS_DRAFT, u'Draft'),
        (STATUS_SUBMITTED, u'Submitted'),
        (STATUS_PROCESSING_UNDERWAY, u'Processing Underway'),
        (STATUS_PROCESSING_FAILED, u'Processing Failed'),
        (STATUS_PROCESSED, u'Processed'),
    )

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    status = models.CharField(max_length=50, default=STATUS_DRAFT, choices=STATUS_CHOICES)

    project = models.ForeignKey("Project", help_text="The project this file was loaded for.")
    template = models.ForeignKey(ProjectCSVTemplate, null=True, blank=True, help_text="The template this file was loaded for.")

    uploaded_xlsx = SecureFileField(upload_to="bulkimport_xlsx_uploads", null=True, blank=True)
    uploaded_data = JSONField(null=True, blank=True)
    notes = models.TextField(blank=True, null=True, help_text="Notes from the user regarding file.")

    def status_is_processed(self):
        return self.status == self.STATUS_PROCESSED

    def status_is_submitted(self):
        return self.status == self.STATUS_SUBMITTED

    def status_is_draft(self):
        return self.status == self.STATUS_DRAFT

    def status_is_processing_underway(self):
        return self.status == self.STATUS_PROCESSING_UNDERWAY

    def status_is_processing_failed(self):
        return self.status == self.STATUS_PROCESSING_FAILED

    def submit(self):
        if self.status != self.STATUS_DRAFT:
            return False

        if self.type_page_status != self.PAGE_STATUS_LOCKED:
            return False

        self.status = self.STATUS_SUBMITTED
        self.save()

        return True

    def cancel_submit(self):
        if self.status != self.STATUS_SUBMITTED:
            return False

        self.status = self.STATUS_DRAFT
        self.save()

    def reset_failed_processing(self):
        if self.status != self.STATUS_PROCESSING_FAILED:
            return False

        self.status = self.STATUS_DRAFT
        self.save()

    def get_workbook(self):
        from utils.xlxs_reader import ImportedWorkbook, InvalidImportException

        try:
            workbook = ImportedWorkbook(self)
        except InvalidImportException:
            workbook = None

        return workbook

    def copy_to_import_template_fields(self):
        from string import whitespace, punctuation
        if ".xlsx" in self.uploaded_xlsx.name:
            wb = xlrd.open_workbook(self.uploaded_xlsx.path)
            sh = wb.sheet_by_index(0)

            i = 1
            headers = None
            for rownum in range(sh.nrows):
                headers = sh.row_values(rownum)

                i+=1
                if i == 2:
                    break

            if headers:
                headers_list_dict = []
                accepted_positions = []
                created_import = ProjectCSVTemplateImport.objects.create(template=self.template)

                i = 0
                for item in headers:
                    item = item.translate(None, punctuation)

                    #item = item.replace('\n', ' ', 10).replace('\r', '', 10).replace('(', '', 10).replace(')', '', 10).replace('%', '', 10).replace('&', '', 10)
                    item_field = ProjectCSVTemplateFields.objects.filter(column_field_mapping__iexact=item, template=self.template)

                    if item_field:
                        item_field = item_field[0]
                        check_field = ProjectCSVTemplateFields.objects.filter(template=self.template,
                                                                              column_table_mapping=item_field.column_table_mapping,
                                                                              column_field_mapping=item_field.column_field_mapping,
                                                                              column_pretty_name=item_field.column_pretty_name)

                        if check_field:
                            headers_dict = {}
                            headers_dict['position'] = i
                            headers_dict['column'] = item
                            headers_dict['project_field'] = check_field[0]

                            headers_list_dict.append(headers_dict)
                            accepted_positions.append(i)
                    i +=1

                i = 0
                adding = 0
                for rownum in range(sh.nrows):
                    values = sh.row_values(rownum)

                    adding += 1
                    i += 1
                    if i > 1: # So we skip header row
                        for cell_index, cell in enumerate(values, start=0):

                            for available_headers in headers_list_dict:
                                if available_headers['position'] == cell_index:
                                    cell = str(cell).strip()
                                    cell = cell.replace("'", "").replace('"', '')
                                    item_field = available_headers['project_field']
                                    item_field_string = str(item_field).strip()

                                    if str(item_field_string) in ["Floor", "floor"]:
                                        try:
                                            cell = int(float(cell))
                                        except:
                                            cell = smart_str(cell, encoding='utf-8', strings_only=False,
                                                             errors='strict')

                                    try:
                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)
                                    except Exception as e:
                                        ##print('import field error', e
                                        cell = re.sub('[^a-zA-Z0-9\n\.]', ' ', cell)
                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)
        else:
            csv_file = open(self.uploaded_xlsx.path, 'rU', encoding='utf-8')
            csv_reader = csv.reader(csv_file)

            headers = None
            for index, row in enumerate(csv_reader):
                if index == 0:
                    headers = row

            if headers:
                headers_list_dict = []
                accepted_positions = []
                created_import = ProjectCSVTemplateImport.objects.create(template=self.template)

                i = 0
                for item in headers:
                    print('item', item)
                    if str(item).strip().lower() == "building":
                        item = "building_name"

                    string_value = str(item).replace("  ", " ", 10).replace("/", "", 10)
                    string_value = str(string_value).strip().replace("  ", " ", 10)
                    string_value = str(string_value).replace(" ", "_", 10).lower()

                    item_field = ProjectCSVTemplateFields.objects.filter(column_field_mapping__iexact=string_value,
                                                                         template=self.template)

                    if item_field:
                        item_field = item_field[0]
                        check_field = ProjectCSVTemplateFields.objects.filter(template=self.template,
                                                                              column_table_mapping=item_field.column_table_mapping,
                                                                              column_field_mapping=item_field.column_field_mapping,
                                                                              column_pretty_name=item_field.column_pretty_name)

                        if check_field:
                            headers_dict = {}
                            headers_dict['position'] = i
                            headers_dict['column'] = item
                            headers_dict['project_field'] = check_field[0]

                            headers_list_dict.append(headers_dict)
                            accepted_positions.append(i)
                    i += 1

                i = 0
                adding = 0
                csv_file = open(self.uploaded_xlsx.path, 'rU', encoding='utf-8')
                csv_reader = csv.reader(csv_file)

                for index, row in enumerate(csv_reader):
                    adding += 1
                    i += 1
                    if i > 1:
                        for cell_index, cell in enumerate(row, start=0):
                            for available_headers in headers_list_dict:
                                if available_headers['position'] == cell_index:
                                    item_field = available_headers['project_field']
                                    item_field_string = str(item_field).strip()

                                    cell = str(cell).strip()
                                    cell = cell.replace("'", "").replace('"', '')

                                    if str(item_field_string) in ["Floor", "floor"]:
                                        try:
                                            cell = int(float(cell))
                                        except:
                                            cell = smart_str(cell, encoding='utf-8', strings_only=False,
                                                             errors='strict')

                                    try:
                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)
                                    except Exception as e:
                                        print('import field error', e)
                                        cell = re.sub('[^a-zA-Z0-9\n\.]', ' ', cell)

                                        ProjectCSVTemplateFieldValues.objects.create(import_instance=created_import,
                                                                                     field=item_field,
                                                                                     value=cell,
                                                                                     row_id=adding)


class ProjectSyncBackLog(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(auto_created=True, null=True, blank=True)

    direction = models.CharField(max_length=255, null=True, blank=True)
    project = models.ForeignKey(Project)
    log = models.TextField(null=True, blank=True)

    logged = models.BooleanField(default=False)
    status_log = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return '%s' % self.project

    def __str__(self):
        return '%s' % self.project


class ProjectImagePath(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(auto_created=True, null=True, blank=True)

    image_type = models.CharField(max_length=255, null=True, blank=True)
    image_output = models.CharField(max_length=255, null=True, blank=True)
    image_mode = models.CharField(max_length=255, null=True, blank=True)

    image_path = models.CharField(max_length=255)

    project = models.ForeignKey(Project, null=True, blank=True)
    description = models.CharField(max_length=400)

    def __str__(self):
        return '%s' % self.image_type


class ProjectVersion(models.Model):

    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(auto_created=True, null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True, related_name="version_history")
    project_name = models.CharField(max_length=50, null=True, blank=True)
    active_version = models.FloatField(null=True, blank=True)

    def __str__(self):
        return '(%s) %s' % (self.pk, self.project_name)

    def __unicode__(self):
        return '(%s) %s' % (self.pk, self.project_name)


class ProjectFamily(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True, related_name="project_family_created_by")
    last_modified_by = models.ForeignKey("users.User", null=True, blank=True, related_name="project_family_modified_by")

    family_name = models.CharField(max_length=50, null=True, blank=True)

    project_list = models.ManyToManyField(Project, blank=True, related_name="projects_all_family")

    def __str__(self):
        return '(%s) %s' % (self.pk, self.family_name)

    def __unicode__(self):
        return '(%s) %s' % (self.pk, self.family_name)

    @property
    def client_viewable_projects(self):
        return self.project_list.filter(status__in=[Project.STAGING, Project.LIVE],
                                        active=True).order_by('version')

    @property
    def family_version_list(self):
        return self.project_list.all().values_list('version', flat=True).order_by('-version').distinct()

    @property
    def latest(self):
        if self.project_list.filter(active=True):
            latest = self.project_list.filter(status=Project.LIVE, active=True).order_by('-version')
            if latest:
                return latest[0]

            latest = self.project_list.filter(status=Project.STAGING, active=True).order_by('-version')
            if latest:
                return latest[0]

        if self.project_list.all():
            return self.project_list.all().order_by('-version')[0]

    @property
    def latest_version(self):
        if self.project_list.filter(active=True):
            latest = self.project_list.filter(status=Project.LIVE, active=True).order_by('-version')
            if latest:
                return latest[0].version

            latest = self.project_list.filter(status=Project.STAGING, active=True).order_by('-version')
            if latest:
                return latest[0].version

        if self.project_list.all():
            return self.project_list.all().order_by('-version')[0]

    @property
    def all_scripts(self):
        all_scripts = []
        for famproject in self.project_list.all():
            all_scripts += famproject.all_logged_scripts

        all_scripts = list(set(all_scripts))
        return all_scripts

    @property
    def all_comments(self):
        all_comments = []
        for famproject in self.project_list.all():
            all_comments += famproject.all_logged_comments

        all_comments = list(set(all_comments))
        return all_comments

    @property
    def all_logged_changes(self):
        all_logged_changes = []
        for famproject in self.project_list.all():
            all_logged_changes += famproject.all_logged_changes

            all_logged_changes = list(set(all_logged_changes))
        return all_logged_changes


class ProjectNotes(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True, related_name="notes_list")
    version = models.FloatField(null=True, blank=True)

    title = models.TextField(max_length=255, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    document = models.FileField(upload_to="project_comment_documents", null=True, blank=True)

    class Meta:
        verbose_name_plural = "Project Notes"

    def __str__(self):
        return '%s' % self.title


class ProjectChangeLog(models.Model):

    created = models.DateField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True, related_name="change_log")
    version = models.FloatField(null=True, blank=True)

    title = models.TextField(max_length=255, null=True, blank=True)
    log = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s' % self.title

    def __unicode__(self):
        return '%s' % self.title


class ProjectScripts(models.Model):

    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(auto_created=True, null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True)

    project = models.ForeignKey(Project, null=True, blank=True, related_name="logged_scripts")
    script_name = models.CharField(max_length=50, null=True, blank=True)
    version = models.FloatField(null=True, blank=True)

    file = models.FileField(upload_to='project_scripts', null=True, blank=True)
    notes = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Project Scripts"

    def __str__(self):

        return '%s: %s' % (self.project, self.script_name)

    def __unicode__(self):
        return '%s: %s' % (self.project, self.script_name)


class ProjectTranslations(models.Model):

    STAGING = 'cmsstaging.displaysweet.com'
    ADMIN = 'admin.displaysweet.com'

    PUBLISHED_TO_OPTIONS = (
        (STAGING, 'cmsstaging.displaysweet.com'),
        (ADMIN, 'admin.displaysweet.com')
    )

    PUBLISH_SELECT = {STAGING: 1, ADMIN: 2}

    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    modified = models.DateTimeField(auto_created=True, null=True, blank=True)

    created_by = models.ForeignKey("users.User", null=True, blank=True)

    project = models.ForeignKey(Project, null=True, blank=True, related_name="translations")

    database_file = models.FileField(upload_to="translation_databases", null=True, blank=True)
    apartment_translation_file = models.FileField(upload_to="translations_apartment", null=True, blank=True)
    sold_status_translation_file = models.FileField(upload_to="translations_sold_status", null=True, blank=True)
    projector_canvas_name = models.CharField(max_length=255, null=True, blank=True)
    kwall_canvas_name = models.CharField(max_length=255, null=True, blank=True)
    proj_canvas_name = models.CharField(max_length=255, null=True, blank=True)
    version_published = models.FloatField(default=1.0)
    version_prefix_count = models.FloatField(default=1.0)

    create_slim_database = models.BooleanField(default=False)
    apartment_duplicates = models.BooleanField(default=False, help_text="Do apartments have duplicate names?")
    published_to = models.CharField(max_length=400, choices=PUBLISHED_TO_OPTIONS, null=True, blank=True)

    post_details_log = models.CharField(max_length=400, null=True, blank=True)

    translated_database = models.FileField(upload_to="translated_complete_db", null=True, blank=True)

    database_id = models.IntegerField(null=True)
    updated_script_file = models.FileField(upload_to="translated_complete_script", null=True, blank=True)

    project_select_image = models.FileField(upload_to="translated_select_images", null=True, blank=True)
    project_select_chinese_image = models.FileField(upload_to="translated_select_chinese_images", null=True, blank=True)
    project_loading_image = models.FileField(upload_to="translated_loading_images", null=True, blank=True)
    project_cms_image = models.FileField(upload_to="translated_cms_images", null=True, blank=True)
    project_cms_chinese_image = models.FileField(upload_to="translated_cms_chinese_images", null=True, blank=True)

    class Meta:
        verbose_name_plural = "Project Translations"

    def __str__(self):

        return '%s: %s' % (self.project, self.created)

    def __unicode__(self):
        return '%s: %s' % (self.project, self.created)