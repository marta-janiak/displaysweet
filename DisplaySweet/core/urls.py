from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^$', 'core.views.index', name='home'),
    url(r'^404/$', 'core.views.force_404', name='404'),
)
