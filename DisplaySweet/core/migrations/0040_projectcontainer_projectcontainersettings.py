# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import projects.permissions


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0039_auto_20151210_1436'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectContainer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('container_id', models.IntegerField()),
                ('string_field', models.CharField(max_length=255, null=True, blank=True)),
                ('field_type', models.CharField(max_length=255, null=True, blank=True)),
                ('initial_value', models.CharField(max_length=255, null=True, blank=True)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
            bases=(projects.permissions.ProjectsPermissionsMixin, models.Model),
        ),
        migrations.CreateModel(
            name='ProjectContainerSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('container_id', models.IntegerField()),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('width', models.IntegerField(default=100)),
                ('orderidx', models.IntegerField(default=0)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
            bases=(projects.permissions.ProjectsPermissionsMixin, models.Model),
        ),
    ]
