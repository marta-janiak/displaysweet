# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_projectcsvtemplate_csv_file_import'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplatefields',
            name='column_csv_table_mapping',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
