# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20150922_1025'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagelocationconfiguration',
            name='configuration',
            field=models.ForeignKey(blank=True, to='core.DisplaySweetConfiguration', null=True),
        ),
    ]
