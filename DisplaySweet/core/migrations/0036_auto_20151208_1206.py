# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0035_projectcsvtemplate_modified'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplate',
            name='is_editable_template',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplate',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime.now(), auto_now_add=True, auto_created=True),
            preserve_default=False,
        ),
    ]
