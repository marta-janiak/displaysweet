# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_projectimagepath_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectimagepath',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
    ]
