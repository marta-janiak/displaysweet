# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_remove_project_image_dimensions'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='floor_plan_location',
        ),
        migrations.RemoveField(
            model_name='project',
            name='key_plan_location',
        ),
        migrations.RemoveField(
            model_name='project',
            name='level_plan_location',
        ),
        migrations.RemoveField(
            model_name='project',
            name='level_plan_location_full_res',
        ),
    ]
