# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0067_auto_20160609_0115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectscripts',
            name='project',
            field=models.ForeignKey(related_name='logged_scripts', blank=True, to='core.Project', null=True),
        ),
    ]
