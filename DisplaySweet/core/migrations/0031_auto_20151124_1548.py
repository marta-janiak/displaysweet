# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_projectcsvtemplatefields_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectcsvtemplatefields',
            name='order',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplatefields',
            name='template',
            field=models.ForeignKey(related_name='template_fields', to='core.ProjectCSVTemplate'),
        ),
    ]
