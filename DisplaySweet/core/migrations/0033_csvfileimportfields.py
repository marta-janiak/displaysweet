# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import projects.permissions


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0032_auto_20151201_1154'),
    ]

    operations = [
        migrations.CreateModel(
            name='CSVFileImportFields',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('csv_column_name', models.CharField(max_length=255, null=True)),
                ('column_table_mapping', models.CharField(max_length=255, null=True, blank=True)),
                ('column_field_mapping', models.CharField(max_length=255, null=True, blank=True)),
                ('column_pretty_name', models.CharField(max_length=255, null=True, blank=True)),
                ('column_field_mapping_type', models.CharField(max_length=255, null=True, blank=True)),
                ('order', models.IntegerField(default=99, null=True, blank=True)),
                ('csv_import', models.ForeignKey(to='core.CSVFileImport')),
                ('project', models.ForeignKey(to='core.Project')),
            ],
            bases=(projects.permissions.ProjectsPermissionsMixin, models.Model),
        ),
    ]
