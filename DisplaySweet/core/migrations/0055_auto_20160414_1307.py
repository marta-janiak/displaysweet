# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0054_auto_20160413_0617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='project_copied_from',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.Project', null=True),
        ),
    ]
