# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_projectcsvtemplate_projectcsvtemplatefields'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='projectcsvtemplatefields',
            options={'verbose_name_plural': 'Project CSV Template Fields'},
        ),
        migrations.AddField(
            model_name='projectcsvtemplatefields',
            name='column_field_mapping_type',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='projectcsvtemplatefields',
            unique_together=set([('column_table_mapping', 'column_field_mapping')]),
        ),
    ]
