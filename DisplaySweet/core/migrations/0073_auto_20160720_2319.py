# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_auto_20160717_2339'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='projectnotes',
            options={'verbose_name_plural': 'Project Notes'},
        ),
        migrations.AlterModelOptions(
            name='projectscripts',
            options={'verbose_name_plural': 'Project Scripts'},
        ),
        migrations.AlterModelOptions(
            name='projecttranslations',
            options={'verbose_name_plural': 'Project Translations'},
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='post_details_log',
            field=models.CharField(null=True, max_length=400, blank=True),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='translated_database',
            field=models.FileField(upload_to="translated_complete_db", null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projecttranslations',
            name='published_to',
            field=models.CharField(max_length=400, null=True, choices=[('cmsstaging.displaysweet.com', 'cmsstaging.displaysweet.com'), ('admin.displaysweet.com', 'admin.displaysweet.com')], blank=True),
        ),
    ]
