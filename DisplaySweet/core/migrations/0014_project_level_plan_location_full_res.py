# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20151026_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='level_plan_location_full_res',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
    ]
