# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0046_auto_20160210_1536'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='image_dimensions',
        ),
    ]
