# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', 'add_database_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='projecttranslations',
            name='updated_script_file',
            field=models.FileField(upload_to="translated_complete_script", null=True, blank=True),
        ),
    ]
