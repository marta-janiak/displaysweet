# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20151109_1340'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='projectcsvtemplatefields',
            unique_together=set([]),
        ),
    ]
