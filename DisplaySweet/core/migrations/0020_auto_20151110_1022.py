# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import utils.secure_file_field
import utils.json_field


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_auto_20151109_1609'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectCSVTemplateFieldValues',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('field', models.ForeignKey(to='core.ProjectCSVTemplateFields')),
            ],
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='uploaded_data',
            field=utils.json_field.JSONField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='uploaded_xlsx',
            field=utils.secure_file_field.SecureFileField(null=True, upload_to=b'bulkimport_xlsx_uploads', blank=True),
        ),
    ]
