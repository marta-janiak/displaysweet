# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0073_auto_20160720_2319'),
    ]

    operations = [
        migrations.AddField(
            model_name='projecttranslations',
            name='database_id',
            field=models.IntegerField(null=True),
        ),
    ]
