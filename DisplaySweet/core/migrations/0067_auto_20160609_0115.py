# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0066_auto_20160605_1350'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectScripts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modified', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, auto_created=True)),
                ('script_name', models.CharField(max_length=50, null=True, blank=True)),
                ('version', models.FloatField(null=True, blank=True)),
                ('file', models.FileField(null=True, upload_to=b'project_scripts', blank=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('project', models.ForeignKey(blank=True, to='core.Project', null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='projectchangelog',
            name='project',
            field=models.ForeignKey(related_name='change_log', blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='projectnotes',
            name='project',
            field=models.ForeignKey(related_name='notes_list', blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='projectversion',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
    ]
