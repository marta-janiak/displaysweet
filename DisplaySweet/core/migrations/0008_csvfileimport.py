# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import projects.permissions


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150906_2122'),
    ]

    operations = [
        migrations.CreateModel(
            name='CSVFileImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('csv_file', models.FileField(upload_to=b'csv_import_files')),
                ('table_name', models.CharField(max_length=255, null=True, blank=True)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
            bases=(projects.permissions.ProjectsPermissionsMixin, models.Model),
        ),
    ]
