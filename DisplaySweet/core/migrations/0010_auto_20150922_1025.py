# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_bulkimport'),
    ]

    operations = [
        migrations.CreateModel(
            name='DisplaySweetConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ImageLocationConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('image_file_location', models.CharField(max_length=400, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='displaysweetconfiguration',
            name='image_locations',
            field=models.ManyToManyField(to='core.ImageLocationConfiguration', null=True),
        ),
    ]
