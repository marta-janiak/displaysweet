# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_bulkimportcompletedtemplate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='template',
            field=models.ForeignKey(blank=True, to='core.ProjectCSVTemplate', help_text=b'The template this file was loaded for.', null=True),
        ),
    ]
