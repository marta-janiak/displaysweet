# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0059_project_project_publishing_path'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectChangeLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('version', models.FloatField(null=True, blank=True)),
                ('title', models.TextField(max_length=255, null=True, blank=True)),
                ('log', models.TextField(null=True, blank=True)),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectNotes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('version', models.FloatField(null=True, blank=True)),
                ('title', models.TextField(max_length=255, null=True, blank=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('document', models.FileField(null=True, upload_to=b'project_comment_documents', blank=True)),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='FloorOrder',
        ),
        migrations.AddField(
            model_name='project',
            name='created_by',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='last_modified_by',
            field=models.ForeignKey(related_name='modified_by', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='parent_version',
            field=models.FloatField(default=1, help_text=b"The version number of this instance's parent", null=True, blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='status',
            field=models.CharField(default=b'in_progress', max_length=50, choices=[(b'in_progress', b'In Progress'), (b'staging', b'Staging'), (b'testing', b'Test'), (b'not_active', b'Not Active'), (b'live', b'Live')]),
        ),
        migrations.AddField(
            model_name='project',
            name='version',
            field=models.FloatField(default=1, help_text=b'The version number of this instance', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='projectnotes',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
        migrations.AddField(
            model_name='projectchangelog',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
    ]
