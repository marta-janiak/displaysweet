# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_projectcsvtemplatefieldvalues_row_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectCSVTemplateImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('template', models.ForeignKey(to='core.ProjectCSVTemplate')),
            ],
        ),
        migrations.AddField(
            model_name='projectcsvtemplatefieldvalues',
            name='import_instance',
            field=models.ForeignKey(blank=True, to='core.ProjectCSVTemplateImport', null=True),
        ),
    ]
