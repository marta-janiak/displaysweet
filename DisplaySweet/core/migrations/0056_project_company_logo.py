# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0055_auto_20160414_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='company_logo',
            field=models.FileField(null=True, upload_to=b'project_logo', blank=True),
        ),
    ]
