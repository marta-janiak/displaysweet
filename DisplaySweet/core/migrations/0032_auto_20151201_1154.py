# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0031_auto_20151124_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='csvfileimport',
            name='table_name',
        ),
        migrations.AlterField(
            model_name='projectcsvtemplatefields',
            name='order',
            field=models.IntegerField(default=99, null=True, blank=True),
        ),
    ]
