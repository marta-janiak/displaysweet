# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20150930_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='floor_plan_location',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='key_plan_location',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='level_plan_location',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
    ]
