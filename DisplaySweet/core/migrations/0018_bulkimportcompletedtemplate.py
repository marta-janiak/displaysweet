# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import utils.secure_file_field
import utils.json_field


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20151109_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='BulkImportCompletedTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(default=b'draft', max_length=50, choices=[(b'draft', 'Draft'), (b'submitted', 'Submitted'), (b'processing_underway', 'Processing Underway'), (b'processing_failed', 'Processing Failed'), (b'processed', 'Processed')])),
                ('uploaded_xlsx', utils.secure_file_field.SecureFileField(help_text=b'The XLSX file containing the registration data', null=True, upload_to=b'bulkimport_xlsx_uploads', blank=True)),
                ('uploaded_data', utils.json_field.JSONField(help_text=b'The data contained in the registration data upload', null=True, blank=True)),
                ('notes', models.TextField(help_text=b'Notes from the user regarding file.', null=True, blank=True)),
                ('project', models.ForeignKey(help_text=b'The project this file was loaded for.', to='core.Project')),
                ('template', models.ForeignKey(help_text=b'The template this file was loaded for.', to='core.ProjectCSVTemplate')),
            ],
        ),
    ]
