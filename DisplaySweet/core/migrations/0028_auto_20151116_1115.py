# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0027_project_image_dimensions'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectimagepath',
            name='image_mode',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectimagepath',
            name='image_output',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
