# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0042_auto_20151229_2125'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcontainersettings',
            name='main_container_id',
            field=models.IntegerField(null=True),
        ),
    ]
