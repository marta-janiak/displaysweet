# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0064_projectversion_active_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectversion',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
    ]
