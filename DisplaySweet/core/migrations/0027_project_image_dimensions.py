# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20151112_0652'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='image_dimensions',
            field=models.FileField(null=True, upload_to=b'image_dimensions', blank=True),
        ),
    ]
