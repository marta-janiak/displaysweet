# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0044_auto_20160112_1708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectcsvtemplate',
            name='created',
            field=models.DateField(auto_now_add=True, null=True, auto_created=True),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplate',
            name='modified',
            field=models.DateTimeField(auto_now_add=True, null=True, auto_created=True),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplate',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),

    ]
