# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0057_projectsyncbacklog'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectsyncbacklog',
            name='logged',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='projectsyncbacklog',
            name='status_log',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectsyncbacklog',
            name='log',
            field=models.TextField(null=True, blank=True),
        ),
    ]
