# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20151110_1124'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectImagePath',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('image_type', models.CharField(max_length=255)),
                ('image_path', models.CharField(max_length=255)),
            ],
        ),
    ]
