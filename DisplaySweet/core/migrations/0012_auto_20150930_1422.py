# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_imagelocationconfiguration_configuration'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagelocationconfiguration',
            name='location_type',
            field=models.CharField(default=b'floor_plan_location', max_length=255, choices=[(b'floor_plan_location', b'Floor Plan Location'), (b'ipad_image_config', b'Ipad Floorplan location'), (b'projector_image_config', b'Projector Floorplan location')]),
        ),
        migrations.AlterField(
            model_name='imagelocationconfiguration',
            name='configuration',
            field=models.ForeignKey(related_name='image_configuration', blank=True, to='core.DisplaySweetConfiguration', null=True),
        ),
        migrations.AlterField(
            model_name='imagelocationconfiguration',
            name='image_file_location',
            field=models.CharField(help_text=b'Please include the full path to the project image directory as follows: /var/www/html/project_data/projects/', max_length=400, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='project',
            field=models.ForeignKey(related_name='project_connections', to='core.Project'),
        ),
    ]
