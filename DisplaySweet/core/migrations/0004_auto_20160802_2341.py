# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0003_projectversion_all_projects'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectFamily',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', models.DateField(auto_created=True, auto_now_add=True)),
                ('modified', models.DateTimeField(blank=True, null=True)),
                ('family_name', models.CharField(blank=True, null=True, max_length=50)),
                ('created_by', models.ForeignKey(blank=True, null=True, related_name='project_family_created_by', to=settings.AUTH_USER_MODEL)),
                ('last_modified_by', models.ForeignKey(blank=True, null=True, related_name='project_family_modified_by', to=settings.AUTH_USER_MODEL)),
                ('project_list', models.ManyToManyField(blank=True, null=True, related_name='projects_all_family', to='core.Project')),
            ],
        ),
        migrations.RemoveField(
            model_name='projectversion',
            name='all_projects',
        ),
    ]
