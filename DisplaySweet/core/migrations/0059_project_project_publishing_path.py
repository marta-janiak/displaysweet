# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0058_auto_20160429_0813'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='project_publishing_path',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
    ]
