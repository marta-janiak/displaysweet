# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0037_auto_20151210_1407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='display_suites',
        ),
        migrations.RemoveField(
            model_name='client',
            name='projects',
        ),
        migrations.DeleteModel(
            name='Client',
        ),
        migrations.DeleteModel(
            name='DisplaySuite',
        ),
    ]
