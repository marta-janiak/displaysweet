# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='description',
            field=models.TextField(help_text=b'Enter a project description.', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='database_file',
            field=models.FileField(help_text=b'Select the SQLite database to load. Only required for SQLite.', null=True, upload_to=b'', blank=True),
        ),
    ]
