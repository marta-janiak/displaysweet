# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0069_auto_20160617_0146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectconnection',
            name='path_to_database',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
    ]
