# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20160724_2146'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectversion',
            name='all_projects',
            field=models.ManyToManyField(to='core.Project', related_name='projects_all_version', blank=True, null=True),
        ),
    ]
