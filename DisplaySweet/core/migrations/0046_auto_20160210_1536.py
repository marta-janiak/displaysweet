# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0045_auto_20160118_0943'),
    ]

    operations = [
        migrations.CreateModel(
            name='FloorOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('floor_name', models.CharField(max_length=255, null=True, blank=True)),
                ('orderidx', models.IntegerField(default=0)),
            ],
        ),
        migrations.AddField(
            model_name='projectcsvtemplate',
            name='is_editable_template_default',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='projectcsvtemplatefields',
            name='is_aspects_field',
            field=models.BooleanField(default=False),
        ),
    ]
