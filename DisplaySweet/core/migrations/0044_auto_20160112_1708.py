# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0043_projectcontainersettings_main_container_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectcontainer',
            name='container_settings',
        ),
        migrations.RemoveField(
            model_name='projectcontainersettings',
            name='project',
        ),
        migrations.DeleteModel(
            name='ProjectContainer',
        ),
        migrations.DeleteModel(
            name='ProjectContainerSettings',
        ),
    ]
