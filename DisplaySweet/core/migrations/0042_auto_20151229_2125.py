# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0041_auto_20151229_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcontainer',
            name='orderidx',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='projectcontainer',
            name='container_settings',
            field=models.ForeignKey(related_name='project_containers', to='core.ProjectContainerSettings'),
        ),
    ]
