# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0060_auto_20160605_1056'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modified', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, auto_created=True)),
                ('active_version', models.FloatField(null=True, blank=True)),
                ('status', models.CharField(default=b'in_progress', max_length=50, choices=[(b'in_progress', b'In Progress'), (b'staging', b'Staging'), (b'testing', b'Test'), (b'not_active', b'Not Active'), (b'live', b'Live')])),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('project', models.ForeignKey(blank=True, to='core.Project', null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='projectimagepath',
            name='modified',
            field=models.DateTimeField(null=True, auto_created=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectsyncbacklog',
            name='modified',
            field=models.DateTimeField(null=True, auto_created=True, blank=True),
        ),
    ]
