# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('name', models.CharField(help_text=b'Enter a project name.', max_length=50, null=True, blank=True)),
                ('description', models.TimeField(help_text=b'Enter a project description.', null=True, blank=True)),
                ('installed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectConnection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('connection_type', models.CharField(max_length=50, choices=[(b'sqlite', b'SQLite'), (b'postgres', b'PostgresSQL')])),
                ('database_file', models.FileField(help_text=b'Select the SQLite database to load.', null=True, upload_to=b'', blank=True)),
                ('path_to_database', models.CharField(max_length=400, null=True, blank=True)),
                ('username', models.CharField(max_length=30, null=True, blank=True)),
                ('password', models.CharField(max_length=50, null=True, blank=True)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
        ),
    ]
