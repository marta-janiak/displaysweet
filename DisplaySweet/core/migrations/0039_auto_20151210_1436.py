# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_auto_20151210_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='number_of_buildings',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_address_same_as_physical',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_postcode',
            field=models.CharField(help_text=b'Enter the postal address postcode.', max_length=255, null=True, verbose_name=b'Postcode', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_state',
            field=models.CharField(help_text=b'Enter the postal address state.', max_length=255, null=True, verbose_name=b'State', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_street_name',
            field=models.CharField(help_text=b'Enter the postal address street name.', max_length=255, null=True, verbose_name=b'Street name', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_street_number',
            field=models.CharField(help_text=b'Enter the postal address street number.', max_length=255, null=True, verbose_name=b'Street number', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_street_suffix',
            field=models.CharField(help_text=b'Enter the postal address street suffix.', max_length=255, null=True, verbose_name=b'Street suffix', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_street_type',
            field=models.CharField(help_text=b'Enter the postal address street type.', max_length=255, null=True, verbose_name=b'Street type', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postal_suburb',
            field=models.CharField(help_text=b'Enter the postal address suburb.', max_length=255, null=True, verbose_name=b'Suburb', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='postcode',
            field=models.CharField(help_text=b'Enter the postcode.', max_length=255, null=True, verbose_name=b'Postcode', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='state',
            field=models.CharField(default=b'VIC', help_text=b'Enter the state.', max_length=255, verbose_name=b'State'),
        ),
        migrations.AddField(
            model_name='project',
            name='street_name',
            field=models.CharField(help_text=b'Enter the street name.', max_length=255, null=True, verbose_name=b'Street name', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='street_number',
            field=models.CharField(help_text=b'Enter the street number.', max_length=255, null=True, verbose_name=b'Street number', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='street_suffix',
            field=models.CharField(help_text=b'Enter the street suffix.', max_length=255, null=True, verbose_name=b'Street suffix', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='street_type',
            field=models.CharField(help_text=b'Enter the street type.', max_length=255, null=True, verbose_name=b'Street type', blank=True),
        ),
        migrations.AddField(
            model_name='project',
            name='suburb',
            field=models.CharField(help_text=b'Enter the suburb.', max_length=255, null=True, verbose_name=b'Suburb', blank=True),
        ),
    ]
