# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0051_projectcsvtemplatefields_column_csv_table_mapping'),
    ]

    operations = [
        migrations.AlterField(
            model_name='csvfileimportfields',
            name='csv_import',
            field=models.ForeignKey(related_name='csv_template_fields', to='core.CSVFileImport'),
        ),
        migrations.AlterField(
            model_name='project',
            name='users',
            field=models.ManyToManyField(related_name='project_users_list', null=True, to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplatefields',
            name='template',
            field=models.ForeignKey(related_name='project_template_fields', to='core.ProjectCSVTemplate'),
        ),
    ]
