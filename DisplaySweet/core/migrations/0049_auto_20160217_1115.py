# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_auto_20160212_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='csvfileimportfields',
            name='is_aspects_field',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='csvfileimportfields',
            name='order',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectcsvtemplatefields',
            name='order',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
    ]
