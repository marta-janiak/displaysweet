# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0071_auto_20160712_2228'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectTranslations',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True, auto_created=True)),
                ('created', models.DateTimeField(auto_now_add=True, auto_created=True)),
                ('database_file', models.FileField(null=True, blank=True, upload_to='translation_databases')),
                ('apartment_translation_file', models.FileField(null=True, blank=True, upload_to='translations_apartment')),
                ('sold_status_translation_file', models.FileField(null=True, blank=True, upload_to='translations_sold_status')),
                ('projector_canvas_name', models.CharField(null=True, blank=True, max_length=255)),
                ('kwall_canvas_name', models.CharField(null=True, blank=True, max_length=255)),
                ('proj_canvas_name', models.CharField(null=True, blank=True, max_length=255)),
                ('version_published', models.FloatField(default=1.0)),
                ('version_prefix_count', models.FloatField(default=1.0)),
                ('create_slim_database', models.BooleanField(default=False)),
                ('apartment_duplicates', models.BooleanField(help_text='Do apartments have duplicate names?', default=False)),
                ('published_to', models.CharField(null=True, choices=[(1, 'cmsstaging.displaysweet.com'), (2, 'admin.displaysweet.com')], blank=True, max_length=400)),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='project',
            name='apartment_translation_file',
            field=models.FileField(null=True, blank=True, upload_to='apartment_translations'),
        ),
        migrations.AddField(
            model_name='project',
            name='sold_status_translation_file',
            field=models.FileField(null=True, blank=True, upload_to='sold_status_translations'),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='project',
            field=models.ForeignKey(blank=True, to='core.Project', null=True, related_name='translations'),
        ),
    ]
