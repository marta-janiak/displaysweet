# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0065_projectversion_project'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='version_family',
            field=models.ForeignKey(related_name='version_list', on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.ProjectVersion', null=True),
        ),
        migrations.AlterField(
            model_name='projectchangelog',
            name='project',
            field=models.ForeignKey(related_name='change_log', on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='projectnotes',
            name='project',
            field=models.ForeignKey(related_name='notes_list', on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='projectversion',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.Project', null=True),
        ),
    ]
