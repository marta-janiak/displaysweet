# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_projectimagepath'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectimagepath',
            name='project',
            field=models.ForeignKey(default=1, to='core.Project'),
            preserve_default=False,
        ),
    ]
