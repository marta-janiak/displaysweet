# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150902_0703'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='project_connection_name',
            field=models.CharField(help_text=b'Enter a project connection name. This must be unique with no spaces.', max_length=50, unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='created',
            field=models.DateField(auto_now_add=True, auto_created=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='database_file',
            field=models.FileField(help_text=b'Select the SQLite database to load. Only required for SQLite.', null=True, upload_to=b'database_files', blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='password',
            field=models.CharField(help_text=b'Only required for PostgreSQL connection', max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='username',
            field=models.CharField(help_text=b'Only required for PostgreSQL connection', max_length=30, null=True, blank=True),
        ),
    ]
