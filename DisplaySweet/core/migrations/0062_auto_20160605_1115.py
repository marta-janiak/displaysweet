# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0061_auto_20160605_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectversion',
            name='current',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='projectversion',
            name='project',
            field=models.ForeignKey(related_name='version_list', blank=True, to='core.Project', null=True),
        ),
    ]
