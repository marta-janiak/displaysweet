# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0056_project_company_logo'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectSyncBackLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('modified', models.DateTimeField(null=True, blank=True)),
                ('direction', models.CharField(max_length=255, null=True, blank=True)),
                ('log', models.TextField(max_length=255, null=True, blank=True)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
        ),
    ]
