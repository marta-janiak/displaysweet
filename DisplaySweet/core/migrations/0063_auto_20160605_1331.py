# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0062_auto_20160605_1115'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='parent_version',
        ),
        migrations.RemoveField(
            model_name='projectversion',
            name='active_version',
        ),
        migrations.RemoveField(
            model_name='projectversion',
            name='current',
        ),
        migrations.RemoveField(
            model_name='projectversion',
            name='project',
        ),
        migrations.RemoveField(
            model_name='projectversion',
            name='status',
        ),
        migrations.AddField(
            model_name='project',
            name='version_family',
            field=models.ForeignKey(related_name='version_list', blank=True, to='core.ProjectVersion', null=True),
        ),
        migrations.AddField(
            model_name='projectversion',
            name='project_name',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectchangelog',
            name='project',
            field=models.ForeignKey(related_name='change_log', blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='projectnotes',
            name='project',
            field=models.ForeignKey(related_name='notes_list', blank=True, to='core.Project', null=True),
        ),
    ]
