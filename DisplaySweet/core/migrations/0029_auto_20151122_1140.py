# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_auto_20151116_1115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectimagepath',
            name='image_mode',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectimagepath',
            name='image_output',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectimagepath',
            name='image_type',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
