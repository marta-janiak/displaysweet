# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_auto_20151110_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplatefieldvalues',
            name='row_id',
            field=models.IntegerField(default=1),
        ),
    ]
