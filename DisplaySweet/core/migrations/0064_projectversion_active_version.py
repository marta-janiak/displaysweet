# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0063_auto_20160605_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectversion',
            name='active_version',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
