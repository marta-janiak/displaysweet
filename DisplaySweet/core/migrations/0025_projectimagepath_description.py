# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_projectimagepath_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectimagepath',
            name='description',
            field=models.CharField(default='', max_length=400),
            preserve_default=False,
        ),
    ]
