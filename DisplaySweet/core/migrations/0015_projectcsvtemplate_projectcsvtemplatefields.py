# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_project_level_plan_location_full_res'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectCSVTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('template_name', models.CharField(max_length=255, null=True, blank=True)),
                ('project', models.ForeignKey(to='core.Project')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectCSVTemplateFields',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateField(auto_now_add=True, auto_created=True)),
                ('column_table_mapping', models.CharField(max_length=255, null=True, blank=True)),
                ('column_field_mapping', models.CharField(max_length=255, null=True, blank=True)),
                ('column_pretty_name', models.CharField(max_length=255, null=True, blank=True)),
                ('template', models.ForeignKey(to='core.ProjectCSVTemplate')),
            ],
        ),
    ]
