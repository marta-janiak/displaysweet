# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0040_projectcontainer_projectcontainersettings'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectcontainer',
            name='container_id',
        ),
        migrations.RemoveField(
            model_name='projectcontainer',
            name='project',
        ),
        migrations.AddField(
            model_name='projectcontainer',
            name='container_settings',
            field=models.ForeignKey(default=1, to='core.ProjectContainerSettings'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='projectcontainersettings',
            name='container_id',
            field=models.IntegerField(null=True),
        ),
    ]
