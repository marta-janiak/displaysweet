# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0068_auto_20160609_0126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='version_family',
            field=models.ForeignKey(related_name='version_list', blank=True, to='core.ProjectVersion', null=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='path_to_database',
            field=models.CharField(help_text=b'Path to sqlite database', max_length=400, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectversion',
            name='project',
            field=models.ForeignKey(related_name='version_history', blank=True, to='core.Project', null=True),
        ),
    ]
