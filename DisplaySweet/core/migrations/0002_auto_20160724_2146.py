# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_auto_20160724_2126'),
    ]

    operations = [
        migrations.AddField(
            model_name='projecttranslations',
            name='project_cms_chinese_image',
            field=models.FileField(blank=True, upload_to='translated_cms_chinese_images', null=True),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='project_cms_image',
            field=models.FileField(blank=True, upload_to='translated_cms_images', null=True),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='project_loading_image',
            field=models.FileField(blank=True, upload_to='translated_loading_images', null=True),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='project_select_chinese_image',
            field=models.FileField(blank=True, upload_to='translated_select_chinese_images', null=True),
        ),
        migrations.AddField(
            model_name='projecttranslations',
            name='project_select_image',
            field=models.FileField(blank=True, upload_to='translated_select_images', null=True),
        ),
    ]
