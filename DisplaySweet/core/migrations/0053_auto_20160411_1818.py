# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0052_auto_20160217_1445'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplate',
            name='is_allocation_template',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='displaysweetconfiguration',
            name='image_locations',
            field=models.ManyToManyField(to='core.ImageLocationConfiguration'),
        ),
    ]
