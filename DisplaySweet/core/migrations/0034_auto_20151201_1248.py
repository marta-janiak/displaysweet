# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_csvfileimportfields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='csvfileimportfields',
            name='project',
        ),
        migrations.AlterField(
            model_name='csvfileimportfields',
            name='csv_import',
            field=models.ForeignKey(related_name='template_fields', to='core.CSVFileImport'),
        ),
    ]
