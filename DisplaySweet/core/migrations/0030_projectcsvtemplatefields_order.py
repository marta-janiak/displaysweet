# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_auto_20151122_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplatefields',
            name='order',
            field=models.IntegerField(default=0),
        ),
    ]
