# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', 'add_updated_script'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='project_cms_chinese_image',
            field=models.FileField(blank=True, null=True, upload_to='project_cms_chinese_images'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_cms_image',
            field=models.FileField(blank=True, null=True, upload_to='project_cms_images'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_loading_image',
            field=models.FileField(blank=True, null=True, upload_to='project_loading_images'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_select_chinese_image',
            field=models.FileField(blank=True, null=True, upload_to='project_select_chinese_images'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_select_image',
            field=models.FileField(blank=True, null=True, upload_to='project_select_images'),
        ),
        migrations.AlterField(
            model_name='project',
            name='status',
            field=models.CharField(default='staging', max_length=50, choices=[('not_active', 'Inactive'), ('staging', 'Staging'), ('live', 'Live')]),
        ),
    ]
