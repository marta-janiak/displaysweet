# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0049_auto_20160217_1115'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplate',
            name='csv_file_import',
            field=models.ForeignKey(blank=True, to='core.CSVFileImport', null=True),
        ),
    ]
