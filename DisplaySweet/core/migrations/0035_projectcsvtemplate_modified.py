# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0034_auto_20151201_1248'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcsvtemplate',
            name='modified',
            field=models.DateTimeField(null=True, blank=True, auto_now_add=True, auto_created=True),
            preserve_default=False,
        ),
    ]
