# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_auto_20160411_1818'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='project_copied_from',
            field=models.ForeignKey(blank=True, to='core.Project', null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='modified',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        # migrations.AlterField(
        #     model_name='project',
        #     name='users',
        #     field=models.ManyToManyField(related_name='project_users_list', to=settings.AUTH_USER_MODEL, blank=True),
        # ),
    ]
