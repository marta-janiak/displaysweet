# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import utils.secure_file_field
import utils.json_field


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0070_auto_20160623_2002'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='selected_script',
            field=models.ForeignKey(blank=True, to='core.ProjectScripts', null=True, related_name='selected_project_script'),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='notes',
            field=models.TextField(blank=True, null=True, help_text='Notes from the user regarding file.'),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='project',
            field=models.ForeignKey(help_text='The project this file was loaded for.', to='core.Project'),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='status',
            field=models.CharField(max_length=50, choices=[('draft', 'Draft'), ('submitted', 'Submitted'), ('processing_underway', 'Processing Underway'), ('processing_failed', 'Processing Failed'), ('processed', 'Processed')], default='draft'),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='table_name',
            field=models.CharField(max_length=255, null=True, help_text='The table this file was loaded for.', blank=True),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='uploaded_data',
            field=utils.json_field.JSONField(blank=True, null=True, help_text='The data contained in the registration data upload'),
        ),
        migrations.AlterField(
            model_name='bulkimport',
            name='uploaded_xlsx',
            field=utils.secure_file_field.SecureFileField(upload_to='bulkimport_xlsx_uploads', null=True, help_text='The XLSX file containing the registration data', blank=True),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='notes',
            field=models.TextField(blank=True, null=True, help_text='Notes from the user regarding file.'),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='project',
            field=models.ForeignKey(help_text='The project this file was loaded for.', to='core.Project'),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='status',
            field=models.CharField(max_length=50, choices=[('draft', 'Draft'), ('submitted', 'Submitted'), ('processing_underway', 'Processing Underway'), ('processing_failed', 'Processing Failed'), ('processed', 'Processed')], default='draft'),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='template',
            field=models.ForeignKey(blank=True, to='core.ProjectCSVTemplate', null=True, help_text='The template this file was loaded for.'),
        ),
        migrations.AlterField(
            model_name='bulkimportcompletedtemplate',
            name='uploaded_xlsx',
            field=utils.secure_file_field.SecureFileField(upload_to='bulkimport_xlsx_uploads', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='csvfileimport',
            name='csv_file',
            field=models.FileField(upload_to='csv_import_files'),
        ),
        migrations.AlterField(
            model_name='imagelocationconfiguration',
            name='image_file_location',
            field=models.CharField(max_length=400, null=True, help_text='Please include the full path to the project image directory as follows: /var/www/html/project_data/projects/', blank=True),
        ),
        migrations.AlterField(
            model_name='imagelocationconfiguration',
            name='location_type',
            field=models.CharField(max_length=255, choices=[('floor_plan_location', 'Floor Plan Location'), ('ipad_image_config', 'Ipad Floorplan location'), ('projector_image_config', 'Projector Floorplan location')], default='floor_plan_location'),
        ),
        migrations.AlterField(
            model_name='project',
            name='company_logo',
            field=models.FileField(upload_to='project_logo', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='description',
            field=models.TextField(blank=True, null=True, help_text='Enter a project description.'),
        ),
        migrations.AlterField(
            model_name='project',
            name='name',
            field=models.CharField(max_length=50, null=True, help_text='Enter a project name.', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_postcode',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address postcode.', verbose_name='Postcode', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_state',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address state.', verbose_name='State', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_street_name',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address street name.', verbose_name='Street name', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_street_number',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address street number.', verbose_name='Street number', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_street_suffix',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address street suffix.', verbose_name='Street suffix', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_street_type',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address street type.', verbose_name='Street type', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postal_suburb',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postal address suburb.', verbose_name='Suburb', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='postcode',
            field=models.CharField(max_length=255, null=True, help_text='Enter the postcode.', verbose_name='Postcode', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='project_connection_name',
            field=models.CharField(unique=True, max_length=50, null=True, help_text='Enter a project connection name. This must be unique with no spaces.', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='state',
            field=models.CharField(max_length=255, help_text='Enter the state.', verbose_name='State', default='VIC'),
        ),
        migrations.AlterField(
            model_name='project',
            name='status',
            field=models.CharField(max_length=50, choices=[('in_progress', 'In Progress'), ('staging', 'Staging'), ('testing', 'Test'), ('not_active', 'Inactive'), ('live', 'Live')], default='in_progress'),
        ),
        migrations.AlterField(
            model_name='project',
            name='street_name',
            field=models.CharField(max_length=255, null=True, help_text='Enter the street name.', verbose_name='Street name', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='street_number',
            field=models.CharField(max_length=255, null=True, help_text='Enter the street number.', verbose_name='Street number', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='street_suffix',
            field=models.CharField(max_length=255, null=True, help_text='Enter the street suffix.', verbose_name='Street suffix', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='street_type',
            field=models.CharField(max_length=255, null=True, help_text='Enter the street type.', verbose_name='Street type', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='suburb',
            field=models.CharField(max_length=255, null=True, help_text='Enter the suburb.', verbose_name='Suburb', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='version',
            field=models.FloatField(blank=True, null=True, help_text='The version number of this instance', default=1),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='connection_type',
            field=models.CharField(max_length=50, choices=[('sqlite', 'SQLite'), ('postgres', 'PostgresSQL')]),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='database_file',
            field=models.FileField(upload_to='database_files', null=True, help_text='Select the SQLite database to load. Only required for SQLite.', blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='password',
            field=models.CharField(max_length=50, null=True, help_text='Only required for PostgreSQL connection', blank=True),
        ),
        migrations.AlterField(
            model_name='projectconnection',
            name='username',
            field=models.CharField(max_length=30, null=True, help_text='Only required for PostgreSQL connection', blank=True),
        ),
        migrations.AlterField(
            model_name='projectnotes',
            name='document',
            field=models.FileField(upload_to='project_comment_documents', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='projectscripts',
            name='file',
            field=models.FileField(upload_to='project_scripts', null=True, blank=True),
        ),
    ]
