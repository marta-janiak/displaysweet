import datetime
import arrow
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.shortcuts import redirect
from django.utils.html import escape as escape_html
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView as BaseDetailView
from django.views.generic.edit import UpdateView as BaseUpdateView
from django.views.generic.edit import DeleteView as BaseDeleteView
from django.views.generic.edit import CreateView as BaseCreateView
from django.views.generic.base import TemplateView as BaseTemplateView
from django.views.generic.edit import FormMixin
from django.core.urlresolvers import reverse
from django.contrib import messages
from utils.permissions import user_admin_required



class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class AdminRequiredMixin(object):
    @method_decorator(user_admin_required)
    def dispatch(self, *args, **kwargs):
        return super(AdminRequiredMixin, self).dispatch(*args, **kwargs)


class ListView(LoginRequiredMixin, BaseTemplateView):
    callback_view_class = None
    filter_form_class = None
    model_object = None

    def get_context_data(self, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)

        context['columns'] = self.callback_view_class().column_dict

        if self.filter_form_class:
            context['filter_form'] = self.filter_form_class()

        return context


class ListCallbackView(LoginRequiredMixin, BaseDatatableView):
    """
    django-datatables-view does a pretty good job of handling
    all the various crap that datatables needs in a json response,
    but this class add some things we needed on top of that. These
    include:

    - Proper formatting of datetimes
    - XSS protection
    - Calling of callables
    - Filter form integration
    - A configurable 'actions' column
    - Column prefixes
    """
    filter_form_class = None
    column_prefixes = None

    @property
    def columns(self):
        return [c[1] for c in self.column_details]

    @property
    def order_columns(self):
        return [c[2] for c in self.column_details]

    @property
    def column_dict(self):
        return [{'label': c[0], 'selector': c[1], 'ordering': c[2], 'hidden': c[3]} for c in self.column_details]

    def filter_queryset(self, qs):
        if self.filter_form_class:
            filter_form = self.filter_form_class(self.request.GET)

            if filter_form.is_valid():
                qs = filter_form.apply_filter(qs)
            else:
                qs = qs.none()

        return qs

    def render_column(self, row, column):
        """
        Renders a column on a row
        """
        if column == '_actions':
            return self.render_actions(row)

        if hasattr(row, 'get_%s_display' % column):
            # It's a choice field
            text = getattr(row, 'get_%s_display' % column)()
        else:
            try:
                text = getattr(row, column)
            except AttributeError:
                obj = row
                for part in column.split('.'):
                    if obj is None:
                        break
                    obj = getattr(obj, part)

                text = obj

        # Call callables
        if hasattr(text, '__call__'):
            text = text()

        if hasattr(text, 'strftime'):
            text = self.render_datetime(text)

        text = escape_html(text)

        if self.column_prefixes and column in self.column_prefixes:
            text = u'%s%s' % (self.column_prefixes[column], text)

        return text

    def render_actions(self, obj):
        if hasattr(obj, 'get_absolute_url'):
            return '<a href="%s">View</a>' % obj.get_absolute_url()
        else:
            return ''

    def render_datetime(self, dt):
        format = "YYYY-MM-DD, H:mma"

        return arrow.get(dt).format(format)


class ProjectListCallbackView(LoginRequiredMixin, BaseDatatableView):
    """
    Extending Project List Call Back View as I expect there to many customisations
    """
    filter_form_class = None
    column_prefixes = None

    @property
    def columns(self):
        return [c[1] for c in self.column_details]

    @property
    def order_columns(self):
        return [c[2] for c in self.column_details]

    @property
    def column_dict(self):
        return [{'label': c[0], 'selector': c[1], 'ordering': c[2], 'hidden': c[3]} for c in self.column_details]

    def filter_queryset(self, qs):
        if self.filter_form_class:
            filter_form = self.filter_form_class(self.request.GET)

            if filter_form.is_valid():
                qs = filter_form.apply_filter(qs)
            else:
                qs = qs.none()

        qs = qs.filter(users__pk__contains=self.request.user.pk)
        return qs

    def render_column(self, row, column):
        """
        Renders a column on a row
        """
        if column == '_actions':
            return self.render_actions(row)

        if hasattr(row, 'get_%s_display' % column):
            # It's a choice field
            text = getattr(row, 'get_%s_display' % column)()
        else:
            try:
                text = getattr(row, column)
            except AttributeError:
                obj = row
                for part in column.split('.'):
                    if obj is None:
                        break
                    obj = getattr(obj, part)

                text = obj

        # Call callables
        if hasattr(text, '__call__'):
            text = text()

        if hasattr(text, 'strftime'):
            text = self.render_datetime(text)

        text = escape_html(text)

        if self.column_prefixes and column in self.column_prefixes:
            text = u'%s%s' % (self.column_prefixes[column], text)

        return text

    def render_actions(self, obj):
        if hasattr(obj, 'get_absolute_url'):
            return '<a href="%s">View</a>' % obj.get_absolute_url()
        else:
            return ''

    def render_datetime(self, dt):
        format = "YYYY-MM-DD, H:mma"

        return arrow.get(dt).format(format)

class DetailView(LoginRequiredMixin, BaseDetailView):
    navigation_view = None

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        return context

    def reverse_action_urls(self, actions):
        """
        A little helper function to make the creation of action
        lists a bit cleaner. Means you can write an action as:

        ('Action Name', 'action_url_name')

        Rather than:

        ('Action Name', reverse('action_url_name', args=[obj.pk]))
        """
        reversed_actions = []
        obj = self.get_object()

        for action_name, url_name in actions:
            reversed_actions.append((action_name, reverse(url_name, args=[obj.pk])))

        return reversed_actions


class ActionView(FormMixin, DetailView):
    form_class = None
    success_message = "Action completed successfully"
    error_message = "There was a problem with your submission. Please check the form below for errors"

    def get_context_data(self, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        context = super(ActionView, self).get_context_data(**kwargs)
        context['form'] = form

        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, request, args, kwargs)

    def form_valid(self, form):
        if self.success_message:
            messages.success(self.request, self.success_message)

        self.perform_action(form)

        return redirect(self.get_object().get_absolute_url())

    def form_invalid(self, form, request, args, kwargs):
        if self.error_message:
            messages.error(request, self.success_message)

        return self.get(request, *args, **kwargs)

    def perform_action(self, form):
        raise NotImplementedError


class VersionView(LoginRequiredMixin, BaseDetailView):
    def get_version_idx(self):
        try:
            return int(self.kwargs['version_idx'])
        except:
            return None

    def get_model_instance(self):
        obj = self.get_object()
        return obj

    def get_version(self):
        obj = self.get_object()
        return obj.get_version(self.get_version_idx())

    def get_version_object(self):
        obj = self.get_object()
        return obj.get_object_version(self.get_version_idx())

    def get_last_update_compared(self):
        version_idx = self.get_version_idx()
        obj = self.get_version_object()

        try:
            return obj.get_last_update_compared(version_idx)
        except:
            return ""

    def get_context_data(self, **kwargs):
        context = super(VersionView, self).get_context_data(**kwargs)

        # context['version_idx'] = self.get_version_idx()
        # context['version_object'] = self.get_version_object()
        # context['version'] = self.get_version()
        # context['last_update_compared'] = self.get_last_update_compared()
        # context['model_instance'] = self.get_model_instance()


        return context


class CreateView(LoginRequiredMixin, BaseCreateView):
    pass


class UpdateView(LoginRequiredMixin, BaseUpdateView):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        form_class = self.get_form_class()

        form = self.get_form(form_class)
        formsets = self.get_formsets()

        return self.render_to_response(self.get_context_data(form=form, formsets=formsets))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        form_class = self.get_form_class()

        form = self.get_form(form_class)
        formsets = self.get_formsets()

        if form.is_valid() and self.validate_formsets(formsets):
            return self.form_valid(form, formsets)
        else:
            return self.form_invalid(form, formsets)

    def get_formsets(self):
        return {}

    def validate_formsets(self, formsets):
        invalid_formsets = False

        for formset_name, formset in formsets.items():
            if not formset.is_valid():
                invalid_formsets = True

        return not invalid_formsets

    def form_valid(self, form, formsets):
        self.formsets_valid(formsets)
        return super(BaseUpdateView, self).form_valid(form)

    def formsets_valid(self, formsets):
        for formset_name, formset in formsets.items():
            formset.save()

        return

    def form_invalid(self, form, formsets):
        return self.render_to_response(self.get_context_data(form=form, formsets=formsets))


class DeleteView(LoginRequiredMixin, BaseDeleteView):
    pass


class TemplateView(LoginRequiredMixin, BaseTemplateView):
    pass
