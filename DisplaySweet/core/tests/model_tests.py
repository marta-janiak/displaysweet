from django.test import TestCase
from core.models import Project, ProjectCSVTemplate, CSVFileImport
from django.core.files.uploadedfile import SimpleUploadedFile
from os import listdir
import os
import shutil
from django.conf import settings

from os.path import isfile, join
from projects.models.model_mixin import PrimaryModelMixin
from django.core.files import File as DjangoFile
import datetime
from django.core.files import File


class ProjectTestCase(TestCase):
    fixtures = ['project_testdata.json']

    def setUp(self):
        super(ProjectTestCase, self).setUp()
        self.project = Project.objects.get(pk=1)

    def test_was_created_today(self):
        #project should already be in the db
        self.assertFalse(self.project.created is datetime.datetime.today())

    def test_project_date(self):
        project = Project.objects.create(
            name="Test Project",
            active=True,
            status=Project.STAGING
        )

        self.assertEqual(project.created.today(), datetime.date.today())

    def test_must_have_a_name(self):
        project = self.project
        project.name = ''
        self.assertFalse(project.save())


class CSVFileImportTestCase(TestCase):
    fixtures = ['texttcategory_testdata.json', 'names_testdata.json', 'floor_testdata.json',
                'propertytype_test_data.json', 'property_testdata.json',
                'project_testdata.json', 'project_csv_template_tesdata.json', ]

    def setUp(self):
        super(CSVFileImportTestCase, self).setUp()
        self.csv_files = [f for f in listdir("core/tests/csv_import_files/")]
        self.project = Project.objects.get(pk=1)
        self.model_mixin = PrimaryModelMixin(request=None, project_id=self.project.pk)
        self.created_imports = []

    def test_can_save_with_file(self):

        for file in self.csv_files:
            file_to_test = os.path.join("core/tests/csv_import_files/", file)
            file_path = os.path.join(settings.PROJECT_ROOT, file_to_test)
            file_name = file_to_test.split('/')[-1]
            csv_file = File(open(file_path, "rb"))
            csv_import = CSVFileImport.objects.create(project=self.project)
            csv_import.csv_file.save(file_name, csv_file)
            csv_import.save()

            self.created_imports.append(csv_import)
            self.assertTrue(csv_import.has_csv_file())

    def test_import_file_has_headers(self):
        for import_file in self.created_imports:
            self.assertIsNotNone(import_file.get_file_headers())

    def test_can_run_mapping_from_files(self):
        for import_file in self.created_imports:
            import_file.create_field_mappings(self.model_mixin)

            self.assertEqual(import_file.has_mapped_all_fields(), 0)

    def test_has_mapped_all_field_values(self):
        self.assertIsNotNone(self.created_imports)

        for file in self.csv_files:
            file_to_test = os.path.join("core/tests/csv_import_files/", file)
            file_path = os.path.join(settings.PROJECT_ROOT, file_to_test)
            csv_file = File(open(file_path, "rb"))

            import_templates = ProjectCSVTemplate.objects.filter(project=self.project)

            for template in import_templates:
                csv_file.copy_to_import_template_fields(template=template)
                self.assertEqual(csv_file.has_mapped_all_field_values(template=template), 0)


class CSVFileImportFieldsTestCase(TestCase):
    pass


class BulkImportCompletedTemplateTestCase(TestCase):
    pass


class ProjectSyncBackLogTestCase(TestCase):
    pass


class ProjectVersionTestCase(TestCase):
    pass


class ProjectFamilyTestCase(TestCase):
    pass


class ProjectNotesTestCase(TestCase):
    pass


class ProjectChangeLogTestCase(TestCase):
    pass


class ProjectScriptsTestCase(TestCase):
    pass


class ProjectTranslationsTestCase(TestCase):
    pass

