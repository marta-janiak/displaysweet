import datetime
import time

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from core.models import Project, ProjectVersion
from .forms import PasswordResetForm
from django.contrib import messages
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from projects.models.model_mixin import PrimaryModelMixin


def force_404(request):
    template = "404.html"
    return render(request, template, {})


def login(request):
    template = "login.html"

    context = {

    }

    return render(request, template, context)


@login_required
def index(request):
    template = "index.html"

    projects = request.user.project_users_list.all()

    context = {}
    if projects:
        project = projects[0]
        context.update({
            'project': project
        })

    return render(request, template, context)


def reset_password(request):
    template = "registration/password_reset_form.html"

    form = PasswordResetForm()
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            messages.success(request, "An email has been sent to the email address "
                                      "you have provided for you to reset your password.")
            form.send_email()

            return redirect('home')

    context = {
        'form': form
    }

    return render(request, template, context)