import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:
            # canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            update_all_success = True
            print ('updating project: ', project)
            print ('------ PROPERTY RESOURCES TABLE UPDATE BEGIN ------')

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print ('Failed to begin db connection: ', e)
                update_all_success = False
                print ('')
                continue

            try:
                cursor.execute("drop table property_resources_temp;")
            except Exception as e:
                pass

            print ('creating temporary proprties table')

            created_temp_property_table = True

            new_proprty_temp_table_script = "CREATE TABLE property_resources_temp (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                            "property_id INTEGER REFERENCES properties (id), " \
                                            "resource_id INTEGER REFERENCES resources (id), " \
                                            "key VARCHAR(50), orderidx INTEGER, caption VARCHAR(250) NULL);"


            # print ('new_proprty_temp_table_script')
            # print (new_proprty_temp_table_script)

            try:
                cursor.execute(new_proprty_temp_table_script)
            except Exception as e:
                created_temp_property_table = False
                update_all_success = False
                print ('ERROR: Failed to create property temp table: ', e)

            if created_temp_property_table:
                import_data_success = True
                print ('Trying to import current property information to temp table')

                new_proprty_temp_table_script = "INSERT INTO property_resources_temp (id, property_id, resource_id, key, orderidx ) " \
                                                "SELECT id, property_id, resource_id, key, 1 as orderidx from property_resources;"

                try:
                    cursor.execute(new_proprty_temp_table_script)
                except Exception as e:
                    update_all_success = False
                    import_data_success = False
                    print ('ERROR: failed to import data to property temp table:', e)


                if import_data_success:
                    print ('Imported data successfully')

                    print ('--- deleting current property table ')
                    drop_table_script = "drop table property_resources;"

                    drop_table_success = True
                    try:
                        cursor.execute(drop_table_script)
                    except Exception as e:
                        drop_table_success = False
                        print ('Failed to drop property_resources table: ', e)

                    if drop_table_success:

                        created_property_table = True
                        new_proprty_table_script = "CREATE TABLE property_resources (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                                    "property_id INTEGER REFERENCES properties (id), " \
                                                    "resource_id INTEGER REFERENCES resources (id), " \
                                                    "key VARCHAR(50), orderidx INTEGER, caption VARCHAR(250) NULL);"

                        try:
                            cursor.execute(new_proprty_table_script)
                        except Exception as e:
                            created_property_table = False
                            update_all_success = False
                            print ("ERROR: Failed to create property table", e)

                        if created_property_table:
                            import_data_success = True
                            import_data_script = "INSERT INTO property_resources (id, property_id, resource_id, key, orderidx ) " \
                                                 "SELECT id, property_id, resource_id, key, 1 as orderidx from property_resources_temp;"

                            try:
                                cursor.execute(import_data_script)
                            except Exception as e:
                                import_data_success = False
                                update_all_success = False
                                print ('ERROR: Failed to import data to property_resources table:', e)

                            if import_data_success:
                                print ('==== SUCCESS: completed update of order field to property_resources table')

            print ('------ PROPERTY RESOURCES UPDATE END ------ ')
            print ('')

            try:
                file_path = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
                copy_file_path = os.path.join(settings.PROJECT_ROOT,
                                              "projects/project_folders/%s/models.py" % project.project_connection_name)

                shutil.copy2(file_path, copy_file_path)

                print ("SUCCESS: copied models py file")
            except Exception as e:
                update_all_success = False
                print ('ERROR: failed to update models file: ', e)


            print ('======= Updated models file')