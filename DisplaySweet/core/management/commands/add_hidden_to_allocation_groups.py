import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:
            # canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            update_all_success = True
            print ('updating project: ', project)
            print('------ PROPERTY TABLE UPDATE BEGIN ------')

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print('Failed to begin db connection: ', e)
                update_all_success = False
                print('')
                continue

            try:
                cursor.execute("drop table allocation_groups_temp;")
            except Exception as e:
                pass


            print('=-=-=-=- ALLOCATION GROUPS TABLE UPDATE BEGIN =-=-=-=-')

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print('Failed to begin db connection: ', e)
                update_all_success = False
                print('')
                continue





            print('creating temporary allocation groups table')

            created_temp_property_table = True
            new_proprty_temp_table_script = "CREATE TABLE allocation_groups_temp (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                            "name_id INTEGER NOT NULL REFERENCES names(id), " \
                                            "allocation_group_parent_id INTEGER REFERENCES allocation_groups(id), " \
                                            "owner_id INTEGER NOT NULL, " \
                                            "is_hidden INTEGER NOT NULL);"

            try:
                cursor.execute(new_proprty_temp_table_script)
            except Exception as e:
                created_temp_property_table = False
                update_all_success = False
                print('ERROR: Failed to create allocation group temp table: ', e)

            if created_temp_property_table:
                import_data_success = True
                print('Trying to import current property information to temp table')

                user = User.objects.all().order_by('pk')[0]

                new_proprty_temp_table_script = "INSERT INTO allocation_groups_temp (id, name_id, allocation_group_parent_id, owner_id, is_hidden) " \
                                                "SELECT id, name_id, allocation_group_parent_id, owner_id, 1 as is_hidden from allocation_groups ;"

                try:
                    cursor.execute(new_proprty_temp_table_script)
                except Exception as e:
                    update_all_success = False
                    import_data_success = False
                    print('ERROR: failed to import data to allocation groups temp table:', e)

                if import_data_success:
                    print('Imported data successfully')

                    print('--- deleting current allocation groups table ')
                    drop_table_script = "drop table allocation_groups;"

                    drop_table_success = True
                    try:
                        cursor.execute(drop_table_script)
                    except Exception as e:
                        drop_table_success = False
                        print('Failed to drop allocation_groups table: ', e)

                    if drop_table_success:

                        created_property_table = True
                        new_proprty_table_script = "CREATE TABLE allocation_groups (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                                   "name_id INTEGER NOT NULL REFERENCES names(id), " \
                                                   "allocation_group_parent_id INTEGER REFERENCES allocation_groups(id), " \
                                                   "owner_id INTEGER NOT NULL, is_hidden INTEGER NOT NULL);"

                        try:
                            cursor.execute(new_proprty_table_script)
                        except Exception as e:
                            created_property_table = False
                            update_all_success = False
                            print ("ERROR: Failed to create floors table", e)

                        if created_property_table:
                            import_data_success = True
                            import_data_script = "INSERT INTO allocation_groups (id, name_id, allocation_group_parent_id, owner_id, is_hidden) " \
                                                "SELECT id, name_id, allocation_group_parent_id, owner_id, is_hidden from allocation_groups_temp ;"

                            try:
                                cursor.execute(import_data_script)
                            except Exception as e:
                                import_data_success = False
                                update_all_success = False
                                print('ERROR: Failed to import data to allocation_groups table:', e)

                            if import_data_success:
                                print('==== SUCCESS: completed update of owner_id field to allocation_groups table')

            print('=-=-=-=- ALLOCATION GROUPS TABLE UPDATE END =-=-=-=-')
            print('')

            if update_all_success:
                print('!!!!! UPDATING MODELS FILE WITH UPDATES BEGIN !!!!! ')

                print('PROJECT_ROOT', settings.PROJECT_ROOT)

                try:
                    file_path = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
                    copy_file_path = os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/models.py" % project.project_connection_name)

                    shutil.copy2(file_path, copy_file_path)

                    print( "SUCCESS: copied models py file")
                except Exception as e:
                    print('ERROR: failed to update models file: ', e)

                print('!!!!! UPDATING MODELS FILE WITH UPDATES END !!!!! ')
                print('')
                print('')

