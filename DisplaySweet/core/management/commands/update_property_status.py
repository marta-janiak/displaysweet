import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Update property status. Use --property_name OR --floor_name OR --building_name THEN --status (all string values)'

    def handle(self, *args, **options):

        pass