from django.core.management.base import BaseCommand, CommandError

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):

    def handle(self, *args, **options):
        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:

            print('At project: %s' % project.name)

            try:
                canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

                properties = canvas_mixin.class_object("Properties").all()

                for apartment in properties:
                    property_id = apartment.pk
                    resources = canvas_mixin.class_object("PropertyResources").filter(property=apartment, key="key_plans")

                    if resources:
                        resource_id = resources[0].resource_id
                        asset = canvas_mixin.class_object("Assets").filter(source_id=resource_id)
                        if asset:
                            asset = asset[0]

                            key_string = canvas_mixin.class_object("Strings").filter(english='key_plans_image_id')
                            if not key_string:
                                canvas_mixin.class_object("Strings").create(english='key_plans_image_id')
                                key_string = canvas_mixin.class_object("Strings").filter(english='key_plans_image_id')

                            key_string = key_string[0]

                            property_int_resource = canvas_mixin.class_object("PropertiesIntFields").filter(type_string=key_string,
                                                                                                    property_id=property_id)

                            if property_int_resource:
                                property_int_resource = property_int_resource[0]
                                property_int_resource.value = asset.pk
                                property_int_resource.save(using=project.project_connection_name)

            except:
                pass