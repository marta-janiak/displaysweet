import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Sets up the initial Airports Application process'


    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.filter(project_copied_from__isnull=False)

        for project in projects:
            print 'project.project_copied_from: ', project.project_copied_from, 'project: ', project
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            templates = ProjectCSVTemplate.objects.filter(project=project.project_copied_from)

            for template in templates:
                print 'copying template: ', template
                fields = ProjectCSVTemplateFields.objects.filter(template=template)

                if fields:
                    new_template = template
                    new_template.id = None
                    new_template.project = project
                    new_template.save()

                    for field in fields:
                        new_field = field
                        new_field.id = None
                        new_field.template = new_template
                        new_field.save()

        print 'Update Complete'