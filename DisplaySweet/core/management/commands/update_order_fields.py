import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            print 'updating project: ', project
            print '------ PROPERTY & FLOOR TABLE UPDATE BEGIN ------'

            try:
                floors = canvas_mixin.class_object("Floors").all().order_by('pk', 'name__english')
                f = 1
                for floor in floors:
                    floor.orderidx = f
                    floor.save(using=project.project_connection_name)

                    properties = canvas_mixin.class_object("Properties").filter(floor_id=floor.pk).order_by('name__english')

                    i = 1
                    for apartment in properties:
                        apartment.orderidx = i
                        apartment.save(using=project.project_connection_name)

                        i+=1

                    f+=1
            except Exception as e:
                print "ERROR:", e

            print '------ PROPERTY & FLOOR TABLE UPDATE END ------'


