import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin
from users.models import Purchaser


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:

            update_all_success = True

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print 'Failed to begin db connection: ', e
                update_all_success = False
                print ''
                continue

            try:
                cursor.execute("drop table buyer_details;")
            except Exception as e:
                pass


            print 'creating temporary buyer_details table'

            created_temp_property_table = True

            new_proprty_temp_table_script = "CREATE TABLE buyer_details ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                            "title VARCHAR(30), first_name VARCHAR(30), last_name VARCHAR(30), email VARCHAR(255), phone_number VARCHAR(255), " \
                                            "date_of_birth DATE, user_type VARCHAR(30), is_active BOOLEAN, can_access_django_backend BOOLEAN, status VARCHAR(30)," \
                                            "project_id INTEGER, agent_id INTEGER, property_id INTEGER , buyer_type VARCHAR(30), company_name VARCHAR(50), website VARCHAR(50), occupy_status VARCHAR(30), " \
                                            "purchaser_location VARCHAR(30), firb VARCHAR(30), nras VARCHAR(30), enquiry_source VARCHAR(30), agent_bonus VARCHAR(50), " \
                                            "rebate VARCHAR(50), rental_guarantee VARCHAR(50), sales_price VARCHAR(50), sales_type VARCHAR(50), weekly_amount VARCHAR(50)," \
                                            "percentage_amount VARCHAR(50), number_of_weeks VARCHAR(50), max_cost VARCHAR(50), weeks_leased VARCHAR(50), leased_amount VARCHAR(50)," \
                                            "recouped VARCHAR(50), current_position VARCHAR(50), reservation_fee_date DATE, reservation_fee_refund BOOLEAN, contract_exchange_date DATE," \
                                            "cancelled BOOLEAN, deposit_date DATE,  deposit_refund BOOLEAN);"

            # print 'new_proprty_temp_table_script'
            # print new_proprty_temp_table_script

            try:
                cursor.execute(new_proprty_temp_table_script)

            except Exception as e:
                created_temp_property_table = False
                update_all_success = False
                print 'ERROR: Failed to create buyer_details table: ', e
                print '=-=-=-=-=-=-=-=-=-=-=-=-=-='

            if created_temp_property_table and update_all_success:
                print '!!!!! UPDATING MODELS FILE WITH UPDATES BEGIN !!!!! '

                print 'PROJECT_ROOT', settings.PROJECT_ROOT

                try:
                    file_path = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
                    copy_file_path = os.path.join(settings.PROJECT_ROOT,
                                                  "projects/project_folders/%s/models.py" % project.project_connection_name)

                    shutil.copy2(file_path, copy_file_path)

                    print "SUCCESS: copied models py file"
                except Exception as e:
                    update_all_success = False
                    print 'ERROR: failed to update models file: ', e

                print '!!!!! UPDATING MODELS FILE WITH UPDATES END !!!!! '
                print ''
                print ''

                canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)
                print 'Trying to import current property information to temp table'

                all_purchasers = Purchaser.objects.all()
                created_files = 0
                for buyer in all_purchasers:
                    print "---- buyer ", buyer

                    buyer_project = buyer.projects.all()
                    if buyer_project:
                        buyer_project = buyer_project[0].pk
                    else:
                        buyer_project = 0

                    buyer_property_id = 0
                    if buyer.property_id:
                        buyer_property_id = buyer.property_id

                    buyer_agent_id = 0
                    if buyer.agent and buyer.agent.pk:
                        buyer_agent_id = buyer.agent.pk

                    try:
                        canvas_mixin.class_object("BuyerDetails").create(id=buyer.id,
                                                                         title=buyer.title,
                                                                         first_name=buyer.first_name,
                                                                         last_name=buyer.last_name,
                                                                         email=buyer.email,
                                                                         phone_number=buyer.phone_number,
                                                                        date_of_birth=buyer.date_of_birth,
                                                                        user_type=buyer.user_type,
                                                                        is_active=buyer.is_active,
                                                                        can_access_django_backend=buyer.can_access_django_backend,
                                                                        status=buyer.status,
                                                                        project_id=buyer_project,
                                                                        property_id=buyer_property_id,
                                                                        agent_id=buyer_agent_id,
                                                                        buyer_type=buyer.buyer_type,
                                                                        company_name=buyer.company_name,
                                                                        website=buyer.website,
                                                                        occupy_status=buyer.occupy_status,
                                                                        purchaser_location=buyer.purchaser_location,
                                                                        firb=buyer.firb,
                                                                        nras=buyer.nras,
                                                                        enquiry_source=buyer.enquiry_source,
                                                                        agent_bonus=buyer.agent_bonus,
                                                                        rebate=buyer.rebate,
                                                                        rental_guarantee=buyer.rental_guarantee,
                                                                        sales_price=buyer.sales_price,
                                                                        sales_type=buyer.sales_type,
                                                                        weekly_amount=buyer.weekly_amount,
                                                                        percentage_amount=buyer.percentage_amount,
                                                                        number_of_weeks=buyer.number_of_weeks,
                                                                        max_cost=buyer.max_cost,
                                                                        weeks_leased=buyer.weeks_leased,
                                                                        leased_amount=buyer.leased_amount,
                                                                        recouped=buyer.recouped,
                                                                        current_position=buyer.current_position,
                                                                        reservation_fee_date=buyer.reservation_fee_date,
                                                                        reservation_fee_refund=buyer.reservation_fee_refund,
                                                                        contract_exchange_date=buyer.contract_exchange_date,
                                                                        cancelled=buyer.cancelled,
                                                                        deposit_date=buyer.deposit_date,
                                                                        deposit_refund=buyer.deposit_refund)
                        created_files+= 1
                    except Exception as e:
                        print 'Failed', e


                print ''
                print 'created_files', created_files

                print '------ BUYER DETAILS TABLE UPDATE END ------ '
                print ''