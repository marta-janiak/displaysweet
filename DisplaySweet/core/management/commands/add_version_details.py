import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Add Initial Version'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        projects.update(version_family=None)

        ProjectVersion.objects.all().update(project=None)
        ProjectVersion.objects.all().delete()

        for project in projects:
            project.version = 1.0
            project.save()

            new_version = ProjectVersion(active_version=1.0, project=project, project_name=project.name)
            new_version.save()

            project.version_family = new_version
            project.save()





