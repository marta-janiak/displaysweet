import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:
            # canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            update_all_success = True
            print ('updating project: ', project)
            print ('------ PROPERTY RENDERS TABLE UPDATE BEGIN ------')

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print ('Failed to begin db connection: ', e)
                update_all_success = False
                print ('')
                continue

            try:
                cursor.execute("drop table property_renders;")
            except Exception as e:
                pass

            print ('creating property renders table')


            new_proprty_temp_table_script = "CREATE TABLE property_renders (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " \
                                            "property_id INTEGER REFERENCES properties (id), key VARCHAR(50), " \
                                            "resource_id INTEGER REFERENCES resources (id), " \
                                            "orderidx INTEGER DEFAULT 0 NOT NULL, caption varchar(50) );"

            try:
                cursor.execute(new_proprty_temp_table_script)
                created_temp_property_table = True
            except Exception as e:
                created_temp_property_table = False
                print ('ERROR: Failed to create property temp table: ', e)

            if created_temp_property_table:
                print ('------ PROPERTY RENDERS UPDATE END ------ ')
                print ('')

                try:
                    file_path = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
                    copy_file_path = os.path.join(settings.PROJECT_ROOT,
                                                  "projects/project_folders/%s/models.py" % project.project_connection_name)

                    shutil.copy2(file_path, copy_file_path)

                    print ("++ SUCCESS: copied models py file")
                except Exception as e:
                    update_all_success = False
                    print ('--> ERROR: failed to update models file: ', e)


                print ('======= Updated models file')
                print('')