import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:
            update_all_success = True
            print('updating project: ', project)
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            print('------ CONTENT DATA UPDATE BEGIN ------')
            canvas_mixin.class_object("Content").filter(canvas_id=1, template_id=1, wireframe_id=1, container_type_id=1).delete()

            canvases = canvas_mixin.class_object("Canvases").all()

            for selected_canvas in canvases:
                canvas_id = selected_canvas.pk

                menu_publish_list, main_wireframe_controller, wire_parent_id = canvas_mixin.get_canvas_publish_display(
                    selected_canvas)

                for menu in menu_publish_list:
                    for list_item in menu['publish_items']:
                        wireframe_id = list_item['wireframe_id']
                        template_id = list_item['template_id']
                        wireframe_controller_id = list_item['main_wireframe_controller'].pk

                        hashed_file_details, publishing_message = canvas_mixin.pubish_canvas_template(wireframe_id,
                                                                                                      canvas_id,
                                                                                                      template_id,
                                                                                                      wireframe_controller_id,
                                                                                                      user_id=1,
                                                                                                      deliver=False)

                        for key, value in hashed_file_details.items():
                            content_row = canvas_mixin.class_object("Content").filter(pk=key)
                            if content_row:
                                content_row = content_row[0]
                                content_row.has = value
                                content_row.save(using=project.project_connection_name)

            try:
                cursor = connections[project.project_connection_name].cursor()
            except Exception as e:
                print('Failed to begin db connection: ', e)
                update_all_success = False
                print ('')
                continue

            if update_all_success:
                cursor.execute('DELETE from content where path not in (select path from content_temp);')
                print(cursor.fetchone())

            print('------ CONTENT DATA UPDATE END ------')