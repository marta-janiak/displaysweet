import sqlite3

import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin
import django.apps


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        with open('core/management/commands/test.sql', 'w') as f:
            f.write('')

        projects = Project.objects.filter(active=True)

        for project in projects:
            for application in django.apps.apps.get_models():
                db_file_path = os.path.join(settings.PROJECT_ROOT, "projects/project_folders/%s/database/%s.db" %
                                            (project.project_connection_name, project.project_connection_name))

                if os.path.isfile(db_file_path):

                    db_file = File(open(db_file_path, "rb"))
                    db_file = File(db_file)

                    TABLE_TO_DUMP = 'table_to_dump'
                    DB_FILE = 'db_file'

                    # try:
                    print('application', application.__name__)
                    print(getTableDump(db_file_path, str(application.__name__).lower()))
                    # except Exception as e:
                    #     print('sqlite file stuff', e, application)



def getTableDump(db_file, table_to_dump):
    conn = sqlite3.connect(':memory:')
    cu = conn.cursor()
    print(1)
    cu.execute("attach database '" + db_file + "' as attached_db;")
    print(2)
    script = "select sql from attached_db.sqlite_master where type='table' and name='" + table_to_dump + "';"
    print('script', script)
    cu.execute(script)

    print(3)
    sql_create_table = cu.fetchone()

    if sql_create_table:
        print(4)
        sql_create_table = sql_create_table[0]

        sql_create_table = '%s; \n\n' % sql_create_table

        cu.execute(sql_create_table);

        with open('core/management/commands/test.sql', 'a') as f:
            f.write(sql_create_table)

        print(7)
        # cu.execute("insert into " + table_to_dump +
        #            " select * from attached_db." + table_to_dump)
        #
        # print(4)
        # conn.commit()
        cu.execute("DETACH DATABASE attached_db")
        return "\n".join(conn.iterdump())