import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):

    def handle(self, *args, **options):
        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:

            print ('At project: %s' % project.name)

            try:
                canvas_mixin = PrimaryModelMixin(request=None, user=user, project=project)

                properties = canvas_mixin.class_object("Properties").all()

                for apartment in properties:
                    print ('Property: ', apartment)

                    property_id = apartment.pk

                    print ('--- Updating floor plans')

                    key_string = canvas_mixin.class_object("Strings").filter(english='floor_plans_image_id')
                    if not key_string:
                        canvas_mixin.class_object("Strings").create(english='floor_plans_image_id')
                        key_string = canvas_mixin.class_object("Strings").filter(english='floor_plans_image_id')

                    key_string = key_string[0]

                    property_int_resource = canvas_mixin.class_object("PropertiesIntFields").filter(type_string=key_string,
                                                                                                    property_id=property_id)

                    if property_int_resource:
                        property_int_resource = property_int_resource[0]

                        key_asset = canvas_mixin.class_object("Assets").filter(source_id=property_int_resource.value)
                        if key_asset:
                            key_asset = key_asset[0]

                            property_int_resource.value = key_asset.pk
                            property_int_resource.save(using=project.project_connection_name)

                    print ('--- Updating key plans')

                    key_string = canvas_mixin.class_object("Strings").filter(english='key_plans_image_id')
                    if not key_string:
                        canvas_mixin.class_object("Strings").create(english='key_plans_image_id')
                        key_string = canvas_mixin.class_object("Strings").filter(english='key_plans_image_id')

                    key_string = key_string[0]

                    property_int_resource = canvas_mixin.class_object("PropertiesIntFields").filter(type_string=key_string,
                                                                                                    property_id=property_id)

                    if property_int_resource:
                        property_int_resource = property_int_resource[0]
                        key_asset = canvas_mixin.class_object("Assets").filter(source_id=property_int_resource.value)
                        if key_asset:
                            key_asset = key_asset[0]

                            property_int_resource.value = key_asset.pk
                            property_int_resource.save(using=project.project_connection_name)



                    print ('')

            except Exception as e:
                print ('Cannot update project: ', e)
