import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()

        for project in projects:

            update_all_success = True

            print 'updating project: ', project
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            print 'updating container types'

            try:
                container_types = canvas_mixin.class_object("ContainerTypes").all()
                print 'container_types', container_types
            except Exception as e:
                print 'Failed to begin db connection: ', e
                update_all_success = False
                print ''

            if update_all_success:
                container_types = canvas_mixin.class_object("ContainerTypes").all()

                print 'existing container_types: ', container_types

                print '- Updating thumbnail container types'
                thumbnail_type_string = canvas_mixin.class_object("Strings").filter(english="Thumbnail Files")
                if not thumbnail_type_string:
                    canvas_mixin.class_object("Strings").create(english="Thumbnail Files")
                    thumbnail_type_string = canvas_mixin.class_object("Strings").filter(english="Thumbnail Files")

                if thumbnail_type_string:
                    thumbnail_type_string = thumbnail_type_string[0]

                    thumbnail_container_type = canvas_mixin.class_object("ContainerTypes").filter(type_string=thumbnail_type_string)
                    if not thumbnail_container_type:
                        canvas_mixin.class_object("ContainerTypes").create(
                            type_string=thumbnail_type_string)

                        thumbnail_container_type = canvas_mixin.class_object("ContainerTypes").filter(
                            type_string=thumbnail_type_string)

                    if thumbnail_container_type:
                        thumbnail_container_type = thumbnail_container_type[0]

                        print '-- Updating thumbnail container types to proper type'

                        thumbnail_names = canvas_mixin.class_object("Names").filter(english='Thumbnail Details')
                        thumbnail_containers = canvas_mixin.class_object("Containers").filter(container_name__in=thumbnail_names)

                        for tcontainer in thumbnail_containers:
                            tcontainer.container_type_id = thumbnail_container_type.pk
                            tcontainer.save(using=project.project_connection_name)

                print '- Updating source container types'
                source_type_string = canvas_mixin.class_object("Strings").filter(english="Default Source Files")
                if not source_type_string:
                    canvas_mixin.class_object("Strings").create(english="Default Source Files")
                    source_type_string = canvas_mixin.class_object("Strings").filter(english="Default Source Files")

                if source_type_string:
                    source_type_string = source_type_string[0]

                    source_container_type = canvas_mixin.class_object("ContainerTypes").filter(
                        type_string=source_type_string)
                    if not source_container_type:
                        canvas_mixin.class_object("ContainerTypes").create(
                            type_string=source_type_string)

                        source_container_type = canvas_mixin.class_object("ContainerTypes").filter(
                            type_string=source_type_string)

                    if source_container_type:
                        source_container_type = source_container_type[0]

                        print '-- Updating source container types to proper type'

                        source_names = canvas_mixin.class_object("Names").filter(english='Default Source Image Settings')
                        thumbnail_containers = canvas_mixin.class_object("Containers").filter(
                            container_name__in=source_names)

                        for tcontainer in thumbnail_containers:
                            tcontainer.container_type_id = source_container_type.pk
                            tcontainer.save(using=project.project_connection_name)

                print '- Updating unique thumbnail container types'
                type_string = canvas_mixin.class_object("Strings").filter(english="Unique Thumbnail Files")
                if not type_string:
                    canvas_mixin.class_object("Strings").create(english="Unique Thumbnail Files")
                    type_string = canvas_mixin.class_object("Strings").filter(english="Unique Thumbnail Files")

                if type_string:
                    type_string = type_string[0]

                    container_type = canvas_mixin.class_object("ContainerTypes").filter(
                        type_string=type_string)
                    if not container_type:
                        canvas_mixin.class_object("ContainerTypes").create(
                            type_string=type_string)

                        container_type = canvas_mixin.class_object("ContainerTypes").filter(
                            type_string=type_string)

                    if container_type:
                        container_type = container_type[0]

                        print '-- Updating unique thumbnail container types to proper type'

                        source_names = canvas_mixin.class_object("Names").filter(english='Unique Thumbnail Files')
                        thumbnail_containers = canvas_mixin.class_object("Containers").filter(
                            container_name__in=source_names)

                        for tcontainer in thumbnail_containers:
                            tcontainer.container_type_id = container_type.pk
                            tcontainer.save(using=project.project_connection_name)

                print ''


                print '------ CONTENT SCHEMA UPDATE BEGIN ------'

                try:
                    cursor = connections[project.project_connection_name].cursor()
                except Exception as e:
                    print 'Failed to begin db connection: ', e
                    update_all_success = False
                    print ''
                    continue

                try:
                    cursor.execute("drop table content_temp;")
                except Exception as e:
                    pass

                print 'creating temporary content table'

                created_temp_property_table = True

                new_proprty_temp_table_script = "CREATE TABLE content_temp (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), " \
                                                "resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), " \
                                                "canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), " \
                                                "wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), " \
                                                "orderidx INTEGER, published DATE, published_by INTEGER );"

                # print 'new_proprty_temp_table_script'
                # print new_proprty_temp_table_script

                try:
                    cursor.execute(new_proprty_temp_table_script)
                except Exception as e:
                    created_temp_property_table = False
                    update_all_success = False
                    print 'ERROR: Failed to create content temp table: ', e

                if created_temp_property_table:
                    import_data_success = True
                    print 'Trying to import current property information to temp table'

                    new_proprty_temp_table_script = "INSERT INTO content_temp (id, container_id, resource_id, hash, path, canvas_id, " \
                                                    "template_id, wireframe_id, container_type_id, orderidx, published, published_by) " \
                                                    "SELECT id, container_id, resource_id, hash, path, 1 as canvas_id, " \
                                                    "1 as template_id, 1 as wireframe_id, 1 as container_type_id, 1 as orderidx, " \
                                                    "'' as published, 1 as published_by from content ;"

                    try:
                        cursor.execute(new_proprty_temp_table_script)
                    except Exception as e:
                        update_all_success = False
                        import_data_success = False
                        print 'ERROR: failed to import data to content temp table:', e

                    if import_data_success:
                        print 'Imported data successfully'

                        print '--- deleting current content table '
                        drop_table_script = "drop table content;"

                        drop_table_success = True
                        try:
                            cursor.execute(drop_table_script)
                        except Exception as e:
                            drop_table_success = False
                            print 'Failed to drop content table: ', e

                        if drop_table_success:

                            created_property_table = True
                            new_proprty_table_script =  "CREATE TABLE content (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), " \
                                                        "resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), " \
                                                        "canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), " \
                                                        "wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), " \
                                                        "orderidx INTEGER, published DATE, published_by INTEGER );"

                            try:
                                cursor.execute(new_proprty_table_script)
                            except Exception as e:
                                created_property_table = False
                                update_all_success = False
                                print "ERROR: Failed to create property table", e

                            if created_property_table:
                                import_data_success = True
                                import_data_script = "INSERT INTO content (id, container_id, resource_id, hash, path, canvas_id, " \
                                                     "template_id, wireframe_id, container_type_id, orderidx, published, published_by) " \
                                                     "SELECT id, container_id, resource_id, hash, path, canvas_id, " \
                                                     "template_id, wireframe_id, container_type_id, orderidx, published, published_by from content_temp ;"

                                try:
                                    cursor.execute(import_data_script)
                                except Exception as e:
                                    import_data_success = False
                                    update_all_success = False
                                    print 'ERROR: Failed to import data to content table:', e

                                if import_data_success:
                                    print '==== SUCCESS: completed update of main content schema'

                print '------ CONTENT SCHEMA TABLE UPDATE END ------'
                print ''

                if update_all_success:
                    print '!!!!! UPDATING MODELS FILE WITH UPDATES BEGIN !!!!! '

                    print 'PROJECT_ROOT', settings.PROJECT_ROOT

                    try:
                        file_path = os.path.join(settings.PROJECT_ROOT, "fixtures/new_models_file.py")
                        copy_file_path = os.path.join(settings.PROJECT_ROOT,
                                                      "projects/project_folders/%s/models.py" % project.project_connection_name)

                        shutil.copy2(file_path, copy_file_path)

                        print "SUCCESS: copied models py file"
                    except Exception as e:
                        print 'ERROR: failed to update models file: ', e

                    print '!!!!! UPDATING MODELS FILE WITH UPDATES END !!!!! '
                    print ''
                    print ''




