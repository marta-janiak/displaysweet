import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Removed plan duplicates'

    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.filter(project_copied_from__isnull=False)

        for project in projects:
            print 'project: ', project
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)

            properties = canvas_mixin.class_object("Properties").all()

            for property in properties:
                plans = canvas_mixin.class_object("PropertyResources").filter(key="floor_plans",
                                                                              property_id=property.pk)

                if len(plans) > 1:
                    plans = canvas_mixin.class_object("PropertyResources").filter(key="floor_plans",
                                                                                  resource__path__icontains='UI_placeholder_floorplan.jpg',
                                                                                  property_id=property.pk)

                    print('property %s has %s "floor plans" to delete' % (property, len(plans)))
                    for plan in plans:
                        check_plans = canvas_mixin.class_object("PropertyResources").filter(key="floor_plans", property_id=property.pk)
                        if not len(check_plans) == 1:
                            plan.resource_id = 0
                            plan.save(using=project.project_connection_name)
                            plan.delete(using=project.project_connection_name)


                plans = canvas_mixin.class_object("PropertyResources").filter(key="key_plans",
                                                                              property_id=property.pk)

                if len(plans) > 1:
                    plans = canvas_mixin.class_object("PropertyResources").filter(key="key_plans",
                                                                                  resource__path__icontains='UI_placeholder_keyplan.jpg',
                                                                                  property_id=property.pk)

                    print 'property %s has %s "key plans" to delete' % (property, len(plans))
                    for plan in plans:
                        check_plans = canvas_mixin.class_object("PropertyResources").filter(key="key_plans", property_id=property.pk)
                        if not len(check_plans) == 1:
                            plan.resource_id = 0
                            plan.save(using=project.project_connection_name)
                            plan.delete(using=project.project_connection_name)


            floors = canvas_mixin.class_object("Floors").all()

            for floor in floors:
                print '-- floor', floor
                level_plans = canvas_mixin.class_object("FloorsPlans").filter(key="level_plans", floor_id=floor.pk)

                if len(level_plans) > 1:
                    plans = canvas_mixin.class_object("FloorsPlans").filter(key="level_plans",
                                                                            resource__path__icontains='UI_placeholder_levelplan.jpg',
                                                                            floor_id=floor.pk)

                    print 'Floor %s has %s "level plans" to delete' % (floor, len(level_plans) -1)
                    for plan in plans:
                        check_plans = canvas_mixin.class_object("FloorsPlans").filter(key="level_plans", floor_id=floor.pk)
                        if not len(check_plans) == 1:
                            plan.resource_id = 0
                            plan.save(using=project.project_connection_name)
                            plan.delete(using=project.project_connection_name)


        print 'Update Complete'