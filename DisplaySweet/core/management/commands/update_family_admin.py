import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from django.apps import apps

from core.models import *
from users.models import User
from django.db import connections
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    help = 'Add admin families'

    def handle(self, *args, **options):
        projects = Project.objects.all()

        for project in projects:
            new_family = ProjectFamily.objects.create(family_name=project.name)
            new_family.project_list.add(project)
            new_family.save()

        print('Completed.')