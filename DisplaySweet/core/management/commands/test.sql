CREATE TABLE assets (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    asset_type_id INTEGER NOT NULL REFERENCES asset_types(id),
    source_id INTEGER NOT NULL
); 

CREATE TABLE buildings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE canvases (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    canvas_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE containers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_type_id INTEGER NOT NULL, container_name_id INTEGER NOT NULL REFERENCES names(id), container_parent_id INTEGER REFERENCES containers(id), orderidx INTEGER DEFAULT 0 NOT NULL, cms_display BOOLEAN); 

CREATE TABLE content (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), orderidx INTEGER, published DATE, published_by INTEGER ); 

CREATE TABLE descriptions (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE devices (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    device_type_id INTEGER NOT NULL REFERENCES device_types(id),
    device_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE fittings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, resource_id INTEGER NOT NULL REFERENCES resources(id), name_id INTEGER NOT NULL REFERENCES names(id), description_id INTEGER NOT NULL REFERENCES descriptions(id), fitting_categories_id INTEGER REFERENCES fitting_categories(id)); 

CREATE TABLE floors (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name_id INTEGER NOT NULL REFERENCES names(id), orderidx integer); 

CREATE TABLE fonts (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    font_name VARCHAR(32) NOT NULL,
    font_path VARCHAR(100) NOT NULL
); 

CREATE TABLE listings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, listing_status_id INTEGER NOT NULL REFERENCES listing_status(id)); 

CREATE TABLE names (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE properties ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, floor_id INTEGER NOT NULL REFERENCES floors(id), name_id INTEGER NOT NULL REFERENCES names(id), property_type_id INTEGER NOT NULL REFERENCES property_types(id), orderidx integer ); 

CREATE TABLE resources (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description_id INTEGER NOT NULL REFERENCES descriptions(id), size INTEGER DEFAULT 0 NOT NULL, path VARCHAR(100), md5 VARCHAR(32)); 

CREATE TABLE strings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE templates (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR(50) NOT NULL, template_type_id INTEGER REFERENCES template_types(id)); 

CREATE TABLE videos (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description_id INTEGER NOT NULL REFERENCES descriptions(id),
    size INTEGER DEFAULT 0 NOT NULL,
    path VARCHAR(100),
    md5 VARCHAR(32)
); 

CREATE TABLE wireframes (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    wireframe_parent_id INTEGER REFERENCES wireframes(id),
    template_id INTEGER REFERENCES templates(id),
    orderidx INTEGER DEFAULT 0 NOT NULL,
    name VARCHAR(50) NOT NULL,
    display_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE assets (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    asset_type_id INTEGER NOT NULL REFERENCES asset_types(id),
    source_id INTEGER NOT NULL
); 

CREATE TABLE buildings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE canvases (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    canvas_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE containers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_type_id INTEGER NOT NULL, container_name_id INTEGER NOT NULL REFERENCES names(id), container_parent_id INTEGER REFERENCES containers(id), orderidx INTEGER DEFAULT 0 NOT NULL, cms_display BOOLEAN); 

CREATE TABLE content (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), orderidx INTEGER, published DATE, published_by INTEGER ); 

CREATE TABLE descriptions (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE devices (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    device_type_id INTEGER NOT NULL REFERENCES device_types(id),
    device_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE fittings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, resource_id INTEGER NOT NULL REFERENCES resources(id), name_id INTEGER NOT NULL REFERENCES names(id), description_id INTEGER NOT NULL REFERENCES descriptions(id), fitting_categories_id INTEGER REFERENCES fitting_categories(id)); 

CREATE TABLE floors (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name_id INTEGER NOT NULL REFERENCES names(id), orderidx integer); 

CREATE TABLE fonts (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    font_name VARCHAR(32) NOT NULL,
    font_path VARCHAR(100) NOT NULL
); 

CREATE TABLE listings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, listing_status_id INTEGER NOT NULL REFERENCES listing_status(id)); 

CREATE TABLE names (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE properties ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, floor_id INTEGER NOT NULL REFERENCES floors(id), name_id INTEGER NOT NULL REFERENCES names(id), property_type_id INTEGER NOT NULL REFERENCES property_types(id), orderidx integer ); 

CREATE TABLE resources (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description_id INTEGER NOT NULL REFERENCES descriptions(id), size INTEGER DEFAULT 0 NOT NULL, path VARCHAR(100), md5 VARCHAR(32)); 

CREATE TABLE strings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE templates (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR(50) NOT NULL, template_type_id INTEGER REFERENCES template_types(id)); 

CREATE TABLE videos (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description_id INTEGER NOT NULL REFERENCES descriptions(id),
    size INTEGER DEFAULT 0 NOT NULL,
    path VARCHAR(100),
    md5 VARCHAR(32)
); 

CREATE TABLE wireframes (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    wireframe_parent_id INTEGER REFERENCES wireframes(id),
    template_id INTEGER REFERENCES templates(id),
    orderidx INTEGER DEFAULT 0 NOT NULL,
    name VARCHAR(50) NOT NULL,
    display_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE assets (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    asset_type_id INTEGER NOT NULL REFERENCES asset_types(id),
    source_id INTEGER NOT NULL
); 

CREATE TABLE buildings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE canvases (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    canvas_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE containers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_type_id INTEGER NOT NULL, container_name_id INTEGER NOT NULL REFERENCES names(id), container_parent_id INTEGER REFERENCES containers(id), orderidx INTEGER DEFAULT 0 NOT NULL, cms_display BOOLEAN); 

CREATE TABLE content (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), orderidx INTEGER, published DATE, published_by INTEGER ); 

CREATE TABLE descriptions (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE devices (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    device_type_id INTEGER NOT NULL REFERENCES device_types(id),
    device_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE fittings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, resource_id INTEGER NOT NULL REFERENCES resources(id), name_id INTEGER NOT NULL REFERENCES names(id), description_id INTEGER NOT NULL REFERENCES descriptions(id), fitting_categories_id INTEGER REFERENCES fitting_categories(id)); 

CREATE TABLE floors (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name_id INTEGER NOT NULL REFERENCES names(id), orderidx integer); 

CREATE TABLE fonts (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    font_name VARCHAR(32) NOT NULL,
    font_path VARCHAR(100) NOT NULL
); 

CREATE TABLE listings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, listing_status_id INTEGER NOT NULL REFERENCES listing_status(id)); 

CREATE TABLE names (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE properties ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, floor_id INTEGER NOT NULL REFERENCES floors(id), name_id INTEGER NOT NULL REFERENCES names(id), property_type_id INTEGER NOT NULL REFERENCES property_types(id), orderidx integer ); 

CREATE TABLE resources (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description_id INTEGER NOT NULL REFERENCES descriptions(id), size INTEGER DEFAULT 0 NOT NULL, path VARCHAR(100), md5 VARCHAR(32)); 

CREATE TABLE strings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE templates (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR(50) NOT NULL, template_type_id INTEGER REFERENCES template_types(id)); 

CREATE TABLE videos (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description_id INTEGER NOT NULL REFERENCES descriptions(id),
    size INTEGER DEFAULT 0 NOT NULL,
    path VARCHAR(100),
    md5 VARCHAR(32)
); 

CREATE TABLE wireframes (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    wireframe_parent_id INTEGER REFERENCES wireframes(id),
    template_id INTEGER REFERENCES templates(id),
    orderidx INTEGER DEFAULT 0 NOT NULL,
    name VARCHAR(50) NOT NULL,
    display_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE assets (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    asset_type_id INTEGER NOT NULL REFERENCES asset_types(id),
    source_id INTEGER NOT NULL
); 

CREATE TABLE buildings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE canvases (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    canvas_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE containers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_type_id INTEGER NOT NULL, container_name_id INTEGER NOT NULL REFERENCES names(id), container_parent_id INTEGER REFERENCES containers(id), orderidx INTEGER DEFAULT 0 NOT NULL, cms_display BOOLEAN); 

CREATE TABLE content (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, container_id INTEGER NOT NULL REFERENCES containers(id), resource_id INTEGER NOT NULL REFERENCES resources(id), hash VARCHAR(100), path VARCHAR(100), canvas_id INTEGER NOT NULL REFERENCES  canvases(id), template_id INTEGER NOT NULL REFERENCES templates(id), wireframe_id INTEGER NOT NULL REFERENCES wireframes(id), container_type_id INTEGER NOT NULL REFERENCES container_types(id), orderidx INTEGER, published DATE, published_by INTEGER ); 

CREATE TABLE descriptions (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE devices (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    device_type_id INTEGER NOT NULL REFERENCES device_types(id),
    device_name_id INTEGER NOT NULL REFERENCES names(id)
); 

CREATE TABLE fittings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, resource_id INTEGER NOT NULL REFERENCES resources(id), name_id INTEGER NOT NULL REFERENCES names(id), description_id INTEGER NOT NULL REFERENCES descriptions(id), fitting_categories_id INTEGER REFERENCES fitting_categories(id)); 

CREATE TABLE floors (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name_id INTEGER NOT NULL REFERENCES names(id), orderidx integer); 

CREATE TABLE fonts (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    font_name VARCHAR(32) NOT NULL,
    font_path VARCHAR(100) NOT NULL
); 

CREATE TABLE listings (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, listing_status_id INTEGER NOT NULL REFERENCES listing_status(id)); 

CREATE TABLE names (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE properties ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, floor_id INTEGER NOT NULL REFERENCES floors(id), name_id INTEGER NOT NULL REFERENCES names(id), property_type_id INTEGER NOT NULL REFERENCES property_types(id), orderidx integer ); 

CREATE TABLE resources (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description_id INTEGER NOT NULL REFERENCES descriptions(id), size INTEGER DEFAULT 0 NOT NULL, path VARCHAR(100), md5 VARCHAR(32)); 

CREATE TABLE strings (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    text_table_category_id INTEGER REFERENCES text_table_categories(id),
    english VARCHAR(50),
    chinese VARCHAR(50)
); 

CREATE TABLE templates (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR(50) NOT NULL, template_type_id INTEGER REFERENCES template_types(id)); 

CREATE TABLE videos (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description_id INTEGER NOT NULL REFERENCES descriptions(id),
    size INTEGER DEFAULT 0 NOT NULL,
    path VARCHAR(100),
    md5 VARCHAR(32)
); 

CREATE TABLE wireframes (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    wireframe_parent_id INTEGER REFERENCES wireframes(id),
    template_id INTEGER REFERENCES templates(id),
    orderidx INTEGER DEFAULT 0 NOT NULL,
    name VARCHAR(50) NOT NULL,
    display_name_id INTEGER NOT NULL REFERENCES names(id)
); 

