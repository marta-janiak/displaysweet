import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand
from django.apps import apps

from core.models import *
from users.models import User
from projects.models.model_mixin import PrimaryModelMixin


class Command(BaseCommand):
    def handle(self, *args, **options):

        user = User.objects.get(pk=1)

        projects = Project.objects.all()
        for project in projects:
            canvas_mixin = CanvasModelMixin(request=None, user=user, project=project)
            container_model = apps.get_model(project.project_connection_name, 'Containers')
            container = container_model.objects.using(project.project_connection_name).filter()




        print 'Update Complete'



def get_or_save_display_name(project, value, canvas_mixin):
    names_text_category = canvas_mixin.TextTableCategoriesModel.objects.using(canvas_mixin.project.project_connection_name).filter(category="names")
    if names_text_category:
        names_text_category = names_text_category[0]

    display_name = canvas_mixin.NamesModel.objects.using(project.project_connection_name).filter(english=value, text_table_category=names_text_category)
    if len(display_name) == 0:
        canvas_mixin.NamesModel.objects.using(project.project_connection_name).create(english=value, text_table_category=names_text_category)
        display_name = canvas_mixin.NamesModel.objects.using(project.project_connection_name).filter(english=value, text_table_category=names_text_category)

    display_name = display_name[0]
    return display_name


def get_or_save_container_string_field(project, string_value, canvas_mixin):
    names_text_category = canvas_mixin.TextTableCategoriesModel.objects.using(canvas_mixin.project.project_connection_name).filter(category="container_strings")
    if not names_text_category:
        canvas_mixin.TextTableCategoriesModel.objects.using(canvas_mixin.project.project_connection_name).create(category="container_strings")
        names_text_category = canvas_mixin.TextTableCategoriesModel.objects.using(canvas_mixin.project.project_connection_name).filter(category="container_strings")

    if names_text_category:
        names_text_category = names_text_category[0]

    container_string = canvas_mixin.StringsModel.objects.using(project.project_connection_name).filter(english=string_value, text_table_category=names_text_category)
    if len(container_string) == 0:
        container_string = canvas_mixin.StringsModel.objects.using(project.project_connection_name).create(english=string_value, text_table_category=names_text_category)
        return container_string

    container_string = container_string[0]
    return container_string