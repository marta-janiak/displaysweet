import csv, re
from datetime import datetime
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import lzma
import zipfile
import threading
import os
# import urllib.request as urlrequest
import sqlite3
import re

import xml.etree.ElementTree as ET

tablesToClear = ['users', 'apartments', 'buildings', 'carspaces', 'commercial', 'fittings',
                 'allocation_group_relationships', 'allocation_settings',
                 'allocation_user_groups', 'allocations', 'displays', 'fitting_categories',
                 'fitting_sub_categories', 'fittings', 'floor_view_images', 'floors',
                 'location_overlays', 'price_list_data', 'project_users', 'propertybase_offers', 'storage',
                 'prueba_vistas', '_app_layout', '_form_data']


class Command(BaseCommand):
    help = 'Run template generator. Takes no args.'

    app = ProjectTemplateGenerator()
    app.run()


class ProjectTemplateGenerator:
    def __init__(self):
        self.app = QApplication([])
        self.dialog = QDialog(flags=None)
        self.generator.setupUi(self.dialog)

        self.generator.chooseProjectButton.clicked.connect(self.findFile)
        self.generator.stripItButton.clicked.connect(self.stripIt)

    def run(self):
        self.dialog.show()
        self.app.exec_()

    def findFile(self):
        fileLocation = QFileDialog()
        if fileLocation.exec_():
            self.generator.projectFileTextbox.setText(fileLocation.selectedFiles()[0])

    def stripIt(self):
        consoleOutput = ConsoleOutput(self.generator.projectFileTextbox.text(), self.generator.databaseIdTextbox.text())
        consoleOutput.run()


class ConsoleOutput:
    def __init__(self, location, newDatabaseId):
        self.location = location
        self.newDbId = int(newDatabaseId)
        if newDatabaseId == "":
            self.newDbId = None

        self.dialog = QDialog(flags=None)

        self.generator.setupUi(self.dialog)
        self.generator.closeProcessOutputButton.setEnabled(False)
        self.worker = threading.Thread(target=self.doConversion)
        self.generator.consoleOutputTextEdit.clear()

    def stripDatabaseFile(self, databaseFile):
        textBox = self.generator.consoleOutputTextEdit
        db = sqlite3.connect(databaseFile)
        cursor = db.cursor()
        tablesToDelete = []

        # Collate the backup tables
        print ("Collating backed up tables")
        for row in cursor.execute(
                "SELECT tbl_name FROM sqlite_master WHERE type='table' AND tbl_name LIKE '%_backup%'"):
            tablesToDelete.append(row[0])

        # Clear out the resource table and drop related tables
        print ("Collating resource table linked")
        for row in cursor.execute("SELECT resource_table FROM _app_layout"):
            tablesToDelete.append(row[0])

        # Drop all collated tables
        for table in tablesToDelete:
            try:
                cursor.execute("DROP TABLE %s" % (table))
                textBox.appendPlainText("Dropped table %s" % (table))
            except:
                textBox.appendPlainText("Unable to drop table %s" % (table))
            textBox.repaint()

        # Clean up the images table manually
        cursor.execute("DELETE FROM images WHERE id > 7")
        #cursor.execute("VACUUM images")

        # Clear out all the tables necessary
        for table in tablesToClear:
            try:
                cursor.execute("DELETE FROM %s" % (table))
                cursor.execute("VACUUM %s" % (table))
                textBox.appendPlainText("Cleared out the %s table" % (table))
            except:
                textBox.appendPlainText("Couldn't delete from the %s table" % (table))
            textBox.repaint()

        # Reset all the tables auto-increments
        tablesToReset = []
        for row in cursor.execute("SELECT tbl_name FROM sqlite_master WHERE type='table'"):
            tablesToReset.append(row[0])

        for table in tablesToReset:
            try:
                cursor.execute(
                    "UPDATE sqlite_sequence SET seq = (SELECT MAX(id) FROM " + table + ") WHERE name='" + table + "'")
                textBox.appendPlainText("Reset auto-increment on table %s" % (table))
            except:
                textBox.appendPlainText("Couldn't reset auto-increment on table %s" % (table))
            textBox.repaint()

        db.commit()
        textBox.appendPlainText("Committed changes")
        db.close()

    def findDatabaseId(self, rootNode):
        if rootNode.tag == "Attribute":
            if rootNode.attrib['name'] == "Database ID":
                return rootNode.attrib['value']
        for node in rootNode:
            dbId = self.findDatabaseId(node)
            if dbId != None:
                return dbId

        return None

    def downloadDatabase(self, databaseId):
        textBox = self.generator.consoleOutputTextEdit
        response = urlrequest.urlopen('http://103.245.145.34/get_database_exp.php?databaseID=%s' % (databaseId))
        locationParts = self.location.split('.')
        locationParts[-1] = "db"
        locationParts[-2] += "_%s_stripped" % (databaseId)
        savedDbFileName = ".".join(locationParts)
        with open(savedDbFileName, 'wb') as f:
            textBox.appendPlainText("Downloading database to %s" % (savedDbFileName))
            f.write(response.read())
        return savedDbFileName

    def replaceDatabaseId(self, node, textBox):
        if "value" in node.attrib:
            matches = re.search('databaseID=[0-9]+', node.attrib['value'])
            if matches != None:
                textBox.appendPlainText(
                    "Replaced (%s) with (%s)" % (matches.group(0), 'databaseID=%d' % (self.newDbId)))
                node.attrib['value'] = node.attrib['value'].replace(matches.group(0), 'databaseID=%d' % (self.newDbId))

        if "name" in node.attrib and node.attrib['name'] == "Database ID":
            node.attrib['value'] = str(self.newDbId)
            textBox.appendPlainText("Set Database ID tag value to %d" % (self.newDbId))

        for child in node:
            self.replaceDatabaseId(child, textBox)

    def stripPixelScript(self, rootNode):
        print ("stripPixelScript", rootNode.tag)
        if self.newDbId != None:
            self.replaceDatabaseId(rootNode, self.generator.consoleOutputTextEdit)

    def stripFiles(self, rootNode):
        textBox = self.generator.consoleOutputTextEdit
        databaseId = self.findDatabaseId(rootNode)
        textBox.appendPlainText("Database ID is %s" % (databaseId))
        savedDb = self.downloadDatabase(databaseId)
        # print ("Stripping database")
        # self.stripDatabaseFile(savedDb)
        # print ("Done stripping database")
        print ("Stripping Pixile script")
        self.stripPixelScript(rootNode)

    def sanitiseXml(self, xmlString):
        return xmlString.replace("::", "_._")

    def desanitiseXml(self, xmlString):
        return xmlString.replace("_._", "::")

    def doConversion(self):
        textBox = self.generator.consoleOutputTextEdit
        if os.path.isfile(self.location):
            locationParts = self.location.split('.')
            locationParts[-2] += "_stripped"
            outputFileName = ".".join(locationParts)
            if ".pxz" in self.location:
                textBox.appendPlainText("Archived script chosen")
                with zipfile.ZipFile(self.location, 'r') as f:
                    textBox.appendPlainText("Successfully opened archive")
                    wantedFiles = [x for x in filter(lambda x: ".pxl" in x, f.namelist())]
                    for fileName in wantedFiles:
                        textBox.appendPlainText("Going to strip: " + fileName)
                    newFileString = {}
                    for script in wantedFiles:
                        textBox.appendPlainText("Stripping " + script)
                        info = f.getinfo(script)
                        fileBytes = self.sanitiseXml(f.read(info).decode('UTF-8'))
                        root = ET.fromstring(fileBytes)
                        self.stripFiles(root)
                        outputBytes = ET.tostring(root, encoding='utf-8', method='xml')
                        outputString = self.desanitiseXml(outputBytes.decode('UTF-8'))
                        newFileString[script] = outputString
                        textBox.appendPlainText("Done stripping " + script)

                    with zipfile.ZipFile(outputFileName, 'w') as w:
                        textBox.appendPlainText("Generating " + outputFileName)
                        for archiveFile in f.namelist():
                            if archiveFile in newFileString:
                                w.writestr(f.getinfo(archiveFile), newFileString[archiveFile])
                            else:
                                info = f.getinfo(archiveFile)
                                fdata = f.read(info)
                                w.writestr(info, fdata)
                            textBox.appendPlainText("Wrote " + archiveFile)

            else:
                outputString = ""
                with open(self.location, 'r', encoding='utf-8') as f:
                    textBox.appendPlainText("Successfully opened " + self.location)
                    textBox.appendPlainText("Stripping " + self.location)
                    fileBytes = self.sanitiseXml(f.read())
                    root = ET.fromstring(fileBytes)
                    self.stripFiles(root)
                    outputBytes = ET.tostring(root, encoding='utf-8', method='xml')
                    outputString = self.desanitiseXml(outputBytes.decode('UTF-8'))

                with open(outputFileName, 'w', encoding='utf-8') as f:
                    textBox.appendPlainText("Writing file " + outputFileName)
                    f.write(outputString)

        else:
            textBox.appendPlainText("Invalid File")
        self.generator.closeProcessOutputButton.setEnabled(True)

    def run(self):
        self.dialog.show()
        self.worker.start()
        self.dialog.exec_()