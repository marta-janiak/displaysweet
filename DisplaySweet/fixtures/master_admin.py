from django.contrib import admin
from django.db.models.base import ModelBase

# Very hacky!
from projects.project_folders.capitol import models as capitol_models

for name, var in capitol_models.__dict__.items():
    if type(var) is ModelBase:
        admin.site.register(var)
