class MasterProjectRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on masterproject models to 'masterproject'"

        if model._meta.app_label == 'masterproject':
            return 'masterproject'
        return 'default'

    def db_for_write(self, model, **hints):
        "Point all operations on masterproject models to 'masterproject'"
        if model._meta.app_label == 'masterproject':
            return 'masterproject'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in masterproject app"
        if obj1._meta.app_label == 'masterproject' and obj2._meta.app_label == 'masterproject':
            return True
        # Allow if neither is masterproject app
        elif 'masterproject' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_migrate(self, db, model):
        if db == 'masterproject' or model._meta.app_label == "masterproject":
            return False # we're not using syncdb on our legacy database
        else: # but all other models/databases are fine
            return True