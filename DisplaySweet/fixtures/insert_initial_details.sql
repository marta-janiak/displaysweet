BEGIN TRANSACTION;

-- CREATE TABLE "django_content_type" ("id" integer NOT NULL PRIMARY KEY , "app_label" varchar(100) NOT NULL, "model" varchar(100) NOT NULL, UNIQUE ("app_label", "model"));
-- CREATE TABLE "django_migrations" ("id" integer NOT NULL PRIMARY KEY , "app" varchar(255) NOT NULL, "name" varchar(255) NOT NULL, "applied" timestamp NOT NULL);
-- CREATE TABLE "users_user" ("id" integer NOT NULL PRIMARY KEY , "password" varchar(128) NOT NULL, "last_login" timestamp NULL, "username" varchar(255) NOT NULL UNIQUE, "title" varchar(30) NULL, "first_name" varchar(30) NULL, "last_name" varchar(30) NULL, "email" varchar(255) NOT NULL UNIQUE, "phone_number" varchar(255) NULL, "date_of_birth" date NULL, "user_type" varchar(50) NOT NULL, "is_active" bool NOT NULL, "can_access_django_backend" bool NOT NULL, "status" varchar(50) NOT NULL);
-- CREATE TABLE "django_admin_log" ("id" integer NOT NULL PRIMARY KEY , "action_time" timestamp NOT NULL, "object_id" text NULL, "object_repr" varchar(200) NOT NULL, "action_flag" smallint NOT NULL, "change_message" text NOT NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id"), "user_id" integer NOT NULL REFERENCES "users_user" ("id"));

-- CREATE TABLE "auth_group" ("id" integer NOT NULL PRIMARY KEY , "name" varchar(80) NOT NULL UNIQUE);
-- CREATE TABLE "auth_group_permissions" ("id" integer NOT NULL PRIMARY KEY , "group_id" integer NOT NULL REFERENCES "auth_group" ("id"), "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"), UNIQUE ("group_id", "permission_id"));
-- CREATE TABLE "auth_permission" ("id" integer NOT NULL PRIMARY KEY , "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"), "codename" varchar(100) NOT NULL, "name" varchar(255) NOT NULL, UNIQUE ("content_type_id", "codename"));
-- CREATE TABLE "reversion_version" ("id" integer NOT NULL PRIMARY KEY , "object_id" text NOT NULL, "object_id_int" integer NULL, "format" varchar(255) NOT NULL, "serialized_data" text NOT NULL, "object_repr" text NOT NULL, "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"), "revision_id" integer NOT NULL REFERENCES "reversion_revision" ("id"));
-- CREATE TABLE "reversion_revision" ("id" integer NOT NULL PRIMARY KEY , "date_created" timestamp NOT NULL, "comment" text NOT NULL, "user_id" integer NULL REFERENCES "users_user" ("id"), "manager_slug" varchar(191) NOT NULL);
-- CREATE TABLE "django_session" ("session_key" varchar(40) NOT NULL PRIMARY KEY, "session_data" text NOT NULL, "expire_date" timestamp NOT NULL);
-- CREATE TABLE "django_site" ("id" integer NOT NULL PRIMARY KEY , "domain" varchar(100) NOT NULL, "name" varchar(50) NOT NULL);
-- CREATE TABLE "core_project_users" ("id" integer NOT NULL PRIMARY KEY , "project_id" integer NOT NULL REFERENCES "core_project" ("id"), "user_id" integer NOT NULL REFERENCES "users_user" ("id"), UNIQUE ("project_id", "user_id"));
-- CREATE TABLE "users_user_projects" ("id" integer NOT NULL PRIMARY KEY , "user_id" integer NOT NULL REFERENCES "users_user" ("id"), "project_id" integer NOT NULL REFERENCES "core_project" ("id"), UNIQUE ("user_id", "project_id"));
-- CREATE TABLE "core_bulkimport" ("id" integer NOT NULL PRIMARY KEY , "created" timestamp NOT NULL, "updated" timestamp NOT NULL, "status" varchar(50) NOT NULL, "table_name" varchar(255) NULL, "uploaded_xlsx" varchar(100) NULL, "uploaded_data" text NULL, "notes" text NULL, "project_id" integer NOT NULL REFERENCES "core_project" ("id"));
-- CREATE TABLE "core_displaysweetconfiguration" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL);
-- CREATE TABLE "core_displaysweetconfiguration_image_locations" ("id" integer NOT NULL PRIMARY KEY , "displaysweetconfiguration_id" integer NOT NULL REFERENCES "core_displaysweetconfiguration" ("id"), "imagelocationconfiguration_id" integer NOT NULL REFERENCES "core_imagelocationconfiguration" ("id"), UNIQUE ("displaysweetconfiguration_id", "imagelocationconfiguration_id"));
-- CREATE TABLE "core_imagelocationconfiguration" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL, "configuration_id" integer NULL REFERENCES "core_displaysweetconfiguration" ("id"), "location_type" varchar(255) NOT NULL, "image_file_location" varchar(400) NULL);
-- CREATE TABLE "core_projectconnection" ("id" integer NOT NULL PRIMARY KEY , "connection_type" varchar(50) NOT NULL, "database_file" varchar(100) NULL, "path_to_database" varchar(400) NULL, "username" varchar(30) NULL, "password" varchar(50) NULL, "project_id" integer NOT NULL REFERENCES "core_project" ("id"));
-- CREATE TABLE "core_bulkimportcompletedtemplate" ("id" integer NOT NULL PRIMARY KEY , "created" timestamp NOT NULL, "updated" timestamp NOT NULL, "status" varchar(50) NOT NULL, "uploaded_data" text NULL, "notes" text NULL, "project_id" integer NOT NULL REFERENCES "core_project" ("id"), "template_id" integer NULL REFERENCES "core_projectcsvtemplate" ("id"), "uploaded_xlsx" varchar(100) NULL);
-- CREATE TABLE "core_projectcsvtemplateimport" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "template_id" integer NOT NULL REFERENCES "core_projectcsvtemplate" ("id"));
-- CREATE TABLE "core_projectcsvtemplatefieldvalues" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "value" varchar(255) NULL, "field_id" integer NOT NULL REFERENCES "core_projectcsvtemplatefields" ("id"), "row_id" integer NOT NULL, "import_instance_id" integer NULL REFERENCES "core_projectcsvtemplateimport" ("id"));
-- CREATE TABLE "core_project" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL, "name" varchar(50) NULL, "description" text NULL, "installed" bool NOT NULL, "project_connection_name" varchar(50) NULL UNIQUE, "active" bool NOT NULL, "floor_plan_location" varchar(600) NULL, "key_plan_location" varchar(600) NULL, "level_plan_location" varchar(600) NULL, "level_plan_location_full_res" varchar(600) NULL, "image_dimensions" varchar(100) NULL);
-- CREATE TABLE "core_projectimagepath" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL, "image_path" varchar(255) NOT NULL, "project_id" integer NULL REFERENCES "core_project" ("id"), "description" varchar(400) NOT NULL, "image_mode" varchar(255) NULL, "image_output" varchar(255) NULL, "image_type" varchar(255) NULL);
-- CREATE TABLE "core_csvfileimport" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL, "csv_file" varchar(100) NOT NULL, "project_id" integer NOT NULL REFERENCES "core_project" ("id"));
-- CREATE TABLE "core_projectcsvtemplatefields" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "column_table_mapping" varchar(255) NULL, "column_field_mapping" varchar(255) NULL, "column_pretty_name" varchar(255) NULL, "template_id" integer NOT NULL REFERENCES "core_projectcsvtemplate" ("id"), "column_field_mapping_type" varchar(255) NULL, "order" integer NULL);
-- CREATE TABLE "core_csvfileimportfields" ("created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "modified" timestamp NULL, "csv_column_name" varchar(255) NULL, "column_table_mapping" varchar(255) NULL, "column_field_mapping" varchar(255) NULL, "column_pretty_name" varchar(255) NULL, "column_field_mapping_type" varchar(255) NULL, "order" integer NULL, "csv_import_id" integer NOT NULL REFERENCES "core_csvfileimport" ("id"));
-- CREATE TABLE "core_projectcsvtemplate" ("modified" timestamp NULL, "created" date NOT NULL, "id" integer NOT NULL PRIMARY KEY , "template_name" varchar(255) NULL, "project_id" integer NOT NULL REFERENCES "core_project" ("id"));

INSERT INTO "django_migrations" VALUES(1,'users','0001_initial','2015-09-06 13:59:03.908985');
INSERT INTO "django_migrations" VALUES(2,'contenttypes','0001_initial','2015-09-06 13:59:03.924984');
INSERT INTO "django_migrations" VALUES(3,'admin','0001_initial','2015-09-06 13:59:03.937455');
INSERT INTO "django_migrations" VALUES(4,'contenttypes','0002_remove_content_type_name','2015-09-06 13:59:03.976543');
INSERT INTO "django_migrations" VALUES(5,'auth','0001_initial','2015-09-06 13:59:04.010373');
INSERT INTO "django_migrations" VALUES(6,'auth','0002_alter_permission_name_max_length','2015-09-06 13:59:04.030860');
INSERT INTO "django_migrations" VALUES(7,'auth','0003_alter_user_email_max_length','2015-09-06 13:59:04.048195');
INSERT INTO "django_migrations" VALUES(8,'auth','0004_alter_user_username_opts','2015-09-06 13:59:04.065866');
INSERT INTO "django_migrations" VALUES(9,'auth','0005_alter_user_last_login_null','2015-09-06 13:59:04.083608');
INSERT INTO "django_migrations" VALUES(10,'auth','0006_require_contenttypes_0002','2015-09-06 13:59:04.085542');
INSERT INTO "django_migrations" VALUES(11,'core','0001_initial','2015-09-06 13:59:04.107142');
INSERT INTO "django_migrations" VALUES(12,'core','0002_auto_20150902_0703','2015-09-06 13:59:04.141199');
INSERT INTO "django_migrations" VALUES(13,'core','0003_auto_20150903_0349','2015-09-06 13:59:04.216954');
INSERT INTO "django_migrations" VALUES(14,'reversion','0001_initial','2015-09-06 13:59:04.251711');
INSERT INTO "django_migrations" VALUES(15,'reversion','0002_auto_20141216_1509','2015-09-06 13:59:04.275434');
INSERT INTO "django_migrations" VALUES(16,'sessions','0001_initial','2015-09-06 13:59:04.289447');
INSERT INTO "django_migrations" VALUES(17,'sites','0001_initial','2015-09-06 13:59:04.304979');
INSERT INTO "django_migrations" VALUES(18,'capitol','0001_initial','2015-09-06 21:07:37.867555');
INSERT INTO "django_migrations" VALUES(19,'core','0004_project_active','2015-09-06 21:07:37.927291');
INSERT INTO "django_migrations" VALUES(20,'core','0005_project_users','2015-09-06 21:20:48.177839');
INSERT INTO "django_migrations" VALUES(21,'core','0006_auto_20150906_2122','2015-09-06 21:22:05.326475');
INSERT INTO "django_migrations" VALUES(22,'fitzroy_v3','0001_initial','2015-09-10 10:08:09.544173');
INSERT INTO "django_migrations" VALUES(23,'core','0007_auto_20150910_1008','2015-09-10 14:38:02.656977');
INSERT INTO "django_migrations" VALUES(24,'core','0008_csvfileimport','2015-09-10 14:38:02.715989');
INSERT INTO "django_migrations" VALUES(25,'users','0002_user_projects','2015-09-16 10:16:36.071071');
INSERT INTO "django_migrations" VALUES(26,'core','0009_bulkimport','2015-09-17 12:21:58.230615');
INSERT INTO "django_migrations" VALUES(27,'core','0010_auto_20150922_1025','2015-09-22 10:25:23.408440');
INSERT INTO "django_migrations" VALUES(28,'core','0011_imagelocationconfiguration_configuration','2015-09-22 10:28:01.953401');
INSERT INTO "django_migrations" VALUES(29,'core','0012_auto_20150930_1422','2015-09-30 14:22:38.411994');
INSERT INTO "django_migrations" VALUES(30,'core','0013_auto_20151026_1406','2015-12-02 09:25:30.467932');
INSERT INTO "django_migrations" VALUES(31,'core','0014_project_level_plan_location_full_res','2015-12-02 09:25:30.575830');
INSERT INTO "django_migrations" VALUES(32,'core','0015_projectcsvtemplate_projectcsvtemplatefields','2015-12-02 09:25:30.710204');
INSERT INTO "django_migrations" VALUES(33,'core','0016_auto_20151109_1340','2015-12-02 09:25:31.015059');
INSERT INTO "django_migrations" VALUES(34,'core','0017_auto_20151109_1418','2015-12-02 09:25:31.121621');
INSERT INTO "django_migrations" VALUES(35,'core','0018_bulkimportcompletedtemplate','2015-12-02 09:25:31.196018');
INSERT INTO "django_migrations" VALUES(36,'core','0019_auto_20151109_1609','2015-12-02 09:25:31.303054');
INSERT INTO "django_migrations" VALUES(37,'core','0020_auto_20151110_1022','2015-12-02 09:25:31.584830');
INSERT INTO "django_migrations" VALUES(38,'core','0021_projectcsvtemplatefieldvalues_row_id','2015-12-02 09:25:31.703448');
INSERT INTO "django_migrations" VALUES(39,'core','0022_auto_20151110_1124','2015-12-02 09:25:32.008042');
INSERT INTO "django_migrations" VALUES(40,'core','0023_projectimagepath','2015-12-02 09:25:32.076944');
INSERT INTO "django_migrations" VALUES(41,'core','0024_projectimagepath_project','2015-12-02 09:25:32.155935');
INSERT INTO "django_migrations" VALUES(42,'core','0025_projectimagepath_description','2015-12-02 09:25:32.265674');
INSERT INTO "django_migrations" VALUES(43,'core','0026_auto_20151112_0652','2015-12-02 09:25:32.370603');
INSERT INTO "django_migrations" VALUES(44,'core','0027_project_image_dimensions','2015-12-02 09:25:32.480209');
INSERT INTO "django_migrations" VALUES(45,'core','0028_auto_20151116_1115','2015-12-02 09:25:32.687462');
INSERT INTO "django_migrations" VALUES(46,'core','0029_auto_20151122_1140','2015-12-02 09:25:33.000426');
INSERT INTO "django_migrations" VALUES(47,'core','0030_projectcsvtemplatefields_order','2015-12-02 09:25:33.123395');
INSERT INTO "django_migrations" VALUES(48,'core','0031_auto_20151124_1548','2015-12-02 09:25:33.365737');
INSERT INTO "django_migrations" VALUES(49,'core','0032_auto_20151201_1154','2015-12-02 09:25:33.609921');
INSERT INTO "django_migrations" VALUES(50,'core','0033_csvfileimportfields','2015-12-02 09:25:33.693726');
INSERT INTO "django_migrations" VALUES(51,'core','0034_auto_20151201_1248','2015-12-02 09:25:33.928190');
INSERT INTO "django_migrations" VALUES(52,'core','0035_projectcsvtemplate_modified','2015-12-02 09:25:34.190191');

INSERT INTO "users_user" VALUES(1,'pbkdf2_sha256$20000$nO1eTa2L9pP7$2axmOoutB9auqA8NJtwx5gGj0yCeuLurYfKFUM9gIbk=','2015-12-03 11:20:22.115514','martajaniak',NULL,'Marta','Janiak','marta@test.com','',NULL,'normal_user',True,True,'approved');
INSERT INTO "users_user" VALUES(2,'pbkdf2_sha256$20000$6Y6LB6IrsOop$8lP2IAUeYHK/FrXxhpeKep3EXSE5fSppsfejw3YoPfM=',NULL,'steven',NULL,'Steven','','steven@displaysweet.com','','2015-09-01','super_admin_user',True,True,'approved');
--
-- INSERT INTO "django_content_type" VALUES(1,'auth','permission');
-- INSERT INTO "django_content_type" VALUES(2,'auth','group');
-- INSERT INTO "django_content_type" VALUES(3,'contenttypes','contenttype');
-- INSERT INTO "django_content_type" VALUES(4,'sessions','session');
-- INSERT INTO "django_content_type" VALUES(5,'sites','site');
-- INSERT INTO "django_content_type" VALUES(6,'admin','logentry');
-- INSERT INTO "django_content_type" VALUES(7,'reversion','revision');
-- INSERT INTO "django_content_type" VALUES(8,'reversion','version');
-- INSERT INTO "django_content_type" VALUES(9,'core','project');
-- INSERT INTO "django_content_type" VALUES(10,'core','projectconnection');
-- INSERT INTO "django_content_type" VALUES(11,'users','user');

INSERT INTO "django_site" VALUES(1,'example.com','example.com');

INSERT INTO "core_project_users" VALUES(2,1,1);
INSERT INTO "core_project_users" VALUES(3,2,1);
INSERT INTO "core_project_users" VALUES(4,2,2);
INSERT INTO "core_project_users" VALUES(7,5,1);
INSERT INTO "core_project_users" VALUES(8,5,2);
INSERT INTO "core_project_users" VALUES(9,6,1);
INSERT INTO "core_project_users" VALUES(10,6,2);
INSERT INTO "core_project_users" VALUES(11,4,1);
INSERT INTO "core_project_users" VALUES(12,4,2);
INSERT INTO "core_project_users" VALUES(15,7,1);
INSERT INTO "core_project_users" VALUES(16,7,2);
INSERT INTO "core_project_users" VALUES(17,3,2);
INSERT INTO "core_project_users" VALUES(30,15,1);
INSERT INTO "core_project_users" VALUES(31,16,1);
INSERT INTO "core_project_users" VALUES(32,17,1);

INSERT INTO "core_displaysweetconfiguration" VALUES('2015-09-22',1,NULL);


INSERT INTO "core_projectconnection" VALUES(1,'sqlite','database_files/capitol.db',NULL,'','',1);
INSERT INTO "core_projectconnection" VALUES(2,'sqlite','database_files/fitzroy_v3.db',NULL,'','',2);
INSERT INTO "core_projectconnection" VALUES(3,'sqlite','database_files/knoxia.db',NULL,'','',3);
INSERT INTO "core_projectconnection" VALUES(4,'sqlite','database_files/exampleDatabase_fgB2j7W.db',NULL,'','',4);
INSERT INTO "core_projectconnection" VALUES(5,'sqlite','database_files/royal_como_3rHKNzt.db',NULL,'','',5);
INSERT INTO "core_projectconnection" VALUES(6,'sqlite','database_files/trentwood_YwffI27.db',NULL,'','',6);
INSERT INTO "core_projectconnection" VALUES(7,'sqlite','database_files/principal.db',NULL,'','',7);
INSERT INTO "core_projectconnection" VALUES(51,'sqlite','database_files/exampleDatabase_fgB2j7W_bngfJmy.db',NULL,NULL,NULL,15);
INSERT INTO "core_projectconnection" VALUES(52,'sqlite','',NULL,NULL,NULL,16);
INSERT INTO "core_projectconnection" VALUES(53,'sqlite','',NULL,NULL,NULL,17);



INSERT INTO "core_project" VALUES('2015-09-06',1,NULL,'Capitol','',False,'capitol',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-06',2,NULL,'Fitzroy','',False,'fitzroy_v3',False,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-10',3,NULL,'Knoxia','',False,'knoxia',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-10',4,NULL,'Master','',False,'master',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-22',5,NULL,'Royal Como','',False,'royal_como',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-22',6,NULL,'Trentwood','',False,'trentwood',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-09-30',7,NULL,'Principal','',False,'principal',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-10-15',15,NULL,'Testing - Principal','',False,'testing',True,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "core_project" VALUES('2015-12-02',16,NULL,'new test','',False,'newtest',False,NULL,NULL,NULL,NULL,'image_dimensions/dimensions_aioIdAX.xml');
INSERT INTO "core_project" VALUES('2015-12-02',17,NULL,'another test','',False,'another',False,NULL,NULL,NULL,NULL,'image_dimensions/dimensions_kDwXPv6.xml');


COMMIT;
