import os
import csv
import subprocess
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.apps import apps
from django.conf import settings
from django.http import HttpResponse
from django.db.models.fields.related import ManyToOneRel
from django.forms.models import inlineformset_factory

from core.generic_views import ListView, ProjectListCallbackView, DetailView, VersionView, UpdateView, CreateView
from core.models import Project, ProjectFamily, ProjectCSVTemplateFields, ProjectVersion, ProjectConnection
from projects.permissions import CreatePermissionMixin, ProjectModelMixin
from projects.forms import ProjectAddForm, ProjectFamilyAddForm, ProjectVersionCommentForm, ProjectScriptsForm, \
                           AddProjectTranslation
from users.models import User
import shutil

from projects.models.model_mixin import PrimaryModelMixin


@login_required
def project_version_details(request, pk, version_idx=None):
    template = "projects/version/version.html"

    all_families = ProjectFamily.objects.all().extra(select={'case_insensitive_title':
                                                                 'lower(family_name)'}).order_by(
                                                                 'case_insensitive_title')

    project_family = ProjectFamily.objects.get(pk=pk)
    model_mixin = PrimaryModelMixin(request=request, project_id=project_family.latest.pk)

    all_project_families = ProjectFamily.objects.all().extra(select={'case_insensitive_title':
                                                                 'lower(family_name)'}).order_by(
                                                                 'case_insensitive_title')

    project_list_ids = []
    for family in all_project_families:
        if family.latest:
            project_list_ids.append(family.latest.pk)

    all_projects = Project.objects.filter(pk__in=project_list_ids).extra(select={'case_insensitive_title': 'lower(name)'}).order_by('case_insensitive_title')

    context = {
        'project': model_mixin.project,
        'all_projects': all_projects,
        'family': project_family,
        'all_families': all_families,
        'all_project_families': all_project_families
    }

    return render(request, template, context)


@login_required
def get_active_project_version(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    if project.version_family:
        all_project_versions = Project.objects.filter(version_family=project.version_family, version=project.version_family.active_version)

        if all_project_versions:
            project = all_project_versions[0]

            if project and project.project_copied_from and project.get_publish_path:
                base_image_location = settings.BASE_S3_IMAGE_LOCATION
                copy_image_location = os.path.join(base_image_location, 'project_images/%s/' % project.project_copied_from.project_connection_name)
                new_location = os.path.join(base_image_location, 'project_images/%s/' % project.project_connection_name)

                try:
                    shutil.copytree(copy_image_location, new_location)
                except Exception as e:
                    print ('could not copy image directory', e)
                    print('locations:', copy_image_location, 'to', new_location)

                resources = model_mixin.model_object("Resources")
                for item in resources:
                    item.path = item.path.replace(project.project_copied_from.project_connection_name, project.project_connection_name)
                    item.save(using=project.project_connection_name)

    return redirect('projects-version', project.pk)


@login_required
def project_families(request, pk=None):
    template = "projects/version/project_families.html"

    all_families = ProjectFamily.objects.all().extra(select={'case_insensitive_title':
                                                     'lower(family_name)'}).order_by('case_insensitive_title')

    if pk:
        family = ProjectFamily.objects.get(pk=pk)

        all_projects = family.project_list.all().extra(select={'case_insensitive_title':
                                                                   'lower(name)'}).order_by('case_insensitive_title')

        all_project_ids = all_projects.values_list('pk', flat=True).distinct()

        not_in_family = Project.objects.all().exclude(pk__in=all_project_ids).extra(select={'case_insensitive_title':
                                                                                                'lower(name)'}).order_by('case_insensitive_title')

        family_form = ProjectFamilyAddForm(instance=family)

    else:
        all_projects = []

        not_in_family = Project.objects.all().extra(select={'case_insensitive_title': 'lower(name)'}).order_by(
            'case_insensitive_title')

        family = None
        family_form = ProjectFamilyAddForm()

    form = ProjectAddForm()

    context = {
        'all_projects': all_projects,
        'all_families': all_families,
        'not_in_family': not_in_family,
        'form': form,
        'family_form': family_form,
        'family': family
    }

    return render(request, template, context)