from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.apps import apps
import datetime
from django.db import connections
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import inlineformset_factory

from projects.forms import BulkImportCompletedForm, get_template_creator_csv_column_formset, CSVFileImportAddForm, \
                           BulkImportUploadForm, CSVFileImportAllTablesForm, get_template_creator_formset, \
                           get_apartment_template_formset, CreateImportTemplateForm

from utils.xlxs_generator import BulkImportTemplate
from core.models import *
from utils.revision_helpers import add_new_revision
from projects.models.model_mixin import PrimaryModelMixin
from django.db import transaction


@login_required
def select_import_template_preview(request, pk, template_id):
    template = "projects/import/select_preview.html"
    project = Project.objects.get(pk=pk)

    import_templates = ProjectCSVTemplate.objects.filter(project=project)
    selected_template = ProjectCSVTemplate.objects.get(pk=template_id)
    imports = ProjectCSVTemplateImport.objects.filter(template=selected_template)

    rows = None
    row_dictionary = None
    headers = None

    floor_dictionary = {}
    floor_values = []
    apartment_list = []

    if "delete_import_id" in request.GET:
        ProjectCSVTemplateImport.objects.get(pk=request.GET['delete_import_id']).delete()
        return redirect('projects-preview-imports', project.pk, template_id)

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    preview_id = None
    preview = None

    key_list = []
    aspect_field_list = []
    building_list = []

    if "preview_id" in request.GET:
        preview_id = request.GET['preview_id']
        preview = ProjectCSVTemplateImport.objects.get(pk=preview_id)

        headers = ProjectCSVTemplateFields.objects.filter(template=preview.template)
        rows = ProjectCSVTemplateFieldValues.objects.filter(import_instance=preview).order_by('row_id')

        building_fields = ProjectCSVTemplateFields.objects.filter(template=preview.template,
                                                                  column_table_mapping__iexact='buildings')

        floor_fields = ProjectCSVTemplateFields.objects.filter(template=preview.template,
                                                               column_field_mapping='floor')

        aspect_fields = ProjectCSVTemplateFields.objects.filter(template=preview.template,
                                                                is_aspects_field=True)

        for a_field in aspect_fields:
            aspect_field_list.append(a_field.column_field_mapping)

        editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
        template_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template).order_by('order')

        for field in template_fields:
            try:
                if field.column_field_mapping not in key_list and not str(field.column_field_mapping).strip() == "New Field:":
                    key_list.append(field.column_field_mapping)
            except:
                if field.column_field_mapping not in key_list and not field.column_field_mapping == "New Field:":
                    key_list.append(field.column_field_mapping)

        row_dictionary = {}
        for row in rows:
            row_dictionary[row.row_id] = {}
            row_dictionary[row.row_id]['row_id'] = row.row_id
            row_dictionary[row.row_id]['aspect_fields'] = aspect_field_list

        for row in rows:
            if str(row.field.column_table_mapping).lower() == "buildings":
                row_dictionary[row.row_id]['building_name'] = row.value
            else:
                row_dictionary[row.row_id][str(row.field.column_field_mapping).lower()] = row.value

        building_values = ProjectCSVTemplateFieldValues.objects.filter(field=building_fields).values_list('value', flat=True).distinct()

        building_floor_list = {}
        for building in building_values:
            if building:
                building_floor_list[building] = []

        if floor_fields:
            floor_fields = floor_fields[0]

            all_building_values =  ProjectCSVTemplateFieldValues.objects.filter(field=building_fields)

            for row in all_building_values:
                row_id = row.row_id
                straight_floor_values = ProjectCSVTemplateFieldValues.objects.filter(field=floor_fields,
                                                                                     row_id=row_id,
                                                                                     value__isnull=False).values_list('value', flat=True).distinct().exclude(value='')

                if straight_floor_values:
                    straight_floor_values = straight_floor_values[0]
                    try:
                        floor_name_value = int(float(straight_floor_values))
                    except Exception as e:
                        floor_name_value = straight_floor_values

                    if floor_name_value not in building_floor_list[row.value]:
                        building_floor_list[row.value].append(floor_name_value)

            straight_floor_values = ProjectCSVTemplateFieldValues.objects.filter(field=floor_fields).values_list('value',
                                                                                                                 flat=True).distinct()

            straight_floor_values = sorted(straight_floor_values)

            for value in straight_floor_values:
                if value:
                    try:
                        floor_name_value = int(float(value))
                    except Exception as e:
                        floor_name_value = value

                    floor_values.append(floor_name_value)

            for value in floor_values:
                if value:
                    floor_dictionary[value] = {}

            for value in floor_values:
                if value:
                    apartments = 0
                    floor_dictionary[value]['number'] = value
                    apartment_list = []
                    building_name = 'n/a'

                    for k, v in row_dictionary.items():
                        #changing all items to strings because it round property name numbers down.
                        for i_key, i_value in v.items():
                            try:
                                v.update({i_key: str(i_value)})
                            except:
                                v.update({i_key: i_value})

                        if 'building_name' in v:
                            building_name = v['building_name']

                            if building_name and building_name not in building_list:
                                building_list.append(building_name)

                        if "floor" in v:
                            floor_value_name = v['floor']
                            try:
                                floor_value_name = int(float(floor_value_name))
                            except Exception as e:
                                floor_value_name = floor_value_name

                            if floor_value_name == value:
                                apartments += 1
                                apartment_list.append(v)

                    floor_dictionary[value]['building_name'] = building_name
                    floor_dictionary[value]['apartments'] = apartments
                    floor_dictionary[value]['apartment_list'] = apartment_list

        building_details_list = {}

        floor_names = []

        for name in building_list:
            building_details_list[name] = {}

        for name in building_list:
            for row, value in floor_dictionary.items():
                if row not in floor_names:
                    floor_names.append(row)

                building_details_list[name][row] = {}
                building_details_list[name][row]['apartment_list'] = []

                for item in value['apartment_list']:
                    if "building_name" in item:
                        building_name = item['building_name']
                        if building_name == name:
                            building_details_list[name][row]['apartment_list'].append(item)

    total_apartments_created = 0
    total_errors = 0

    cursor = connections[project.project_connection_name].cursor()

    if request.method == "POST":

        time_started = datetime.datetime.now()

        buildings = model_mixin.model_object('Buildings').all()

        names_model_object = model_mixin.get_class_object('Names')
        property_model_object = model_mixin.get_class_object('Properties')
        property_type_model_object = model_mixin.get_class_object('Propertytypes')

        override = False
        property_type_override = None
        apartment_type_object = property_type_model_object.objects.using(project.project_connection_name).get(type='apartment')
        if "property_override_type" in request.POST and request.POST['property_override_type']:
            property_type_override = request.POST['property_override_type']
            override = True

        apartment_model_fields = property_model_object._meta.get_fields()
        names_category = model_mixin.get_string_category('names')

        apartment_fields = []
        for field in apartment_model_fields:
            if field.name not in ['id', 'floor', 'building']:
                apartment_fields.append(field)

        if len(buildings) and len(building_list) == 0:
            building_name = model_mixin.model_object("Names").filter(english='Building 1')
            if not building_name:
                model_mixin.model_object("Names").create(english='Building 1')
                building_name = model_mixin.model_object("Names").filter(english='Building 1')

            building_name = building_name[0]
            building = model_mixin.model_object("Buildings").filter(name=building_name)
            if not building:
                model_mixin.model_object("Buildings").create(name=building_name)

        apartment_update_list = []
        for name in building_list:
            building_name = model_mixin.model_object("Names").filter(english=name)
            if not building_name:
                model_mixin.model_object("Names").create(english=name)
                building_name = model_mixin.model_object("Names").filter(english=name)

            building_name = building_name[0]

            building = model_mixin.model_object("Buildings").filter(name=building_name)
            if not building:
                model_mixin.model_object("Buildings").create(name=building_name)
                building = model_mixin.model_object("Buildings").filter(name=building_name)

            building = building[0]

            fo = 0
            for floor_name_value in building_floor_list[name]:

                fo+=1

                try:
                    floor_name_value = int(float(floor_name_value))
                except Exception as e:
                    pass

                kwargs = {
                    '{0}'.format('english'): floor_name_value,
                    '{0}'.format('text_table_category'): names_category,
                    '{0}'.format('chinese'): building.pk,
                }

                floor_name = model_mixin.model_object("Names").filter(**kwargs)
                if not floor_name:
                    model_mixin.model_object("Names").create(**kwargs)
                    floor_name = names_model_object.objects.using(project.project_connection_name).filter(**kwargs)[0]
                else:
                    floor_name = floor_name[0]

                floor_kwargs_check = {
                    '{0}'.format('name'): floor_name,
                }

                new_floor = model_mixin.model_object("Floors").filter(**floor_kwargs_check)

                if not new_floor:

                    floor_kwargs = {
                        '{0}'.format('name'): floor_name,
                        '{0}'.format('orderidx'): fo,
                    }

                    model_mixin.model_object("Floors").create(**floor_kwargs)
                    new_floor = model_mixin.model_object("Floors").filter(**floor_kwargs_check)

                    #Save level plan placeholder
                    if new_floor:
                        model_mixin.klass("LevelPlans").save_level_plan_image(new_floor[0].pk)

                if new_floor:
                    new_floor = new_floor[0]

                #create the floors building connection:
                kwargs = {
                    '{0}'.format('building'): building,
                    '{0}'.format('floor'): new_floor,
                }

                floor_building = model_mixin.model_object("FloorsBuildings").filter(**kwargs)
                if not floor_building:
                    model_mixin.model_object("FloorsBuildings").create(**kwargs)
                    floor_building = model_mixin.model_object("FloorsBuildings").filter(**kwargs)

                floor_building = floor_building[0]
                if name and new_floor and floor_name_value and floor_building:
                    building_details_list[name][floor_name_value]['floor_building'] = floor_building

                    i = 0
                    for item in building_details_list[name][floor_name_value]['apartment_list']:
                        i += 1

                        property_type = apartment_type_object
                        apartment_kwargs = {'building_id': building.pk,
                                            'floor': new_floor,
                                            'property_type': apartment_type_object}

                        check_update_kwargs = {'building_id': building.pk, 'floor': new_floor}
                        string_kwargs = {}
                        int_fields_kwargs = {}
                        boolean_fields_kwargs = {}
                        float_fields_kwargs = {}
                        aspect_kwargs = []

                        count_aspect_fields = ProjectCSVTemplateFields.objects.filter(template=selected_template,
                                                                                      is_aspects_field=True)

                        count_aspect_fields = count_aspect_fields.count()

                        for i_key, i_value in item.items():
                            i_key = model_mixin.klass("Strings").clean_for_mapping(i_key)

                            try:
                                i_value = str(i_value).strip()
                            except Exception as e:
                                pass

                            mapping_table_key = ProjectCSVTemplateFields.objects.filter(template=selected_template,
                                                                                        column_field_mapping=i_key)

                            if mapping_table_key:
                                mapping_table_key = mapping_table_key[0]
                                table_mapping = mapping_table_key.column_table_mapping

                                if mapping_table_key.is_aspects_field:
                                    i_value = i_value.replace('"', '').replace('"', '').replace("'", "").replace("'",
                                                                                                                 "")
                                    i_value = i_value.strip()
                                    aspect_fields_listed = i_value.split(',')

                                    for aspect_value in aspect_fields_listed:
                                        if aspect_value:
                                            aspect_kwargs.append(aspect_value)

                                if "Propertiesstringfields" in table_mapping:
                                    if count_aspect_fields > 1:
                                        string_kwargs.update({i_key: i_value})

                                    string_kwargs.update({i_key: i_value})

                                    if i_key == "floor plan typical":
                                        string_kwargs.update({"floor_plan_typical": i_value})

                                elif "Propertiesintfields" in table_mapping:
                                    try:
                                        int_fields_kwargs.update({i_key: int(i_value)})
                                    except:
                                        pass

                                elif "Propertiesbooleanfields" in table_mapping:
                                    try:
                                        boolean_fields_kwargs.update({i_key: bool(i_value)})
                                    except:
                                        pass

                                elif "Propertiesfloatfields" in table_mapping:
                                    try:
                                        float_fields_kwargs.update({i_key: float(i_value)})
                                    except:
                                        pass
                                else:
                                    apartment_key = i_key

                                    for a_field in apartment_fields:
                                        if a_field.name == apartment_key and apartment_key == "property_type":
                                            if not override:
                                                property_type = model_mixin.model_object("PropertyTypes").filter(type__iexact=i_value)

                                                if not property_type:
                                                    model_mixin.model_object("PropertyTypes").create(
                                                        type=i_value)
                                                    property_type = model_mixin.model_object("PropertyTypes").filter(
                                                        type__iexact=i_value)

                                                property_type = property_type[0]

                                                apartment_kwargs.update(
                                                    {'{0}'.format(a_field.name): property_type}
                                                )
                                            else:
                                                property_type = i_value

                                        elif type(a_field) in [ForeignKey, ManyToOneRel] and a_field.name == apartment_key:

                                            name_text_category = model_mixin.model_object("TextTableCategories").filter(category="Property Names")

                                            if not name_text_category:
                                                model_mixin.model_object(
                                                    "TextTableCategories").create(category="Property Names")
                                                name_text_category = model_mixin.model_object(
                                                    "TextTableCategories").filter(category="Property Names")

                                            name_text_category = name_text_category[0]

                                            new_item_name = model_mixin.model_object("Names").filter(english__iexact=i_value,
                                                                                                     text_table_category=name_text_category)

                                            if not new_item_name:
                                                new_item_name = model_mixin.model_object("Names").filter(english__iexact=i_value)

                                            if not new_item_name:
                                                kwargs = {
                                                    '{0}'.format('english'): i_value,
                                                    '{0}'.format('text_table_category'): name_text_category
                                                }
                                                model_mixin.model_object("Names").create(**kwargs)
                                                new_item_name = model_mixin.model_object("Names").filter(english__iexact=i_value,
                                                                                                         text_table_category=name_text_category)
                                                new_item_name = new_item_name[0]
                                            else:
                                                new_item_name = new_item_name[0]

                                            related_object = a_field.rel.to

                                            try:
                                                new_item_kwargs = {
                                                    '{0}'.format('name_id'): new_item_name.pk,
                                                }
                                                new_items = related_object.objects.using(project.project_connection_name).filter(**new_item_kwargs)

                                            except Exception as e:
                                                new_item_kwargs = {
                                                    '{0}'.format('english'): i_value,
                                                }
                                                new_items = related_object.objects.using(project.project_connection_name).filter(**new_item_kwargs)

                                            if not new_items:

                                                related_object.objects.using(project.project_connection_name).create(**new_item_kwargs)
                                                new_items = related_object.objects.using(project.project_connection_name).filter(**new_item_kwargs)

                                                if new_items:
                                                    new_item = new_items[0]
                                                    new_item = new_item.pk
                                                else:
                                                    new_item = None

                                            else:
                                                new_item = new_items[0]
                                                new_item_id = new_item.pk

                                            if new_item:
                                                apartment_kwargs.update(
                                                    {'{0}'.format('name_id'): new_item_id }
                                                )

                                                check_update_kwargs.update(
                                                    {'{0}'.format('name_id'): new_item_id}
                                                )

                        # try:
                        property_model_object = model_mixin.get_class_object('Properties')
                        existing_apartment_id = None

                        if "name_id" in apartment_kwargs:
                            if "floor" in apartment_kwargs:
                                floor = apartment_kwargs['floor']
                                building_id = apartment_kwargs['building_id']
                                apartment_name = apartment_kwargs['name_id']

                                existing_apartment = model_mixin.model_object("Properties").filter(name=apartment_name,
                                                                                                   floor=floor,
                                                                                                   property_type=property_type)

                                if existing_apartment:
                                    created_new_apartment = False
                                    existing_apartment = existing_apartment[0]
                                    for ekey, evalue in apartment_kwargs.items():
                                        if not ekey == "building_id":
                                            existing_apartment.__setattr__(ekey, evalue)

                                    existing_apartment.save(using=project.project_connection_name)
                                    new_apartment = existing_apartment

                                    if override:
                                        new_apartment.property_type_id = property_type_override
                                        new_apartment.save(using=model_mixin.project.project_connection_name)

                                else:
                                    created_new_apartment = True
                                    shortened_kwargs = {'orderidx': i}
                                    for o, p in apartment_kwargs.items():
                                        if not o == "building_id":
                                            shortened_kwargs.update({o : p})

                                    new_apartment = model_mixin.model_object("Properties").filter(**shortened_kwargs)

                                    if not new_apartment:
                                        try:
                                            model_mixin.model_object("Properties").create(**shortened_kwargs)

                                            new_apartment = model_mixin.model_object("Properties").filter(**shortened_kwargs)

                                            if new_apartment:
                                                new_apartment = new_apartment[0]

                                                if override:
                                                    new_apartment.property_type_id = property_type_override
                                                    new_apartment.orderidx = i
                                                    new_apartment.save(using=model_mixin.project.project_connection_name)

                                            total_apartments_created += 1
                                        except:
                                            total_errors += 1
                                    else:
                                        if new_apartment:
                                            new_apartment = new_apartment[0]

                                if new_apartment:
                                    # create all plans
                                    model_mixin.klass("LevelPlans").save_floor_and_key_plan_image(property_id=new_apartment.pk)

                                    apartment_kwargs.update({'building_id': building_id})
                                    apartment_kwargs.update({'property_id': new_apartment.pk})
                                    apartment_kwargs.update({'created_new_apartment': created_new_apartment})

                                    if not string_kwargs == {}:
                                        apartment_kwargs.update({'Propertiesstringfields': string_kwargs})

                                    if not int_fields_kwargs == {}:
                                        apartment_kwargs.update({'Propertiesintfields': int_fields_kwargs})

                                    if not boolean_fields_kwargs == {}:
                                        apartment_kwargs.update({'Propertiesbooleanfields': boolean_fields_kwargs})

                                    if not float_fields_kwargs == {}:
                                        apartment_kwargs.update({'Propertiesfloatfields': float_fields_kwargs})

                                    if not aspect_kwargs == {}:
                                        apartment_kwargs.update({'PropertyAspects': aspect_kwargs})

                                    apartment_update_list.append(apartment_kwargs)

        # key_table_dict = {'Propertiesstringfields': 'properties_string_fields',
        #                   'Propertiesintfields': 'properties_int_fields',
        #                   'Propertiesbooleanfields': 'properties_boolean_fields',
        #                   'Propertiesfloatfields': 'properties_float_fields'}

        print ('-------- ')
        print ('at start of import transaction', datetime.datetime.now())
        print ('-------- ')

        type_string_key_table = {}

        for new_apartment in apartment_update_list:
            property_id = new_apartment['property_id']
            created_new_apartment = new_apartment['created_new_apartment']

            # print('----', new_apartment)
            # print(datetime.datetime.now())
            # print('')

            for key, value in new_apartment.items():
                if key in ['Propertiesstringfields', 'Propertiesintfields', 'Propertiesbooleanfields', 'Propertiesfloatfields']:
                    for ikey, ivalue in value.items():
                        try:
                            type_string_id = type_string_key_table[ikey]
                        except Exception as e:
                            strings_item = model_mixin.klass("TextTableCategories").check_category_string(key, ikey)
                            type_string_id = strings_item.pk
                            type_string_key_table.update({ikey: type_string_id})

                        try:
                            model_mixin.model_object(key).update_or_create(property_id=property_id,
                                                                           type_string_id=type_string_id,
                                                                           defaults={'value': ivalue})

                        except Exception as e:
                            print(e)
                            try:
                                model_mixin.model_object(key).filter(property_id=property_id,
                                                                     type_string_id=type_string_id).update(value=ivalue)
                            except Exception as d:
                                print('error', d)

                if key == "PropertyAspects":
                    for aspect_key in value:
                        aspect_field = model_mixin.model_object("PropertyAspects").filter(property_id=property_id, key=aspect_key)
                        if not aspect_field:
                            model_mixin.model_object("PropertyAspects").create(property_id=property_id, key=aspect_key, resource_id=0)

        time_ended = datetime.datetime.now()
        print ('-------- ')
        print ('at end of import transaction', datetime.datetime.now())
        print ('-------- ')

        print ('Import time taken: %s seconds' % (time_ended - time_started).total_seconds())

        model_mixin.klass("Properties").update_property_typicals()

        messages.success(request, "Property details imported successfully")
        return redirect('projects-model-filter', project.pk, 'Properties')

    property_types = model_mixin.model_object("PropertyTypes").all().order_by('type')


    context = {
        'project': project,
        'import_templates': import_templates,
        'imports': imports,
        'selected_template': selected_template,
        'preview': preview,
        'preview_rows': rows,
        'row_dictionary': row_dictionary,
        'floor_values': floor_values,
        'floor_dictionary': floor_dictionary,
        'apartment_list': apartment_list,
        'preview_id': preview_id,
        'total_apartments_created': total_apartments_created,
        'total_errors': total_errors,
        'headers': headers,
        'key_list': key_list,
        'key_list_count': len(key_list) + 1,
        'property_types': property_types
    }

    return render(request, template, context)


def get_sec(s):
    l = s.split(':')
    return float(l[0]) * 3600 + float(l[1]) * 60 + float(l[2])


@login_required
def created_import_templates(request, pk):
    template = "projects/import/import_templates.html"

    projects = Project.objects.all()

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    myapp = apps.get_app_config(model_mixin.project.project_connection_name)
    model_names = myapp.models.values()

    import_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project)

    if "delete_id" in request.GET:
        ProjectCSVTemplate.objects.get(id=request.GET['delete_id']).delete()
        messages.success(request, "Template deleted successfully")
        return redirect('projects-select-template', model_mixin.project.pk)

    if "export_format" in request.GET:
        if "id" in request.GET:
            editable_template = ProjectCSVTemplate.objects.get(pk=request.GET['id'])

            filename = 'database_template_data_%s.csv' % editable_template.template_name
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename

            writer = csv.writer(response)

            header_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template).order_by(
                'order').values_list('column_pretty_name', flat=True)
            writer.writerow(header_fields)

            return response

    if "export_format_with_data" in request.GET:
        if "id" in request.GET:
            editable_template = ProjectCSVTemplate.objects.get(pk=request.GET['id'])
            template_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template).order_by('order')

            filename = 'database_template_data_%s.csv' % editable_template.template_name
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename

            writer = csv.writer(response)

            header_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template).order_by('order').values_list('column_pretty_name', flat=True)
            writer.writerow(header_fields)

            properties = model_mixin.model_object("Properties").all()

            for property in properties:
                property_fields, data_list, key_list = model_mixin.klass("Floors").get_select_preview_floor_dictionary(property,
                                                                                                                       template_fields)

                writer.writerow(data_list)

            return response

    bulk_import = BulkImportCompletedTemplate.objects.create(project=model_mixin.project)
    upload_form = BulkImportCompletedForm(instance=bulk_import)

    project_tables = []
    for mname in model_names:
        project_tables.append(str(mname.__name__))

    simple_form = CSVFileImportAddForm(projects, project_tables, initial={'project': model_mixin.project})

    if request.method == "POST":

        if 'submit__simple_upload' in request.POST:
            simple_form = CSVFileImportAddForm(projects, project_tables, request.POST, request.FILES)

            if simple_form.is_valid():
                mapping = simple_form.save()
                mapping.create_field_mappings(model_mixin)

                add_new_revision(model_mixin.project, request, Project, "Import template created for %s " % model_mixin.project.name)

                messages.success(request, "Header files selected. Please map your file to table columns.")
                return redirect('projects-map-fields', model_mixin.project.pk, mapping.pk)

        if 'submit__upload' in request.POST:

            if "template" in request.POST and request.POST['template']:
                template = request.POST['template']
                template = ProjectCSVTemplate.objects.get(pk=template)

                if 'uploaded_xlsx' in request.FILES and request.FILES['uploaded_xlsx']:
                    uploaded_file = request.FILES['uploaded_xlsx']
                    file_extension = uploaded_file.name[-3:]

                    if file_extension in ["csv", "txt"]:

                        bulk_import = BulkImportCompletedTemplate.objects.create(uploaded_xlsx=uploaded_file,
                                                                                               project=model_mixin.project,
                                                                                               template=template)

                        bulk_import.copy_to_import_template_fields()

                    else:
                        upload_form = BulkImportCompletedForm(request.POST, request.FILES, instance=bulk_import)
                        if upload_form.is_valid():
                            bulk_import = upload_form.save()
                            bulk_import.copy_to_import_template_fields()

                    selected_import_templates = ProjectCSVTemplateImport.objects.filter(
                        template__project=model_mixin.project).order_by("-pk")

                    selected_import_templates = selected_import_templates[0]

                    add_new_revision(model_mixin.project, request, Project, "Import success and template created for %s " % model_mixin.project.name)

                    messages.success(request, "You excel file was imported successfully the information is ready for preview.")
                    return redirect('/projects/%s/select/template/%s/preview/?preview_id=%s' % (model_mixin.project.pk,
                                                                                                bulk_import.template.pk,
                                                                                                selected_import_templates.pk))

    context = {
        'project': model_mixin.project,
        'import_templates': import_templates,
        'upload_form': upload_form,
        'simple_form': simple_form
    }

    return render(request, template, context)


@login_required
def create_template_header_list(request, pk):
    template = "projects/import/create_template_header.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                              form=get_template_creator_formset(model_mixin.project),
                                              can_delete=True, extra=30)

    form = CreateImportTemplateForm(initial={'project': model_mixin.project})
    ingredient_formset = IngredientFormSet(instance=Project())

    if request.method == "POST":
        form = CreateImportTemplateForm(request.POST)

        if form.is_valid():
            mapping_template = form.save(commit=False)
            ingredient_formset = IngredientFormSet(request.POST, instance=mapping_template)

            if ingredient_formset.is_valid():
                mapping_template.save()
                ingredient_formset.save()

                new_fields_to_create = ProjectCSVTemplateFields.objects.filter(template=mapping_template,
                                                                               column_field_mapping='New Field:',
                                                                               column_pretty_name__isnull=False)

                model_mixin.klass("Properties").add_new_plain_string_value("floor_plan_typical")

                for field in new_fields_to_create:
                    english_value = model_mixin.klass("Strings").clean_for_mapping(field.column_pretty_name)
                    model_mixin.klass("TextTableCategories").check_category_string(field.column_table_mapping, english_value)

                    model_mixin.klass("Properties").add_new_string_value(field)
                    field.column_field_mapping = english_value
                    field.save()

                    properties = model_mixin.model_object("Properties").all()
                    pretty_field_name = model_mixin.klass("Strings").clean_for_new_mapping(field.column_pretty_name)

                    for property in properties:
                        type_string = model_mixin.model_object("Strings").filter(english=pretty_field_name)

                        if not type_string:
                            model_mixin.model_object("Strings").create(english=pretty_field_name)
                            type_string = model_mixin.model_object("Strings").filter(english=pretty_field_name)

                        type_string = type_string[0]

                        try:
                            check_field = model_mixin.model_object(field.column_table_mapping).filter(type_string=type_string,
                                                                                                      property=property)
                            if not check_field:
                                model_mixin.model_object(field.column_table_mapping).create(
                                    type_string=type_string, property=property)

                        except Exception as e:
                            print('save error', e)
                            pass

                blank_fields = ProjectCSVTemplateFields.objects.filter(template=mapping_template, column_pretty_name='')
                blank_fields.delete()

                messages.success(request, "Header files selected. Please map your file to table columns.")
                return redirect('projects-select-template', model_mixin.project.pk)

        else:
            ingredient_formset = IngredientFormSet(request.POST)

    context = {
        'project': model_mixin.project,
        'template_form': form,
        'template_columns_form': ingredient_formset
    }

    return render(request, template, context)


@login_required
def edit_import_template(request, pk, template_id):
    template = "projects/import/edit_template_header.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
    total_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template)

    IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                              form=get_template_creator_formset(model_mixin.project),
                                              can_delete=True, extra=30)

    form = CreateImportTemplateForm(instance=editable_template)
    ingredient_formset = IngredientFormSet(instance=editable_template,
                                           queryset=editable_template.project_template_fields.order_by("order"))

    if request.method == "POST":
        form = CreateImportTemplateForm(request.POST)

        if form.is_valid():
            if "template_name" in request.POST:
                editable_template.template_name = request.POST['template_name']
                editable_template.save()

            ingredient_formset = IngredientFormSet(request.POST, instance=editable_template)

            if ingredient_formset.is_valid():
                for form in ingredient_formset:
                    if form.cleaned_data and not form.cleaned_data['column_table_mapping'] == 'Select a table mapping':
                        form.save()

                blank_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template, column_pretty_name='')
                blank_fields.delete()

                new_fields_to_create = ProjectCSVTemplateFields.objects.filter(template=editable_template,
                                                                               column_field_mapping='New Field:',
                                                                               column_pretty_name__isnull=False)

                success = True
                for field in new_fields_to_create:
                    english_value = model_mixin.klass("Strings").clean_for_mapping(field.column_pretty_name)
                    model_mixin.klass("TextTableCategories").check_category_string(field.column_table_mapping,
                                                                                   english_value)
                    model_mixin.klass("Properties").add_new_string_value(field)

                    try:
                        field.column_field_mapping = english_value
                    except:
                        pass

                    field.save()

                if success:
                    messages.success(request, "Import template saved successfully.")
                if not success:
                    messages.error(request, "There was an error with some of the data in your template")

                return redirect('projects-select-template', model_mixin.project.pk)

        else:
            ingredient_formset = IngredientFormSet(request.POST)


    context = {
        'project': model_mixin.project,
        'template_form': form,
        'template_columns_form': ingredient_formset,
        'total_fields': len(total_fields)
    }

    return render(request, template, context)


@login_required
def import_header_list(request, pk):
    template = "projects/import/create_header_set.html"

    projects = Project.objects.all()
    project = Project.objects.get(pk=pk)
    myapp = apps.get_app_config(project.project_connection_name)
    model_names = myapp.models.values()

    project_tables = []
    for mname in model_names:
        project_tables.append(str(mname.__name__))

    form = CSVFileImportAddForm(projects, project_tables, initial={'project': project})

    if request.method == "POST":
        form = CSVFileImportAddForm(projects, project_tables, request.POST, request.FILES)

        if form.is_valid():
            mapping = form.save()
            messages.success(request, "Header files selected. Please map your file to table columns.")
            return redirect('projects-map-fields', project.pk, mapping.pk)

    project_permissions = ProjectsPermissionsMixin()
    actions = []

    context = {
        'project': project,
        'form': form,
        'actions': actions
    }

    return render(request, template, context)


@login_required
def import_header_list_multiple_sources(request, pk):
    template = "projects/import/import_headers_multiple.html"

    projects = Project.objects.all()
    project = Project.objects.get(pk=pk)
    myapp = apps.get_app_config(project.project_connection_name)
    model_names = myapp.models.values()

    project_tables = []
    for mname in model_names:
        project_tables.append(str(mname.__name__))

    form = CSVFileImportAllTablesForm(projects, project_tables, initial={'project': project})

    if request.method == "POST":
        form = CSVFileImportAllTablesForm(projects, project_tables, request.POST, request.FILES)

        if form.is_valid():
            mapping = form.save()
            add_new_revision(project, request, Project, "Import template edited for %s " % project.name)
            messages.success(request, "Header files selected. Please map your file to table columns.")
            return redirect('projects-map-multiple-fields', project.pk, mapping.pk)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Import CSV to Single Table' , reverse('projects-import-header', args=[project.pk])))

    context = {
        'project': project,
        'form': form,
        'actions': actions
    }

    return render(request, template, context)


@login_required
def project_model_csv_template(request, pk, model_name):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(response)

    project = Project.objects.get(pk=pk)
    add_new_revision(project, request, Project, "Template [%s] downloaded for %s " % (model_name, project.name))
    project_model_object = get_model(project.project_connection_name, model_name)
    field_names = project_model_object._meta.get_all_field_names()
    writer.writerow(field_names)

    return response


@login_required
def import_csv_table_map(request, pk, file_id):
    template = "projects/import/map_file_to_table.html"

    mapping = CSVFileImport.objects.get(pk=file_id)
    total_fields = len(mapping.csv_template_fields.all())

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    column_mapping_preview = None
    submit_function = None

    IngredientFormSet = inlineformset_factory(CSVFileImport, CSVFileImportFields,
                                              form=get_template_creator_csv_column_formset(model_mixin.project),
                                              can_delete=True, extra=0)

    template_form = CreateImportTemplateForm(initial={'project': model_mixin.project})
    ingredient_formset = IngredientFormSet(instance=mapping)

    if request.method == "POST":
        template_form = CreateImportTemplateForm(request.POST)

        if template_form.is_valid():
            mapping_template = template_form.save(commit=False)
            ingredient_formset = IngredientFormSet(request.POST, instance=mapping)

            if ingredient_formset.is_valid():
                mapping_template.save()

                for form in ingredient_formset:
                    if form.cleaned_data and not form.cleaned_data['column_table_mapping'] in ['', 'Select a table mapping']:
                        csv_template_field = form.save()
                        csv_template_field.order = form.cleaned_data['order']
                        csv_template_field.is_aspects_field = form.cleaned_data['is_aspects_field']
                        csv_template_field.save()

                blank_fields = CSVFileImportFields.objects.filter(csv_import=mapping, column_pretty_name='')
                blank_fields.delete()

                new_fields_to_create = CSVFileImportFields.objects.filter(csv_import=mapping,
                                                                          column_field_mapping='New Field:',
                                                                          column_pretty_name__isnull=False)

                model_mixin.klass("Properties").add_new_plain_string_value("floor_plan_typical")
                for field in new_fields_to_create:
                    english_value = model_mixin.klass("Strings").clean_for_mapping(field.column_pretty_name)
                    model_mixin.klass("TextTableCategories").check_category_string(field.column_table_mapping,
                                                                                   english_value)
                    model_mixin.klass("Properties").add_new_string_value(field)

                    try:
                        field.column_field_mapping = english_value
                    except:
                        pass

                    field.save()

                mapping.copy_to_template_fields(mapping_template)
                mapping.copy_to_import_template_fields(mapping_template)

                blank_fields = ProjectCSVTemplateFields.objects.filter(template=mapping_template, column_pretty_name='')
                blank_fields.delete()

                import_templates = ProjectCSVTemplateImport.objects.filter(template=mapping_template).order_by("-pk")
                if import_templates:
                    messages.success(request, "Import of data saved successfully")
                    return redirect('/projects/%s/select/template/%s/preview/?preview_id=%s' % (model_mixin.project.pk,
                                                                                                mapping_template.pk,
                                                                                                import_templates[0].pk))

        if "submit" in request.POST and request.POST['submit']:
            submit_function = request.POST['submit']

        column_mapping_preview = {}
        for column in mapping.get_file_headers():
            if str(column) in request.POST and request.POST[column]:
                column_mapping_preview.update({column: {'name': column, 'field': request.POST[column]}})

        if submit_function == "Save":
            csv_file = open(mapping.csv_file.path, 'rU')
            csv_reader = csv.reader(csv_file)

            row_dict = {}
            for index, row in enumerate(csv_reader):
                if index == 0:
                    i = 1
                    for item in row:
                        row_dict.update({item: i})
                        i += 1

            csv_file = open(mapping.csv_file.path, 'rU')
            csv_reader = csv.reader(csv_file)

            for index, row in enumerate(csv_reader):
                if index == 0:
                    continue

    context = {
        'project': model_mixin.project,
        'mapping': mapping,
        'column_mapping_preview': column_mapping_preview,
        'submit_function': submit_function,
        'template_columns_form': ingredient_formset,
        'total_fields': total_fields,
        'template_form': template_form
    }

    return render(request, template, context)


@login_required
def edit_apartments_import_template(request, pk, template_id, apartment_id):
    template = "projects/dynamic_editing/editable_template.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    model_mixin.get_class_object(model_mixin.project.apartment_model)
    apartment = model_mixin.get_model_instance(apartment_id)

    all_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project, is_editable_template=True).order_by('id')
    editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
    total_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template)

    IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                              form=get_apartment_template_formset(model_mixin.project, model_mixin, apartment),
                                              extra=0)

    form = CreateImportTemplateForm(instance=editable_template)
    ingredient_formset = IngredientFormSet(instance=editable_template,
                                           queryset=editable_template.project_template_fields.order_by("order"))

    if request.method == "POST":

        ingredient_formset = IngredientFormSet(request.POST, instance=editable_template)

        if ingredient_formset.is_valid():
            for form in ingredient_formset:
                form.save_apartment_details(model_mixin, apartment)

            messages.success(request, "Apartment details saved successfully.")
            return redirect('projects-model-filter', model_mixin.project.pk, model_mixin.project.apartment_model)

    context = {
        'project': model_mixin.project,
        'template_form': form,
        'template_columns_form': ingredient_formset,
        'total_fields': len(total_fields),
        'apartment': apartment,
        'all_templates': all_templates,
        'template_id': int(template_id)
    }

    return render(request, template, context)


@login_required
def upload_model_data(request, pk, model_name):
    template = "projects/dynamic_editing/upload_model_data.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = get_model(project.project_connection_name, model_name)

    bulk_import = BulkImport.objects.create(project=project, table_name=model_name, status=BulkImport.STATUS_DRAFT)
    upload_form = BulkImportUploadForm(instance=bulk_import)

    if request.method == 'POST':
        if 'submit__upload' in request.POST:
            upload_form = BulkImportUploadForm(request.POST, request.FILES, instance=bulk_import)

            if upload_form.is_valid():
                bulk_import = upload_form.save(commit=False)
                bulk_import.status = BulkImport.STATUS_PROCESSED
                bulk_import.save()
                messages.success(request, "You excel file was imported successfully.")
                return redirect('projects-model-filter', project.pk, model_name)

    context = {
        'project': project,
        'model': project_model_object,
        'model_name': model_name,
        'upload_form': upload_form
    }

    return render(request, template, context)




@login_required
def upload_model_data_complete(request, pk, bulk_import_id):
    template = "projects/dynamic_editing/upload_model_data_complete.html"

    project = Project.objects.get(pk=pk)
    bulk_import = BulkImport.objects.get(pk=bulk_import_id)

    context = {
        'project': project,
        'bulk_import': bulk_import
    }

    return render(request, template, context)