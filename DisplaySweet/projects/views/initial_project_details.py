import os
import csv
from django.core.files import File
import subprocess
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.apps import apps
from django.conf import settings
from django.http import HttpResponse
from django.db.models.fields.related import ManyToOneRel
from django.forms.models import inlineformset_factory

from core.generic_views import ListView, ProjectListCallbackView, DetailView, VersionView, UpdateView, CreateView
from core.models import Project, ProjectCSVTemplate, ProjectCSVTemplateFields, \
    ProjectVersion, ProjectConnection, ProjectFamily, CSVFileImport
from projects.permissions import CreatePermissionMixin, ProjectModelMixin
from projects.forms import ProjectAddForm, ProjectImagePathForm, ProjectVersionCommentForm, ProjectScriptsForm
from users.models import User
import shutil



from projects.models.model_mixin import PrimaryModelMixin
from utils.revision_helpers import add_new_revision
from projects.project_script_load import PixileScriptDbUpdater
from projects.cms_sync import ProjectCMSSync


class ProjectListCallbackView(ProjectListCallbackView):
    model = Project
    max_display_length = 100

    # Label, selector, ordering, hidden by default
    column_details = (
         ('ID', 'id', 'id', False),
         ('Name', 'name', 'name', False),
         ('Active', 'active', 'active', False),
         ('Database Connection', 'database_connection', 'database_connection', False),
         ('Actions', '_actions', '', False),
    )


class ProjectListView(CreatePermissionMixin, ListView):
    template_name = "projects/list.html"
    callback_view_class = ProjectListCallbackView

    @property
    def actions(self):
        user = self.request.user

        actions = []
        if self.can_create_project(user):
            actions.append(('Add a Project', reverse('projects-create')))

        return actions

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)
        context['project_list'] = Project.objects.filter(name='Capitol')
        return context


@login_required
def project_list_view(request):
    template = "projects/project_list.html"

    projects = Project.objects.filter(users__pk__contains=request.user.pk,
                                      active=True).exclude(name="Master").distinct().order_by('name', 'modified')

    if not request.user.is_ds_admin:
        projects = projects.filter(status=Project.LIVE)

    actions = []

    project_mixin = ProjectModelMixin()
    if request.user.can_create_project():
        actions.append(('Add a Project', reverse('projects-create')))

    context = {
        'projects': projects,
        'actions': actions,
        'project_mixin': project_mixin
    }

    return render(request, template, context)


class ProjectUpdateView(UpdateView):
    template_name = "projects/update.html"
    model = Project
    context_object_name = "project"

    fields = (
        'name',
        'active',
    )


class ProjectCreateView(CreateView):
    template_name = "projects/create.html"
    model = Project
    form_class = ProjectAddForm
    context_object_name = "project"
    success_url = reverse_lazy('projects-list')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        project = form.save(commit=True)

        project.users.add(self.request.user)
        project.active = True
        project.save()

        new_version = ProjectVersion(active_version=1.0, project=project, project_name=project.name)
        new_version.save()

        project.version_family = new_version
        project.save()

        project_connection = ProjectConnection.objects.all()
        if project_connection:
            project_connection = project_connection[0]
            project_connection.map_database_connection()

        add_new_revision(self.request, Project, "Project created %s " % project.name)

        messages.success(self.request, "Project successfully created")
        return redirect('projects-list')


@login_required
def force_restart(request):

    try:
        print ('trying to run project restart: ')
        restart_file = os.path.join(settings.PROJECT_ROOT, "restartApache.sh")
        command_args = './%s' % restart_file
        popen = subprocess.Popen(command_args, bufsize=4096,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                 shell=True)

        popen.wait()
        print ('completed project restart: ')
    except Exception as e:
        print ('failed to run restart sub process: ', e)

    messages.success(request, "New Project created successfully")
    return redirect('projects-list')


@login_required
def ProjectDeleteView(request, pk):

    project = Project.objects.get(pk=pk)

    add_new_revision(project, request, Project, "Project deleted %s " % project.name)

    try:
        project.delete()
    except:
        pass

    project_connection = ProjectConnection.objects.all()
    if project_connection:
        project_connection[0].map_database_connection()

    return redirect('projects-list')


class ProjectDetailView(DetailView):
    template_name = "projects/detail.html"
    model = Project
    context_object_name = "project"

    form = ''

    def get(self, request, pk):

        project = self.get_object()
        request.session['project_id'] = project.pk

        if "export_format" in request.GET and request.GET['export_format'] == "csv":
            filename = 'project_user_details.csv'
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename

            writer = csv.writer(response)
            users = project.users.all().order_by('first_name')

            header_fields = ['Project', 'User', 'Username', 'Password']

            writer.writerow(header_fields)
            for user in users:
                data_list = [project.name, user.get_full_name(), user.username, user.temporary_setup_password]

                writer.writerow(data_list)

            return response

        return render(request, self.template_name, {'actions': self.actions, 'project': project })

    @property
    def models(self):
        project = self.get_object()
        myapp = apps.get_app_config(project.project_connection_name)
        return myapp.models.values()

    @property
    def actions(self):
        project = self.get_object()
        user = self.request.user
        actions = []

        model_mixin = PrimaryModelMixin(request=self.request, project_id=project.pk)

        self.request.session['project_id'] = project.pk

        model_mixin.check_and_update_project_templates(project)
        resources = model_mixin.model_object("Resources").all()

        update_import_templates = False

        for resource in resources:
            if "/mnt/twenty/cms/static/" in resource.path:
                resource.path = resource.path.replace("/mnt/twenty/cms/static/", "/mnt/twenty/static/")
                resource.save(using=project.project_connection_name)

            update_templates = True
            if "/mnt" in resource.path:
                split_name = resource.path.split("/")

                for sname in split_name:
                    if sname == project.project_connection_name:
                        update_templates = False
                        update_import_templates = True

                if update_templates:
                    image_name = split_name[-1]
                    connection_to_change = split_name[int(len(split_name) - 2)]

                    if not connection_to_change in ['mnt', 'static', 'cms', 'media'] and not connection_to_change == image_name:
                        split_name[int(len(split_name) - 2)] = project.project_connection_name
                        new_name = '/'.join(split_name)
                        resource.path = new_name

                        resource.save(using=project.project_connection_name)

        if update_import_templates:
            current_templates = ProjectCSVTemplate.objects.filter(project=project)
            if len(current_templates) == 0:
                templates = ProjectCSVTemplate.objects.filter(project=project.project_copied_from)

                for template in templates:
                    fields = ProjectCSVTemplateFields.objects.filter(template=template)

                    if fields:
                        new_template = template
                        new_template.id = None
                        new_template.project = project
                        new_template.save()

                        for field in fields:
                            new_field = field
                            new_field.id = None
                            new_field.template = new_template
                            new_field.save()

        if user.can_view_allocation_admin():
            actions.append(('View Allocations', 'project-allocations'))

        if user.can_edit_allocation_admin():
            actions.append(('Project Administration', 'projects-administration'))

        if self.request.user.is_ds_admin:
            actions.append(('View All Models (tables)', 'projects-model-list'))
            actions.append(('Import Templates', 'projects-select-template'))
            actions.append(('Download Database', 'projects-download-sql'))

        return self.reverse_action_urls(actions)


class ProjectModelDetailView(DetailView):
    template_name = "projects/dynamic_editing/model_list.html"
    model = Project
    context_object_name = "project"

    @property
    def models(self):
        project = self.get_object()
        myapp = apps.get_app_config(project.project_connection_name)
        return myapp.models.values()

    @property
    def actions(self):
        project = self.get_object()
        user = self.request.user

        actions = []

        if project.can_be_edited_by(user) and project.has_building_model():
            actions.append(('Edit Buildings', 'projects-building-list'))

        if project.can_be_edited_by(user) and project.has_floors_model():
            actions.append(('Edit Floors', 'projects-floors-list'))

        if project.can_be_edited_by(user):
            actions.append(('Import Data', 'projects-update'))

        return self.reverse_action_urls(actions)




class ProjectSettingsView(DetailView):
    template_name = "projects/settings.html"
    model = Project
    context_object_name = "project"

    @property
    def project_list(self):
        project_list = Project.objects.all()

        return project_list

    @property
    def version_options(self):
        version_options = []
        for i in range(1, 100):
            version_options.append(i)

        return version_options

@login_required
def save_project_database(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    if request.FILES:
        try:
            project.over_write_new_database_file(request.FILES)
            messages.success(request, "Database has been overwritten successfully")

            add_new_revision(project, request, Project,
                             "Database for %s have been overwritten successfully " % project.name)
        except Exception as e:
            messages.error(request, e)

        return redirect('projects-view', project.pk)

    return redirect('projects-view', project.pk)


@login_required
def project_user_list(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    template = "projects/users.html"

    user_types = User.USER_TYPE_CHOICES

    actions = []
    if project.can_be_edited_by(request.user):
        actions.append(('Allocations', 'project-allocations'))
        actions.append(('Project Administration', 'projects-administration'))
        actions.append(('View Project Details', 'projects-view'))

    actions = model_mixin.get_reversed_urls_from_actions(actions, pk)

    context = {
        'project': project,
        'actions': actions,
        'user_types': user_types
    }

    return render(request, template, context)


@login_required
def download_sql(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    for connection in project.project_connections.all():
        database_file = connection.save_new_database_file()

        if database_file:
            add_new_revision(project, request, Project,
                             "Database for %s have been downloaded successfully " % project.name)

            file_name = str(database_file.name)
            response = HttpResponse(database_file, content_type='file')
            response['Content-Disposition'] = 'attachment; filename=%s' % str(file_name)

            return response
        else:
            messages.error(request, "Something went wrong downloading the db file")
            return redirect("/projects/%s/" % project.pk)


def return_position_page(page, total):
    m = 1
    try:
        if type(int(page)) == int:
            for i in range(1, int(page)):
                m += int(total)
    except:
        pass

    return m


def project_actions(request, project, model_name):
    user = request.user

    actions = []

    if model_name == "buildings":
        if project.can_be_edited_by(user):
            actions.append(('Edit Floors', 'projects-floors-list'))

        if project.can_be_edited_by(user) and project.has_building_model():
            actions.append(('Edit Building Hotspots', 'projects-update'))

    return actions


def save_form_data(project_model_object, cleaned_data, model_fields, project, model_instance=None):

    if model_instance:
        for field in model_fields:
            cleaned_value = cleaned_data.get(field, None)
            setattr(model_instance, field, cleaned_value)

        model_instance.save(using=project.project_connection_name)

    return model_instance


def filter_model_fields_for_form(model_fields, drop_id=False):

    field_names = []

    for field in model_fields:
        if drop_id:
            #not type(field) == ForeignKey and
            if not field.name == "id" and not type(field) == ManyToOneRel:
                field_names.append(field.name)
        else:
            if not type(field) == ManyToOneRel: # and not type(field) == ForeignKey:
                field_names.append(field.name)

    return field_names