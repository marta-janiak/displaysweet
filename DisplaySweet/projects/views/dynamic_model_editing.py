import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.conf import settings
from django.forms.models import model_to_dict
from django.db.models.loading import get_model
from django.core import serializers

from projects.permissions import ProjectsPermissionsMixin, ProjectModelMixin
from projects.forms import get_object_filter_form, apply_form_filter, get_formfield_form

from utils.xlxs_generator import BulkImportTemplate

from .initial_project_details import save_form_data, filter_model_fields_for_form
from core.models import Project, DisplaySweetConfiguration
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def delete_model_row_instant(request, pk, model_name, row_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    model_instance.delete(using=project.project_connection_name)
    messages.success(request, "Data deleted successfully")
    return redirect('projects-model-filter', project.pk, model_name)


@login_required
def project_model(request, pk, model_name):
    template = "projects/dynamic_editing/project_model.html"

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    field_names = project_model_object._meta.get_all_field_names()
    table_values = project_model_object.objects.using(project.project_connection_name).all()

    project_permissions = ProjectsPermissionsMixin()

    actions = []

    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))
        actions.append(('Filter all data', reverse('projects-model-filter', args=[project.pk, model_name])))
        actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'actions': actions
    }

    return render(request, template, context)


@login_required
def update_apartment_types_floor_plan_image(request, pk, apartment_type_id):
    template = 'projects/dynamic_editing/floor_plan_details/apartment_types_floor_plans.html'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = model_mixin.get_class_object('Apartmenttypes')
    model_instance = model_mixin.get_model_instance(apartment_type_id)

    model_mixin.get_class_object(project.images_model)
    images_model_instance = model_mixin.get_model_instance(model_instance.name, field='description')

    image_field = None
    image_exists = None
    if request.GET and 'image' in request.GET:
        image_selected = request.GET['image']

        try:
            image_field = getattr(images_model_instance, image_selected)
        except:
            pass

        if image_field:
            image_exists = os.path.isfile(image_field)

    context = {
        'project': project,
        'model': project_model_object,
        'row_id': apartment_type_id,
        'instance': model_instance,
        'images_model_instance': images_model_instance,
        'image_field': image_field,
        'image_exists': image_exists
    }

    return render(request, template, context)


@login_required
def update_apartment_save_view(request, pk, model_name, row_id, apartment_id):

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [int(x) for x in apartment_ids_list]
    apartment_ids_list.append(apartment_id)
    apartment_ids_list = [str(x) for x in apartment_ids_list]

    apartment_ids_string = ','.join(apartment_ids_list)
    model_instance.apartment_ids = apartment_ids_string
    model_instance.save(using=project.project_connection_name)
    messages.success(request, "Apartment views updated successfully")

    return redirect('projects-apartment-views-update', project.pk, model_name, row_id)


@login_required
def update_apartment_remove_view(request, pk, model_name, row_id, apartment_id):

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [int(x) for x in apartment_ids_list]

    if int(apartment_id) in apartment_ids_list:
        apartment_ids_list.remove(int(apartment_id))
        apartment_ids_list = [str(x) for x in apartment_ids_list]

        apartment_ids_string = ','.join(apartment_ids_list)
        model_instance.apartment_ids = apartment_ids_string
        model_instance.save(using=project.project_connection_name)

        messages.success(request, "Apartment views updated successfully")
    else:
        messages.error(request, "Apartment views is not a part of the Assigned Views")

    return redirect('projects-apartment-views-update', project.pk, model_name, row_id)


@login_required
def model_filter_view(request, pk, model_name, extra_context=None):
    template = "projects/dynamic_editing/project_model_filter.html"

    project = Project.objects.get(pk=pk)

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    model_field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, model_field_names, project_model_object, project.project_connection_name)

    has_hot_spot_configuration = False
    if "picking_coord_x" in model_fields and "picking_coord_y" in model_fields:
        has_hot_spot_configuration = True

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []


    field_names = model_field_names
    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":
        if "sort_by" in request.GET and request.GET['sort_by']:
            sorting_field = request.GET['sort_by']

        if "sorting_direction" in request.GET and request.GET['sorting_direction']:
            sorting_direction = request.GET['sorting_direction']

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):
        if data and 'id' in model_field_names:
            actions.append(('Download Filtered List', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))

    paginate_by = 50
    position = 0
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']
        position = return_position_page(page, paginate_by)

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    page_list = []
    for i in range(1, int(pages + 1)):
        page_list.append(int(i))

    next_page = int(page) + 1
    previous_page = int(page) - 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'position': position,
        'page': int(page),
        'pages': int(pages),
        'page_list': page_list,
        'next_page': next_page,
        'previous_page': previous_page,
        'data': data[position:(int(paginate_by) * int(page))],
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'queries_encoded': str(queries.urlencode()),
        'actions': actions,
        'applied_filters': applied_filters,
        'has_hot_spot_configuration': has_hot_spot_configuration,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction),
        'page_template': 'projects/dynamic_editing/includes/project_model_filter_pagination.html'
    }

    if extra_context is not None:
        context.update(extra_context)

    return render(request, template, context)


@login_required
def add_model_row(request, pk, model_name):
    print('add_model_row')
    template = "projects/dynamic_editing/add_model_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = model_mixin.get_class_object(model_name)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)

    form_class = get_formfield_form(None, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            model_kwargs = {}
            for field in model_fields:
                field_value = form.cleaned_data.get(field, None)
                if not field_value:
                    field_value = 0

                model_kwargs.update({
                    '{0}'.format(field): field_value,
                })

            model_items = project_model_object.objects.using(project.project_connection_name).filter(**model_kwargs)
            if not model_items:
                project_model_object.objects.using(project.project_connection_name).create(**model_kwargs)
                messages.success(request, "Data saved successfully")

            return redirect('projects-model-filter', project.pk, model_name)
    context = {
        'project': project,
        'model': project_model_object,
        'form': form
    }

    return render(request, template, context)


@login_required
def update_model_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_model_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    if model_name.lower() == 'floors':
        return redirect('projects-floors-update', project.pk, model_name, row_id)
    elif model_name.lower() == 'apartmenthotspots':
        return redirect('projects-hotspot-update', project.pk, model_name, row_id)
    elif model_name.lower() == 'apartmentviews':
        return redirect('projects-apartment-views-update', project.pk, model_name, row_id)

    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)

    has_hot_spot_configuration = False
    if "picking_coord_x" in model_fields and "picking_coord_y" in model_fields:
        has_hot_spot_configuration = True

    # Getting floor plan image if exists
    has_floor_plan_image_id = False
    image_field = None
    image_exists = None

    if "floor_plan_image_id" in model_fields:
        has_floor_plan_image_id = True

        model_mixin.get_class_object(project.images_model)
        images_model_instance = model_mixin.get_model_instance(model_instance.floor_plan_image_id)
        image_field = images_model_instance.highres_path
        if image_field:
            if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_field)):
                image_exists = True

    form_class = get_formfield_form(model_instance, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            save_form_data(project_model_object, form.cleaned_data, model_fields, project,
                           model_instance=model_instance)
            messages.success(request, "Data saved successfully")
            return redirect('projects-model-filter', project.pk, model_name)

    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

        if has_hot_spot_configuration:
            actions.append(('Update Hotspot configuration',
                            reverse('projects-hotspot-configuration', args=[project.pk, model_name, row_id])))
        if has_floor_plan_image_id:
            actions.append(('Update Floor Plan Image',
                            reverse('projects-hotspot-configuration', args=[project.pk, model_name, row_id])))

        if model_name.lower() == 'apartmenttypes':
            actions.append(
                ('Edit Floor Plan Images', reverse('projects-apartment-types-floor-plan', args=[project.pk, row_id])))

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'has_hot_spot_configuration': has_hot_spot_configuration,
        'has_floor_plan_image_id': has_floor_plan_image_id,
        'image_exists': image_exists,
        'image_field': image_field,
        'row_id': row_id
    }

    return render(request, template, context)


@login_required
def delete_model_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/delete_model_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    if request.method == "POST":
        model_instance.delete(using=project.project_connection_name)
        messages.success(request, "Data deleted successfully")
        return redirect('projects-model-filter', project.pk, model_name)

    context = {
        'project': project,
        'model': project_model_object,
        'instance': model_instance,
        'actions': project_actions(request, project, model_name),
        'model_name': model_name
    }

    return render(request, template, context)


@login_required
def update_floors_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_floors_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)

    configuration = DisplaySweetConfiguration.objects.all()
    if configuration:
        configuration = configuration[0]

    has_floor_plan_image = False
    image_path = None
    image_exists = None
    if not project.has_images_schema():
        model_dict = model_to_dict(model_instance)
        if "floor_plate_image_id" in model_dict:
            has_floor_plan_image = project.has_floor_plan_image(model_instance.floor_plate_image_id)

            model_mixin.get_class_object(project.images_model)
            images_model_instance = model_mixin.get_model_instance(model_instance.floor_plate_image_id)
            image_path = str(images_model_instance.highres_path)

            image_exists = None
            if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
                image_exists = True
    else:

        model_mixin.get_class_object('FloorViewImages')
        images_model_instance = model_mixin.get_model_queryset()

    form_class = get_formfield_form(model_instance, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            save_form_data(project_model_object, form.cleaned_data, model_fields, project, model_instance=model_instance)
            messages.success(request, "Data saved successfully")
            return redirect('projects-model-filter', project.pk, model_name)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

        actions.append(('Add / Edit Apartments', "%s?floor_id=%s" % (reverse('projects-model-filter', args=[project.pk, 'Apartments']), row_id)))
        actions.append(('Update Floor Plan Image', reverse('projects-floors-add-floor-plan', args=[project.pk, row_id])))

        if project.has_hotspot_model():
            actions.append(('Add / Edit Hotspots', reverse('projects-model-filter', args=[project.pk, 'ApartmentHotspots'])))
        else:
            actions.append(('Edit Floor Hotspots', reverse('projects-floors-hotspots', args=[project.pk, model_instance.id])))

        if project.has_views_model():
            actions.append(('Assign Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))
            actions.append(('Add / Edit Apartment Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'configuration': configuration,
        'has_floor_plan_image': has_floor_plan_image,
        'image_path': image_path,
        'image_exists': image_exists
    }

    return render(request, template, context)


@login_required
def update_apartment_views_custom_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_views_row.html"

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [ int(x) for x in apartment_ids_list]

    apartment_object = get_model(project.project_connection_name, 'Apartments')

    assigned_instances = apartment_object.objects.using(project.project_connection_name).filter(id__in=apartment_ids_list)
    apartment_instances = apartment_object.objects.using(project.project_connection_name).all().exclude(id__in=assigned_instances)

    images_object = get_model(project.project_connection_name, project.images_model)
    image_instance = images_object.objects.using(project.project_connection_name).get(pk=model_instance.resource_id)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

    context = {
        'project': project,
        'model': project_model_object,
        'image_instance': image_instance,
        'apartment_instances': apartment_instances,
        'assigned_instances': assigned_instances,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'row_id': row_id
    }

    return render(request, template, context)


@login_required
def project_floors(request, pk):
    template = "projects/dynamic_editing/project_model_filter.html"

    model_name = 'floors'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = model_mixin.get_class_object("Floors")
    all_floors = model_mixin.get_model_queryset(list_needed=True)

    first_floor = None
    if all_floors:
        first_floor = all_floors[0]

    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, field_names, project_model_object, project.project_connection_name)

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []

    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":
        if "sort_by" in request.GET and request.GET['sort_by']:
            sorting_field = request.GET['sort_by']

        if "sorting_direction" in request.GET and request.GET['sorting_direction']:
            sorting_direction = request.GET['sorting_direction']

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):

        if data:
            actions.append(('Download For Bulk Edit', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add Floor', reverse('projects-model-insert', args=[project.pk, model_name])))

        if first_floor:
            actions.append(('Update Hotspots', reverse('projects-floors-hotspots', args=[project.pk, first_floor.id])))
            actions.append(('Assign Views', reverse('projects-floors-views', args=[project.pk, first_floor.id])))
    paginate_by = 50
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'page': int(page),
        'pages': int(pages),
        'data': data,
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'actions': actions,
        'applied_filters': applied_filters,
        'has_hot_spot_configuration': True,
        'floors': True,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction)
    }

    return render(request, template, context)


@login_required
def project_buildings(request, pk):
    template = "projects/dynamic_editing/project_model_filter.html"

    model_name = 'buildings'

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, field_names, project_model_object, project.project_connection_name)

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []

    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):

        if data:
            actions.append(('Download For Bulk Edit', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))

    paginate_by = 50
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'data': data,
        'page': int(page),
        'pages': int(pages),
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'actions': actions,
        'applied_filters': applied_filters,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction)

    }

    return render(request, template, context)



def project_actions(request, project, model_name):
    user = request.user

    actions = []

    if model_name == "buildings":
        if project.can_be_edited_by(user):
            actions.append(('Edit Floors', 'projects-floors-list'))

        if project.can_be_edited_by(user) and project.has_building_model():
            actions.append(('Edit Building Hotspots', 'projects-update'))

    return actions


def return_position_page(page, total):
    m = 1
    try:
        if type(int(page)) == int:
            for i in range(1, int(page)):
                m += int(total)
    except:
        pass

    return m