import json
import csv
import os
import string

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.apps import apps
from django.forms.models import model_to_dict
from django.conf import settings
from django.db.models.loading import get_model
from django.core import serializers
from django.http import HttpResponse
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.core.files import File

from core.generic_views import ListView, ProjectListCallbackView, DetailView, VersionView, UpdateView, DeleteView, CreateView
from core.models import Project, CSVFileImport, BulkImport, DisplaySweetConfiguration, ProjectImagePath
from projects.permissions import CreatePermissionMixin, ProjectsPermissionsMixin, ProjectModelMixin
from projects.forms import ProjectAddForm, get_object_filter_form, apply_form_filter, CSVFileImportAddForm, \
                   BulkImportUploadForm, get_formfield_form, get_object_add_floor_plan_form, CSVFileImportAllTablesForm, \
                   CreateProjectDetailsForm

from projects.models.model_mixin import PrimaryModelMixin
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def add_floor_plan_image(request, pk, floor_id):
    template = "projects/dynamic_editing/floor_plan_details/add_floor_plan_image.html"

    model_name = 'Floors'

    configuration = DisplaySweetConfiguration.objects.all()
    if configuration:
        configuration = configuration[0]

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    has_floor_plan_image = False
    
    project_model_object = model_mixin.get_class_object(model_name)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)
    model_instance = model_mixin.get_model_instance(floor_id)

    model_mixin.get_class_object('Resources')
    images_model_instance = model_mixin.get_model_instance(model_instance.floor_plate_image_id)
    image_path = str(images_model_instance.highres_path)

    form_class = get_object_add_floor_plan_form(project, model_fields, project_model_object, model_instance,
                                                configuration, image_path)

    form = form_class(request.POST or None, request.FILES or None)
    model_dict = model_to_dict(model_instance)
    if "floor_plate_image_id" in model_dict:
        has_floor_plan_image = project.has_floor_plan_image(model_instance.floor_plate_image_id)

    if request.method == "POST":
        if form.is_valid():
            image_saved = form.save()
            if image_saved:
                messages.success(request, "Floor plan image saved successfully")
                return redirect('projects-floors-update', project.pk, model_name, floor_id)
            else:
                messages.error(request, "Floor plan image could not be saved.")

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'model_name': model_name,
        'configuration': configuration,
        'has_floor_plan_image': has_floor_plan_image,
        'image_path': image_path
    }

    return render(request, template, context)


@login_required
def add_floor_plan_image_apartment(request, pk, apartment_id):
    template = "projects/dynamic_editing/floor_plan_details/add_floor_plan_image.html"

    model_name = 'Apartments'

    configuration = DisplaySweetConfiguration.objects.all()
    if configuration:
        configuration = configuration[0]

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    project_model_object = model_mixin.get_class_object(model_name)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)
    model_instance = model_mixin.get_model_instance(apartment_id)

    model_mixin.get_class_object('Resources')
    images_model_instance = model_mixin.get_model_instance(model_instance.floor_plan_image_id)
    image_path = str(images_model_instance.highres_path)

    form_class = get_object_add_floor_plan_form(project, model_fields, project_model_object, model_instance,
                                                configuration, image_path)

    form = form_class(request.POST or None, request.FILES or None)

    has_floor_plan_image = project.has_floor_plan_image(model_instance.floor_plan_image_id)

    if request.method == "POST":
        if form.is_valid():
            image_saved = form.save()
            if image_saved:
                messages.success(request, "Floor plan image saved successfully")
                return redirect('projects-model-update', project.pk, model_name, apartment_id)
            else:
                messages.error(request, "Floor plan image could not be saved.")

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'model_name': model_name,
        'configuration': configuration,
        'has_floor_plan_image': has_floor_plan_image,
        'image_path': image_path
    }

    return render(request, template, context)


@login_required
def project_floors_views(request, pk, floor_id):
    template = "projects/dynamic_editing/floor_plan_details/all_floors_views.html"

    model_name = 'floors'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    project_model_object = model_mixin.get_class_object(model_name)    
    model_instance = model_mixin.get_model_instance(floor_id)
    all_floors = model_mixin.get_model_queryset()

    model_mixin.get_class_object('Resources')
    images_model_instance = model_mixin.get_model_instance(model_instance.floor_plate_image_id)
    image_path = str(images_model_instance.highres_path)

    model_mixin.get_class_object(project.apartment_model)
    apartments = model_mixin.get_model_queryset('floor_id', floor_id)

    apartment_list = []
    for apartment in apartments:
        apartment_list.append({'id': apartment.name,
                               'x_coordinate': apartment.picking_coord_x,
                               'y_coordinate': apartment.picking_coord_y
                              })

    apartment_list = json.dumps(apartment_list)

    image_exists = None
    if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
        image_exists = True

    queries = request.GET.copy()
    context = {
        'project': project,
        'model': project_model_object,
        'model_instance': model_instance,
        'model_name': model_name,
        'has_hot_spot_configuration': True,
        'floors': True,
        'image_path': image_path,
        'image_exists': image_exists,
        'queries': queries,
        'apartments': apartments,
        'apartment_list': apartment_list,
        'all_floors': all_floors
    }

    return render(request, template, context)


@login_required
def floor_plan_editor(request, pk, floor_id=None):
    template = "projects/floor_plan_editor/editor.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    if project.has_apartment_string_fields():
        return redirect('floor-plan-editor-new-schema', project.pk)

    floor_list = []
    image_path = ''
    image_exists = False
    all_floor_images = []
    buildings = 0

    first_floor = None
    floors = model_mixin.get_model_object_and_row('Floors')
    if len(floors)> 1:
        first_floor = floors[0]
        floor_id = first_floor.id

    if project.has_building_model():
        model_mixin.get_class_object('Buildings')
        buildings = len(model_mixin.get_model_queryset(list_needed=True))

    apartments_dict = []
    if first_floor:
        model_mixin.get_class_object(project.apartment_model)
        apartments = model_mixin.get_model_queryset(field='floor_id', value=first_floor.pk, list_needed=True)

        apartment_list = []

        for apartment in apartments:
            resource_id = apartment.floor_plan_image_id

            image = model_mixin.model_object('Images').filter(pk=resource_id)

            if image:
                image = image[0]
                high_res_path = image.highres_path
                thumbnail_path = image.thumbnail_path
            else:
                high_res_path = ''
                thumbnail_path = ''

            apartment_list.append({'id': apartment.name,
                                   'x_coordinate': apartment.picking_coord_x,
                                   'y_coordinate': apartment.picking_coord_y,
                                   'floor_image': high_res_path,
                                   'key_image': thumbnail_path,
                                   'name': apartment.name
                                  })

            apartments_dict.append({'id': apartment.id,
                                    'x_coordinate': apartment.picking_coord_x,
                                    'y_coordinate': apartment.picking_coord_y,
                                    'floor_image': high_res_path,
                                    'floor_image_name': high_res_path.split("/")[-1],
                                    'key_image': thumbnail_path,
                                    'key_image_name': thumbnail_path.split("/")[-1],
                                    'name': apartment.name
                                  })

        apartment_list = json.dumps(apartment_list)

        first_apartment_id = None
        y_coordinate = None
        x_coordinate = None

        if apartments:
            first_apartment_id = apartments[0].pk
            x_coordinate = apartments[0].picking_coord_x
            y_coordinate = apartments[0].picking_coord_y

        model_mixin.get_class_object('Resources')
        images_model_instance = model_mixin.get_model_instance(first_floor.floor_plate_image_id)
        image_path = str(images_model_instance.highres_path)

        image_exists = None
        if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
            image_exists = True

        floor_list = []
        all_floors = model_mixin.klass("Floors").get_all_floors()
        for floor in all_floors:
            try:
                apartment_count = len(model_mixin.get_model_queryset('floor_id', floor.id))
            except:
                apartment_count = 0

            floor_list.append(
                {'floor': floor, 'apartment_count': apartment_count }
            )

        all_floor_images = model_mixin.klass("Resources").get_all_plan_images(floor_id)
    else:
        apartments = []
        apartment_list = []
        all_floors = []
        first_apartment_id = None
        x_coordinate = 0
        y_coordinate = 0

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user) and floor_id:
        # actions.append(('Edit Project Details', reverse('projects-update', args=[project.pk])))
        actions.append(('Edit Floor Hotspots', reverse('projects-floors-hotspots', args=[project.pk, floor_id])))

    context = {
        'project': project,
        'all_floors': all_floors,
        'floor_list': floor_list,
        'model_instance': first_floor,
        'apartments': apartments,
        'total_apartments': len(apartments),
        'apartment_list':apartment_list,
        'apartments_dict': apartments_dict,
        'floor_id': floor_id,
        'image_path': image_path,
        'image_exists': image_exists,
        'canvas_width': '600px',
        'actions': actions,
        'all_floor_images': all_floor_images,
        'existing_level_images': model_mixin.klass("Resources").get_project_image_directory(image_type='level'),
        'existing_level_full_res_images': model_mixin.klass("Resources").get_project_image_directory(image_type='level_full_res'),
        'existing_floor_images': model_mixin.klass("Resources").get_project_image_directory(image_type='floor'),
        'existing_key_images': model_mixin.klass("Resources").get_project_image_directory(image_type='key'),
        'first_apartment_id': first_apartment_id,
        'x_coordinate': x_coordinate,
        'y_coordinate': y_coordinate,
        'buildings': buildings
    }

    return render(request, template, context)


def new_project_plan_editor(request, pk):
    template = "projects/floor_plan_editor/quick_add_floors.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    model_mixin.get_class_object("Floors")

    form = CreateProjectDetailsForm()
    if request.method == "POST":
        form = CreateProjectDetailsForm(request.POST)
    if form.is_valid():
        form.save(project)
        return redirect('floor-plan-editor', pk)

    context = {
        'project': project,
        'form': form
    }

    return render(request, template, context)


def floor_plan_typicals(request, pk, floor_id):
    template = "projects/floor_plan_editor/typicals.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Plans Preview Editor', reverse('floor-plan-editor', args=[pk])))


    context = {
        'project': project,
        'actions': actions
    }

    return render(request, template, context)


@login_required
def floor_plan_editor_new_schema(request, pk, floor_id=None):
    template = "projects/floor_plan_editor/new_schema_editor.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    asset_list = []
    asset_list_ids = []

    project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, project.project_connection_name)
    images = model_mixin.model_object("Resources").all()

    image_list = []
    for image in images:
        if image.pk not in asset_list_ids:
            tags = model_mixin.klass("Assets").get_image_asset_tags(image)
            image_list.append({'image': image, 'tags': tags})

    tag_selected = "level"
    context = model_mixin.klass("LevelPlans").get_level_plan_details()

    context.update({'tag_selected': tag_selected.replace("_", " "),
                    'asset_list': asset_list,
                    'asset_list_ids': asset_list_ids,
                    'image_list': image_list,
                    'project': project,
                    'model_mixin': model_mixin,
                    'tag_names': model_mixin.model_object("Assets").all().order_by('tag').exclude(tag=''),
                    'static_image_folder': '/static/%s/' % project.project_connection_name,
                    'project_image_folder': project_image_folder
                    })

    return render(request, template, context)