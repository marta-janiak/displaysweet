import csv
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse

from projects.permissions import ProjectModelMixin
from projects.forms import get_allocation_group_form, get_search_filter_form
from users.forms import UserCreateFromAllocationForm
from projects.cms_sync import ProjectCMSSync
from django.db.models import Q

from core.models import ProjectCSVTemplate, ProjectCSVTemplateFields
from users.models import User
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def project_allocations(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    template = "projects/admin/allocations.html"
    user_types = User.USER_TYPE_CHOICES
    actions = []

    if not request.user.is_allocations_only_user:
        return redirect("projects-administration", project.pk)

    if project.can_be_edited_by(request.user):
        actions.append(('Edit Project Users', 'projects-user-list'))
        actions.append(('Edit Project Details', 'projects-update'))
        actions.append(('View Project Details', 'projects-view'))

    actions = model_mixin.get_reversed_urls_from_actions(actions, pk)

    user_allocations = model_mixin.model_object("AllocatedUsers").filter(user_id=request.user.pk).values_list('allocation_group_id', flat=True)

    if request.user.is_client_admin or request.user.is_master_agent:
        allocation_groups = model_mixin.model_object("AllocationGroups").filter(Q(pk__in=user_allocations) | Q(owner_id=request.user.pk)).order_by('name')
    else:
        allocation_groups = model_mixin.model_object("AllocationGroups").filter(pk__in=user_allocations).order_by('name')
    allocation_object = model_mixin.get_class_object("AllocationGroups")

    filter_form = get_search_filter_form(request.GET, model_mixin, [])
    form_class = get_allocation_group_form(request, allocation_object, allocation_groups, model_mixin)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        user_add_form = UserCreateFromAllocationForm(request, project, request.POST)
        if user_add_form.is_valid():
            user = user_add_form.save()
            user.created_by = request.user
            user.owned_by = request.user
            user.last_modified_by = request.user

            project.users.add(user)
            project.save()

            new_sync = ProjectCMSSync()
            try:
                new_sync.add_user_into_sync(project.pk, user.first_name, user.last_name, user.email, user.is_super_admin)
            except Exception as e:
                print(e)

            try:
                new_sync.add_user_access_into_sync(project.pk, project.name, user.email, user.can_edit_reservation(), user.can_edit_reservation())
            except Exception as e:
                print(e)
        else:
            email_address = user_add_form.cleaned_data.get('email', None)
            if email_address:
                print ('email_address', email_address)
                pass

        if request.FILES:
            project.over_write_new_database_file(request.FILES)
            messages.success(request, "Database has been overwritten successfully")
            return redirect('projects-administration', project.pk)
        else:
            if form.is_valid():
                form.save()
                messages.success(request, "Allocation group added successfully")
                return redirect('projects-administration', project.pk)

    allocation_template = ProjectCSVTemplate.objects.filter(project=project, is_allocation_template=True)
    if not allocation_template:
        messages.error(request, "You have not been set an allocation template for this project.")
        return redirect('projects-select-template', project.pk)

    allocation_template = allocation_template[0]
    template_fields = ProjectCSVTemplateFields.objects.filter(template=allocation_template).order_by('order')

    property_allocations = model_mixin.model_object("PropertyAllocations").filter(allocation_group_id__in=user_allocations).values_list('property_id', flat=True)
    floors = model_mixin.model_object("Properties").filter(pk__in=property_allocations).values_list('floor', flat=True).order_by('floor__orderidx')

    agent_allocated_floors = floors

    if not floors:
        messages.error(request, "You have not been allocated to any groups.")
        return redirect('home')

    if floors:
        floor = floors[0]

        allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                 [floor,],
                                                                                                 request=request,
                                                                                                 agent_allocated_floors=agent_allocated_floors)

    all_prop_rendered = allocation_details['all_prop_rendered']
    search_filter_rendered = allocation_details['search_filter_rendered']
    search_filter_dict = allocation_details['search_filter_dict']

    property_preview_list = allocation_details['property_preview_list']
    key_display_list = allocation_details['key_display_list']
    key_list = allocation_details['key_list']
    property_reservation_list = allocation_details['property_reservation_list']
    property_price_field = allocation_details['property_price_field']

    allocation_group_list = []
    for group in allocation_groups:
        allocation_group_list.append({'group': group,
                                      'apartment_count': len(
                                          model_mixin.model_object("PropertyAllocations").filter(
                                              allocation_group=group))})

    user_add_form = UserCreateFromAllocationForm(request, project)

    if "export_format_with_data" in request.GET:
        filename = 'database_template_data_%s.csv' % allocation_template.template_name
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename

        writer = csv.writer(response)

        csv_header_fields = ProjectCSVTemplateFields.objects.filter(template=allocation_template).order_by(
            'order').values_list('column_pretty_name', flat=True)

        header_fields = ['Row','Project', 'Property ID', 'Property Name', 'Status']
        header_fields = header_fields + [str(x).capitalize() for x in csv_header_fields]

        writer.writerow(header_fields)

        allocations_group_ids = allocation_groups.values_list('pk', flat=True)

        allocations_group_property_ids = model_mixin.model_object("PropertyAllocations")\
            .filter(allocation_group_id__in=allocations_group_ids).values_list('property_id', flat=True)

        properties = model_mixin.model_object("Properties").filter(pk__in=allocations_group_property_ids)

        row_id = 1
        for apartment in properties:

            property_exclusive = None
            check_property_exclusive = model_mixin.klass("Properties")\
                .get_or_create_property_string_field(apartment.pk, 'Exclusive Allocation Group ID')

            if check_property_exclusive:
                property_exclusive = check_property_exclusive[0].value

            if property_exclusive and not int(property_exclusive) in allocations_group_ids:
                data_list = None
            else:
                property_fields, data_list, key_list = model_mixin.klass("Floors").get_select_preview_floor_dictionary(apartment,
                                                                                                                       template_fields)
            if data_list:
                property_name = \
                    model_mixin.model_object("Properties").filter(pk=apartment.pk).values_list('name__english',
                                                                                                 flat=True)[0]

                data_list = [row_id, project.name, apartment.pk, property_name] + data_list
                writer.writerow(data_list)

            row_id += 1

        return response

    reservation_key_display_list = ['status', 'reservation_status', 'date_reserved', 'agent', 'email', 'phone_number',
                                    'retail_price']

    price_mins = []
    for i in range(100000, 3000000, 20000):
        price_mins.append(int(i))

    price_maxs = []
    for i in range(300000, 3500000, 20000):
        price_maxs.append(int(i))

    context = {
        'project': project,
        'actions': actions,
        'user_types': user_types,
        'allocation_groups': allocation_groups,
        'form': form,
        'filter_form': filter_form,
        'users': User.objects.all(),
        'allocation_template_id': allocation_template.pk,
        'all_prop_rendered': all_prop_rendered,
        'search_filter_rendered': search_filter_rendered,
        'search_filter_dict': search_filter_dict,
        'property_preview_list': property_preview_list,
        'key_display_list': key_display_list,
        'key_list': key_list,
        'user_add_form': user_add_form,
        'allocation_group_list': allocation_group_list,
        'property_reservation_list': property_reservation_list,
        'reservation_key_display_list': reservation_key_display_list,
        'property_price_field': property_price_field,
        'price_mins': price_mins,
        'price_maxs': price_maxs

    }

    return render(request, template, context)


@login_required
def project_administration(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    template = "projects/admin/admin.html"
    user_types = User.USER_TYPE_CHOICES
    actions = []

    if request.user.is_allocations_only_user:
        return redirect('project-allocations', project.pk)

    if project.can_be_edited_by(request.user):
        actions.append(('Edit Project Users', 'projects-user-list'))
        actions.append(('Edit Project Details', 'projects-update'))
        actions.append(('View Project Details', 'projects-view'))

    actions = model_mixin.get_reversed_urls_from_actions(actions, pk)

    allocation_groups = model_mixin.model_object("AllocationGroups").all().order_by('name')
    allocation_object = model_mixin.get_class_object("AllocationGroups")
    filter_form = get_search_filter_form(request.GET, model_mixin, [])
    form_class = get_allocation_group_form(request, allocation_object, allocation_groups, model_mixin)
    form = form_class(request.POST or None, request.FILES or None)

    if "export_format_with_data" in request.GET:

        if "id" in request.GET:
            response = None
            group_id = request.GET['id']

            if group_id in ["all", "", None]:
                allocation_group_name = project.name
                filename = 'allocation_group_reservations_%s.csv' % str(allocation_group_name).lower().replace(" ",
                                                                                                               "_",
                                                                                                               10)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="%s"' % filename

                writer = csv.writer(response)

                header_fields = ['Row','Project', 'Property ID', 'Property Name', 'Status',
                                 'Purchaser Title',
                                 'Purchaser First Name', 'Purchaser Last Name', 'Purchaser Email',
                                 'Purchaser Phone Number',
                                 'Buyer Type', 'Company Name', 'Occupy Status', 'Purchaser Location',
                                 'FIRB', 'NRAS', 'Enquiry Source', 'Sales Price',
                                 'Agent Name', 'Agent Company', 'Agent Website', 'Agent Phone Number', 'Agent Email Address']

                writer.writerow(header_fields)

                allocation_properties = model_mixin.model_object("PropertyAllocations").all().values_list('property_id', flat=True).distinct()

                row_id = 1
                for property_id in allocation_properties:

                    apartment = model_mixin.model_object("Properties").get(pk=property_id)
                    string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(apartment.pk, 'status')
                    property_name = model_mixin.klass("Names").get_names_instance(apartment.name_id)

                    show_property = False
                    for field in string_fields:
                        if field.value == "Reserved":
                            print('have reserved property', property_id)
                            show_property = True

                    if show_property:
                        purchaser_details = model_mixin.model_object("BuyerDetails").filter(project_id=project.pk,
                                                                                            property_id=apartment.pk)
                        row_fields = [row_id, project.name, apartment.pk, property_name]

                        row_id +=1

                        full_agent_name = ''
                        agent_company = ''
                        agent_website = ''
                        agent_number = ''
                        agent_email = ''

                        if not purchaser_details:
                            row_fields = row_fields + ['Reserved',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       '',
                                                       full_agent_name,
                                                       agent_company,
                                                       agent_website,
                                                       agent_number,
                                                       agent_email
                                                       ]

                            writer.writerow(row_fields)

                        if purchaser_details:
                            details = purchaser_details[0]

                            if details.agent_id:
                                buyer_agent = User.objects.get(pk=details.agent_id)
                                full_agent_name = buyer_agent.get_full_name()
                                agent_company = buyer_agent.company_name
                                agent_website = buyer_agent.website
                                agent_number = buyer_agent.phone_number
                                agent_email = buyer_agent.email

                            row_fields = row_fields + [details.get_status_display(), details.title,
                                                       details.first_name, details.last_name, details.email,
                                                       details.phone_number,
                                                       details.get_buyer_type_display(), details.company_name,
                                                       details.get_occupy_status_display(),
                                                       details.get_purchaser_location_display(), details.firb,
                                                       details.nras, details.get_enquiry_source_display(),
                                                       details.sales_price,
                                                       full_agent_name,
                                                       agent_company,
                                                       agent_website,
                                                       agent_number,
                                                       agent_email
                                                       ]

                            writer.writerow(row_fields)

            elif group_id == "all_data":
                pass

            else:
                allocation_group = model_mixin.model_object("AllocationGroups").filter(pk=int(group_id))

                if allocation_group:
                    allocation_group = allocation_group[0]
                    allocation_group_name = model_mixin.model_object("AllocationGroups").filter(pk=request.GET['id']).values_list('name__english', flat=True)[0]

                    filename = 'allocation_group_reservations_%s.csv' % str(allocation_group_name).lower().replace(" ", "_", 10)
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment; filename="%s"' % filename

                    writer = csv.writer(response)

                    header_fields = ['Project', 'Property ID', 'Property Name', 'Allocation Group', 'Status', 'Purchaser Title',
                                     'Purchaser First Name', 'Purchaser Last Name', 'Purchaser Email', 'Purchaser Phone Number',
                                     'Agent Name', 'Buyer Type', 'Company Name', 'Occupy Status', 'Purchaser Location',
                                     'FIRB', 'NRAS', 'Enquiry Source', 'Sales Price']

                    writer.writerow(header_fields)

                    allocation_properties = model_mixin.model_object("PropertyAllocations").filter(allocation_group=allocation_group)

                    displayed_properties = []
                    for allocated_property in allocation_properties:
                        if allocated_property.property_id not in displayed_properties:
                            displayed_properties.append(allocated_property.property_id)

                            apartment = model_mixin.model_object("Properties").filter(pk=allocated_property.property_id)[0]
                            string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(apartment.pk, 'status')

                            show_property = False
                            for field in string_fields:
                                if field.value == "Reserved":
                                    show_property = True

                            if show_property:

                                purchaser_details = model_mixin.model_object("BuyerDetails").filter(project_id=project.pk,
                                                                                                      property_id=apartment.pk)
                                row_fields = [project.name, apartment.pk,
                                              model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                              allocation_group_name]

                                if purchaser_details:
                                    details = purchaser_details[0]
                                    try:
                                        agent = User.objects.get(pk=details.agent_id)
                                        agent = agent.get_full_name()
                                    except Exception as e:
                                        print(e)
                                        agent = ''

                                    row_fields = row_fields + [details.get_status_display(), details.title,
                                                               details.first_name, details.last_name,
                                                               details.email, details.phone_number,
                                                               agent,
                                                               details.get_buyer_type_display(),
                                                               details.company_name,
                                                               details.get_occupy_status_display(),
                                                               details.get_purchaser_location_display(), details.firb,
                                                               details.nras, details.get_enquiry_source_display(),
                                                               details.sales_price]

                                    writer.writerow(row_fields)

            if response:
                return response

    if request.method == "POST":
        user_add_form = UserCreateFromAllocationForm(request, project, request.POST)

        if user_add_form.is_valid():
            user = user_add_form.save()
            user.created_by = request.user
            user.owned_by = request.user
            user.last_modified_by = request.user

            project.users.add(user)
            project.save()

            new_sync = ProjectCMSSync()
            try:
                new_sync.add_user_into_sync(project.pk, user.first_name, user.last_name, user.email,
                                            user.is_super_admin)
            except Exception as e:
                print(e)

            try:
                new_sync.add_user_access_into_sync(project.pk, project.name, user.email, user.can_edit_reservation(), user.can_edit_reservation())
            except Exception as e:
                print(e)
        else:
            print (user_add_form.errors)

        if request.FILES:
            project.over_write_new_database_file(request.FILES)
            messages.success(request, "Database has been overwritten successfully")
            return redirect('projects-administration', project.pk)
        else:
            if form.is_valid():
                form.save()
                messages.success(request, "Allocation group added successfully")
                return redirect('projects-administration', project.pk)

        return redirect('projects-administration', project.pk)

    allocation_template = ProjectCSVTemplate.objects.filter(project=project, is_allocation_template=True)
    if not allocation_template:
        allocation_template = ProjectCSVTemplate.objects.filter(project=project, template_name__icontains='allocation')
        if not allocation_template:
            allocation_template = ProjectCSVTemplate.objects.filter(project=project)
            if not allocation_template:
                messages.error(request, "You have not been set any template for this project.")
                return redirect('projects-select-template', project.pk)

    allocation_template = allocation_template[0]
    allocation_template.is_allocation_template = True
    allocation_template.save()

    template_fields = ProjectCSVTemplateFields.objects.filter(template=allocation_template).order_by('order')

    all_prop_rendered = None
    search_filter_rendered = None
    search_filter_dict = None
    property_preview_list = None
    key_display_list = None
    key_list = None
    property_reservation_list = None

    model_mixin.klass("AllocationGroups").check_first_allocation_group()

    floor = model_mixin.model_object("Properties").all().values_list('floor', flat=True).order_by('floor__orderidx')
    if floor:
        floor = floor[0]
    else:
        messages.error(request, "The project %s has not had any data imported. " % project.name)
        return redirect('home')

    building = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor)
    if building:
        building = building[0].pk

    if floor:
        allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                 [floor, ],
                                                                                                 request=request)
        all_prop_rendered = allocation_details['all_prop_rendered']
        search_filter_rendered = allocation_details['search_filter_rendered']
        search_filter_dict = allocation_details['search_filter_dict']

        property_preview_list = allocation_details['property_preview_list']
        key_display_list = allocation_details['key_display_list']
        key_list = allocation_details['key_list']
        property_reservation_list = allocation_details['property_reservation_list']
        property_price_field = allocation_details['property_price_field']

    allocation_group_list = []
    for group in allocation_groups:
        allocation_group_list.append({'group': group,
                                      'apartment_count': len(model_mixin.model_object("PropertyAllocations").filter(allocation_group=group))})

    user_add_form = UserCreateFromAllocationForm(request, project)

    if "export_format_with_data" in request.GET:

        filename = 'database_template_data_%s.csv' % allocation_template.template_name
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename

        writer = csv.writer(response)

        downloadable_fields = ProjectCSVTemplateFields.objects.filter(template__project=project, template__is_editable_template_default=True).order_by('order')
        if not downloadable_fields:
            downloadable_fields = ProjectCSVTemplateFields.objects.filter(template__project=project, template__is_allocation_template=True).order_by('order')

        header_fields = downloadable_fields.values_list('column_pretty_name', flat=True).distinct()
        header_fields = [str(x).capitalize() for x in header_fields]
        writer.writerow(header_fields)

        properties = model_mixin.model_object("Properties").all()
        for property in properties:
            property_fields, data_list, key_list = model_mixin.klass("Floors").get_select_preview_floor_dictionary(property, downloadable_fields)
            writer.writerow(data_list)

        return response

    reservation_key_display_list = ['status', 'reservation_status', 'date_reserved', 'agent', 'email',
                                    'phone_number', 'retail_price']

    price_mins = []
    for i in range(100000, 3000000, 20000):
        price_mins.append(int(i))

    price_maxs = []
    for i in range(300000, 3500000, 20000):
        price_maxs.append(int(i))

    users = request.user.viewable_users.filter(project_users_list=model_mixin.project)

    view_filter_form = True
    user_types = User.USER_TYPE_CHOICES
    if request.user.is_client_admin:
        user_types = User.CLIENT_ADMIN_USER_TYPE_CHOICES

    if request.user.is_master_agent:
        user_types = User.MASTER_AGENT_USER_TYPE_CHOICES

    if request.user.is_agent or request.user.is_view_only_user:
        user_types = []
        view_filter_form = False

    context = {
        'project': project,
        'actions': actions,
        'user_types': user_types,
        'view_filter_form': view_filter_form,
        'allocation_groups': allocation_groups,
        'form': form,
        'filter_form': filter_form,
        'users': users,
        'all_users': users,
        'allocation_template_id': allocation_template.pk,
        'all_prop_rendered': all_prop_rendered,
        'search_filter_rendered': search_filter_rendered,
        'search_filter_dict': search_filter_dict,
        'property_preview_list': property_preview_list,
        'key_display_list': key_display_list,
        'key_list': key_list,
        'user_add_form': user_add_form,
        'allocation_group_list': allocation_group_list,
        'property_reservation_list': property_reservation_list,
        'reservation_key_display_list': reservation_key_display_list,
        'property_price_field': property_price_field,
        'price_mins': price_mins,
        'price_maxs': price_maxs,
        'first_building': building,
        'first_floor': floor

    }

    return render(request, template, context)