import json
import csv
import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.apps import apps
from django.conf import settings
from django.db.models.loading import get_model
from django.core import serializers
from django.http import HttpResponse
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.core.files import File

from core.generic_views import ListView, ProjectListCallbackView, DetailView, VersionView, UpdateView, DeleteView, CreateView
from core.models import Project, CSVFileImport, BulkImport, DisplaySweetConfiguration
from projects.permissions import CreatePermissionMixin, ProjectsPermissionsMixin, ProjectModelMixin
from projects.forms import ProjectAddForm, get_object_filter_form, apply_form_filter, CSVFileImportAddForm, \
                   BulkImportUploadForm, get_formfield_form, get_object_add_floor_plan_form, CSVFileImportAllTablesForm, \
                   CreateProjectDetailsForm

from projects.models.model_mixin import PrimaryModelMixin


@login_required
def update_hotspot_configuration(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/floor_plan_details/update_hot_spot.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    model_mixin.get_class_object('Floors')
    floors_model_instance = model_mixin.get_model_instance(model_instance.floor_id)
    has_floor_plan_image = project.has_floor_plan_image(floors_model_instance.floor_plate_image_id)

    model_mixin.get_class_object('Resources')
    images_model_instance = model_mixin.get_model_instance(floors_model_instance.floor_plate_image_id)
    image_path = str(images_model_instance.highres_path)

    image_exists = None
    if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
        image_exists = True

    if request.method == "POST" and "selected_value_x" in request.POST and request.POST['selected_value_y']:
        model_instance.picking_coord_x = request.POST['selected_value_x']
        model_instance.picking_coord_y = request.POST['selected_value_y']
        model_instance.save(using=project.project_connection_name)
        messages.success(request, "Hotspot configuration saved successfully.")

    context = {
        'project': project,
        'model': project_model_object,
        'instance': model_instance,
        'floors_instance': floors_model_instance,
        'model_name': model_name,
        'row_id': row_id,
        'has_floor_plan_image': has_floor_plan_image,
        'image_path': image_path,
        'images_model_instance': images_model_instance,
        'image_exists': image_exists
    }

    return render(request, template, context)


@login_required
def project_floors_hotpots(request, pk, floor_id):
    template = "projects/dynamic_editing/floor_plan_details/all_floor_hotspots.html"

    model_name = 'floors'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    project_model_object = model_mixin.get_class_object(model_name)
    all_floors = model_mixin.klass("Floors").get_all_floors()

    floor_list = []

    model_instance = model_mixin.class_object("Floors").filter(pk=floor_id)[0]
    model_mixin.get_class_object(project.apartment_model)

    for floor in all_floors:
        try:
            apartment_count = len(model_mixin.get_model_queryset('floor_id', floor.id))
        except:
            apartment_count = 0

        floor_list.append(
            {'floor': floor,
             'apartment_count': apartment_count}
        )

    image_path = None

    if project.has_apartment_string_fields():
        level_image = model_mixin.klass("LevelPlans").get_floor_level_plan(floor_id)
        if level_image:
            image_path = level_image.path


    apartment_list = []
    apartments_dict = []

    for floor in all_floors:
        floor_id = floor.pk
        apartments = model_mixin.class_object(project.apartment_model).filter(floor_id=floor_id)

        for apartment in apartments:
            if project.has_apartment_string_fields():

                spot_list = model_mixin.klass("Properties").get_apartment_hotspot_values(apartment.pk, floor_id=floor.pk)

                try:
                    picking_coord_x = spot_list[0]['picking_coord_x']
                    picking_coord_y = spot_list[0]['picking_coord_y']
                except:
                    picking_coord_x = 0
                    picking_coord_y = 0

                apartment_kwargs = {'id': apartment.pk,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'spot_list': spot_list,
                                    'name': model_mixin.klass("Names").get_names_instance(apartment.name_id),
                                    'floor_id': floor_id,
                                     'building_id': floor.building_id
                                     }

                apartment_list.append(apartment_kwargs)
                apartments_dict.append(apartment_kwargs)


    apartment_list = json.dumps(apartment_list)

    image_exists = None
    try:
        check_path = os.path.join(settings.BASE_IMAGE_LOCATION, image_path)
        check_path = check_path.replace("//", "/")
        if os.path.isfile(check_path):
            image_exists = True

            static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
    except:
        pass

    queries = request.GET.copy()
    context = {
        'project': project,
        'model': project_model_object,
        'model_instance': model_instance,
        'model_name': model_name,
        'has_hot_spot_configuration': True,
        'floors': True,
        'image_path': image_path,
        'static_image': static_image,
        'image_exists': image_exists,
        'queries': queries,
        'apartments': apartments,
        'apartment_list': apartment_list,
        'apartments_dict': apartments_dict,
        'all_floors': all_floors,
        'floor_list': floor_list
    }

    return render(request, template, context)


@login_required
def project_floors_hotpots_reassign(request, pk, floor_id):
    template = "projects/dynamic_editing/floor_plan_details/all_floor_hotspots_reassign.html"

    model_name = 'floors'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    
    project_model_object = model_mixin.get_class_object(model_name)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)
    model_instance = model_mixin.get_model_instance(floor_id)
    all_floors = model_mixin.get_model_queryset()

    floor_list = []

    model_mixin.get_class_object(project.apartment_model)

    for floor in all_floors:
        apartments = len(model_mixin.get_model_queryset('floor_id', floor.id))

        floor_list.append(
            {'floor': floor, 'apartment_count': apartments }
        )

    model_mixin.get_class_object('Resources')
    images_model_instance = model_mixin.get_model_instance(model_instance.floor_plate_image_id)
    image_path = str(images_model_instance.highres_path)

    model_mixin.get_class_object(project.apartment_model)
    apartments = model_mixin.get_model_queryset('floor_id', floor_id)

    apartment_list = []
    for apartment in apartments:
        apartment_list.append({'id': apartment.name,
                               'x_coordinate': apartment.picking_coord_x,
                               'y_coordinate': apartment.picking_coord_y
                              })

    apartment_list = json.dumps(apartment_list)

    image_exists = None
    if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
        image_exists = True

    if request.method == "POST":
        for apartment in apartments:
            apartment_in_form = "selected_apartment_%s" % apartment.id

            if apartment_in_form in request.POST and request.POST[apartment_in_form]:
                selected = request.POST[apartment_in_form]
                selected_apartment = model_mixin.get_model_queryset('name', selected)

                x_in_form = "apt_x_%s" % selected_apartment.id
                x_in_form = request.POST[x_in_form]

                y_in_form = "apt_y_%s" % selected_apartment.id
                y_in_form = request.POST[y_in_form]

                apartment.picking_coord_x = x_in_form
                apartment.picking_coord_y = y_in_form

                apartment.save(using=project.project_connection_name)

        messages.success(request, "Apartments reassigned successfully.")

    queries = request.GET.copy()
    context = {
        'project': project,
        'model': project_model_object,
        'model_instance': model_instance,
        'model_name': model_name,
        'has_hot_spot_configuration': True,
        'floors': True,
        'image_path': image_path,
        'image_exists': image_exists,
        'queries': queries,
        'apartments': apartments,
        'apartment_list': apartment_list,
        'all_floors': all_floors,
        'floor_list': floor_list
    }

    return render(request, template, context)




@login_required
def update_hotspot_custom_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_custom_hotspot.html"

    project = Project.objects.get(pk=pk)

    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)
    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields(), drop_id=True)

    configuration = DisplaySweetConfiguration.objects.all()
    if configuration:
        configuration = configuration[0]

    floor_model_object = get_model(project.project_connection_name, 'Floors')
    floor_model_instance = floor_model_object.objects.using(project.project_connection_name).get(pk=row_id)
    has_floor_plan_image = project.has_floor_plan_image(floor_model_instance.floor_plate_image_id)

    form_class = get_formfield_form(model_instance, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            save_form_data(project_model_object, form.cleaned_data, model_fields, project, model_instance=model_instance)
            messages.success(request, "Data saved successfully")
            return redirect('projects-model-filter', project.pk, model_name)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

        actions.append(('Add / Edit Apartments', "%s?floor_id=%s" % (reverse('projects-model-filter', args=[project.pk, 'Apartments']), row_id)))
        actions.append(('Update Floor Plan Image', reverse('projects-floors-add-floor-plan', args=[project.pk, row_id])))

        if project.has_views_model():
            actions.append(('Assign Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))
            actions.append(('Add / Edit Apartment Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'floor_model_instance': floor_model_instance,
        'actions': actions,
        'model_name': model_name,
        'configuration': configuration,
        'has_floor_plan_image': has_floor_plan_image,
        'file_location': ''
    }

    return render(request, template, context)


@login_required
def project_hotspot_redirect(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    model_mixin.get_class_object("Floors")
    all_floors = model_mixin.get_model_queryset(list_needed=True)

    if all_floors:
        first_floor = all_floors[0]
        return redirect('projects-floors-hotspots', model_mixin.project.pk, first_floor.id)

    return redirect('home')


def save_form_data(project_model_object, cleaned_data, model_fields, project, model_instance=None):

    if model_instance:
        for field in model_fields:
            cleaned_value = cleaned_data.get(field, None)
            setattr(model_instance, field, cleaned_value)

        model_instance.save(using=project.project_connection_name)

    return model_instance


def filter_model_fields_for_form(model_fields, drop_id=False):

    field_names = []

    for field in model_fields:
        if drop_id:
            #not type(field) == ForeignKey and
            if  not field.name == "id" and not type(field) == ManyToOneRel:
                field_names.append(field.name)
        else:
            if not type(field) == ManyToOneRel and not type(field) == ForeignKey:
                field_names.append(field.name)

    return field_names