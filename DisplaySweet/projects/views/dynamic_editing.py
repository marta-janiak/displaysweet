import json
import csv
import os
import subprocess
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.apps import apps
from django.conf import settings
from django.forms.models import model_to_dict
from django.db.models.loading import get_model
from django.core import serializers
from django.http import HttpResponse
from django.utils.encoding import smart_str
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.core.files import File
from django.template.loader import render_to_string
from django.db.models import Q

from core.generic_views import ListView, ProjectListCallbackView, DetailView, VersionView, UpdateView, DeleteView, CreateView
from core.models import Project, ProjectCSVTemplate, ProjectCSVTemplateFields, BulkImport, DisplaySweetConfiguration, ProjectImagePath, ProjectConnection
from projects.permissions import CreatePermissionMixin, ProjectsPermissionsMixin, ProjectModelMixin
from projects.forms import ProjectAddForm, get_object_filter_form, apply_form_filter, CSVFileImportAddForm, \
                   BulkImportUploadForm, get_formfield_form, get_object_add_floor_plan_form, CSVFileImportAllTablesForm, \
                   ProjectImagePathForm, get_apartment_template_formset, CreateImportTemplateForm

from utils.xlxs_generator import BulkImportTemplate
from django.forms.models import inlineformset_factory
from .initial_project_details import return_position_page, filter_model_fields_for_form, save_form_data, project_actions
from projects.models.model_mixin import PrimaryModelMixin


@login_required
def model_filter_view(request, pk, model_name, extra_context=None):
    template = "projects/dynamic_editing/project_model_filter.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    model_field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, model_field_names, project_model_object, project.project_connection_name)

    has_hot_spot_configuration = False
    if "picking_coord_x" in model_fields and "picking_coord_y" in model_fields:
        has_hot_spot_configuration = True

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []


    field_names = model_field_names
    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":
        if "sort_by" in request.GET and request.GET['sort_by']:
            sorting_field = request.GET['sort_by']

        if "sorting_direction" in request.GET and request.GET['sorting_direction']:
            sorting_direction = request.GET['sorting_direction']

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):
        if data and 'id' in model_field_names:
            actions.append(('Download Filtered List', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))

    paginate_by = 50
    position = 0
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']
        position = return_position_page(page, paginate_by)

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    page_list = []
    for i in range(1, int(pages + 1)):
        page_list.append(int(i))

    next_page = int(page) + 1
    previous_page = int(page) - 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'position': position,
        'page': int(page),
        'pages': int(pages),
        'page_list': page_list,
        'next_page': next_page,
        'previous_page': previous_page,
        'data': data[position:(int(paginate_by) * int(page))],
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'queries_encoded': str(queries.urlencode()),
        'actions': actions,
        'applied_filters': applied_filters,
        'has_hot_spot_configuration': has_hot_spot_configuration,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction),
        'page_template': 'projects/dynamic_editing/includes/project_model_filter_pagination.html'
    }

    if extra_context is not None:
        context.update(extra_context)

    return render(request, template, context)



@login_required
def project_model(request, pk, model_name):
    template = "projects/dynamic_editing/project_model.html"

    project = Project.objects.get(pk=pk)
    project_model_object = get_model(project.project_connection_name, model_name)
    field_names = project_model_object._meta.get_all_field_names()
    table_values = project_model_object.objects.using(project.project_connection_name).all()

    project_permissions = ProjectsPermissionsMixin()

    actions = []

    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))
        actions.append(('Filter all data', reverse('projects-model-filter', args=[project.pk, model_name])))
        actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'actions': actions
    }

    return render(request, template, context)


@login_required
def update_model_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_model_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    if model_name.lower() == 'floors':
        return redirect('projects-floors-update', project.pk, model_name, row_id)
    elif model_name.lower() == 'apartmenthotspots':
        return redirect('projects-hotspot-update', project.pk, model_name, row_id)
    elif model_name.lower() == 'apartmentviews':
        return redirect('projects-apartment-views-update', project.pk, model_name, row_id)

    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)

    has_hot_spot_configuration = False
    if "picking_coord_x" in model_fields and "picking_coord_y" in model_fields:
        has_hot_spot_configuration = True

    has_floor_plan_image_id = False
    image_field = None
    image_exists = None

    if "floor_plan_image_id" in model_fields:
        has_floor_plan_image_id = True

        model_mixin.get_class_object(project.images_model)
        images_model_instance = model_mixin.get_model_instance(model_instance.floor_plan_image_id)
        image_field = images_model_instance.highres_path
        if image_field:
            if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_field)):
                image_exists = True

    form_class = get_formfield_form(model_instance, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            save_form_data(project_model_object, form.cleaned_data, model_fields, project, model_instance=model_instance)
            messages.success(request, "Data saved successfully")
            return redirect('projects-model-filter', project.pk, model_name)

    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

        if has_hot_spot_configuration:
            actions.append(('Update Hotspot configuration', reverse('projects-hotspot-configuration', args=[project.pk, model_name, row_id])))
        if has_floor_plan_image_id:
            actions.append(('Update Floor Plan Image', reverse('projects-hotspot-configuration', args=[project.pk, model_name, row_id])))

        if model_name.lower() == 'apartmenttypes':
            actions.append(('Edit Floor Plan Images', reverse('projects-apartment-types-floor-plan', args=[project.pk, row_id])))

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'has_hot_spot_configuration': has_hot_spot_configuration,
        'has_floor_plan_image_id': has_floor_plan_image_id,
        'image_exists': image_exists,
        'image_field': image_field,
        'row_id': row_id
    }

    return render(request, template, context)


@login_required
def delete_model_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/delete_model_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    if request.method == "POST":
        model_instance.delete(using=project.project_connection_name)
        messages.success(request, "Data deleted successfully")
        return redirect('projects-model-filter', project.pk, model_name)

    context = {
        'project': project,
        'model': project_model_object,
        'instance': model_instance,
        'actions': project_actions(request, project, model_name),
        'model_name': model_name
    }

    return render(request, template, context)



@login_required
def upload_model_data_complete(request, pk, bulk_import_id):
    template = "projects/dynamic_editing/upload_model_data_complete.html"

    project = Project.objects.get(pk=pk)
    bulk_import = BulkImport.objects.get(pk=bulk_import_id)

    context = {
        'project': project,
        'bulk_import': bulk_import
    }

    return render(request, template, context)



@login_required
def update_apartment_types_floor_plan_image(request, pk, apartment_type_id):
    template = 'projects/dynamic_editing/floor_plan_details/apartment_types_floor_plans.html'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = model_mixin.get_class_object('Apartmenttypes')
    model_instance = model_mixin.get_model_instance(apartment_type_id)

    model_mixin.get_class_object(project.images_model)
    images_model_instance = model_mixin.get_model_instance(model_instance.name, field='description')

    image_field = None
    image_exists = None
    if request.GET and 'image' in request.GET:
        image_selected = request.GET['image']

        try:
            image_field = getattr(images_model_instance, image_selected)
        except:
            pass

        if image_field:
            image_exists = os.path.isfile(image_field)

    context = {
        'project': project,
        'model': project_model_object,
        'row_id': apartment_type_id,
        'instance': model_instance,
        'images_model_instance': images_model_instance,
        'image_field': image_field,
        'image_exists': image_exists
    }

    return render(request, template, context)


@login_required
def delete_model_row_instant(request, pk, model_name, row_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)

    model_instance.delete(using=project.project_connection_name)
    messages.success(request, "Data deleted successfully")
    return redirect('projects-model-filter', project.pk, model_name)


@login_required
def upload_model_data(request, pk, model_name):
    template = "projects/dynamic_editing/upload_model_data.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)

    bulk_import = BulkImport.objects.create(project=project, table_name=model_name, status=BulkImport.STATUS_DRAFT)
    upload_form = BulkImportUploadForm(instance=bulk_import)

    if request.method == 'POST':
        if 'submit__upload' in request.POST:
            upload_form = BulkImportUploadForm(request.POST, request.FILES, instance=bulk_import)

            if upload_form.is_valid():
                bulk_import = upload_form.save(commit=False)
                bulk_import.status = BulkImport.STATUS_PROCESSED
                bulk_import.save()
                messages.success(request, "You excel file was imported successfully.")
                return redirect('projects-model-filter', project.pk, model_name)

    context = {
        'project': project,
        'model': project_model_object,
        'model_name': model_name,
        'upload_form': upload_form
    }

    return render(request, template, context)


@login_required
def project_buildings(request, pk):
    template = "projects/dynamic_editing/project_model_filter.html"

    model_name = 'buildings'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, field_names, project_model_object, project.project_connection_name)

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []

    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):

        if data:
            actions.append(('Download For Bulk Edit', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add %s' % model_name, reverse('projects-model-insert', args=[project.pk, model_name])))

    paginate_by = 50
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'data': data,
        'page': int(page),
        'pages': int(pages),
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'actions': actions,
        'applied_filters': applied_filters,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction)

    }

    return render(request, template, context)


@login_required
def project_floors(request, pk):
    template = "projects/dynamic_editing/project_model_filter.html"

    model_name = 'floors'

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project
    project_model_object = model_mixin.get_class_object("Floors")
    all_floors = model_mixin.get_model_queryset(list_needed=True)

    first_floor = None
    if all_floors:
        first_floor = all_floors[0]

    model_fields = filter_model_fields_for_form(project_model_object._meta.get_fields())
    field_names = project_model_object._meta.get_all_field_names()

    form = get_object_filter_form(request.GET, project.project_connection_name, model_fields, project_model_object, ())
    table_values = apply_form_filter(request.GET, field_names, project_model_object, project.project_connection_name)

    try:
        data = serializers.serialize("python", table_values)
    except:
        data = []

    if data:
        field_names = data[0]['fields']
        field_names = [str(x) for x, y in field_names.items()]

    applied_filters = {}
    sorting_field = ''
    sorting_direction = 'asc'

    if request.method == "GET":
        if "sort_by" in request.GET and request.GET['sort_by']:
            sorting_field = request.GET['sort_by']

        if "sorting_direction" in request.GET and request.GET['sorting_direction']:
            sorting_direction = request.GET['sorting_direction']

        for key, value in request.GET.items():
            if not value in ["All", ""] and not key in ["csrfmiddlewaretoken", "sort_by", "sorting_direction"]:
                applied_filters.update({str(key).replace("_", " "): value})

        if "export_format" in request.GET:
            bulk_import_template = BulkImportTemplate(model_name, project_model_object, len(table_values),
                                                      project=project, table_values=table_values)

            return bulk_import_template.render_to_response()

    queries = request.GET.copy()
    project_permissions = ProjectsPermissionsMixin()
    actions = []

    if project_permissions.can_be_edited_by(request.user):

        if data:
            actions.append(('Download For Bulk Edit', project.product_model_filter_url(model_name, queries)))
            actions.append(('Import Updated Details', reverse('projects-model-upload', args=[project.pk, model_name])))

        actions.append(('Add Floor', reverse('projects-model-insert', args=[project.pk, model_name])))

        if first_floor:
            actions.append(('Update Hotspots', reverse('projects-floors-hotspots', args=[project.pk, first_floor.id])))
            actions.append(('Assign Views', reverse('projects-floors-views', args=[project.pk, first_floor.id])))
    paginate_by = 50
    page = 1

    if 'page' in request.GET:
        page = request.GET['page']

    count_data = len(data)
    pages = float(count_data) / float(paginate_by)

    test_extra_page = str(pages).split('.')
    if int(test_extra_page[1]) > 0:
        pages += 1

    context = {
        'project': project,
        'model': project_model_object,
        'field_names': field_names,
        'table_values': table_values,
        'page': int(page),
        'pages': int(pages),
        'data': data,
        'model_name': model_name,
        'form': form,
        'queries': queries,
        'actions': actions,
        'applied_filters': applied_filters,
        'has_hot_spot_configuration': True,
        'floors': True,
        'sorting_field': str(sorting_field).replace(" ", ""),
        'sorting_direction': str(sorting_direction)
    }

    return render(request, template, context)


@login_required
def edit_apartments_import_template(request, pk, template_id, apartment_id):
    template = "projects/dynamic_editing/editable_template.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin.get_class_object(project.apartment_model)
    apartment = model_mixin.get_model_instance(apartment_id)

    all_templates = ProjectCSVTemplate.objects.filter(project=project, is_editable_template=True).order_by('id')
    editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
    total_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template)

    IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                              form=get_apartment_template_formset(project, model_mixin, apartment),
                                              extra=0)

    form = CreateImportTemplateForm(instance=editable_template)
    ingredient_formset = IngredientFormSet(instance=editable_template,
                                           queryset=editable_template.project_template_fields.order_by("order"))

    if request.method == "POST":

        ingredient_formset = IngredientFormSet(request.POST, instance=editable_template)

        if ingredient_formset.is_valid():
            for form in ingredient_formset:
                form.save_apartment_details(model_mixin, apartment)

            messages.success(request, "Apartment details saved successfully.")
            return redirect('projects-model-filter', project.pk, project.apartment_model)
        else:
            print (ingredient_formset.errors)

    context = {
        'project': project,
        'template_form': form,
        'template_columns_form': ingredient_formset,
        'total_fields': len(total_fields),
        'apartment': apartment,
        'all_templates': all_templates,
        'template_id': int(template_id)
    }

    return render(request, template, context)

@login_required
def update_floors_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_floors_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = model_mixin.get_class_object(model_name)
    model_instance = model_mixin.get_model_instance(row_id)
    model_fields = model_mixin.get_model_object_fields(drop_id=True)

    configuration = DisplaySweetConfiguration.objects.all()
    if configuration:
        configuration = configuration[0]

    has_floor_plan_image = False
    image_path = None
    image_exists = None
    if not project.has_images_schema():
        model_dict = model_to_dict(model_instance)
        if "floor_plate_image_id" in model_dict:
            has_floor_plan_image = project.has_floor_plan_image(model_instance.floor_plate_image_id)

            model_mixin.get_class_object(project.images_model)
            images_model_instance = model_mixin.get_model_instance(model_instance.floor_plate_image_id)
            image_path = str(images_model_instance.highres_path)

            image_exists = None
            if os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, image_path)):
                image_exists = True
    else:

        model_mixin.get_class_object('FloorViewImages')
        images_model_instance = model_mixin.get_model_queryset()

    form_class = get_formfield_form(model_instance, model_fields, project_model_object, project)
    form = form_class(request.POST or None, request.FILES or None)

    if request.method == "POST":
        if form.is_valid():
            save_form_data(project_model_object, form.cleaned_data, model_fields, project, model_instance=model_instance)
            messages.success(request, "Data saved successfully")
            return redirect('projects-model-filter', project.pk, model_name)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

        actions.append(('Add / Edit Apartments', "%s?floor_id=%s" % (reverse('projects-model-filter', args=[project.pk, 'Apartments']), row_id)))
        actions.append(('Update Floor Plan Image', reverse('projects-floors-add-floor-plan', args=[project.pk, row_id])))

        if project.has_hotspot_model():
            actions.append(('Add / Edit Hotspots', reverse('projects-model-filter', args=[project.pk, 'ApartmentHotspots'])))
        else:
            actions.append(('Edit Floor Hotspots', reverse('projects-floors-hotspots', args=[project.pk, model_instance.id])))

        if project.has_views_model():
            actions.append(('Assign Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))
            actions.append(('Add / Edit Apartment Views', reverse('projects-model-filter', args=[project.pk, 'ApartmentViews'])))

    context = {
        'project': project,
        'model': project_model_object,
        'form': form,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'configuration': configuration,
        'has_floor_plan_image': has_floor_plan_image,
        'image_path': image_path,
        'image_exists': image_exists
    }

    return render(request, template, context)



@login_required
def update_apartment_views_custom_row(request, pk, model_name, row_id):
    template = "projects/dynamic_editing/update_views_row.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [ int(x) for x in apartment_ids_list]

    apartment_object = get_model(project.project_connection_name, 'Apartments')

    assigned_instances = apartment_object.objects.using(project.project_connection_name).filter(id__in=apartment_ids_list)
    apartment_instances = apartment_object.objects.using(project.project_connection_name).all().exclude(id__in=assigned_instances)

    images_object = get_model(project.project_connection_name, project.images_model)
    image_instance = images_object.objects.using(project.project_connection_name).get(pk=model_instance.resource_id)

    project_permissions = ProjectsPermissionsMixin()
    actions = []
    if project_permissions.can_be_edited_by(request.user):
        actions.append(('Delete', reverse('projects-model-row-delete', args=[project.pk, model_name, row_id])))

    context = {
        'project': project,
        'model': project_model_object,
        'image_instance': image_instance,
        'apartment_instances': apartment_instances,
        'assigned_instances': assigned_instances,
        'instance': model_instance,
        'actions': actions,
        'model_name': model_name,
        'row_id': row_id
    }

    return render(request, template, context)


@login_required
def update_apartment_save_view(request, pk, model_name, row_id, apartment_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [int(x) for x in apartment_ids_list]
    apartment_ids_list.append(apartment_id)
    apartment_ids_list = [str(x) for x in apartment_ids_list]

    apartment_ids_string = ','.join(apartment_ids_list)
    model_instance.apartment_ids = apartment_ids_string
    model_instance.save(using=project.project_connection_name)
    messages.success(request, "Apartment views updated successfully")

    return redirect('projects-apartment-views-update', project.pk, model_name, row_id)


@login_required
def update_apartment_remove_view(request, pk, model_name, row_id, apartment_id):

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    project_model_object = get_model(project.project_connection_name, model_name)
    model_instance = project_model_object.objects.using(project.project_connection_name).get(pk=row_id)

    apartment_ids_list = model_instance.apartment_ids.split(",")
    apartment_ids_list = [int(x) for x in apartment_ids_list]

    if int(apartment_id) in apartment_ids_list:
        apartment_ids_list.remove(int(apartment_id))
        apartment_ids_list = [str(x) for x in apartment_ids_list]

        apartment_ids_string = ','.join(apartment_ids_list)
        model_instance.apartment_ids = apartment_ids_string
        model_instance.save(using=project.project_connection_name)

        messages.success(request, "Apartment views updated successfully")
    else:
        messages.error(request, "Apartment views is not a part of the Assigned Views")

    return redirect('projects-apartment-views-update', project.pk, model_name, row_id)


@login_required
def map_project_image_paths(request, pk):
    template = "projects/map_image_paths.html"

    model_mixin = PrimaryModelMixin(request=request, project_id=pk)
    project = model_mixin.project

    model_mixin.model_object("TextTableCategories").get_or_create(category='Image Types')
    text_category = model_mixin.model_object("TextTableCategories").filter(category='Image Types')[0]

    model_mixin.model_object("Descriptions").get_or_create(text_table_category=text_category)
    image_types_queryset = model_mixin.model_object("Descriptions").filter(text_table_category=text_category)

    ImagePathFormSet = inlineformset_factory(Project, ProjectImagePath,
                                             form=ProjectImagePathForm(project, image_types_queryset, model_mixin),
                                             can_delete=True, extra=3)

    formset = ImagePathFormSet(instance=project)

    if request.method == "POST":
        formset = ImagePathFormSet(request.POST, request.FILES, instance=project)

        if formset.is_valid():
            for form in formset:
                if form.is_valid():

                    if "DELETE" in form.cleaned_data and form.cleaned_data['DELETE'] == True:
                        if form.cleaned_data['DELETE'] and form.instance:
                            form.instance.delete()
                    else:
                        form.save()

            for key, item in request.FILES.items():
                image_files = request.FILES.getlist(key)
                section_key = key.replace("image_files", "image_path")
                path_selected = request.POST[section_key]

                specified_file = path_selected.replace("projects/%s/" % project.project_connection_name, "")
                base_file_location = settings.BASE_IMAGE_LOCATION
                file_location = '%s/%s/' % (base_file_location, path_selected)
                file_location = file_location.replace('//', '/')
                file_location = file_location.replace("/project_data/project_data/", "/project_data/")

                if not os.path.isdir(file_location):
                    os.makedirs(file_location)

                for image_file in image_files:
                    saving_location = '%s%s' % (project.get_projector_location(), str(image_file.name))
                    with open(saving_location, 'wb+') as destination:
                        for chunk in image_file.chunks():
                            destination.write(chunk)

                # if project.image_dimensions:
                #     ImportedImageDistributor(project, settings.BASE_IMAGE_LOCATION, project.get_projector_location(),
                #                              file_location,
                #                              file_location,
                #                              specified_file=specified_file, request=request)

            messages.success(request, "Image paths saved and images loaded successfully for project")
            ProjectImagePath.objects.filter(image_path='').delete()
            return redirect('projects-map-image-paths', project.pk)
        else:
            messages.error(request, formset.errors)
    else:
        messages.error(request, formset.errors)

    image_type_choices = []
    for image_type in image_types_queryset:
        if image_type and image_type.english:
            image_type_choices.append(image_type.english)

    context = {
        'project': project,
        'formset': formset,
        'project_images': ProjectImagePath.objects.filter(project=project),
    }

    return render(request, template, context)



