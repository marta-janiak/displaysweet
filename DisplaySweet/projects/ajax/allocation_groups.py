from django.http import HttpResponse
import json

from django.db.models.loading import get_model
from django.db.models import Q
from django.http import QueryDict

from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.forms.models import inlineformset_factory
from users.models import User, Purchaser, UserModifiedLog
from users.forms import UserCreateFromAllocationForm
from itertools import chain
from projects.forms import ReservationPurchaserForm, ReservationAgentForm, get_allocations_property_formset
from core.models import ProjectCSVTemplate, ProjectCSVTemplateFields, ProjectCSVTemplateFieldValues, Project
from ..cms_sync import ProjectCMSSync

import datetime
from projects.models.model_mixin import PrimaryModelMixin


def get_allocation_search_list(request, pk):

    if request.is_ajax():
        try:

            model_mixin = PrimaryModelMixin(request=request, project_id=pk)

            status_list = request.GET.getlist('status_list[]')
            if status_list:
                status_list = [int(x) for x in status_list]

            user_type_list = request.GET.getlist('user_type_list[]')
            search_text = request.GET['search_text']

            project_status = None
            if "project_status" in request.GET and request.GET['project_status']:
                project_status = request.GET['project_status']

            if not request.user.is_ds_admin:
                if not user_type_list:
                    if request.user.is_client_admin:
                        user_type_list = User.CLIENT_ADMIN_CHOICES
                    if request.user.is_master_agent:
                        user_type_list = User.MASTER_AGENT_CHOICES

            rendered = None

            project = Project.objects.get(pk=pk)
            users = request.user.viewable_users

            if users:
                if user_type_list:
                    users = users.filter(user_type__in=user_type_list)

                if status_list:
                    users = users.filter(is_active__in=status_list)

                if search_text:
                    users = users.filter(Q(first_name__icontains=search_text) |
                                         Q(last_name__icontains=search_text) |
                                         Q(email__icontains=search_text) |
                                         Q(username__icontains=search_text))

            if project_status:
                updated_users = []
                if project_status == "Yes":
                    project_users = project.users.all()
                    updated_users += list(set(users).intersection(project_users))

                if project_status == "No":
                    found_users = []
                    project_users = project.users.all()
                    found_users += list(set(users).intersection(project_users))

                    updated_users = list(set(users) - set(found_users))

                users = updated_users

            allocation_group = request.GET['group_id']

            allocated_users = []
            user_list = []
            for user in users:
                if allocation_group:
                    allocated = model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                                                  user_id=user.pk)

                    if user and user not in user_list:
                        user_list.append(user)
                        is_allocated = False
                        if allocated:
                            is_allocated = True

                        allocated_users.append({'user': user, 'is_allocated': is_allocated})

            context = {
                'allocated_users': allocated_users,
                'request': request,
            }

            html_template = "users/includes/assigning_user_list.html"
            try:
                rendered = render_to_string(html_template, context)
            except Exception as e:
                print(e)

        except Exception as e:
            print(e)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_user_allocation_list(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        user_id = request.GET['user_id']
        selected_group_id = request.GET['group_id']
        user = User.objects.get(pk=user_id)

        project_group_list = []
        allocation_groups = model_mixin.model_object("AllocationGroups").all().values_list('pk', 'name__english')

        for group_id, group_name in allocation_groups:
            user_access = False
            check_user_access = model_mixin.model_object("AllocatedUsers").filter(user_id=user_id,
                                                                                  allocation_group_id=group_id)

            if check_user_access:
                user_access = True

            project_group_list.append({
                'project': model_mixin.project,
                'allocation_group_name': group_name,
                'allocation_group_access': user_access,
                'selected_group_id': int(selected_group_id),
                'group_id': int(group_id)
            })

        user_types = User.USER_TYPE_CHOICES
        if request.user.is_client_admin:
            user_types = User.CLIENT_ADMIN_USER_TYPE_CHOICES

        if request.user.is_master_agent:
            user_types = User.MASTER_AGENT_USER_TYPE_CHOICES

        if request.user.is_agent or request.user.is_view_only_user:
            user_types = []

        context = {
            'project': model_mixin.project,
            'request': request,
            'project_group_list': project_group_list,
            'user': user,
            'user_types': user_types
        }

        html_template = "projects/admin/includes/user_groups_list.html"
        rendered = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_allocation_group(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        try:
            group_id = request.GET['group_id']
            all_exclusives = model_mixin.klass("Properties").get_all_properties_with_string_value(string_value='Exclusive Allocation Group ID',
                                                                                                  value=group_id)

            new_sync = ProjectCMSSync()
            for property_id in all_exclusives:

                model_mixin.klass("Properties").delete_property_string_field(property_id, 'Exclusive')
                model_mixin.klass("Properties").delete_property_string_field(property_id,
                                                                             'Exclusive Allocation Group ID')

                new_sync.send_delete_sync(model_mixin.project.pk, 'properties_string_fields', 'Exclusive', property_id)
                new_sync.send_delete_sync(model_mixin.project.pk, 'properties_string_fields',
                                          'Exclusive Allocation Group ID', property_id)

            model_mixin.model_object("AllocationGroups").filter(pk=group_id).delete()
            new_sync.send_delete_group_sync(model_mixin.project.pk, group_id)

        except Exception as e:
            print(e)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_edited_allocation_group_name(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)


        group_id = request.GET['group_id']
        edited_group_name = request.GET['edited_group_name']

        is_hidden = 0
        if 'is_hidden' in request.GET:
            is_hidden = str(request.GET['is_hidden']).strip()

        try:
            if not str(is_hidden) == "0":
                is_hidden = int(is_hidden)
        except:
            is_hidden = 1

        selected_parent_id = None
        if "selected_parent_id" in request.GET and request.GET['selected_parent_id']:
            selected_parent_id = request.GET['selected_parent_id']

        name = model_mixin.model_object("AllocationGroups").filter(pk=group_id).values_list('name_id', flat=True)

        new_sync = ProjectCMSSync()

        if name:
            group_name_id = name
            name = model_mixin.model_object("Names").filter(pk=group_name_id)

            if name:
                name = name[0]
                name.english = edited_group_name
                name.save(using=model_mixin.project.project_connection_name)

                new_sync.send_update_allocation_sync(model_mixin.project.pk, 'names', edited_group_name, 'value', group_id)

        group = model_mixin.model_object("AllocationGroups").filter(pk=group_id)
        group = group[0]
        group.is_hidden = is_hidden
        group.save(using=model_mixin.project.project_connection_name)

        if selected_parent_id:
            group = model_mixin.model_object("AllocationGroups").filter(pk=group_id)
            if group and not selected_parent_id == "no_parent":
                group = group[0]
                group.allocation_group_parent_id = selected_parent_id
                group.is_hidden = is_hidden
                group.save(using=model_mixin.project.project_connection_name)

                new_sync.send_update_allocation_sync(model_mixin.project.pk,
                                                     'allocation_groups',
                                                     'allocation_group_parent_id',
                                                     selected_parent_id,
                                                     group_id)

            if group and selected_parent_id == "no_parent":
                group = group[0]
                group.allocation_group_parent_id = None
                group.is_hidden = is_hidden
                group.save(using=model_mixin.project.project_connection_name)

                new_sync.send_update_allocation_sync(model_mixin.project.pk,
                                                     'allocation_groups',
                                                     'allocation_group_parent_id',
                                                     ' ', group_id)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_new_user_form(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        user = None
        form_data = QueryDict(str(request.GET['formdata']), mutable=True, encoding=request._encoding)
        user_add_form = UserCreateFromAllocationForm(request, model_mixin.project, data=form_data)

        if user_add_form.is_valid():
            try:
                user = user_add_form.save()

                if not user.created_by:
                    user.created_by = request.user
                    user.owned_by = request.user
                    user.last_modified_by = request.user
                    user.save()

            except Exception as e:
                print('user add error: ', e)

                email_address = user_add_form.cleaned_data.get('email', None)
                if email_address:
                    user = User.objects.get(email=email_address)

            if user:
                model_mixin.project.users.add(user)
                model_mixin.project.save()
                new_sync = ProjectCMSSync()

                try:
                    new_sync.add_user_into_sync(model_mixin.project.pk, user.first_name, user.last_name, user.email,
                                                user.is_super_admin)

                    new_sync.add_user_access_into_sync(model_mixin.project.pk, model_mixin.project.name, user.email,
                                                       user.can_edit_reservation(), user.can_edit_reservation())
                except Exception as e:
                    pass

                UserModifiedLog.objects.create_log(user, request.user,
                                                   "Added user access to project %s" % model_mixin.project.name)

                default_group = model_mixin.model_object("AllocationGroups").filter(name__english='Default')

                if default_group:
                    default_group = default_group[0]
                    allocate_user = model_mixin.model_object("AllocatedUsers").filter(user_id=user.pk,
                                                                                      allocation_group_id=default_group.pk)
                    if not allocate_user:
                        model_mixin.model_object("AllocatedUsers").create(user_id=user.pk,
                                                                          allocation_group_id=default_group.pk)

                        UserModifiedLog.objects.create_log(user, request.user,
                                                           "Added user access to Default allocation "
                                                           "group for project %s" % model_mixin.project.name)
                        try:
                            new_sync.send_insert_user_allocation_sync(pk, default_group.pk, user.pk)
                        except Exception as e:
                            pass

            success = True
        else:
            success = False

        context = {
            'project': model_mixin.project,
            'request': request,
            'user_add_form': user_add_form
        }

        html_template = "projects/admin/includes/add_new_user_form.html"
        rendered_form = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': success,
                'rendered_form': rendered_form
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_agent_for_reservation(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        user = None

        if "agent_user_id" in request.GET:
            agent_user_id = request.GET['agent_user_id']
            user = User.objects.get(pk=agent_user_id)

        direction = request.GET['direction']
        success = True
        form_error = None

        form = ReservationAgentForm(instance=user)

        if direction == "save":
            form_data = QueryDict(str(request.GET['formdata']), mutable=True, encoding=request._encoding)

            if user:
                form = ReservationAgentForm(data=form_data, instance=user)

                if form.is_valid():
                    try:
                        form.save()
                    except Exception as e:
                        form_error = e
                else:
                    success = False

        context = {
            'project': model_mixin.project,
            'agent_form': form,
            'form_error': form_error,
            'request': request
        }

        html_template = "projects/admin/includes/reservation_agent_form.html"
        rendered_form = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': success,
                'rendered_form': rendered_form
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_reservation_details(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']
        model_mixin.model_object("BuyerDetails").filter(property_id=property_id).delete()

        kwargs = {
            '{0}'.format('property_id'): property_id,
            '{0}'.format('type_string__english'): 'status'
        }

        property_status_field = model_mixin.model_object("PropertiesStringFields").filter(**kwargs)

        for status_field in property_status_field:
            status_field.value = 'Available'
            status_field.save(using=model_mixin.project.project_connection_name)

        new_sync = ProjectCMSSync()
        new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields',
                                  'value', 'Available', 'status', property_id)

        date_reserved_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                   'Date Reserved')

        reservation_string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                        'Reservation Status')

        for date_field in date_reserved_fields:
            date_field.value = ''
            date_field.save(using=model_mixin.project.project_connection_name)

            new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', '', 'Date Reserved',
                                      property_id)

        for status_field in reservation_string_fields:
            status_field.value = 'Pending Approval'
            status_field.save(using=model_mixin.project.project_connection_name)

            new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', 'Available',
                                      'Reservation Status',
                                      property_id)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_property_reservation(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = None
        property_name = ''
        property_type = ''

        if "property_id" in request.GET:
            property_id = request.GET['property_id']
            property_item = model_mixin.model_object("Properties").filter(pk=property_id)[0]
            property_name = \
            model_mixin.model_object("Properties").filter(pk=property_id).values_list('name__english', flat=True)[0]
            property_type = \
            model_mixin.model_object("PropertyTypes").filter(pk=property_item.property_type_id).values_list('type',
                                                                                                            flat=True)[0]

        purchaser_details = model_mixin.model_object("BuyerDetails").filter(property_id=property_id)
        if not purchaser_details:
            model_mixin.model_object("BuyerDetails").create(property_id=property_id)
            purchaser_details = model_mixin.model_object("BuyerDetails").filter(property_id=property_id)

        purchaser_details = purchaser_details[0]

        direction = request.GET['direction']
        success = True
        new_sync = ProjectCMSSync()

        if direction == "save":
            form_data = QueryDict(str(request.GET['formdata']), mutable=True, encoding=request._encoding)

            if not purchaser_details:
                model_mixin.model_object("BuyerDetails").create(property_id=property_id, agent_id=1)
                purchaser_details = model_mixin.model_object("BuyerDetails").filter(property_id=property_id)

            form = ReservationPurchaserForm(purchaser_details, model_mixin.project, request, data=form_data)

            if form.is_valid():
                purchaser = form.save(property_id)

                date_reserved_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id, 'Date Reserved')
                reservation_string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                            'Reservation Status')
                for date_field in date_reserved_fields:

                    date_field.value = datetime.datetime.now()
                    date_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', datetime.datetime.now(),
                                              'Date Reserved', property_id)

                for status_field in reservation_string_fields:
                    status_field.value = 'Pending Approval'
                    status_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', 'Pending Approval',
                                              'Reservation Status', property_id)

                form.send_property_reservation_email(request.user.pk,
                                                      project_id=model_mixin.project.pk,
                                                      property_name=property_name,
                                                      property_type=property_type)
            else:
                print('ERROR', form.errors)
                success = False

        else:
            if purchaser_details:
                try:
                    form = ReservationPurchaserForm(purchaser_details, model_mixin.project, request)
                except Exception as e:
                    form = None
                    print("Error", e)
            else:
                form = ReservationPurchaserForm(None, model_mixin.project, request,
                                                initial={'property_id': property_id})

        context = {
            'project': model_mixin.project,
            'form': form,
            'property_name': property_name,
            'selected_property_id': property_id,
            'request': request
        }

        html_template = "projects/admin/includes/reservation_modal.html"
        rendered_form = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': success,
                'rendered_form': rendered_form
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_to_reserved(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_id = request.GET['property_id']

        purchaser_details = model_mixin.model_object("BuyerDetails").filter(property_id=property_id)
        if not purchaser_details:
            model_mixin.model_object("BuyerDetails").create(property_id=property_id, agent_id=request.user.pk)

        else:
            purchaser_details = purchaser_details[0]
            purchaser_details.agent_id = request.user.pk
            purchaser_details.save(using=model_mixin.project.project_connection_name)

        string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id, 'status')
        reservation_string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                        'Reservation Status')
        date_reserved_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                   'Date Reserved')

        reserved = None
        new_sync = ProjectCMSSync()

        for string_field in string_fields:
            if string_field.value == 'Reserved':
                string_field.value = 'Available'
                reserved = False

                for date_field in date_reserved_fields:
                    date_field.value = ' '
                    date_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', ' ',
                                              'Date Reserved', property_id)

                for status_field in reservation_string_fields:
                    status_field.value = ' '
                    status_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', ' ',
                                              'Reservation Status', property_id)

                new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', 'Available',
                                          'status', property_id)
            else:
                string_field.value = 'Reserved'
                reserved = True

                for date_field in date_reserved_fields:
                    date_field.value = datetime.datetime.now()
                    date_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', datetime.datetime.now(),
                                              'Date Reserved', property_id)

                for status_field in reservation_string_fields:
                    status_field.value = 'Pending Approval'
                    status_field.save(using=model_mixin.project.project_connection_name)

                    new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', 'Pending Approval',
                                              'Reservation Status', property_id)

                new_sync.send_update_sync(model_mixin.project.pk, 'properties_string_fields', 'value', 'Reserved',
                                          'status', property_id)

            string_field.save(using=model_mixin.project.project_connection_name)

        data = json.dumps(
            {
                'success': True,
                'reserved': reserved
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_property_to_exclusive(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        new_sync = ProjectCMSSync()

        group_id = request.GET['group_id']
        property_id = request.GET['property_id']

        string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id, 'Exclusive')

        for string_field in string_fields:
            string_field.value = 'True'
            string_field.save(using=model_mixin.project.project_connection_name)

        string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(property_id, 'Exclusive Allocation Group ID')
        for string_field in string_fields:
            string_field.value = group_id
            string_field.save(using=model_mixin.project.project_connection_name)

        new_sync.send_insert_sync(model_mixin.project.pk, 'properties_string_fields', 'value', property_id, 'True', 'Exclusive')
        new_sync.send_insert_sync(model_mixin.project.pk, 'properties_string_fields', 'value', property_id, group_id,
                                  'Exclusive Allocation Group ID')

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_from_exclusive(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        group_id = request.GET['group_id']
        property_id = request.GET['property_id']

        model_mixin.klass("Properties").delete_property_string_field(property_id, 'Exclusive')
        model_mixin.klass("Properties").delete_property_string_field(property_id, 'Exclusive Allocation Group ID')

        new_sync = ProjectCMSSync()
        new_sync.send_delete_sync(model_mixin.project.pk, 'properties_string_fields', 'Exclusive', property_id)
        new_sync.send_delete_sync(model_mixin.project.pk, 'properties_string_fields', 'Exclusive Allocation Group ID', property_id)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_data_comparison_csv_import(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        row_id = request.GET['row_id']
        preview_id = request.GET['preview_id']
        template_id = request.GET['template_id']

        key_list = []
        found_property_table_rendered = '<em>Error finding data</em>'
        property_list = []

        template_fields = ProjectCSVTemplateFieldValues.objects.filter(row_id=int(row_id),
                                                                       import_instance__pk=preview_id,
                                                                       field__column_table_mapping='Properties',
                                                                       field__column_field_mapping__iexact='floor')
        if template_fields:
            floor = str(template_fields[0].value).strip()

            try:
                floor = int(float(floor))
            except:
                pass

            template_fields = ProjectCSVTemplateFieldValues.objects.filter(row_id=int(row_id),
                                                                           import_instance__pk=preview_id,
                                                                           field__column_table_mapping='Properties',
                                                                           field__column_field_mapping__iexact='name')

            if template_fields:
                property_name = str(template_fields[0].value).strip()
                kwargs = {
                    '{0}'.format('name__english__icontains'): property_name,
                    '{0}'.format('floor__name__english__iexact'): floor
                }

                properties = model_mixin.model_object("Properties").filter(**kwargs)

                if properties:
                    for property_item in properties:
                        property_fields_values = {}
                        editable_template = ProjectCSVTemplate.objects.get(pk=template_id)
                        template_fields = ProjectCSVTemplateFields.objects.filter(template=editable_template).order_by(
                            'order')

                        for field in template_fields:
                            # print ('-- field', field)
                            value = "<em>n/a</em>"

                            if field.column_field_mapping == "building_name":
                                floor_buildings = model_mixin.model_object("FloorsBuildings").filter(
                                    floor_id=property_item.floor_id)
                                # print 'floor_buildings', floor_buildings
                                value = model_mixin.model_object("Buildings").filter(
                                    pk=floor_buildings[0].building_id).values_list('name__english', flat=True)[0]

                                # print 'value: ', value

                            else:
                                try:
                                    property_values = model_mixin.klass("Properties").get_apartment_strings_type_value(
                                        field.column_table_mapping,
                                        field.column_field_mapping,
                                        property_item.pk)

                                    if property_values:
                                        property_value_dict = model_to_dict(property_values)
                                        if "type_string" in property_value_dict:
                                            value = property_value_dict["value"]
                                        else:
                                            get_list_value = property_value_dict[field.column_field_mapping]
                                            check_foreign_key, value = model_mixin.get_model_field_mapping_and_value(
                                                field.column_table_mapping,
                                                field.column_field_mapping,
                                                field_value=get_list_value)

                                        try:
                                            value = str(value).strip()
                                        except:
                                            pass

                                        if not value or value in ["None", ""]:
                                            # print 'setting to NA'
                                            value = "<em>n/a</em>"
                                except Exception as e:
                                    # print 'error: setting to NA'
                                    print('some error', e)
                                    value = "<em>n/a</em>"

                            if field.column_field_mapping not in key_list and not str(
                                    field.column_field_mapping).strip() == "New Field:":
                                key_list.append(field.column_field_mapping)

                            property_fields_values.update({field.column_field_mapping: value})

                        property_list.append(
                            {'property_item': property_item, 'property_fields_values': property_fields_values})

                    context = {
                        'project': model_mixin.project,
                        'property_list': property_list,
                        'key_list': key_list,
                        'request': request
                    }

                    html_template = "projects/import/includes/csv_database_comparison_table.html"
                    found_property_table_rendered = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': True,
                'found_property_table_rendered': found_property_table_rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_csv_import_details(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        key_value = request.GET['key_value']
        new_value = request.GET['new_value']
        row_id = request.GET['row_id']
        preview_id = request.GET['preview_id']

        template_fields = ProjectCSVTemplateFieldValues.objects.filter(row_id=int(row_id),
                                                                       import_instance__pk=preview_id,
                                                                       field__column_field_mapping__iexact=key_value)

        if template_fields and new_value:
            template_field = template_fields[0]
            template_field.value = new_value
            template_field.save()

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_project_data_allocation(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        key_value = request.GET['key_value']

        new_value = request.GET['new_value']

        if new_value:
            new_value = str(new_value).strip()

        property_id = request.GET['property_id']
        template_id = request.GET['allocation_template_id']

        if key_value == "ReservationStatus":
            key_value = "Reservation Status"

        if key_value == "status":
            if new_value == "Reserved":
                try:
                    model_mixin.klass("Properties").set_property_status_reserved_wsync(property_id)
                except Exception as e:
                    print(e)
            elif new_value == "Available":
                try:
                    model_mixin.klass("Properties").set_property_status_available_wsync(property_id)
                except Exception as e:
                    print(e)

        template_fields = ProjectCSVTemplateFields.objects.filter(template_id=template_id,
                                                                  column_field_mapping__iexact=key_value)

        new_sync = ProjectCMSSync()
        for field in template_fields:
            type_string = model_mixin.model_object("Strings").filter(english=key_value)

            if not type_string:
                type_string = model_mixin.model_object("Strings").filter(english=key_value)

            for string_field in type_string:
                property_items = model_mixin.model_object(field.column_table_mapping).filter(property_id=property_id,
                                                                                           type_string=string_field)

                for item_current in property_items:
                    item_current.value = new_value
                    item_current.save(using=model_mixin.project.project_connection_name)

                    table_model = get_model(model_mixin.project.project_connection_name, field.column_table_mapping)
                    table_details = table_model._meta.db_table

                    new_sync.send_update_sync(model_mixin.project.pk, table_details,
                                              'value', new_value, key_value, property_id)

                if not property_items:
                    model_mixin.model_object(field.column_table_mapping).create(property_id=property_id,
                                                                                type_string=string_field,
                                                                                value=new_value)


        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_allocations_property_template(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        template_id = request.GET['template_id']
        apartments = model_mixin.model_object("Properties").all()[:10]

        editable_template = ProjectCSVTemplate.objects.get(pk=template_id)

        property_forms = []
        for apartment in apartments:
            get_form_class = get_allocations_property_formset(model_mixin.project, model_mixin, apartment)

            IngredientFormSet = inlineformset_factory(ProjectCSVTemplate, ProjectCSVTemplateFields,
                                                      form=get_form_class,
                                                      extra=0)

            ingredient_formset = IngredientFormSet(instance=editable_template,
                                                   queryset=editable_template.project_template_fields.order_by("order"))

            context = {
                'project': model_mixin.project,
                'template_columns_form': ingredient_formset,
                'request': request
            }

            html_template = "projects/admin/includes/allocation_properties.html"
            property_table_rendered = render_to_string(html_template, context)
            property_forms.append(
                {'property_id': apartment.pk, 'allocation_property_template': property_table_rendered})

        data = json.dumps(
            {
                'success': True,
                'property_forms': property_forms
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_allocated_search_results(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        property_selected = request.GET['property_selected']
        property_type_selected = request.GET['property_type_selected']
        level_selected = request.GET['level_selected']
        building_selected = request.GET['building_selected']
        user_selected = request.GET['user_selected']

        properties_with_type = []
        property_with_name = []
        property_with_level = []
        property_with_building = []
        property_allocations = []
        user_allocations = []

        if property_type_selected:
            properties_with_type = model_mixin.model_object("Properties").filter(
                property_type_id=property_type_selected).values_list('pk', flat=True)

        if property_selected:
            name = model_mixin.model_object("Names").filter(english__iexact=property_selected).values_list('pk',
                                                                                                           flat=True)
            if name:
                property_with_name = model_mixin.model_object("Properties").filter(name_id__in=name)

        if level_selected:
            level_name = model_mixin.model_object("Names").filter(english=level_selected).values_list('pk', flat=True)
            if level_name:
                levels = model_mixin.model_object("Floors").filter(name_id__in=level_name).values_list('pk', flat=True)
                if levels:
                    property_with_level = model_mixin.model_object("Properties").filter(floor_id__in=levels)

        if building_selected:
            building_name = model_mixin.model_object("Names").filter(english=building_selected).values_list('pk',
                                                                                                            flat=True)
            if building_name:
                buildings = model_mixin.model_object("Buildings").filter(name_id__in=building_name).values_list('pk',
                                                                                                                flat=True)
                if buildings:
                    floors_with_building = model_mixin.model_object("FloorsBuildings").filter(
                        building_id__in=buildings).values_list('floor_id', flat=True)
                    if floors_with_building:
                        levels = model_mixin.model_object("Floors").filter(pk__in=floors_with_building).values_list(
                            'pk', flat=True)
                        if levels:
                            property_with_building = model_mixin.model_object("Properties").filter(floor_id__in=levels)

        property_result_list = list(
            chain(properties_with_type, property_with_name, property_with_level, property_with_building))
        property_allocations = model_mixin.model_object("PropertyAllocations").filter(
            property_id__in=property_result_list).values_list('allocation_group_id', flat=True)

        if user_selected:
            user_allocations = model_mixin.model_object("AllocatedUsers").filter(user_id=user_selected).values_list(
                'allocation_group_id', flat=True)

        goup_result_list = list(chain(user_allocations, property_allocations))
        allocation_groups = model_mixin.model_object("AllocationGroups").filter(pk__in=goup_result_list)

        allocation_group_list = []
        for group in allocation_groups:
            allocation_group_list.append({'group': group,
                                          'apartment_cout': model_mixin.model_object("PropertyAllocations").filter(
                                              allocation_group=group)})

        context = {'project': model_mixin.project,
                   'model_mixin': model_mixin,
                   'allocation_groups': allocation_groups,
                   'allocation_group_list': allocation_group_list,
                   'request': request}

        html_template = "projects/admin/includes/allocated_groups.html"
        rendered = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_property_to_allocation_group(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        new_sync = ProjectCMSSync()

        allocation_group_id = request.GET['group_id']
        property_id = request.GET['property_id']

        properties = model_mixin.model_object("PropertyAllocations").filter(allocation_group_id=allocation_group_id,
                                                                            property_id=property_id)

        if not properties:
            model_mixin.model_object("PropertyAllocations").create(allocation_group_id=allocation_group_id,
                                                                   property_id=property_id)

            new_sync.send_insert_allocation_sync(model_mixin.project.pk, 'property_allocations', allocation_group_id, property_id)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_all_properties(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        allocation_template_id = request.GET['allocation_template_id']
        template_fields = ProjectCSVTemplateFields.objects.filter(template_id=allocation_template_id).order_by('order')

        floor_id = None
        building_id = None
        all_prop_rendered = ''
        all_prop_rendered_editable = ''
        reservation_properties_rendered = ''

        group_id = None
        if "group_id" in request.GET and request.GET['group_id']:
            group_id = request.GET['group_id']
            group_id = int(group_id)

        floor_name = None
        if "floor_name" in request.GET and request.GET['floor_name'] and not request.GET['floor_name'] in ["all", "[]"]:
            floor_name = request.GET['floor_name']

        building_name = None
        if "building_name" in request.GET and request.GET['building_name']:
            if request.GET['building_name'] == "all":
                building_name = None
            else:
                building_name = request.GET['building_name']

        if request.GET['building_name'] == "all":
            building_name = None

        if floor_name and not building_name:
            if not floor_name == "all":
                kwargs = {
                    '{0}'.format('name__english__iexact'): floor_name
                }

                floors = model_mixin.model_object("Floors").filter(**kwargs).values_list('pk',
                                                                                         flat=True).order_by('name__english')

                allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                         floors=floors,
                                                                                                         request=request,
                                                                                                         group_id=group_id)


            else:
                allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                         request=request,
                                                                                                         group_id=group_id)

            all_prop_rendered = allocation_details['all_prop_rendered']
            all_prop_rendered_editable = allocation_details['all_prop_rendered_editable']

            try:
                reservation_properties_rendered = allocation_details['reservation_properties_rendered']
            except Exception as e:
                print('getting reservation error: ', e)

        if building_name and not floor_name:
            kwargs = {
                '{0}'.format('name__english__iexact'): building_name
            }

            building = model_mixin.model_object("Buildings").filter(**kwargs)[0]
            building_id = building.pk

            allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                     building_id=building.pk,
                                                                                                     request=request,
                                                                                                     group_id=group_id)

            all_prop_rendered = allocation_details['all_prop_rendered']
            all_prop_rendered_editable = allocation_details['all_prop_rendered_editable']
            try:
                reservation_properties_rendered = allocation_details['reservation_properties_rendered']
            except Exception as e:
                print('getting reservation error: ', e)

        try:
            if building_name and floor_name:
                kwargs = {
                    '{0}'.format('name__english__iexact'): building_name
                }

                building = model_mixin.model_object("Buildings").filter(**kwargs)[0]
                building_id = building.pk
                floors = None

                if not floor_name == "all":
                    floors = model_mixin.model_object("FloorsBuildings")\
                        .filter(building_id=building_id, floor__name__english=floor_name).values_list('floor_id', flat=True).order_by('floor__orderidx')



                    try:
                        floor_id = floors[0]
                    except Exception as e:
                        print('error marking floor_id', e, 'floors', floors)

                allocation_details = model_mixin.klass("AllocationGroups").get_allocation_all_properties(template_fields,
                                                                                                         floors=floors,
                                                                                                         building_id=building.pk,
                                                                                                         request=request,
                                                                                                         group_id=group_id)

                all_prop_rendered = allocation_details['all_prop_rendered']
                all_prop_rendered_editable = allocation_details['all_prop_rendered_editable']
                try:
                    reservation_properties_rendered = allocation_details['reservation_properties_rendered']
                except Exception as e:
                    print('getting reservation error: ', e)

        except Exception as e:
            print('error', e)

        try:
            data = json.dumps(
                {
                    'success': True,
                    'all_prop_rendered': all_prop_rendered,
                    'all_prop_rendered_editable': all_prop_rendered_editable,
                    'reservation_properties_rendered': reservation_properties_rendered,
                    'reservation_key_display_list': reservation_properties_rendered,
                    'floor': floor_id,
                    'building_id': building_id
                }
            )
        except Exception as e:
            print(e)

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_allocated_properties(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        allocation_group = request.GET['group_id']
        allocation_template_id = request.GET['allocation_template_id']

        floor_name = None
        if "floor_name" in request.GET and request.GET['floor_name']:
            floor_name = request.GET['floor_name']
            if floor_name == "all":
                floor_name = None

        selected_building = None
        if "selected_building" in request.GET and request.GET['selected_building']:
            selected_building = request.GET['selected_building']

        if allocation_group:
            allocated_properties = model_mixin.model_object("PropertyAllocations").filter(
                allocation_group_id=allocation_group).order_by('property__name__english')
        else:
            allocated_properties = model_mixin.model_object("PropertyAllocations").all().order_by(
                'property__name__english')

        if floor_name and allocated_properties:
            allocated_properties = allocated_properties.filter(property__floor__name__english=floor_name)
            if selected_building and allocated_properties:
                floor_buildings = model_mixin.model_object("FloorsBuildings").filter(building__name__english=selected_building)
                if floor_buildings:
                    floor_buildings = floor_buildings.filter(floor__name__english=floor_name)
                    floor_buildings = floor_buildings[0]
                    allocated_properties = allocated_properties.filter(property__floor_id=floor_buildings.floor_id)

        template_fields = ProjectCSVTemplateFields.objects.filter(template_id=allocation_template_id).order_by('order')
        key_list = []
        property_preview_list = []

        for allocation in allocated_properties:
            property_item = model_mixin.model_object("Properties").get(pk=allocation.property_id)
            property_preview, data_list, key_list = model_mixin.klass("Floors").get_select_preview_floor_dictionary(property_item,
                                                                                                                    template_fields)

            property_exclusive = "False"
            check_property_exclusive = model_mixin.klass("Properties").get_or_create_property_string_field(property_item.pk, 'Exclusive')

            if check_property_exclusive:
                property_exclusive = check_property_exclusive[0].value
                if not property_exclusive:
                    property_exclusive = "False"

            property_exclusive_group = None
            check_property_exclusive_group = model_mixin.klass("Properties").get_or_create_property_string_field(property_item.pk,
                                                                                                                 'Exclusive Allocation Group ID')
            if check_property_exclusive_group:
                property_exclusive_group = check_property_exclusive_group[0].value

                check_group = model_mixin.model_object("AllocationGroups").filter(pk=property_exclusive_group)

                if not check_group:
                    model_mixin.klass("Properties").delete_property_string_field(property_item, 'Exclusive')
                    model_mixin.klass("Properties").delete_property_string_field(property_item,
                                                                                 'Exclusive Allocation Group ID')

            property_allocations = model_mixin.model_object("PropertyAllocations").filter(property=property_item).values_list('allocation_group__name__english', flat=True)

            assign = True
            if property_exclusive_group and property_exclusive_group == allocation_group:
                assign = True
            elif property_exclusive_group and not property_exclusive_group == allocation_group:
                assign = False
            elif not property_exclusive_group:
                assign = True

            show_prop_in_all = True
            if allocation_group in property_allocations:
                show_prop_in_all = False

            if assign:
                property_dict = {'property': property_item,
                                 'property_values': property_preview,
                                 'property_exclusive': property_exclusive,
                                 'property_exclusive_group': property_exclusive_group,
                                 'property_allocations': property_allocations,
                                 'show_prop_in_all': show_prop_in_all,
                                 'request': request}

                property_preview_list.append(property_dict)

        key_display_list = []
        if key_list:
            key_display_list = [str(x).replace("_", " ") for x in key_list]

        context = {'project': model_mixin.project,
                   'key_display_list': key_display_list,
                   'key_list': key_list,
                   'property_preview_list': property_preview_list,
                   'request': request}

        html_template = "projects/admin/includes/allocated_properties.html"
        rendered = render_to_string(html_template, context)

        html_template = "projects/admin/includes/all_properties.html"
        all_rendered = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered,
                'all_rendered': all_rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def add_property_to_allocation_group(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        new_sync = ProjectCMSSync()

        allocation_group_id = request.GET['group_id']
        property_id = request.GET['added_property_id']

        properties = model_mixin.model_object("PropertyAllocations").filter(allocation_group_id=allocation_group_id,
                                                                            property_id=property_id)

        if not properties:
            model_mixin.model_object("PropertyAllocations").create(allocation_group_id=allocation_group_id,
                                                                   property_id=property_id)

            new_sync.send_insert_allocation_sync(model_mixin.project.pk, 'property_allocations', allocation_group_id, property_id)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_property_from_allocation_group(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        allocation_group = request.GET['group_id']
        removed_property_id = request.GET['removed_property_id']
        model_mixin.model_object("PropertyAllocations").filter(allocation_group_id=allocation_group,
                                                               property_id=removed_property_id).delete()

        new_sync = ProjectCMSSync()
        new_sync.send_delete_allocation_sync(model_mixin.project.pk, removed_property_id, allocation_group)

        string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(removed_property_id,
                                                                                            'Exclusive')
        for string_field in string_fields:
            string_field.value = None
            string_field.save(using=model_mixin.project.project_connection_name)

            new_sync.send_delete_sync(model_mixin.project.pk, 'properties_string_fields',
                                      'Exclusive', removed_property_id)

        string_fields = model_mixin.klass("Properties").get_or_create_property_string_field(removed_property_id,
                                                                        'Exclusive Allocation Group ID')

        for string_field in string_fields:
            string_field.value = None
            string_field.save(using=model_mixin.project.project_connection_name)

            new_sync.send_delete_sync(model_mixin.project.pk,
                                      'properties_string_fields',
                                      'Exclusive Allocation Group ID',
                                      removed_property_id)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_user_to_allocation_group(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        new_sync = ProjectCMSSync()

        direction = None
        if "direction" in request.GET:
            direction = request.GET['direction']
        allocation_group = request.GET['group_id']
        user_id = request.GET['user_id']

        users = model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                                  user_id=user_id)

        if direction:
            if direction == "add" and not users:
                model_mixin.model_object("AllocatedUsers").create(allocation_group_id=allocation_group,
                                                                  user_id=user_id)

                new_sync.send_insert_user_allocation_sync(model_mixin.project.pk, allocation_group, user_id)

            if direction == "remove":
                model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                                  user_id=user_id).delete()

                new_sync.send_delete_user_allocation_sync(model_mixin.project.pk, allocation_group, user_id)
        else:
            if not users:
                model_mixin.model_object("AllocatedUsers").create(allocation_group_id=allocation_group, user_id=user_id)
                new_sync.send_insert_user_allocation_sync(model_mixin.project.pk, allocation_group, user_id)

            else:
                model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                                  user_id=user_id).delete()

                new_sync.send_delete_user_allocation_sync(model_mixin.project.pk, allocation_group, user_id)

        user = User.objects.get(pk=user_id)
        model_mixin.project.users.add(user)
        model_mixin.project.save()

        data = json.dumps(
            {
                'success': True,
                # 'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_user_from_allocation_group(request, pk):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        allocation_group = request.GET['group_id']
        user_id = request.GET['user_id']

        new_sync = ProjectCMSSync()

        model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                          user_id=user_id).delete()

        new_sync.send_delete_user_allocation_sync(model_mixin.project.pk, allocation_group, user_id)

        data = json.dumps(
            {
                'success': True,
                # 'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_allocated_users(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if "group_id" in request.GET and request.GET['group_id']:
            allocation_group = request.GET['group_id']
        else:
            allocation_group = model_mixin.model_object("AllocationGroups").all()[0]

        group_name = None
        if "group_name" in request.GET and request.GET['group_name']:
            group_name = request.GET['group_name']

        users = request.user.viewable_users.filter(project_users_list=model_mixin.project)

        allocated_users = []
        for user in users:
            if allocation_group:
                allocated = model_mixin.model_object("AllocatedUsers").filter(allocation_group_id=allocation_group,
                                                                              user_id=user.pk)

                if user and user not in allocated_users:
                    is_allocated = False
                    if allocated:
                        is_allocated = True

                    allocated_users.append({'user': user, 'is_allocated': is_allocated})

        view_filter_form = True
        user_types = User.USER_TYPE_CHOICES
        if request.user.is_client_admin:
            user_types = User.CLIENT_ADMIN_USER_TYPE_CHOICES

        if request.user.is_master_agent:
            user_types = User.MASTER_AGENT_USER_TYPE_CHOICES

        if request.user.is_agent or request.user.is_view_only_user:
            user_types = []
            view_filter_form = False

        context = {'allocated_users': allocated_users,
                   'group_name': group_name,
                   'request': request,
                   'user_types': user_types,
                   'view_filter_form': view_filter_form
                   }

        html_template = "projects/admin/includes/allocated_users.html"
        rendered = render_to_string(html_template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
