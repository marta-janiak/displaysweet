import json
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from users.models import User
from projects.forms import ProjectAddForm
from core.models import Project, ProjectFamily
from projects.forms import ProjectVersionCommentForm, ProjectScriptsForm, AddProjectTranslation
from projects.models.model_mixin import PrimaryModelMixin
from utils.revision_helpers import add_new_revision


def remove_project_family(request):
    if request.is_ajax():

        family_id = None
        if "family_id" in request.GET and request.GET['family_id']:
            family_id = request.GET['family_id']

        if family_id:
            family = ProjectFamily.objects.get(pk=family_id)

            for project in family.project_list.all():
                family.project_list.remove(project)
                family.save()

            family.delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_projects_to_family(request):
    if request.is_ajax():

        created = 0

        family_id = None
        if "family_id" in request.GET and request.GET['family_id']:
            family_id = request.GET['family_id']

        family_name = None
        if "family_name" in request.GET and request.GET['family_name']:
            family_name = request.GET['family_name']

        if not family_id and family_name:
            family = ProjectFamily.objects.create(family_name=family_name)
            created = 1
        else:
            family = ProjectFamily.objects.get(pk=family_id)
            family.family_name = family_name
            family.save()

        project_list = None
        if "project_list[]" in request.GET:
            project_list = request.GET.getlist('project_list[]')

        if project_list:
            for project in project_list:
                family.project_list.add(project)
                family.save()

        data = json.dumps(
            {
                'success': True,
                'created': created,
                'family_id': family.pk
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_projects_from_family(request):
    if request.is_ajax():

        family_id = None
        if "family_id" in request.GET and request.GET['family_id']:
            family_id = request.GET['family_id']

        family_name = None
        if "family_name" in request.GET and request.GET['family_name']:
            family_name = request.GET['family_name']

        if not family_id and family_name:
            family = ProjectFamily.objects.create(family_name=family_name)
        else:
            family = ProjectFamily.objects.get(pk=family_id)
            family.family_name = family_name
            family.save()

        project_list = None
        if "project_list[]" in request.GET:
            project_list = request.GET.getlist('project_list[]')

        if project_list:
            for project in project_list:
                family.project_list.remove(project)
                family.save()


        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_new_project_version(request, pk):
    if request.is_ajax():

        family = ProjectFamily.objects.get(pk=pk)
        active_projects = family.project_list.filter(active=True, status__in=[Project.STAGING, Project.LIVE])

        all_projects = family.project_list.all().order_by('-version')
        ordered_all_projects = family.project_list.all().order_by('-version', '-status')

        model_mixin = PrimaryModelMixin(request=request, project_id=family.latest.pk)

        comment_form = ProjectVersionCommentForm(initial={'project': model_mixin.project, 'created_by': request.user,
                                                          'version': model_mixin.project.version})

        scripts_form = ProjectScriptsForm(initial={'project': model_mixin.project, 'created_by': request.user,
                                                   'version': model_mixin.project.version})

        translation_form = AddProjectTranslation(model_mixin, family,
                                                 initial={'project': model_mixin.project, 'created_by': request.user,
                                                          'version': model_mixin.project.version})
        context = {
            'project': model_mixin.project,
            'family': family,
            'all_projects': all_projects,
            'ordered_all_projects': ordered_all_projects,
            'all_scripts': family.all_scripts,
            'all_logged_changes': family.all_logged_changes,
            'all_comments': family.all_comments,
            'active_projects': active_projects,
            'comment_form': comment_form,
            'scripts_form': scripts_form,
            'users': User.objects.filter(project_users_list=model_mixin.project),
            'user_types': User.USER_TYPE_CHOICES,
            'project_statuses': Project.STATUS_OPTIONS,
            'translation_form': translation_form,
            'form': ProjectAddForm()
        }

        template = "projects/version/includes/project_details.html"
        rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


@csrf_exempt
def save_version_script(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if request.method == "POST":
            scripts_form = ProjectScriptsForm(request.POST, request.FILES)
            if scripts_form.is_valid():
                scripts_form.save()

                add_new_revision(model_mixin.project, request, Project,
                                 "New script for %s loaded" % model_mixin.project.name)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_translation_form_for_project(request, pk, family_id):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        family = ProjectFamily.objects.get(pk=family_id)

        translation_form = AddProjectTranslation(model_mixin, family,
                                                 initial={'project': model_mixin.project,
                                                          'created_by': request.user,
                                                          'version': model_mixin.project.version})
        context = {
            'project': model_mixin.project,
            'translation_form': translation_form,
            'family': family
        }

        template = "projects/version/includes/translation_form.html"
        rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)



@csrf_exempt
def save_project_translation(request, pk, family_id):
    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        translation_message = ''

        family = ProjectFamily.objects.get(pk=family_id)

        if request.method == "POST":
            translation_form = AddProjectTranslation(model_mixin, family, request.POST, request.FILES)
            if translation_form.is_valid():
                try:
                    translation_saved, error_message = translation_form.save()
                    translation_message += error_message
                    add_new_revision(model_mixin.project, request, Project,
                                     "New translation for %s created" % model_mixin.project.name)

                except Exception as e:
                    print('error saving form', e)
                    translation_message = str(e)

                    add_new_revision(model_mixin.project, request, Project,
                                     "New translation for %s failed" % model_mixin.project.name)
            else:
                print('---errors', translation_form.errors)

        translation_form = AddProjectTranslation(model_mixin, family,
                                                 initial={'project': model_mixin.project,
                                                          'created_by': request.user,
                                                          'version': model_mixin.project.version})
        context = {
            'project': model_mixin.project,
            'translation_form': translation_form,
            'family': family
        }

        template = "projects/version/includes/translation_details.html"
        rendered = render_to_string(template, context)


        data = json.dumps(
            {
                'success': True,
                'translation_message': translation_message,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


@csrf_exempt
def start_version_translation(request, pk, family_id):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        translation_message = ''

        family = ProjectFamily.objects.get(pk=family_id)

        if request.method == "POST":
            translation_form = AddProjectTranslation(model_mixin, family, request.POST, request.FILES)
            if translation_form.is_valid():

                try:
                    translation, translation_message = translation_form.save()
                    if not translation_message:
                        translation, translation_message = translation_form.run_translation(translation)

                    add_new_revision(model_mixin.project, request, Project,
                                     "New translation for %s created" % model_mixin.project.name)

                except Exception as e:
                    print(e)
                    translation_message = str(e)

                    add_new_revision(model_mixin.project, request, Project,
                                     "New translation for %s failed" % model_mixin.project.name)
            else:
                print('---errors', translation_form.errors)

        translation_form = AddProjectTranslation(model_mixin, family,
                                                 initial={'project': model_mixin.project, 'created_by': request.user,
                                                          'version': model_mixin.project.version})

        context = {
            'project': model_mixin.project,
            'translation_form': translation_form,
            'family': family
        }

        template = "projects/version/includes/translation_details.html"
        rendered = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'translation_message': translation_message,
                'rendered': rendered
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


@csrf_exempt
def save_version_comment(request, pk):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        if request.method == "POST":
            comment_form = ProjectVersionCommentForm(request.POST)
            if comment_form.is_valid():
                comment_form.save()

                add_new_revision(model_mixin.project, request, Project,
                                 "New comment for %s added" % model_mixin.project.name)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
