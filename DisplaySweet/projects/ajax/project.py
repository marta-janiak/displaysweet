import json
from multiprocessing import Queue
import os
import time
import datetime

from django.conf import settings
from django.http import HttpResponse

from django.core import serializers

from ..forms import apply_form_filter
from django.template.loader import render_to_string
from core.models import ProjectImagePath, ProjectScripts

from users.models import User, UserModifiedLog
from ..permissions import MyThread
from projects.forms import ProjectAddForm
from core.models import Project, ProjectConnection, ProjectVersion, ProjectFamily
from ..cms_sync import ProjectCMSSync
from utils.revision_helpers import add_new_revision
from projects.project_script_load import PixileScriptDbUpdater
from projects.models.model_mixin import PrimaryModelMixin


def save_user_to_project(request):
    if request.is_ajax():
        user_id = request.GET['user_id']
        project_id = request.GET['project_id']

        user = User.objects.get(pk=user_id)

        project = Project.objects.get(pk=project_id)
        project.users.add(user)
        project.save()

        try:
            user.send_added_to_project_email(project)
        except Exception as e:
            print ('error sending email', e)

        new_sync = ProjectCMSSync()
        new_sync.add_user_access_into_sync(project.pk, project.name, user.email,
                                           user.can_edit_reservation(), user.can_edit_reservation())

        UserModifiedLog.objects.create_log(user, request.user,
                                           "Added user access to project %s" % project.name)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_user_to_project_without_sync(request):
    if request.is_ajax():
        user_id = request.GET['user_id']
        project_id = request.GET['project_id']

        user = User.objects.get(pk=user_id)
        project = Project.objects.get(pk=project_id)
        project.users.add(user)
        project.save()

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_user_from_project(request):
    if request.is_ajax():
        user_id = request.GET['user_id']
        project_id = request.GET['project_id']

        user = User.objects.get(pk=user_id)
        project = Project.objects.get(pk=project_id)
        project.users.remove(user)

        if "sync" in request.GET:
            new_sync = ProjectCMSSync()
            new_sync.remove_user_access_from_sync(project.pk, project.name, user.email)

            UserModifiedLog.objects.create_log(user, request.user,
                                               "Removed user access to project %s" % project.name)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)






def import_images_ns(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        data = json.dumps({'success': False})

        if "import_image_path" in request.POST:
            path_selected = request.POST['import_image_path']
            path_selected = path_selected.split("_|_")
            path_name = path_selected[0]
            file_location = path_selected[1]

            base_file_location = settings.BASE_IMAGE_LOCATION
            file_location = '%s/%s/' % (base_file_location, file_location)
            file_location = file_location.replace('//', '/')

            if request.FILES and request.FILES.getlist('floor_images'):
                if not os.path.isdir(file_location):
                    os.makedirs(file_location)

                for item in request.FILES.getlist('floor_images'):
                    saving_location = '%s%s' % (file_location, str(item.name))
                    saving_location = saving_location.replace("/project_data/project_data/", "/project_data/")
                    with open(saving_location, 'wb+') as destination:
                        for chunk in item.chunks():
                            destination.write(chunk)

            model_mixin.project.save()

            data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_image_path(request, pk, path_type):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        data = json.dumps({'success': False})
        if "image_plan_path" in request.POST and request.POST['image_plan_path']:
            path = request.POST['image_plan_path']

            if path_type == 'level':
                model_mixin.project.level_plan_location = path
            elif path_type == 'floor':
                model_mixin.project.floor_plan_location = path
            elif path_type == 'key':
                model_mixin.project.key_plan_location = path

        check_path = '%s-image_plan_path' % path_type
        if check_path in request.POST:
            image_types_queryset = model_mixin.model_object('Imagetypes').all()
            image_type_choices = []

            for type in image_types_queryset:
                if type.type_string_id:
                    string_field = model_mixin.model_object('Strings').filter(type_string_id=type.type_string_id)
                    if string_field and not string_field.english in image_type_choices:
                        image_type_choices.append(string_field.english)

            if path_type in image_type_choices:
                image_path = ProjectImagePath.objects.filter(project=model_mixin.project, image_type=path_type)
                if image_path:
                    image_path[0].image_path = request.POST[check_path]
                    image_path[0].save()

            model_mixin.project.save()
            data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def save_image_name(request, pk, floor_id, image_type):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        data = json.dumps({'success': False})
        if 'floor_of_image_name' in request.GET:
            floor_of_image_name = request.GET['floor_of_image_name']
            image_name = request.GET['image_name']

            if image_type in ['level', 'level_full_res']:
                floor = model_mixin.model_object("Floors").get(pk=floor_of_image_name)
                # image = []
                #
                # floor_plan_path = image.highres_path
                # floor_plan_path_split = floor_plan_path.split("/")
                # current_image_name = floor_plan_path_split[-1:][0]
                #
                # image_name = floor_plan_path.replace(current_image_name, image_name)
                # image.highres_path = image_name
                # image.save(using=model_mixin.project.project_connection_name)

            # if image_type in ['floor', 'key']:
            #     # apartment = model_mixin.get_model_object_and_row('Apartments', floor_id)
            #     # resource_id = apartment.floor_plan_image_id
            #     # image = model_mixin.get_model_object_and_row("Resources", resource_id)
            #
            #     if image_type == "floor":
            #         floor_plan_path = image.highres_path
            #         floor_plan_path_split = floor_plan_path.split("/")
            #         current_image_name = floor_plan_path_split[-1:][0]
            #
            #         image_name = floor_plan_path.replace(current_image_name, image_name)
            #         image.highres_path = image_name
            #         image.save(using=model_mixin.project.project_connection_name)
            #     else:
            #         floor_plan_path = image.thumbnail_path
            #         floor_plan_path_split = floor_plan_path.split("/")
            #         current_image_name = floor_plan_path_split[-1:][0]
            #
            #         image_name = floor_plan_path.replace(current_image_name, image_name)
            #         image.thumbnail_path = image_name
            #         image.save(using=model_mixin.project.project_connection_name)

            data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_image_from_path(request, pk):
    if request.is_ajax():

        file_location = os.path.join(request.GET['path'], request.GET['image'])
        base_file_location = settings.BASE_IMAGE_LOCATION
        file_location = '%s/%s.jpg' % (base_file_location, file_location)
        file_location = file_location.replace('//', '/')

        if os.path.isfile(file_location):
            os.remove(file_location)
            data = json.dumps({'success': True})
        else:
            data = json.dumps({'success': False})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def send_user_email(request, project_id, pk):
    if request.is_ajax():
        project = Project.objects.get(pk=project_id)
        user = User.objects.get(pk=pk)

        email_type = request.GET['email_type']

        if email_type == "account":
            user.send_setup_email(project=project)

        if email_type == "password":
            user.send_password_email(project=project)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def copy_resource_image_to_properties(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        property_id = request.GET['property_id']
        floor_id = request.GET['floor_id']
        resource_id = request.GET['resource_id']

        plan = model_mixin.model_object("PropertyResources").filter(pk=resource_id)[0]
        resource = model_mixin.model_object("Resources").filter(pk=plan.resource_id)[0]

        main_instance = model_mixin.model_object("Properties"). \
            filter(pk=property_id).values_list('pk', 'name__english')

        name = main_instance[0][1]
        id = main_instance[0][0]

        apartment_instances = model_mixin.model_object("Properties"). \
            filter(floor_id=floor_id).values_list('pk', 'name__english')

        if len(name) == 1:
            name = '00' + str(name)

        if len(name) == 2:
            name = '0' + str(name)

        for new_property_id, property_name in apartment_instances:
            if str(property_name[1:]) == str(name[1:]):

                key_exists = model_mixin.model_object("PropertyResources").filter(property_id=new_property_id,
                                                                                  key=plan.key)
                if not key_exists:
                    check_key = plan.key
                    model_mixin.model_object("PropertyResources").create(property_id=new_property_id,
                                                                         resource_id=resource.pk,
                                                                         key=check_key)

                break

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def copy_resource_image_to_floors(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        resource_id = request.GET['resource_id']
        floor_id = request.GET['floor_id']

        plan = model_mixin.model_object("FloorsPlans").filter(pk=resource_id)[0]

        resource = model_mixin.model_object("Resources").filter(pk=plan.resource_id)[0]

        model_mixin.model_object("Resources").create(description_id=resource.description_id,
                                                     path=resource.path,
                                                     size=0)

        new_resource = model_mixin.model_object("Resources").filter(description_id=resource.description_id,
                                                                    path=resource.path,
                                                                    size=0).order_by('-id')[0]

        check_key = ''
        key_exists = model_mixin.model_object("FloorsPlans").filter(floor_id=floor_id, key=plan.key)
        if not key_exists:
            check_key = plan.key

        model_mixin.model_object("FloorsPlans").create(floor_id=floor_id,
                                                       resource_id=new_resource.pk,
                                                       key=check_key)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)



# Define a function for the thread
def print_time(threadName, delay):
    count = 0
    while count < 5:
        time.sleep(delay)
        count += 1
        print ("%s: %s" % (threadName, time.ctime(time.time())))



def save_building_name(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        building_name = request.GET['building_name']
        direction = request.GET['direction']

        building_id = None

        if building_name:
            if direction == "new":

                try:
                    model_mixin.model_object("Names").create(english=building_name)
                    name = model_mixin.model_object("Names").filter(english=building_name).order_by('-pk')

                    name = name[0]
                    building = model_mixin.model_object("Buildings").filter(name=name)
                    if not building:
                        model_mixin.model_object("Buildings").create(name=name)
                        building = model_mixin.model_object("Buildings").filter(name=name)

                    building = building[0]
                    building_id = building.pk

                except Exception as e:
                    print('build err', e)

            else:
                building_id = request.GET['building']

                building = model_mixin.model_object("Buildings").filter(pk=building_id)[0]
                name = model_mixin.model_object("Names").filter(pk=building.name_id)
                if name:
                    name = name[0]
                    name.english = building_name
                    name.save(using=model_mixin.project.project_connection_name)

        if building_id:
            context = model_mixin.klass("Floors").get_select_floors_html(building_id, request=request)
            template = "canvas/templates/includes/floor_select_list.html"
            rendered = render_to_string(template, context)

            data = json.dumps({'success': True,
                               'rendered': rendered})

            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


def save_floor_buildings(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():

        floor_id = request.GET['floor_id']
        building_id = request.GET['building_id']
        direction = request.GET['direction']

        assigned = model_mixin.model_object("FloorsBuildings").filter(floor_id=floor_id, building_id=building_id)

        if direction == "save":
            if not assigned:
                model_mixin.model_object("FloorsBuildings").create(floor_id=floor_id, building_id=building_id)
        else:
            if assigned:
                assigned.delete()

        context = model_mixin.klass("Floors").get_select_floors_html(building_id, floor_id, request=request)
        template = "canvas/templates/includes/floor_select_list.html"
        rendered = render_to_string(template, context)

        data = json.dumps({'success': True,
                           'rendered': rendered})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_building(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        building_id = request.GET['building_id']

        try:
            floor_buildings = model_mixin.model_object("FloorsBuildings").filter(building_id=building_id)
            for fb in floor_buildings:
                floor = model_mixin.model_object("Floors").filter(pk=fb.floor_id)
                properties = model_mixin.model_object("Properties").filter(floor_id=floor[0].pk)

                properties.delete()
                floor.delete()

            floor_buildings.delete()

        except Exception as e:
            print(e)

        data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_project_model_filter(request, project_id, model_name):
    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=project_id)

        project_model_object = model_mixin.get_class_object(model_name)
        model_fields = model_mixin.get_model_object_fields(drop_id=True, model_name=model_name)
        model_field_names = project_model_object._meta.get_all_field_names()

        applied_filters = {}
        ignore_filter_list = ['data_page_position', 'data_page_direction', 'order_page_by', 'data_page_number',
                              'order_page_by_direction', "csrfmiddlewaretoken", "sort_by", "sorting_direction",
                              "data_field_names"]

        for key, value in request.POST.items():
            if not value in ["All", ""] and not str(key) in ignore_filter_list:
                applied_filters.update({str(key): value})

        has_hot_spot_configuration = False
        floors = False
        if "picking_coord_x" in model_fields and "picking_coord_y" in model_fields:
            has_hot_spot_configuration = True

        if model_name == "Floors":
            floors = True
            has_hot_spot_configuration = True

        table_values = apply_form_filter(applied_filters, model_field_names, project_model_object,
                                         model_mixin.project.project_connection_name)

        template = "projects/dynamic_editing/includes/project_model_filter_pagination.html"

        try:
            data = serializers.serialize("python", table_values)
        except:
            data = []

        if data:
            field_names = data[0]['fields']
            field_names = [str(x) for x, y in field_names.items()]

        sorting_field = request.POST['order_page_by']
        sorting_direction = request.POST['order_page_by_direction']

        paginate_by = 50
        position = 0
        data_page_direction = None
        page = 1

        if 'data_page_position' in request.POST:
            position = request.POST['data_page_position']

        if 'data_page_direction' in request.POST:
            data_page_direction = request.POST['data_page_direction']

        if 'data_page_number' in request.POST:
            page = request.POST['data_page_number']

            if data_page_direction == "Next":
                page = int(page) + 1

            if data_page_direction == "Previous":
                page = int(page) - 1

            position = return_position_page(page, paginate_by)

        count_data = len(data)
        pages = float(count_data) / float(paginate_by)

        test_extra_page = str(pages).split('.')
        if int(test_extra_page[1]) > 0:
            pages += 1

        page_list = []
        for i in range(1, int(pages + 1)):
            page_list.append(int(i))

        next_page = int(page) + 1
        previous_page = int(page) - 1

        context = {
            'project': model_mixin.project,
            'model': project_model_object,
            'field_names': field_names,
            'table_values': table_values,
            'position': position,
            'page': int(page),
            'pages': int(pages),
            'page_list': page_list,
            'next_page': next_page,
            'previous_page': previous_page,
            'data': data[position:(int(paginate_by) * int(page))],
            'model_name': model_name,
            'sorting_field': str(sorting_field),
            'sorting_direction': str(sorting_direction),
            'has_hot_spot_configuration': has_hot_spot_configuration,
            'floors': floors
        }

        rendered = render_to_string(template, context)

        return_data = {
            'success': True,
            'rendered_html': rendered,
            'position': position,
            'page': int(page),
            'pages': int(pages),
            'sorting_field': str(sorting_field),
            'sorting_direction': str(sorting_direction),
        }

        return_data = json.dumps(return_data)

        mimetype = 'application/json'
        return HttpResponse(return_data, mimetype)


def return_position_page(page, total):
    if page == '1':
        return 0

    m = 1
    try:
        if type(int(page)) == int:
            for i in range(1, int(page)):
                m += int(total)
    except:
        pass

    return m


def create_new_project(request):
    if request.is_ajax():

        project_name = request.POST['name']
        project_name = project_name.replace("'", "").replace('"', "")
        project_copied_from = request.POST['project_to_copy']

        project_to_clone = Project.objects.get(pk=project_copied_from)

        try:
            project = project_to_clone
            project.pk = None
            project.project_connection_name = None
            project.name = project_name
            project.active = True
            project.version = 1.0
            project.version_family = None
            project.project_copied_from = None
            project.created_by = request.user
            project.status = Project.STAGING
            project.save()

            admins = User.objects.filter(user_type__in=[User.USER_TYPE_ADMIN,
                                                        User.USER_TYPE_SUPER_ADMIN], is_active=True)
            for admin in admins:
                project.users.add(admin)
                project.save()

            project_to_clone = Project.objects.get(pk=project_copied_from)
            project.project_copied_from = project_to_clone
            project.project_connection_name = '%s_%s' % (project.name.replace("'", "", 10).replace('"', "", 10)
                                                         .replace(" ", "_", 10).replace(".", "", 10), project.pk)
            project.save()

            add_new_revision(project, request, Project, "Project created successfully")

            for user in project_to_clone.users.all():
                project.users.add(user)
                project.save()

            new_project_connection = ProjectConnection.objects.create(project=project,
                                                                      connection_type=ProjectConnection.SQLITE_CONNECTION)

            new_sync = ProjectCMSSync()
            new_sync.add_new_project_into_sync(project.pk, project.name)

            new_project_connection.run_project_mapping(create_from_master=False)

            new_version = ProjectVersion(active_version=1.0, project=project, project_name=project.name)
            new_version.save()

            project.version_family = new_version
            project.project_publishing_path = project.get_publish_path_example
            project.save()

            new_sync = ProjectCMSSync()
            new_sync.add_new_project_into_sync(project.pk, project.name)

            project_connection = ProjectConnection.objects.filter(project=project)
            if not project_connection:
                ProjectConnection.objects.create(project=project)

            for connection in project.project_connections.all():
                connection.run_project_mapping()
        except Exception as e:
            print('error', e)

        data = json.dumps(
            {
                'success': True,
                'project_id': project.pk
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def clone_project(request):
    if request.is_ajax():

        project_id = request.GET['project_id']
        with_data = request.GET['with_master_data']

        project_to_clone = Project.objects.get(pk=project_id)

        project = project_to_clone
        project.pk = None
        project.project_connection_name = None
        project.active = True
        project.save()

        new_version_number = float(project_to_clone.version_family.active_version + 1)

        project.project_connection_name = '%s_%s' % (project.name.replace("'", "", 10).replace('"', "", 10).replace(" ", "_", 10).replace(".", "", 10), project.pk)
        project.project_publishing_path = project.get_publish_path_example
        project.project_copied_from_id = project_id
        project.version = new_version_number
        project.status = Project.STAGING
        project.created_by = request.user
        project.save()

        add_new_revision(project, request, Project, "Project cloned successfully")

        if project.version_family:
            project.version_family.active_version = new_version_number
            project.version_family.save()

        project_to_clone = Project.objects.get(pk=project_id)
        project_to_clone.modified = datetime.datetime.now()
        project_to_clone.last_modified_by = request.user
        project_to_clone.save()

        admins = User.objects.filter(user_type__in=[User.USER_TYPE_ADMIN,
                                                    User.USER_TYPE_SUPER_ADMIN], is_active=True)
        for admin in admins:
            project.users.add(admin)
            project.save()

        if project_to_clone.family:
            project_to_clone.family.project_list.add(project)
            project_to_clone.family.save()

        for user in project_to_clone.users.all():
            project.users.add(user)
            project.save()

        for script in project_to_clone.logged_scripts.all():
            new_script = script
            new_script.pk = None
            new_script.project = project
            new_script.version = project.version
            new_script.save()

        new_project_connection = ProjectConnection.objects.create(project=project,
                                                                  connection_type=ProjectConnection.SQLITE_CONNECTION)

        new_sync = ProjectCMSSync()

        try:
            new_sync.add_new_project_into_sync(project.pk, project.name)
        except Exception as e:
            pass

        new_project_connection.run_project_mapping(create_from_master=with_data)

        data = json.dumps(
            {
                'success': True,
                'project_id': project.pk
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def delete_selected_project(request, pk):
    if request.is_ajax():
        project_to_delete = Project.objects.get(pk=pk)
        Project.objects.filter(project_copied_from=project_to_delete).update(project_copied_from=None)
        project_to_delete.delete()

        time.sleep(5)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def remove_project_script(request, pk):

    if request.is_ajax():

        script_id = request.GET['script_id']

        ProjectScripts.objects.get(pk=script_id).delete()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def change_project_selected_script(request, pk):

    if request.is_ajax():

        script_id = request.GET['script_id']
        selected_project = Project.objects.get(pk=pk)

        if script_id == "0":
            selected_project.selected_script = None
        else:
            selected_project.selected_script_id = script_id

        selected_project.save()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def change_project_version_active(request, pk):
    if request.is_ajax():

        project = Project.objects.get(pk=pk)
        new_active_version = request.GET['new_active_version']
        version_family = project.version_family

        version_family.active_version = new_active_version
        version_family.save()

        all_projects_in_family = Project.objects.filter(version_family=version_family)
        all_projects_in_family.update(active=False)
        all_projects_in_family.filter(status=Project.IN_PROGRESS).update(status=Project.NOT_ACTIVE)

        active_version = all_projects_in_family.filter(version=new_active_version)
        if active_version:
            for version in active_version:

                if version.status == Project.NOT_ACTIVE:
                    version.status = Project.IN_PROGRESS

                version.active = True
                version.save()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def change_project_version_status(request, pk):

    if request.is_ajax():

        project = Project.objects.get(pk=pk)
        current_status = project.status

        status = request.GET['status']
        project = Project.objects.get(pk=pk)

        if status in [Project.STAGING, Project.LIVE]:
            project.active = True
        else:
            #torch all user access!
            for user in project.users.all():
                project.users.remove(user)
                project.save()

            project.active = False

        project.status = status
        project.created_by = request.user
        project.save()

        if project.status == Project.STAGING:
            admins = User.objects.filter(user_type__in=[User.USER_TYPE_ADMIN,
                                                        User.USER_TYPE_SUPER_ADMIN], is_active=True)
            for admin in admins:
                project.users.add(admin)
                project.save()

        user_move = 'all_users'
        if "user_move" in request.GET and request.GET['user_move']:
            user_move = request.GET['user_move']

        if user_move == "ds_users":
            user_list = project.users.filter(user_type__in=[User.USER_TYPE_ADMIN, User.USER_TYPE_SUPER_ADMIN])
        else:
            # print('moving all users to new live')
            user_list = []
            family_projects = project.family.project_list.filter(status__in=[Project.STAGING, Project.LIVE])
            # print('-- family_projects', family_projects)
            for project in family_projects:
                # print('project:', project)
                users = project.users.filter(is_active=True)
                # print(' ---> all users:', users)
                for user in users:
                    # print('------ ', user)
                    user_list.append(user)
            # print('+++++++')

        for user in project.users.all():
            project.users.remove(user)
            user.save()
            project.save()

        for user in user_list:
            project.users.add(user)
            user.save()
            project.save()

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def update_project_details(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    resources = model_mixin.model_object("Resources").all().order_by('path')
    for resource in resources:
        resource.path = str(resource.path).replace("/srv/", "/mnt/twenty/")
        resource.save(using=model_mixin.project.project_connection_name)

        update_templates = True

        if "/mnt" in resource.path:
            split_name = resource.path.split("/")

            for sname in split_name:
                if sname == model_mixin.project.project_connection_name:
                    update_templates = False

            if update_templates:
                split_name[int(len(split_name) - 1)] = model_mixin.project.project_connection_name
                new_name = '/'.join(split_name)
                resource.path = new_name

                # resource.save(using=model_mixin.project.project_connection_name)

                # if update_templates:
                #     templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project.project_copied_from)
                #
                #     for template in templates:
                #         check_templates = ProjectCSVTemplate.objects.filter(project=model_mixin.project, template_name=template.template_name)
                #         if not check_templates:
                #             pass
                #
                #
                #         fields = ProjectCSVTemplateFields.objects.filter(template=template)
                #
                #         if fields:
                #
                #             new_template = template
                #             new_template.id = None
                #             new_template.project = model_mixin.project
                #             new_template.save()
                #
                #             for field in fields:
                #                 new_field = field
                #                 new_field.id = None
                #                 new_field.template = new_template
                #                 new_field.save()

        return True
