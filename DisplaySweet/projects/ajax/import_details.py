import json
import csv
import os
from PIL import Image
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.conf import settings

from django.template.loader import render_to_string
from difflib import SequenceMatcher
from projects.models.model_mixin import PrimaryModelMixin
from users.models import UserImports, User, UserModifiedLog
from users.forms import UserImportsForm
from projects.cms_sync import ProjectCMSSync


def get_new_image_list(request, pk):

    if request.is_ajax():

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, model_mixin.project.project_connection_name)

        image_list = model_mixin.klass("Assets").get_image_list_for_tags()
        template = "canvas/includes/folder_path_images.html"

        context = {
            'image_list': image_list,
            'project_image_folder': project_image_folder,
            'request': request
        }

        rendered_list = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered': rendered_list
            }
        )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


@csrf_exempt
def upload_image_files(request, pk):

    if request.is_ajax():
        model_mixin = PrimaryModelMixin(request=request, project_id=pk)

        project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, model_mixin.project.project_connection_name)
        if not os.path.isdir(project_image_folder):
            os.makedirs(project_image_folder)

        thumbnail_path = os.path.join(project_image_folder, "thumbnails")
        if not os.path.isdir(thumbnail_path):
            os.makedirs(thumbnail_path)

        media_path = os.path.join(project_image_folder, "media")
        if not os.path.isdir(media_path):
            os.makedirs(media_path)

        if request.FILES:
            basewidth = 500
            for afile in request.FILES.getlist('image_files'):

                file_name = afile.name
                file_name = file_name.replace(" ", "_", 10)

                extension = file_name[-3:]
                handle_uploaded_file(afile, os.path.join(project_image_folder, file_name))

                if extension in model_mixin.image_files():
                    img = Image.open(os.path.join(project_image_folder, file_name))

                    wpercent = (basewidth / float(img.size[0]))
                    hsize = int((float(img.size[1]) * float(wpercent)))

                    try:
                        img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                        img.save(os.path.join(thumbnail_path, file_name))
                    except Exception as e:
                        print (e)
                    model_mixin.klass("Assets").add_new_image_asset(afile, project_image_folder)

                else:
                    handle_uploaded_file(afile, os.path.join(media_path, file_name))
                    model_mixin.klass("Assets").add_new_image_asset(afile, media_path)

        data = json.dumps(
            {
                'success': True
            }
        )

        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


@csrf_exempt
def upload_user_details(request):

    if request.is_ajax():
        rendered_list = ''
        if request.FILES:

            file_form = UserImportsForm(request.POST, request.FILES)
            file_error = False
            if file_form.is_valid():

                try:
                    file_details = file_form.save()
                except Exception as e:
                    print('error', e)

                user_list = []

                try:
                    csv_file = open(file_details.user_file.path, 'rU', encoding='utf-8')
                    csv_reader = csv.reader(csv_file)

                    for index, row in enumerate(csv_reader):
                        if index == 0:
                            if not row == ['First Name', 'Last Name', 'Email Address', 'Password', 'User Type']:
                                file_error = True

                        if index > 0:
                            first_name = row[0]
                            last_name = row[1]
                            email = row[2]
                            password = row[3]
                            user_type = row[4]

                            user_list.append({'first_name': first_name, 'last_name': last_name,
                                              'email': email, 'password': password, 'user_type': user_type})

                except Exception as e:
                    print('csv error', e)

                template = "users/includes/user_import_display.html"
                context = {
                    'request': request,
                    'users': user_list,
                    'file_saved_id': file_details.pk
                }

                rendered_list = render_to_string(template, context)

        data = json.dumps(
            {
                'success': True,
                'rendered_list': rendered_list,
                'file_error': file_error
            }
        )

        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


@csrf_exempt
def import_user_details(request, pk):

    if request.is_ajax():
        file_details = UserImports.objects.get(pk=request.GET['file_id'])
        user_list = []

        try:
            csv_file = open(file_details.user_file.path, 'rU', encoding='utf-8')
            csv_reader = csv.reader(csv_file)

            for index, row in enumerate(csv_reader):
                if index == 0:
                    if not row == ['First Name', 'Last Name', 'Email Address', 'Password', 'User Type']:
                        file_error = True

                if index > 0:
                    first_name = str(row[0]).strip()
                    last_name = str(row[1]).strip()
                    email = str(row[2]).strip()
                    password = str(row[3]).strip()
                    user_type = str(row[4]).strip()

                    user_list.append({'first_name': first_name, 'last_name': last_name,
                                      'email': email, 'password': password, 'user_type': user_type})

        except Exception as e:
            print('read csv error', e)

        model_mixin = PrimaryModelMixin(request=request, project_id=pk)
        project = model_mixin.project

        user_choices = {'Display Sweet Super Admin': User.USER_TYPE_SUPER_ADMIN,
                        'Client Admin': User.USER_TYPE_CLIENT_ADMIN,
                        'Master Agent': User.USER_TYPE_MASTER_AGENT,
                        'Agent': User.USER_TYPE_AGENT,
                        'View Only User': User.USER_TYPE_VIEW_ONLY_USER}

        new_sync = ProjectCMSSync()

        for user in user_list:
            check_user = User.objects.filter(email=user['email'], )
            if not check_user:
                user_type = user_choices[user['user_type']]

                if not user_type:
                    user_type = User.USER_TYPE_AGENT

                if user_type:
                    new_user = User.objects.create(first_name=user['first_name'],
                                        last_name=user['last_name'],
                                        email=user['email'],
                                        username=user['email'],
                                        user_type=user_type,
                                        created_by=request.user,
                                        owned_by=request.user,
                                        last_modified_by=request.user,
                                        temporary_setup_password=user['password'])

                    new_user.set_password(user['password'])
                    new_user.save()

                    try:
                        project.users.add(new_user)
                        project.save()
                    except Exception as e:
                        print(e, new_user)

                    UserModifiedLog.objects.create_log(user, request.user,
                                                       "Added user access to project %s" % project.name)

                    try:
                        new_sync.add_user_into_sync(project.pk, user['first_name'], user['last_name'], user['email'], 0)

                        new_sync.update_password_sync(project.pk, user['password'], user['first_name'], user['last_name'], user['email'])
                    except Exception as e:
                        pass

                    try:
                        new_sync.add_user_access_into_sync(project.pk, project.name, new_user.email,
                                                           new_user.can_edit_reservation(),
                                                           new_user.can_edit_reservation())
                    except Exception as e:
                        pass

                    try:
                        new_sync.update_password_sync(project.pk, user['password'], user['first_name'],
                                                      user['last_name'], user['email'])
                    except Exception as e:
                        pass
            else:
                try:
                    project.users.add(check_user[0])
                    check_user[0].set_password(user['password'])
                    check_user[0].last_modified_by = request.user
                    check_user[0].save()
                    project.save()

                except Exception as e:
                    print(e, check_user[0])

                try:
                    new_sync.add_user_access_into_sync(project.pk, project.name, check_user[0].email,
                                                       check_user[0].can_edit_reservation(),
                                                       check_user[0].can_edit_reservation())
                except Exception as e:
                    pass

                try:
                    new_sync.update_password_sync(project.pk,
                                                  check_user[0]['password'],
                                                  check_user[0]['first_name'],
                                                  check_user[0]['last_name'],
                                                  check_user[0]['email'])
                except Exception as e:
                    pass

                UserModifiedLog.objects.create_log(check_user[0], request.user,
                                                   "Updated user access to project %s" % project.name)

        data = json.dumps(
            {
                'success': True,
            }
        )

        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


def handle_uploaded_file(f, file_name_and_path):
    with open(file_name_and_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def get_table_mapping_options(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        model_name = request.GET['model_name']

        csv_column_name = None
        if "csv_column_name" in request.GET:
            csv_column_name = request.GET['csv_column_name']

        model_fields = model_mixin.get_model_object_fields(model_name=model_name)

        if model_fields:
            model_fields = model_mixin.clean_model_mapping_fields(project=model_mixin.project,
                                                                  model_fields=model_fields,
                                                                  model_name=model_name)

        if model_fields:
            most_likely_field = None
            score = 0
            for field in model_fields:
                ratio_score = get_likely_score_ration(csv_column_name.lower(), field)
                if float(ratio_score) >= float(score):
                    most_likely_field = field
                    score = ratio_score

            data = json.dumps(
                {
                    'success': True,
                    'model_fields': model_fields,
                    'most_likely_field': most_likely_field
                }
            )
        else:
            data = json.dumps(
                {
                    'success': False,
                    'model_fields': [],
                    'most_likely_field': ''

                }
            )

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def get_likely_score_ration(field_one, field_two):
    return SequenceMatcher(None, field_one.lower(), field_two.lower()).ratio()


def import_images(request, pk):
    model_mixin = PrimaryModelMixin(request=request, project_id=pk)

    if request.is_ajax():
        data = json.dumps({'success': False})

        if "path_to_save_to" in request.POST:
            path_selected = request.POST['path_to_save_to']
            path_name = '%s_path' % path_selected
            file_location = request.POST[path_name]

            base_file_location = settings.BASE_IMAGE_LOCATION
            file_location = '%s/%s/' % (base_file_location, file_location)
            file_location = file_location.replace('//', '/')

            if request.FILES and request.FILES.getlist('floor_images'):
                if not os.path.isdir(file_location):
                    os.makedirs(file_location)

                for item in request.FILES.getlist('floor_images'):
                    saving_location = '%s%s' % (file_location, str(item.name))
                    saving_location = saving_location.replace("/project_data/project_data/", "/project_data/")
                    with open(saving_location, 'wb+') as destination:
                        for chunk in item.chunks():
                            destination.write(chunk)

            model_mixin.project.level_plan_location = request.POST['level_plan_path']
            model_mixin.project.floor_plan_location = request.POST['floor_plan_path']
            model_mixin.project.key_plan_location = request.POST['key_plan_path']

            model_mixin.project.save()

            data = json.dumps({'success': True})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)