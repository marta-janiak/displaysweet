from django.db.models.loading import get_model
from django.db.models.fields.related import ForeignKey, ManyToOneRel
import os
import datetime
from django.conf import settings
import imghdr
from django.forms.models import model_to_dict
import json
from itertools import chain
from difflib import SequenceMatcher
from django.template.loader import render_to_string
import codecs
import parser

from django.db import connections
import time
from django.core.urlresolvers import reverse_lazy, reverse
import shutil
import operator
from django.db.models import Q

import threading


class ProjectsPermissionsMixin(object):
    """
    All methods relating to permissions go on this mixin!
    """
    def can_be_viewed_in_api_by(self, user):
        if user.is_administrator:
            return True

        if self.objects.filter(users__contains=self.request.user):
            return True

        return False

    def can_be_viewed_by(self, user):
        if self.objects.filter(users__contains=self.request.user):
            return True

        return False

    def can_be_edited_by(self, user):
        if user.is_administrator:
            return True

        if user.is_site_administrator:
            return True

        return False

    def can_edit_users(self, user):
        if user.is_site_administrator:
            return True

        return False

    def can_be_deleted_by(self, user):
        if user.is_ds_admin:
            return self.can_be_edited_by(user)

    def has_building_model(self, project):
        return project.has_building_model()


class ProjectModelMixin(object):
    """
    All methods relating to project model details go on this mixin!
    """
    project = None
    project_model_object = None
    model_instance = None
    model_name = None
    model_fields = None
    field_names = None
    request = None
    building_id = None

    def get_project(self, pk):
        from core.models import Project
        if pk:
            project = Project.objects.get(pk=pk)
            self.project = project
            return project

    def get_model_object(self, model_name):
        project_model_object = get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        self.model_name = model_name

        return project_model_object

    def get_reversed_urls_from_actions(self, actions, pk):
        reversed_actions = []
        for action_name, url_name in actions:
            reversed_actions.append((action_name, reverse(url_name, args=[pk])))

        return reversed_actions

    def get_model_object_and_row(self, model_name, row_id=None):
        project_model_object = get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        queryset = project_model_object.objects.using(self.project.project_connection_name).all()

        if row_id:
            queryset = project_model_object.objects.using(self.project.project_connection_name).filter(id=row_id)
            if len(queryset) == 1:
                return queryset[0]

        return queryset

    def get_apartment_related_object_and_queryset(self, model_name, apartment):

        project_model_object = get_model(self.project.project_connection_name, model_name)
        queryset = project_model_object.objects.using(self.project.project_connection_name).all()
        model_dict = model_to_dict(queryset[0])

        if "apartment" in model_dict:
            queryset = queryset.filter(apartment_id=apartment)
        else:
            queryset = queryset.filter(id=apartment.pk)

        return queryset

    def get_model_instance(self, row_id, field=None):

        if field:
            instances = self.project_model_object.objects.using(self.project.project_connection_name).all()
            for instance in instances:
                attribute = getattr(instance, field)
                if str(attribute) == str(row_id):
                    self.model_instance = instance
                    return instance

        else:
            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(pk=row_id)
            self.model_instance = model_instance
            return model_instance

    def get_model_queryset(self, field=None, value=None, list_needed=False):

        queryset = self.project_model_object.objects.using(self.project.project_connection_name).all()

        if field:
            kwargs = {
                '{0}'.format(field): value,
            }
            queryset = queryset.filter(**kwargs)

        if len(queryset) == 1 and not list_needed:
            return queryset[0]

        return queryset

    def get_model_object_fields(self, project=None, model_name=None, drop_id=True):
        if model_name:
            model_fields = []
            try:
                model_object = get_model(project.project_connection_name, model_name)

                model_fields_objects = model_object._meta.get_fields()
                for field in model_fields_objects:
                    if not field.name == "id" and not type(field) == ManyToOneRel:
                        model_fields.append(field.name)

                return model_fields
            except:
                return []
        else:
            self.model_fields = self.project_model_object._meta.get_fields()
            return self.filter_model_fields_for_form(drop_id=drop_id)

    def get_mapping_table_value(self, category):

        category = str(category).strip().lower()
        
        if category == 'propertiesstringfields':
            return 'apartment_strings'

        if category == 'properties':
            return None

        if category == 'propertiesbooleanfields':
            return 'apartment_bools'

        if category == 'propertiesfloatfields':
            return 'apartment_floats'

        if category == 'propertiesintfields':
            return 'apartment_ints'

        if category == "floor_plan_typical":
            return "floor_plan_typical"

        return 'names'

    def get_string_category(self, model_name):
        category_model_object = self.get_model_object('Texttablecategories')
        table_category_mapping = self.get_mapping_table_value(model_name)
        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)

        if not category_queryset:
            category_model_object.objects.using(self.project.project_connection_name).create(category=table_category_mapping)
            category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)

        if category_queryset:
            return category_queryset

    def add_new_string_value(self, field):
        self.get_model_object('Texttablecategories')

        try:
            english_value = str(field.column_pretty_name).replace(" ", "_").replace("/", "", 10).replace("2", "", 10).replace("$", "", 10).lower()

        except:
            english_value = 'error'

        table_category_mapping = self.get_mapping_table_value(field.column_table_mapping)

        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)

        if category_queryset:
            category = category_queryset

            queryset = self.class_object('Strings').filter(text_table_category=category,
                                                           english=english_value)

            if queryset:
                string_field = queryset[0]
                properties = self.class_object("Properties").all()

                for apartment in properties:
                    check_field = self.class_object("PropertiesStringFields").filter(property=apartment,
                                                                                     type_string=string_field)

                    if not check_field:
                        self.class_object("PropertiesStringFields").create(property=apartment,
                                                                           type_string=string_field,
                                                                           value=' ')

                return string_field
            else:
                self.class_object('Strings').create(text_table_category=category, english=english_value)

                item_created = self.class_object('Strings').filter(text_table_category=category, english=english_value)
                item_created = item_created[0]
                return item_created

    def check_category_string(self, key, value):

        if value == settings.FLOOR_PLAN_TYPICAL:
            category_queryset = self.class_object("Texttablecategories").filter(category=settings.FLOOR_PLAN_TYPICAL)
            if not category_queryset:
                self.class_object("Texttablecategories").create(category=settings.FLOOR_PLAN_TYPICAL)
                category_queryset = self.class_object("Texttablecategories").filter(
                    category=settings.FLOOR_PLAN_TYPICAL)

            category = category_queryset[0]
            strings_queryset = self.class_object('Strings').filter(text_table_category=category,
                                                                   english=value)

            if strings_queryset:
                type_string = strings_queryset[0]
                return type_string

        self.get_model_object('Texttablecategories')

        table_category_mapping = self.get_mapping_table_value(key)
        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)

        if category_queryset:
            category = category_queryset
            queryset = self.class_object("Strings").filter(text_table_category=category, english=value)

            if queryset:
                return queryset[0]
            else:
                self.class_object("Strings").create(text_table_category=category, english=value)
                item_created = self.class_object("Strings").filter(text_table_category=category, english=value)
                item_created = item_created[0]
                return item_created

    def get_or_create_name_category_string(self, key, value):
        self.get_model_object('Texttablecategories')

        table_category_mapping = self.get_mapping_table_value(key)
        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)

        if category_queryset:
            category = category_queryset
            names_model_object = self.get_model_object('Names')
            queryset = names_model_object.objects.using(self.project.project_connection_name).filter(text_table_category=category,
                                                                                                     english=value)

            if queryset:
                return queryset[0]
            else:
                self.project_model_object.objects.using(self.project.project_connection_name).create(text_table_category=category,
                                                                                                     english=value)

                item_created = self.project_model_object.objects.using(self.project.project_connection_name).filter(text_table_category=category,
                                                                                                                    english=value)
                item_created = item_created[0]
                return item_created

    def get_property_name_value(self, name_object):

        ##print ('property', name_object, type(name_object), name_object.english

        # property_name = self.class_object("Names").filter(pk=property.name_id)
        # if property_name:
        #     return property_name[0].english

        return ''

    def save_asset_to_plans_container(self, page_type, resource_id, canvas_id):

        ##print ('in save_asset_to_plans_container', page_type, resource_id, canvas_id

        template = self.class_object("Templates").filter(name__iexact=page_type)

        # print ('--- template', template, 'template ID: ', template[0].pk

        if template:
            template_id = template[0].pk

            wireframe_controllers = self.class_object("WireframeControllers").filter(canvas_id=canvas_id,
                                                                                     template_id=template_id)

            # print ('wireframe_controllers', wireframe_controllers

            for wireframe_controller in wireframe_controllers:
                wireframe_id = wireframe_controller.wireframe_id

                template_containers = self.class_object("WireframeControllerContainers").filter(wireframe_controller_id=wireframe_controller.pk).values_list('container_id', flat=True)
                # print ('template_containers', template_containers

                containers = self.class_object("Containers").filter(pk__in=template_containers)

                # print ('containers', containers

                if containers:
                    container = containers[0]
                    asset_created = self.class_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                        template_id=template_id,
                                                                                        wireframe_id=wireframe_id,
                                                                                        asset_id=resource_id,
                                                                                        container=container)

                    # print ('------- asset_created', asset_created, 'wireframe_id: ', wireframe_id

                    if not asset_created:
                        self.class_object("ContainerAssignedAssets").create(canvas_id=canvas_id,
                                                                            template_id=template_id,
                                                                            wireframe_id=wireframe_id,
                                                                            asset_id=resource_id,
                                                                            container_id=container.pk,
                                                                            orderidx=1)
                        

    def save_property_name_value(self, name_instance, property):

        property_name = self.class_object("Names").filter(pk=property.name_id)
        if property_name and name_instance:
            property_name = property_name[0]

            property_name.english = name_instance
            property_name.save(using=self.project.project_connection_name)

    def get_apartment_strings_type_value(self, model_name, english_value, apartment_id):

        if english_value.strip() in ["floor_plan_typical", "floor plan typical"]:

            if english_value.strip() == "floor plan typical":
                english_value = "floor_plan_typical"

            category_queryset = self.class_object("Texttablecategories").filter(category=settings.FLOOR_PLAN_TYPICAL)
            category = category_queryset[0]
            strings_queryset = self.class_object('Strings').filter(text_table_category=category,
                                                                   english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]
                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = self.class_object(model_name).filter(**kwargs)
                if not queryset:
                    self.class_object(model_name).create(**kwargs)
                    queryset = self.class_object(model_name).filter(**kwargs)

                if queryset:
                    return queryset[0]

        if model_name == self.project.apartment_model:
            self.get_model_object(self.project.apartment_model)
            return self.get_model_queryset(field='id', value=apartment_id, list_needed=False)

        if model_name == self.project.hotspot_model:
            self.get_model_object(self.project.hotspot_model)
            hotspots = self.class_object("PropertyHotspots").filter(property_id=apartment_id)

            if not hotspots:
                self.class_object("PropertyHotspots").create(property_id=apartment_id)
                hotspots = self.class_object("PropertyHotspots").filter(property_id=apartment_id)

            if hotspots:
                return hotspots[0]

        self.get_model_object('Texttablecategories')
        table_category_mapping = self.get_mapping_table_value(model_name)

        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)
        self.get_model_object('Strings')

        if category_queryset:
            category = category_queryset
            strings_queryset = self.class_object('Strings').filter(text_table_category=category,
                                                                   english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]

                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = self.class_object(model_name).filter(**kwargs)

                if not queryset:
                    self.class_object(model_name).create(**kwargs)
                    queryset = self.class_object(model_name).filter(**kwargs)

                if queryset:
                    return queryset[0]

    def return_canvas_save_path(self, project_name, canvas_name, template_name):

        project_name = project_name.replace(" ", "_", 10)

        creating_save_path = 'project_images/%s/staging/%s/%s/' % (
            project_name, canvas_name, template_name.strip().replace(" ", "_").lower())

        if self.project.project_publishing_path:

            if self.project.project_publishing_path[-1] == "/":
                creating_save_path = str(self.project.project_publishing_path) + '%s/%s/' % (
                canvas_name, template_name.strip().replace(" ", "_").lower())
            else:
                creating_save_path = str(self.project.project_publishing_path) + '/%s/%s/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())

        try:
            os.makedirs(creating_save_path)
        except:
            pass

        return creating_save_path

    def return_canvas_thumbnails_save_path(self, project_name, canvas_name, template_name):

        project_name = project_name.replace(" ", "_", 10)
        creating_save_path = 'project_images/%s/staging/%s/%s/thumbnails/' % (
        project_name, canvas_name, template_name.strip().replace(" ", "_", 10).lower())

        if self.project.project_publishing_path:
            if self.project.project_publishing_path[-1] == "/":
                creating_save_path = str(self.project.project_publishing_path) + '%s/%s/thumbnails/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())
            else:
                creating_save_path = str(self.project.project_publishing_path) + '/%s/%s/thumbnails/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())

        try:
            os.makedirs(creating_save_path)
        except:
            pass
        return creating_save_path

    def get_canvases_assets(self, canvas_id):
        selected_canvas = self.class_object("Canvases").filter(pk=canvas_id)
        if selected_canvas:
            selected_canvas = selected_canvas[0]

            canvas_wireframes = self.class_object("CanvasWireframes").filter(canvas=selected_canvas)

            if canvas_wireframes:
                canvas_wireframes = canvas_wireframes[0]
                model_dict = model_to_dict(canvas_wireframes)
                wire_parent_id = model_dict['wireframe']
                menus = self.class_object("Wireframes").filter(wireframe_parent_id=wire_parent_id).order_by('orderidx')

                menu_item_list = []
                for menu in menus:
                    list_items = self.class_object("Wireframes").filter(wireframe_parent=menu).order_by('orderidx')

                    for item in list_items:
                        dict_item = model_to_dict(item)

                        t_template = self.class_object("Templates").filter(pk=item.template_id)[0]
                        template_name = t_template.name

                        containers = []
                        template_types = self.class_object("TemplateTypeContainers").filter(template_type_id=t_template.template_type_id)
                        for ttype in template_types:
                            containers.append(ttype.container_id)

                        children_containers = self.class_object("Containers").filter(container_parent_id__in=containers)
                        for c in children_containers:
                            containers.append(c.pk)

                        assets = self.class_object("ContainerAssignedAssets").filter(container_id__in=containers,
                                                                                     template=t_template,
                                                                                     wireframe=item,
                                                                                     canvas_id=canvas_id).values_list('asset__source_id', flat=True)

                        resources = self.class_object("Resources").filter(pk__in=assets)
                        dict_item.update({'template_name': template_name, 'assets': resources, 'asset_count': len(resources)})

                        menu_item_list.append(dict_item)

                return menu_item_list

    def check_model_field_mapping(self, model_name, field_name, field_value=None):
        self.get_model_object(model_name)
        model_object = get_model(self.project.project_connection_name, model_name)
        model_fields_objects = model_object._meta.get_fields()

        names_model_object = self.get_model_object('Names')
        floors_model_object = self.get_model_object('Floors')
        property_types_model_object = self.get_model_object('Propertytypes')

        for field in model_fields_objects:
            if field.name == field_name and type(field) == ForeignKey and field_value:
                if field.rel.to == names_model_object:
                    name_string = names_model_object.objects.using(self.project.project_connection_name).get(id=int(field_value))
                    return True, name_string.english
                elif field.rel.to == floors_model_object:
                    floors_options = floors_model_object.objects.using(self.project.project_connection_name).all().\
                        values_list('pk', 'name__english').order_by('name__english')
                    return True, floors_options
                elif field.rel.to == property_types_model_object:
                    types_options = property_types_model_object.objects.using(self.project.project_connection_name).all().values_list('pk', 'type')
                    return True, types_options

        return False, []

    def get_model_field_mapping_and_value(self, model_name, field_name, field_value):
        self.get_model_object(model_name)
        model_object = get_model(self.project.project_connection_name, model_name)
        model_fields_objects = model_object._meta.get_fields()

        names_model_object = self.get_model_object('Names')
        floors_model_object = self.get_model_object('Floors')
        property_types_model_object = self.get_model_object('Propertytypes')

        for field in model_fields_objects:
            if field.name == field_name and type(field) == ForeignKey and field_value:
                if field.rel.to == names_model_object:
                    name_string = names_model_object.objects.using(self.project.project_connection_name).get(
                        id=int(field_value))
                    return True, name_string.english
                elif field.rel.to == floors_model_object:
                    floors_options = floors_model_object.objects.using(self.project.project_connection_name).filter(pk=field_value). \
                        values_list('name__english', flat=True).order_by('name__english')
                    if floors_options:
                        floors_options = floors_options[0]
                    return True, floors_options
                elif field.rel.to == property_types_model_object:
                    types_options = property_types_model_object.objects.using(
                        self.project.project_connection_name).filter(pk=field_value).values_list('type', flat=True)
                    if types_options:
                        types_options = types_options[0]
                    return True, types_options

        return False, []

    def save_apartment_strings_type_value(self, model_name, english_value, apartment_id, new_value, field_value=None):

        if not new_value:
            new_value = 0

        if model_name == self.project.apartment_model:
            names_model_object = self.get_model_object('Names')
            apartment_model_object = self.get_model_object(self.project.apartment_model)
            instance = self.get_model_queryset(field='id', value=apartment_id, list_needed=False)

            if field_value:
                model_fields_objects = apartment_model_object._meta.get_fields()
                for field in model_fields_objects:
                    if field.name == english_value and type(field) == ForeignKey and new_value:
                        if field.rel.to == names_model_object:
                            name_string = names_model_object.objects.using(self.project.project_connection_name).get(id=int(field_value))
                            if name_string and not str(name_string.english) == str(new_value):
                                name_string.english = new_value
                                name_string.save(using=self.project.project_connection_name)
                            return True

            try:
                setattr(instance, english_value, new_value)
                instance.save(using=self.project.project_connection_name)
            except:
                id_value = '%s_id' % english_value
                setattr(instance, id_value, new_value)
                instance.save(using=self.project.project_connection_name)

            return True

        if model_name == self.project.hotspot_model:
            self.get_model_object(self.project.hotspot_model)
            instance = self.get_model_queryset(field=self.project.apartment_model_id_field, value=apartment_id, list_needed=False)

            if instance and type(instance) == list and len(instance) > 1:
                instance = instance[0]

            try:
                setattr(instance, english_value, new_value)
                instance.save(using=self.project.project_connection_name)
            except:
                id_value = "%s_id" % english_value
                setattr(instance, id_value, new_value)
                instance.save(using=self.project.project_connection_name)

            return True

        self.get_model_object('Texttablecategories')
        table_category_mapping = self.get_mapping_table_value(model_name)

        category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)
        self.get_model_object('Strings')

        if category_queryset:
            category = category_queryset
            strings_queryset = self.project_model_object.objects.using(self.project.project_connection_name).filter(text_table_category=category,
                                                                                                                    english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]
                project_model_object = get_model(self.project.project_connection_name, model_name)

                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = project_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)

                if not queryset:
                    project_model_object.objects.using(self.project.project_connection_name).create(**kwargs)
                    queryset = project_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)

                if queryset:
                    instance = queryset[0]
                    setattr(instance, "value", new_value)
                    instance.save(using=self.project.project_connection_name)
                    return True

    def get_model_object_existing_fields(self, project=None, model_name=None, drop_id=True):

        if model_name:
            try:
                strings_model_object = get_model(project.project_connection_name, "Strings")
                extra_fields = []
                table_category_mapping = self.get_mapping_table_value(model_name)
                if table_category_mapping:
                    self.get_model_object('Texttablecategories')
                    category_queryset = self.get_model_queryset(field='category', value=table_category_mapping, list_needed=False)
                    strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(text_table_category=category_queryset)
                    for item in strings_queryset:
                        string_dict = model_to_dict(strings_model_object.objects.using(project.project_connection_name).get(pk=item.id))

                        if "english" in string_dict:
                            if string_dict['english']not in extra_fields:
                                extra_fields.append(string_dict['english'])

                return extra_fields

            except:
                return []
        else:
            self.model_fields = self.project_model_object._meta.get_fields()
            return self.filter_model_fields_for_form(drop_id=drop_id)

    def clean_apartment_ids(self, existing_apartment_id):
        self.get_model_object(self.project.apartment_model)
        related_field_queryset = self.get_model_queryset(field='id', value=existing_apartment_id, list_needed=True)

    def get_strings_instance(self, table_mapping, type_string_id):
        self.get_model_object('Strings')
        related_field_queryset = self.get_model_queryset(field='id', value=type_string_id, list_needed=True)
        return related_field_queryset[0]

    def clean_model_mapping_fields(self, project=None, model_fields=None, model_name=None, csv_column_name=None):
        if model_fields:
            extra_fields = self.get_model_object_existing_fields(project=project, model_name=model_name)

            has_string_model_fields = ['New Field:', ]
            updated_model_fields = []
            ignore_fields = ['apartment', 'property', 'value']

            add_strings = False
            for field in model_fields:
                if field not in ignore_fields:
                    if not field == "type_string":
                        updated_model_fields.append(field)
                    else:
                        add_strings = True

            if model_name == "Buildings":
                new_updated_model_fields = updated_model_fields

                updated_model_fields = []
                for field in new_updated_model_fields:
                    if field in ['name', "Name"]:
                        field = "building_name"
                    else:
                        field = field

                    updated_model_fields.append(field)

            if add_strings:
                return has_string_model_fields + updated_model_fields + extra_fields

            return updated_model_fields + extra_fields
        return model_fields

    def get_all_object_fields(self, drop_id=True):
        self.model_fields = self.project_model_object._meta.get_fields()
        return self.filter_model_fields_for_form(drop_id=drop_id, include_all=True)

    def get_model_object_field_names(self):
        return self.field_names

    def get_model_object_table_values(self):
        pass

    def get_image_location_configuration(self):
        pass

    def get_model_has_redirect(self):
        pass

    def filter_model_fields_for_form(self, drop_id=False, include_all=False):
        field_names = []
        for field in self.model_fields:

            if include_all:
               field_names.append(field.name)
            else:
                if drop_id:
                    if  not field.name == "id" and not type(field) == ManyToOneRel:
                        field_names.append(field.name)
                else:
                    if not include_all and not type(field) == ManyToOneRel and not type(field) == ForeignKey:
                        field_names.append(field.name)

        self.field_names = field_names

        return field_names

    def has_limited_import_schema(self, table_name):
        pass

    def get_all_property_types(self):

        types = self.class_object("PropertyTypes").all()
        return types

    def get_all_buildings(self, return_first=False):

        floor_buildings = self.class_object("FloorsBuildings").all().values_list('building_id', flat=True).distinct()
        queryset = self.class_object("Buildings").filter(pk__in=floor_buildings).order_by('name__english')
        # queryset = self.class_object("Buildings").all().order_by('name__english')

        if queryset and return_first:
            first_floor = queryset[0]
            return first_floor

        return queryset

    def get_all_floors(self, return_first=False, building_id=None):

        floor_buildings = []
        if building_id:
            floor_buildings = self.class_object("FloorsBuildings").filter(building_id=building_id).values_list('floor_id', flat=True).order_by('floor__orderidx')

        queryset = self.class_object("Floors").all().order_by('orderidx')

        if building_id:
            queryset = queryset.filter(pk__in=floor_buildings)

        result_list = list(queryset)

        if result_list and return_first:
            first_floor = result_list[0]
            return first_floor

        return result_list

    def get_selected_floor(self, floor_id):
        project_model_object = get_model(self.project.project_connection_name, "Floors")
        queryset = project_model_object.objects.using(self.project.project_connection_name).filter(id=floor_id).order_by('id')

        if queryset and floor_id:
            first_floor = queryset[0]
            return first_floor

        return queryset



    def get_project_image_directory(self, image_type='level'):
        if self.project:
            images = []
            image_types = ['gif', 'jpeg', 'jpg', 'png']

            if image_type == 'level' and self.project.level_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.level_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'level_full_res' and self.project.level_plan_location_full_res:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.level_plan_location_full_res)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'floor' and self.project.floor_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.floor_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'key' and self.project.key_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.key_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(os.path.join(path, item)) in image_types:
                            images.append(item)

            return images

    def get_new_schema_image_directory(self, project, image_type):

        asset_tag = self.class_object("AssetTags").filter(tag=image_type)
        tagged_assets = self.class_object("AssetsTagged").filter(asset_tag=asset_tag).values_list('asset_id', flat=True)
        assets = self.class_object("Assets").filter(pk__in=tagged_assets).values_list('source_id', flat=True)
        images = self.class_object("Resources").filter(pk__in=assets)

        # ##print ('asset_tag', asset_tag, tagged_assets, assets, 'images', images
        # ##print ('-----'

        selected_images = []
        for image in images:
            image_types = ['gif', 'jpeg', 'jpg', 'png', 'tif']
            path = image.path
            path = path.replace("/project_data/project_data/", "/project_data/")
            image_path = path.replace(settings.BASE_IMAGE_LOCATION, "")
            image_path = image_path.replace("/project_data/project_data/", "/project_data/")

            name = image_path.split("/")
            if os.path.isfile(path):
                if imghdr.what(path) in image_types:
                    static_path = '/static/' + str(image_path)
                    image_dict = {'name': name[-1], 'image_path': image_path, 'path': path, 'static_path': static_path, 'resource_id': image.pk}
                    if image_dict not in selected_images:
                        selected_images.append(image_dict)

        return selected_images

    def copy_apartment_hotspot_values(self, selected_apartment, apartment_id, floor_id=None):
        apartment_hot_spot = self.class_object("PropertyHotspots").filter(property_id=apartment_id)
        if apartment_hot_spot:
            apartment_hot_spot = apartment_hot_spot[0]
            new_spot = apartment_hot_spot
            new_spot.pk = None
            new_spot.property_id = selected_apartment
            new_spot.save(using=self.project.project_connection_name)
            new_spot = self.class_object("PropertyHotspots").filter(property_id=selected_apartment)[0]
            return new_spot

    def get_apartment_hotspot_values(self, property_id, floor_id=None):
        apartment_hot_spot = self.class_object("PropertyHotspots").filter(property_id=property_id)

        picking_coord_x = None
        picking_coord_y = None

        if apartment_hot_spot:
            apartment_hot_spot = apartment_hot_spot[0]

            linked_floor_spot = self.class_object("FloorsHotspots").filter(pk=apartment_hot_spot.floors_hotspot_id)

            if floor_id:
                linked_floor_spot = linked_floor_spot.filter(floor_id=floor_id)

            if linked_floor_spot:
                model_dict = model_to_dict(linked_floor_spot[0])

                if "x" in model_dict and "y" in model_dict:
                    picking_coord_x = model_dict['x']
                    picking_coord_y = model_dict['y']
                else:
                    picking_coord_x = None
                    picking_coord_y = None

        return picking_coord_x, picking_coord_y

    def get_hotspot_values(self, property_id, floor_id):

        picking_coord_x = None
        picking_coord_y = None
        floor_hotspot_id = None
        orderidx = 1

        linked_floor_spots = self.class_object("FloorsHotspots").filter(floor_id=floor_id).values_list('pk', flat=True)
        apartment_hot_spots = self.class_object("PropertyHotspots").filter(property_id=property_id,
                                                                           floors_hotspot_id__in=linked_floor_spots)

        hotspot_list = []
        if apartment_hot_spots:
            for apartment in apartment_hot_spots:
                floors_hotspot_id = apartment.floors_hotspot_id
                if apartment.orderidx:
                    orderidx = apartment.orderidx

                linked_floor_spot = self.class_object("FloorsHotspots").filter(floor_id=floor_id, pk=floors_hotspot_id)
                for hotspot in linked_floor_spot:

                    model_dict = model_to_dict(hotspot)

                    if "x" in model_dict and "y" in model_dict:
                        picking_coord_x = model_dict['x']
                        picking_coord_y = model_dict['y']
                        hotspot_order = orderidx
                    else:
                        picking_coord_x = None
                        picking_coord_y = None
                        hotspot_order = orderidx

                    hotspot_list.append({'picking_coord_x': picking_coord_x,
                                         'picking_coord_y': picking_coord_y,
                                         'floor_hotspot_id': hotspot.pk,
                                         'hotspot_order': hotspot_order})

        else:
            hotspot_list.append({'picking_coord_x': picking_coord_x,
                                 'picking_coord_y': picking_coord_y,
                                 'floor_hotspot_id': floor_hotspot_id,
                                 'hotspot_order': orderidx})
        return hotspot_list

    def save_apartment_hotspot_values(self, property, x_coordinate, y_coordinate, floor_id=0, new_hotspot_name='', hotspot_order=None):


        spots = self.class_object("FloorsHotspots").filter(floor_id=floor_id).values_list('pk', flat=True)

        apartment_hot_spot = None
        if property:
            if type(property) == int:
                apartment_hot_spot = self.class_object("PropertyHotspots").filter(property_id=property,
                                                                                  floors_hotspot_id__in=spots)
            else:
                apartment_hot_spot = self.class_object("PropertyHotspots").filter(property_id=property.pk,
                                                                                  floors_hotspot_id__in=spots)
                property = property.pk

        #print ('apartment_hot_spot', apartment_hot_spot

        if not apartment_hot_spot and property:
            if new_hotspot_name:
                ##print (1
                self.class_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]


            else:
                ##print (2
                self.class_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name='')

                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name='').order_by('-pk')[0]

            self.class_object("PropertyHotspots").create(property_id=property,
                                                         orderidx=hotspot_order,
                                                         floors_hotspot_id=new_hotspot.pk)

            hotspot = self.class_object("PropertyHotspots").filter(property_id=property,
                                                                   floors_hotspot_id=new_hotspot.pk).order_by('-pk')[0]

            return hotspot

        elif apartment_hot_spot and property:
            #print (3

            apartment_hot_spot = apartment_hot_spot[0]

            updating_hotspots = self.class_object("FloorsHotspots").filter(pk=apartment_hot_spot.floors_hotspot_id,
                                                                           floor_id=floor_id)
            if updating_hotspots:
                #print ('updating_hotspots', updating_hotspots

                floor_hotspot = updating_hotspots[0]
                floor_hotspot.x = x_coordinate
                floor_hotspot.y = y_coordinate
                floor_hotspot.save(using=self.project.project_connection_name)

                hotspot = self.class_object("PropertyHotspots").filter(property_id=property,
                                                                       floors_hotspot_id=floor_hotspot.pk)

                if not hotspot:
                    self.class_object("PropertyHotspots").create(property_id=property,
                                                                 orderidx=hotspot_order,
                                                                 floors_hotspot_id=floor_hotspot.pk)

                    hotspot = self.class_object("PropertyHotspots").filter(property_id=property,
                                                                           floors_hotspot_id=floor_hotspot.pk)

                #print ('hotspot', hotspot
                if hotspot:
                    hotspot = hotspot[0]
                    if hotspot_order:
                        hotspot.orderidx = hotspot_order
                        hotspot.save(using=self.project.project_connection_name)
                return hotspot
            else:
                #print ('creating new with property'
                if not new_hotspot_name:
                    new_hotspot_name = ''

                self.class_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]


                self.class_object("PropertyHotspots").create(property_id=property,
                                                             floors_hotspot_id=new_hotspot.pk)

                hotspot = self.class_object("PropertyHotspots").filter(property_id=property,
                                                                       floors_hotspot_id=new_hotspot.pk).order_by('-pk')[0]

                if hotspot_order:
                    hotspot.orderidx = hotspot_order
                    hotspot.save(using=self.project.project_connection_name)

                return hotspot

        elif not apartment_hot_spot and not property:
            if new_hotspot_name:
                self.class_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]
            else:
                self.class_object("FloorsHotspots").create(floor_id=floor_id, x=x_coordinate, y=y_coordinate)
                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id, x=x_coordinate, y=y_coordinate).order_by('-pk')[0]

            return new_hotspot

        else:
            ##print (5
            if new_hotspot_name:
                self.class_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.class_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]

                return new_hotspot

    def get_property_image(self, apartment_id, image_type_string):
        image_type_string = image_type_string.replace(" ", "_").lower()
        property_images = self.class_object("PropertyResources").filter(property_id=apartment_id, key=image_type_string).values_list("resource_id", flat=True)

        images = self.class_object("Resources").filter(pk=property_images)
        return images

    def get_image_tag_list(self, resource_id):
        this_image_assets = self.class_object("Assets").filter(source_id=resource_id).values_list('pk', flat=True)
        this_image_asset_with_tag = self.class_object("AssetsTagged").filter(asset_id=this_image_assets).values_list('asset_tag_id', flat=True)
        this_image_assets_tags = self.class_object("AssetTags").filter(pk__in=this_image_asset_with_tag).exclude(tag__in=self.ignore_list()).values_list('tag', flat=True)
        return this_image_assets_tags

    def get_property_aspects(self, property_id):
        aspects = ''
        images = self.class_object("PropertyAspects").filter(property_id=property_id)
        if images:
            aspects = images

        return aspects

    def get_all_aspects(self):
        images = self.class_object("PropertyAspects").all().values_list('key', flat=True).order_by('key').distinct()
        aspects = ','.join(images)

        return aspects

    def get_first_editable_template(self):
        from core.models import ProjectCSVTemplate

        editable_templates = ProjectCSVTemplate.objects.filter(project=self.project,
                                                               is_editable_template=True).order_by('-is_editable_template_default', 'id')

        if not editable_templates:
            editable_templates = ProjectCSVTemplate.objects.filter(project=self.project).order_by('-is_editable_template_default', 'id')

        if editable_templates:
            return editable_templates[0]

    def get_property_aspects_floor(self, floor_id):
        directions = ['north', 'south', 'east', 'west']
        aspects = self.class_object("FloorsPlans").filter(floor_id=floor_id, key__in=directions)
        return aspects

    def remove_property_resource_by_id(self, aspect_id=None):

        if aspect_id:
            self.class_object("PropertyResources").filter(pk=aspect_id).delete()

    def remove_property_resource(self, aspect_id):

        if aspect_id:
            self.class_object("PropertyResources").filter(pk=aspect_id).delete()

    def remove_floor_resource_by_id(self, aspect_id):

        if aspect_id:
            self.class_object("FloorsPlans").filter(pk=aspect_id).delete()

    def remove_property_aspect_by_id(self, aspect_id=None):

        if aspect_id:
            self.class_object("PropertyAspects").filter(pk=aspect_id).delete()

    def remove_property_aspect(self, property_id, aspect, aspect_id=None):

        if aspect_id:
            self.class_object("PropertyAspects").filter(pk=aspect_id).delete()
        else:
            self.class_object("PropertyAspects").filter(property_id=property_id, key__contains=aspect).delete()

    def get_next_aspect(self, property_id):
        aspects = self.get_property_aspects(property_id)

        directions = ['north', 'south', 'east', 'west']

        has_items = []
        for in_aspect in aspects:
            has_items.append(in_aspect.key)

        doesnt_have = list(set(directions) - set(has_items))

        if doesnt_have:
            return doesnt_have[0]

    def get_next_aspect_floor(self, floor_id):
        aspects = self.get_property_aspects_floor(floor_id)

        directions = ['north', 'south', 'east', 'west']

        has_items = []
        for in_aspect in aspects:
            has_items.append(in_aspect.key)

        doesnt_have = list(set(directions) - set(has_items))

        if doesnt_have:
            return doesnt_have[0]

    def add_floor_aspect(self, floor_id, resource_id):

        self.class_object("FloorsPlans").create(floor_id=floor_id,
                                                resource_id=resource_id,
                                                key='floor_view')

        aspect = self.class_object("FloorsPlans").filter(floor_id=floor_id,
                                                         resource_id=resource_id,
                                                         key='floor_view')
        return  aspect[0]



    def get_or_create_property_string_field(self, property_id, english_value):

        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')
        property_category_string = self.get_text_table_category(mapping_table_value)
        string_field = self.class_object("Strings").filter(text_table_category=property_category_string, english__iexact=english_value)

        if not string_field:
            self.class_object("Strings").create(text_table_category=property_category_string, english=english_value)
            string_field = self.class_object("Strings").filter(text_table_category=property_category_string, english__iexact=english_value)

        string_field = string_field[0]
        property_strings = self.class_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field)

        if not property_strings:
            self.class_object("PropertiesStringFields").create(property_id=property_id, type_string=string_field)
            property_strings = self.class_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field)

        if property_strings:
            for string in property_strings:
                string.text_table_category=property_category_string
                string.save(using=self.project.project_connection_name)

        return property_strings

    def delete_property_string_field(self, property_id, english_value):

        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')
        property_category_string = self.get_text_table_category(mapping_table_value)
        string_field = self.class_object("Strings").filter(text_table_category=property_category_string, english__iexact=english_value)

        if not string_field:
            return

        string_field = string_field[0]
        self.class_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field).delete()


    def find_property_string(self, property_id, english_value):
        string_field = self.class_object("Strings").filter(english__icontains=english_value)

        if string_field:
            string_field = string_field[0]

            property_strings = self.class_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field).exclude(value='')
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

            property_strings = self.class_object("PropertiesIntFields").filter(property_id=property_id, type_string=string_field)
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

            property_strings = self.class_object("PropertiesFloatFields").filter(property_id=property_id, type_string=string_field)
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

    def get_all_property_typicals(self):
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.class_object("Strings").filter(text_table_category=property_category_string)

        type_list = []
        new_list = []
        new_list_dict = {}
        static_image = '/static/common/img/placeholder.jpg'

        for string_field in string_fields:
            property_strings = self.class_object("PropertiesStringFields").filter(type_string=string_field)

            image_id = ''

            if property_strings:
                for row in property_strings:
                    image_name = 'n/a'
                    image = self.class_object("PropertyResources").filter(property_id=row.property_id, key="floor_plans")

                    if image:
                        pimage = self.class_object("Resources").filter(pk=image[0].resource_id)
                        if pimage:
                            # print ('image', pimage[0].path, row.property_id

                            image = pimage[0].path
                            image_name = str(image.split("/")[-1]).lstrip().rstrip()
                            image_id = pimage[0].pk

                            static_image = '/static/' + str(pimage[0].path.replace(settings.BASE_IMAGE_LOCATION, ''))
                            static_image = static_image.replace("//", "/")

                    # print ('row.value', row.value
                    if row.value and row.value.strip() not in type_list:
                        type_list.append(row.value.strip())
                        new_list.append({row.value.strip():
                                             {'type': row.value.strip(),
                                              'id': row.pk,
                                              'string_id': row.pk,
                                              'image': image_name,
                                              'image_id': image_id,
                                              'static_image': static_image}
                                         })

                        new_list_dict.update(
                            {row.value.strip():
                                 {'type': row.value.strip(),
                                  'id': row.pk,
                                  'string_id': row.pk,
                                  'image': image_name,
                                  'image_id': image_id,
                                  'static_image': static_image}
                             }
                        )

        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.class_object("Strings").filter(text_table_category=property_category_string)

        for string_field in string_fields:
            if string_field.english and string_field.english.strip() not in type_list \
                    and not string_field.english.strip() == "floor_plan_typical":

                type_list.append(string_field.english.strip())
                new_list.append({string_field.english.strip():
                                {'type': string_field.english.strip(),
                                 'id': string_field.pk,
                                 'string_id': string_field.pk,
                                 'image': 'n/a',
                                 'image_id': '',
                                 'static_image': static_image}})

                new_list_dict.update(
                     {string_field.english.strip():
                     {'type': string_field.english.strip(),
                      'id': string_field.pk,
                      'string_id': string_field.pk,
                      'image': 'n/a',
                      'image_id': '',
                      'static_image': static_image}}
                )

        sorted_list = []
        for row in new_list_dict:
            found_value = new_list_dict[row]
            if found_value:
                sorted_list.append(found_value)

        return sorted_list

    def get_all_property_typical_strings(self):
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)

        string_fields = self.class_object("Strings").filter(category=property_category_string)
        if not string_fields:
            self.class_object("Strings").create(category=property_category_string)
            string_fields = self.class_object("Strings").filter(category=property_category_string)

        return string_fields
        
    def get_text_table_category(self, text_category):
        try:
            property_typical_string  = self.class_object("TextTableCategories").filter(category=text_category)
            if not property_typical_string:
                self.class_object("TextTableCategories").create(category=text_category)
                property_typical_string  = self.class_object("TextTableCategories").filter(category=text_category)

            property_typical_string = property_typical_string[0]

            return property_typical_string
        except Exception as e:
            print ('get_text_table_category error', e)
    
    def add_new_property_typical_string(self, string_value):
        
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.class_object("Strings").filter(text_table_category=property_category_string, english=string_value)
        if not string_fields:
            self.class_object("Strings").create(text_table_category=property_category_string, english=string_value)
            string_fields = self.class_object("Strings").filter(text_table_category=property_category_string, english=string_value)

        return string_fields[0]

    def remove_new_property_typical_string(self, string_value_id):

        string_fields = self.class_object("Strings").filter(english=settings.FLOOR_PLAN_TYPICAL)

        for floor_plan_typical in string_fields:
            self.class_object("PropertiesStringFields").filter(type_string=floor_plan_typical, value=string_value_id).delete()

        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        self.class_object("Strings").filter(text_table_category=property_category_string, english=string_value_id).delete()

    def add_typical_string_to_property(self, property_id, set_string):
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)

        self.class_object("PropertiesStringFields").filter(property_id=property_id,
                                                           type_string__text_table_category=property_category_string).delete()

        if set_string:
            string_field = self.class_object("Strings").filter(english=set_string,
                                                               text_table_category=property_category_string)

            if not string_field:
                self.class_object("Strings").create(english=set_string, text_table_category=property_category_string)
                string_field = self.class_object("Strings").filter(english=set_string, text_table_category=property_category_string)

            string_field = string_field[0]
            property_strings = self.class_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field)

            if property_strings:
                property_strings = property_strings[0]
                property_strings.value = set_string
                property_strings.save(using=self.project.project_connection_name)
            else:
                self.class_object("PropertiesStringFields").create(type_string=string_field, property_id=property_id, value=set_string)

            return True

    def get_property_typicals(self, property_id):
        #check
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.class_object("Strings").filter(text_table_category=property_category_string)
        for string_field in string_fields:
            property_strings = self.class_object("PropertiesStringFields").filter(property_id=property_id,
                                                                                  type_string=string_field)

            if property_strings:
                property_strings = property_strings[0]
                if property_strings.value:
                    return property_strings.value

    def update_with_property_typicals(self, type_string, resource_id):
        type_string = type_string.lstrip().rstrip()

        asset_id = resource_id
        resource_asset = self.class_object("Assets").filter(source_id=resource_id)
        if resource_asset:
            resource_asset = resource_asset[0]
            asset_id = resource_asset.pk

        id_string_field_field = self.class_object("Strings").filter(english="floor_plan_image_id")
        if not id_string_field_field:
            self.class_object("Strings").create(english="floor_plan_image_id")
            id_string_field_field = self.class_object("Strings").filter(english="floor_plan_image_id")

        id_string_field_field = id_string_field_field[0]
        property_category_string = self.get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.class_object("Strings").filter(text_table_category=property_category_string)

        property_strings = None
        for string_field in string_fields:
            property_strings = self.class_object("PropertiesStringFields").filter(type_string=string_field,
                                                                                  value=type_string)

            if property_strings:
                for property in property_strings:
                    property_images = self.class_object("PropertyResources").filter(property_id=property.property_id,
                                                                                    key="floor_plans")
                    if not property_images:
                        self.class_object("PropertyResources").create(property_id=property.property_id,
                                                                      key="floor_plans",
                                                                      resource_id=resource_id)

                    else:
                        for prop in property_images:
                            prop.resource_id = resource_id
                            prop.save(using=self.project.project_connection_name)

                    # Updating all int fields for this type for Aren
                    int_field_image = self.class_object("PropertiesIntFields").filter(property_id=property.property_id,
                                                                                      type_string=id_string_field_field)

                    if not int_field_image:
                        self.class_object("PropertiesIntFields").create(property_id=property.property_id,
                                                                        type_string=id_string_field_field,
                                                                        value=asset_id)
                    else:
                        int_field_image = int_field_image[0]
                        int_field_image.value = asset_id
                        int_field_image.save(using=self.project.project_connection_name)

        if property_strings:
            return True

        return False

    def ignore_list(self):
        return ['level_plans', 'key_plans', 'floor_plans', 'Floor Plans', 'Key Plans', 'Level Plans']
    
    def get_property_image_all(self):

        assets_tags = self.class_object("AssetTags").filter(tag__icontains="type")
        assets_tags = assets_tags.exclude(tag__in=self.ignore_list()).values_list('pk', flat=True)

        asset_with_tag = self.class_object("AssetsTagged").filter(asset_tag=assets_tags).values_list('asset_id', flat=True)
        assets = self.class_object("Assets").filter(pk__in=asset_with_tag).values_list('source_id', flat=True)
        images = self.class_object("Resources").filter(pk__in=assets)
            
        image_list = []
        image_names = []

        for image in images:            
            image_name = str(image.path.split("/")[-1]).lstrip().rstrip()

            this_image_assets = self.class_object("Assets").filter(source_id=image.pk).values_list('pk', flat=True)
            this_image_asset_with_tag = self.class_object("AssetsTagged").filter(asset_id=this_image_assets).values_list('asset_tag_id', flat=True)
            this_image_assets_tags = self.class_object("AssetTags").filter(pk__in=this_image_asset_with_tag).exclude(tag__in=self.ignore_list()).values_list('tag', flat=True)

            if image_name not in image_names:
                image_names.append(image_name)
                image_list.append({'name': image_name,
                                   'path': image.path,
                                   'resource_id': image.pk,
                                   'tags': this_image_assets_tags})
        return image_list

    def check_if_image_name_in_property_list(self, new_image_name, new_image_id):
        property_images = self.class_object("PropertyResources").all().exclude(key="key_plans")

        image_names = []

        for proprty_image in property_images:
            image = self.class_object("Resources").filter(pk=proprty_image.resource_id)
            image_name = str(image[0].path.split("/")[-1]).lstrip().rstrip()

            if image_name not in image_names:
                image_names.append(image_name)

        if new_image_name not in image_names:
            return True

    def get_string_field_id(self, english_value):
        self.get_model_object('Texttablecategories')
        category_queryset = self.get_model_queryset(field='category', value='apartment_strings', list_needed=False)
        self.get_model_object('Strings')

        if category_queryset:
            category = category_queryset

            queryset = self.project_model_object.objects.using(self.project.project_connection_name).filter(text_table_category=category,
                                                                                                            english=english_value)

            if queryset:
                return queryset[0]
            else:
                self.project_model_object.objects.using(self.project.project_connection_name).create(text_table_category=category, english=english_value)
                item_created = self.project_model_object.objects.using(self.project.project_connection_name).filter(text_table_category=category, english=english_value)
                item_created = item_created[0]
                return item_created


    def get_image_type_id(self, image_type_string, image_mode, image_output):

        image_type_string_id = self.get_string_field_id(image_type_string)
        image_mode_string_id = self.get_string_field_id(image_mode)
        image_output_string_id = self.get_string_field_id(image_output)

        kwargs = {'{0}'.format('output_string'): image_output_string_id,
                  '{0}'.format('type_string'): image_type_string_id,
                  '{0}'.format('mode_string'): image_mode_string_id}

        try:
            image_types_model_object = self.get_model_object('Imagetypes')
            queryset = image_types_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)

            if not queryset:
                image_types_model_object.objects.using(self.project.project_connection_name).create(**kwargs)
                image_type = image_types_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)
                image_type = image_type[0]
            else:
                image_type = queryset[0]

            return image_type
        except:
            pass

    def get_description_field_id(self, english_value):
        project_model_object = self.get_model_object('Descriptions')

        english_value = str(english_value).lstrip().rstrip()
        queryset = self.get_model_queryset(field='english', value=english_value, list_needed=True)
        if queryset:
            return queryset[0]
        else:
            project_model_object.objects.using(self.project.project_connection_name).create(english=english_value)
            item_created = project_model_object.objects.using(self.project.project_connection_name).filter(english=english_value)
            item_created = item_created[0]
            return item_created

    def get_image(self, project, image_type_description, image_path):
        project_model_object = self.get_model_object("Resources")

        kwargs = {
            '{0}'.format('path'): image_path,
            '{0}'.format('description'): image_type_description,
            '{0}'.format('size'): 0
        }

        queryset = project_model_object.objects.using(project.project_connection_name).filter(**kwargs)

        if not queryset:
            project_model_object.objects.using(project.project_connection_name).create(**kwargs)
            queryset = project_model_object.objects.using(project.project_connection_name).filter(**kwargs)

        if queryset:
            return queryset[0]


    def get_floor_level_plan(self, floor_id):
        plans = self.class_object("FloorsPlans").filter(floor_id=floor_id, key="level_plans").values_list("resource_id", flat=True)

        images = self.class_object("Resources").filter(pk=plans)
        if images:
            plan = images[0]
            return plan

    def get_floor_level_image(self, floor_id):
        plans = self.class_object("FloorsPlans").filter(floor_id=floor_id, key="level_plans").values_list("resource_id",                                                                                                                                                  flat=True)
        images = self.class_object("Resources").filter(pk=plans)

        if images:
            plan = images[0]
            image_path = plan.path
            resource_id = plan.pk
            image_exists = True
            image_name = plan.path.split("/")[-1]

            if image_path:
                if not os.path.isfile(image_path):
                    static_image = '/static/common/img/placeholder.jpg'
                    image_path = '<em>This floor has no image selected (placeholder displayed).</em>'
                    image_name = ''
                else:
                    image_name = image_path.split("/")[-1]
                    static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                    static_image = static_image.replace("//", "/")
                    image_path = str(image_path)

            return image_name

    def get_floor_image(self, project, image_type_description, floor_id, image_type_id, image_path):
        project_model_object = self.get_model_object('Floorsplans')

        kwargs = {
            '{0}'.format('floor_id'): floor_id,
            '{0}'.format('image_type_id'): image_type_id,
        }

        try:
            queryset = project_model_object.objects.using(project.project_connection_name).filter(**kwargs)
            if queryset:
                return queryset[0]

            else:
                project_model_object = self.get_model_object("Resources")

                kwargs = {
                    '{0}'.format('path'): image_path,
                    '{0}'.format('description'): image_type_description,
                    '{0}'.format('size'): 0
                }

                queryset = project_model_object.objects.using(project.project_connection_name).filter(**kwargs)

                if not queryset:
                    project_model_object.objects.using(project.project_connection_name).create(**kwargs)
                    queryset = project_model_object.objects.using(project.project_connection_name).filter(**kwargs)

                if queryset:
                    return queryset[0]
        except:
            pass

    def create_new_apartment_image(self, project, apartment_id, image_type_description, image_type_id, path):

        description = self.get_description_field_id(image_type_description)

        if image_type_id:
            project_model_object = self.get_model_object('Apartmentsplans')

            queryset = project_model_object.objects.using(self.project.project_connection_name).filter(apartment_id=apartment_id,
                                                                                                       image_type_id=image_type_id)

            if not queryset:
                project_model_object = self.get_model_object('Resources')
                queryset = project_model_object.objects.using(self.project.project_connection_name).filter(path=path, description=description)

                if not queryset:

                    project_model_object.objects.using(self.project.project_connection_name).create(path=path, description=description, size=0)
                    queryset = project_model_object.objects.using(self.project.project_connection_name).filter(path=path, description=description)

                if queryset:
                    image = queryset[0]

                    project_model_object = self.get_model_object('Apartmentsplans')
                    queryset = project_model_object.objects.using(self.project.project_connection_name).filter(apartment_id=apartment_id,
                                                                                                               image_type_id=image_type_id,
                                                                                                               resource_id=image.pk)

                    if not queryset:
                        project_model_object = self.get_model_object('Apartmentsplans')
                        project_model_object.objects.using(self.project.project_connection_name).create(apartment_id=apartment_id,
                                                                                                        image_type=image_type_id,
                                                                                                        resource_id=image.pk)

                        queryset = project_model_object.objects.using(self.project.project_connection_name).filter(apartment_id=apartment_id,
                                                                                                                   image_type=image_type_id,
                                                                                                                   resource_id=image.pk)
                    if queryset:
                        project_model_object = self.get_model_object('Resources')
                        queryset = project_model_object.objects.using(self.project.project_connection_name).filter(id=image.pk)

                        if queryset:
                            return queryset[0]

            if queryset:
                apartment_plan = queryset[0]

                self.get_model_object('Resources')
                queryset = self.project_model_object.objects.using(self.project.project_connection_name).filter(id=apartment_plan.resource_id, path=path)

                if queryset:
                    image = queryset[0]
                    image.path = path
                    image.save(using=project.project_connection_name)
                    return image
                else:
                    project_model_object = self.get_model_object('Resources')
                    queryset = project_model_object.objects.using(self.project.project_connection_name).filter(id=apartment_plan.resource_id)

                    if queryset:
                        image = queryset[0]
                        image.path = path
                        image.save(using=project.project_connection_name)
                        return image
                    else:
                        project_model_object.objects.using(self.project.project_connection_name).create(path=path, description=description, size=0)
                        queryset = project_model_object.objects.using(self.project.project_connection_name).filter(path=path, description=description, size=0)

                        if queryset:
                            image = queryset[0]
                            apartment_plan.resource_id = image.pk
                            apartment_plan.save(using=project.project_connection_name)
                            return image

        return None

    def get_item_name(self, item, item_id):
        self.get_model_object(item)
        item_object = self.get_model_queryset(field='id', value=item_id)

        if item_object:
            self.get_model_object('Names')
            item_object = self.get_model_queryset(field='id', value=item_object.name_id)
            return str(item_object.english)


    def class_object(self, class_name):
        model_object = self.get_model_object(class_name)
        model_objects = model_object.objects.using(self.project.project_connection_name)

        return model_objects


class CreatePermissionMixin(object):

    def check_user(self, user):
        return user

    def check_user_add_permissions(self, user):
        return user.user_type in [user.USER_TYPE_SUPER_ADMIN, ]

    def can_create_project(self, user):
        return self.check_user(user) and self.check_user_add_permissions(user)


def sortkeypicker(keynames):
    negate = set()
    for i, k in enumerate(keynames):
        if k[:1] == '-':
            keynames[i] = k[1:]
            negate.add(k[1:])
    def getit(adict):
       composite = [adict[k] for k in keynames]
       for i, (k, v) in enumerate(zip(keynames, composite)):
           if k in negate:
               composite[i] = -v
       return composite
    return getit


exitFlag = 0


class MyThread(threading.Thread):
    def __init__(self, threadID, results, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.results = results
        self.counter = counter

    def run(self):
        # print ("Starting " + self.results
        threadLock = threading.Lock()

        # Get lock to synchronize threads
        threadLock.acquire()
        #print_time(self.name, self.counter, 3)
        # Free lock to release next thread
        threadLock.release()


def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit()

        #time.sleep(delay)

        #print ("%s: %s" % (threadName, time.ctime(time.time()))
        counter -= 1