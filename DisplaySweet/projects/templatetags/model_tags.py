from django import template
from projects.permissions import ProjectModelMixin
from projects.models.model_mixin import PrimaryModelMixin
import os
from django.conf import settings

from datetime import date
from datetime import datetime
register = template.Library()


@register.filter(name='date_to_seconds')
def date_to_seconds(t):

    try:
        if t:
            ttime = datetime.combine(t, datetime.min.time())
            return (ttime - datetime(1970, 1, 1)).total_seconds()
    except Exception as e:
        return t
    return 0

@register.filter(name='show_nice_key')
def show_nice_key(key):

    try:
        key = str(key).capitalize()
        nice_key = key.replace("_", " ")
        return nice_key
    except Exception as e:
        return key

@register.filter(name='project_properties')
def project_properties(request, project):
    model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)

    return project.property_count(model_mixin)


@register.filter(name='project_reserved_count')
def project_reserved_count(request, project):
    model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)
    return project.reserved_count(model_mixin)


@register.filter(name='project_available_count')
def project_available_count(request, project):
    model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)
    return project.available_count(model_mixin)


@register.filter(name='project_sold_count')
def project_sold_count(request, project):
    model_mixin = PrimaryModelMixin(request=request, project_id=project.pk)
    return project.sold_count(model_mixin)


@register.filter(name='model_name_filter')
def model_name_filter(obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """
    model_name = str(obj.__class__.__name__).capitalize()

    return model_name


@register.filter(name='column_row_value')
def column_row_value(obj, column_name):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    model_name = str(obj.__class__.__name__).capitalize()

    return model_name


@register.filter(name='column_field_matching')
def column_field_matching(column_name, field_name):
    """
    Tries to find a matching table field name to the csv column being imported

    Arguments: The model object, table column name, csv field name
    """
    if field_name == column_name:
        return True

    if str(field_name) in str(column_name):
        return True

    if str(column_name) in str(field_name):
        return True

    if "_" in column_name:
        field_split = column_name.split("_")
        for item in field_split:
            if str(item) in str(field_name):
                return True

    return False


@register.filter(name="get_form_assignment")
def get_form_assignment(model_instance, project_id):
    from projects.forms import get_assign_apartment_form

    project_mixin = ProjectModelMixin()
    project = project_mixin.get_project(project_id)
    project_model_object = project_mixin.get_model_object(project.apartment_model)

    apartments = project_mixin.get_model_queryset('floor_id', model_instance.floor_id)

    form_class = get_assign_apartment_form(project_model_object,  model_instance, apartments)
    form = form_class(None, None)

    return form


@register.filter(name="check_field_selected")
def check_field_selected(field, field_names):
    if field in field_names:
        return True


@register.filter(name="show_floor_plan_image_name")
def show_floor_plan_image_name(project, floor):

    project_mixin = ProjectModelMixin()
    project_mixin.get_project(project.pk)
    project_mixin.get_model_object("Resources")

    try:
        image = project_mixin.get_model_queryset('id', floor.floor_plate_image_id, list_needed=False)
        return image.highres_path.split("/")[-1]
    except:
        return None


@register.filter(name='get_item_name')
def get_item_name(project, model_instance):

    project_mixin = ProjectModelMixin()
    project_mixin.get_project(project.pk)

    try:
        project_model_object = project_mixin.get_model_object_and_row("Names", model_instance.name_id)
        if project_model_object:
            return str(project_model_object.english)
    except:
        try:
            name = model_instance.name
            return name
        except:
            return None


@register.filter(name='one_more')
def one_more(object, item):
    return object, item


@register.filter(name="level_image_exists_on_path")
def level_image_exists_on_path(project, image):
    image_name = '%s%s' % (project.level_plan_location, image)

    path = os.path.join(settings.BASE_IMAGE_LOCATION, image_name)
    path = path.replace("/project_data/project_data/", "/project_data/")

    if os.path.isfile(path):
        return image_name


@register.filter(name="floor_image_exists_on_path")
def floor_image_exists_on_path(project, image):

    image_name = '%s%s' % (project.floor_plan_location, image)
    path = os.path.join(settings.BASE_IMAGE_LOCATION, image_name)
    path = path.replace("/project_data/project_data/", "/project_data/")

    if os.path.isfile(path):
        return image_name


@register.filter(name="key_image_exists_on_path")
def key_image_exists_on_path(project, image):

    image_name = '%s%s' % (project.key_plan_location, image)
    path = os.path.join(settings.BASE_IMAGE_LOCATION, image_name)
    path = path.replace("/project_data/project_data/", "/project_data/")

    if os.path.isfile(path):
        return image_name


@register.filter(name="image_exists")
def image_exists(project, image):
    if os.path.isfile(image):
        return True


@register.filter(name="image_exists_on_ns_path")
def image_exists_on_ns_path(image_path, image):

    image_name = '%s%s' % (image_path, image)
    if os.path.isfile(image):
        return image_name

@register.filter(name='prettify_select_option')
def prettify_select_option(obj):
    """
    Gets the name of the model being viewed

    Argument: The model object
    """

    if obj:
        nice_string = obj.replace("_", " ")
        return nice_string


@register.filter(name='check_field_id')
def check_field_id(value, field):
    if field == "value":
        return False

    if type(value) == int:
        return True


@register.filter(name='show_names_object')
def show_names_object(project_field, instance):

    project, field = project_field

    if instance['fields']:
        value = instance['fields'][field]

        if field == "name":
            model_name = "Names"

            project_mixin = ProjectModelMixin()
            project_mixin.get_project(project.pk)

            try:
                project_model_object = project_mixin.get_model_object_and_row(model_name, value)

                if project_model_object:
                    return str(project_model_object.english)
            except:
                return value

        if "_string" in field:
            model_name = "Strings"

            project_mixin = ProjectModelMixin()
            project_mixin.get_project(project.pk)

            try:
                project_model_object = project_mixin.get_model_object_and_row(model_name, value)

                if project_model_object:
                    return str(project_model_object.english)
            except:
                return value

        return value


@register.filter(name='show_string_object')
def show_string_object(project, value):

    project_mixin = ProjectModelMixin()
    project = project_mixin.get_project(project.pk)
    project_model_object = project_mixin.get_model_object_and_row("Strings", value)

    if project_model_object:
        try:
            return str(project_model_object.english)
        except:
            return "N/A"


@register.filter(name='show_string_value')
def show_string_value(project_model_object, id):

    print (project_model_object, type(project_model_object))


@register.filter(name='get_preview_value_from_dict')
def get_preview_value_from_dict(dict_object, key):

    if key in dict_object:
        try:
            value = dict_object[key]
            value = value.strip()
            return value
        except Exception as e:
            pass

    other_key = str(key).replace('_', ' ')
    if other_key in dict_object:
        try:
            value = dict_object[other_key]
            value = value.strip()
            return value
        except Exception as e:
            pass

    last_check = str(key).replace(' ', '_')
    if last_check in dict_object:
        try:
            value = dict_object[last_check]
            value = value.strip()
            return value
        except Exception as e:
            pass

    return '-'


@register.filter(name='get_allocations_property_dict_value')
def get_allocations_property_dict_value(dict_object, key):

    property, dict_object = dict_object

    for row in dict_object:
        if row['property'] == property.pk:
            dict_object = row['values']

            if key in dict_object:
                value = dict_object[key]
                value = value.strip()
                return value


@register.filter(name='get_preview_aspect_class')
def get_preview_aspect_class(dict_object, key):

    if "aspect_fields" in dict_object:
        aspect_fields = dict_object['aspect_fields']

        if key in aspect_fields:
            return True

@register.filter(name='check_value_in_dict')
def check_value_in_dict(dict_object, key):

    dict_object, project = dict_object

    for row in dict_object:
        for row_user in row['users']:
            if row['project'] == project and row_user == key:
                return True

    return False