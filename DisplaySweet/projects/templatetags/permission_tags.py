from django import template
from projects.permissions import ProjectModelMixin
from users.permissions import AllocationDetailsPermissionMixin
from django.forms.models import model_to_dict
import os
from django.conf import settings
from django.db.models.loading import get_model
from django.db.models.fields.related import ForeignKey, ManyToOneRel
register = template.Library()


@register.filter(name='user_project')
def user_project(user, project):
    return user, project


@register.filter(name='user_has_project_permission')
def user_has_project_permission(user_project, permission):

    user, project = user_project

    return True


@register.filter(name='user_has_access')
def user_has_access(project, user):

    projects = user.project_users_list.filter(active=True, pk=project.pk)

    if projects:
        return True

    return False

