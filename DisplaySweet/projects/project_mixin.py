from django.db.models.loading import get_model
from django.db.models.fields.related import ForeignKey, ManyToOneRel
import os
import datetime
from django.conf import settings
import imghdr

from django.core.urlresolvers import reverse_lazy, reverse
import shutil
import operator
from django.db.models import Q

import threading
from multiprocessing.dummy import Pool as ThreadPool



class ProjectModelMixin(object):
    """
    All methods relating to project model details go on this mixin!
    """
    project = None
    project_model_object = None
    model_instance = None
    model_name = None
    model_fields = None
    field_names = None
    request = None
    building_id = None

    def get_project(self, pk):
        from core.models import Project
        if pk:
            project = Project.objects.get(pk=pk)
            self.project = project
            return project

    def get_model_object(self, model_name):
        project_model_object = get_model(self.project.project_connection_name, model_name)

        self.project_model_object = project_model_object
        self.model_name = model_name

        return project_model_object

    def get_reversed_urls_from_actions(self, actions, pk):
        reversed_actions = []
        for action_name, url_name in actions:
            reversed_actions.append((action_name, reverse(url_name, args=[pk])))

        return reversed_actions

    def get_model_object_and_row(self, model_name, row_id=None):
        project_model_object = get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        queryset = project_model_object.objects.using(self.project.project_connection_name).all()

        if row_id:
            queryset = project_model_object.objects.using(self.project.project_connection_name).filter(id=row_id)
            if len(queryset) == 1:
                return queryset[0]

        return queryset

    def get_model_queryset(self, field=None, value=None, list_needed=False):

        queryset = self.project_model_object.objects.using(self.project.project_connection_name).all()

        if field:
            kwargs = {
                '{0}'.format(field): value,
            }
            queryset = queryset.filter(**kwargs)

        if len(queryset) == 1 and not list_needed:
            return queryset[0]

        return queryset

    def check_and_update_project_templates(self):
        if self.project.created == datetime.date.today():
            from core.models import ProjectCSVTemplate, ProjectCSVTemplateFields
            templates = ProjectCSVTemplate.objects.filter(project=self.project.project_copied_from)
            for template in templates:
                check_templates = ProjectCSVTemplate.objects.filter(project=self.project, template_name=template.template_name)
                if not check_templates:
                    fields = ProjectCSVTemplateFields.objects.filter(template=template)
                    if fields:
                        new_template = template
                        new_template.id = None
                        new_template.project = self.project
                        new_template.save()

                        for field in fields:
                            new_field = field
                            new_field.id = None
                            new_field.template = new_template
                            new_field.save()

    def class_objects(self, class_name):
        model_object = self.get_model_object(class_name)
        model_objects = model_object.objects.using(self.project.project_connection_name)
        return model_objects

    def get_model_object_fields(self, project=None, model_name=None, drop_id=True):
        # For dynamic model editing
        if model_name:
            if not model_name == "Select a table mapping":
                model_fields = []
                try:
                    model_object = get_model(project.project_connection_name, model_name)
                    model_fields_objects = model_object._meta.get_fields()
                    for field in model_fields_objects:
                        if not field.name == "id" and not type(field) == ManyToOneRel:
                            model_fields.append(field.name)
                    return model_fields
                except:
                    return []
            return []
        else:
            project_model_object = self.get_class_object(model_name)
            self.project_model_object = project_model_object
            self.model_fields = project_model_object._meta.get_fields()
            return self.filter_model_fields_for_form(drop_id=drop_id)

    def get_class_object(self, model_name):
        project_model_object = get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        self.model_name = model_name
        return project_model_object

    def filter_model_fields_for_form(self, drop_id=False, include_all=False):
        field_names = []
        for field in self.model_fields:

            if include_all:
                field_names.append(field.name)
            else:
                if drop_id:
                    if not field.name == "id" and not type(field) == ManyToOneRel:
                        field_names.append(field.name)
                else:
                    if not include_all and not type(field) == ManyToOneRel and not type(field) == ForeignKey:
                        field_names.append(field.name)

        return field_names