class MasterRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on master models to 'master'"

        if model._meta.app_label == 'master':
            return 'master'
        return 'default'

    def db_for_write(self, model, **hints):
        "Point all operations on master models to 'master'"
        if model._meta.app_label == 'master':
            return 'master'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in master app"
        if obj1._meta.app_label == 'master' and obj2._meta.app_label == 'master':
            return True
        # Allow if neither is master app
        elif 'master' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_migrate(self, db, model):
        if db == 'master' or model._meta.app_label == "master":
            return False # we're not using syncdb on our legacy database
        else: # but all other models/databases are fine
            return True