# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class AllocatedUsers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user_id = models.IntegerField()
    allocation_group = models.ForeignKey('AllocationGroups', related_name='+')

    class Meta:
        db_table = 'allocated_users'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class AllocationGroups(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.ForeignKey('Names', related_name='+')
    allocation_group_parent = models.ForeignKey('self', blank=True, null=True, related_name='+')

    owner_id = models.IntegerField()
    is_hidden = models.IntegerField()

    class Meta:
        db_table = 'allocation_groups'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class AssetTags(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    tag = models.CharField(max_length=30)

    class Meta:
        db_table = 'asset_tags'

    def __unicode__(self):
        return '%s' % self.tag

    def __str__(self):
        return '%s' % self.id


class AssetTypes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')

    class Meta:
        db_table = 'asset_types'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class Assets(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    asset_type = models.ForeignKey(AssetTypes)
    source_id = models.IntegerField()

    class Meta:
        db_table = 'assets'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class AssetsTagged(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    asset = models.ForeignKey(Assets)
    asset_tag = models.ForeignKey(AssetTags)

    class Meta:
        db_table = 'assets_tagged'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class AvailableFeatures(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    feature = models.CharField(max_length=30)
    device_type = models.ForeignKey('DeviceTypes', related_name='+')
    required = models.BooleanField()

    class Meta:
        db_table = 'available_features'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Buildings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.ForeignKey('Names', related_name='+')

    class Meta:
        db_table = 'buildings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class CanvasBooleanFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey('Canvases', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.NullBooleanField()

    class Meta:
        db_table = 'canvas_boolean_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.id


class CanvasDevices(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    device = models.ForeignKey('Devices', related_name='+')
    canvas = models.ForeignKey('Canvases', related_name='+')

    class Meta:
        db_table = 'canvas_devices'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class CanvasFloatFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey('Canvases', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'canvas_float_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class CanvasIntFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey('Canvases', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'canvas_int_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class CanvasStringFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey('Canvases', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'canvas_string_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value

class CanvasWireframes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey('Canvases', related_name='+')
    wireframe = models.ForeignKey('Wireframes', related_name='+')

    class Meta:
        db_table = 'canvas_wireframes'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class Canvases(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas_name = models.ForeignKey('Names', related_name='+')

    class Meta:
        db_table = 'canvases'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ColourPalettes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    rgba = models.CharField(max_length=8)

    class Meta:
        db_table = 'colour_palettes'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ContainerAssignedAssets(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey('Containers', related_name='+')
    asset = models.ForeignKey(Assets)
    canvas = models.ForeignKey(Canvases)
    template = models.ForeignKey('Templates')
    wireframe = models.ForeignKey('Wireframes')
    orderidx = models.IntegerField()

    class Meta:
        db_table = 'container_assigned_assets'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ContainerAssignedAssetSettings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container_assigned_asset = models.ForeignKey('ContainerAssignedAssets', related_name='+')
    setting_string = models.CharField(max_length=50, null=True, blank=True)
    value = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = 'container_assigned_asset_settings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ContainerBooleanFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey('Containers', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.NullBooleanField()

    class Meta:
        db_table = 'container_boolean_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class ContainerFloatFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey('Containers', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'container_float_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class ContainerIntFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey('Containers', related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'container_int_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class ContainerStringFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    template_container = models.ForeignKey('WireframeControllerContainers', blank=True, null=True, related_name='+')
    key_string = models.ForeignKey('Strings', related_name='+')
    value = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'container_string_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class ContainerTypes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')

    class Meta:
        db_table = 'container_types'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Containers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container_type_id = models.IntegerField()
    container_name = models.ForeignKey('Names', related_name='+')

    container_parent = models.ForeignKey('self', blank=True, null=True, related_name='+')
    orderidx = models.IntegerField()

    cms_display = models.NullBooleanField()

    class Meta:
        db_table = 'containers'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Content(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey(Containers)
    resource = models.ForeignKey("Resources")
    hash = models.CharField(max_length=100, blank=True, null=True)
    path = models.CharField(max_length=100, blank=True, null=True)

    template = models.ForeignKey('Templates', blank=True, null=True, related_name='+')
    canvas = models.ForeignKey('Canvases', blank=True, null=True, related_name='+')
    wireframe = models.ForeignKey('Wireframes', blank=True, null=True, related_name='+')
    container_type_id = models.IntegerField()

    orderidx = models.IntegerField()

    published = models.DateField(null=True, blank=True)
    published_by = models.IntegerField()

    class Meta:
        db_table = 'content'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class WireframeControllerContainers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?

    container = models.ForeignKey('Containers', blank=True, null=True, related_name='+')
    wireframe_controller = models.ForeignKey('WireframeControllers', blank=True, null=True, related_name='+')

    class Meta:
        db_table = 'wireframe_controller_containers'

    def __unicode__(self):
        return '%s' % self.pk

    def __str__(self):
        return '%s' % self.pk


class Descriptions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    text_table_category = models.ForeignKey('TextTableCategories', blank=True, null=True, related_name='+')
    english = models.CharField(max_length=50, blank=True, null=True)
    chinese = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'descriptions'

    def __unicode__(self):
        return '%s' % self.english

    def __str__(self):
        return '%s' % self.english

class DeviceTypes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')

    class Meta:
        db_table = 'device_types'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class Devices(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    device_type = models.ForeignKey(DeviceTypes)
    device_name = models.ForeignKey('Names', related_name='+')

    class Meta:
        db_table = 'devices'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class FittingCategories(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.ForeignKey('Names', related_name='+')
    apartment_id = models.IntegerField(blank=True, null=True)
    floor = models.ForeignKey('Floors', blank=True, null=True, related_name='+')

    class Meta:
        db_table = 'fitting_categories'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class FittingSubCategories(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.ForeignKey('Names', related_name='+')

    class Meta:
        db_table = 'fitting_sub_categories'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class FittingSubCategoryMembers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    fittings = models.ForeignKey('Fittings', related_name='+')
    fitting_sub_categories = models.ForeignKey(FittingSubCategories)

    class Meta:
        db_table = 'fitting_sub_category_members'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Fittings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    resource = models.ForeignKey('Resources', related_name='+')
    name = models.ForeignKey('Names', related_name='+')
    description = models.ForeignKey(Descriptions)
    fitting_categories = models.ForeignKey(FittingCategories, blank=True, null=True)

    class Meta:
        db_table = 'fittings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Floors(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.ForeignKey('Names', related_name='+')

    orderidx = models.IntegerField()

    class Meta:
        db_table = 'floors'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class FloorsBuildings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    floor = models.ForeignKey(Floors)
    building = models.ForeignKey(Buildings)

    class Meta:
        db_table = 'floors_buildings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class FloorsHotspots(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    floor = models.ForeignKey(Floors)
    x = models.FloatField()
    y = models.FloatField()
    name = models.CharField(max_length=200, default='')

    class Meta:
        db_table = 'floors_hotspots'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class FloorsPlans(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    floor = models.ForeignKey(Floors)
    resource = models.ForeignKey('Resources', related_name='+')
    key = models.CharField(max_length=50)

    class Meta:
        db_table = 'floors_plans'

    def __unicode__(self):
        return '%s' % self.key

    def __str__(self):
        return '%s' % self.id

class Fonts(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    font_name = models.CharField(max_length=32)
    font_path = models.CharField(max_length=100)

    class Meta:
        db_table = 'fonts'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Listings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    listing_status = models.ForeignKey('ListingStatus', blank=True, null=True, related_name='+')

    class Meta:
        db_table = 'listings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class ListingMembers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    property = models.ForeignKey('Properties', blank=True, null=True, related_name='+')
    listing = models.ForeignKey('Listings', blank=True, null=True, related_name='+')

    class Meta:
        db_table = 'listing_members'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class ListingStatus(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    status = models.CharField(max_length=50)

    class Meta:
        db_table = 'listing_status'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class Names(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    text_table_category = models.ForeignKey('TextTableCategories', blank=True, null=True, related_name='+')
    english = models.CharField(max_length=50, blank=True, null=True)
    chinese = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'names'

    def __unicode__(self):
        return '%s' % self.english

    def __repr__(self):
        return '%s' % self.english

    def __str__(self):
        return '%s' % self.english


class Properties(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    floor = models.ForeignKey(Floors)
    name = models.ForeignKey(Names)
    property_type = models.ForeignKey('PropertyTypes', related_name='+')

    orderidx = models.IntegerField()

    class Meta:
        db_table = 'properties'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertiesBooleanFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')
    property = models.ForeignKey(Properties)
    value = models.NullBooleanField()

    class Meta:
        db_table = 'properties_boolean_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class PropertiesFloatFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')
    property = models.ForeignKey(Properties)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'properties_float_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value


class PropertiesIntFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')
    property = models.ForeignKey(Properties)
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'properties_int_fields'

    def __unicode__(self):
        return '%s' % self.value

    def __str__(self):
        return '%s' % self.value

class PropertiesStringFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type_string = models.ForeignKey('Strings', related_name='+')
    property = models.ForeignKey(Properties)
    value = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'properties_string_fields'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertyAllocations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    property = models.ForeignKey(Properties)
    allocation_group = models.ForeignKey(AllocationGroups)

    class Meta:
        db_table = 'property_allocations'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertyHotspots(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    property = models.ForeignKey(Properties)
    floors_hotspot = models.ForeignKey(FloorsHotspots)

    orderidx = models.IntegerField()

    class Meta:
        db_table = 'property_hotspots'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertyResources(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?

    property = models.ForeignKey(Properties)
    resource = models.ForeignKey('Resources')
    key = models.CharField(max_length=50)

    orderidx = models.IntegerField(default=1)
    caption = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        db_table = 'property_resources'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertyAspects(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?

    property = models.ForeignKey(Properties)
    resource = models.ForeignKey('Resources')
    key = models.CharField(max_length=50)
    orderidx = models.IntegerField(default=0)

    class Meta:
        db_table = 'property_aspects'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class PropertyRenders(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?

    property = models.ForeignKey(Properties)
    resource = models.ForeignKey('Resources')
    key = models.CharField(max_length=50)
    orderidx = models.IntegerField(default=0)
    caption = models.CharField(max_length=50)

    class Meta:
        db_table = 'property_renders'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class PropertyTypes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    type = models.CharField(max_length=32)

    class Meta:
        db_table = 'property_types'

    def __unicode__(self):
        return '%s' % self.type

    def __str__(self):
        return '%s' % self.type


class Resources(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    description = models.ForeignKey(Descriptions)
    size = models.IntegerField()
    path = models.CharField(max_length=100, blank=True, null=True)
    md5 = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        db_table = 'resources'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Strings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    text_table_category = models.ForeignKey('TextTableCategories', blank=True, null=True, related_name='+')
    english = models.CharField(max_length=50, blank=True, null=True)
    chinese = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'strings'

    def __unicode__(self):
        return '%s' % self.english

    def __str__(self):
        return '%s' % self.english


class Templates(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=50)

    template_type = models.ForeignKey("TemplateTypes",  blank=True, null=True, related_name="+")

    class Meta:
        db_table = 'templates'

    def __unicode__(self):
        return '%s' % self.name

    def __str__(self):
        return '%s' % self.name


class TemplateTypes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=30)

    class Meta:
        db_table = 'template_types'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class TemplateTypeContainers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container = models.ForeignKey('Containers', blank=True, null=True, related_name='+')
    template_type = models.ForeignKey('TemplateTypes', blank=True, null=True, related_name='+')

    class Meta:
        db_table = 'template_type_containers'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id



class TextTableCategories(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    category = models.CharField(max_length=50)

    class Meta:
        db_table = 'text_table_categories'

    def __unicode__(self):
        return '%s' % self.category

    def __str__(self):
        return '%s' % self.category


class Videos(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    description = models.ForeignKey(Descriptions)
    size = models.IntegerField()
    path = models.CharField(max_length=100, blank=True, null=True)
    md5 = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        db_table = 'videos'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class WireframeControllers(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    canvas = models.ForeignKey(Canvases)
    wireframe = models.ForeignKey('Wireframes', related_name='+')
    template = models.ForeignKey(Templates)

    class Meta:
        db_table = 'wireframe_controllers'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class Wireframes(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    wireframe_parent = models.ForeignKey('self', blank=True, null=True, related_name='+')
    template = models.ForeignKey(Templates, blank=True, null=True)
    orderidx = models.IntegerField()
    name = models.CharField(max_length=50)
    display_name = models.ForeignKey(Names)

    class Meta:
        db_table = 'wireframes'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ContainerSettings(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    main_container = models.ForeignKey('Containers', related_name="+")
    container = models.ForeignKey('Containers', related_name="+")
    title = models.CharField(max_length=50)
    width = models.IntegerField(default=12)
    orderidx = models.IntegerField(default=1)

    class Meta:
        db_table = 'container_settings'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class ContainerFields(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    container_settings = models.ForeignKey('ContainerSettings', related_name="+")

    string_field = models.CharField(max_length=50)
    field_type = models.CharField(max_length=50, default='text_box')
    initial_value = models.CharField(max_length=50)
    orderidx = models.IntegerField(default=1)

    class Meta:
        db_table = 'container_fields'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id

class ProjectProperties(models.Model):

    id = models.IntegerField(primary_key=True)  # AutoField?
    key = models.CharField(max_length=64)
    value = models.CharField(max_length=64)

    class Meta:
        db_table = 'project_properties'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id


class BuyerDetails(models.Model):

    MR = 'Mr'
    MS = 'Ms'
    MRS = 'Mrs'
    DR = 'Dr'
    MISS = 'Miss'

    TITLE_OPTIONS = (
        (MS, 'Ms'),
        (MR, 'Mr'),
        (MRS, 'Mrs'),
        (MISS, 'Miss'),
        (DR, 'Dr'),
    )

    PRE_APPROVED = 'pending_registration'
    SUSPENDED = 'suspended'
    APPROVED = 'approved'

    STATUS_CHOICES = (
        (PRE_APPROVED, 'Pending Approval'),
        (SUSPENDED, 'Suspended'),
        (APPROVED, 'Approved'),
    )

    BUYER_TYPE_CHOICES = (
        ('investor', 'Investor'),
        ('owner_occupier', 'Owner Occupier'),
    )

    OCCUPY_CHOICES = (
        ('owner_occupier', 'Owner Occupier'),
    )

    LOCATION_CHOICES = (
        ('local', 'Local'),
        ('overseas', 'Overseas'),
    )

    YES_NO_CHOICES = (
        ('no', 'No'),
        ('yes', 'Yes'),

    )

    SALES_TYPE_CHOICES = (
        ('weekly', 'Weekly Amount'),
        ('percentage', 'Percentage'),
    )

    USER_TYPE_PURCHASE_USER = 'purchase_user'

    USER_TYPE_CHOICES = (
        (USER_TYPE_PURCHASE_USER, 'Purchase User'),
    )

    id = models.IntegerField(primary_key=True)  # AutoField?

    title = models.CharField('title', choices=TITLE_OPTIONS, max_length=30, null=True, blank=True)
    first_name = models.CharField('first name', max_length=30, null=True, blank=True)
    last_name = models.CharField('last name', max_length=30, null=True, blank=True)
    email = models.CharField('email address', max_length=255)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    date_of_birth = models.CharField(max_length=30, null=True, blank=True)

    user_type = models.CharField(max_length=50, choices=USER_TYPE_CHOICES, default=USER_TYPE_PURCHASE_USER)
    is_active = models.BooleanField(default=True)
    can_access_django_backend = models.BooleanField(default=False)

    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=APPROVED)
    project_id = models.IntegerField(null=True, blank=True)

    property_id = models.IntegerField(null=True, blank=True)
    agent_id = models.IntegerField(null=True, blank=True)

    buyer_type = models.CharField(max_length=50, choices=BUYER_TYPE_CHOICES, default="resident")
    company_name = models.CharField(max_length=50, null=True, blank=True)
    website = models.CharField(max_length=50, null=True, blank=True)
    occupy_status = models.CharField(max_length=50, choices=OCCUPY_CHOICES, default="owner_occupier")
    purchaser_location = models.CharField(max_length=50, choices=LOCATION_CHOICES, default="local")
    firb = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")
    nras = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")
    enquiry_source = models.CharField(max_length=50, choices=YES_NO_CHOICES, default="no")

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    # Summary Fields
    agent_bonus = models.CharField(max_length=50, null=True, blank=True)
    rebate = models.CharField(max_length=50, null=True, blank=True)
    rental_guarantee = models.CharField(max_length=50, null=True, blank=True)

    # Rental Fields
    sales_price = models.CharField(max_length=50, null=True, blank=True)
    sales_type = models.CharField(max_length=50, default="weekly", choices=SALES_TYPE_CHOICES)
    weekly_amount = models.CharField(max_length=50, null=True, blank=True)
    percentage_amount = models.CharField(max_length=50, null=True, blank=True)
    number_of_weeks = models.CharField(max_length=50, null=True, blank=True)
    max_cost = models.CharField(max_length=50, null=True, blank=True)

    # Leasing Details
    weeks_leased = models.CharField(max_length=50, null=True, blank=True)
    leased_amount = models.CharField(max_length=50, null=True, blank=True)
    recouped = models.CharField(max_length=50, null=True, blank=True)
    current_position = models.CharField(max_length=50, null=True, blank=True)

    # Contract Info
    reservation_fee_date = models.DateTimeField(null=True, blank=True)
    reservation_fee_refund = models.BooleanField(default=False)
    contract_exchange_date = models.DateTimeField(null=True, blank=True)
    cancelled = models.BooleanField(default=False)
    deposit_date = models.DateTimeField(null=True, blank=True)
    deposit_refund = models.BooleanField(default=False)

    class Meta:
        db_table = 'buyer_details'

    def __unicode__(self):
        return '%s' % self.id

    def __str__(self):
        return '%s' % self.id