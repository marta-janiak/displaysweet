from datetime import timedelta
import os
import shutil
from django import forms
from django.utils.encoding import smart_str
from django.conf import settings
from itertools import chain
import re
import csv
from core.models import Project, CSVFileImport
from core.forms import FilterForm
from django.contrib.contenttypes.models import ContentType
from users.models import User
from django.db import connections
from utils import happyforms

import urllib
import requests

from utils.tools import DynamicFileInput
from django.forms.models import model_to_dict
from utils.xlxs_reader import ImportedWorkbook, InvalidImportException
from utils.template_creator.xlxs_reader import ImportedWorkbook as TemplateImportedWorkBook

from django.db.models.fields.related import ForeignKey, ManyToOneRel
from projects.project_script_load import PixileScriptDbUpdater
from django.db.models.loading import get_model

from django.core.files import File
from openpyxl import load_workbook
from users.models import User, Purchaser
from core.models import BulkImport, BulkImportCompletedTemplate, ProjectCSVTemplate, ProjectCSVTemplateFields, \
                        ProjectImagePath, ProjectCSVTemplateImport, CSVFileImportFields, ProjectNotes, ProjectScripts, \
                        ProjectSyncBackLog, ProjectTranslations, ProjectFamily

from .cms_sync import ProjectCMSSync

from django.utils.html import conditional_escape, escape
from django.utils.encoding import force_str
from six import string_types


class ProjectFamilyAddForm(forms.ModelForm):

    project_list = forms.ModelMultipleChoiceField(queryset=Project.objects.all(),
                                   widget=forms.SelectMultiple(attrs={'class': 'property-agent-select', 'style': 'height: 500px;'}))

    class Meta:
        model = ProjectFamily
        fields = ('family_name', 'project_list')

    def __init__(self, *args, **kwargs):
        super(ProjectFamilyAddForm, self).__init__(*args, **kwargs)

        self.fields['family_name'].widget = forms.TextInput(attrs={'style': 'margin-bottom: 18px; width: 200px;'})


def render(FormInput, name, value, attrs=None):
    if 'name' in attrs:
        name = attrs['name']
        del attrs['name']
    return super(FormInput).render(name, value, attrs)


class AddProjectTranslation(forms.ModelForm):

    modified = forms.DateTimeField(widget=forms.HiddenInput(), required=False)
    project = forms.ModelChoiceField(queryset=Project.objects.all(), required=False)

    created_by = forms.ModelChoiceField(queryset=User.objects.all(), required=False, widget=forms.HiddenInput())
    database_file = forms.FileField(widget=forms.HiddenInput(), required=False)

    apartment_translation_file = forms.FileField(required=False)
    sold_status_translation_file = forms.FileField(required=False)

    # project_select_image = forms.FileField(required=False)
    # project_select_chinese_image = forms.FileField(required=False)
    # project_loading_image = forms.FileField(required=False)
    # project_cms_image = forms.FileField(required=False)
    # project_cms_chinese_image = forms.FileField(required=False)

    projector_canvas_name = forms.ChoiceField(choices=[])
    kwall_canvas_name = forms.ChoiceField(choices=[])
    proj_canvas_name = forms.ChoiceField(choices=[])

    create_slim_database = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    apartment_duplicates = forms.BooleanField(widget=forms.CheckboxInput(), required=False, initial=True)

    version_published = forms.ChoiceField(choices=[],
                                          help_text="Select the version of this project you wish to publish.",
                                          label="Version to publish", required=False)

    version_prefix_count = forms.CharField(widget=forms.TextInput(),
                                           help_text="")
    published_to = forms.ChoiceField(choices=ProjectTranslations.PUBLISHED_TO_OPTIONS,
                                     widget=forms.Select())

    class Meta:
        model = ProjectTranslations
        fields = '__all__'
        exclude = ('post_details_log',
                   'translated_database',
                   'database_id',
                   'updated_script_file',
                   'project_select_image',
                   'project_select_chinese_image',
                   'project_loading_image',
                   'project_cms_image',
                   'project_cms_chinese_image'
                   )

    def __init__(self, model_mixin, family, *args, **kwargs):

        self.project = model_mixin.project
        self.model_mixin = model_mixin

        super(AddProjectTranslation, self).__init__(*args, **kwargs)

        self.fields['project'].queryset = family.project_list.filter(active=True, status__in=[Project.STAGING, Project.LIVE])
        self.fields['project'].initial = self.project

        if self.project and self.model_mixin:
            if self.project.apartment_translation_file:
                self.fields['apartment_translation_file'].help_text = "Leave blank to use project file <a target='_blank' href='%s'>%s</a>"  \
                                                                      % (self.project.apartment_translation_file.url,
                                                                         self.project.apartment_translation_file.url)
            if self.project.sold_status_translation_file:
                self.fields['sold_status_translation_file'].help_text = "Leave blank to use project file <a target='_blank' href='%s'>%s</a>"  \
                                                                      % (self.project.sold_status_translation_file.url,
                                                                         self.project.sold_status_translation_file.url)

            if not self.project.version:
                self.project.version = 1.0
                self.project.save()

            version_choices = [(x, x) for x in self.project.project_versions]
            self.fields['version_published'].initial = self.project.version
            self.fields['version_published'].choices = version_choices

            self.fields['version_prefix_count'].initial = 2
            self.fields['version_prefix_count'].help_text = self.project.get_publish_path_example

            canvases = model_mixin.model_object("Canvases").all().values_list('canvas_name__english',
                                                                              'canvas_name__english').order_by('canvas_name__english')
            self.fields['projector_canvas_name'].choices = canvases
            self.fields['kwall_canvas_name'].choices = canvases
            self.fields['proj_canvas_name'].choices = canvases

    def save(self, *args, **kwargs):
        translation_saved = super(AddProjectTranslation, self).save(*args, **kwargs)

        error_message = ''
        if self.project:
            try:
                self.project.connection.save_new_database_file()
            except Exception as e:
                error_message += str(e) + '\n'

            db_file = open(self.project.connection.database_file.path, "rb")

            file_name = '%s.db' % self.project.project_connection_name
            translation_saved.database_file.save(file_name, File(db_file))
            translation_saved.translated_database.save(file_name, File(db_file))

            if not translation_saved.apartment_translation_file:
                translation_saved.apartment_translation_file = self.project.apartment_translation_file
            else:
                self.project.apartment_translation_file = translation_saved.apartment_translation_file
                self.project.save()

            if not translation_saved.sold_status_translation_file:
                translation_saved.sold_status_translation_file = self.project.sold_status_translation_file
            else:
                self.project.sold_status_translation_file = translation_saved.sold_status_translation_file
                self.project.save()

            translation_saved.save()

            db_file = open(self.project.connection.database_file.path, "rb")

            if self.project.selected_script:

                if self.project.apartment_translation_file:

                    trans_file = open(self.project.apartment_translation_file.path, "rb")
                    sold_file = None
                    if self.project.sold_status_translation_file:
                        sold_file = open(self.project.sold_status_translation_file.path, "rb")

                    url = 'http://services.displaysweet.com/translate_databasev2.php'
                    publishedto = ProjectTranslations.PUBLISH_SELECT[translation_saved.published_to]

                    project_nice_version_name = '%s_%s' % (str(self.project.name).replace(" ", "_", 10).lower(),
                                                           str(self.project.version).replace('.', ''))

                    values = {'uploaded': 1,
                              'projectname': project_nice_version_name,
                              'projector_canvas': translation_saved.projector_canvas_name,
                              'fourk_canvas': translation_saved.kwall_canvas_name,
                              'threep_canvas': translation_saved.proj_canvas_name,
                              'duplicatenames': translation_saved.apartment_duplicates,
                              'version': translation_saved.version_published,
                              'prefixcount': translation_saved.version_prefix_count,
                              'publishedto': publishedto
                              }

                    if translation_saved.create_slim_database:
                        values.update(
                            {'slimdb': translation_saved.create_slim_database,}
                        )

                    files = {
                        'dbfile': db_file,
                        'tranfile': trans_file
                    }

                    if sold_file:
                        files.update(
                            {'statusfile': sold_file}
                        )

                    try:
                        r = requests.post(url, data=values, files=files)
                        with open(translation_saved.translated_database.path, 'wb') as w:
                            w.write(r.content)

                        translation_saved.save()

                    except Exception as e:
                        error_message += str(e) + '\n'
                        print('error with requests post', e)

                    try:
                        proper_filename = '%s.db' % str(self.project.name).replace(" ", "_", 10).lower()
                        project_nice_name = str(self.project.name).replace(" ", "_", 10).lower()
                        file_path_copy_to = os.path.join(settings.TRANSLATE_DB_PATH,
                                                         "%s/%s" % (project_nice_name, self.project.version))

                        copy_to = os.path.join(file_path_copy_to, proper_filename)
                        path_for_db = copy_to.replace(settings.TRANSLATE_DB_PATH_REPLACE, '')
                        description = "%s Database" % self.project.name

                        if os.path.isfile(translation_saved.translated_database.path):

                            try:
                                if not os.path.exists(file_path_copy_to):
                                    os.makedirs(file_path_copy_to)
                            except Exception as e:
                                print('create folder error', e)

                            try:
                                shutil.copy2(translation_saved.translated_database.path, copy_to)
                                print('successfully wrote database to save folder')
                            except Exception as e:
                                print('copy db error', e)

                    except Exception as e:
                        error_message += str(e) + '\n'
                        print(e)

                    try:
                        print(connections['projects_db'])
                    except Exception as e:
                        print('connection error', e)

        return translation_saved, error_message

    def run_translation(self, translation_saved):

        error_message = ''

        if self.project:
            db_file = open(self.project.connection.database_file.path, "rb")

            if self.project.selected_script:

                if self.project.apartment_translation_file:

                    trans_file = open(self.project.apartment_translation_file.path, "rb")
                    sold_file = None
                    if self.project.sold_status_translation_file:
                        sold_file = open(self.project.sold_status_translation_file.path, "rb")

                    url = 'http://services.displaysweet.com/translate_databasev2.php'
                    publishedto = ProjectTranslations.PUBLISH_SELECT[translation_saved.published_to]

                    project_nice_version_name = '%s_%s' % (str(self.project.name).replace(" ", "_", 10).lower(),
                                                           str(self.project.version).replace('.', ''))

                    values = {'uploaded': 1,
                              'projectname': project_nice_version_name,
                              'projector_canvas': translation_saved.projector_canvas_name,
                              'fourk_canvas': translation_saved.kwall_canvas_name,
                              'threep_canvas': translation_saved.proj_canvas_name,
                              'duplicatenames': translation_saved.apartment_duplicates,
                              'version': translation_saved.version_published,
                              'prefixcount': translation_saved.version_prefix_count,
                              'publishedto': publishedto
                              }

                    if translation_saved.create_slim_database:
                        values.update(
                            {'slimdb': translation_saved.create_slim_database,}
                        )

                    files = {
                        'dbfile': db_file,
                        'tranfile': trans_file
                    }

                    if sold_file:
                        files.update(
                            {'statusfile': sold_file}
                        )

                    try:
                        r = requests.post(url, data=values, files=files)
                        with open(translation_saved.translated_database.path, 'wb') as w:
                            w.write(r.content)

                        translation_saved.save()

                    except Exception as e:
                        error_message += str(e) + '\n'
                        print('error with requests post', e)

                    try:
                        proper_filename = '%s.db' % str(self.project.name).replace(" ", "_", 10).lower()
                        project_nice_name = str(self.project.name).replace(" ", "_", 10).lower()
                        file_path_copy_to = os.path.join(settings.TRANSLATE_DB_PATH,
                                                         "%s/%s" % (project_nice_name, self.project.version))
                        copy_to = os.path.join(file_path_copy_to, proper_filename)
                        path_for_db = copy_to.replace(settings.TRANSLATE_DB_PATH_REPLACE, '')
                        description = "%s Database" % self.project.name

                        if os.path.isfile(translation_saved.translated_database.path):

                            try:
                                if not os.path.exists(file_path_copy_to):
                                    os.makedirs(file_path_copy_to)
                            except Exception as e:
                                print('create folder error', e)

                            try:
                                shutil.copy2(translation_saved.translated_database.path, copy_to)
                                print('successfully wrote database to save folder')
                            except Exception as e:
                                print('copy db error', e)

                    except Exception as e:
                        error_message += str(e) + '\n'
                        print(e)

                    try:
                        print(connections['projects_db'])
                    except Exception as e:
                        print('connection error', e)

                    # Now check the projects.db for the current project and database
                    try:
                        found_database_id = None
                        raw_sql_str = "select id from images where image_path like '%" + str(
                            path_for_db) + "%' and image_path like '%.db%';"
                        cursor = connections['projects_db'].cursor()
                        cursor.execute(raw_sql_str)
                        found_row = cursor.fetchall()

                        if found_row:
                            found_database_id = found_row[0][0]
                            error_message += self.update_script_details(translation_saved, found_database_id,
                                                                        error_message)

                        if not found_database_id:
                            raw_sql_str = "insert into images (image_path, description, md5) values ('%s', '%s', ' ')" % (
                            path_for_db, description)
                            cursor = connections['projects_db'].cursor()
                            cursor.execute(raw_sql_str)

                            raw_sql_str = "select id from images where image_path like '%" + str(
                                path_for_db) + "%' and image_path like '%.db%';"
                            cursor = connections['projects_db'].cursor()
                            cursor.execute(raw_sql_str)
                            found_row = cursor.fetchall()

                            if found_row:
                                found_database_id = found_row[0][0]
                                error_message += self.update_script_details(translation_saved, found_database_id,
                                                                            error_message)

                        if not found_database_id:
                            error_message += str("There was not database ID found or created.") + '\n'
                        else:
                            translation_saved.database_id = found_database_id
                            translation_saved.save()

                    except Exception as e:
                        error_message += " [projects.db] " + str(e) + '\n'
                        print('error', e)

                else:
                    error_message += str("You have not uploaded an Apartment Translation File.") + '\n'

            else:
                error_message += str("You have not selected a translation script for this project and version.") + '\n'

        if error_message:
            translation_saved.post_details_log = error_message
        else:
            translation_saved.post_details_log = 'Success.'

        translation_saved.save()
        return translation_saved, error_message

    def update_script_details(self, translation_saved, found_database_id, error_message):

        # Update script process
        update_script = True
        try:
            PixileScriptDbUpdater(self.project.selected_script.file.path, found_database_id)
        except Exception as e:
            update_script = False
            error_message += str("[Project script file] ") + str(e) + '\n'

        if update_script:
            script_description = "%s Script" % self.project.name

            new_script_path = str(self.project.selected_script.file.path)
            db_file = open(new_script_path, "rb")
            file_name = '%s.pxz' % str(self.project.name).replace(" ", "_", 10).lower()

            translation_saved.updated_script_file.save(file_name, File(db_file))
            script_path = os.path.join(settings.TRANSLATE_SCRIPT_PATH, file_name)
            script_path_for_db = script_path.replace(settings.TRANSLATE_DB_PATH_REPLACE, '')

            print('----- script_path_for_db', script_path_for_db)
            print('------ script_path', script_path)

            raw_sql_str = "select id from images where image_path like '%" + str(script_path_for_db) \
                          + "%' and image_path like '%.pxz%';"
            cursor = connections['projects_db'].cursor()
            cursor.execute(raw_sql_str)
            found_script_row = cursor.fetchall()

            if not found_script_row:
                raw_sql_str = "insert into images (image_path, description, md5) values ('%s', '%s', ' ')" \
                              % (script_path_for_db, script_description)

                cursor = connections['projects_db'].cursor()
                cursor.execute(raw_sql_str)

            try:
                shutil.copyfile(translation_saved.updated_script_file.path, script_path)
            except Exception as e:
                error_message += "Could not copy script across %s " % e

        return error_message


class ProjectVersionCommentForm(forms.ModelForm):

    project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput)
    version = forms.FloatField(required=False, widget=forms.HiddenInput)
    created_by = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput)

    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'input-sm', 'placeholder': 'Subject / Title'}))
    notes = forms.CharField(widget=forms.Textarea(attrs={'style': 'width: 100%; height: 80px; margin-bottom: 10px; float: left;', 'placeholder': 'Comment / Notes..'}), label="Notes")

    class Meta:
        model = ProjectNotes
        fields = ('project', 'version', 'title', 'notes', 'created_by')


class ProjectScriptsForm(forms.ModelForm):

    project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput)
    version = forms.FloatField(required=False, widget=forms.HiddenInput)
    created_by = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput)

    script_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'input-sm', 'placeholder': 'Script name'}))
    notes = forms.CharField(required=False, widget=forms.Textarea(attrs={'style': 'width: 100%; height: 80px; margin-bottom: 10px; float: left;', 'placeholder': 'Comment / Notes..'}), label="Notes")

    class Meta:
        model = ProjectScripts
        fields = ('project', 'created_by', 'version', 'script_name', 'notes',  'file')


class ReservationPurchaserForm(forms.Form):

    property_id = forms.IntegerField(widget=forms.HiddenInput, label='', required=False)

    first_name = forms.CharField(max_length=100, label="First Name", required=True)
    last_name = forms.CharField(max_length=100, label="Last Name", required=True)

    email = forms.CharField(max_length=100, label="Email", required=True)
    phone_number = forms.CharField(max_length=100, label="Phone Number", required=False)

    buyer_type = forms.ChoiceField(choices=Purchaser.BUYER_TYPE_CHOICES, label="Buyer Type", required=True)
    company_name = forms.CharField(max_length=100, label="Company Name", required=False)

    website = forms.CharField(max_length=100, label="Website", required=False)

    occupy_status = forms.ChoiceField(choices=Purchaser.OCCUPY_CHOICES, label="Occupy Status", required=False)

    purchaser_location = forms.ChoiceField(choices=Purchaser.LOCATION_CHOICES, label="Purchaser Location", required=False)
    firb = forms.ChoiceField(choices=Purchaser.YES_NO_CHOICES, label="FIRB", required=False)
    nras = forms.ChoiceField(choices=Purchaser.YES_NO_CHOICES, label="NRAS", required=False)
    enquiry_source = forms.ChoiceField(choices=Purchaser.YES_NO_CHOICES, label="Enquiry Source", required=False)

    agent = forms.ModelChoiceField(queryset=User.objects.all().exclude(user_type='purchase_user'), label="Agent",
                                   widget=forms.Select(attrs={'class': 'property-agent-select'}))

    def __init__(self, buyer_instance, project, request, *args, **kwargs):

        self.buyer_instance = buyer_instance
        self.project = project

        super(ReservationPurchaserForm, self).__init__(*args, **kwargs)

        if User.objects.filter(project_users_list=project):
            self.fields['agent'].queryset = User.objects.filter(project_users_list=project).exclude(user_type__in=[User.USER_TYPE_SUPER_ADMIN,
                                                                                                                   User.USER_TYPE_ADMIN])

        try:
            if buyer_instance:
                self.fields['agent'].initial = User.objects.get(pk=buyer_instance.agent_id)
                self.fields['property_id'].initial = buyer_instance.property_id
                self.fields['first_name'].initial = buyer_instance.first_name
                self.fields['last_name'].initial = buyer_instance.last_name
                self.fields['email'].initial = buyer_instance.email
                self.fields['buyer_type'].initial = buyer_instance.buyer_type
                self.fields['company_name'].initial = buyer_instance.company_name
                self.fields['occupy_status'].initial = buyer_instance.occupy_status
                self.fields['purchaser_location'].initial = buyer_instance.purchaser_location
                self.fields['firb'].initial = buyer_instance.firb
                self.fields['nras'].initial = buyer_instance.nras
                self.fields['enquiry_source'].initial = buyer_instance.enquiry_source
                self.fields['phone_number'].initial = buyer_instance.phone_number

        except Exception as e:
            print(e)

    def save(self, property_id, commit=True):
        cleaned_data = self.cleaned_data

        agent = cleaned_data.get('agent', None)
        if agent:
            agent = agent.pk

        if self.buyer_instance:
            buyer_instance = self.buyer_instance
            buyer_instance.property_id = property_id
            buyer_instance.phone_number = cleaned_data.get('phone_number', None)
            buyer_instance.website = cleaned_data.get('website', None)
            buyer_instance.first_name = cleaned_data.get('first_name', None)
            buyer_instance.email = cleaned_data.get('email', None)
            buyer_instance.last_name = cleaned_data.get('last_name', None)
            buyer_instance.enquiry_source = cleaned_data.get('enquiry_source', None)
            buyer_instance.nras = cleaned_data.get('nras', None)
            buyer_instance.occupy_status = cleaned_data.get('occupy_status', None)
            buyer_instance.agent_id = agent
            buyer_instance.firb = cleaned_data.get('firb', None)
            buyer_instance.company_name = cleaned_data.get('company_name', None)
            buyer_instance.buyer_type = cleaned_data.get('buyer_type', None)
            buyer_instance.resident = cleaned_data.get('resident', None)
            buyer_instance.project_id = self.project.pk
            buyer_instance.save(using=self.project.project_connection_name)

            return buyer_instance

    def send_property_reservation_email(self, user_id, project_id, property_name=None, property_type=None):

        if self.buyer_instance:

            new_sync = ProjectCMSSync()
            project = Project.objects.get(pk=project_id)
            project_project_id = new_sync.get_app_project_id(project_id, project.name)
            project_project_id = str(project_project_id).strip()

            email = ''
            first_name = ''
            last_name = ''
            phone_number = ''
            company_name = ''
            if self.buyer_instance.agent_id:
                agent = User.objects.filter(pk=self.buyer_instance.agent_id)
                if agent:
                    agent = agent[0]
                    email = agent.email
                    first_name = agent.first_name
                    last_name = agent.last_name
                    phone_number = agent.phone_number
                    company_name = agent.company_name

            values = {'agent_email': email,
                      'agent_first_name': first_name,
                      'agent_last_name': last_name,
                      'agent_phone': phone_number,
                      'agent_company': company_name,
                      'email_send_to': email,
                      'buyer_email': self.buyer_instance.email,
                      'buyer_first_name': self.buyer_instance.first_name,
                      'buyer_last_name': self.buyer_instance.last_name,
                      'buyer_address': '',
                      'buyer_city': '',
                      'buyer_phone': self.buyer_instance.phone_number,
                      'buyer_country': '',
                      'buyer_enquiry_source': self.buyer_instance.get_enquiry_source_display(),
                      'buyer_postcode': '',
                      'buyer_state': '',
                      'firb_status': self.buyer_instance.get_firb_display(),
                      'property_name': property_name,
                      'property_type': property_type,
                      'send_to_agent': 1,
                      'send_to_client': 1,
                      'send_to_developer': 1,
                      'user_id': user_id,
                      'cms_project_id': project_id,
                      'project_id': project_project_id,
                      'apartment_id': self.buyer_instance.property_id}

            data = urllib.parse.urlencode(values)
            full_url = 'http://live.displaysweet.com/email2/sendEmail.php?' + data

            ProjectSyncBackLog.objects.create(direction=0, project_id=project_id, log=full_url)

            print ('reservation full_url: ')
            print (full_url)
            print ('----------')

            try:
                urllib.request.urlopen(full_url)
            except Exception as e:
                print ('Error submitting registration email to live: ', e)



class ReservationSummaryForm(forms.ModelForm):
    pass
    # class Meta:
    #     model = Purchaser
    #     fields = ('agent_bonus', 'rebate', 'rental_guarantee',)


class ReservationRentalForm(forms.ModelForm):
    pass
    # class Meta:
    #     model = Purchaser
    #     fields = ('sales_price', 'sales_type', 'weekly_amount', 'percentage_amount', 'number_of_weeks', 'max_cost',
    #               'weeks_leased', 'leased_amount', 'recouped', 'current_position')

class ReservationContractForm(forms.ModelForm):
    pass
    # class Meta:
    #     model = Purchaser
    #     fields = ('reservation_fee_date', 'reservation_fee_refund', 'contract_exchange_date', 'cancelled',
    #               'deposit_date', 'deposit_refund')


class ReservationAgentForm(forms.ModelForm):

    agent_first_name = forms.CharField(max_length=100, label="Agent First name", required=True)
    agent_last_name = forms.CharField(max_length=100, label="Agent Last name", required=True)
    agent_email = forms.EmailField(label="Agent Email Address", required=True)
    agent_company_name = forms.CharField(max_length=100,label="Agent Company Name", required=False)
    agent_company_website =  forms.CharField(max_length=100,label="Agent Website", required=False)

    class Meta:
        model = User
        fields = ('created_by',)

    def __init__(self, *args, **kwargs):
        super(ReservationAgentForm, self).__init__(*args, **kwargs)

        if self.instance:
            self.fields['agent_first_name'].initial = self.instance.first_name
            self.fields['agent_last_name'].initial = self.instance.last_name
            self.fields['agent_email'].initial = self.instance.email
            self.fields['agent_company_name'].initial = self.instance.company_name
            self.fields['agent_company_website'].initial = self.instance.website

    def clean(self):
        cleaned_data = self.cleaned_data

        return cleaned_data

    def save(self, commit=True):
        agent = super(ReservationAgentForm, self).save(commit=False)
        cleaned_data = self.cleaned_data
        if "agent_first_name" in cleaned_data:
            agent.first_name = cleaned_data.get('agent_first_name')

        if "agent_last_name" in cleaned_data:
            agent.last_name = cleaned_data.get('agent_last_name')

        if "agent_email" in cleaned_data:
            agent.email = cleaned_data.get('agent_email')

        if "agent_company_name" in cleaned_data:
            agent.company_name = cleaned_data.get('agent_company_name')

        if "agent_company_website" in cleaned_data:
            agent.website = cleaned_data.get('agent_company_website')

        agent.save()


class ProjectAddForm(forms.ModelForm):

    name = forms.CharField(label="Project name", required=True)
    project_to_copy = forms.ModelChoiceField(queryset=Project.objects.all(),
                                             label="Project to copy from. (Master is the default project template)")

    mapping_choices = (
        ('new_schema', 'New Schema'),
        ('old_schema', 'Old Schema'),
        ('postgres_schema', 'Postgres Schema'),
    )

    class Meta:
        model = Project
        fields = ('name', 'active')

    def __init__(self, *args, **kwargs):
        super(ProjectAddForm, self).__init__(*args, **kwargs)

        self.fields['project_to_copy'].initial = Project.objects.get(name__iexact='master')

        projects = Project.objects.filter(active=True)
        if projects:
            self.fields['project_to_copy'].queryset = Project.objects.filter(active=True)

        self.fields['active'].initial = True
        self.fields['active'].widget = forms.HiddenInput

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data

    def save(self, commit=True):
        project = super(ProjectAddForm, self).save(commit=False)

        project_to_copy = self.cleaned_data['project_to_copy']
        print ('project_to_copy', project_to_copy, project_to_copy.project_connection_name)

        if commit:
            if project_to_copy:
                project.project_copied_from = project_to_copy

            project.save()

            project.project_connection_name = '%s_%s' % (project.name.replace(" ", "_", 10).replace(".", "", 10).replace("'", "", 10).replace('"', "", 10), project.pk)

            project.save()


        print ('project.project_copied_from', project.project_copied_from)

        return project



class CSVFileImportAddForm(forms.ModelForm):

    class Meta:
        model = CSVFileImport
        fields = ('csv_file', 'project',)

    def __init__(self, projects, project_tables, *args, **kwargs):
        super(CSVFileImportAddForm, self).__init__(*args, **kwargs)

        self.fields['project'] = forms.ModelChoiceField(queryset=projects, widget=forms.HiddenInput)
        self.fields['csv_file'].label = "Excel File"

    def clean(self):
        cleaned_data = self.cleaned_data

        csv_file = cleaned_data.get('csv_file', None)

        if csv_file:
            if "csv" not in str(csv_file.name) and not "xlsx" in str(csv_file.name):
                self._errors['csv_file'] = self.error_class(['Please upload a valid Excel file'])

        return cleaned_data


class CSVFileImportAllTablesForm(forms.ModelForm):

    class Meta:
        model = CSVFileImport
        fields = ('csv_file', 'project')

    def __init__(self, projects, project_tables, *args, **kwargs):
        super(CSVFileImportAllTablesForm, self).__init__(*args, **kwargs)

        self.fields['project'] = forms.ModelChoiceField(queryset=projects, widget=forms.HiddenInput)
        self.fields['csv_file'].label = "Select CSV or XLSX file:"

    def clean(self):
        cleaned_data = self.cleaned_data

        csv_file = cleaned_data.get('csv_file', None)

        if csv_file:
            if "csv" not in str(csv_file.name) and not "xlsx" in str(csv_file.name):
                self._errors['csv_file'] = self.error_class(['Please upload a valid Excel file'])

        return cleaned_data


def get_object_csv_import_form(project):

    class _ObjectFilterForm(forms.ModelForm):
        class Meta:
            model = CSVFileImport
            excludes = ()
            fields = ('csv_file', 'project',)

        def __init__(self, *args, **kwargs):
            super(_ObjectFilterForm, self).__init__(*args, **kwargs)

            self.fields['project'] = forms.ModelChoiceField(queryset=Project.objects.get(pk=project.pk), widget=forms.HiddenInput)
            self.fields['project'].initial = project

    return _ObjectFilterForm


class ProjectsActionForm(forms.Form):
    comment = forms.CharField(required=False)


class CreateProjectDetailsForm(forms.Form):
    floors = forms.FloatField(required=True, label="How many floors are there?")
    apartments = forms.FloatField(required=True, label="How many apartments are there?")

    def save(self, project, commit=True):
        cleaned_data = self.cleaned_data

        #create floors and apartments

        return False



def get_override_database(model_object, queryset, model_mixin):

    class _ObjectForm(forms.ModelForm):

        database_file = forms.FileField(label="Database File", required=True)
        user_created_by = forms.IntegerField(label='', widget=forms.HiddenInput)

        class Meta:
            model = model_object
            excludes = ()
            fields = ()

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

        def save(self, commit=True):
            cleaned_data = self.cleaned_data

            group_name = cleaned_data.get('group_name', None)
            allocation_parent = cleaned_data.get('allocation_parent', None)
            user_created_by = cleaned_data.get('user_created_by', None)

            if group_name and user_created_by:
                new_sync = ProjectCMSSync()

                model_mixin.model_object("Names").create(english=group_name)
                group_name = model_mixin.model_object("Names").filter(english=group_name).order_by('-pk')[0]

                if allocation_parent:
                    model_mixin.model_object("AllocationGroups").create(name=group_name,
                                                                          allocation_group_parent_id=allocation_parent,
                                                                          owner_id=user_created_by,
                                                                          is_hidden=1)

                    new_sync.send_insert_group_sync(model_mixin.project.pk, group_name, allocation_parent, user_created_by)
                else:
                    model_mixin.model_object("AllocationGroups").create(name=group_name,
                                                                          owner_id=user_created_by,
                                                                          is_hidden=1)

                    new_sync.send_insert_group_sync(model_mixin.project.pk, group_name, ' ',
                                                    user_created_by)

            return False

    return _ObjectForm


def get_allocation_group_form(request, model_object, queryset, model_mixin):

    class _ObjectForm(forms.ModelForm):

        group_name = forms.CharField(label="Name", required=True)
        allocation_parent = forms.ModelChoiceField(queryset=queryset, required=False, label="Group Parent", widget=forms.Select)

        class Meta:
            model = model_object
            excludes = ()
            fields = ()

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

        def save(self, commit=True):
            cleaned_data = self.cleaned_data

            group_name = cleaned_data.get('group_name', None)
            allocation_parent = cleaned_data.get('allocation_parent', None)

            if group_name:
                new_sync = ProjectCMSSync()

                model_mixin.model_object("Names").create(english=group_name)
                group_name = model_mixin.model_object("Names").filter(english=group_name).order_by('-pk')[0]

                if allocation_parent:
                    model_mixin.model_object("AllocationGroups").create(name=group_name,
                                                                          allocation_group_parent=allocation_parent,
                                                                          owner_id=request.user.pk,
                                                                          is_hidden=1)

                    new_sync.send_insert_group_sync(model_mixin.project.pk,
                                                    group_name,
                                                    allocation_parent,
                                                    request.user.pk)
                else:
                    model_mixin.model_object("AllocationGroups").create(name=group_name,
                                                                          owner_id=request.user.pk,
                                                                          is_hidden=1)

                    new_sync.send_insert_group_sync(model_mixin.project.pk, group_name, ' ',
                                                    request.user.pk)

            return group_name

    return _ObjectForm


def get_search_filter_form(data, model_mixin, model_object):

    class _ObjectFilterForm(forms.Form):

        property_type = forms.ModelChoiceField(queryset=model_mixin.model_object("PropertyTypes").all(),
                                               label="Property Type", required=False, widget=forms.Select(attrs={'class': 'allocation-search'}), empty_label="Select property type")
        properties = forms.ModelChoiceField(queryset=model_mixin.model_object("Properties").all().order_by('name__english').values_list('name__english', flat=True),
                                            label="Property Name", required=False, widget=forms.Select(attrs={'class': 'allocation-search'}), empty_label="Select property")
        level = forms.ModelChoiceField(queryset=model_mixin.model_object("Floors").all().order_by('name__english').values_list('name__english', flat=True),
                                       label="Level Name", required=False, widget=forms.Select(attrs={'class': 'allocation-search'}), empty_label="Select level")
        building = forms.ModelChoiceField(queryset=model_mixin.model_object("Buildings").all().order_by('name__english').values_list('name__english', flat=True),
                                          label="Building", required=False, widget=forms.Select(attrs={'class': 'allocation-search'}), empty_label="Select building")
        users  = forms.ModelChoiceField(queryset=model_mixin.model_object("AllocatedUsers").all(), label="Allocated Users", required=False,
                                        widget=forms.Select(attrs={'class': 'allocation-search'}), empty_label="Select user")
        # text_search_details = forms.CharField(required=False)

        def __init__(self, *args, **kwargs):
            super(_ObjectFilterForm, self).__init__(*args, **kwargs)


    return _ObjectFilterForm


def get_object_filter_form(data, project_connection, model_fields, model_object, exclude_fields):

    class _ObjectFilterForm(forms.ModelForm):
        class Meta:
            model = model_object
            excludes = exclude_fields
            fields = model_fields

        def __init__(self, *args, **kwargs):
            super(_ObjectFilterForm, self).__init__(*args, **kwargs)

            value = 'All'
            for field in self.fields:
                field_choices = ['All']
                try:
                    if field in data:
                        value = str(data[field])
                    try:
                        field_choices += model_object.objects.all().using(project_connection).values_list(field, flat=True).distinct().order_by(field)
                        self.fields[field] = forms.ChoiceField(initial=value,
                                                               choices=((str(x), x) for x in field_choices),
                                                               widget=forms.Select(attrs={'class': 'auto-select-field'}))
                    except:
                        pass
                except:
                    pass

    return _ObjectFilterForm


def apply_form_filter(data, model_fields, project_model_object, project_connection_name):
    queryset = project_model_object.objects.using(project_connection_name).all()

    sorting_field = None
    if "sort_by" in data and data['sort_by']:
        sorting_field = str(data['sort_by']).replace(" ", "")

    if "sorting_direction" in data and data['sorting_direction']:
        sorting_direction = str(data['sorting_direction'])

        if sorting_direction == "asc":
            queryset = queryset.order_by(sorting_field)
        else:
            queryset = queryset.order_by('-'+sorting_field)

    for field in model_fields:
        if field in data and data[field] and not data[field] == "All":
            value = str(data[field])

            kwargs = {
                '{0}'.format(field): value,
            }

            queryset = queryset.filter(**kwargs)

    return queryset


class SelectWithTitles(forms.Select):
    def __init__(self, *args, **kwargs):
        super(SelectWithTitles, self).__init__(*args, **kwargs)
        # Ensure the titles dict exists
        self.titles = {}

    def render_option(self, selected_choices, option_value, option_label):
        title_html = (option_label in self.titles) and \
            u' title="%s" ' % escape(force_str(self.titles[option_label])) or ''

        option_value = force_str(option_value)

        selected_html = (option_value in selected_choices) and u' selected="selected"' or ''

        return u'<option value="%s"%s%s>%s</option>' % (
            escape(option_value), title_html, selected_html,
            conditional_escape(force_str(option_label)))


class ChoiceFieldWithTitles(forms.ChoiceField):
    widget = SelectWithTitles

    def __init__(self, choices=(), *args, **kwargs):
        choice_pairs = [(c[0], c[1]) for c in choices]
        super(ChoiceFieldWithTitles, self).__init__(choices=choice_pairs, *args, **kwargs)
        self.widget.titles = dict([(c[1], c[2]) for c in choices])


def get_assign_apartment_form(model_object, model_instance, choices):

    class _ObjectForm(forms.ModelForm):

        assignment = ChoiceFieldWithTitles()
        assignment_x_coordinate = forms.CharField(widget=forms.HiddenInput(attrs={"id": 'apt_x_coord_%s' % model_instance.pk }), required=False, initial=model_instance.picking_coord_x, label='')
        assignment_y_coordinate = forms.CharField(widget=forms.HiddenInput(attrs={"id": 'apt_y_coord_%s' % model_instance.pk }), required=False, initial=model_instance.picking_coord_y, label='')

        class Meta:
            model = model_object
            excludes = ()
            fields = ()

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

            selected_choices = []
            for pt in choices:
                selected_choices.append((pt.id, pt.name, pt.name))

            self.fields['assignment'] = ChoiceFieldWithTitles(choices=selected_choices, initial=model_instance)
            self.fields['assignment'].label = ''

        def save(self, commit=True):
            cleaned_data = self.cleaned_data

            assignment_x_coordinate = cleaned_data.get('assignment_x_coordinate', None)
            assignment_y_coordinate = cleaned_data.get('assignment_y_coordinate', None)

            model_instance.picking_coord_x = assignment_x_coordinate
            model_instance.picking_coord_y = assignment_y_coordinate
            model_instance.save()

            return False

    return _ObjectForm


def get_formfield_form(model_instance, model_fields, model_object, project):

    class _ObjectForm(forms.Form):

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

            for field in model_fields:
                field_instance = model_object._meta.get_field(field)
                try:
                    if type(field_instance) == ForeignKey:

                        field_model_class_name = str(field_instance.rel.to.__name__)
                        field_model_object = get_model(project.project_connection_name, field_model_class_name)

                        try:
                            option_list = field_model_object.objects.all().using(project.project_connection_name)
                        except:
                            option_list = []

                        self.fields[field] = forms.ModelChoiceField(queryset=option_list, required=True, label=field)

                        if model_instance:
                            new_field = '%s_id' % field
                            id_value = get_repr(get_field(model_instance, new_field))

                            edit_related_url = '/projects/%s/update/%s/row/%s/' % (project.pk,
                                                                                   field_model_class_name,
                                                                                   id_value)

                            self.fields[
                                field].help_text = '<a href="%s" target="_blank" style="margin-bottom: 8px;"><i class="fa fa-pencil"></i> ' \
                                                   'Edit this field in new window</a>' % edit_related_url

                    else:
                        self.fields[field] = forms.CharField(required=True, label=field)
                except:
                    continue

            if model_instance:
                for field in self.fields:
                    try:
                        value = get_repr(get_field(model_instance, field))
                        self.fields[field].initial = value
                    except:
                        new_field = '%s_id' % field
                        value = getattr(model_instance, new_field)
                        self.fields[field].initial = value

    return _ObjectForm


def get_object_form(model_instance, model_fields, model_object, exclude_fields):

    class _ObjectForm(forms.ModelForm):
        class Meta:
            model = model_object
            excludes = exclude_fields
            fields = model_fields

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

            if model_instance:
                for field in self.fields:
                    value = get_repr(get_field(model_instance, field))
                    self.fields[field].initial = value

    return _ObjectForm


def get_object_add_floor_plan_form(project, model_fields, model_object, model_instance, configuration, image_path):

    class _ObjectForm(forms.ModelForm):

        floor_plan_path = forms.CharField(required=True, label="Floor plan path", initial=image_path)
        floor_plan_image = forms.FileField(label="Select Floor Plan Image", required=True)

        class Meta:
            model = model_object
            excludes = model_fields
            fields = ()

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

        def clean(self):
            cleaned_data = self.cleaned_data

            floor_plan_image = cleaned_data.get('floor_plan_image', None)

            if floor_plan_image:
                ext = floor_plan_image.name.split('.')[-1]
                if not ext == "jpg":
                    self._errors['floor_plan_image'] = self.error_class(['The file type of "%s" you have loaded is incorrect. Please upload a jpg image.' % ext])

            return cleaned_data

        def save(self, commit=True):
            cleaned_data = self.cleaned_data

            floor_plan_image = cleaned_data.get('floor_plan_image', None)
            floor_plan_path = cleaned_data.get('floor_plan_path', None)

            floor_plan_path_split = floor_plan_path.split("/")
            image_name = floor_plan_path_split[-1:][0]

            floor_plan_path = str(floor_plan_path).replace(image_name, '')
            base_file_location = settings.BASE_IMAGE_LOCATION

            file_location = None
            for image_locations in configuration.image_configuration.all():
                file_location = '%s/%s/' % (base_file_location, floor_plan_path)

                if not os.path.isdir(file_location):
                    os.makedirs(file_location)

            if floor_plan_image and os.path.isdir(file_location):
                floor_location_and_image = '%s%s' % (file_location, image_name)

                handle_uploaded_file(floor_plan_image, floor_location_and_image)
                if os.path.isfile(floor_location_and_image):
                    return True

            self._errors['floor_plan_image'] = self.error_class(['This image could not be saved to any of the named locations above.'])
            return False

    return _ObjectForm


def get_floor_plan_form():
    class _ObjectForm(forms.Form):

        floor_plan_path = forms.CharField(required=True, label="Floor plan path")
        floor_plan_image = forms.FileField(label="Select Floor Plan Image", required=True)

        def __init__(self, *args, **kwargs):
            super(_ObjectForm, self).__init__(*args, **kwargs)

        def save(self, commit=True):
            cleaned_data = self.cleaned_data

            floor_plan_image = cleaned_data.get('floor_plan_image', None)
            floor_plan_path = cleaned_data.get('floor_plan_path', None)

            floor_plan_path_split = floor_plan_path.split("/")
            image_name = floor_plan_path_split[-1:][0]

            floor_plan_path = str(floor_plan_path).replace(image_name, '')
            base_file_location = settings.BASE_IMAGE_LOCATION

            file_location = None

            file_location = '%s/%s/' % (base_file_location, floor_plan_path)

            if not os.path.isdir(file_location):
                os.makedirs(file_location)

            if floor_plan_image and os.path.isdir(file_location):
                floor_location_and_image = '%s%s' % (file_location, image_name)

                handle_uploaded_file(floor_plan_image, floor_location_and_image)
                if os.path.isfile(floor_location_and_image):
                    return True

            return False

    return _ObjectForm


def handle_uploaded_file(f, new_file_location):
    with open(new_file_location, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def get_repr(value):
    if callable(value):
        return '%s' % value()
    return value


def get_field(instance, field):
    field_path = field.split('.')
    attr = instance
    for elem in field_path:
        try:
            attr = getattr(attr, elem)
        except AttributeError:
            return None
    return attr


class BulkImportUploadForm(happyforms.ModelForm):
    class Meta:
        model = BulkImport
        fields = ('uploaded_xlsx',)
        widgets = {
            'uploaded_xlsx': DynamicFileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(BulkImportUploadForm, self).__init__(*args, **kwargs)

        self.fields['uploaded_xlsx'].required = True
        self.fields['uploaded_xlsx'].label = "Upload file"

    def clean_uploaded_xlsx(self):
        xlsx = self.cleaned_data['uploaded_xlsx']

        try:
            workbook = ImportedWorkbook(self.instance, xlsx)
        except InvalidImportException:
            raise forms.ValidationError("The excel file you uploaded didn't match up with the expected format. Please use the form above to download the appropriate template and try again.")

        if workbook.validation_errors_for_display:
            self._errors["uploaded_xlsx"] = self.error_class(workbook.validation_errors_for_display)

        return xlsx


class CreateImportTemplateForm(forms.ModelForm):
    template_name = forms.CharField(label="", initial="New Template Name", required=True, help_text="Enter an informative name for this template.", widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': 'autofocus'}))
    project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput)

    class Meta:
        model = ProjectCSVTemplate
        fields = ('project', 'template_name')


def get_create_import_template_form(project, template_id, instance=None):
    class _ObjectForm(forms.ModelForm):

        template_name = forms.CharField(label="", initial="New Template Name", required=True, help_text="Enter an informative name for this template.", widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': 'autofocus'}))
        project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput, initial=project)

        class Meta:
            model = ProjectCSVTemplate
            fields = ('project', 'template_name')

    return _ObjectForm




class BulkImportWithoutForm(happyforms.ModelForm):

    class Meta:
        model = BulkImportCompletedTemplate
        fields = ('uploaded_xlsx', 'template')
        widgets = {
            'uploaded_xlsx': DynamicFileInput(),
            }

    def __init__(self, *args, **kwargs):

        project = None
        if "project" in kwargs:
            project = kwargs['project']

        super(BulkImportWithoutForm, self).__init__(*args, **kwargs)

        self.fields['uploaded_xlsx'].required = True
        self.fields['uploaded_xlsx'].label = "Upload file"

        if project:
            try:
                self.fields['template'].queryset = ProjectCSVTemplate.objects.filter(project=project)
            except:
                pass

    def clean(self):
        cleaned_data = self.cleaned_data

        xlsx = cleaned_data.get('uploaded_xlsx', None)
        template = cleaned_data.get('template', None)

        if template:
            try:
                workbook = TemplateImportedWorkBook(self.instance, template, xlsx)
            except:
                raise forms.ValidationError("The excel file you uploaded didn't match up with the expected format. "
                                            "Please download the appropriate template and try again.")

            if workbook.validation_errors_for_display:
                self._errors["uploaded_xlsx"] = self.error_class(workbook.validation_errors_for_display)
        else:
            self._errors["template"] = self.error_class(("", "Please select a template."))

        return cleaned_data


class BulkImportCompletedForm(happyforms.ModelForm):

    template = forms.ModelChoiceField(queryset=ProjectCSVTemplate.objects.all(), required=True, label="Template")

    class Meta:
        model = BulkImportCompletedTemplate
        fields = ('uploaded_xlsx', 'template')
        widgets = {
            'uploaded_xlsx': DynamicFileInput(),
            }

    def __init__(self, *args, **kwargs):

        project = None
        if "project" in kwargs:
            project = kwargs['project']

        super(BulkImportCompletedForm, self).__init__(*args, **kwargs)

        self.fields['uploaded_xlsx'].required = True
        self.fields['uploaded_xlsx'].label = "Upload file"

        if project:
            try:
                self.fields['template'].queryset = ProjectCSVTemplate.objects.filter(project=project)
            except:
                pass

    def clean(self):
        cleaned_data = self.cleaned_data

        xlsx = cleaned_data.get('uploaded_xlsx', None)
        template = cleaned_data.get('template', None)

        if template:
            file_extension = xlsx.name[-3:]
            if file_extension in ["csv", "txt"]:
                pass
            else:
                try:
                    workbook = TemplateImportedWorkBook(self.instance, template, xlsx)
                except Exception as e:
                    print('error', e)
                    raise forms.ValidationError("The excel file you uploaded didn't match up with the expected format. "
                                                "Please download the appropriate template and try again.")

                if workbook.validation_errors_for_display:
                    self._errors["uploaded_xlsx"] = self.error_class(workbook.validation_errors_for_display)
        else:
            self._errors["template"] = self.error_class(("", "Please select a template."))

        return cleaned_data



def get_template_creator_formset(project):

    class _ObjectForm(forms.ModelForm):

        empty_selects = [None]
        empty_choices = ((None, 'Select a field mapping') for c in empty_selects)

        tables = [
            'Select a table mapping',
            'Buildings',
            'Properties',
            'Propertiesstringfields',
            'Propertiesintfields',
            'Propertiesbooleanfields',
            'Propertiesfloatfields',
        ]
        table_choices = ((c, c) for c in tables)

        column_table_mapping = forms.ChoiceField(label='', required=False, widget=forms.Select(attrs={'class': 'input-sm table_mapping'}),
                                                 choices=table_choices)

        column_field_mapping = forms.CharField(label='', required=False, widget=forms.Select(attrs={'class': 'input-sm column_mapping'}))

        column_field_mapping_new = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'class': 'column_mapping_new input-sm', 'placeholder': 'Enter new string field value'}))

        column_field_mapping_type = forms.ChoiceField(label='', initial='text_input', required=False, widget=forms.Select, choices=[])

        column_pretty_name = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'class': ' form-control'}))

        form_row_shown = forms.BooleanField(initial=True, required=False, widget=forms.HiddenInput(attrs={'class': 'form_row_shown'}))

        is_aspects_field = forms.BooleanField(initial=False, label='', required=False, widget=forms.CheckboxInput())

        order = forms.IntegerField(label="", required=False, initial=0)

        class Meta:
            model = ProjectCSVTemplateFields
            fields = ('template', 'column_table_mapping', 'column_field_mapping',
                      'column_pretty_name', 'column_field_mapping_type', 'order', 'is_aspects_field')

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance and self.instance.column_field_mapping:
                self.fields['column_field_mapping'].initial = self.instance.column_field_mapping

    return _ObjectForm


def get_template_creator_csv_column_formset(project, csv_column=None):

    class _ObjectForm(forms.ModelForm):

        empty_selects = [None]
        empty_choices = ((None, 'Select a field mapping') for c in empty_selects)

        tables = project.import_table_list
        table_choices = ((c, c) for c in tables)

        column_table_mapping = forms.ChoiceField(label='', required=False, widget=forms.Select(attrs={'class': 'input-sm table_mapping'}),
                                                 choices=table_choices)

        column_field_mapping = forms.CharField(label='', required=False, widget=forms.Select(attrs={'class': 'input-sm column_mapping'}))
        column_field_mapping_new = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'class': 'column_mapping_new input-sm', 'placeholder': 'Enter new string field value'}))
        column_field_mapping_type = forms.ChoiceField(label='', initial='text_input', required=False, widget=forms.Select, choices=[])
        column_pretty_name = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'class': ' input-sm form-control'}))
        form_row_shown = forms.BooleanField(initial=True, required=False, widget=forms.HiddenInput(attrs={'class': 'form_row_shown'}))

        is_aspects_field = forms.BooleanField(initial=False, label='', required=False, widget=forms.CheckboxInput())

        order = forms.IntegerField(label="", required=False, initial=0)

        csv_column_name = forms.CharField(label="", required=False, widget=forms.HiddenInput())

        class Meta:
            model = CSVFileImportFields
            fields = ('csv_column_name', 'column_table_mapping', 'column_field_mapping',
                      'column_pretty_name', 'column_field_mapping_type', 'order', 'is_aspects_field')

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance and self.instance.column_field_mapping:
                self.fields['column_field_mapping'].initial = self.instance.column_field_mapping

        def clean(self):
            cleaned_data = self.cleaned_data

            csv_column_name = cleaned_data.get('csv_column_name', None)
            #csv_column_name = csv_column_name.replace("/", "").replace("$", "").replace("2", "").replace('\n', ' ', 10).replace('\r', '', 10).replace('(', '', 10).replace(')', '', 10).replace('%', '', 10).replace('&', '', 10)

            csv_column_name = csv_column_name.strip()

            cleaned_data['csv_column_name'] = csv_column_name

            return cleaned_data

    return _ObjectForm


def get_apartment_template_formset(project, model_mixin, apartment, building_id=None):

    class _ObjectForm(forms.ModelForm):

        empty_selects = [None]
        empty_choices = ((None, 'Select a field mapping') for c in empty_selects)

        tables = project.import_table_list
        table_choices = ((c, c) for c in tables)

        column_table_mapping = forms.ChoiceField(label='', required=False,
                                                 widget=forms.HiddenInput(attrs={'class': 'input-sm table_mapping'}),
                                                 choices=table_choices)

        column_field_mapping = forms.CharField(label='', required=False,
                                               widget=forms.HiddenInput(attrs={'class': 'input-sm column_mapping'}))
        is_aspects_field = forms.BooleanField(initial=False, label='', required=False, widget=forms.CheckboxInput())

        apartment_value = forms.CharField(label='', required=False,
                                          widget=forms.TextInput(attrs={'class': ' input-sm form-control'}))

        class Meta:
            model = ProjectCSVTemplateFields
            fields = ('template', 'column_table_mapping', 'column_field_mapping', 'column_pretty_name',)

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance and self.instance.column_field_mapping:
                self.fields['column_field_mapping'].initial = self.instance.column_field_mapping

            if model_mixin and apartment:
                if self.instance.column_pretty_name.strip().lower() == "floor plan typical" and not self.instance.column_field_mapping:
                    self.instance.column_field_mapping = "floor_plan_typical"
                    self.instance.save()

                instance = model_mixin.klass("Properties").get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                            self.instance.column_field_mapping,
                                                                                            apartment.pk)

                if instance:
                    try:
                        model_dict = model_to_dict(instance)
                        if "type_string" in model_dict:
                            self.fields['apartment_value'].initial = model_dict["value"]
                        else:
                            value = model_dict[self.instance.column_field_mapping]

                            check_foreign_key, foreign_key_options = model_mixin.check_model_field_mapping(self.instance.column_table_mapping,
                                                                                                           self.instance.column_field_mapping,
                                                                                                           field_value=value,
                                                                                                           limit_to_building=building_id)
                            if check_foreign_key and foreign_key_options:

                                if len(foreign_key_options) >= 1 and not isinstance(foreign_key_options, string_types):
                                    choice_pairs = [(c[0], c[1]) for c in foreign_key_options]
                                    self.fields['apartment_value'] = forms.ChoiceField(label="", choices=choice_pairs, widget=forms.Select(attrs={'class': 'input-sm form-control'}), initial=value)
                                else:
                                    self.fields['apartment_value'].initial = foreign_key_options
                            else:
                                self.fields['apartment_value'].initial = value
                    except Exception as e:
                        print ('error in editable form 3', e)

        def clean(self):
            cleaned_data = self.cleaned_data

            return cleaned_data

        def save_apartment_details(self, model_mixin, apartment):
            cleaned_data = self.cleaned_data

            if "apartment_value" in cleaned_data:
                instance = model_mixin.get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                  self.instance.column_field_mapping,
                                                                                  apartment.pk)


                if instance:
                    model_dict = model_to_dict(instance)
                    current_value = None

                    if not "type_string" in model_dict:
                        current_value = model_dict[self.instance.column_field_mapping]

                    model_mixin.save_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                            self.instance.column_field_mapping,
                                                                            apartment.pk,
                                                                            cleaned_data.get("apartment_value"),
                                                                            field_value=current_value)

    return _ObjectForm


def get_new_property_template_formset(project, model_mixin, apartment, building_id=None):

    class _ObjectForm(forms.ModelForm):

        empty_selects = [None]
        empty_choices = ((None, 'Select a field mapping') for c in empty_selects)

        tables = project.import_table_list
        table_choices = ((c, c) for c in tables)

        column_table_mapping = forms.ChoiceField(label='', required=False, widget=forms.HiddenInput(attrs={'class': 'input-sm table_mapping'}),
                                                 choices=table_choices)

        column_field_mapping = forms.CharField(label='', required=False, widget=forms.HiddenInput(attrs={'class': 'input-sm column_mapping'}))
        is_aspects_field = forms.BooleanField(initial=False, label='', required=False, widget=forms.CheckboxInput())

        apartment_value = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'class': ' input-sm form-control'}))

        class Meta:
            model = ProjectCSVTemplateFields
            fields = ('template', 'column_table_mapping', 'column_field_mapping', 'column_pretty_name',)

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance and self.instance.column_field_mapping:
                self.fields['column_field_mapping'].initial = self.instance.column_field_mapping

            if model_mixin and apartment:
                instance = model_mixin.klass("Properties").get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                            self.instance.column_field_mapping,
                                                                                            apartment.pk)

                if instance:
                    try:
                        model_dict = model_to_dict(instance)
                        if "type_string" in model_dict:
                            checked_value = model_dict['value']
                            property_status = ['available', 'reserved', 'sold', 'pending', 'unavailable']
                            if str(checked_value).lower() in property_status:
                                choice_pairs = [(str(c).capitalize(), str(c).capitalize()) for c in property_status]
                                self.fields['apartment_value'] = forms.ChoiceField(label="", choices=choice_pairs,
                                                                                   widget=forms.Select(attrs={
                                                                                       'class': 'input-sm form-control'}))
                            else:
                                self.fields['apartment_value'].initial = ''
                        else:
                            value = model_dict[self.instance.column_field_mapping]
                            check_foreign_key, foreign_key_options = model_mixin.check_model_field_mapping(self.instance.column_table_mapping,
                                                                                                           self.instance.column_field_mapping,
                                                                                                           field_value=value,
                                                                                                           limit_to_building=building_id)

                            if check_foreign_key and foreign_key_options:
                                if len(foreign_key_options) > 1 and not isinstance(foreign_key_options, string_types):
                                    choice_pairs = [(c[0], c[1]) for c in foreign_key_options]
                                    self.fields['apartment_value'] = forms.ChoiceField(label="", choices=choice_pairs, widget=forms.Select(attrs={'class': 'input-sm form-control'}))
                    except Exception as e:
                        print('error in editable form 1', e)

        def clean(self):
            cleaned_data = self.cleaned_data

            return cleaned_data

        def save_apartment_details(self, model_mixin, apartment):
            cleaned_data = self.cleaned_data

            if "apartment_value" in cleaned_data:
                instance = model_mixin.get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                  self.instance.column_field_mapping,
                                                                                  apartment.pk)

                model_dict = model_to_dict(instance)
                current_value = None

                if not "type_string" in model_dict:
                    current_value = model_dict[self.instance.column_field_mapping]

                model_mixin.save_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                        self.instance.column_field_mapping,
                                                                        apartment.pk,
                                                                        cleaned_data.get("apartment_value"),
                                                                        field_value=current_value)

    return _ObjectForm


def get_allocations_property_formset(project, model_mixin, apartment):
    class _ObjectForm(forms.ModelForm):

        empty_selects = [None]
        empty_choices = ((None, 'Select a field mapping') for c in empty_selects)

        tables = project.import_table_list
        table_choices = ((c, c) for c in tables)

        column_table_mapping = forms.ChoiceField(label='', required=False,
                                                 widget=forms.HiddenInput(attrs={'class': 'input-sm table_mapping'}),
                                                 choices=table_choices)

        column_field_mapping = forms.CharField(label='', required=False,
                                               widget=forms.HiddenInput(attrs={'class': 'input-sm column_mapping'}))
        is_aspects_field = forms.BooleanField(initial=False, label='', required=False, widget=forms.CheckboxInput())

        apartment_value = forms.CharField(label='', required=False,
                                          widget=forms.TextInput(attrs={'class': ' input-sm form-control'}))

        class Meta:
            model = ProjectCSVTemplateFields
            fields = ('template', 'column_table_mapping', 'column_field_mapping', 'column_pretty_name',)

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance and self.instance.column_field_mapping:
                self.fields['column_field_mapping'].initial = self.instance.column_field_mapping

            if model_mixin and apartment:
                instance = model_mixin.get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                  self.instance.column_field_mapping,
                                                                                  apartment.pk)

                if instance:
                    try:
                        model_dict = model_to_dict(instance)
                        if "type_string" in model_dict:
                            self.fields['apartment_value'].initial = model_dict["value"]
                        else:
                            value = model_dict[self.instance.column_field_mapping]

                            check_foreign_key, foreign_key_options = model_mixin.check_model_field_mapping(
                                self.instance.column_table_mapping,
                                self.instance.column_field_mapping,
                                field_value=value)
                            if check_foreign_key and foreign_key_options:
                                if len(foreign_key_options) > 1 and not isinstance(foreign_key_options, string_types):
                                    choice_pairs = [(c[0], c[1]) for c in foreign_key_options]
                                    self.fields['apartment_value'] = forms.ChoiceField(label="", choices=choice_pairs,
                                                                                       widget=forms.Select(attrs={
                                                                                           'class': 'input-sm form-control'}),
                                                                                       initial=value)
                                else:
                                    self.fields['apartment_value'].initial = foreign_key_options
                            else:
                                self.fields['apartment_value'].initial = value
                    except Exception as e:
                        print('error in editable form 2', e)

        def clean(self):
            cleaned_data = self.cleaned_data

            return cleaned_data

        def save_apartment_details(self, model_mixin, apartment):
            cleaned_data = self.cleaned_data

            if "apartment_value" in cleaned_data:
                instance = model_mixin.get_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                                  self.instance.column_field_mapping,
                                                                                  apartment.pk)

                model_dict = model_to_dict(instance)
                current_value = None

                if not "type_string" in model_dict:
                    current_value = model_dict[self.instance.column_field_mapping]

                model_mixin.save_apartment_strings_type_value(self.instance.column_table_mapping,
                                                                        self.instance.column_field_mapping,
                                                                        apartment.pk,
                                                                        cleaned_data.get("apartment_value"),
                                                                        field_value=current_value)

    return _ObjectForm


def get_property_name_template_form(project, model_mixin, model_object):

    class _ObjectForm(forms.ModelForm):

        english = forms.CharField(label='', required=False)
        property = forms.CharField(required=False)

        class Meta:
            model = model_object
            fields = ('english', )
            excludes = ()

        def __init__(self, *arg, **kwarg):
            super(_ObjectForm, self).__init__(*arg, **kwarg)
            self.empty_permitted = True

            if self.instance:
                self.fields['english'].initial = ' ' #model_mixin.get_property_name_value(self.instance)

                property_value = model_mixin.model_object("Properties").filter(name_id=self.instance.pk)
                if property_value:
                    property_value = property_value[0]
                    self.fields['property'].value = property_value

        def clean(self):
            cleaned_data = self.cleaned_data
            return cleaned_data

        def save(self, commit=True):
            name = super(_ObjectForm, self).save(commit=False)

            cleaned_data = self.cleaned_data
            name = cleaned_data.get('english', None)

            if name:
                if self.instance and self.instance.pk:
                    names = model_object.objects.using(project.project_connection_name).filter(pk=self.instance.pk)
                    if names:
                        names[0].english = name
                        names[0].save(using=project.project_connection_name)

    return _ObjectForm

class ImageTypeForm(forms.Form):
    image_type = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'input-sm'}))


def ProjectImagePathForm(project, image_types_queryset, model_mixin):

    image_type_choices = ['',]
    output_choices = ['',]
    mode_choices = ['',]

    for type in image_types_queryset:
        if type and type.english:
            image_type_choices.append(type.english)

    class _ObjectFilterForm(forms.ModelForm):

        project = forms.ModelChoiceField(queryset=Project.objects.all(), label='', required=False)

        choice_pairs = [(c, c) for c in image_type_choices]
        image_type = forms.ChoiceField(choices=choice_pairs, label='', required=False)

        choice_pairs = [(c, c) for c in output_choices]
        image_output = forms.ChoiceField(choices=choice_pairs, label='', required=False)

        choice_pairs = [(c, c) for c in mode_choices]
        image_mode = forms.ChoiceField(choices=choice_pairs, label='', required=False)

        image_path = forms.CharField(required=False, label='')
        description = forms.CharField(required=False, label='')
        image_files = forms.FileField(required=False, label='')


        class Meta:
            model = ProjectImagePath
            fields = ('image_type', 'image_path', 'image_output', 'image_mode', 'description', 'project')

        def __init__(self, *args, **kwargs):
            super(_ObjectFilterForm, self).__init__(*args, **kwargs)

            self.fields['image_path'].widget = forms.TextInput(attrs={'class': 'input-sm'})
            self.fields['description'].widget = forms.TextInput(attrs={'class': 'input-sm'})

            self.fields['image_path'].label = ''
            self.fields['image_mode'].label = ''
            self.fields['image_output'].label = ''

            self.fields['description'].label = ''

        def clean(self):
            cleaned_data = self.cleaned_data
            if cleaned_data:
                image_path = cleaned_data.get('image_path', None)
                if not image_path:
                    cleaned_data['image_type'] = None
                    cleaned_data['image_path'] = None
                    cleaned_data['description'] = None
                    cleaned_data['project'] = None
            return cleaned_data

    return _ObjectFilterForm