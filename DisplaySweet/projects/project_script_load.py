import lzma
import zipfile
import threading
import os
import sqlite3
import re

import xml.etree.ElementTree as ET

class PixileScriptDbUpdater:
    def __init__(self, location, newDatabaseId):
        self.location = location
        self.newDbId = int(newDatabaseId)
        if newDatabaseId == "":
            self.newDbId = None

        print('at conversion')
        self.doConversion()
        print('at conversion end')

    def findDatabaseId(self, rootNode):
        if rootNode.tag == "Attribute":
            if rootNode.attrib['name'] == "Database ID":
                return rootNode.attrib['value']
        for node in rootNode:
            dbId = self.findDatabaseId(node)
            if dbId != None:
                return dbId

        return None

    def replaceDatabaseId(self, node):
        if "value" in node.attrib:
            matches = re.search('databaseID=[0-9]+', node.attrib['value'])
            if matches != None:
                node.attrib['value'] = node.attrib['value'].replace(matches.group(0), 'databaseID=%d' % (self.newDbId))

        if "name" in node.attrib and node.attrib['name'] == "Database ID":
            node.attrib['value'] = str(self.newDbId)

        for child in node:
            self.replaceDatabaseId(child)

    def stripPixelScript(self, rootNode):
        print ("stripPixelScript", rootNode.tag)
        if self.newDbId != None:
            self.replaceDatabaseId(rootNode)

    def stripFiles(self, rootNode):
        databaseId = self.findDatabaseId(rootNode)
        print ("Stripping Pixile script")
        self.stripPixelScript(rootNode)

    def sanitiseXml(self, xmlString):
        return xmlString.replace("::", "_._")

    def desanitiseXml(self, xmlString):
        return xmlString.replace("_._", "::")

    def doConversion(self):
        if os.path.isfile(self.location):
            locationParts = self.location.split('.')
            locationParts[-2] += "_stripped"
            outputFileName = ".".join(locationParts)
            if ".pxz" in self.location:
                with zipfile.ZipFile(self.location, 'r') as f:
                    wantedFiles = [x for x in filter(lambda x: ".pxl" in x, f.namelist())]
                    newFileString = {}
                    for script in wantedFiles:
                        info = f.getinfo(script)
                        fileBytes = self.sanitiseXml(f.read(info).decode('UTF-8'))
                        root = ET.fromstring(fileBytes)
                        self.stripFiles(root)
                        outputBytes = ET.tostring(root, encoding='utf-8', method='xml')
                        outputString = self.desanitiseXml(outputBytes.decode('UTF-8'))
                        newFileString[script] = outputString

                    with zipfile.ZipFile(outputFileName, 'w') as w:
                        for archiveFile in f.namelist():
                            if archiveFile in newFileString:
                                w.writestr(f.getinfo(archiveFile), newFileString[archiveFile])
                            else:
                                info = f.getinfo(archiveFile)
                                fdata = f.read(info)
                                w.writestr(info, fdata)

            else:
                outputString = ""
                with open(self.location, 'r', encoding='utf-8') as f:
                    fileBytes = self.sanitiseXml(f.read())
                    root = ET.fromstring(fileBytes)
                    self.stripFiles(root)
                    outputBytes = ET.tostring(root, encoding='utf-8', method='xml')
                    outputString = self.desanitiseXml(outputBytes.decode('UTF-8'))

                with open(outputFileName, 'w', encoding='utf-8') as f:
                    f.write(outputString)

        else:
            print ("Invalid File")
