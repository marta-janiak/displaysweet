from django.conf.urls import patterns, url


from .views import ProjectListView, ProjectListCallbackView, ProjectCreateView, ProjectDetailView, \
                   ProjectDeleteView,ProjectUpdateView, ProjectModelDetailView


urlpatterns = patterns('',
    # url(r'^$', ProjectListView.as_view(), name='projects-list'),
    url(r'^$', 'projects.views.project_list_view', name='projects-list'),
    url(r'^list_callback/$', ProjectListCallbackView.as_view(), name='projects-list_callback'),
    url(r'^create/$', ProjectCreateView.as_view(), name='projects-create'),
    
    url(r'^(?P<pk>\d+)/$', ProjectDetailView.as_view(), name='projects-view'),

    url(r'^(?P<pk>\d+)/history/$', 'projects.views.project_version_details', name='projects-version'),
    url(r'^(?P<pk>\d+)/history/(?P<version_idx>\d+)/$', 'projects.views.project_version_details', name='projects-version-idx'),

    url(r'^(?P<pk>\d+)/update/$', ProjectUpdateView.as_view(), name='projects-update'),

    url(r'^restart/$', 'projects.views.force_restart', name='projects-restart'),
    url(r'^latest/version/(?P<pk>\d+)/$', 'projects.views.get_active_project_version', name='projects-latest-version'),

    url(r'^family/management/$', 'projects.views.project_families', name='projects-family-management'),
    url(r'^family/management/(?P<pk>\d+)/$', 'projects.views.project_families', name='projects-family-edit'),

    url(r'^(?P<pk>\d+)/delete/$', 'projects.views.ProjectDeleteView', name='projects-delete'),
    url(r'^(?P<pk>\d+)/building/list/$', 'projects.views.project_buildings', name='projects-building-list'),
    url(r'^(?P<pk>\d+)/floors/list/$', 'projects.views.project_floors', name='projects-floors-list'),
    url(r'^(?P<pk>\d+)/hotspots/$', 'projects.views.project_hotspot_redirect', name='projects-go-hotspots'),
    url(r'^(?P<pk>\d+)/users/$', 'projects.views.project_user_list', name='projects-user-list'),
    url(r'^(?P<pk>\d+)/administration/$', 'projects.views.project_administration', name='projects-administration'),
    url(r'^(?P<pk>\d+)/view/allocations/$', 'projects.views.project_allocations', name='project-allocations'),

    url(r'^(?P<pk>\d+)/save/database/$', 'projects.views.save_project_database', name='project-save-database'),


    url(r'^(?P<pk>\d+)/custom/(?P<model_name>\w+)/row/(?P<row_id>\d+)/$', 'projects.views.update_floors_row', name='projects-floors-update'),
    url(r'^(?P<pk>\d+)/floors/hotspots/(?P<floor_id>\d+)/$', 'projects.views.project_floors_hotpots', name='projects-floors-hotspots'),

    url(r'^(?P<pk>\d+)/floors/hotspots/(?P<floor_id>\d+)/reassign/$', 'projects.views.project_floors_hotpots_reassign', name='projects-floors-hotspots-reassign'),

    url(r'^(?P<pk>\d+)/floors/(?P<floor_id>\d+)/views/$', 'projects.views.project_floors_views', name='projects-floors-views'),



    url(r'^(?P<pk>\d+)/custom/(?P<model_name>\w+)/spot/(?P<row_id>\d+)/$', 'projects.views.update_hotspot_custom_row', name='projects-hotspot-update'),
    url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/(?P<row_id>\d+)/$', 'projects.views.update_apartment_views_custom_row', name='projects-apartment-views-update'),

    url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/(?P<row_id>\d+)/save/(?P<apartment_id>\d+)/$', 'projects.views.update_apartment_save_view', name='projects-save-assigned-view'),
    url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/(?P<row_id>\d+)/remove/(?P<apartment_id>\d+)/$', 'projects.views.update_apartment_remove_view', name='projects-remove-assigned-view'),

    url(r'^(?P<pk>\d+)/apartment/types/plan/image/(?P<apartment_type_id>\d+)/$', 'projects.views.update_apartment_types_floor_plan_image', name='projects-apartment-types-floor-plan'),

    url(r'^(?P<pk>\d+)/floor/plan/image/(?P<floor_id>\d+)/$', 'projects.views.add_floor_plan_image', name='projects-floors-add-floor-plan'),
    url(r'^(?P<pk>\d+)/apartment/plan/image/(?P<apartment_id>\d+)/$', 'projects.views.add_floor_plan_image_apartment', name='projects-apartments-add-floor-plan'),
    url(r'^(?P<pk>\d+)/hot/spot/(?P<model_name>\w+)/configuration/(?P<row_id>\d+)/$', 'projects.views.update_hotspot_configuration', name='projects-hotspot-configuration'),

    url(r'^(?P<pk>\d+)/models/list/$', ProjectModelDetailView.as_view(), name='projects-model-list'),

    url(r'^(?P<pk>\d+)/import/header/list/$', 'projects.views.import_header_list', name='projects-import-header'),
    url(r'^(?P<pk>\d+)/import/header/list/multiple/$', 'projects.views.import_header_list_multiple_sources', name='projects-import-header-multiple'),
    url(r'^(?P<pk>\d+)/map/file/(?P<file_id>\d+)/fields/$', 'projects.views.import_csv_table_map', name='projects-map-fields'),

    url(r'^(?P<pk>\d+)/create/template/$', 'projects.views.create_template_header_list', name='projects-create-template'),
    url(r'^(?P<pk>\d+)/select/import/template/$', 'projects.views.created_import_templates', name='projects-select-template'),
    url(r'^(?P<pk>\d+)/edit/template/(?P<template_id>\d+)/$', 'projects.views.edit_import_template', name='projects-edit-template'),
    url(r'^(?P<pk>\d+)/select/template/(?P<template_id>\d+)/preview/$', 'projects.views.select_import_template_preview', name='projects-preview-imports'),


    url(r'^(?P<pk>\d+)/update/(?P<model_name>\w+)/row/(?P<row_id>\d+)/$', 'projects.views.update_model_row', name='projects-model-update'),
    url(r'^(?P<pk>\d+)/update/(?P<model_name>\w+)/row/(?P<row_id>\d+)/delete/$', 'projects.views.delete_model_row', name='projects-model-row-delete'),
    url(r'^(?P<pk>\d+)/update/(?P<model_name>\w+)/row/delete/(?P<row_id>\d+)/instant/$', 'projects.views.delete_model_row_instant', name='projects-model-delete-instant'),
    url(r'^(?P<pk>\d+)/add/(?P<model_name>\w+)/new/$', 'projects.views.add_model_row', name='projects-model-insert'),

    url(r'^(?P<pk>\d+)/update/templated/apartments/(?P<template_id>\w+)/(?P<apartment_id>\w+)/$',
        'projects.views.edit_apartments_import_template', name='apartments-edit-with-template'),

    url(r'^(?P<pk>\d+)/add/(?P<model_name>\w+)/upload/$', 'projects.views.upload_model_data', name='projects-model-upload'),
    url(r'^(?P<pk>\d+)/bulk/(?P<bulk_import_id>\w+)/upload/complete/$', 'projects.views.upload_model_data_complete', name='projects-model-upload-complete'),

    url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/$', 'projects.views.project_model', name='projects-model-view'),

    url(r'^(?P<pk>\d+)/filter/values/(?P<model_name>\w+)/$', 'projects.views.model_filter_view', name='projects-model-filter'),
    url(r'^(?P<pk>\d+)/filter/values/Apartments/$', 'projects.views.model_filter_view', name='projects-apartments-list'),
    url(r'^(?P<pk>\d+)/filter/values/Apartmenttypes/$', 'projects.views.model_filter_view', name='projects-apartment-types-list'),



    url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/download/csv/$', 'projects.views.project_model_csv_template', name='projects-model-download-template'),


    url(r'^(?P<pk>\d+)/floor/plan/editor/$', 'projects.views.floor_plan_editor', name='floor-plan-editor'),
    url(r'^(?P<pk>\d+)/floor/schema/plan/editor/$', 'projects.views.floor_plan_editor_new_schema', name='floor-plan-editor-new-schema'),
    url(r'^(?P<pk>\d+)/floor/schema/plan/editor/(?P<floor_id>\d+)/$', 'projects.views.floor_plan_editor_new_schema', name='floor-plan-editor-new-schema'),

    url(r'^(?P<pk>\d+)/floor/plan/editor/(?P<floor_id>\d+)/$', 'projects.views.floor_plan_editor', name='floor-plan-editor'),
    url(r'^(?P<pk>\d+)/floor/plan/typicals/(?P<floor_id>\d+)/$', 'projects.views.floor_plan_typicals', name='floor-plan-typicals'),
    url(r'^(?P<pk>\d+)/project/plan/editor/$', 'projects.views.new_project_plan_editor', name='project-plan-editor'),


    url(r'^(?P<pk>\d+)/map/project/image/paths/$', 'projects.views.map_project_image_paths', name='projects-map-image-paths'),
    url(r'^(?P<pk>\d+)/download/sql/$', 'projects.views.download_sql', name='projects-download-sql'),


    #url(r'^(?P<pk>\d+)/(?P<model_name>\w+)/download/data/$', 'projects.views.project_model_csv_data', name='projects-model-download-data'),
)


