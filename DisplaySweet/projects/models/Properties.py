from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin
from projects.cms_sync import ProjectCMSSync
import datetime


class Properties(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Properties, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def add_property_aspect(self, property_id, order, key, resource_id=None):

        aspect = self.model_object("PropertyAspects").filter(property_id=property_id,
                                                             key=key,
                                                             resource_id=resource_id)

        if aspect:
            aspect = aspect[0]
            aspect.orderidx = order
            aspect.save(using=self.project.project_connection_name)
            return aspect

        if not aspect:
            self.model_object("PropertyAspects").create(property_id=property_id,
                                                        resource_id=resource_id,
                                                        key=key,
                                                        orderidx=order)

            aspect = self.model_object("PropertyAspects").filter(property_id=property_id,
                                                                 resource_id=resource_id,
                                                                 key=key)

            aspect = aspect[0]
            return aspect



    def get_property_type(self, type_id):
        if type_id:
            prop_type = self.model_object("PropertyTypes").get(pk=type_id)
            return prop_type.type
        return 'n/a'

    def get_image_type_canvas(self, image_type, property_id):
        static_image = '/static/common/img/placeholder.jpg'
        image_path = '<em>n/a</em>'
        image_exists = False
        image_name = ''
        resource_id = None

        property_images = self.model_object("PropertyResources").filter(key=image_type,
                                                                        property_id=property_id).values_list("resource_id",flat=True)

        images = self.model_object("Resources").filter(pk__in=property_images).order_by('pk')

        if images:
            plan = images[0]
            image_path = plan.path
            resource_id = plan.pk
            image_exists = True
            image_name = plan.path.split("/")[-1]

            if image_path:
                image_name = image_path.split("/")[-1]
                static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                static_image = static_image.replace("//", "/")
                image_path = str(image_path)

        context = {
            'success': True,
            'static_image': str(static_image),
            'image_path': str(image_path),
            'image_exists': image_exists,
            'image_name': image_name,
            'resource_id': resource_id
        }

        return context

    ## Property Hotspot Details
    def get_hotspot_values(self, property_id, floor_id):

        picking_coord_x = None
        picking_coord_y = None
        floor_hotspot_id = None
        name = ''
        orderidx = 1

        linked_floor_spots = self.model_object("FloorsHotspots").filter(floor_id=floor_id).values_list('pk', flat=True)
        apartment_hot_spots = self.model_object("PropertyHotspots").filter(property_id=property_id,
                                                                           floors_hotspot_id__in=linked_floor_spots,
                                                                           orderidx=1)

        hotspot_list = []
        if apartment_hot_spots:
            for apartment in apartment_hot_spots:
                floors_hotspot_id = apartment.floors_hotspot_id
                if apartment.orderidx:
                    orderidx = apartment.orderidx

                linked_floor_spot = self.model_object("FloorsHotspots").filter(floor_id=floor_id, pk=floors_hotspot_id)
                for hotspot in linked_floor_spot:

                    model_dict = model_to_dict(hotspot)

                    if "x" in model_dict and "y" in model_dict:
                        picking_coord_x = model_dict['x']
                        picking_coord_y = model_dict['y']
                        hotspot_order = orderidx

                        name = ''
                        if model_dict['name']:
                            name = model_dict['name']

                    else:
                        picking_coord_x = None
                        picking_coord_y = None
                        hotspot_order = orderidx
                        name = ''

                    hotspot_list.append({'picking_coord_x': picking_coord_x,
                                         'picking_coord_y': picking_coord_y,
                                         'floor_hotspot_id': hotspot.pk,
                                         'hotspot_order': hotspot_order,
                                         'name': name})

        else:
            hotspot_list.append({'picking_coord_x': picking_coord_x,
                                 'picking_coord_y': picking_coord_y,
                                 'floor_hotspot_id': floor_hotspot_id,
                                 'hotspot_order': orderidx,
                                 'name': name
                                 })
        return hotspot_list

    def get_apartment_hotspot_values(self, property_id, floor_id=None):
        apartment_hot_spot = self.model_object("PropertyHotspots").filter(property_id=property_id)

        picking_coord_x = None
        picking_coord_y = None

        if apartment_hot_spot:
            apartment_hot_spot = apartment_hot_spot[0]

            linked_floor_spot = self.model_object("FloorsHotspots").filter(pk=apartment_hot_spot.floors_hotspot_id)

            if floor_id:
                linked_floor_spot = linked_floor_spot.filter(floor_id=floor_id)

            if linked_floor_spot:
                model_dict = model_to_dict(linked_floor_spot[0])

                if "x" in model_dict and "y" in model_dict:
                    picking_coord_x = model_dict['x']
                    picking_coord_y = model_dict['y']
                else:
                    picking_coord_x = None
                    picking_coord_y = None

        return picking_coord_x, picking_coord_y

    def save_apartment_hotspot_values(self, property, x_coordinate, y_coordinate, floor_id=0,
                                      new_hotspot_name='',
                                      hotspot_order=None):

        spots = self.model_object("FloorsHotspots").filter(floor_id=floor_id).values_list('pk', flat=True)

        apartment_hot_spot = None
        if property:
            if type(property) == int:
                apartment_hot_spot = self.model_object("PropertyHotspots").filter(property_id=property,
                                                                                  floors_hotspot_id__in=spots)
            else:
                apartment_hot_spot = self.model_object("PropertyHotspots").filter(property_id=property.pk,
                                                                                  floors_hotspot_id__in=spots)
                property = property.pk

        if not apartment_hot_spot and property:
            if new_hotspot_name:
                self.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]


            else:
                self.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name='')

                new_hotspot = self.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name='').order_by('-pk')[0]

            self.model_object("PropertyHotspots").create(property_id=property,
                                                         orderidx=hotspot_order,
                                                         floors_hotspot_id=new_hotspot.pk)

            hotspot = self.model_object("PropertyHotspots").filter(property_id=property,
                                                                   floors_hotspot_id=new_hotspot.pk).order_by('-pk')[0]

            return hotspot

        elif apartment_hot_spot and property:
            apartment_hot_spot = apartment_hot_spot[0]

            updating_hotspots = self.model_object("FloorsHotspots").filter(pk=apartment_hot_spot.floors_hotspot_id,
                                                                           floor_id=floor_id)
            if updating_hotspots:
                floor_hotspot = updating_hotspots[0]
                floor_hotspot.x = x_coordinate
                floor_hotspot.y = y_coordinate
                floor_hotspot.save(using=self.project.project_connection_name)

                hotspot = self.model_object("PropertyHotspots").filter(property_id=property,
                                                                       floors_hotspot_id=floor_hotspot.pk)

                if not hotspot:
                    self.model_object("PropertyHotspots").create(property_id=property,
                                                                 orderidx=hotspot_order,
                                                                 floors_hotspot_id=floor_hotspot.pk)

                    hotspot = self.model_object("PropertyHotspots").filter(property_id=property,
                                                                           floors_hotspot_id=floor_hotspot.pk)

                if hotspot:
                    hotspot = hotspot[0]
                    if hotspot_order:
                        hotspot.orderidx = hotspot_order
                        hotspot.save(using=self.project.project_connection_name)
                return hotspot
            else:
                if not new_hotspot_name:
                    new_hotspot_name = ''

                self.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]

                self.model_object("PropertyHotspots").create(property_id=property,
                                                             floors_hotspot_id=new_hotspot.pk)

                hotspot = self.model_object("PropertyHotspots").filter(property_id=property,
                                                                       floors_hotspot_id=new_hotspot.pk).order_by('-pk')[0]

                if hotspot_order:
                    hotspot.orderidx = hotspot_order
                    hotspot.save(using=self.project.project_connection_name)

                return hotspot

        elif not apartment_hot_spot and not property:
            if new_hotspot_name:
                self.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]
            else:
                self.model_object("FloorsHotspots").create(floor_id=floor_id, x=x_coordinate, y=y_coordinate)
                new_hotspot = \
                self.model_object("FloorsHotspots").filter(floor_id=floor_id, x=x_coordinate, y=y_coordinate).order_by('-pk')[0]

            return new_hotspot

        else:
            if new_hotspot_name:
                self.model_object("FloorsHotspots").create(floor_id=floor_id,
                                                           x=x_coordinate,
                                                           y=y_coordinate,
                                                           name=new_hotspot_name)

                new_hotspot = self.model_object("FloorsHotspots").filter(floor_id=floor_id,
                                                                         x=x_coordinate,
                                                                         y=y_coordinate,
                                                                         name=new_hotspot_name).order_by('-pk')[0]

                return new_hotspot

    ## Property Typical Updates
    def get_all_property_typicals(self):
        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string)

        type_list = []
        new_list = []
        new_list_dict = {}
        static_image = '/static/common/img/placeholder.jpg'

        for string_field in string_fields:
            property_strings = self.model_object("PropertiesStringFields").filter(type_string=string_field)

            image_id = ''

            if property_strings:
                for row in property_strings:
                    image_name = 'n/a'
                    image = self.model_object("PropertyResources").filter(property_id=row.property_id,
                                                                          key="floor_plans")

                    if image:
                        pimage = self.model_object("Resources").filter(pk=image[0].resource_id)
                        if pimage:
                            # print ('image', pimage[0].path, row.property_id

                            image = pimage[0].path
                            image_name = str(image.split("/")[-1]).lstrip().rstrip()
                            image_id = pimage[0].pk

                            static_image = '/static/' + str(pimage[0].path.replace(settings.BASE_IMAGE_LOCATION, ''))
                            static_image = static_image.replace("//", "/")

                    # print ('row.value', row.value
                    if row.value and row.value.strip() not in type_list:
                        type_list.append(row.value.strip())
                        new_list.append({row.value.strip():
                                             {'type': row.value.strip(),
                                              'id': row.pk,
                                              'string_id': row.pk,
                                              'image': image_name,
                                              'image_id': image_id,
                                              'static_image': static_image}
                                         })

                        new_list_dict.update(
                            {row.value.strip():
                                 {'type': row.value.strip(),
                                  'id': row.pk,
                                  'string_id': row.pk,
                                  'image': image_name,
                                  'image_id': image_id,
                                  'static_image': static_image}
                             }
                        )

        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string)

        for string_field in string_fields:
            if string_field.english and string_field.english.strip() not in type_list \
                    and not string_field.english.strip() == "floor_plan_typical":
                type_list.append(string_field.english.strip())
                new_list.append({string_field.english.strip():
                                     {'type': string_field.english.strip(),
                                      'id': string_field.pk,
                                      'string_id': string_field.pk,
                                      'image': 'n/a',
                                      'image_id': '',
                                      'static_image': static_image}})

                new_list_dict.update(
                    {string_field.english.strip():
                         {'type': string_field.english.strip(),
                          'id': string_field.pk,
                          'string_id': string_field.pk,
                          'image': 'n/a',
                          'image_id': '',
                          'static_image': static_image}}
                )

        sorted_list = []
        for row in new_list_dict:
            found_value = new_list_dict[row]
            if found_value:
                sorted_list.append(found_value)

        return sorted_list

    def get_all_property_typical_strings(self):
        property_category_string = self.klass("TextTableCategories").get_text_table_category(
            settings.FLOOR_PLAN_TYPICAL)

        string_fields = self.model_object("Strings").filter(category=property_category_string)
        if not string_fields:
            self.model_object("Strings").create(category=property_category_string)
            string_fields = self.model_object("Strings").filter(category=property_category_string)

        return string_fields

    def get_property_typicals(self, property_id):
        property_category_string = self.klass("TextTableCategories").get_text_table_category(
            settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string)
        for string_field in string_fields:
            property_strings = self.model_object("PropertiesStringFields").filter(property_id=property_id,
                                                                                  type_string=string_field)

            if property_strings:
                property_strings = property_strings[0]
                if property_strings.value:
                    return property_strings.value

    def update_property_typicals(self):
        try:
            property_category_string = self.klass("TextTableCategories").get_text_table_category(
                settings.FLOOR_PLAN_TYPICAL)

            check_typical_string_category = self.model_object("Strings").filter(english=settings.FLOOR_PLAN_TYPICAL)

            for checked_string in check_typical_string_category:
                if not checked_string.text_table_category_id == property_category_string:
                    checked_string.text_table_category_id = property_category_string.pk
                    checked_string.save(using=self.project.project_connection_name)

            if not check_typical_string_category:
                self.model_object("Strings").create(english=settings.FLOOR_PLAN_TYPICAL)

        except Exception as e:
            print ('update_property_typicals error: ', e)

        string_cat = self.get_string_category("propertiesstringfields")
        check_typical_string_category = self.model_object("Strings").filter(text_table_category=string_cat,
                                                                            english=settings.FLOOR_PLAN_TYPICAL)
        if not check_typical_string_category:
            self.model_object("Strings").create(text_table_category=string_cat, english=settings.FLOOR_PLAN_TYPICAL)

    def check_and_update_typicals(self):
        # An ugly fix for properties and floor plan typicals
        properties = self.model_object("Properties").all()

        for apartment in properties:
            typical_value = None

            properties_with_t = self.model_object("PropertiesStringFields").filter(type_string__english='floor_plan_typical',
                                                                                   property_id=apartment.pk)

            for property_w_typical in properties_with_t:
                if property_w_typical.value and not typical_value:
                    typical_value = property_w_typical.value

            # print('-----> apartment  ---- ', apartment, typical_value)

            properties_with_t = self.model_object("PropertiesStringFields").filter(
                                type_string__english='floor_plan_typical',
                                property_id=apartment.pk)

            for property_w_typical in properties_with_t:
                if typical_value:
                    property_w_typical.value = typical_value
                    property_w_typical.save(using=self.project.project_connection_name)

        # print('---- end ----')

    def update_with_property_typicals(self, type_string, resource_id):
        type_string = type_string.lstrip().rstrip()

        id_string_field_field = self.model_object("Strings").filter(english="floor_plan_image_id")
        if not id_string_field_field:
            self.model_object("Strings").create(english="floor_plan_image_id")
            id_string_field_field = self.model_object("Strings").filter(english="floor_plan_image_id")

        id_string_field_field = id_string_field_field[0]
        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string)

        property_strings = None
        for string_field in string_fields:
            property_strings = self.model_object("PropertiesStringFields").filter(type_string=string_field,
                                                                                  value=type_string)

            if property_strings:
                for property in property_strings:
                    property_images = self.model_object("PropertyResources").filter(property_id=property.property_id,
                                                                                    key="floor_plans")
                    if not property_images:
                        self.model_object("PropertyResources").create(property_id=property.property_id,
                                                                      key="floor_plans",
                                                                      resource_id=resource_id)

                    else:
                        for prop in property_images:
                            prop.resource_id = resource_id
                            prop.save(using=self.project.project_connection_name)

                    # Updating all int fields for this type for Aren
                    int_field_image = self.model_object("PropertiesIntFields").filter(property_id=property.property_id,
                                                                                      type_string=id_string_field_field)

                    asset = self.model_object("Assets").filter(source_id=resource_id)
                    if not asset:
                        self.model_object("Assets").create(source_id=resource_id, asset_type_id=1)
                        asset = self.model_object("Assets").filter(source_id=resource_id)

                    asset = asset[0]
                    if not int_field_image:
                        self.model_object("PropertiesIntFields").create(property_id=property.property_id,
                                                                        type_string=id_string_field_field,
                                                                        value=asset.pk)
                    else:
                        int_field_image = int_field_image[0]
                        int_field_image.value = asset.pk
                        int_field_image.save(using=self.project.project_connection_name)

        if property_strings:
            return True
        return False
    
    def set_property_status_reserved_wsync(self, property_id):
        string_fields = self.klass("Properties").get_or_create_property_string_field(property_id, 'status')
        reservation_string_fields = self.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                 'Reservation Status')
        date_reserved_fields = self.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                            'Date Reserved')
        
        new_sync = ProjectCMSSync()

        for string_field in string_fields:
            string_field.value = 'Reserved'
            string_field.save(using=self.project.project_connection_name)

            for date_field in date_reserved_fields:
                date_field.value = datetime.datetime.now()
                date_field.save(using=self.project.project_connection_name)
    
                new_sync.send_update_sync(self.project.pk, 'properties_string_fields', 'value',
                                          datetime.datetime.now(),
                                          'Date Reserved', property_id)
    
            for status_field in reservation_string_fields:
                status_field.value = 'Pending Approval'
                status_field.save(using=self.project.project_connection_name)
    
                new_sync.send_update_sync(self.project.pk, 'properties_string_fields', 
                                          'value', 'Pending Approval',
                                          'Reservation Status', property_id)

    def set_property_status_available_wsync(self, property_id):
        string_fields = self.klass("Properties").get_or_create_property_string_field(property_id, 'status')
        reservation_string_fields = self.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                                 'Reservation Status')
        date_reserved_fields = self.klass("Properties").get_or_create_property_string_field(property_id,
                                                                                            'Date Reserved')

        new_sync = ProjectCMSSync()

        for string_field in string_fields:
            string_field.value = 'Available'
            string_field.save(using=self.project.project_connection_name)

            new_sync.send_update_sync(self.project.pk, 'properties_string_fields',
                                      'value', 'Available',
                                      'status', property_id)

            for date_field in date_reserved_fields:
                date_field.value = datetime.datetime.now()
                date_field.save(using=self.project.project_connection_name)

                new_sync.send_update_sync(self.project.pk, 'properties_string_fields', 'value',
                                          '',
                                          'Date Reserved', property_id)

            for status_field in reservation_string_fields:
                status_field.value = ''
                status_field.save(using=self.project.project_connection_name)

                new_sync.send_update_sync(self.project.pk, 'properties_string_fields',
                                          'value', ' ',
                                          'Reservation Status', property_id)

    def add_new_property_typical_string(self, string_value):

        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                            english=string_value)
        if not string_fields:
            self.model_object("Strings").create(text_table_category=property_category_string, english=string_value)
            string_fields = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                                english=string_value)

        return string_fields[0]

    def remove_new_property_typical_string(self, string_value_id):

        string_fields = self.model_object("Strings").filter(english=settings.FLOOR_PLAN_TYPICAL)

        for floor_plan_typical in string_fields:
            self.model_object("PropertiesStringFields").filter(type_string=floor_plan_typical,
                                                               value=string_value_id).delete()

        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)
        self.model_object("Strings").filter(text_table_category=property_category_string, english=string_value_id).delete()

    def add_typical_string_to_property(self, property_id, set_string):

        property_category_string = self.klass("TextTableCategories").get_text_table_category(settings.FLOOR_PLAN_TYPICAL)

        self.model_object("PropertiesStringFields").filter(property_id=property_id,
                                                           type_string__text_table_category=property_category_string).delete()

        if set_string:
            string_field = self.model_object("Strings").filter(english=set_string,
                                                               text_table_category=property_category_string)
            if not string_field:
                self.model_object("Strings").create(english=set_string,
                                                    text_table_category=property_category_string)

                string_field = self.model_object("Strings").filter(english=set_string,
                                                                   text_table_category=property_category_string)

            string_field = string_field[0]

            self.model_object("PropertiesStringFields").update_or_create(property_id=property_id,
                                                                         type_string=string_field,
                                                                         defaults={'value': set_string})
            return True
        
        
    #Property Resources
    def get_property_aspects_floor(self, floor_id):
        directions = ['north', 'south', 'east', 'west']
        aspects = self.model_object("FloorsPlans").filter(floor_id=floor_id, key__in=directions)
        return aspects

    def remove_property_resource_by_id(self, aspect_id=None):

        if aspect_id:
            self.model_object("PropertyResources").filter(pk=aspect_id).delete()

    def remove_property_resource(self, aspect_id):

        if aspect_id:
            self.model_object("PropertyResources").filter(pk=aspect_id).delete()
            
    #Aspects
    def remove_property_aspect_by_id(self, aspect_id=None):

        if aspect_id:
            self.model_object("PropertyAspects").filter(pk=aspect_id).delete()

    def remove_property_aspect(self, property_id, aspect, aspect_id=None):

        if aspect_id:
            self.model_object("PropertyAspects").filter(pk=aspect_id).delete()
        else:
            self.model_object("PropertyAspects").filter(property_id=property_id, key__contains=aspect).delete()

    def get_next_aspect(self, property_id):
        aspects = self.get_property_aspects(property_id)

        directions = ['north', 'south', 'east', 'west']

        has_items = []
        for in_aspect in aspects:
            has_items.append(in_aspect.key)

        doesnt_have = list(set(directions) - set(has_items))

        if doesnt_have:
            return doesnt_have[0]

    def get_next_aspect_floor(self, floor_id):
        aspects = self.get_property_aspects_floor(floor_id)

        directions = ['north', 'south', 'east', 'west']

        has_items = []
        for in_aspect in aspects:
            has_items.append(in_aspect.key)

        doesnt_have = list(set(directions) - set(has_items))

        if doesnt_have:
            return doesnt_have[0]

    def add_floor_aspect(self, floor_id, resource_id):

        self.model_object("FloorsPlans").create(floor_id=floor_id,
                                                resource_id=resource_id,
                                                key='floor_view')

        aspect = self.model_object("FloorsPlans").filter(floor_id=floor_id,
                                                         resource_id=resource_id,
                                                         key='floor_view')
        return aspect[0]

    def get_or_create_property_string_field(self, property_id, english_value):

        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')
        property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)
        string_field = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                           english__iexact=english_value)

        if not string_field:
            self.model_object("Strings").create(text_table_category=property_category_string, english=english_value)
            string_field = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                               english__iexact=english_value)

        string_field = string_field[0]
        property_strings = self.model_object("PropertiesStringFields").filter(property_id=property_id,
                                                                              type_string=string_field)

        if not property_strings:
            self.model_object("PropertiesStringFields").create(property_id=property_id, type_string=string_field)
            property_strings = self.model_object("PropertiesStringFields").filter(property_id=property_id,
                                                                                  type_string=string_field)

        if property_strings:
            for string in property_strings:
                string.text_table_category = property_category_string
                string.save(using=self.project.project_connection_name)

        return property_strings

    def get_all_properties_with_string_field(self, english_value):

        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')
        property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)
        string_field = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                           english__iexact=english_value)

        if not string_field:
            self.model_object("Strings").create(text_table_category=property_category_string, english=english_value)
            string_field = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                               english__iexact=english_value)

        string_field = string_field[0]
        property_strings = self.model_object("PropertiesStringFields").filter(type_string=string_field).values_list('property_id', 'value')

        return property_strings

    def delete_property_string_field(self, property_id, english_value):

        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')
        property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)
        string_field = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                           english__iexact=english_value)

        if not string_field:
            return

        string_field = string_field[0]
        self.model_object("PropertiesStringFields").filter(property_id=property_id, type_string=string_field).delete()

    def find_property_string(self, property_id, english_value):
        string_field = self.model_object("Strings").filter(english__icontains=english_value)

        if string_field:
            string_field = string_field[0]

            property_strings = self.model_object("PropertiesStringFields").filter(property_id=property_id,
                                                                                  type_string=string_field).exclude(
                value='')
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

            property_strings = self.model_object("PropertiesIntFields").filter(property_id=property_id,
                                                                               type_string=string_field)
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

            property_strings = self.model_object("PropertiesFloatFields").filter(property_id=property_id,
                                                                                 type_string=string_field)
            if property_strings:
                property_strings = property_strings[0]
                return property_strings.value

    def get_property_image_all(self):

        assets_tags = self.model_object("AssetTags").filter(tag__icontains="type")
        assets_tags = assets_tags.exclude(tag__in=self.ignore_list()).values_list('pk', flat=True)

        asset_with_tag = self.model_object("AssetsTagged").filter(asset_tag=assets_tags).values_list('asset_id',
                                                                                                     flat=True)
        assets = self.model_object("Assets").filter(pk__in=asset_with_tag).values_list('source_id', flat=True)
        images = self.model_object("Resources").filter(pk__in=assets)

        image_list = []
        image_names = []

        for image in images:
            image_name = str(image.path.split("/")[-1]).lstrip().rstrip()

            this_image_assets = self.model_object("Assets").filter(source_id=image.pk).values_list('pk', flat=True)
            this_image_asset_with_tag = self.model_object("AssetsTagged").filter(
                asset_id=this_image_assets).values_list('asset_tag_id', flat=True)

            this_image_assets_tags = self.model_object("AssetTags").filter(pk__in=this_image_asset_with_tag).exclude(
                tag__in=self.ignore_list()).values_list('tag', flat=True)

            if image_name not in image_names:
                image_names.append(image_name)
                image_list.append({'name': image_name,
                                   'path': image.path,
                                   'resource_id': image.pk,
                                   'tags': this_image_assets_tags})
        return image_list

    def check_if_image_name_in_property_list(self, new_image_name, new_image_id):
        property_images = self.model_object("PropertyResources").all().exclude(key="key_plans")

        image_names = []

        for proprty_image in property_images:
            image = self.model_object("Resources").filter(pk=proprty_image.resource_id)
            image_name = str(image[0].path.split("/")[-1]).lstrip().rstrip()

            if image_name not in image_names:
                image_names.append(image_name)

        if new_image_name not in image_names:
            return True

    def get_apartment_image_name(self, apartment_id, image_type):
        image = self.get_apartment_image(apartment_id, image_type)
        if image:
            image_path = image.path
            image_path_split = image_path.split("/")
            image_name = image_path_split[-1]
            return image_name

    def add_new_string_value(self, field):
        english_value = self.klass("Strings").clean_for_mapping(field.column_pretty_name)

        if english_value == "floor plan typical":
            english_value = "floor_plan_typical"

        table_category_mapping = self.get_mapping_table_value(field.column_table_mapping)

        category_queryset = self.model_object("TextTableCategories").filter(category=table_category_mapping)

        if category_queryset:
            category = category_queryset[0]

            queryset = self.model_object('Strings').filter(text_table_category=category,
                                                           english=english_value)

            if queryset:
                string_field = queryset[0]
                properties = self.model_object("Properties").all()

                for apartment in properties:
                    check_field = self.model_object("PropertiesStringFields").filter(property=apartment,
                                                                                     type_string=string_field)

                    if not check_field:
                        self.model_object("PropertiesStringFields").create(property=apartment,
                                                                           type_string=string_field,
                                                                           value=' ')

                return string_field
            else:
                self.model_object('Strings').create(text_table_category=category,
                                                    english=english_value)

                item_created = self.model_object('Strings').filter(text_table_category=category, english=english_value)
                item_created = item_created[0]
                return item_created

    def add_new_plain_string_value(self, string_value):
        english_value = self.klass("Strings").clean_for_mapping(string_value)

        category_queryset = self.model_object("TextTableCategories").filter(category='apartment_strings')

        if category_queryset:
            category = category_queryset[0]

            queryset = self.model_object('Strings').filter(text_table_category=category,
                                                           english=english_value)

            if queryset:
                string_field = queryset[0]
                properties = self.model_object("Properties").all()

                for apartment in properties:
                    check_field = self.model_object("PropertiesStringFields").filter(property=apartment,
                                                                                     type_string=string_field)

                    if not check_field:
                        self.model_object("PropertiesStringFields").create(property=apartment,
                                                                           type_string=string_field,
                                                                           value=' ')

                return string_field
            else:
                self.model_object('Strings').create(text_table_category=category,
                                                    english=english_value)

                item_created = self.model_object('Strings').filter(text_table_category=category, english=english_value)
                item_created = item_created[0]
                return item_created

    def get_apartment_image_path(self, apartment_id, image_type):
        image = self.get_apartment_image(apartment_id, image_type)
        if image:
            image_path = image.path
            return image_path

    def get_apartment_image(self, apartment_id, image_type_string, all=False):

        text_category = self.model_object("TextTableCategories").filter(category='Image Types')
        if not text_category:
            self.model_object("TextTableCategories").create(category='Image Types')
            text_category = self.model_object("TextTableCategories").filter(category='Image Types')

        description = self.model_object("Descriptions").filter(text_table_category=text_category,
                                                               english=image_type_string)
        
        images = self.model_object("Resources").filter(description=description).values_list("pk", flat=True)

        property_images = self.model_object("PropertyResources").filter(resource_id__in=images, 
                                                                        property_id=apartment_id)

        if images:
            if not all and property_images:
                return property_images[0]
            else:
                return property_images

        return None

    def get_property_static_image(self, apartment_id, image_type_string):
        image_type_string = image_type_string.replace(" ", "_").lower()
        property_images = self.model_object("PropertyResources").filter(property_id=apartment_id,
                                                                        key=image_type_string).values_list(
            "resource_id", flat=True)

        images = self.model_object("Resources").filter(pk=property_images)
        if images:
            image = images[0]
            static_image = '/static/' + str(image.path.replace(settings.BASE_IMAGE_LOCATION, ''))
            static = static_image.replace("//", "/")
            return static

        return None

    def get_property_image(self, apartment_id, image_type_string):
        image_type_string = image_type_string.replace(" ", "_").lower()
        property_images = self.model_object("PropertyResources").filter(property_id=apartment_id,
                                                                        key=image_type_string).values_list(
            "resource_id", flat=True)

        images = self.model_object("Resources").filter(pk=property_images)
        return images

    def get_property_aspects(self, property_id):
        aspects = ''
        images = self.model_object("PropertyAspects").filter(property_id=property_id)
        if images:
            aspects = images

        return aspects

    def save_apartment_strings_type_value(self, model_name, english_value, apartment_id, new_value, field_value=None):

        if not new_value:
            new_value = 0

        if model_name == self.project.apartment_model:
            apartment = self.model_object("Properties").get(pk=apartment_id)
            instance = self.klass("Names").get_names_instance(apartment.name_id)

            if field_value:
                model_fields_objects = self.model_object("Properties")._meta.get_fields()
                for field in model_fields_objects:
                    if field.name == english_value and type(field) == ForeignKey and new_value:
                        if field.rel.to == self.klass("Names"):
                            name_string = self.klass("Names").objects.using(self.project.project_connection_name).get(
                                id=int(field_value))
                            if name_string and not str(name_string.english) == str(new_value):
                                name_string.english = new_value
                                name_string.save(using=self.project.project_connection_name)
                            return True

            try:
                setattr(instance, english_value, new_value)
                instance.save(using=self.project.project_connection_name)
            except:
                id_value = '%s_id' % english_value
                setattr(instance, id_value, new_value)
                instance.save(using=self.project.project_connection_name)

            return True

        if model_name == self.project.hotspot_model:
            instance = self.klass("PropertyHotspots").filter(property_id=apartment_id)

            if instance and type(instance) == list and len(instance) > 1:
                instance = instance[0]
                
            try:
                setattr(instance, english_value, new_value)
                instance.save(using=self.project.project_connection_name)
            except:
                id_value = "%s_id" % english_value
                setattr(instance, id_value, new_value)
                instance.save(using=self.project.project_connection_name)

            return True
        
        table_category_mapping = self.get_mapping_table_value(model_name)
        category_queryset = self.model_object('Texttablecategories').filter(category=table_category_mapping)
        
        if category_queryset:
            category = category_queryset[0]
            strings_queryset = self.model_object("Strings").filter(text_table_category=category, english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]
                project_model_object = apps.get_model(self.project.project_connection_name, model_name)

                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = project_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)

                if not queryset:
                    project_model_object.objects.using(self.project.project_connection_name).create(**kwargs)
                    queryset = project_model_object.objects.using(self.project.project_connection_name).filter(**kwargs)

                if queryset:
                    instance = queryset[0]
                    setattr(instance, "value", new_value)
                    instance.save(using=self.project.project_connection_name)
                    return True

    def get_apartment_strings_type_value(self, model_name, english_value, apartment_id):

        if english_value.strip() in ["floor_plan_typical", "floor plan typical"]:

            if english_value.strip() == "floor plan typical":
                english_value = "floor_plan_typical"

            category_queryset = self.model_object("Texttablecategories").filter(category=settings.FLOOR_PLAN_TYPICAL)
            category = category_queryset[0]
            strings_queryset = self.model_object('Strings').filter(text_table_category=category,
                                                                   english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]
                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = self.model_object(model_name).filter(**kwargs)
                if not queryset:
                    self.model_object(model_name).create(**kwargs)
                    queryset = self.model_object(model_name).filter(**kwargs)

                if queryset:
                    return queryset[0]

        if model_name == self.project.apartment_model:
            return self.model_object("Properties").get(pk=apartment_id)

        if model_name == self.project.hotspot_model:
            hotspots = self.model_object("PropertyHotspots").filter(property_id=apartment_id)

            if not hotspots:
                self.model_object("PropertyHotspots").create(property_id=apartment_id)
                hotspots = self.model_object("PropertyHotspots").filter(property_id=apartment_id)

            if hotspots:
                return hotspots[0]

        table_category_mapping = self.get_mapping_table_value(model_name)
        category_queryset = self.model_object("TextTableCategories").filter(category=table_category_mapping)

        if category_queryset:
            category = category_queryset[0]
            strings_queryset = self.model_object('Strings').filter(text_table_category=category,
                                                                   english=english_value)

            if strings_queryset:
                type_string = strings_queryset[0]

                kwargs = {
                    '{0}'.format(self.project.apartment_model_id_field): apartment_id,
                    '{0}'.format('type_string'): type_string,
                }

                queryset = self.model_object(model_name).filter(**kwargs)

                if not queryset:
                    self.model_object(model_name).create(**kwargs)
                    queryset = self.model_object(model_name).filter(**kwargs)

                if queryset:
                    return queryset[0]

    def get_all_properties_with_string_status_value(self, status=None):
        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')

        property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)

        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                            english__iexact='status')

        if string_fields:
            properties = []
            for string_field in string_fields:
                property_string_status = self.model_object("PropertiesStringFields").filter(type_string=string_field)
                if status and property_string_status:
                    append = [properties.append(x) for x in property_string_status.filter(value__icontains=status)]

            return properties

    def get_all_properties_with_string_value(self, string_value=None, value=None):
        try:
            mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')

            property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)

            string_fields = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                                english__iexact=string_value)

            if string_fields:
                properties = []
                for string_field in string_fields:
                    property_string_status = self.model_object("PropertiesStringFields").filter(type_string=string_field)

                    if value and property_string_status:
                        append = [properties.append(x.property_id) for x in property_string_status.filter(value=value)]
                    elif not value and property_string_status:
                        append = [properties.append(x.property_id) for x in property_string_status]

                return properties
        except Exception as e:
            print('error', e)

    def get_count_properties_with_string_status_value(self, status=None):
        mapping_table_value = self.get_mapping_table_value('PropertiesStringFields')

        property_category_string = self.klass("TextTableCategories").get_text_table_category(mapping_table_value)

        string_fields = self.model_object("Strings").filter(text_table_category=property_category_string,
                                                            english__iexact='status')

        if string_fields:
            count_status = 0
            for string_field in string_fields:
                property_string_status = self.model_object("PropertiesStringFields").filter(type_string=string_field)
                if status and property_string_status:
                    count_status += property_string_status.filter(value__icontains=status).count()

            return count_status

    def get_all_distinct_editable_values(self, type_string_value, table_name, floor_list=None):
        table_values_list = []
        type_strings = self.model_object("Strings").filter(english=type_string_value)
        if type_strings:
            for type_string in type_strings:
                if floor_list:
                    table_values = self.model_object(table_name).filter(type_string=type_string,
                                                                        property__floor_id__in=floor_list).distinct().values_list('value', flat=True)
                else:
                    table_values = self.model_object(table_name).filter(type_string=type_string).distinct().values_list('value', flat=True)

                for value in table_values:
                    if value and not value in table_values_list:
                        table_values_list.append(value)

            return table_values_list

    def get_property_distinct_editable_value_for_key(self, property_id, type_string_value, table_name):
        table_values_list = []
        type_strings = self.model_object("Strings").filter(english__icontains=type_string_value)
        if type_strings:
            for type_string in type_strings:
                table_values = self.model_object(table_name).filter(type_string=type_string,
                                                                    property_id=property_id).distinct().values_list(
                    'value', flat=True)

                for value in table_values:
                    if value and not value in table_values_list:
                        table_values_list.append(value)

            return table_values_list

    def get_all_properties_with_render_key_and_value(self, render_key, cleaned_render_key, selected_value, floor_list):
        # print('floor_list', floor_list)
        properties_in_floor_list = self.model_object("Properties").filter(floor_id__in=floor_list).values_list("pk", flat=True)
        # print('properties_in_floor_list', len(properties_in_floor_list), properties_in_floor_list)
        properties_with_individual = self.model_object("PropertyRenders").filter(key="Individual").values_list("property_id", flat=True)

        table_name = self.klass("PropertyRenders").get_property_render_table(render_key)
        final_property_list = []
        type_strings = self.model_object("Strings").filter(english=cleaned_render_key)
        if type_strings:
            for type_string in type_strings:
                try:
                    property_list = self.model_object(table_name).filter(type_string=type_string,
                                                                         value=selected_value,
                                                                         property_id__in=properties_in_floor_list)

                    if properties_with_individual:
                        property_list = property_list.exclude(property_id__in=properties_with_individual)

                    if property_list:
                        final_property_list += property_list.values_list("property_id", flat=True).distinct()

                except Exception as e:
                    print('property_list error', e)

        return final_property_list

    def get_total_renders_count_for_key(self, type_string_value):
        resources = self.model_object("PropertyRenders").filter(key=type_string_value).count()
        return resources

    def set_property_render_field(self, property_id, render_field_text):


        pass
