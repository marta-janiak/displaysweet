from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Devices(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Devices, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    @property
    def ModelFields(self):
        fields = []
        try:
            device_model = self.model_object('Devices')
            model_fields = device_model._meta.get_fields()
            for field in model_fields:
                if not type(field) == ManyToOneRel and not field.name == 'id':
                    fields.append(field.name)
            return fields
        except:
            return []

    #more functionality to come