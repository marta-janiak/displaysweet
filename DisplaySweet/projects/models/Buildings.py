from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Buildings(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Buildings, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_all_buildings(self, return_first=False):
        floor_buildings = self.model_object("FloorsBuildings").all().values_list('building_id', flat=True).distinct()
        queryset = self.model_object("Buildings").filter(pk__in=floor_buildings).order_by('name__english')

        if queryset and return_first:
            first_floor = queryset[0]
            return first_floor

        return queryset