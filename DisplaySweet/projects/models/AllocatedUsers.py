from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class AllocatedUsers(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(AllocatedUsers, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_user_allocated_properties(self, user_id):
        property_ids = None
        user_allocated_groups = self.objects.filter(user_id=user_id).values_list('allocation_group_id', flat=True)
        if user_allocated_groups:
            property_ids = self.model_object("PropertyAllocations").filter(
                allocation_group_id__in=user_allocated_groups).values_list('property_id', flat=True)

        return property_ids