from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class AssetTags(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(AssetTags, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def create_asset_tag(self, tag_name):
        tags = self.objects.filter(tag=tag_name)

        if not tags:
            self.model_object("AssetTags").create(tag=tag_name)

    def remove_tag(self, tag_id):
        self.objects.using(self.project.project_connection_name).filter(pk=tag_id).delete()

    def get_image_tag_list(self, resource_id):
        this_image_assets = self.model_object("Assets").filter(source_id=resource_id).values_list('pk',
                                                                                                  flat=True)
        this_image_asset_with_tag = self.model_object("AssetsTagged").filter(
            asset_id=this_image_assets).values_list('asset_tag_id', flat=True)
        
        this_image_assets_tags = self.model_object("AssetTags").filter(
            pk__in=this_image_asset_with_tag).exclude(tag__in=self.ignore_list()).values_list('tag', flat=True)
        return this_image_assets_tags

    def get_all_assets_with_tags(self):
        asset_tags = self.model_object("AssetsTagged").all().values_list('asset_id', flat=True).distinct()
        assets = self.model_object("Assets").filter(pk__in=asset_tags)
        image_list = []
        asset_list_ids = []

        for asset in assets:
            image = self.model_object("Resources").filter(pk=asset.source_id)

            if image:
                image = image[0]
                if image.pk not in asset_list_ids:
                    # tags = self.klass("Assets").get_image_asset_tags(image)
                    image_name = image.path.split("/")[-1]
                    extension = image_name[-3:]

                    image_static_path = '/static/%s/thumbnails/%s' % (
                        self.project.project_connection_name, image_name)
                    static_file_path = '/static/%s/thumbnails/%s' % (
                        self.project.project_connection_name, image_name)
                    file_type = 'image'

                    if extension in self.sound_files():
                        image_static_path = '/static/common/img/musical-notes.png'
                        static_file_path = '/static/%s/media/%s' % (
                            self.project.project_connection_name, image_name)
                        file_type = 'sound'
                    if extension in self.video_files():
                        image_static_path = '/static/common/img/movie.png'
                        static_file_path = '/static/%s/media/%s' % (
                            self.project.project_connection_name, image_name)
                        file_type = 'video'

                    if extension in self.pdf_files():
                        image_static_path = '/static/common/img/pdf.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                            self.project.project_connection_name, image_name)
                        file_type = 'pdf'

                    if extension in self.document_files():
                        image_static_path = '/static/common/img/document.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                            self.project.project_connection_name, image_name)
                        file_type = 'document'

                    full_image_name = image_name
                    if len(image_name) > 20:
                        try:
                            image_name = str(image_name)[:20] + '...'
                        except:
                            pass

                    image_list.append({'image': image,
                                       'asset': asset,
                                       'image_name': image_name,
                                       'full_image_name': full_image_name,
                                       'tags': self.get_image_tag_list(image.pk),
                                       'image_static_path': image_static_path,
                                       'static_file_path': static_file_path,
                                       'file_type': file_type})

        return image_list

    def get_assets_without_tags(self):
        asset_tags = self.model_object("AssetsTagged").all().values_list('asset_id', flat=True).distinct()
        assets = self.model_object("Assets").all().exclude(pk__in=asset_tags)
        image_list = []
        asset_list_ids = []

        for asset in assets:
            image = self.model_object("Resources").filter(pk=asset.source_id)

            if image:
                image = image[0]
                if image.pk not in asset_list_ids:
                    # tags = self.klass("Assets").get_image_asset_tags(image)
                    image_name = image.path.split("/")[-1]
                    extension = image_name[-3:]

                    image_static_path = '/static/%s/thumbnails/%s' % (
                    self.project.project_connection_name, image_name)
                    static_file_path = '/static/%s/thumbnails/%s' % (
                    self.project.project_connection_name, image_name)
                    file_type = 'image'

                    if extension in self.sound_files():
                        image_static_path = '/static/common/img/musical-notes.png'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'sound'
                    if extension in self.video_files():
                        image_static_path = '/static/common/img/movie.png'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'video'

                    if extension in self.pdf_files():
                        image_static_path = '/static/common/img/pdf.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'pdf'

                    if extension in self.document_files():
                        image_static_path = '/static/common/img/document.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'document'

                    full_image_name = image_name
                    if len(image_name) > 20:
                        try:
                            image_name = str(image_name)[:20] + '...'
                        except:
                            pass

                    image_list.append({'image': image,
                                       'asset': asset,
                                       'image_name': image_name,
                                       'full_image_name': full_image_name,
                                       'tags': [],
                                       'image_static_path': image_static_path,
                                       'static_file_path': static_file_path,
                                       'file_type': file_type})
        return image_list

    def get_assets_with_tag(self, tag_name):
        
        asset_tags = self.model_object("AssetsTagged").filter(asset_tag__tag=tag_name).values_list('asset_id', flat=True).distinct()
        assets = self.model_object("Assets").filter(pk__in=asset_tags)
        image_list = []
        asset_list_ids = []

        for asset in assets:
            image = self.model_object("Resources").filter(pk=asset.source_id)
            if image:
                image = image[0]
                if image.pk not in asset_list_ids:
                    # tags = self.klass("Assets").get_image_asset_tags(image)
                    image_name = image.path.split("/")[-1]
                    extension = image_name[-3:]

                    image_static_path = '/static/%s/thumbnails/%s' % (
                    self.project.project_connection_name, image_name)
                    static_file_path = '/static/%s/thumbnails/%s' % (
                    self.project.project_connection_name, image_name)
                    file_type = 'image'

                    if extension in self.sound_files():
                        image_static_path = '/static/common/img/musical-notes.png'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'sound'
                    if extension in self.video_files():
                        image_static_path = '/static/common/img/movie.png'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'video'

                    if extension in self.pdf_files():
                        image_static_path = '/static/common/img/pdf.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'pdf'

                    if extension in self.document_files():
                        image_static_path = '/static/common/img/document.jpg'
                        static_file_path = '/static/%s/media/%s' % (
                        self.project.project_connection_name, image_name)
                        file_type = 'document'

                    full_image_name = image_name
                    if len(image_name) > 20:
                        try:
                            image_name = str(image_name)[:20] + '...'
                        except:
                            pass

                    image_list.append({'image': image,
                                       'asset': asset,
                                       'image_name': image_name,
                                       'full_image_name': full_image_name,
                                       'tags': [],
                                       'image_static_path': image_static_path,
                                       'static_file_path': static_file_path,
                                       'file_type': file_type})
                    
        return image_list