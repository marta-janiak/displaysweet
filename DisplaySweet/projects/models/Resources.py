from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
import imghdr
from .model_mixin import PrimaryModelMixin


class Resources(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Resources, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def save_resource_to_asset(self, resource_id):
        text_category, created = self.model_object("TextTableCategories").get_or_create(category="Asset")
        type_string, created = self.model_object("Strings").get_or_create(english='Images', text_table_category_id=text_category.pk)

        asset_type = self.model_object("AssetTypes").filter(type_string=type_string)

        if not asset_type:
            self.model_object("AssetTypes").create(type_string=type_string)
            asset_type = self.model_object("AssetTypes").filter(type_string=type_string)

        asset_type = asset_type[0]
        new_asset = self.model_object("Assets").filter(asset_type=asset_type, source_id=resource_id)
        if not new_asset:
            self.model_object("Assets").create(asset_type=asset_type, source_id=resource_id)
            new_asset = self.model_object("Assets").filter(asset_type=asset_type, source_id=resource_id)

        return new_asset[0]

    def get_project_image_directory(self, image_type='level'):
        if self.project:
            images = []
            image_types = ['gif', 'jpeg', 'jpg', 'png']

            if image_type == 'level' and self.project.level_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.level_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(
                                os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'level_full_res' and self.project.level_plan_location_full_res:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.level_plan_location_full_res)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(
                                os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'floor' and self.project.floor_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.floor_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(
                                os.path.join(path, item)) in image_types:
                            images.append(item)

            if image_type == 'key' and self.project.key_plan_location:
                path = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.key_plan_location)
                path = path.replace("/project_data/project_data/", "/project_data/")
                if os.path.isdir(path):
                    for item in os.listdir(path):
                        if os.path.isfile(os.path.join(path, item)) and imghdr.what(
                                os.path.join(path, item)) in image_types:
                            images.append(item)

            return images

    def get_all_plan_images(self, floor_id):
        floor = self.klass("Floors").get_selected_floor(floor_id)
        images = self.get_class_object("Resources")

        image = self.get_model_queryset('id', floor.floor_plate_image_id, list_needed=False)
        selected_path_name = str(image.highres_path).replace(image.highres_path.split("/")[-1], '')

        queryset = images.objects.using(self.project.project_connection_name).all()
        kwargs = {
            '{0}'.format('highres_path__icontains'): selected_path_name,
        }

        queryset = queryset.filter(**kwargs)

        return queryset