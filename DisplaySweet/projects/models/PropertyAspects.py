from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class PropertyAspects(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(PropertyAspects, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_keys(self):
        images = self.model_object("PropertyAspects").all().values_list('key', flat=True).order_by('key').distinct()
        aspects = ','.join(images)

        return aspects

    def get_all_aspects(self):
        images = self.class_object("PropertyAspects").all().values_list('key', flat=True).order_by('key').distinct()
        aspects = ','.join(images)

        return aspects

    def create_new_aspect_key(self, property_id, aspect_text, file_order):
        aspect = self.model_object("PropertyAspects").filter(property_id=property_id,
                                                             key=aspect_text)

        if not aspect:
            self.model_object("PropertyAspects").create(property_id=property_id,
                                                        key=aspect_text,
                                                        resource_id=0,
                                                        orderidx=file_order)