from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Floors(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Floors, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_all_floors(self, return_first=False, building_id=None):

        floor_buildings = []
        if building_id:
            floor_buildings = self.model_object("FloorsBuildings").filter(building_id=building_id).values_list('floor_id',
                                                                                                               flat=True).order_by('floor__orderidx')

        queryset = self.model_object("Floors").all().order_by('orderidx')

        if building_id:
            queryset = queryset.filter(pk__in=floor_buildings)

        result_list = list(queryset)

        if result_list and return_first:
            first_floor = result_list[0]
            return first_floor

        return result_list

    def get_select_floors_html(self, building_id, floor_id=None, request=None):

        floors = self.get_all_floors(building_id=building_id)
        floor_select_list = []

        for row_floor in floors:
            if not floor_id:
                floor_id = row_floor.pk

            floor_buildings = self.model_object("FloorsBuildings").filter(building_id=building_id,
                                                                          floor=row_floor).values_list('floor_id',
                                                                          flat=True).distinct().order_by('floor__orderidx')
            for row_floor_id in floor_buildings:
                floor = self.model_object("Floors").filter(pk=row_floor_id)[0]

                apartment_count = len(self.model_object("Properties").filter(floor_id=floor.pk))
                floor_select_list.append({'floor': floor,
                                          'orderidx': floor.orderidx,
                                          'building_id': building_id,
                                          'apartment_count': apartment_count,
                                          'level_plan_image': self.get_floor_level_image(floor.pk),})

        return {'floor_select_list': floor_select_list, 'project': self.project, 'floor_id': floor_id,
                'building_id': building_id, 'buildings': self.klass("Buildings").get_all_buildings(),}

    def get_floor_level_image(self, floor_id):
        plans = self.model_object("FloorsPlans").filter(floor_id=floor_id, key="level_plans").values_list(
            "resource_id", flat=True)
        images = self.model_object("Resources").filter(pk=plans)

        if images:
            plan = images[0]
            image_path = plan.path
            resource_id = plan.pk
            image_exists = True
            image_name = plan.path.split("/")[-1]

            if image_path:
                if not os.path.isfile(image_path):
                    static_image = '/static/common/img/placeholder.jpg'
                    image_path = '<em>This floor has no image selected (placeholder displayed).</em>'
                    image_name = ''
                else:
                    image_name = image_path.split("/")[-1]
                    static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
                    static_image = static_image.replace("//", "/")
                    image_path = str(image_path)

            return image_name

    def get_floor_image(self, project, image_type_description, floor_id, image_type_id, image_path):
        kwargs = {
            '{0}'.format('floor_id'): floor_id,
            '{0}'.format('image_type_id'): image_type_id,
        }

        try:
            queryset = self.model_object("Floorsplans").filter(**kwargs)
            if queryset:
                return queryset[0]

            else:

                kwargs = {
                    '{0}'.format('path'): image_path,
                    '{0}'.format('description'): image_type_description,
                    '{0}'.format('size'): 0
                }

                queryset, created = self.model_object("Resources").get_or_create(**kwargs)
                return queryset
        except:
            pass

    def get_floor_plan_details_path(self, pk, floor_id=None, tag_selected=None):
        floor_name = ''
        resource_id = ''

        project = self.project

        image_type_choices = []
        output_choices = []
        mode_choices = []

        saved_image_type = self.model_object("AssetTags").all()

        for si_type in saved_image_type:
            if si_type.tag and not si_type.tag in image_type_choices:
                image_type_choices.append(si_type.tag)

        image_exists = False
        all_floor_images = []
        buildings = 0

        first_floor = None
        if floor_id:
            first_floor = self.model_object('Floors').get(pk=floor_id)
        else:
            floors = self.model_object('Floors').all()
            if len(floors) > 1:
                first_floor = floors[0]
                floor_id = first_floor.id

        if project.has_building_model():
            buildings = len(self.model_object("Buildings").all())

        apartments_dict = []
        first_image_path = None

        if first_floor:

            floor = self.model_object("Floors").get(pk=first_floor.pk)
            floor_name = self.klass("Names").get_names_instance(floor.name_id)
            apartments = self.model_object("Properties").filter(pk=first_floor.pk).order_by('orderidx')

            for apartment in apartments:
                spot_list = self.klass("Properties").get_apartment_hotspot_values(apartment.pk)

                try:
                    picking_coord_x = spot_list[0]['picking_coord_x']
                    picking_coord_y = spot_list[0]['picking_coord_y']
                except:
                    picking_coord_x = 0
                    picking_coord_y = 0

                apartment_images = []
                for image_type in image_type_choices:
                    if self.klass("Properties").get_property_image(apartment.id, image_type):
                        apartment_images.append({image_type: self.klass("Properties").get_property_image(apartment.id, image_type)[0].path})

                apartment_kwargs = {'id': apartment.id,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'name': self.model_object("Names").get_names_instance(apartment.name_id),
                                    'images': apartment_images,
                                    'property_type': self.klass("Properties").get_property_type(apartment.property_type_id).pk,
                                    'property_type_name': self.klass("Properties").get_property_type(apartment.property_type_id).type
                                    }

                apartments_dict.append(apartment_kwargs)

            first_apartment_id = None
            y_coordinate = None
            x_coordinate = None
            resource_id = None
            hotspot_order = None

            if apartments and image_type_choices:
                first_apartment_id = apartments[0].pk
                spot_list = self.klass("Properties").get_apartment_hotspot_values(first_apartment_id)

                try:
                    x_coordinate = spot_list[0]['picking_coord_x']
                    y_coordinate = spot_list[0]['picking_coord_y']
                    hotspot_order = spot_list[0]['hotspot_order']
                except:
                    x_coordinate = 0
                    y_coordinate = 0
                    hotspot_order = 1

                first_image_path = self.klass("Properties").get_apartment_image_path(apartments[0].pk, image_type_choices[0])

            image_exists = None

            if first_image_path and os.path.isfile(os.path.join(settings.BASE_IMAGE_LOCATION, first_image_path)):
                image_exists = True

                image = self.klass("Properties").get_apartment_image(apartments[0].pk, image_type_choices[0])
                if image:
                    resource_id = image[0].pk

            all_floor_images = []  # self.get_all_plan_images(floor_id)
        else:

            apartments = []
            first_apartment_id = None
            x_coordinate = 0
            y_coordinate = 0
            hotspot_order = 1

        all_floors = self.get_all_floors()
        floor_list = []

        apartment_list = []
        property_typical_image_list = self.klass("Properties").get_all_property_typicals()

        i = 1
        for floor in all_floors:

            try:
                apartment_count = len(self.model_object("Properties").filter(floor_id=floor.pk))
            except:
                apartment_count = 0

            properties = self.model_object("Properties").filter(floor_id=floor.pk)
            property_images = []
            apartments_dict = []

            for property in properties:
                if self.klass("Properties").get_property_image(property.pk, "level_plans"):
                    property_images.append(self.klass("Properties").get_property_image(property.pk, "level_plans"))

                spot_list = self.klass("Properties").get_apartment_hotspot_values(property.pk)

                try:
                    picking_coord_x = spot_list[0]['picking_coord_x']
                    picking_coord_y = spot_list[0]['picking_coord_y']
                except:
                    picking_coord_x = 0
                    picking_coord_y = 0

                apartment_images = {}
                for image_type in image_type_choices:
                    if self.klass("Properties").get_property_image(property.id, image_type):

                        current_image = self.klass("Properties").get_property_image(property.id, image_type)[0]
                        current_image_name = ''
                        path = ''
                        image_tags = ''

                        if current_image:
                            path = current_image.path
                            resource_id = current_image.pk
                            current_image_name = path.split("/")[-1]

                            if self.klass("Properties").get_image_tag_list(resource_id):
                                image_tags = self.klass("Properties").get_image_tag_list(resource_id)

                        apartment_images.update({image_type.replace(" ", "_").lower():
                                                     {'path': path,
                                                      'name': current_image_name,
                                                      'tag': image_tags,
                                                      'resource_id': current_image.pk}
                                                 })

                if i == 1:
                    x_coordinate = picking_coord_x
                    y_coordinate = picking_coord_y

                apartment_kwargs = {'id': property.id,
                                    'x_coordinate': picking_coord_x,
                                    'y_coordinate': picking_coord_y,
                                    'hotspot_order': hotspot_order,
                                    'name': self.klass("Names").get_names_instance(property.name_id),
                                    'images': apartment_images,
                                    'property_type': self.klass("Properties").get_property_type(property.property_type_id).pk,
                                    'property_type_name': self.klass("Properties").get_property_type(property.property_type_id).type
                                    }

                apartments_dict.append(apartment_kwargs)
                apartment_list.append(apartment_kwargs)

                i = i + 1

            floors_buildings = self.model_object("FloorsBuildings").filter(floor_id=floor.pk).values_list('building_id',
                                                                                                          flat=True)
            floors_buildings = self.model_object("Buildings").filter(pk__in=floors_buildings)

            floor_list.append(
                {'floor': floor,
                 'apartment_count': apartment_count,
                 'name': self.klass("Names").get_names_instance(floor.name_id),
                 'level_plan_image': self.get_floor_level_image(floor.pk),
                 'apartments': apartments_dict,
                 'assigned_buildings': floors_buildings,
                 }
            )


        first_image_type = None
        if image_type_choices:
            first_image_type = image_type_choices[0].replace(" ", "_").lower()
            if tag_selected:
                first_image_type = tag_selected

        apartment_list = []
        context = {
            'project': project,
            'all_floors': all_floors,
            'floor_list': floor_list,
            'property_type_list': self.model_object("PropertyTypes").all(),
            'model_instance': first_floor,
            'apartments': apartments,
            'total_apartments': len(apartments),
            'apartment_list': apartment_list,
            'apartments_dict': apartments_dict,
            'floor_id': floor_id,
            'image_path': first_image_path,
            'image_exists': image_exists,
            'resource_id': resource_id,
            'property_typical_image_list': property_typical_image_list,
            'canvas_width': '550px',
            'all_floor_images': all_floor_images,
            'first_apartment_id': first_apartment_id,
            'x_coordinate': x_coordinate,
            'y_coordinate': y_coordinate,
            'buildings': buildings,
            'image_type_choices': image_type_choices,
            'first_image_type': first_image_type,
            'floor_name': floor_name,
            'output_choices': output_choices,
            'mode_choices': mode_choices,
            'property_types': self.model_object("PropertyTypes").all(),
            'selected_property_type': 1,
            'key_plan_details': self.klass("Properties").get_image_type_canvas('key_plans', first_apartment_id),
            'floor_plan_details': self.klass("Properties").get_image_type_canvas('floor_plans', first_apartment_id),

        }

        # ##print 'context', context

        return context

    def get_select_preview_floor_dictionary(self, property, template_fields):
        key_list = []
        property_fields_values = {}
        data_list = []

        for field in template_fields:
            value = " "

            if field.column_field_mapping == "building_name":
                floor_buildings = self.model_object("FloorsBuildings").filter(
                    floor_id=property.floor_id)

                if floor_buildings:
                    floor_buildings = floor_buildings[0]
                    value = self.model_object("Buildings").filter(
                        pk=floor_buildings.building_id).values_list('name__english', flat=True)

                    if value:
                        value = value[0]
            else:
                try:
                    property_values = self.klass("Properties").get_apartment_strings_type_value(field.column_table_mapping,
                                                                                                field.column_field_mapping,
                                                                                                property.pk)

                    if property_values:
                        property_value_dict = model_to_dict(property_values)
                        if "type_string" in property_value_dict:
                            value = property_value_dict["value"]
                        else:
                            get_list_value = property_value_dict[field.column_field_mapping]
                            check_foreign_key, value = self.get_model_field_mapping_and_value(
                                field.column_table_mapping,
                                field.column_field_mapping,
                                field_value=get_list_value)

                        value = str(value).strip().lower().capitalize()

                        if not value or value in ["None", ""]:
                            # print 'setting to NA'
                            value = " "
                except:
                    # print 'error: setting to NA'
                    value = " "

            if field.column_field_mapping not in key_list and not str(
                    field.column_field_mapping).strip() == "New Field:":
                key_list.append(field.column_field_mapping)

            property_fields_values.update({field.column_field_mapping: value})
            data_list.append(value)

        return property_fields_values, data_list, key_list

    def remove_floor_resource_by_id(self, aspect_id):
        if aspect_id:
            self.model_object("FloorsPlans").filter(pk=aspect_id).delete()

    def get_selected_floor(self, floor_id):
        project_model_object = apps.get_model(self.project.project_connection_name, "Floors")
        queryset = project_model_object.objects.using(self.project.project_connection_name).filter(
            id=floor_id).order_by('id')

        if queryset and floor_id:
            first_floor = queryset[0]
            return first_floor

        return queryset
