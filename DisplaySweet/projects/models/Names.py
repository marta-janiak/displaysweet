from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Names(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Names, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_names_instance(self, name_id):
        display_name = self.model_object('Names').filter(pk=name_id).values_list('english', flat=True)
        if display_name:
            return display_name[0]

    def get_or_create_names_instance(self, value):

        names_text_category = self.model_object("TextTableCategories").filter(category="names")
        if names_text_category:
            names_text_category = names_text_category[0]

        display_name = self.model_object("Names").filter(english=value, text_table_category=names_text_category)

        if not display_name:
            self.model_object("Names").create(english=value, text_table_category=names_text_category)
            display_name = self.model_object("Names").filter(english=value, text_table_category=names_text_category)

        if display_name:
            display_name = display_name[0]
            return display_name

    def get_or_save_display_name(self, value):
        names_text_category = self.model_object("TextTableCategories").filter(category="names")
        if names_text_category:
            names_text_category = names_text_category[0]

        display_name = self.objects.filter(english=value,  text_table_category=names_text_category)
        if len(display_name) == 0:
            self.model_object("Names").create(english=value, text_table_category=names_text_category)
            display_name = self.model_object("Names").filter(english=value, text_table_category=names_text_category)

        display_name = display_name[0]
        return display_name