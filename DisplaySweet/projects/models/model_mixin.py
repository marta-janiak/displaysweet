from core.models import Project
from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
import importlib
import datetime
from django.conf import settings
from django.core.urlresolvers import reverse_lazy, reverse


class PrimaryModelMixin(object):
    """
    The primary model mixin for project classes.
    """
    project = None
    project_id = None
    user = None
    request = None
    class_name = None
    model = None
    model_fields = None
    project_model_object = None

    def __init__(self, project=None, request=None, project_id=None, user=None):
        super(PrimaryModelMixin, self).__init__()

        self.request = request
        self.project = project
        self.project_id = project_id
        self.user = user

        if project_id:
            self.process_project_details(project_id, user)
            self.set_project_id(project_id)

    def set_project(self, project):
        self.project = project
        self.project_id = project.pk
        print('setting project id in model mixin')
        self.request.session['project_id'] = project.pk
        return self.project

    def set_project_id(self, project_id):
        if project_id:
            self.project_id = project_id
            self.project = Project.objects.get(pk=project_id)

    def process_project_details(self, project_id=None, user=None):

        try:
            self.request.session['project_id'] = project_id
            self.project = Project.objects.get(pk=project_id)
            return self.project
        except:
            if project_id:
                project = Project.objects.get(pk=project_id)
                self.project = project
                self.project_id = project_id
                try:
                    self.request.session['project_id'] = project.pk
                except:
                    pass
            else:
                self.project = Project.objects.all().order_by('id')[0]

        if not self.project and user:
            projects = Project.objects.filter(users=user)
            if projects:
                project = projects[0]
                self.project = project
                self.project_id = project.pk

        return self.project

    def set_model(self, model_name):
        model = apps.get_model(self.project.project_connection_name, model_name)
        return model

    def get_objects(self, class_name):
        class_object = apps.get_model(self.project.project_connection_name, class_name)
        model_objects = class_object.objects.using(self.project.project_connection_name)
        return model_objects

    def model_object(self, model_name):

        try:
            # print('attempting connection in model_object', self.project.project_connection_name, model_name)
            project_model_object = apps.get_model(self.project.project_connection_name, model_name)
            self.project_model_object = project_model_object
            self.model_name = model_name
            return project_model_object.objects.using(self.project.project_connection_name)

        except Exception as e:
            print('--------- model_object error', e)

            # try:
            #     self.project.connection.map_installed_apps_to_memory()
            # except Exception as e:
            #     print(e)

            project_model_object = apps.get_model(self.project.project_connection_name, model_name)
            self.project_model_object = project_model_object
            self.model_name = model_name

            return project_model_object.objects.using(self.project.project_connection_name)

    def get_class_object(self, model_name):
        project_model_object = apps.get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        self.model_name = model_name
        return project_model_object

    def model_object_items(self, model_name=None):
        if not model_name:
            model_name = self.model_name

        self.set_model(model_name)

        class_object = self.get_class_object(model_name)
        model_objects = class_object.objects.using(self.project.project_connection_name)
        return model_objects

    def launch(self, project_id):
        self.set_project_id(project_id)

        def class_objects(class_name):
            m = __import__(class_name, globals=globals(), locals=locals(), fromlist=[], level=1)
            found_class = getattr(m, class_name)
            return found_class(self.project)
        return class_objects

    def klass(self, class_name):
        m = __import__(class_name, globals=globals(), locals=locals(), fromlist=[], level=1)
        found_class = getattr(m, class_name)
        return found_class(self.project)

    def class_object(self, class_name):
        cobject = self.launch(self.project_id)
        return cobject(class_name)

    def project_list(self):
        from core.models import Project
        projects = Project.objects.filter(users=self.user, active=True)

        if projects:
            project_list = []
            for project in projects:
                try:
                    device_model = self.model_object('Devices')
                    if device_model:
                        project_list.append(project)
                except:
                    pass

            return project_list

    def queryset_to_list_of_ids(self, queryset):
        new_list = []
        for item in queryset:
            new_list.append(item.pk)

        return new_list

    def queryset_to_list(self, queryset):
        new_list = []
        for item in queryset:
            new_list.append(item.pk)

        return new_list

    def image_files(self):
        return ['jpg', 'png', 'gif']

    def sound_files(self):
        return ['mp3', 'wav']

    def video_files(self):
        return ['mp4', 'mov', 'avi', 'mkv']

    def pdf_files(self):
        return ['pdf', ]

    def document_files(self):
        return ['txt', 'doc', 'docx', 'csv']

    def get_model_instance(self, row_id, field=None):
        #For dynamic model editing

        if field:
            instances = self.project_model_object.objects.using(self.project.project_connection_name).all()
            for instance in instances:
                attribute = getattr(instance, field)
                if str(attribute) == str(row_id):
                    self.model_instance = instance
                    return instance

        else:
            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(
                pk=row_id)
            self.model_instance = model_instance
            return model_instance

    def get_model_queryset(self, field=None, value=None, list_needed=False):

        queryset = self.project_model_object.objects.using(self.project.project_connection_name).all()

        if field:
            kwargs = {
                '{0}'.format(field): value,
            }
            queryset = queryset.filter(**kwargs)

        if len(queryset) == 1 and not list_needed:
            return queryset[0]

        return queryset

    def get_reversed_urls_from_actions(self, actions, pk):
        reversed_actions = []
        for action_name, url_name in actions:
            reversed_actions.append((action_name, reverse(url_name, args=[pk])))

        return reversed_actions

    def get_instance_from_id(self, row_id, field=None):

        if field:
            instances = self.project_model_object.objects.using(self.project.project_connection_name).all()
            for instance in instances:
                attribute = getattr(instance, field)
                if str(attribute) == str(row_id):
                    self.model_instance = instance
                    return instance

        else:
            model_instance = self.project_model_object.objects.using(self.project.project_connection_name).get(
                pk=row_id)
            self.model_instance = model_instance
            return model_instance

    def get_mapping_table_value(self, category):

        category = str(category).strip().lower()

        if category == 'propertiesstringfields':
            return 'apartment_strings'

        if category == 'properties':
            return None

        if category == 'propertiesbooleanfields':
            return 'apartment_bools'

        if category == 'propertiesfloatfields':
            return 'apartment_floats'

        if category == 'propertiesintfields':
            return 'apartment_ints'

        if category == "floor_plan_typical":
            return "floor_plan_typical"

        return 'names'

    def get_string_category(self, model_name):
        table_category_mapping = self.get_mapping_table_value(model_name)
        category_queryset = self.model_object('Texttablecategories').filter(category=table_category_mapping)

        if not category_queryset:
            self.model_object('Texttablecategories').create(category=table_category_mapping)

            category_queryset = self.model_object('Texttablecategories').filter(category=table_category_mapping)

        if category_queryset:
            category_queryset = category_queryset[0]
            return category_queryset

    def get_model_object_fields(self, project=None, model_name=None, drop_id=True):
        if not project:
            project = self.project

        if model_name:
            if not model_name == "Select a table mapping":
                model_fields = []
                try:
                    model_object = apps.get_model(project.project_connection_name, model_name)
                    model_fields_objects = model_object._meta.get_fields()
                    for field in model_fields_objects:
                        if not field.name == "id" and not type(field) == ManyToOneRel:
                            model_fields.append(field.name)
                    return model_fields
                except:
                    return []
            return []
        else:
            project_model_object = self.get_class_object(self.model_name)
            self.project_model_object = project_model_object
            self.model_fields = project_model_object._meta.get_fields()
            return self.filter_model_fields_for_form(drop_id=drop_id)

    def filter_model_fields_for_form(self, drop_id=False, include_all=False):
        field_names = []
        for field in self.model_fields:
            if include_all:
                field_names.append(field.name)
            else:
                if drop_id:
                    if not field.name == "id" and not type(field) == ManyToOneRel:
                        field_names.append(field.name)
                else:
                    if not include_all and not type(field) == ManyToOneRel and not type(field) == ForeignKey:
                        field_names.append(field.name)

        return field_names

    def clean_model_mapping_fields(self, project=None, model_fields=None, model_name=None, csv_column_name=None):
        if model_fields:
            extra_fields = self.get_model_object_existing_fields(project=project, model_name=model_name)

            has_string_model_fields = ['New Field:', ]
            updated_model_fields = []
            ignore_fields = ['apartment', 'property', 'value']

            add_strings = False
            for field in model_fields:
                if field not in ignore_fields:
                    if not field == "type_string":
                        updated_model_fields.append(field)
                    else:
                        add_strings = True

            if model_name == "Buildings":
                new_updated_model_fields = updated_model_fields

                updated_model_fields = []
                for field in new_updated_model_fields:
                    if field in ['name', "Name"]:
                        field = "building_name"
                    else:
                        field = field

                    updated_model_fields.append(field)

            if add_strings:
                #print('****** model_name', model_name, model_fields, 'RETURNING updated_model_fields', has_string_model_fields + updated_model_fields + extra_fields)
                return has_string_model_fields + updated_model_fields + extra_fields

            # print('****** model_name', model_name, model_fields, 'RETURNING OTHER updated_model_fields', updated_model_fields + extra_fields)
            return updated_model_fields + extra_fields

        # print('****** model_name', model_name, model_fields, 'RETURNING model_fields', model_fields)
        return model_fields

    def get_model_object_existing_fields(self, project=None, model_name=None, drop_id=True):

        if model_name:
            try:
                strings_model_object = apps.get_model(project.project_connection_name, "Strings")
                extra_fields = []
                table_category_mapping = self.get_mapping_table_value(model_name)
                if table_category_mapping:
                    category_queryset = self.model_object("TextTableCategories").filter(category=table_category_mapping)

                    strings_queryset = strings_model_object.objects.using(project.project_connection_name).filter(
                        text_table_category=category_queryset[0])

                    for item in strings_queryset:
                        string_dict = model_to_dict(
                            strings_model_object.objects.using(project.project_connection_name).get(pk=item.id))

                        if "english" in string_dict:
                            if string_dict['english'] not in extra_fields:
                                extra_fields.append(string_dict['english'])

                return extra_fields

            except:
                return []
        else:
            self.model_fields = self.project_model_object._meta.get_fields()
            return self.filter_model_fields_for_form(drop_id=drop_id)

    def ignore_list(self):
        return ['level_plans', 'key_plans', 'floor_plans', 'Floor Plans', 'Key Plans', 'Level Plans']

    def get_model_field_mapping_and_value(self, model_name, field_name, field_value):
        model_object = apps.get_model(self.project.project_connection_name, model_name)
        model_fields_objects = model_object._meta.get_fields()

        names_model_object = self.get_class_object('Names')
        floors_model_object = self.get_class_object('Floors')
        property_types_model_object = self.get_class_object('Propertytypes')

        for field in model_fields_objects:
            if field.name == field_name and type(field) == ForeignKey and field_value:
                if field.rel.to == names_model_object:
                    name_string = names_model_object.objects.using(self.project.project_connection_name).get(
                        id=int(field_value))
                    return True, name_string.english
                elif field.rel.to == floors_model_object:
                    floors_options = floors_model_object.objects.using(self.project.project_connection_name).filter(
                        pk=field_value). \
                        values_list('name__english', flat=True).order_by('name__english')
                    if floors_options:
                        floors_options = floors_options[0]
                    return True, floors_options
                elif field.rel.to == property_types_model_object:
                    types_options = property_types_model_object.objects.using(
                        self.project.project_connection_name).filter(pk=field_value).values_list('type', flat=True)
                    if types_options:
                        types_options = types_options[0]
                    return True, types_options

        return False, []

    def check_model_field_mapping(self, model_name, field_name, field_value=None, limit_to_building=None):

        model_object = apps.get_model(self.project.project_connection_name, model_name)
        model_fields_objects = model_object._meta.get_fields()

        names_model_object = self.get_class_object('Names')
        floors_model_object = self.get_class_object('Floors')
        property_types_model_object = self.get_class_object('Propertytypes')

        for field in model_fields_objects:
            if field.name == field_name and field_value and str(field_value).lower() == "status":
                return True, [('Available', 'Available'), ('Pending', 'Pending'),
                               ('Reserved', 'Reserved'), ('Sold', 'Sold'), ('Unavailable', 'Unavailable')]

            if field.name == field_name and type(field) == ForeignKey and field_value:

                if field.rel.to == names_model_object:
                    name_string = self.model_object("Names").get(pk=int(field_value))
                    return True, name_string.english
                elif field.rel.to == floors_model_object:
                    if not limit_to_building:
                        floors_options = self.model_object("Floors").all(). \
                            values_list('pk', 'name__english').order_by('name__english')
                    else:
                        building_floors = self.model_object("FloorsBuildings").filter(building_id=limit_to_building)\
                            .values_list('floor_id', flat=True)
                        floors_options = self.model_object("Floors").filter(pk__in=building_floors). \
                            values_list('pk', 'name__english').order_by('name__english')
                    return True, floors_options
                elif field.rel.to == property_types_model_object:
                    types_options = self.model_object("Propertytypes").all().values_list('pk', 'type')
                    return True, types_options

        return False, []

    def check_and_update_project_templates(self, project):
        project.check_connection_name()
        project.check_connection_mapping()

        if self.project.created == datetime.date.today():
            from core.models import ProjectCSVTemplate, ProjectCSVTemplateFields
            templates = ProjectCSVTemplate.objects.filter(project=self.project.project_copied_from)
            for template in templates:
                check_templates = ProjectCSVTemplate.objects.filter(project=self.project,
                                                                    template_name=template.template_name)
                if not check_templates:
                    fields = ProjectCSVTemplateFields.objects.filter(template=template)
                    if fields:
                        new_template = template
                        new_template.id = None
                        new_template.project = self.project
                        new_template.save()

                        for field in fields:
                            new_field = field
                            new_field.id = None
                            new_field.template = new_template
                            new_field.save()

    # def get_or_create(self, kwargs):
    #     pass
