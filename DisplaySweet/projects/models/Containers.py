from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin
from .common import BaseModelMixin


class Containers(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Containers, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_container_string_field(self, template_container, project_container):
        display_name = self.model_object("Strings").filter(english=project_container.string_field)

        if template_container and template_container.pk and not template_container in [None, 'None', '']:

            if display_name:
                display_name = display_name[0]

                check_existing_field = self.model_object("ContainerStringFields").filter(key_string=display_name,
                                                                                         template_container=template_container)

                if not check_existing_field:
                    self.model_object("ContainerStringFields").create(key_string=display_name,
                                                                      template_container=template_container)

                    check_existing_field = self.model_object("ContainerStringFields").filter(key_string=display_name,
                                                                                             template_container=template_container)

                if check_existing_field:
                    check_existing_field = check_existing_field[0]
                    return check_existing_field.pk, check_existing_field.value

        return '', ''

    def save_container_to_template(self, containers, selected_template, selected_canvas, wire_frame):

        # print('containers', len(containers))
        # print('selected_template', selected_template, type(selected_template))
        # print('selected_canvas', selected_canvas, type(selected_canvas))
        # print('wire_frame', wire_frame, type(wire_frame))

        wireframe_controller = self.model_object('WireframeControllers').filter(canvas_id=selected_canvas.pk,
                                                                                template_id=selected_template.pk,
                                                                                wireframe=wire_frame)

        if not wireframe_controller:
            self.model_object('WireframeControllers').create(canvas_id=selected_canvas.pk,
                                                             template_id=selected_template.pk,
                                                             wireframe=wire_frame)

            wireframe_controller = self.model_object('WireframeControllers').filter(canvas_id=selected_canvas.pk,
                                                                                    template_id=selected_template.pk,
                                                                                    wireframe=wire_frame)

        wireframe_controller = wireframe_controller[0]

        self.model_object('WireframeControllerContainers').filter(wireframe_controller=wireframe_controller)\
            .exclude(container_id__in=containers).delete()

        for container in containers:
            self.model_object("TemplateTypeContainers").get_or_create(
                template_type_id=selected_template.template_type_id, container_id=container)


            template_container = self.model_object('WireframeControllerContainers').filter(container_id=container,
                                                                                           wireframe_controller=wireframe_controller)
            if not template_container:

                self.model_object('WireframeControllerContainers').create(container_id=container,
                                                                          wireframe_controller=wireframe_controller)

                template_container = self.model_object('WireframeControllerContainers').filter(container_id=container,
                                                                                               wireframe_controller=wireframe_controller)

            template_container = template_container[0]

            container_settings = self.model_object("ContainerSettings").filter(main_container_id=container).order_by('orderidx')

            if not container_settings:
                children_containers = self.model_object("Containers").filter(
                    container_parent_id=container)

                for child in children_containers:
                    container_settings = self.model_object("ContainerSettings").create(
                        main_container_id=container,
                        container_id=child.pk)

                    check_existing_field = self.model_object("ContainerStringFields").filter(
                        template_container=template_container)

                    for existing_field in check_existing_field:
                        key_string_id = existing_field.key_string_id

                        key_string_field_value = self.model_object("Strings").filter(pk=key_string_id).values_list('value', flat=True)
                        if key_string_field_value:
                            self.model_object("ContainerFields").create(string_field=key_string_field_value[0],
                                                                        container_settings=container_settings,
                                                                        orderidx=1,
                                                                        field_type='text_box')

                return

            for csettings in container_settings:
                children_containers = self.model_object("ContainerFields").filter(
                    container_settings=csettings).order_by('orderidx')
                for field in children_containers:
                    strings_field = self.klass("Strings").get_or_create_strings_instance(field.string_field)
                    if strings_field:
                        check_existing_field = self.model_object("ContainerStringFields").filter(key_string=strings_field,
                                                                                          template_container=template_container)
                        if not check_existing_field:
                            self.model_object("ContainerStringFields").create(key_string=strings_field,
                                                                              template_container=template_container)

    def save_container_string_fields(self, data, selected_canvas_id=None, wireframe_controller=None, save_to_all=False):

        # print '****************'
        # print wireframe_controller
        # print data
        # print '****************'

        if not save_to_all:
            for key, value in data.items():
                if "field_" in key:
                    field_id = key.replace("field_", "")
                    if value in [None, "None", False, "False", " "]:
                        value = ''

                    try:
                        self.model_object("ContainerStringFields").filter(pk=field_id).update(value=value)
                    except Exception as e:
                        print('update error', e)
        else:
            for key, value in data.items():
                if "field_" in key:
                    field_id = key.replace("field_", "")
                    check_existing_field = self.model_object("ContainerStringFields").filter(pk=field_id)

                    if check_existing_field:
                        check_existing_field = check_existing_field[0]
                        key_string_id = check_existing_field.key_string_id

                        template_container = self.model_object("WireframeControllerContainers").filter(
                            pk=check_existing_field.template_container_id,
                            wireframe_controller__canvas_id=selected_canvas_id)

                        if template_container:
                            template_container = template_container[0]
                            container_id = template_container.container_id

                            other_wireframe_field = self.model_object("ContainerStringFields").filter(
                                key_string_id=key_string_id,
                                template_container__container_id=container_id,
                                template_container__wireframe_controller=wireframe_controller)

                            # print '----- ', wireframe_controller
                            # print 'template_container', template_container
                            # print 'container_id', container_id
                            # print 'other_wireframe_field', other_wireframe_field

                            if other_wireframe_field:
                                if value in [None, "None", False, "False"]:
                                    value = ''

                                for other_field in other_wireframe_field:
                                    other_field.value = value
                                    other_field.save(using=self.project.project_connection_name)

    def get_appropriate_container(self, containers, page_type, section):

        for container in containers:
            c = self.model_object("Containers").filter(pk=container)
            if c:
                selected_container = c[0]
                container_name = self.model_object("Names").filter(pk=selected_container.container_name_id)
                if container_name:
                    container_name = container_name[0].english

                    if section == "main" and container_name == "Thumbnail Details":
                        return container
                    if section == "watermark" and container_name.lower() == "watermark":
                        return container
                    if section == "unique_thumbnail_files" and container_name.lower() == "unique thumbnail files":
                        return container

                    if section == "individual_image":
                        if container_name.lower() == "level image selector":

                            kwargs = {
                                '{0}'.format('container_parent_id'): selected_container.pk,
                                '{0}'.format('container_name__english'): "Individual Image",
                            }

                            individual_containers = self.model_object("Containers").filter(**kwargs)
                            if individual_containers:
                                return individual_containers[0]

                    if section == "multi_image":
                        if container_name.lower() == "level image selector":

                            kwargs = {
                                '{0}'.format('container_parent_id'): selected_container.pk,
                                '{0}'.format('container_name__english'): "Multi Level Image",
                            }

                            individual_containers = self.model_object("Containers").filter(**kwargs)
                            if individual_containers:
                                return individual_containers[0]

        return containers[0]

    def get_container_template_by_id(self, container_id, template, selected_canvas, wire_frame):
        container = self.get_container_instance(container_id)
        if container:
            container_template = self.get_container_template(container, selected_canvas, template, wire_frame)

            if container_template:
                return container_template

    def get_container_template(self, container, selected_canvas, template, wire_frame):

        wireframe_controller, created = self.model_object('WireframeControllers').get_or_create(canvas_id=selected_canvas.pk,
                                                                                                template_id=template.pk,
                                                                                                wireframe=wire_frame)

        container_template, created = self.model_object('WireframeControllerContainers').get_or_create(wireframe_controller=wireframe_controller,
                                                                                                       container=container)

        return container_template

    def get_container_instance(self, container_id):
        container = self.model_object("Containers").filter(pk=container_id)

        if container:
            container = container[0]
            return container

    def save_new_container_data(self, data):
        if data['wireframe_main_name']:

            update_dict = {}
            display_name = self.klass("Names").get_or_save_display_name(data['wireframe_main_name'])

            parent_wire_frame_parent, created = self.model_object("Containers").get_or_create(
                container_name=display_name,
                container_parent__isnull=True,
                cms_display=True,
                orderidx=1, container_type_id=1)

            if created:
                parent_wire_frame_parent, created = self.model_object("Containers").get_or_create(
                    container_name=display_name,
                    container_parent__isnull=True,
                    cms_display=True, orderidx=1,
                    container_type_id=1)

            for key, value in data.items():
                if "main-menu" in key and value:
                    orderidx = key.replace('main-menu-', '').replace('wireframe-', '')

                    if parent_wire_frame_parent:

                        parent_wire_frame_parent.name = data['wireframe_main_name']
                        parent_wire_frame_parent.save(using=self.project.project_connection_name)

                        update_dict[orderidx] = {}
                        update_dict[orderidx]['order'] = orderidx
                        update_dict[orderidx]['name'] = value

                        data_key = "section-%s-width" % orderidx
                        update_dict[orderidx]['width'] = 100
                        if data_key in data:
                            update_dict[orderidx]['width'] = data[data_key]

                        display_name = self.klass("Names").get_or_save_display_name(value)
                        container_parent, created = self.model_object("Containers").get_or_create(
                            container_name=display_name,
                            container_parent=parent_wire_frame_parent,
                            orderidx=orderidx,
                            container_type_id=1)
                        if created:
                            container_parent, created = self.model_object("Containers").get_or_create(
                                container_name=display_name,
                                container_parent=parent_wire_frame_parent,
                                orderidx=orderidx, container_type_id=1)

                        container_settings = self.model_object('ContainerSettings').filter(
                            main_container_id=parent_wire_frame_parent.pk,
                            container_id=container_parent.pk)

                        if not container_settings:
                            self.model_object('ContainerSettings').create(orderidx=orderidx,
                                                                          main_container_id=parent_wire_frame_parent.pk,
                                                                          container_id=container_parent.pk,
                                                                          title=value,
                                                                          width=update_dict[orderidx]['width'])

                        container_settings = self.model_object('ContainerSettings').filter(
                            main_container_id=parent_wire_frame_parent.pk,
                            container_id=container_parent.pk)

                        container_settings = container_settings[0]
                        container_settings.title = value
                        container_settings.width = update_dict[orderidx]['width']
                        container_settings.save(using=self.project.project_connection_name)

                        sub_id_list = []
                        update_dict = {}

                        for i_key, i_value in data.items():
                            if "wireframe-%s" % orderidx in i_key:
                                try:
                                    order_sub_orderidx = i_key[-2:]
                                    order_sub_orderidx = int(order_sub_orderidx)
                                    if order_sub_orderidx < 0:
                                        order_sub_orderidx = i_key[-1:]
                                        try:
                                            order_sub_orderidx = int(order_sub_orderidx)
                                        except:
                                            order_sub_orderidx = None
                                except:
                                    order_sub_orderidx = i_key[-1:]
                                    try:
                                        order_sub_orderidx = int(order_sub_orderidx)
                                    except:
                                        order_sub_orderidx = None

                                if order_sub_orderidx:
                                    if order_sub_orderidx not in sub_id_list:
                                        sub_id_list.append(order_sub_orderidx)

                                    update_dict[order_sub_orderidx] = {}
                                    update_dict[order_sub_orderidx]['container_parent'] = container_parent
                                    update_dict[order_sub_orderidx]['order'] = order_sub_orderidx

                                    data_key = "wireframe-%s-sub-menu-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['sub_name'] = data[data_key]

                                    data_key = "wireframe-%s-input-type-%s" % (orderidx, order_sub_orderidx)
                                    update_dict[order_sub_orderidx]['input_type'] = 'text_box'
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['input_type'] = data[data_key]

                                    data_key = "wireframe-%s-initial-value-%s" % (orderidx, order_sub_orderidx)
                                    update_dict[order_sub_orderidx]['initial_value'] = ''
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['initial_value'] = data[data_key]

                        for id in sub_id_list:
                            self.get_or_save_container_string_field(update_dict[id]['sub_name'])
                            self.model_object('ContainerFields').create(container_settings=container_settings,
                                                                                string_field=update_dict[id][
                                                                                    'sub_name'],
                                                                                initial_value=update_dict[id][
                                                                                    'initial_value'],
                                                                                field_type=update_dict[id][
                                                                                    'input_type'],
                                                                                orderidx=update_dict[id]['order'])

    def edit_new_container_data(self, data, parent_wire_frame_parent):
        update_dict = {}

        if data['wireframe_main_name']:
            display_name = self.klass("Names").get_or_save_display_name(data['wireframe_main_name'])
            parent_wire_frame_parent.container_name = display_name
            parent_wire_frame_parent.save(using=self.project.project_connection_name)

        if parent_wire_frame_parent:
            containers = self.model_object("Containers").filter(container_parent=parent_wire_frame_parent)

            for container in containers:
                if "wireframe-main-menu-%s" % container.pk in data:
                    data_key = "wireframe-main-menu-%s" % container.pk
                    title = data[data_key]

                    self.model_object("Names").filter(pk=container.container_name_id).update(
                        english=data[data_key])

                    data_key = "section-width-%s" % container.pk

                    width = 100
                    if data_key in data:
                        width = data[data_key]

                    data_key = "section-order-%s" % container.pk
                    order = 1
                    if data_key in data:
                        order = data[data_key]
                        container.orderidx = order

                    container.save(using=self.project.project_connection_name)

                    list_items = self.model_object('ContainerSettings').filter(container_id=container.pk)
                    if not list_items:
                        self.model_object('ContainerSettings').create(container_id=container.pk)
                        list_items = self.model_object('ContainerSettings').filter(container_id=container.pk)

                    item = list_items[0]
                    item.main_container_id = parent_wire_frame_parent.pk
                    item.title = title
                    item.width = width
                    item.orderidx = order
                    item.save(using=self.project.project_connection_name)

                    child_options = self.model_object('ContainerFields').filter(container_settings=item)
                    for option in child_options:
                        data_key = 'section-label-%s' % option.pk
                        if data_key in data:
                            section_label = data[data_key]
                            option.string_field = section_label
                            self.get_or_save_container_string_field(section_label)

                        data_key = 'section-input-type-%s' % option.pk
                        if data_key in data:
                            option.field_type = data[data_key]

                        data_key = 'section-initial-value-%s' % option.pk
                        if data_key in data:
                            option.initial_value = data[data_key]

                        data_key = 'wire-list-order-%s' % option.pk
                        if data_key in data:
                            option.orderidx = data[data_key]

                        option.save(using=self.project.project_connection_name)

                    sub_id_list = []
                    for key, value in data.items():
                        try:
                            order_sub_orderidx = key[-2:]
                            order_sub_orderidx = int(order_sub_orderidx)
                            if order_sub_orderidx < 0:
                                order_sub_orderidx = key[-1:]
                                try:
                                    order_sub_orderidx = int(order_sub_orderidx)
                                except:
                                    order_sub_orderidx = None
                        except:
                            order_sub_orderidx = key[-1:]
                            try:
                                order_sub_orderidx = int(order_sub_orderidx)
                            except:
                                order_sub_orderidx = None

                        if order_sub_orderidx:

                            update_dict[order_sub_orderidx] = {}
                            update_dict[order_sub_orderidx]['settings'] = item

                            update_dict[order_sub_orderidx]['order'] = order_sub_orderidx
                            if order_sub_orderidx not in sub_id_list:
                                sub_id_list.append(order_sub_orderidx)

                            data_key = "wireframe-section-%s-sub-menu-%s" % (container.pk, order_sub_orderidx)

                            if data_key in data:
                                update_dict[order_sub_orderidx]['sub_name'] = data[data_key]

                            data_key = "wireframe-section-%s-input-type-%s" % (container.pk, order_sub_orderidx)
                            update_dict[order_sub_orderidx]['input_type'] = 'text_box'

                            if data_key in data:
                                update_dict[order_sub_orderidx]['input_type'] = data[data_key]

                            data_key = "wireframe-section-%s-initial-value-%s" % (container.pk, order_sub_orderidx)
                            update_dict[order_sub_orderidx]['initial_value'] = 0

                            if data_key in data:
                                update_dict[order_sub_orderidx]['initial_value'] = data[data_key]

                    for id in sub_id_list:
                        if "sub_name" in update_dict[id]:
                            self.get_or_save_container_string_field(update_dict[id]['sub_name'])
                            self.model_object('ContainerFields').get_or_create(
                                container_settings=update_dict[id]['settings'],
                                string_field=update_dict[id]['sub_name'],
                                initial_value=update_dict[id]['initial_value'],
                                field_type=update_dict[id]['input_type'],
                                orderidx=update_dict[id]['order'])

            sub_id_list = []
            update_dict = {}
            for key, value in data.items():
                if "main-menu-section-" in key and value:
                    orderidx = key.replace('main-menu-section-', '').replace('wireframe-', '')

                    if orderidx not in sub_id_list:
                        sub_id_list.append(orderidx)

                    if parent_wire_frame_parent:

                        update_dict[orderidx] = {}
                        update_dict[orderidx]['order'] = orderidx
                        update_dict[orderidx]['name'] = value

                        data_key = "main-section-%s-width" % orderidx
                        update_dict[orderidx]['width'] = 100
                        if data_key in data:
                            update_dict[orderidx]['width'] = data[data_key]

                        display_name = self.klass("Names").get_or_save_display_name(value)
                        container_parent, created = self.model_object("Containers").get_or_create(
                            container_name=display_name,
                            container_parent=parent_wire_frame_parent,
                            orderidx=orderidx,
                            container_type_id=1)
                        if created:
                            container_parent, created = self.model_object("Containers").get_or_create(
                                container_name=display_name,
                                container_parent=parent_wire_frame_parent,
                                orderidx=orderidx, container_type_id=1)

                        container_settings = self.model_object('ContainerSettings').filter(
                            main_container_id=parent_wire_frame_parent.pk,
                            container_id=container_parent.pk)

                        if not container_settings:
                            container_settings = self.model_object('ContainerSettings').create(
                                orderidx=orderidx,
                                main_container_id=parent_wire_frame_parent.pk,
                                container_id=container_parent.pk,
                                title=value,
                                width=update_dict[orderidx]['width'])

                        container_settings = self.model_object('ContainerSettings').filter(
                            main_container_id=parent_wire_frame_parent.pk,
                            container_id=container_parent.pk)

                        container_settings = container_settings[0]
                        container_settings.title = value
                        container_settings.width = update_dict[orderidx]['width']
                        container_settings.save(using=self.project.project_connection_name)

                        # self.model_object('ContainerFields').filter(container_settings=container_settings).delete()

                        baby_sub_ids = []
                        for i_key, i_value in data.items():
                            if "wireframe-section-%s" % orderidx in i_key:

                                try:
                                    order_sub_orderidx = i_key[-2:]
                                    order_sub_orderidx = int(order_sub_orderidx)
                                    if order_sub_orderidx < 0:
                                        order_sub_orderidx = i_key[-1:]
                                        try:
                                            order_sub_orderidx = int(order_sub_orderidx)
                                        except:
                                            order_sub_orderidx = None
                                except:
                                    order_sub_orderidx = i_key[-1:]
                                    try:
                                        order_sub_orderidx = int(order_sub_orderidx)
                                    except:
                                        order_sub_orderidx = None

                                if order_sub_orderidx:

                                    if order_sub_orderidx not in baby_sub_ids:
                                        baby_sub_ids.append(order_sub_orderidx)

                                    update_dict[orderidx]['baby_sub_ids'] = baby_sub_ids

                                    update_dict[orderidx][order_sub_orderidx] = {}
                                    update_dict[orderidx][order_sub_orderidx]['container_parent'] = container_parent
                                    update_dict[orderidx][order_sub_orderidx]['order'] = order_sub_orderidx

                                    data_key = "wireframe-section-%s-sub-menu-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[orderidx][order_sub_orderidx]['sub_name'] = data[data_key]

                                    data_key = "wireframe-section-%s-input-type-%s" % (orderidx, order_sub_orderidx)
                                    update_dict[orderidx][order_sub_orderidx]['input_type'] = 'text_box'
                                    if data_key in data:
                                        update_dict[orderidx][order_sub_orderidx]['input_type'] = data[data_key]

                                    data_key = "wireframe-section-%s-initial-value-%s" % (orderidx, order_sub_orderidx)
                                    update_dict[orderidx][order_sub_orderidx]['initial_value'] = ''
                                    if data_key in data:
                                        update_dict[orderidx][order_sub_orderidx]['initial_value'] = data[data_key]

                        for id in baby_sub_ids:
                            self.get_or_save_container_string_field(update_dict[orderidx][id]['sub_name'])

                            self.model_object('ContainerFields').get_or_create(
                                container_settings=container_settings,
                                string_field=update_dict[orderidx][id]['sub_name'],
                                initial_value=update_dict[orderidx][id]['initial_value'],
                                field_type=update_dict[orderidx][id]['input_type'],
                                orderidx=update_dict[orderidx][id]['order'])

    def get_or_save_container_string_field(self, string_value):
        names_text_category = self.model_object("TextTableCategories").filter(category="container_strings")
        if not names_text_category:
            self.model_object("TextTableCategories").create(category="container_strings")
            names_text_category = self.model_object("TextTableCategories").filter(category="container_strings")

        if names_text_category:
            names_text_category = names_text_category[0]

        container_string = self.model_object("Strings").filter(english=string_value,
                                                               text_table_category=names_text_category)
        if len(container_string) == 0:
            container_string = self.model_object("Strings").create(english=string_value,
                                                                   text_table_category=names_text_category)
            return container_string

        container_string = container_string[0]
        return container_string


    def get_template_container_object_list(self, main_container_list, selected_template, selected_canvas, wire_frame):
        
        container_list = []
        template_containerlist = []

        for ct in main_container_list:
            model_dict = model_to_dict(ct)
            container_list.append(model_dict['container'])
            main_container = self.model_object('Containers').filter(pk=model_dict['container'],
                                                                            container_parent__isnull=True)
    
            if main_container:
                main_container = main_container[0]
                container_name = self.klass("Names").get_names_instance(main_container.container_name_id)
                selected_settings = self.model_object("ContainerSettings").filter(main_container_id=model_dict['container']).order_by('orderidx')
    
                container_settings = []
                for csettings in selected_settings:
                    children_containers = self.model_object("ContainerFields").filter(container_settings=csettings).order_by('orderidx')
    
                    children_objects = []
                    for children in children_containers:
                        check_template_container = self.klass("Containers").get_container_template_by_id(csettings.container_id,
                                                                                                        selected_template,
                                                                                                        selected_canvas,
                                                                                                        wire_frame)
                        if check_template_container:
                            check_container_string_field, check_container_string_field_value = \
                                self.klass("Containers").get_container_string_field(check_template_container, children)
    
                            object_dict = {
                                'project_container': children,
                                'container_string_field_id': check_container_string_field,
                                'check_container_string_field_value': check_container_string_field_value,
                                'template_container': check_template_container,
                                'orderidx': children.orderidx
                            }
    
                            if object_dict not in children_objects:
                                children_objects.append(object_dict)
    
                    container_settings.append({'csettings': csettings,
                                               'container_template_id': self.klass("Containers").get_container_template_by_id(csettings.pk,
                                                                                                                              selected_template,
                                                                                                                              selected_canvas,
                                                                                                                              wire_frame),
                                               'container_children': children_objects})
    
                template_containerlist.append(
                    {
                        'main_container_id': main_container.pk,
                        'container_name': container_name,
                        'settings': container_settings,
                        'container_template': self.klass("Containers").get_container_template(main_container, selected_canvas, selected_template, wire_frame)
                    }
                )

        return template_containerlist, container_list

    def get_template_container_settings(self, selected_template, canvas_id, wire_frame_id):

        # print('--- new ', selected_template, canvas_id, wire_frame_id)

        controllers = None

        if selected_template:
            controllers = self.model_object("WireframeControllers").filter(template_id=selected_template.pk)

        canvases = self.model_object("Canvases").all()

        selected_canvas = None
        if canvases:
            selected_canvas = canvases[0]

        if canvas_id:
            selected_canvas = self.model_object("Canvases").filter(pk=canvas_id)
            selected_canvas = selected_canvas[0]

        wire_frames = self.model_object("Wireframes").all()
        wire_frame = wire_frames[0]

        if wire_frame_id:
            wire_frame = self.model_object("Wireframes").filter(pk=wire_frame_id)
            if wire_frame:
                wire_frame = wire_frame[0]
        else:
            if wire_frame:
                wire_frame_id = wire_frame.pk

        wireframe_controllers = self.model_object('WireframeControllers').filter(template=selected_template,
                                                                                 canvas=selected_canvas)
        if wire_frame_id:
            wireframe_controllers = wireframe_controllers.filter(wireframe_id=wire_frame.pk)

            if not wireframe_controllers:
                self.model_object('WireframeControllers').create(template=selected_template, canvas=selected_canvas,
                                                                 wireframe_id=wire_frame_id)

            wireframe_controllers = self.model_object('WireframeControllers').filter(template=selected_template,
                                                                                     canvas=selected_canvas,
                                                                                     wireframe_id=wire_frame.pk)

        selected_container_templates = self.model_object('WireframeControllerContainers').filter(
            wireframe_controller=wireframe_controllers)

        if controllers and not selected_container_templates:
            containers = self.model_object('TemplateTypeContainers').filter(
                template_type_id=selected_template.template_type_id)
            for item in wireframe_controllers:
                for container in containers:
                    self.model_object('WireframeControllerContainers').create(wireframe_controller=item,
                                                                              container_id=container.container_id)

        selected_container_templates = self.model_object('WireframeControllerContainers').filter(
            wireframe_controller=wireframe_controllers)

        main_container_list = []
        selected_container_id_list = []
        for sc in selected_container_templates:
            if sc.container_id not in selected_container_id_list:
                main_container_list.append(sc)
                selected_container_id_list.append(sc.container_id)

        container_list = []
        template_containerlist = []

        for ct in main_container_list:
            model_dict = model_to_dict(ct)
            container_list.append(model_dict['container'])
            main_container = self.model_object('Containers').filter(pk=model_dict['container'],
                                                                    container_parent__isnull=True)

            if main_container:
                main_container = main_container[0]
                container_name = self.klass("Names").get_names_instance(main_container.container_name_id)
                selected_settings = self.model_object("ContainerSettings").filter(
                    main_container_id=model_dict['container']).order_by('orderidx')

                container_settings = []
                for csettings in selected_settings:
                    children_containers = self.model_object("ContainerFields").filter(
                        container_settings=csettings).order_by('orderidx')

                    children_objects = []
                    for children in children_containers:
                        check_template_container = self.get_container_template_by_id(csettings.container_id,
                                                                                     selected_template, selected_canvas,
                                                                                     wire_frame)
                        if check_template_container:
                            check_container_string_field, check_container_string_field_value = \
                                self.get_container_string_field(check_template_container, children)

                            object_dict = {
                                'project_container': children,
                                'container_string_field_id': check_container_string_field,
                                'check_container_string_field_value': check_container_string_field_value,
                                'template_container': check_template_container,
                                'orderidx': children.orderidx
                            }

                            if object_dict not in children_objects:
                                children_objects.append(object_dict)

                    container_settings.append({'csettings': csettings,
                                               'container_template_id': self.get_container_template_by_id(csettings.pk,
                                                                                                          selected_template,
                                                                                                          selected_canvas,
                                                                                                          wire_frame),
                                               'container_children': children_objects})

                template_containerlist.append(
                    {
                        'main_container_id': main_container.pk,
                        'container_name': container_name,
                        'settings': container_settings,
                        'container_template': self.get_container_template(main_container, selected_canvas,
                                                                          selected_template, wire_frame)
                    }
                )

        return template_containerlist