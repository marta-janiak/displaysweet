from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin
from django.db.models import Q


class Wireframes(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Wireframes, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def clone_entire_wireframe(self, canvas_to_clone, new_canvas_name, new_wireframe_name):

        main_container_list = []

        if canvas_to_clone:
            canvas_to_clone = canvas_to_clone[0]

            self.model_object("Names").create(english=new_canvas_name)
            new_canvas_name = self.model_object("Names").filter(english=new_canvas_name).order_by('-pk')[0]

            self.model_object("Canvases").create(canvas_name_id=new_canvas_name.pk)
            new_canvas = self.model_object("Canvases").filter(canvas_name_id=new_canvas_name.pk)[0]
            new_canvas_id = new_canvas.pk

            canvas_wireframes_to_clone = self.model_object("CanvasWireframes").filter(canvas_id=canvas_to_clone.pk)

            if canvas_wireframes_to_clone:
                canvas_wireframes_to_clone = canvas_wireframes_to_clone[0]
                main_wireframe_to_clone = self.model_object("Wireframes").filter(pk=canvas_wireframes_to_clone.wireframe_id)[0]
                self.model_object("Names").create(english=new_wireframe_name)

                new_wireframe_name = self.model_object("Names").filter(english=new_wireframe_name).order_by('-pk')[0]

                self.model_object("Wireframes").create(display_name_id=new_wireframe_name.pk,
                                                       name=main_wireframe_to_clone.name,
                                                       wireframe_parent_id='',
                                                       template_id=main_wireframe_to_clone.template_id,
                                                       orderidx=main_wireframe_to_clone.orderidx)

                new_wireframe = self.model_object("Wireframes").filter(display_name_id=new_wireframe_name.pk,
                                                                       name=main_wireframe_to_clone.name,
                                                                       template_id=main_wireframe_to_clone.template_id,
                                                                       orderidx=main_wireframe_to_clone.orderidx)[0]

                new_wireframe_parent_id = new_wireframe.pk

                self.model_object("CanvasWireframes").create(canvas_id=new_canvas_id,
                                                             wireframe_id=new_wireframe_parent_id)

                menus_to_clone = self.model_object("Wireframes").filter(
                    wireframe_parent_id=main_wireframe_to_clone.pk).order_by('orderidx')

                for menu in menus_to_clone:
                    menu_name = model_to_dict(menu)
                    menu_name = menu_name['name']

                    self.model_object("Names").create(english=menu_name)
                    new_menu_name = self.model_object("Names").filter(english=menu_name).order_by('-pk')[0]

                    self.model_object("Wireframes").create(display_name_id=new_menu_name.pk,
                                                           name=menu.name,
                                                           template_id=menu.template_id,
                                                           wireframe_parent_id=new_wireframe_parent_id,
                                                           orderidx=menu.orderidx)

                    new_menu = self.model_object("Wireframes").filter(display_name_id=new_menu_name.pk,
                                                                      name=menu.name,
                                                                      template_id=menu.template_id,
                                                                      wireframe_parent_id=new_wireframe_parent_id,
                                                                      orderidx=menu.orderidx)[0]

                    if menu.template_id:

                        selected_container_id_list = []

                        wireframe_controllers = self.model_object('WireframeControllers').filter(
                            template=menu.template,
                            canvas=canvas_to_clone,
                            wireframe_id=menu.pk)

                        selected_container_templates = self.model_object('WireframeControllerContainers').filter(
                            wireframe_controller=wireframe_controllers)

                        for sc in selected_container_templates:
                            if sc.container_id not in selected_container_id_list:
                                main_container_list.append(sc)

                        get_template_container_object_list = self.klass("Containers") \
                            .get_template_container_object_list(main_container_list, menu.template, canvas_to_clone,
                                                                menu)

                        post_create_save = []

                        self.model_object("WireframeControllers").create(canvas_id=new_canvas_id,
                                                                         wireframe_id=new_menu.pk,
                                                                         template_id=menu.template_id)

                        contoller_to_clone = \
                        self.model_object("WireframeControllers").filter(canvas_id=canvas_to_clone.pk,
                                                                         wireframe_id=menu.pk,
                                                                         template_id=menu.template_id)[0]

                        new_menu_controller = \
                        self.model_object("WireframeControllers").filter(canvas_id=new_canvas_id,
                                                                         wireframe_id=new_menu.pk,
                                                                         template_id=menu.template_id)[0]

                        template_containers_to_clone = self.model_object("WireframeControllerContainers").filter(
                            wireframe_controller_id=contoller_to_clone.pk)

                        for controller in template_containers_to_clone:
                            self.model_object("WireframeControllerContainers").create(
                                wireframe_controller_id=new_menu_controller.pk,
                                container_id=controller.container_id)

                            new_template_container = self.model_object("WireframeControllerContainers") \
                                .filter(wireframe_controller_id=new_menu_controller.pk,
                                        container_id=controller.container_id)[0]

                            container_strings_to_clone = self.model_object("ContainerStringFields").filter(
                                template_container_id=controller.pk)

                            for string_field in container_strings_to_clone:
                                self.model_object("ContainerStringFields").create(
                                    template_container_id=new_template_container.pk,
                                    key_string_id=string_field.key_string_id,
                                    value=string_field.value)

                    baby_menus_to_clone = self.model_object("Wireframes").filter(
                        wireframe_parent_id=menu.pk).order_by('orderidx')

                    for baby_menu in baby_menus_to_clone:
                        baby_menu_name = model_to_dict(baby_menu)
                        baby_menu_name = baby_menu_name['name']

                        self.model_object("Names").create(english=baby_menu_name)
                        new_baby_menu_name = \
                        self.model_object("Names").filter(english=baby_menu_name).order_by('-pk')[0]

                        self.model_object("Wireframes").create(display_name_id=new_baby_menu_name.pk,
                                                               name=baby_menu.name,
                                                               template_id=baby_menu.template_id,
                                                               wireframe_parent_id=new_menu.pk,
                                                               orderidx=baby_menu.orderidx)

                        new_baby_wireframe = \
                        self.model_object("Wireframes").filter(display_name_id=new_baby_menu_name.pk,
                                                               name=baby_menu.name,
                                                               template_id=baby_menu.template_id,
                                                               wireframe_parent_id=new_menu.pk,
                                                               orderidx=baby_menu.orderidx).order_by('-pk')[0]

                        if baby_menu.template_id:

                            self.model_object("WireframeControllers").create(canvas_id=new_canvas_id,
                                                                             wireframe_id=new_baby_wireframe.pk,
                                                                             template_id=baby_menu.template_id)

                            contoller_to_clone = \
                            self.model_object("WireframeControllers").filter(canvas_id=canvas_to_clone.pk,
                                                                             wireframe_id=baby_menu.pk)[0]

                            new_menu_controller = \
                            self.model_object("WireframeControllers").filter(canvas_id=new_canvas_id,
                                                                             wireframe_id=new_baby_wireframe.pk,
                                                                             template_id=baby_menu.template_id)[0]

                            baby_template_containers_to_clone = self.model_object(
                                "WireframeControllerContainers").filter(
                                wireframe_controller_id=contoller_to_clone.pk)

                            for controller in baby_template_containers_to_clone:

                                self.model_object("WireframeControllerContainers").create(
                                    wireframe_controller_id=new_menu_controller.pk,
                                    container_id=controller.container_id)

                                new_template_container = self.model_object("WireframeControllerContainers") \
                                    .filter(wireframe_controller_id=new_menu_controller.pk,
                                            container_id=controller.container_id)[0]

                                baby_container_strings_to_clone = self.model_object("ContainerStringFields").filter(
                                    template_container_id=controller.pk)

                                for string_field in baby_container_strings_to_clone:
                                    self.model_object("ContainerStringFields").create(
                                        template_container_id=new_template_container.pk,
                                        key_string_id=string_field.key_string_id,
                                        value=string_field.value)

    def edit_new_wireframe_data(self, data, selected_canvase, wire_parent_id):
        update_dict = {}
        main_menu_items = []

        if 'wireframe_main_name' in data and data['wireframe_main_name']:
            display_name = self.klass("Names").get_or_create_names_instance(data['wireframe_main_name'])

            if display_name:
                parent_wire_frame_parent = self.objects.get(pk=wire_parent_id)

                self.objects.filter(pk=wire_parent_id).update(orderidx=1, name=data['wireframe_main_name'], display_name=display_name)

                if parent_wire_frame_parent:
                    self.model_object("CanvasWireframes").get_or_create(canvas=selected_canvase, wireframe=parent_wire_frame_parent)

                    self.klass("AssetTags").create_asset_tag(data['wireframe_main_name'])

                if parent_wire_frame_parent:
                    list_items = self.objects.filter(
                        wireframe_parent=parent_wire_frame_parent)

                    for item in list_items:
                        main_menu_items.append(item.pk)
                        update_dict[item.pk] = {}

                        order_key = "wire-list-order-%s" % item.pk
                        if order_key in data:
                            update_dict[item.pk]['order'] = data[order_key]

                        if "section-order-%s" % item.pk in data:
                            update_dict[item.pk]['orderidx'] = data["section-order-%s" % item.pk]

                        order_key = "wireframe-main-menu-%s" % item.pk
                        if order_key in data:
                            update_dict[item.pk]['name'] = data[order_key]
                            display_name = self.klass("Names").get_or_create_names_instance(update_dict[item.pk]['name'])

                        update_dict[item.pk]['display_name'] = display_name

                        new_wire_frame_parent = self.objects.using(
                            self.project.project_connection_name).filter(pk=item.pk)
                        
                        if new_wire_frame_parent:
                            new_wire_frame_parent = new_wire_frame_parent[0]
                            new_wire_frame_parent.wireframe_parent = parent_wire_frame_parent
                            if "orderidx" in update_dict[item.pk]:
                                new_wire_frame_parent.orderidx = update_dict[item.pk]['orderidx']
                            if "name" in update_dict[item.pk]:
                                new_wire_frame_parent.name = update_dict[item.pk]['name']
                                new_wire_frame_parent.display_name = display_name

                                self.klass("AssetTags").create_asset_tag(update_dict[item.pk]['name'])

                            new_wire_frame_parent.save(using=self.project.project_connection_name)
                            wire_model = apps.get_model(self.project.project_connection_name, 'Wireframes')
                            sub_items = wire_model.objects.using(self.project.project_connection_name).filter(
                                wireframe_parent=new_wire_frame_parent)

                            sub_list_items = []
                            for item in sub_items:
                                sub_list_items.append(item.pk)
                                update_dict[item.pk] = {}

                                if "wire-list-section-%s" % item.pk in data:
                                    update_dict[item.pk]['parent_id'] = data["wire-list-section-%s" % item.pk].replace(
                                        'section-', '')

                                order_key = "wire-list-order-%s" % item.pk
                                if order_key in data:
                                    update_dict[item.pk]['order'] = data[order_key]

                                order_key = "wireframe-section-%s-sub-menu-%s" % (new_wire_frame_parent.pk, item.pk)
                                if order_key in data:
                                    update_dict[item.pk]['sub_name'] = data[order_key]

                                order_key = "wireframe-%s-sub-menu-%s" % (new_wire_frame_parent.pk, item.pk)
                                if order_key in data:
                                    update_dict[item.pk]['sub_name'] = data[order_key]

                                    display_name = self.klass("Names").get_or_create_names_instance(update_dict[item.pk]['sub_name'])

                                order_key = "wireframe-%s-template-%s" % (new_wire_frame_parent.pk, item.pk)
                                if order_key in data:
                                    update_dict[item.pk]['template'] = data[order_key]

                                order_key = "wireframe-section-%s-template-%s" % (new_wire_frame_parent.pk, item.pk)
                                if order_key in data:
                                    update_dict[item.pk]['template'] = data[order_key]

                                new_wire_frame_child = self.objects.filter(pk=item.pk)

                                if new_wire_frame_child:
                                    if item.pk in update_dict and update_dict[item.pk]:

                                        new_wire_frame_child = new_wire_frame_child[0]
                                        new_wire_frame_child.display_name = display_name
                                        new_wire_frame_child.wireframe_parent_id = update_dict[item.pk]['parent_id']

                                        if "order" in update_dict[item.pk]:
                                            new_wire_frame_child.orderidx = update_dict[item.pk]['order']
                                        if "sub_name" in update_dict[item.pk]:
                                            new_wire_frame_child.name = update_dict[item.pk]['sub_name']

                                            self.klass("AssetTags").create_asset_tag(update_dict[item.pk]['sub_name'])

                                        if "template" in update_dict[item.pk]:
                                            wire_frame_controllers = self.model_object('WireframeControllers').filter(
                                                canvas_id=selected_canvase.pk,
                                                wireframe_id=wire_parent_id,
                                                template_id=update_dict[item.pk]['template'])

                                            if not wire_frame_controllers:
                                                self.model_object('WireframeControllers').create(
                                                    canvas_id=selected_canvase.pk,
                                                    wireframe_id=wire_parent_id,
                                                    template_id=update_dict[item.pk]['template'])

                                                # Create template container and containers so we can save data in preview
                                                template = self.model_object("Templates").filter(
                                                    pk=update_dict[item.pk]['template'])[0]
                                                containers = []
                                                template_types = self.model_object("TemplateTypeContainers").filter(
                                                    template_type_id=template.template_type_id)
                                                for ttype in template_types:
                                                    containers.append(ttype.container_id)

                                                children_containers = self.model_object("Containers").filter(
                                                    container_parent_id__in=containers)
                                                for c in children_containers:
                                                    containers.append(c.pk)

                                                wireframe = self.model_object("Wireframes").filter(pk=wire_parent_id)[0]
                                                self.klass("Containers").save_container_to_template(containers,
                                                                                                    template, selected_canvase,
                                                                                                    wireframe)

                                            new_wire_frame_child.template_id = update_dict[item.pk]['template']

                                            wire_frame_controllers = self.model_object('WireframeControllers').filter(
                                                canvas_id=selected_canvase.pk,
                                                wireframe_id=new_wire_frame_child.pk,
                                                template_id=update_dict[item.pk]['template'])

                                            if not wire_frame_controllers:
                                                self.model_object('WireframeControllers').create(
                                                    canvas_id=selected_canvase.pk,
                                                    wireframe_id=new_wire_frame_child.pk,
                                                    template_id=update_dict[item.pk]['template'])

                                                # Create template container and containers so we can save data in preview
                                                template = self.model_object("Templates").filter(
                                                    pk=update_dict[item.pk]['template'])[0]

                                                containers = []
                                                template_types = self.model_object("TemplateTypeContainers").filter(
                                                    template_type_id=template.template_type_id)
                                                for ttype in template_types:
                                                    containers.append(ttype.container_id)

                                                children_containers = self.model_object("Containers").filter(
                                                    container_parent_id__in=containers)
                                                for c in children_containers:
                                                    containers.append(c.pk)

                                                self.klass("Containers").save_container_to_template(containers,
                                                                                                    template,
                                                                                                    selected_canvase,
                                                                                                    new_wire_frame_child)

                                        new_wire_frame_child.save(using=self.project.project_connection_name)

                for key, value in data.items():
                    if "main-menu-" in key and value:
                        orderidx = key.replace('main-menu-', '').replace('wireframe-', '')

                        if parent_wire_frame_parent:

                            update_dict[orderidx] = {}
                            update_dict[orderidx]['order'] = orderidx
                            update_dict[orderidx]['name'] = value

                            self.klass("AssetTags").create_asset_tag(value)

                            display_name = self.klass("Names").get_or_create_names_instance(value)

                            update_dict[orderidx]['display_name'] = display_name

                            new_wire_frame_parent = self.objects.filter(wireframe_parent=parent_wire_frame_parent,
                                                                         name=value,
                                                                         display_name=display_name)
                            if not new_wire_frame_parent:
                                new_wire_frame_parent, created = self.objects.get_or_create(
                                    wireframe_parent=parent_wire_frame_parent,
                                    orderidx=orderidx,
                                    name=value,
                                    display_name=display_name)
                                if created:
                                    new_wire_frame_parent, created = self.objects.get_or_create(
                                        wireframe_parent=parent_wire_frame_parent,
                                        orderidx=orderidx,
                                        name=value,
                                        display_name=display_name)

                            sub_id_list = []
                            update_dict = {}

                            for i_key, i_value in data.items():

                                wireframes_key = "wireframe-main-menu-%s" % orderidx
                                if wireframes_key in data and new_wire_frame_parent:

                                    try:
                                        order_sub_orderidx = i_key[-2:]
                                        if "-" in order_sub_orderidx:
                                            order_sub_orderidx = i_key[-1:]

                                        order_sub_orderidx = int(order_sub_orderidx)

                                    except:
                                        order_sub_orderidx = i_key[-1:]

                                    if order_sub_orderidx not in sub_id_list:
                                        sub_id_list.append(order_sub_orderidx)

                                    update_dict[order_sub_orderidx] = {}
                                    update_dict[order_sub_orderidx]['wireframe_parent'] = new_wire_frame_parent

                                    data_key = "wireframe-section-%s-sub-menu-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['sub_name'] = data[data_key]

                                    data_key = "wireframe-section-%s-sub-menu-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['sub_name'] = data[data_key]

                                    data_key = "wireframe-section-%s-template-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['template'] = data[data_key]

                            for id in sub_id_list:
                                if "sub_name" in update_dict[id]:
                                    display_name = self.klass("Names").get_or_create_names_instance(update_dict[id]['sub_name'])
                                    self.klass("AssetTags").create_asset_tag(update_dict[id]['sub_name'])

                                    try:
                                        new_wire_frame_child, created = self.objects. \
                                            using(self.project.project_connection_name).get_or_create(orderidx=id,
                                                                                                      name= update_dict[id]['sub_name'],
                                                                                                      template_id=update_dict[id]['template'],
                                                                                                      display_name=display_name,
                                                                                                      wireframe_parent=new_wire_frame_parent)
                                    except:
                                        new_wire_frame_child, created = self.objects. \
                                            using(self.project.project_connection_name).get_or_create(orderidx=id,
                                                                                                      name=update_dict[id]['sub_name'],
                                                                                                      template_id=update_dict[id]['template'],
                                                                                                      display_name=display_name,
                                                                                                      wireframe_parent=new_wire_frame_parent[0])

                                        new_wire_frame_parent = new_wire_frame_parent[0]

                                    if created:
                                        new_wire_frame_child = self.model_object("Wireframes").filter(orderidx=id,
                                                                                                      name=update_dict[id]['sub_name'],
                                                                                                      template_id=update_dict[id]['template'],
                                                                                                      display_name=display_name,
                                                                                                      wireframe_parent=new_wire_frame_parent)

                                        new_wire_frame_child = new_wire_frame_child[0]

                                    wire_frame_controllers = self.model_object('WireframeControllers').filter(
                                        canvas_id=selected_canvase.pk,
                                        wireframe_id=new_wire_frame_child.pk,
                                        template_id=update_dict[id]['template'])

                                    if not wire_frame_controllers:
                                        self.model_object('WireframeControllers').create(canvas_id=selected_canvase.pk,
                                                                                         wireframe_id=new_wire_frame_child.pk,
                                                                                         template_id=update_dict[id]['template'])

                                        # Create template container and containers so we can save data in preview
                                        template = \
                                        self.model_object("Templates").filter(pk=update_dict[id]['template'])[0]

                                        containers = []
                                        template_types = self.model_object("TemplateTypeContainers").filter(
                                            template_type_id=template.template_type_id)
                                        for ttype in template_types:
                                            containers.append(ttype.container_id)

                                        children_containers = self.model_object("Containers").filter(
                                            container_parent_id__in=containers)
                                        for c in children_containers:
                                            containers.append(c.pk)

                                        self.klass("Containers").save_container_to_template(containers,
                                                                                            template,
                                                                                            selected_canvase,
                                                                                            new_wire_frame_child)
                                        
    def get_wireframe_preview_menus(self, canvas_id, wireframe_id, selected_parent_wireframe, selected_sub_wireframe):

        menus = []
        parent_menus = []
        selected_wireframe = self.objects.filter(pk=wireframe_id)
        selected_sub_wireframe_id = None
        senior_menus = []

        if not selected_wireframe:
            selected_wireframe = self.objects.filter(Q(wireframe_parent__isnull=True) | Q(wireframe_parent=None)).order_by('pk')

        if selected_wireframe:
            selected_wireframe = model_to_dict(selected_wireframe[0])
            if selected_wireframe['wireframe_parent']:
                selected_parent_wireframe = selected_wireframe
                senior_menus = self.objects.filter(wireframe_parent__pk=selected_wireframe['wireframe_parent']).order_by('orderidx')
            else:
                senior_menus = self.objects.filter(wireframe_parent__pk=selected_wireframe['id']).order_by('orderidx')

            for menu in senior_menus:
                parent_menus.append({'menu': menu, 'name': menu.name, 'id': menu.pk })

        if not selected_parent_wireframe:
            if not parent_menus:
                pass
            if parent_menus:
                selected_parent_wireframe = parent_menus[0]

        sub_menus = []

        if selected_sub_wireframe:
            selected_sub_wireframe_id = int(selected_sub_wireframe)

        floor_plan_details = None
        level_plan_details = None
        selected_menu = False
        if selected_parent_wireframe:
            children_menus = self.objects.filter(wireframe_parent__pk=selected_parent_wireframe['id']).order_by('orderidx')
            for menu in children_menus:
                sub_menus.append({'menu': menu, 'name': menu.name, 'id': menu.pk, 'template': menu.template_id })

                if selected_sub_wireframe and selected_sub_wireframe_id == menu.pk:
                    selected_menu = True
                    selected_sub_wireframe = {}
                    selected_sub_wireframe['menu'] = menu
                    selected_sub_wireframe['name'] = menu.name
                    selected_sub_wireframe['id'] = menu.pk
                    selected_sub_wireframe['template'] = menu.template_id

        if sub_menus and not selected_menu:
            selected_sub_wireframe = sub_menus[0]

        selected_canvase = self.model_object("Canvases").filter(pk=canvas_id)

        if selected_canvase:
            selected_canvase = selected_canvase[0]

            canvas_wireframes = self.model_object("CanvasWireframes").filter(canvas=selected_canvase)

            if canvas_wireframes:
                canvas_wireframes = canvas_wireframes[0]
                model_dict = model_to_dict(canvas_wireframes)
                wire_parent_id = model_dict['wireframe']
                menus = self.objects.filter(wireframe_parent_id=wire_parent_id).order_by('orderidx')

        return menus, parent_menus, sub_menus, selected_wireframe, selected_parent_wireframe, selected_sub_wireframe

    def get_wireframe_template_settings(self, selected_canvas, selected_template, selected_sub_wireframe, template_type):
        container_list = self.model_object("TemplateTypeContainers").filter(template_type=selected_template.template_type_id)

        #For Gallery Types we expect: Thumbnail Settings, Size, Padding, Default Source Image Settings and Watermark

        wireframe_controller = self.get_wireframe_controller(selected_canvas, selected_template, selected_sub_wireframe['id'])
        ##print 'wireframe_controller', wireframe_controller

        wireframe_page_settings = {}
        for container in container_list:
            container_parent = self.model_object('Containers').filter(pk=container.container_id)[0]
            container_parent_name = self.model_object("Names").filter(pk=container_parent.container_name_id)[0]
            container_parent_name_safe = container_parent_name.english.replace(' ', '_').lower()

            wireframe_page_settings[container_parent_name_safe] = {}
            wireframe_page_settings[container_parent_name_safe]['display_name'] = container_parent_name.english

            container_children = self.model_object('Containers').filter(container_parent_id=container.container_id)

            ##print container_parent_name_safe, 'container_children', container_children

            for children in container_children:
                template_container = self.model_object("WireframeControllerContainers").filter(wireframe_controller=wireframe_controller, container=children)
                container_string_fields = self.model_object("ContainerStringFields").filter(template_container=template_container)

                # Check if the fields for the container are "radio" then get the only one that is marked true
                # Otherwise get all setting values for checkbox and text boxes

                container_settings = self.model_object("ContainerSettings").filter(container_id=children.pk)

                if container_settings:
                    container_settings = container_settings[0]

                    container_name = self.model_object("Names").filter(pk=children.container_name_id)[0]
                    container_name = container_name.english.replace(" ", "_").lower()

                    wireframe_page_settings[container_parent_name_safe][container_name] = {}

                    for key_string in container_string_fields:
                        key_string_field = self.model_object("Strings").filter(pk=key_string.key_string_id)[0]
                        check_string_field = self.model_object("ContainerFields").filter(container_settings=container_settings,
                                                                                         string_field=key_string_field.english).order_by('orderidx')[0]


                        if check_string_field.field_type in ["radio", "checkbox"]:
                            radio_field_containers = self.model_object("ContainerStringFields").filter(template_container=template_container,
                                                                                                       value=True)

                            if radio_field_containers:
                                radio_field_containers = radio_field_containers[0]
                                selected_radio_field = self.model_object("Strings").filter(pk=radio_field_containers.key_string_id)[0]
                                radio_key = container_name.replace(" ", "_").lower()
                                wireframe_page_settings[container_parent_name_safe][container_name][radio_key] = selected_radio_field.english

                        else:
                            if key_string.value == "None":
                                key_string.value = ''

                            wireframe_page_settings[container_parent_name_safe][container_name][check_string_field.string_field] = key_string.value

        return wireframe_page_settings

    def get_wireframe_controller(self, selected_canvas, selected_template, wire_frame_id):

        kwargs = {
            '{0}'.format('canvas_id'): selected_canvas.pk,
            '{0}'.format('template_id'): selected_template.pk,
            '{0}'.format('wireframe_id'): wire_frame_id
        }

        wireframe_controller = self.model_object('WireframeControllers').filter(**kwargs)
        if not wireframe_controller:
            self.model_object('WireframeControllers').create(**kwargs)
            wireframe_controller = self.model_object('WireframeControllers').filter(**kwargs)

        wireframe_controller = wireframe_controller[0]

        return wireframe_controller

    def get_wireframe_settings_for_variable(self, variable, wireframe_page_settings):

        for key, value in wireframe_page_settings.items():
            if key == variable:
                return value

            for i_key, i_value in value.items():
                if i_key == variable:
                    return i_value

    def save_new_wireframe_data(self, data, saved_template=None):

        names_text_category = self.TextTableCategoriesModel.objects.using(self.project.project_connection_name).filter(
            category="names")
        if names_text_category:
            names_text_category = names_text_category[0]

        update_list = []
        update_dict = {}
        parent_wire_frame_parent = None

        if data['wireframe_main_name']:
            for key, value in data.items():
                if "main-menu-" in key and value:
                    orderidx = key.replace('main-menu-', '').replace('wireframe-', '')
                    display_name, created = self.NamesModel.objects.using(
                        self.project.project_connection_name).get_or_create(english=data['wireframe_main_name'],
                                                                            text_table_category=names_text_category)
                    if created:
                        display_name, created = self.NamesModel.objects.using(
                            self.project.project_connection_name).get_or_create(english=data['wireframe_main_name'],
                                                                                text_table_category=names_text_category)

                    if display_name:
                        if saved_template:
                            parent_wire_frame_parent = saved_template

                        else:
                            parent_wire_frame_parent, created = self.WireframesModel.objects.using(
                                self.project.project_connection_name).get_or_create(orderidx=1,
                                                                                    name=data['wireframe_main_name'],
                                                                                    display_name=display_name,
                                                                                    wireframe_parent__isnull=True)
                            if created:
                                parent_wire_frame_parent, created = self.WireframesModel.objects.using(
                                    self.project.project_connection_name).get_or_create(orderidx=1,
                                                                                        name=data[
                                                                                            'wireframe_main_name'],
                                                                                        display_name=display_name,
                                                                                        wireframe_parent__isnull=True)
                        if parent_wire_frame_parent:
                            update_dict[orderidx] = {}
                            update_dict[orderidx]['order'] = orderidx
                            update_dict[orderidx]['name'] = value

                            self.create_asset_tag(value)

                            display_name, created = self.NamesModel.objects.using(
                                self.project.project_connection_name).get_or_create(english=value,
                                                                                    text_table_category=names_text_category)
                            if created:
                                display_name, created = self.NamesModel.objects.using(
                                    self.project.project_connection_name).get_or_create(english=value,
                                                                                        text_table_category=names_text_category)

                            update_dict[orderidx]['display_name'] = display_name

                            new_wire_frame_parent, created = self.WireframesModel.objects.using(
                                self.project.project_connection_name).get_or_create(
                                wireframe_parent=parent_wire_frame_parent,
                                orderidx=orderidx,
                                name=value,
                                display_name=display_name)

                            if created:
                                new_wire_frame_parent, created = self.WireframesModel.objects.using(
                                    self.project.project_connection_name).get_or_create(
                                    wireframe_parent=parent_wire_frame_parent,
                                    orderidx=orderidx,
                                    name=value,
                                    display_name=display_name)

                            sub_id_list = []
                            update_dict = {}
                            for i_key, i_value in data.items():

                                wireframes_key = "wireframe-%s" % orderidx
                                if wireframes_key in i_key and new_wire_frame_parent:

                                    try:
                                        order_sub_orderidx = i_key[-2:]
                                        if "-" in order_sub_orderidx:
                                            order_sub_orderidx = i_key[-1:]

                                        order_sub_orderidx = int(order_sub_orderidx)

                                    except:
                                        order_sub_orderidx = i_key[-1:]

                                    if order_sub_orderidx not in sub_id_list:
                                        sub_id_list.append(order_sub_orderidx)

                                    update_dict[order_sub_orderidx] = {}
                                    update_dict[order_sub_orderidx]['wireframe_parent'] = new_wire_frame_parent

                                    data_key = "wireframe-%s-sub-menu-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['sub_name'] = data[data_key]

                                    data_key = "wireframe-%s-template-%s" % (orderidx, order_sub_orderidx)
                                    if data_key in data:
                                        update_dict[order_sub_orderidx]['template'] = data[data_key]

                            for id in sub_id_list:
                                self.create_asset_tag(update_dict[id]['sub_name'])

                                display_name, created = self.NamesModel.objects.using(
                                    self.project.project_connection_name).get_or_create(
                                    english=update_dict[id]['sub_name'], text_table_category=names_text_category)
                                if created:
                                    display_name, created = self.NamesModel.objects.using(
                                        self.project.project_connection_name).get_or_create(
                                        english=update_dict[id]['sub_name'], text_table_category=names_text_category)

                                new_wire_frame_child, created = self.model_object("Wireframes").get_or_create(orderidx=id,
                                                                                              name=update_dict[id][
                                                                                                  'sub_name'],
                                                                                              template_id=
                                                                                              update_dict[id][
                                                                                                  'template'],
                                                                                              display_name=display_name,
                                                                                              wireframe_parent=new_wire_frame_parent)
        if parent_wire_frame_parent:
            return parent_wire_frame_parent

    def check_and_delete_wireframes_assets(self, all_wire_ids, data):
        deleted_menus = []
        changed_template_wireframes = []
        for pid in all_wire_ids:
            children_menus = self.model_object("Wireframes").filter(wireframe_parent_id=pid).values_list('pk',
                                                                                                         flat=True)
            if not "section-order-%s" % pid in data or not data["section-order-%s" % pid]:
                deleted_menus.append(pid)
                for cid in children_menus:
                    deleted_menus.append(cid)

            for cid in children_menus:
                children_template = self.model_object("Wireframes").filter(pk=cid).values_list('template_id', flat=True)

                if not "wire-list-order-%s" % cid in data or not data["wire-list-order-%s" % cid]:
                    deleted_menus.append(pid)

                if children_template:
                    template_lookup = "wireframe-%s-template-%s" % (pid, cid)
                    if template_lookup in data and not int(data[template_lookup]) == int(children_template[0]):
                        changed_template_wireframes.append(cid)

        print('deleted_menus', deleted_menus)
        for wireframe_id in deleted_menus:
            controllers = self.model_object("WireframeControllers").filter(wireframe_id=wireframe_id)
            self.model_object("ContainerAssignedAssets").filter(wireframe_id=wireframe_id).delete()

            for controller in controllers:
                self.model_object("CanvasWireframes").filter(wireframe_id=wireframe_id).delete()
                self.model_object("Content").filter(wireframe_id=wireframe_id).delete()
                self.model_object("ContainerAssignedAssets").filter(template_id=controller.template_id).delete()
                self.model_object("WireframeControllerContainers").filter(
                    wireframe_controller_id=controller.pk).delete()

            controllers.delete()

        for wireframe_id in changed_template_wireframes:
            self.model_object("ContainerAssignedAssets").filter(wireframe_id=wireframe_id).delete()
            self.model_object("Content").filter(wireframe_id=wireframe_id).delete()