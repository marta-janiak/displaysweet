from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class TextTableCategories(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(TextTableCategories, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_text_table_category(self, text_category):
        try:
            property_typical_string, created = self.model_object("TextTableCategories").get_or_create(category=text_category)
            return property_typical_string
        except Exception as e:
            print('get_text_table_category error', e)

    def check_category_string(self, key, value):

        if value == settings.FLOOR_PLAN_TYPICAL:
            category_queryset = self.model_object("Texttablecategories").filter(category=settings.FLOOR_PLAN_TYPICAL)
            if not category_queryset:
                self.model_object("Texttablecategories").create(category=settings.FLOOR_PLAN_TYPICAL)

                category_queryset = self.model_object("Texttablecategories").filter(
                    category=settings.FLOOR_PLAN_TYPICAL)

            category = category_queryset[0]
            strings_queryset = self.model_object('Strings').filter(text_table_category=category,
                                                                   english=value)

            if strings_queryset:
                type_string = strings_queryset[0]
                return type_string

        table_category_mapping = self.get_mapping_table_value(key)
        category_queryset = self.model_object("Texttablecategories").filter(category=table_category_mapping)

        if category_queryset:
            category = category_queryset[0]

            queryset = self.model_object("Strings").filter(text_table_category=category, english=value)

            if queryset:
                return queryset[0]
            else:

                self.model_object("Strings").create(text_table_category=category, english=value)
                item_created = self.model_object("Strings").filter(text_table_category=category, english=value)
                item_created = item_created[0]
                return item_created

    def get_or_create_string_category(self, string_category_field):
        category = self.objects.filter(category=string_category_field)
        if not category:
            self.objects.create(category=string_category_field)
            category = self.objects.filter(category=string_category_field)

        return category[0]


