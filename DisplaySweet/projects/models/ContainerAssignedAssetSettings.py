from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class ContainerAssignedAssetSettings(PrimaryModelMixin, object):
    """
    """
    class_name = None
    project = None

    def __init__(self, project):
        super(ContainerAssignedAssetSettings, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_image_settings(self, container_asset, canvas_id=None, template_id=None, wireframe_id=None):

        asset_settings = self.model_object("ContainerAssignedAssetSettings").filter(
            container_assigned_asset=container_asset)

        if canvas_id:
            asset_settings = asset_settings.filter(container_assigned_asset__canvas_id=canvas_id)
        if template_id:
            asset_settings = asset_settings.filter(container_assigned_asset__template_id=template_id)
        if wireframe_id:
            asset_settings = asset_settings.filter(container_assigned_asset__wireframe_id=wireframe_id)

        image_settings = {}
        for asset in asset_settings:
            image_settings.update({asset.setting_string: asset.value})

        return image_settings