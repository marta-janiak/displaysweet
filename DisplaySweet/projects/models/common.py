from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
import os
import time
from core.models import Project


class BaseModelMixin(object):
    """
    The variables and minor functions common to all model mixins.
    """

    def __init__(self, request=None, project_id=None, user=None, project=None):
        super(BaseModelMixin, self).__init__()

        self.project_id = project_id
        self.user = user
        self.project = project
        self.request = request

    def process_project_details(self, project_id=None, user=None):

        try:
            self.request.session['project_id'] = project_id
            self.project = Project.objects.get(pk=project_id)
            print(self.project)
            return self.project
        except:
            if project_id:
                project = Project.objects.get(pk=project_id)
                self.project = project
                self.project_id = project_id
                try:
                    self.request.session['project_id'] = project.pk
                except:
                    pass
            else:
                self.project = Project.objects.all().order_by('id')[0]

        print(self.project)
        if not self.project and user:
            projects = Project.objects.filter(users=user)
            if projects:
                project = projects[0]
                self.project = project
                self.project_id = project.pk

        print(self.project)
        return self.project

    def set_model(self, model_name):
        model = apps.get_model(self.project.project_connection_name, model_name)
        return model

    def get_objects(self, class_name):
        class_object = apps.get_model(self.project.project_connection_name, class_name)
        model_objects = class_object.objects.using(self.project.project_connection_name)
        return model_objects

    def get_class_object(self, model_name):
        project_model_object = apps.get_model(self.project.project_connection_name, model_name)
        self.project_model_object = project_model_object
        self.model_name = model_name
        return project_model_object

    def model_object(self, model_name=None):

        if not model_name:
            model_name = self.model_name

        self.set_model(model_name)

        class_object = self.get_class_object(model_name)
        model_objects = class_object.objects.using(self.project.project_connection_name)
        return model_objects

    def get_class(self, class_name):
        m = __import__(class_name, globals(), locals(), [class_name])
        found_class = getattr(m, class_name)
        class_instance = found_class(self.project)
        return class_instance
