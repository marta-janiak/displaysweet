from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Assets(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Assets, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def save_asset_settings(self, data, asset, assigned_asset, resource_id=None):

        order = 1
        if resource_id:
            asset = resource_id

        if "file-order-%s" % asset in data:
            order = data["file-order-%s" % asset]

        order_setting = self.model_object("ContainerAssignedAssetSettings").filter(
            container_assigned_asset=assigned_asset[0],
            setting_string="order")

        if not order_setting:
            self.model_object("ContainerAssignedAssetSettings").create(container_assigned_asset=assigned_asset[0],
                                                                       setting_string="order",
                                                                       value=order)
        else:
            for file_order in order_setting:
                file_order.value = order
                file_order.save(using=self.project.project_connection_name)

        if "file-caption-%s" % asset in data:
            caption = data["file-caption-%s" % asset]

            order_setting = self.model_object("ContainerAssignedAssetSettings").filter(
                container_assigned_asset=assigned_asset[0],
                setting_string="caption")
            if not order_setting:
                self.model_object("ContainerAssignedAssetSettings").create(container_assigned_asset=assigned_asset[0],
                                                                           setting_string="caption",
                                                                           value=caption)
            else:
                watermark_setting = order_setting[0]
                watermark_setting.value = caption
                watermark_setting.save(using=self.project.project_connection_name)

        if "file-menu-%s" % asset in data:
            menu = data["file-menu-%s" % asset]

            new_setting = self.model_object("ContainerAssignedAssetSettings").filter(
                container_assigned_asset=assigned_asset[0],
                setting_string="menu")
            if not new_setting:
                self.model_object("ContainerAssignedAssetSettings").create(container_assigned_asset=assigned_asset[0],
                                                                           setting_string="menu",
                                                                           value=menu)
            else:
                new_setting = new_setting[0]
                new_setting.value = menu
                new_setting.save(using=self.project.project_connection_name)

        watermarks = []
        if "selected-asset-watermark" in data:
            watermarks = data.getlist('selected-asset-watermark')

        if str(asset) in watermarks:
            has_watermark = True
        else:
            has_watermark = False

        watermark_setting = self.model_object("ContainerAssignedAssetSettings").filter(
            container_assigned_asset=assigned_asset[0],
            setting_string="watermark")
        if not watermark_setting:
            self.model_object("ContainerAssignedAssetSettings").create(container_assigned_asset=assigned_asset[0],
                                                                       setting_string="watermark",
                                                                       value=has_watermark)
        else:
            watermark_setting = watermark_setting[0]
            watermark_setting.value = has_watermark
            watermark_setting.save(using=self.project.project_connection_name)

    def add_new_image_asset(self, image_file, folder_path):

        file_name = image_file.name.replace(" ", "_", 10)

        text_category, created = self.model_object("TextTableCategories").get_or_create(category="Asset")
        description, created = self.model_object("Descriptions").get_or_create(english='Images', text_table_category_id=text_category.pk)
        type_string, created = self.model_object("Strings").get_or_create(english='Images', text_table_category_id=text_category.pk)

        path_to_save = os.path.join(folder_path, file_name)

        new_resource, created = self.model_object("Resources").get_or_create(description_id=description.pk,
                                                                             size=1,
                                                                             path=path_to_save)
        if not new_resource.pk:
            new_resource = self.model_object("Resources").filter(description_id=description.pk,
                                                                 size=1,
                                                                 path=path_to_save)[0]

        asset_type, created = self.model_object("AssetTypes").get_or_create(type_string=type_string)
        new_asset = self.model_object("Assets").filter(asset_type=asset_type, source_id=new_resource.pk)

        if not new_asset:
            self.model_object("Assets").create(asset_type=asset_type,
                                               source_id=new_resource.pk)

            new_asset = self.model_object("Assets").filter(asset_type=asset_type,
                                                           source_id=new_resource.pk)

        if new_asset:
            return new_asset[0]

    def add_new_image_asset_resource(self, resource_id):
        text_category, created = self.model_object("TextTableCategories").get_or_create(category="Asset")
        type_string, created = self.model_object("Strings").get_or_create(english='Images',
                                                                          text_table_category_id=text_category.pk)

        asset_type, created = self.model_object("AssetTypes").get_or_create(type_string=type_string)

        new_asset, created = self.model_object("Assets").get_or_create(asset_type=asset_type,
                                                                       source_id=resource_id)
        return new_asset

    def save_asset_to_plans_container(self, page_type, asset_id, canvas_id, wireframe_id, container_id=None):

        container_for_page = None
        if not container_id:
            container_type_for_page = self.model_object("ContainerTypes").filter(type_string__english=page_type)

            if not container_type_for_page:
                container_type_string = self.model_object("Strings").filter(english=page_type)
                if not container_type_string:
                    self.model_object("Strings").create(english=page_type)
                    container_type_string = self.model_object("Strings").filter(english=page_type)

                if container_type_string:
                    container_type_string = container_type_string[0]
                    self.model_object("ContainerTypes").create(type_string=container_type_string)
                    container_type_for_page = self.model_object("ContainerTypes").filter(type_string__english=page_type)

            if container_type_for_page:
                # print ('have container type'
                container_type_for_page = container_type_for_page[0]
                # print ('container_type_for_page', container_type_for_page

                container_for_page = self.model_object("Containers").filter(container_type_id=container_type_for_page.pk)

                if not container_for_page:
                    container_name = self.model_object("Names").filter(english=page_type)
                    if not container_name:
                        self.model_object("Names").create(english=page_type)
                        container_name = self.model_object("Names").filter(english=page_type)

                    self.model_object("Containers").create(container_type_id=container_type_for_page.pk,
                                                           container_name=container_name[0],
                                                           orderidx=1)

                    container_for_page = self.model_object("Containers").filter(container_type_id=container_type_for_page.pk)

        else:
            container_for_page = self.model_object("Containers").filter(pk=container_id)

        if container_for_page:
            container_for_page = container_for_page[0]

            #print (1

            template = self.model_object("Templates").filter(name__iexact=page_type)
            #print (2

            if template:
                template_id = template[0].pk
                #print (3

                if not wireframe_id:
                    #print (31

                    wireframes = self.model_object("Wireframes").filter(template__template_type__name=page_type)
                    #print (32

                    for wireframe in wireframes:
                        # print (33
                        asset_created = self.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                            template_id=template_id,
                                                                                            wireframe_id=wireframe.pk,
                                                                                            asset_id=asset_id)

                        if not asset_created:
                            try:
                                self.model_object("ContainerAssignedAssets").create(canvas_id=canvas_id,
                                                                                    template_id=template_id,
                                                                                    wireframe_id=wireframe.pk,
                                                                                    asset_id=asset_id,
                                                                                    container=container_for_page,
                                                                                    orderidx=1)

                            except Exception as e:
                                print ('Error saving container assigned asset', e)

                else:
                    asset_created = self.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                        template_id=template_id,
                                                                                        wireframe_id=wireframe_id,
                                                                                        asset_id=asset_id)

                    if not asset_created:
                        try:
                            self.model_object("ContainerAssignedAssets").create(canvas_id=canvas_id,
                                                                                template_id=template_id,
                                                                                wireframe_id=wireframe_id,
                                                                                asset_id=asset_id,
                                                                                container=container_for_page,
                                                                                orderidx=1)

                            asset_created = self.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                                template_id=template_id,
                                                                                                wireframe_id=wireframe_id,
                                                                                                asset_id=asset_id)
                        except Exception as e:
                            print ('error saving container assigned asset', e)

                    return asset_created

    def save_resource_to_asset_plans_container(self, page_type, resource_id, canvas_id, wireframe_id, container_id=None):
        asset = self.model_object("Assets").filter(source_id=resource_id)
        asset_type = self.save_asset_type_id()

        if asset_type and not asset:
            self.model_object("Assets").create(source_id=resource_id, asset_type=asset_type)
            asset = self.model_object("Assets").filter(source_id=resource_id)

        asset_id = asset[0].pk

        if container_id:
            image = self.save_asset_to_plans_container(page_type, asset_id, canvas_id, wireframe_id, container_id=container_id)
            return image
        else:
            image = self.save_asset_to_plans_container(page_type, asset_id, canvas_id, wireframe_id)
            return image

    def get_image_list_for_tags(self):

        assets = self.model_object("Assets").all()
        image_list = []
        for asset in assets:
            image = self.model_object("Resources").filter(pk=asset.source_id)

            if image:
                image = image[0]

                tags = self.get_image_asset_tags(image)
                image_name = image.path.split("/")[-1]
                extension = image_name[-3:]

                image_static_path = '/static/%s/thumbnails/%s' % (self.project.project_connection_name, image_name)
                static_file_path = '/static/%s/thumbnails/%s' % (self.project.project_connection_name, image_name)
                file_type = 'image'

                if extension in self.sound_files():
                    image_static_path = '/static/common/img/musical-notes.png'
                    static_file_path = '/static/%s/media/%s' % (self.project.project_connection_name, image_name)
                    file_type = 'sound'
                if extension in self.video_files():
                    image_static_path = '/static/common/img/movie.png'
                    static_file_path = '/static/%s/media/%s' % (self.project.project_connection_name, image_name)
                    file_type = 'video'

                if extension in self.pdf_files():
                    image_static_path = '/static/common/img/pdf.jpg'
                    static_file_path = '/static/%s/media/%s' % (self.project.project_connection_name, image_name)
                    file_type = 'pdf'

                full_image_name = image_name
                if len(image_name) > 20:
                    try:
                        image_name = str(image_name)[:20] + '...'
                    except:
                        pass

                image_list.append({'image': image,
                                   'asset': asset,
                                   'image_name': image_name,
                                   'full_image_name': full_image_name,
                                   'tags': tags,
                                   'image_static_path': image_static_path,
                                   'static_file_path': static_file_path,
                                   'file_type': file_type})

        try:
            image_list = list(sorted(image_list, key=lambda k: str(k['full_image_name']).lower()))
        except Exception as e:
            print(e)

        return image_list

    def get_image_asset_tags(self, image):
        tag_list = []
        text_category = self.model_object("TextTableCategories").filter(category="Asset")

        if text_category:
            type_string = self.model_object("Strings").filter(english='Images',
                                                              text_table_category_id=text_category[0].pk)

            if type_string:
                asset_type = self.model_object("AssetTypes").filter(type_string=type_string[0])
                if asset_type:
                    asset = self.model_object("Assets").filter(source_id=image.pk, asset_type=asset_type[0])

                    if asset:
                        assets_tagged = self.model_object("AssetsTagged").filter(asset=asset[0])

                        if assets_tagged:
                            for tag in assets_tagged:
                                asset_tag = self.model_object("AssetTags").filter(pk=tag.asset_tag_id)
                                if asset_tag:
                                    tag_name = str(asset_tag[0].tag).lstrip().rstrip()
                                    tag_list.append({'id': tag.asset_tag_id, 'tag': tag_name})

        return tag_list

    def save_asset_type_id(self):

        text_category = self.model_object("TextTableCategories").filter(category="Asset")
        if not text_category:
            self.model_object("TextTableCategories").create(category="Asset")
            text_category = self.model_object("TextTableCategories").filter(category="Asset")

        if text_category:
            type_string = self.model_object("Strings").filter(english='Images',
                                                              text_table_category_id=text_category[0].pk)

            if type_string:
                asset_type = self.model_object("AssetTypes").filter(type_string=type_string[0])

                if not asset_type:
                    self.model_object("AssetTypes").create(type_string=type_string[0])
                    asset_type = self.model_object("AssetTypes").filter(type_string=type_string[0])

                return asset_type[0]

    def save_image_asset_tags(self, resource_id, tag_name, direction='Save'):
        check_tag = self.model_object("AssetTags").filter(tag=tag_name)

        asset_type = self.save_asset_type_id()

        if asset_type:
            asset = self.model_object("Assets").filter(source_id=resource_id, asset_type=asset_type)
            if not asset:
                self.model_object("Assets").create(source_id=resource_id, asset_type=asset_type)
                asset = self.model_object("Assets").filter(source_id=resource_id, asset_type=asset_type)

            if asset:
                assets_tagged = self.model_object("AssetsTagged").filter(asset=asset[0], asset_tag=check_tag[0])
                if direction == "Save":
                    if not assets_tagged:
                        self.model_object("AssetsTagged").create(asset=asset[0], asset_tag=check_tag[0])
                else:
                    if assets_tagged:
                        assets_tagged.delete()