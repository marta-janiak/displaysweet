from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
import shutil
from .model_mixin import PrimaryModelMixin
import json


class LevelPlans(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(LevelPlans, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return None

    def get_level_plan_details(self, floor_id=None, request=None):
        from core.models import ProjectCSVTemplate

        building = self.klass("Buildings").get_all_buildings(return_first=True)

        floor_id = None
        selected_floor = None
        building_id = building.pk
        apartments_dict = []
        apartment_list = []
        linked_hotspots = []
        floor_list = []
        all_floors = []
        selected_floors = None

        floors = self.klass("Floors").get_all_floors(building_id=building.pk)
        floor_select_list = []

        self.building_id = building_id
        
        if selected_floors:
            for floor in floors:
                if floor.pk in selected_floors:
                    all_floors.append(floor)
        else:
            all_floors = floors

        for floor in all_floors:
            floors_buildings = self.model_object("FloorsBuildings").filter(floor_id=floor.pk).values_list('building_id', flat=True)
            floors_buildings = self.model_object("Buildings").filter(pk__in=floors_buildings)

            floor_list.append(
                {'floor': floor,
                 'name': self.klass("Names").get_names_instance(floor.name_id),
                 'level_plan_image': self.klass("Floors").get_floor_level_image(floor.pk),
                 'assigned_buildings': floors_buildings,
                 }
            )

        static_image = '/static/common/img/placeholder.jpg'
        image_path = None
        image_name = 'n/a'
        
        if self.project.has_apartment_string_fields():
            level_image = self.get_floor_level_plan(floor_id)
            if level_image:
                image_path = level_image.path
                image_name = image_path.split("/")[-1]

        image_exists = None
        try:
            check_path = os.path.join(settings.BASE_IMAGE_LOCATION, image_path)
            check_path = check_path.replace("//", "/")
            if os.path.isfile(check_path):
                image_exists = True

                static_image = '/static/' + str(image_path.replace(settings.BASE_IMAGE_LOCATION, ''))
        except:
            pass

        context = {
            'project': self.project,
            'selected_floor': selected_floor,
            'has_hot_spot_configuration': True,
            'floors': True,
            'image_path': image_path,
            'image_name': image_name,
            'static_image': static_image,
            'image_exists': image_exists,
            'all_floors': all_floors,
            'property_type_list': self.model_object("PropertyTypes").all(),
            'floor_id': floor_id,
            'property_typical_image_list' : self.klass("Properties").get_all_property_typicals(),
            'first_apartment_id': self.model_object("Properties").all()[0].pk,
            'property_aspects': self.klass("PropertyAspects").get_keys(),
            'first_template': self.klass("EditableTemplate").get_first_editable_template(),
            'buildings': self.klass("Buildings").get_all_buildings(),
            'all_buildings': self.model_object("Buildings").all().order_by('name__english'),
            'building_id': building_id,
            'floor_select_list': floor_select_list,
            'apartments_dict': apartments_dict,
            'linked_hotspots': linked_hotspots,
            'apartment_list': json.dumps(apartment_list),
            'floor_list': floor_list,
            'editable_templates': ProjectCSVTemplate.objects.filter(project=self.project,
                                                                    is_editable_template=True).order_by('id')
        }

        return context

    def get_floor_level_plan(self, floor_id):
        plans = self.model_object("FloorsPlans").filter(floor_id=floor_id, key="level_plans").values_list("resource_id",
                                                                                                          flat=True)

        images = self.model_object("Resources").filter(pk=plans)
        if images:
            plan = images[0]
            return plan

    def get_floor_level_plan_image_name(self, floor, mode, output):
        image = self.get_floor_level_plan_image(floor, mode, output)
        if image:
            image_path = image.path
            image_path_split = image_path.split("/")
            image_name = image_path_split[-1]

            return image_name

    def get_floor_level_plan_image(self, floor, mode, output):

        if floor:
            image_type = self.klass("ImageTypes").get_image_type_id('level_plans', mode, output)            
            queryset = self.model_object("Floorsplans").filter(floor_id=floor.pk, image_type=image_type)

            if queryset:
                floor_plan = queryset[0]
                queryset = self.model_object("Resources").filter(id=floor_plan.resource_id)
                return queryset[0]

        return None

    def save_floor_and_key_plan_image(self, property_id=None, canvas_id=None):

        text_category = self.model_object("TextTableCategories").filter(category='Image Types')
        if not text_category:
            self.model_object("TextTableCategories").create(category='Image Types')
            text_category = self.model_object("TextTableCategories").filter(category='Image Types')

        description = self.model_object("Descriptions").filter(text_table_category=text_category,
                                                               english="Images")

        base_level_image = settings.PROJECT_ROOT + '/static/common/img/UI_placeholder_floorplan.jpg'
        copy_to = '%s%s//UI_placeholder_floorplan.jpg' % (
        settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)

        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except:
                pass

        copy_to = '%s%s/thumbnails/UI_placeholder_floorplan.jpg' % (
        settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except Exception as e:
                print (e)

        resource_id = None
        resource = self.model_object("Resources").filter(description=description[0], path=copy_to, size=0)
        if resource:
            resource_id = resource[0].pk

        if not resource:
            self.model_object("Resources").create(description=description[0], path=copy_to, size=0)
            resource = self.model_object("Resources").filter(description=description[0], path=copy_to)

            if resource:
                resource_id = resource[0].pk

        canvases = self.model_object("Canvases").all()
        if resource_id:
            # for canvas in canvases:
            #     self.klass("Assets").save_resource_to_asset_plans_container("Floor Plans",
            #                                                                 resource_id,
            #                                                                 canvas.pk,
            #                                                                 None)

            check_plan = self.model_object("PropertyResources").filter(key="floor_plans",
                                                                       property_id=property_id)
            if not check_plan:
                self.model_object("PropertyResources").create(key="floor_plans",
                                                              property_id=property_id,
                                                              resource_id=resource_id)

        base_level_image = settings.PROJECT_ROOT + '/static/common/img/UI_placeholder_keyplan.jpg'
        copy_to = '%s%s/UI_placeholder_keyplan.jpg' % (
        settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)

        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except Exception as e:
                print (e)

        copy_to = '%s%s/thumbnails/UI_placeholder_keyplan.jpg' % (
        settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except Exception as e:
                print (e)

        resource = self.model_object("Resources").filter(description=description[0], path=copy_to, size=0)
        if not resource:
            self.model_object("Resources").create(description=description[0], path=copy_to, size=0)
            resource = self.model_object("Resources").filter(description=description[0], path=copy_to)
            if resource:
                resource_id = resource[0].pk

        if resource_id:
            # canvases = self.model_object("Canvases").all()
            # for canvas in canvases:
            #     self.klass("Assets").save_resource_to_asset_plans_container("Key Plans",
            #                                                                 resource_id,
            #                                                                 canvas.pk,
            #                                                                 None)

            check_plan = self.model_object("PropertyResources").filter(key="key_plans",
                                                                       property_id=property_id)
            if not check_plan:
                self.model_object("PropertyResources").create(key="key_plans",
                                                              property_id=property_id,
                                                              resource_id=resource_id)

    def save_level_plan_image(self, floor_id, canvas_id=None):

        text_category = self.model_object("TextTableCategories").filter(category='Image Types')

        if not text_category:
            self.model_object("TextTableCategories").create(category='Image Types')
            text_category = self.model_object("TextTableCategories").filter(category='Image Types')

        text_category = text_category[0]

        description = self.model_object("Descriptions").filter(text_table_category=text_category,
                                                               english="Images")

        if not description:
            self.model_object("Descriptions").create(text_table_category=text_category,
                                                     english="Images")

            description = self.model_object("Descriptions").filter(text_table_category=text_category,
                                                                   english="Images")

        base_level_image = settings.PROJECT_ROOT + '/static/common/img/UI_placeholder_levelplan.jpg'

        path = '%s%s/' % (settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if not os.path.exists(path):
            os.makedirs(path)

        copy_to = '%s%s/UI_placeholder_levelplan.jpg' % (
            settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except:
                pass


        path = '%s%s/thumbnails/' % (settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if not os.path.exists(path):
            os.makedirs(path)

        copy_to = '%s%s/thumbnails/UI_placeholder_levelplan.jpg' % (
            settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)
        if os.path.isfile(base_level_image):
            try:
                shutil.copy2(base_level_image, copy_to)
            except:
                pass

        resource = self.model_object("Resources").filter(description=description[0], path=copy_to, size=0)
        if not resource:
            self.model_object("Resources").create(description=description[0], path=copy_to, size=0)
            resource = self.model_object("Resources").filter(description=description[0], path=copy_to)

            canvases = self.model_object("Canvases").all()
            resource_id = resource[0].pk

            # for canvas in canvases:
            #     self.klass("Assets").save_resource_to_asset_plans_container("Level Plans", resource_id, canvas.pk, None)

        resource_id = resource[0].pk

        check_floor = self.model_object("FloorsPlans").filter(key="level_plans",
                                                              floor_id=floor_id)

        if not check_floor:
            self.model_object("FloorsPlans").create(key="level_plans",
                                                    floor_id=floor_id,
                                                    resource_id=resource_id)

        # if canvas_id:
        #     self.klass("Assets").save_resource_to_asset_plans_container("Level Plans", resource_id, canvas_id, None)
