from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
import datetime
import boto
import hashlib
from PIL import Image
from .model_mixin import PrimaryModelMixin
from django.conf import settings

import boto
import shutil
import hashlib
import datetime
from boto.s3.connection import OrdinaryCallingFormat



class Canvases(PrimaryModelMixin, object):
    """
    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Canvases, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_or_create_display_name_instance(self, value):
        display_name, created = self.model_object("Names").get_or_create(english=value)
        if display_name:
            return self.model_object("Names").filter(english=value)[0]

    def save_canvas_name(self, canvas_name, selected_canvas=None):
        if selected_canvas:
            canvas_instance = self.model_object("Canvases").get(pk=selected_canvas.pk)
            self.model_object("Names").filter(pk=canvas_instance.canvas_name_id).update(english=canvas_name)
            return selected_canvas
        else:
            canvas_name = self.get_or_create_display_name_instance(canvas_name)
            self.model_object("Canvases").create(canvas_name=canvas_name)
            return self.objects.filter(canvas_name=canvas_name)[0]

    def get_canvas_publish_display(self, selected_canvas):
        main_wireframe_controller = None
        wire_parent_id = None

        menu_publish_list = []

        if selected_canvas:
            canvas_name = \
            self.objects.filter(pk=selected_canvas.pk).values_list('canvas_name__english', flat=True)[0]
            canvas_name = canvas_name.strip().replace(" ", "_", 10).lower()
            canvas_wireframes = self.model_object("CanvasWireframes").filter(canvas=selected_canvas)

            if canvas_wireframes:
                canvas_wireframes = canvas_wireframes[0]
                model_dict = model_to_dict(canvas_wireframes)
                wire_parent_id = model_dict['wireframe']

                menus = self.model_object("Wireframes").filter(wireframe_parent_id=wire_parent_id).order_by('orderidx')

                for menu in menus:
                    list_items = self.model_object("Wireframes").filter(wireframe_parent=menu).order_by('orderidx')

                    menu_item_list = []
                    for item in list_items:
                        wireframe_name = \
                        self.model_object("Wireframes").filter(pk=item.pk).values_list('name', flat=True)[0]
                        wireframe_name = wireframe_name.lstrip().rstrip()
                        wireframe_name = wireframe_name.replace(" ", "_", 10).lower()

                        dict_item = model_to_dict(item)

                        t_template = self.model_object("Templates").filter(pk=item.template_id)
                        if t_template:
                            t_template = t_template[0]
                            template_name = t_template.name

                            containers = []
                            template_types = self.model_object("TemplateTypeContainers").filter(
                                template_type_id=t_template.template_type_id)
                            for ttype in template_types:
                                containers.append(ttype.container_id)

                            children_containers = self.model_object("Containers").filter(
                                container_parent_id__in=containers)
                            for c in children_containers:
                                containers.append(c.pk)

                            main_wireframe_controller = self.model_object("WireframeControllers").filter(
                                wireframe_id=wire_parent_id,
                                template_id=item.template_id,
                                canvas_id=selected_canvas.pk)

                            if main_wireframe_controller:
                                main_wireframe_controller = main_wireframe_controller[0]

                                template_containers = self.model_object("WireframeControllerContainers").filter(
                                    wireframe_controller=main_wireframe_controller).values_list('container_id',
                                                                                                flat=True).distinct()

                                show_image_path = self.return_canvas_save_path(self.project.project_connection_name,
                                                                               canvas_name, wireframe_name)[:-1]

                                assets = self.model_object("ContainerAssignedAssets").filter(canvas_id=selected_canvas.pk,
                                                                                             template_id=t_template.pk,
                                                                                             wireframe_id=item.pk)

                                assets = assets.values_list('asset__source_id', flat=True)
                                resources = self.model_object("Resources").filter(pk__in=assets)

                                container_resources = []
                                for resource in resources:
                                    resource_name = resource.path.split("/")[-1]
                                    publish_link = show_image_path + '/thumbnails/' + resource_name

                                    resource_dimensions = self.get_published_resource_dimensions(resource, publish_link)

                                    static_link = 'http://services.displaysweet.com/get_resource.php?preview_path=%s' % publish_link
                                    static_link = static_link.replace("//", "/")

                                    container_resources.append({'image': resource, 'publish_link': publish_link,
                                                                'resource_dimensions': resource_dimensions,
                                                                'type': 'thumbnail',
                                                                'show_static': static_link})

                                dict_item.update({'template_name': template_name,
                                                  'template_id': item.template_id,
                                                  'assets': resources,
                                                  'asset_count': len(resources),
                                                  'wireframe_id': item.pk,
                                                  'wire_parent_id': item.wireframe_parent_id,
                                                  'show_image_path': show_image_path,
                                                  'canvas_id': selected_canvas.pk,
                                                  'main_wireframe_controller': main_wireframe_controller,
                                                  'container_ids': template_containers,
                                                  'container_resources': container_resources,
                                                  'thumbnail_image_display_dict': self.return_container_objects_thumbnail_resolution(item.template_id, selected_canvas.pk),
                                                  'default_image_display_dict': self.return_container_objects_default_resolution(item.template_id, selected_canvas.pk)})

                                menu_item_list.append(dict_item)

                    menu_publish_list.append({'menu': menu, 'publish_items': menu_item_list})

        return menu_publish_list, main_wireframe_controller, wire_parent_id

    def return_container_objects_thumbnail_resolution(self, template_id, canvas_id):

        wire_frames = self.model_object("Wireframes").all()
        wire_frame = wire_frames[0]
        selected_template = self.model_object("Templates").filter(pk=template_id)[0]

        model_mixin = PrimaryModelMixin(request=self.request, project_id=self.project_id)
        cmodel_object = model_mixin.launch(project_id=self.project.pk)

        template_container_list = cmodel_object("Containers").get_template_container_settings(selected_template,
                                                                                              canvas_id,
                                                                                              wire_frame.pk)

        thumbnail_portrait_width = 0
        thumbnail_portrait_height = 0
        thumbnail_landscape_width = 0
        thumbnail_landscape_height = 0
        has_thumbnails = False

        for item in template_container_list:
            if item['container_name'] == "Thumbnail Details":
                has_thumbnails = True
                for children in item['settings']:
                    title = model_to_dict(children['csettings'])
                    title = title['title']

                    if title == "Portrait":
                        for field in children['container_children']:
                            label = model_to_dict(field['project_container'])
                            label = label['string_field']
                            value = field['check_container_string_field_value']

                            if label == "Height":
                                thumbnail_portrait_height = value

                            if label == "Width":
                                thumbnail_portrait_width = value

                    if title == "Landscape":
                        for field in children['container_children']:
                            label = model_to_dict(field['project_container'])
                            label = label['string_field']
                            value = field['check_container_string_field_value']

                            if label == "Height":
                                thumbnail_landscape_height = value

                            if label == "Width":
                                thumbnail_landscape_width = value

        image_dict = {'has_thumbnails': has_thumbnails, 'thumbnail_portrait_height': thumbnail_portrait_height,
                      'thumbnail_portrait_width': thumbnail_portrait_width,
                      'thumbnail_landscape_width': thumbnail_landscape_width,
                      'thumbnail_landscape_height': thumbnail_landscape_height}

        return image_dict

    def return_container_objects_default_resolution(self, template_id, canvas_id):

        wire_frames = self.model_object("Wireframes").all()
        wire_frame = wire_frames[0]
        selected_template = self.model_object("Templates").filter(pk=template_id)[0]

        model_mixin = PrimaryModelMixin(request=self.request, project_id=self.project_id)
        cmodel_object = model_mixin.launch(project_id=self.project.pk)

        template_container_list = cmodel_object("Containers").get_template_container_settings(selected_template,
                                                                                               canvas_id, wire_frame.pk)

        default_portrait_height = 0
        default_portrait_width = 0
        default_landscape_height = 0
        default_landscape_width = 0

        for item in template_container_list:
            if item['container_name'] == "Default Source Image Settings":
                has_thumbnails = True
                for children in item['settings']:
                    title = model_to_dict(children['csettings'])
                    title = title['title']

                    if title == "Portrait":
                        for field in children['container_children']:
                            label = model_to_dict(field['project_container'])
                            label = label['string_field']
                            value = field['check_container_string_field_value']

                            if label == "Height":
                                default_portrait_height = value

                            if label == "Width":
                                default_portrait_width = value

                    if title == "Landscape":
                        for field in children['container_children']:
                            label = model_to_dict(field['project_container'])
                            label = label['string_field']
                            value = field['check_container_string_field_value']

                            if label == "Height":
                                default_landscape_height = value

                            if label == "Width":
                                default_landscape_width = value

        image_dict = {'default_landscape_height': default_landscape_height,
                      'default_landscape_width': default_landscape_width,
                      'default_portrait_width': default_portrait_width,
                      'default_portrait_height': default_portrait_height}

        return image_dict

    def get_published_resource_dimensions(self, resource, show_image_path):

        s3_location = settings.BASE_S3_IMAGE_LOCATION

        linked_location = os.path.join(s3_location, show_image_path)
        if os.path.isfile(linked_location):
            try:
                img = Image.open(linked_location)
                image_width, image_height = img.size
                return image_width, image_height
            except:
                pass

        return 'Cannot view image at %s ' % linked_location

    def return_canvas_save_path(self, project_name, canvas_name, template_name):

        project_name = project_name.replace(" ", "_", 10)

        creating_save_path = 'project_images/%s/staging/%s/%s/' % (
            project_name, canvas_name, template_name.strip().replace(" ", "_").lower())

        if self.project.project_publishing_path:

            if self.project.project_publishing_path[-1] == "/":
                creating_save_path = str(self.project.project_publishing_path) + '%s/%s/' % (
                canvas_name, template_name.strip().replace(" ", "_").lower())
            else:
                creating_save_path = str(self.project.project_publishing_path) + '/%s/%s/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())

        return creating_save_path

    def return_canvas_thumbnails_save_path(self, project_name, canvas_name, template_name):

        creating_save_path = 'project_images/%s/staging/%s/%s/thumbnails/' % (
        project_name, canvas_name, template_name.strip().replace(" ", "_", 10).lower())

        if self.project.project_publishing_path:
            if self.project.project_publishing_path[-1] == "/":
                creating_save_path = str(self.project.project_publishing_path) + '%s/%s/thumbnails/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())
            else:
                creating_save_path = str(self.project.project_publishing_path) + '/%s/%s/thumbnails/' % (
                    canvas_name, template_name.strip().replace(" ", "_").lower())

        return creating_save_path

    def pubish_canvas_template(self, wireframe_id, canvas_id, template_id, deliver=True,
                               user_id=None, skip_movies=False):

        print('pubish_canvas_template', self.project, self.project.project_connection_name)
        print('+++++++++')

        hashed_file_details = {}
        printed_screen_messages = "Publishing Template Message <br>"
        base_image_location = settings.BASE_S3_IMAGE_LOCATION
        wireframe_name = self.model_object("Wireframes").filter(pk=wireframe_id).values_list('name', flat=True)[0]
        wireframe_name = wireframe_name.lstrip().rstrip()
        wireframe_name = wireframe_name.replace(" ", "_", 10).lower()
        canvas_name = self.model_object("Canvases").filter(pk=canvas_id).values_list('canvas_name__english',
                                                                                     flat=True)[0]
        canvas_name = canvas_name.lstrip().rstrip()
        canvas_name = canvas_name.replace(" ", "_", 10).lower()

        all_template_assets = self.model_object("ContainerAssignedAssets").filter(canvas_id=canvas_id,
                                                                                  template_id=template_id,
                                                                                  wireframe_id=wireframe_id)

        container_ids = all_template_assets.values_list('container_id', flat=True)
        thumbnail_resolution_dict = self.return_container_objects_thumbnail_resolution(template_id, canvas_id)

        print ('template_id, canvas_id', template_id, canvas_id)
        print ('thumbnail_resolution_dict', thumbnail_resolution_dict)

        default_resolution_dict = self.return_container_objects_default_resolution(template_id, canvas_id)

        print ('default_resolution_dict', default_resolution_dict)
        print ('---------')

        resource_list = []
        thumbnail_container_type = 1
        source_container_type = 2

        type_string = self.model_object("Strings").filter(english="Thumbnail Files")
        if type_string:
            thumbnail_container_type = self.model_object("ContainerTypes").filter(type_string=type_string[0])
            thumbnail_container_type = thumbnail_container_type[0].pk

        default_type_string = self.model_object("Strings").filter(english="Default Source Files ")
        if default_type_string:
            source_container_type = self.model_object("ContainerTypes").filter(type_string=default_type_string[0])
            source_container_type = source_container_type[0].pk

        resources = []
        printed_screen_messages += "There are %s total assets to be published in this " \
                                   "template/wireframe/canvas combination <br>" % len(all_template_assets)

        printed_success_connect = False
        printed_path_created = False

        for asset in all_template_assets:
            image = None
            asset_from_container = self.model_object("Assets").filter(pk=asset.asset_id)
            if asset_from_container:
                asset_from_container = asset_from_container[0]
                image = self.model_object("Resources").filter(pk=asset_from_container.source_id)

            check_content_thumbnail = None
            check_content = None
            check_unique_thumbnail = None
            check_unique_thumbnail_files = False

            container = self.model_object("Containers").filter(pk=asset.container_id)
            if container:
                name = container.values_list('container_name__english', flat=True)
                name = name[0]

                if name == "Unique Thumbnail Files":
                    source_container_type = 3
                    check_content_thumbnail = True
                    check_unique_thumbnail_files = True
                else:
                    source_container_type = 2
                    check_content_thumbnail = False
                    check_unique_thumbnail_files = False

            if image:
                image = image[0]
                order = 1
                asset_setting = self.model_object("ContainerAssignedAssetSettings").filter(
                    container_assigned_asset=asset,
                    setting_string="order")

                if asset_setting:
                    order = asset_setting[0].value
                    if order in [0, "0"]:
                        order = 1

                caption = 1
                asset_setting = self.model_object("ContainerAssignedAssetSettings").filter(
                    container_assigned_asset=asset,
                    setting_string="caption")
                if asset_setting:
                    caption = asset_setting[0].value

                watermark = False
                asset_setting = self.model_object("ContainerAssignedAssetSettings").filter(
                    container_assigned_asset=asset,
                    setting_string="watermark")
                if asset_setting:
                    watermark = asset_setting[0].value

                thumbnail_save_path = ''

                if thumbnail_resolution_dict['has_thumbnails']:
                    print('in here', self.project.project_connection_name)

                    template_save_path = self.return_canvas_thumbnails_save_path(
                        self.project.project_connection_name, canvas_name,
                        wireframe_name)

                    thumbnail_save_path = self.return_canvas_thumbnails_save_path(
                        self.project.project_connection_name,
                        canvas_name, wireframe_name) + image.path.split("/")[-1]

                    try:
                        self.model_object("Content").filter(resource_id=image.pk,
                                                            wireframe_id=wireframe_id,
                                                            canvas_id=canvas_id,
                                                            template_id=template_id,
                                                            path__contains=template_save_path).delete()
                    except Exception as e:
                        printed_screen_messages += "Error found: %s" % e
                        print(e)

                    self.model_object("Content").create(resource_id=image.pk,
                                                        container_id=asset.container_id,
                                                        container_type_id=thumbnail_container_type,
                                                        wireframe_id=wireframe_id,
                                                        canvas_id=canvas_id,
                                                        template_id=template_id,
                                                        published=datetime.date.today(),
                                                        published_by=user_id,
                                                        orderidx=order,
                                                        path=thumbnail_save_path)

                    check_content_thumbnail = self.model_object("Content").filter(resource_id=image.pk,
                                                                                  template_id=template_id,
                                                                                  wireframe_id=wireframe_id,
                                                                                  canvas_id=canvas_id,
                                                                                  path=thumbnail_save_path)
                    if check_content_thumbnail:
                        check_content_thumbnail = check_content_thumbnail[0]
                        check_unique_thumbnail = check_content_thumbnail

                template_save_path = self.return_canvas_save_path(self.project.project_connection_name,
                                                                  canvas_name,
                                                                  wireframe_name)

                if not printed_path_created:
                    printed_screen_messages += "Template Path: %s <br>" % template_save_path

                try:
                    if os.path.isdir(os.path.join(base_image_location, template_save_path)):
                        if not printed_path_created:
                            printed_screen_messages += "*** Success - Template path created<br>"
                            printed_path_created = True
                    else:
                        try:
                            os.makedirs(os.path.join(base_image_location, template_save_path))
                        except Exception as e:
                            print ('error', e)
                            printed_screen_messages += "--- Error - Template path not created %s <br>", e

                except Exception as e:
                    print ('error', e)

                creating_save_path = str(template_save_path) + image.path.split("/")[-1]

                if check_unique_thumbnail_files == False:
                    self.model_object("Content").filter(resource_id=image.pk,
                                                        wireframe_id=wireframe_id,
                                                        canvas_id=canvas_id,
                                                        template_id=template_id,
                                                        path__contains=template_save_path).delete()

                    self.model_object("Content").create(resource_id=image.pk,
                                                        container_id=asset.container_id,
                                                        container_type_id=source_container_type,
                                                        wireframe_id=wireframe_id,
                                                        canvas_id=canvas_id,
                                                        template_id=template_id,
                                                        published=datetime.date.today(),
                                                        published_by=user_id,
                                                        orderidx=order,
                                                        path=creating_save_path)

                    check_content = self.model_object("Content").filter(resource_id=image.pk,
                                                                        template_id=template_id,
                                                                        wireframe_id=wireframe_id,
                                                                        canvas_id=canvas_id,
                                                                        path=creating_save_path)

                    if check_content:
                        check_content = check_content[0]

                    check_unique_thumbnail = None

                new_image_dict = {'image': image,
                                  'order': order,
                                  'caption': caption,
                                  'check_content': check_content,
                                  'check_content_thumbnail': check_content_thumbnail,
                                  'check_unique_thumbnail': check_unique_thumbnail,
                                  'watermark': watermark,
                                  'asset_id': asset.pk,
                                  'default_portrait_width': default_resolution_dict['default_portrait_width'],
                                  'default_portrait_height': default_resolution_dict['default_portrait_height'],
                                  'thumbnail_portrait_height': thumbnail_resolution_dict[
                                      'thumbnail_portrait_height'],
                                  'thumbnail_portrait_width': thumbnail_resolution_dict[
                                      'thumbnail_portrait_width'],
                                  'thumbnail_landscape_width': thumbnail_resolution_dict[
                                      'thumbnail_landscape_width'],
                                  'thumbnail_landscape_height': thumbnail_resolution_dict[
                                      'thumbnail_landscape_height'],
                                  'default_landscape_width': default_resolution_dict['default_landscape_width'],
                                  'default_landscape_height': default_resolution_dict[
                                      'default_landscape_height'],
                                  'creating_save_path': creating_save_path,
                                  'thumbnail_save_path': thumbnail_save_path}

                if not image.path in resource_list:
                    resource_list.append(image.path)
                    resources.append(new_image_dict)

            AWS_ACCESS_KEY_ID = 'AKIAJXKAXQUFZALERVTA'
            AWS_SECRET_ACCESS_KEY = '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'
            Bucketname = settings.BUCKETNAME

            project_image_folder = os.path.join(settings.BASE_IMAGE_LOCATION, self.project.project_connection_name)

            bucket = None
            if deliver:
                if not printed_success_connect:
                    printed_screen_messages += '** Connecting to S3-Bucket<br>'

                try:
                    conn = boto.s3.connect_to_region('ap-southeast-2',
                                                     aws_access_key_id=AWS_ACCESS_KEY_ID,
                                                     aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                                                     calling_format=boto.s3.connection.OrdinaryCallingFormat(),
                                                     )

                    bucket = conn.get_bucket(Bucketname)

                    if not printed_success_connect:
                        printed_screen_messages += '*** Success connecting to S3-Bucket<br>'
                        printed_success_connect = True

                except Exception as e:
                    print ('Error connecting to bucket: ', e)
                    printed_screen_messages += 'Error connecting to S3-Bucket %s <br>' % e
                    return {}, printed_screen_messages

        i = 1
        for row in resources:
            # print ('-- ROW ', row
            # print (''
            printed_screen_messages += '-----> <b> Publishing asset %s of %s </b><br>' % (i, len(all_template_assets))
            i += 1

            thumbnail_path = os.path.join(project_image_folder, "temporary")

            try:
                if not os.path.isdir(thumbnail_path):
                    os.makedirs(thumbnail_path)
            except:
                pass

            default_temp_path = os.path.join(project_image_folder, "default_temporary")

            try:
                if not os.path.isdir(default_temp_path):
                    os.makedirs(default_temp_path)
            except:
                pass

            try:
                img = Image.open(row['image'].path)
                image_width, image_height = img.size
            except:

                printed_screen_messages += '---- Not an image file. Will continue to save to S3 <br>'

                if deliver:
                    extension = str(row['image'].path)[-3:]
                    print ('checking extension', extension)
                    print ('file is a movie: ', extension in self.video_files())
                    print ('checking publish movies:', skip_movies)

                    if extension in self.video_files() and skip_movies:
                        printed_screen_messages += '-- *** Item is a movie file and skip publish for movies has been requested<br>'
                        # skip video files
                        print ('-- *** Item is a movie file and skip publish for movies has been requested')
                        continue

                    printed_screen_messages += '---- File path saving of document: %s <br>' % row[
                        'creating_save_path']
                    printed_screen_messages += '---- Actual path of document: %s <br>' % row['image'].path

                    new_resized_image = row['image'].path

                    k = bucket.new_key(row['creating_save_path'])
                    k.set_contents_from_filename(new_resized_image)
                    k.set_acl('public-read')

                    printed_screen_messages += '++ --  Saved document to S3.<br>'

                continue
                # move on to next if not an image file

            if row['check_unique_thumbnail']:
                try:
                    new_resized_image = os.path.join(default_temp_path, row['image'].path.split("/")[-1])
                    img.save(new_resized_image, subsampling=0)

                    image_hash = self.hashfile(new_resized_image, hashlib.md5())

                    row['check_unique_thumbnail'].hash = image_hash
                    row['check_unique_thumbnail'].save(using=self.project.project_connection_name)
                    hashed_file_details.update({row['check_unique_thumbnail'].pk: image_hash})

                    printed_screen_messages += '+++ ---- Hash for thumbnail saved <br>'
                    printed_screen_messages += '---- Attempting to save new thumbnail image to S3 <br>'

                    if deliver:
                        k = bucket.new_key(row['thumbnail_save_path'])
                        k.set_contents_from_filename(new_resized_image)
                        k.set_acl('public-read')
                        printed_screen_messages += '+ --  Saved new thumbnail image.<br>'

                    else:
                        self.image_copy(new_resized_image, row['thumbnail_save_path'])
                        print ('ok: no delivery specified')

                except Exception as e:
                    print ('hash for unique thumbnail did not work.', e)
                    printed_screen_messages += 'Hash for unique thumbnail did not work: %s <br>' % e

            else:
                try:
                    check_thumb_size = int(str(row['thumbnail_portrait_width']).strip())
                except:
                    check_thumb_size = 0

                try:
                    if "thumbnail_portrait_width" in row and check_thumb_size > 0:
                        if int(image_width) < int(image_height):
                            height = row['thumbnail_portrait_height']
                            width = row['thumbnail_portrait_width']

                        else:
                            height = row['thumbnail_landscape_height']
                            width = row['thumbnail_landscape_width']

                        height = int(height)
                        width = int(width)

                        printed_screen_messages += '-- Saving thumbnail to size: width %s, height %s <br>' % (
                        width, height)

                        basewidth = width

                        wpercent = (basewidth / float(image_width))
                        hsize = int((float(image_height) * float(wpercent)))
                        img = img.resize((basewidth, hsize), Image.ANTIALIAS)

                        printed_screen_messages += '+-- Saved thumbnail to size<br>'

                        new_resized_image = os.path.join(default_temp_path,
                                                         row['image'].path.split("/")[-1])

                        try:
                            im = Image.open(new_resized_image)
                            im.thumbnail((basewidth, hsize), Image.ANTIALIAS)
                            im.save(new_resized_image, subsampling=0)
                        except IOError:
                            print("cannot create thumbnail for '%s'" % new_resized_image)

                        img.save(new_resized_image, subsampling=0)
                        printed_screen_messages += '-- Attempting to load thumbnail image to S3 with path %s <br>' % \
                                                   row['thumbnail_save_path']

                        if deliver:
                            k = bucket.new_key(row['thumbnail_save_path'])
                            k.set_contents_from_filename(new_resized_image)
                            k.set_acl('public-read')
                            printed_screen_messages += '+-- Saved thumbnail to S3 <br>'
                        else:
                            self.image_copy(new_resized_image, row['thumbnail_save_path'])

                        printed_screen_messages += '--- Going to save hash for default image (hash 2) new_resized_image: %s <br>' % new_resized_image
                        try:
                            image_hash = self.hashfile(new_resized_image, hashlib.md5())
                            if row['check_content_thumbnail']:
                                row['check_content_thumbnail'].hash = image_hash
                                row['check_content_thumbnail'].save(
                                    using=self.project.project_connection_name)

                                hashed_file_details.update({row['check_content_thumbnail'].pk: image_hash})
                                printed_screen_messages += '+- Successfully saved hash for thumbnail<br>'
                            else:
                                printed_screen_messages += 'Error: no file to save hash for default image - %s<br>' % row['check_content_thumbnail']

                        except Exception as e:
                            printed_screen_messages += 'Error: Saving thumbnail and hash did not work: %s - %s <br>' % (e, row['check_content_thumbnail'])

                except Exception as e:
                    printed_screen_messages += 'Error: Saving thumbnail and hash did not work: %s <br>' % e

                printed_screen_messages += '--- Starting source / default image details <br>'

                try:
                    check_portrait = int(str(row['default_portrait_height']).strip())
                except:
                    check_portrait = 0

                try:
                    check_landscape = int(str(row['default_landscape_height']).strip())
                except:
                    check_landscape = 0

                if "default_portrait_height" in row and check_portrait > 0 and check_landscape > 0:
                    if int(image_width) < int(image_height):

                        height = int(str(row['default_portrait_height']).strip())
                        width = int(str(row['default_portrait_width']).strip())

                    else:
                        height = int(str(row['default_landscape_height']).strip())
                        width = int(str(row['default_landscape_width']).strip())

                    height = int(height)
                    width = int(width)
                    printed_screen_messages += '-- Default size: width %s, height %s <br>' % (width, height)
                    basewidth = width

                    wpercent = (basewidth / float(image_width))
                    hsize = int((float(image_height) * float(wpercent)))
                    img = img.resize((basewidth, hsize), Image.ANTIALIAS)

                try:
                    new_resized_image = os.path.join(default_temp_path, row['image'].path.split("/")[-1])
                    img.save(new_resized_image, subsampling=0)

                    printed_screen_messages += '+-- Saved default to size: %s  at path: %s <br>' % (
                    img.size, new_resized_image)

                    if deliver:
                        printed_screen_messages += 'Deliverying Source image to S3 at path %s <br>' % row[
                            'creating_save_path']

                        k = bucket.new_key(row['creating_save_path'])
                        k.set_contents_from_filename(new_resized_image)
                        k.set_acl('public-read')

                        printed_screen_messages += '+---- Saved source image to S3<br>'
                    else:
                        self.image_copy(new_resized_image,
                                        os.path.join(base_image_location, row['creating_save_path']))

                    if not os.path.isfile(os.path.join(base_image_location, row['creating_save_path'])):
                        try:
                            self.image_copy(new_resized_image,
                                            os.path.join(base_image_location, row['creating_save_path']))
                        except:
                            print ('Cannot copy to s3 bucket')

                    printed_screen_messages += '--- Going to save hash for default image (hash 1)<br>'
                    try:
                        image_hash = self.hashfile(new_resized_image, hashlib.md5())

                        if row['check_content']:
                            row['check_content'].hash = image_hash
                            row['check_content'].save(using=self.project.project_connection_name)

                            hashed_file_details.update({row['check_content'].pk: image_hash})
                            printed_screen_messages += '+- Successfully saved hash for default image<br>'

                    except Exception as e:
                        printed_screen_messages += 'Error: Saving default hash did not work: %s <br>' % e

                        if deliver:
                            printed_screen_messages += 'Deliverying Source image to S3<br>'
                            k = bucket.new_key(row['creating_save_path'])
                            k.set_contents_from_filename(row['image'].path)
                            k.set_acl('public-read')
                            printed_screen_messages += '+---- Saved source image to S3<br>'

                        else:
                            self.image_copy(row['image'].path, row['creating_save_path'])
                            print ('ok: no delivery specified')

                        if not os.path.isfile(os.path.join(base_image_location, row['creating_save_path'])):
                            printed_screen_messages += 'Error: Source image not saved to S3<br>'
                            try:
                                self.image_copy(row['image'].path, row['creating_save_path'])
                            except:
                                print('Cannot copy to s3 bucket')

                except Exception as e:
                    printed_screen_messages += 'Error: Saving default source did not work: %s <br>' % e

                    print ('default image error: ', e)
                    print ('source path saved: ', row['creating_save_path'])

                    if deliver:
                        printed_screen_messages += 'Attempting to deliver Source image to S3<br>'
                        k = bucket.new_key(row['creating_save_path'])
                        k.set_contents_from_filename(row['image'].path)
                        k.set_acl('public-read')
                        printed_screen_messages += '+---- Saved source image to S3<br>'

                    else:
                        self.image_copy(row['image'].path, row['creating_save_path'])
                        print ('ok: no delivery specified')

                    if not os.path.isfile(os.path.join(base_image_location, row['creating_save_path'])):
                        printed_screen_messages += 'Error: Source image not saved to S3<br>'
                        printed_screen_messages += "Attempting to move: %s " % row['image'].path

                        try:
                            self.image_copy(row['image'].path,
                                            os.path.join(base_image_location, row['creating_save_path']))
                        except:
                            print ('Cannot copy to s3 bucket')

        printed_screen_messages += "END: Completed going through publish steps. <br>"
        return hashed_file_details, printed_screen_messages

    def image_copy(self, src, dest):

        try:
            shutil.copy2(src, dest)
        except Exception as e:
            print ('error moving', src, ' to ', dest, ' err: ', e)

    def hashfile(self, file, hasher, blocksize=65536):
        if (file == None): return None
        if not os.path.isfile(file):
            if not file == '':
                print ("(%s) does not exist" % (file))
                return None

        print ('processing ' + file)
        afile = open(file, 'rb')
        buf = afile.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(blocksize)
        return hasher.hexdigest()