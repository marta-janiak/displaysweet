from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Descriptions(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Descriptions, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_description_field_id(self, english_value):
        english_value = str(english_value).lstrip().rstrip()
        queryset = self.model_object("Descriptions").filter(english=english_value)
        if queryset:
            return queryset[0]
        else:
            self.model_object("Descriptions").create(english=english_value)
            item_created = self.model_object("Descriptions").filter(english=english_value)
            item_created = item_created[0]
            return item_created