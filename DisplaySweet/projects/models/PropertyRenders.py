from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin



class PropertyRenders(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(PropertyRenders, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_total_renders_count_for_key(self, type_string_value, floor_list=None):
        if floor_list:
            resources = self.objects.filter(key=type_string_value,
                                            property__floor_id__in=floor_list).values_list('resource_id', flat=True).distinct().count()
        else:
            resources = self.objects.filter(key=type_string_value).values_list('resource_id', flat=True).distinct().count()

        return resources
    
    def get_property_render_values(self, field_value, floor_list):
        from core.models import ProjectCSVTemplateFields
        table_mapping_fields = ProjectCSVTemplateFields.objects.filter(template__project=self.project,
                                                                       column_pretty_name=field_value).values_list(
            'column_table_mapping',
            'column_field_mapping').distinct()

        property_values = []
        for table, field in table_mapping_fields:
            try:
                property_values += self.klass("Properties").get_all_distinct_editable_values(field, table)
            except Exception as e:
                print('list error', e)

        property_render_values = []
        for field in property_values:
            total = self.klass("PropertyRenders").get_total_renders_count_for_key(field, floor_list=floor_list)
            property_render_values.append({'key': field, 'total': total})

        return property_render_values

    def get_property_render_resources(self, field_value, floor_list=None):
        file_list = []
        from core.models import ProjectCSVTemplateFields
        table_mapping_fields = ProjectCSVTemplateFields.objects.filter(template__project=self.project,
                                                                       column_pretty_name=field_value).values_list(
            'column_table_mapping',
            'column_field_mapping').distinct()

        property_values = []
        for table, field in table_mapping_fields:
            if floor_list:
                try:
                    property_values += self.klass("Properties").get_all_distinct_editable_values(field, table, floor_list=floor_list)
                except Exception as e:
                    print('list error', e)
            else:
                try:
                    property_values += self.klass("Properties").get_all_distinct_editable_values(field, table)
                except Exception as e:
                    print('list error', e)

        distinct_resource_list = []
        for field in property_values:
            if floor_list:
                renders = self.model_object("PropertyRenders").filter(key=field, property__floor_id__in=floor_list).values_list('key', 'resource_id').distinct()
            else:
                renders = self.model_object("PropertyRenders").filter(key=field).values_list('key',
                                                                                             'resource_id').distinct()

            for key, resource_id in renders:
                if resource_id:

                    resource = self.model_object("Resources").filter(pk=resource_id)
                    if resource:
                        resource = resource[0]

                        file_list.append(
                            {'resource_id': resource.pk, 'key': key, 'resource': resource, 'caption': '', 'orderidx': 1}
                        )

        return file_list

    def get_property_renders_for_property(self, property_id, field_value):
        from core.models import ProjectCSVTemplateFields

        table_mapping_fields = ProjectCSVTemplateFields.objects.filter(template__project=self.project,
                                                                       column_pretty_name=field_value).values_list(
            'column_table_mapping',
            'column_field_mapping').distinct()

        property_values = []
        for table, field in table_mapping_fields:
            try:
                property_values += self.klass("Properties").get_all_distinct_editable_values(field, table)
            except Exception as e:
                print('list error', e)

        property_render_values = []
        for field in property_values:
            total = self.klass("PropertyRenders").get_total_renders_count_for_key(field)
            property_render_values.append({'key': field, 'total': total})

        return property_render_values

    def get_property_render_table(self, field_value):
        field_value = field_value.strip()
        from core.models import ProjectCSVTemplateFields
        table_mapping_field = ProjectCSVTemplateFields.objects.filter(template__project=self.project,
                                                                      column_pretty_name__icontains=field_value).values_list(
            'column_table_mapping', flat=True).distinct()

        if table_mapping_field:
            return table_mapping_field[0]