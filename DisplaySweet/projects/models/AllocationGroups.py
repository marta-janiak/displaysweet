from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from django.template.loader import render_to_string
from .model_mixin import PrimaryModelMixin
from users.models import User


class AllocationGroups(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(AllocationGroups, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def check_first_allocation_group(self):

        try:
            user_id = User.objects.filter(user_type=User.USER_TYPE_SUPER_ADMIN)[0].pk
        except:
            user_id = 1

        default_group = self.model_object("AllocationGroups").filter(name__english='Default')
        if not default_group:
            name = self.model_object("Names").filter(english='Default')
            if not name:
                self.model_object("Names").create(english='Default')
                name = self.model_object("Names").filter(english='Default')

            name = name[0]
            self.model_object("AllocationGroups").create(name=name, owner_id=user_id, is_hidden=1)
            default_group = self.model_object("AllocationGroups").filter(name__english='Default')

            default_group = default_group[0]

            properties = self.model_object("Properties").all().order_by('name__english')
            for property in properties:
                self.model_object("PropertyAllocations").create(allocation_group=default_group, property=property)

    def get_allocation_all_properties(self, template_fields, floors=None, building_id=None, request=None,
                                      page=None, group_id=None, agent_allocated_floors=None, page_end=None):

        allocation_group_ids = []
        properties = self.model_object("Properties").all().order_by('name__english').values_list('pk', flat=True).distinct()

        if request and not (request.user.is_ds_admin or request.user.is_client_admin):

            check_user_allocations = self.model_object("AllocatedUsers").filter(user_id=request.user.pk).values_list(
                'allocation_group_id', flat=True)
            check_allocation_groups = self.model_object("AllocationGroups").filter(pk__in=check_user_allocations).order_by(
                'name')

            allocation_group_ids = check_allocation_groups.values_list('pk', flat=True)

            check_allocation_properties = self.model_object("PropertyAllocations").filter(
                allocation_group=check_allocation_groups).values_list('property_id', flat=True).distinct()

            properties = self.model_object("Properties").filter(pk__in=check_allocation_properties).order_by('name__english').values_list('pk', flat=True).distinct()

        if floors:
            properties = self.model_object("Properties").filter(floor_id__in=floors,
                                                                pk__in=properties).order_by('name__english')
        else:
            if building_id:
                floor_buildings = self.model_object("FloorsBuildings").filter(building_id=building_id).values_list(
                    'floor_id', flat=True)

                properties = self.model_object("Properties").filter(floor_id__in=floor_buildings, pk__in=properties).order_by(
                    'name__english')

        if floors and building_id:
            floor_buildings = self.model_object("FloorsBuildings").filter(building_id=building_id,
                                                                          floor_id__in=floors).values_list('floor_id',
                                                                                                           flat=True)
            properties = self.model_object("Properties").filter(floor_id__in=floor_buildings,
                                                                pk__in=properties).order_by('name__english')

        key_list = []
        main_key_list = []
        property_preview_list = []
        property_reservation_list = []
        floor_buildings = []
        page_list = []
        property_price_field = None

        for property in properties:
            show_property = True

            floor_buildings = self.model_object("FloorsBuildings").filter(floor_id=property.floor_id).values_list(
                'building_id', flat=True)

            property_preview, data_list, key_list = self.klass("Floors").get_select_preview_floor_dictionary(property,
                                                                                                             template_fields)

            for item in key_list:
                if item not in main_key_list:
                    main_key_list.append(item)

                if "price" in str(item).lower() and not property_price_field:
                    property_price_field = item

            property_exclusive = "False"
            check_property_exclusive = self.klass("Properties").get_or_create_property_string_field(property.pk, 'Exclusive')
            if check_property_exclusive:
                property_exclusive = check_property_exclusive[0].value
                if not property_exclusive:
                    property_exclusive = "False"

            property_exclusive_group = None
            check_property_exclusive_group = self.klass("Properties").get_or_create_property_string_field(property.pk,
                                                                                      'Exclusive Allocation Group ID')

            if not request.user.is_ds_admin or not request.user.is_client_admin:
                if check_property_exclusive_group:
                    try:
                        property_exclusive_group = check_property_exclusive_group[0].value
                        if property_exclusive_group and allocation_group_ids:
                            if not int(property_exclusive_group) in allocation_group_ids:
                                show_property = False
                    except Exception as e:
                        pass

            property_allocations = self.model_object("PropertyAllocations").filter(property=property)
            property_allocations_ids = property_allocations.values_list('allocation_group_id', flat=True)
            property_allocations = property_allocations.values_list('allocation_group__name__english', flat=True)

            show_prop_in_all = False
            if group_id and int(group_id) in property_allocations_ids:
                show_prop_in_all = True

            if property_exclusive_group:
                if not show_property or not (int(property_exclusive_group) == group_id):
                    show_prop_in_all = False
            else:
                if not show_property:
                    show_prop_in_all = False

            reserving_agent_id = None
            purchaser = self.model_object("BuyerDetails").filter(property_id=property.pk)
            if purchaser:
                purchaser = purchaser[0]
                reserving_agent_id = purchaser.agent_id

            property_dict = {'property': property,
                             'show_property': show_property,
                             'reserving_agent_id': reserving_agent_id,
                             'property_values': property_preview,
                             'show_prop_in_all': show_prop_in_all,
                             'property_exclusive': property_exclusive,
                             'property_exclusive_group': property_exclusive_group,
                             'property_allocations': property_allocations,
                             'property_buildings': floor_buildings}

            property_preview_list.append(property_dict)

            status_string_fields = self.klass("Properties").get_or_create_property_string_field(property.pk, 'status')

            if status_string_fields:
                if str(status_string_fields[0].value).lower() == "reserved":
                    date_reserved = self.klass("Properties").get_or_create_property_string_field(property.pk, 'Date Reserved')
                    reservation_status = self.klass("Properties").get_or_create_property_string_field(property.pk, 'Reservation Status')
                    purchaser = self.model_object("BuyerDetails").filter(property_id=property.pk)

                    agent_name = "<em>n/a</em>"
                    email = "<em>n/a</em>"
                    phone = "<em>n/a</em>"
                    date_reserved = str(date_reserved[0].value)
                    reservation_status = str(reservation_status[0].value)

                    if purchaser:
                        purchaser = purchaser[0]
                        if purchaser.agent_id:
                            agent = User.objects.get(pk=purchaser.agent_id)

                            if agent:
                                agent_name = agent.get_full_name()
                                email = str(agent.email)
                                phone = str(agent.phone_number)

                    property_preview, data_list, key_list = self.klass("Floors").get_select_preview_floor_dictionary(
                        property,
                        template_fields)

                    reserved_property_preview = {'property': '',
                                                 'date_reserved': date_reserved,
                                                 'reservation_status': reservation_status,
                                                 'agent': agent_name,
                                                 'email': email,
                                                 'phone_number': phone}

                    property_preview.update(reserved_property_preview)

                    reserved_property_dict = {'property': property,
                                              'property_values': property_preview,
                                              'property_allocations': property_allocations,
                                              'property_buildings': floor_buildings}

                    property_reservation_list.append(reserved_property_dict)

        search_filter_dict = []

        for key in main_key_list:
            field = template_fields.filter(column_field_mapping=key)

            if field:
                field = field[0]

                if not field.column_table_mapping in ['Properties', 'Buildings']:

                    if key == "status":
                        property_distinct_options = ['Available', 'Pending', 'Reserved', 'Sold', 'Unavailable']
                    elif key == "Reservation Status":
                        property_distinct_options = ['Current', 'Pending', 'Awaiting Payment', 'Pending Approval',
                                                     'Sold', 'Cancelled']
                    else:
                        type_string = self.model_object("Strings").filter(english=key)
                        property_distinct_options = self.model_object(field.column_table_mapping). \
                            filter(type_string=type_string).values_list('value', flat=True).distinct()

                    property_distinct_options = list(property_distinct_options)
                    property_options = []
                    for item in property_distinct_options:
                        try:
                            property_options.append(int(item))
                        except:
                            try:
                                property_options.append(str(item))
                            except:
                                pass

                    property_options.sort(key=lambda item: ([str, int].index(type(item)), item))
                    search_filter_dict.append({'key': key,
                                               'property_distinct_options': property_options,
                                               'nice_key': str(key).replace('_', ' ').strip().capitalize()})
                else:
                    if key == "floor":

                        property_distinct_options = self.model_object("Properties").all()
                        if agent_allocated_floors:
                            property_distinct_options = property_distinct_options.filter(floor_id__in=agent_allocated_floors)

                        property_distinct_options = property_distinct_options.values_list('floor__name__english',
                                                                                          flat=True).order_by(
                                                                                          'floor__orderidx',
                                                                                          'floor__name__english').distinct()

                        property_distinct_options = list(property_distinct_options)
                        property_options = []
                        for item in property_distinct_options:
                            try:
                                property_options.append(int(item))
                            except:
                                try:
                                    property_options.append(str(item))
                                except:
                                    pass

                        search_filter_dict.append({'key': key,
                                                   'property_distinct_options': property_options,
                                                   'nice_key': str(key).replace('_', ' ').strip().capitalize()})

                    if key == "name":
                        property_distinct_options = self.model_object("Properties").values_list('name__english',
                                                                                                flat=True).order_by('floor__orderidx').distinct()
                        property_distinct_options = list(property_distinct_options)
                        property_options = []
                        for item in property_distinct_options:
                            try:
                                property_options.append(int(item))
                            except:
                                try:
                                    property_options.append(str(item))
                                except:
                                    pass

                        search_filter_dict.append({'key': key,
                                                   'property_distinct_options': property_options,
                                                   'nice_key': str(key).replace('_', ' ').strip().capitalize()})

                    if key == "building_name":
                        floor_buildings = self.model_object("FloorsBuildings").all()\
                            .values_list('building_id', flat=True)

                        property_distinct_options = self.model_object("Buildings").filter(pk__in=floor_buildings).values_list('name__english',
                                                                                                                               flat=True).distinct()
                        property_distinct_options = list(property_distinct_options)
                        property_options = []
                        for item in property_distinct_options:
                            try:
                                property_options.append(int(item))
                            except:
                                try:
                                    property_options.append(str(item))
                                except:
                                    pass

                        property_options.sort(key=lambda item: ([str, int].index(type(item)), item))
                        search_filter_dict.append({'key': key,
                                                   'property_distinct_options': property_options,
                                                   'nice_key': str(key).replace('_', ' ').strip().capitalize()})

                    if key == "property_type":
                        property_options = self.model_object("PropertyTypes").all().values_list('type',
                                                                flat=True).distinct().order_by('type')

                        search_filter_dict.append({'key': key,
                                                   'property_distinct_options': property_options,
                                                   'nice_key': str(key).replace('_', ' ').strip().capitalize()})

        if "status" not in key_list:
            search_filter_dict.append({'key': 'reserved',
                                       'property_distinct_options': ['True', 'False'],
                                       'nice_key': "Reserved"})

        key_display_list = [str(x).replace("_", " ") for x in key_list]

        context = {'project': self.project,
                   'key_display_list': key_display_list,
                   'key_list': key_list,
                   'property_preview_list': property_preview_list,
                   'page': page,
                   'page_list': page_list}

        if request:
            context.update({'request': request})

        html_template = "projects/admin/includes/all_properties.html"
        all_prop_rendered = render_to_string(html_template, context)

        html_template = "projects/admin/includes/all_properties_with_edit.html"
        all_prop_rendered_editable = render_to_string(html_template, context)

        reservation_list_drop = ["allocated", "name", "building name"]
        add_reservation_list = ["reservation_status", "date_reserved", "agent", "email", "phone_number", "building_name"]
        key_display_list =  [x for x in key_display_list if x not in reservation_list_drop]
        key_display_list = add_reservation_list + key_display_list

        context = {'project': self.project,
                   'key_display_list': key_display_list,
                   'property_reservation_list': property_reservation_list}

        if request:
            context.update({'request': request})

        html_template = "projects/admin/includes/properties_with_reservations.html"
        reservation_properties_rendered = render_to_string(html_template, context)

        price_mins = []
        for i in range(0, 5000000, 5000):
            price_mins.append(i)

        price_maxs = []
        for i in range(10000, 5000000, 5000):
            price_maxs.append(i)

        context = {'project': self.project,
                   'key_display_list': key_display_list,
                   'search_filter_dict': search_filter_dict,
                   'key_list': key_list,
                   'price_mins': price_mins,
                   'price_maxs': price_maxs
                   }

        if request:
            context.update({'request': request})

        html_template = "projects/admin/includes/search_filter_properties.html"
        search_filter_rendered = render_to_string(html_template, context)

        if not property_price_field:
            property_price_field = 'retail_price'

        context = {
            'search_filter_dict': search_filter_dict,
            'all_prop_rendered': all_prop_rendered,
            'all_prop_rendered_editable': all_prop_rendered_editable,
            'search_filter_rendered': search_filter_rendered,
            'property_preview_list': property_preview_list,
            'key_display_list': key_display_list,
            'key_list': key_list,
            'property_price_field': property_price_field,
            'building_id': building_id,
            'floor_buildings': floor_buildings,
            'property_reservation_list': property_reservation_list,
            'reservation_properties_rendered': reservation_properties_rendered
        }

        return context