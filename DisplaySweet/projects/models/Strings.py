from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class Strings(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(Strings, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_string_field_id(self, english_value):
        category_queryset = self.model_object("TextTableCategories").filter(category='apartment_strings')

        if category_queryset:
            category = category_queryset

            queryset = self.model_object("Strings").filter(text_table_category=category, english=english_value)

            if queryset:
                return queryset[0]
            else:
                self.project_model_object.objects.using(self.project.project_connection_name).create \
                    (text_table_category=category, english=english_value)
                item_created = self.project_model_object.objects.using(self.project.project_connection_name).filter \
                    (text_table_category=category, english=english_value)
                item_created = item_created[0]
                return item_created

    def get_or_create_strings_instance(self, value):
        display_name = self.model_object("Strings").filter(english=value)
        if not display_name:
            self.model_object("Strings").create(english=value)
            display_name = self.model_object("Strings").filter(english=value)

        if display_name:
            display_name = display_name[0]
            return display_name

    def clean_for_mapping(self, string_value):
        try:
            english_value = str(string_value).replace("  ", " ", 10).replace("/", "", 10).replace(".", "", 10)
            english_value = str(english_value).strip().replace("  ", " ", 10)
            english_value = str(english_value).replace(" ", "_", 10).lower()

            if english_value == "floor plan typical":
                english_value = "floor_plan_typical"

            string_value = english_value
        except Exception as e:
            print(e)
        return string_value

    def clean_for_new_mapping(self, string_value):
        english_value = str(string_value).replace("  ", " ", 10).replace("/", "", 10).replace("2", "", 10).replace("$", "", 10).lower()
        english_value = str(english_value).strip().replace("  ", " ", 10)
        return english_value

    def get_string_item_by_value(self, english_value):
        item_created = self.model_object("Strings").filter(english=english_value)
        if item_created:
            return item_created[0]

    def get_string_from_category(self, string_category_text):
        category = self.klass("TextTableCategories").get_or_create_string_category(string_category_text)
        string_object = self.objects.filter(text_table_category=category)

        if not string_object:
            self.objects.create(text_table_category=category, english='')
            string_object = self.objects.filter(text_table_category=category, english='')

        return string_object[0]

    def check_and_updated_renderd_field(self, selected_render_field, rendered_field_value):
        if selected_render_field:
            if not str(selected_render_field.english).strip() == str(rendered_field_value).strip():

                selected_render_field.english = str(rendered_field_value).strip()
                selected_render_field.save(using=self.project.project_connection_name)

                #remove existing renders for old key
                self.model_object("PropertyRenders").all().exclude(key='Individual').delete()


