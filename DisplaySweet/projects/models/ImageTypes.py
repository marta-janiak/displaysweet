from django.apps import apps
from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin


class ImageTypes(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(ImageTypes, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return self.get_objects(self.class_name)

    def get_image_type_id(self, image_type_string, image_mode, image_output):

        image_type_string_id = self.klass("Strings").get_string_field_id(image_type_string)
        image_mode_string_id = self.klass("Strings").get_string_field_id(image_mode)
        image_output_string_id = self.klass("Strings").get_string_field_id(image_output)

        kwargs = {'{0}'.format('output_string'): image_output_string_id,
                  '{0}'.format('type_string'): image_type_string_id,
                  '{0}'.format('mode_string'): image_mode_string_id}

        try:
            queryset = self.model_object("Imagetypes").filter(**kwargs)

            if not queryset:
                self.model_object("Imagetypes").create(**kwargs)
                image_type = self.model_object("Imagetypes").filter(**kwargs)
                image_type = image_type[0]
            else:
                image_type = queryset[0]

            return image_type
        except:
            pass