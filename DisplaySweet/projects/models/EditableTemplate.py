from django.apps import apps

from django.db.models.fields.related import ForeignKey, ManyToOneRel
from django.forms.models import model_to_dict
from django.conf import settings
import os
from PIL import Image
from .model_mixin import PrimaryModelMixin
from core.models import ProjectCSVTemplate


class EditableTemplate(PrimaryModelMixin, object):
    """

    """
    class_name = None
    project = None

    def __init__(self, project):
        super(EditableTemplate, self).__init__()
        self.class_name = self.__class__.__name__
        self.project = project

    @property
    def objects(self):
        return ProjectCSVTemplate.objects

    def get_first_editable_template(self):

        editable_templates = ProjectCSVTemplate.objects.filter(project=self.project,
                                                               is_editable_template=True).order_by \
            ('-is_editable_template_default', 'id')

        if not editable_templates:
            editable_templates = ProjectCSVTemplate.objects.filter(project=self.project).order_by \
                ('-is_editable_template_default', 'id')

        if editable_templates:
            return editable_templates[0]

