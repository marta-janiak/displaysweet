from django.db import connections


class ProjectCMSSync(object):
    """
    All methods relating to cms sync go on this mixin!
    """
    def check_project_exists(self, project_id, project_name):
        try:
            created_insert = "Select * from app_access.projects where name = '%s';" % project_name
            cursor = connections['default'].cursor()
            cursor.execute(created_insert)
            total = cursor.rowcount
            return total
        except Exception as e:
            print(e)
            return False

    def get_app_project_id(self, project_id, project_name):
        try:
            created_insert = "Select id from app_access.projects where name = '%s';" % project_name
            cursor = connections['default'].cursor()
            cursor.execute(created_insert)
            found_id = cursor.fetchall()
            self.update_syncdb(project_id, created_insert)
            if found_id:
                return found_id
            
        except Exception as e:
            print(e)
            return None

    def add_new_project_into_sync(self, project_id, project_name):
        if self.check_project_exists(project_id, project_name) == 0:
            try:
                created_insert = "INSERT INTO app_access.projects (name, script_id, database_id,homepage_image_id," \
                                 " table_script_id, alternate_ipad_script_id, alternate_table_script_id, " \
                                 "selection_image_id) VALUES " \
                                 "('%s', 0, 0, 0, 0, 0, 0, 0);" % (project_name)

                self.update_syncdb(project_id, created_insert)
            except Exception as e:
                print(e)

    def check_user_exists(self, project_id, first_name, email_address):
        try:
            created_insert = "Select * from app_access.users where first_name = '%s' and email_address = '%s';" \
                             % (first_name, email_address)

            cursor = connections['default'].cursor()
            cursor.execute(created_insert)

            total = cursor.rowcount
            return total
        except Exception as e:
            print ('error in check_user_exists', e)
            return False

    def update_user_email_address(self, project_id, new_email_address, old_email_address):
        try:
            created_insert = "update app_access.users set email_address = '%s' where email_address = '%s';" \
                             % (new_email_address, old_email_address)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print('error in check_user_project_access_exists', e)
            return False

    def check_user_project_access_exists(self, project_name, email_address):
        try:
            created_insert = "Select * from app_access.user_projects where project_id = " \
                             "(Select id from app_access.projects where name = '%s') and user_id in " \
                             "(Select id from app_access.users where email_address = '%s');" \
                             % (project_name, email_address)

            cursor = connections['default'].cursor()
            cursor.execute(created_insert)
            total = cursor.rowcount
            return total
        except Exception as e:
            print ('error in check_user_project_access_exists', e)
            return False

    def add_user_into_sync(self, project_id, first_name, last_name, email, super_user):
        if self.check_user_exists(project_id, first_name, email) == 0:
            try:
                created_insert = "INSERT INTO app_access.users (first_name, last_name, email_address, " \
                                 "email_password, super_user) VALUES " \
                                 "('%s', '%s', '%s', 'welcome', '%s');" % (
                                first_name, last_name, email, super_user)
    
                self.update_syncdb(project_id, created_insert)
            except Exception as e:
                print(e)

    def add_user_access_into_sync(self, project_id, project_name, user_email_address, can_edit, can_reserve):
        try:
            self.add_new_project_into_sync(project_id, project_name)
        except:
            pass

        try:
            created_insert = "INSERT INTO app_access.user_projects (project_id, user_id, can_edit, can_reserve) VALUES " \
                             "((Select id from app_access.projects where name = '%s'), " \
                             "(Select id from app_access.users where email_address = '%s'), %s, %s);" % (
                             project_name, user_email_address, can_edit, can_reserve)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def remove_user_access_from_sync(self, project_id, project_name, user_email_address):
        try:
            created_insert = "DELETE FROM app_access.user_projects where project_id = (Select id from app_access.projects where name = '%s')" \
                             " and user_id = (Select id from app_access.users where email_address = '%s');"  % (
                             project_name, user_email_address)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def update_password_sync(self, project_id, password_text, first_name, last_name, email):
        try:
            self.add_user_into_sync(project_id, first_name, last_name, email, 0)
        except:
            pass
        
        try:
            created_insert = "UPDATE app_access.users set email_password = '%s' where first_name = '%s' " \
                             "and last_name = '%s' and email_address = '%s' ;" % (
                             password_text, first_name, last_name, email)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def send_insert_group_sync(self, project_id, group_name, allocation_group_parent_id, owner_id):
        print ('send_insert_group_sync')
        try:
            created_insert = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                             "(2, %s, 'INSERT into names SET english=''%s'' ');" % (
                             project_id, group_name)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                   "(2, %s, 'INSERT INTO %s (name_id, allocation_group_parent_id, owner_id) " \
                   "VALUES (SELECT id FROM names WHERE english=''%s'', ''%s'', %s) ');" % (project_id,
                                                                                               'allocation_groups',
                                                                                               group_name,
                                                                                               allocation_group_parent_id,
                                                                                               owner_id)

            print ('---- send_insert_group_sync', str(test))
            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_delete_group_sync(self, project_id, group_id):

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                   "(2, %s, 'DELETE from allocation_groups where id = %s');" % (project_id, group_id)

            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_update_sync(self, project_id, table_name, table_column, set_value, english_value, property_id):
        set_value = str(set_value).strip()
        english_value = str(english_value).strip()

        try:
            created_insert = "INSERT INTO cms_temp_sync.sync_backlog (direction, project_id, log) VALUES (2, %s, 'UPDATE %s SET %s=''%s''  " \
                             "WHERE type_string_id IN (SELECT id FROM strings WHERE english=''%s'') AND property_id=%s');" % (
                             project_id, table_name, table_column, set_value, english_value, property_id)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def send_insert_sync(self, project_id, table_name, table_column, property_id, set_value, english_value):
        set_value = str(set_value).strip()
        english_value = str(english_value).strip()

        try:
            created_insert = "INSERT INTO cms_temp_sync.sync_backlog (direction, project_id, log) VALUES (2, %s, 'INSERT INTO %s   " \
                             "(property_id, %s, type_string_id) VALUES (%s, ''%s'', SELECT id FROM strings WHERE english=''%s'')');" % (
                             project_id, table_name, table_column, property_id, set_value, english_value)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def send_delete_sync(self, project_id, table_name, english_value, property_id):
        try:
            test = "INSERT INTO cms_temp_sync.sync_backlog (direction, project_id, log) VALUES (2, %s, 'DELETE from %s   " \
                   "WHERE type_string_id IN (SELECT id FROM strings WHERE english=''%s'') AND property_id=%s');" % (
                   project_id,  table_name, english_value, property_id)

            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_update_allocation_sync(self, project_id, table_name, table_column, first_value, second_id):
        try:
            created_insert = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) " \
                             "VALUES (2, %s, 'UPDATE %s SET %s=''%s'' where id=''%s'' ');" % (
                             project_id, table_name, table_column, first_value, second_id)

            self.update_syncdb(project_id, created_insert)
        except Exception as e:
            print(e)

    def send_insert_allocation_sync(self, project_id, table_name, allocation_group_id, property_id):

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) " \
                   "VALUES (2, %s, 'INSERT INTO %s (allocation_group_id, property_id) VALUES (''%s'',''%s'') ');" % (
                    project_id, table_name, allocation_group_id, property_id)
            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_delete_allocation_sync(self, project_id, property_id, allocation_group_id):

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                   "(2, %s, 'DELETE from property_allocations " \
                   "WHERE property_id=%s and allocation_group_id=%s ');" % (
                   project_id, property_id, allocation_group_id)

            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_insert_user_allocation_sync(self, project_id, allocation_group_id, user_id):

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                   "(2, %s, 'INSERT INTO %s (allocation_group_id, user_id) " \
                   "VALUES (''%s'', ''%s'') ');" % (project_id, 'allocated_users', allocation_group_id, user_id)

            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)

    def send_delete_user_allocation_sync(self, project_id, allocation_group_id, user_id):

        try:
            test = "INSERT INTO cms_temp_sync.allocation_backlog (direction, project_id, log) VALUES " \
                   "(2, %s, 'DELETE FROM allocated_users where allocation_group_id = %s and user_id = %s;')" \
                   % (project_id, allocation_group_id, user_id)

            self.update_syncdb(project_id, test)
        except Exception as e:
            print(e)
    
    def update_syncdb(self, project_id, script_to_sync):

        from django.conf import settings
        from core.models import ProjectSyncBackLog

        cursor = connections['default'].cursor()

        new_log = None

        try:
            new_log = ProjectSyncBackLog.objects.create(direction=2, project_id=project_id, log=script_to_sync)

        except Exception as e:
            print ('exception error ', e)

        if settings.SYNC_CMS:
            try:
                success = cursor.execute(script_to_sync)

                if new_log and success == True:
                    new_log.logged = True
                    new_log.save()
                else:
                    new_log.logged = False
                    new_log.status_log = success
                    new_log.save()

                return new_log

            except Exception as e:
                new_log.logged = False
                new_log.status_log = e
                new_log.save()
                return new_log