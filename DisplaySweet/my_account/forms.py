from django import forms
from django.contrib.auth.forms import (PasswordChangeForm as BasePasswordChangeForm)
from users.models import User, UserModifiedLog
from projects.cms_sync import ProjectCMSSync
from core.models import Project



class UserDetailsForm(forms.ModelForm):

    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={'placeholder': 'Enter a username'}), help_text="Enter a username")
    first_name = forms.CharField(label="First Name", widget=forms.TextInput(attrs={'placeholder': 'Enter a First Name'}), help_text="Enter your first name")
    last_name = forms.CharField(label="Last Name", widget=forms.TextInput(attrs={'placeholder': 'Enter a Last Name'}), help_text="Enter your last name")
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={'placeholder': 'Email Address', 'class': 'input-xlarge', 'style': 'width: 350px;'}), help_text="Enter your email address")

    class Meta:
        model = User
        fields = ('first_name', 'last_name','email', 'username')


class PasswordChangeForm(BasePasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

        self.fields['old_password'].widget.attrs['placeholder'] = "Enter your current password"
        self.fields['new_password1'].label = "New Password:"
        self.fields['new_password2'].label = "Confirm New Password:"

        self.fields['new_password1'].widget.attrs['placeholder'] = "Enter a new password"
        self.fields['new_password2'].widget.attrs['placeholder'] = "Confirm your new password"

    def clean_new_password1(self):
        password1 = self.cleaned_data.get('new_password1')

        min_length = 5

        # At least MIN_LENGTH long
        if min_length and len(password1) < min_length:
            self._errors["new_password1"] = self.error_class(["The new password must be at least %d characters long." % min_length])

        return password1

    def save(self, commit=True):
        user = super(PasswordChangeForm, self).save(commit=False)

        password1 = self.cleaned_data.get('new_password1')
        user.temporary_setup_password = password1
        user.save()

        project = Project.objects.get(name='Master')
        new_sync = ProjectCMSSync()

        new_sync.update_password_sync(project.pk, password1, user.first_name, user.last_name, user.email)
        UserModifiedLog.objects.create_log(user, None, "User password updated %s " % password1)







