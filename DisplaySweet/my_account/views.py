import datetime
import time
from django.conf import settings

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .forms import UserDetailsForm, PasswordChangeForm
from core.navigation import user_navigation


@login_required
def my_account_details(request):
    template = "my_account/detail.html"

    context = {
        'user': request.user,
        'view': {'navigation': user_navigation(request.user)}
    }

    return render(request, template, context)


@login_required
def update_account(request):
    template = "my_account/update_details.html"

    if request.method == "POST":
        form = UserDetailsForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('my_account-details')
    else:
        form = UserDetailsForm(instance=request.user)

    context = {
        'user': request.user,
        'form': form,
        'view': {'navigation': user_navigation(request.user)}
    }

    return render(request, template, context)


@login_required
def update_password(request):
    template = "my_account/update_password.html"

    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return redirect('my_account-details')
    else:
        form = PasswordChangeForm(request.user)

    context = {
        'user': request.user,
        'form': form,
        'view': {'navigation': user_navigation(request.user)}
    }

    return render(request, template, context)


@login_required
def version_view(request, version_idx):
    template = "my_account/version.html"


    context = {
        'user': request.user,
        'version_idx': version_idx,
        'view': {'navigation': user_navigation(request.user)}
    }

    return render(request, template, context)

