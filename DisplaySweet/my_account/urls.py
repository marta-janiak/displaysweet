from django.conf.urls import patterns, url



urlpatterns = patterns('',
    url(r'^$','my_account.views.my_account_details', name='my_account-details'),
    url(r'^update/account/$','my_account.views.update_account', name='my_account-update_details'),
    url(r'^update/password/$', 'my_account.views.update_password', name='my_account-update_password'),
    url(r'^history/(?P<version_idx>\d+)/$', 'my_account.views.version_view', name='my_account-version'),
)
