#!/usr/bin/env python
import os
import sys
import socket

from display_sweet.settings.utils import add_settings_to_environ


if __name__ == "__main__":
    add_settings_to_environ()

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)